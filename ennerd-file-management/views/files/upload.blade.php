@extends($state->dialog ? 'theme/dialog' : 'theme/default')
@section('title', \Fubber\trans("File Upload"))
@section('main')
<div class="padding">
    <?php if(!$state->dialog) { ?>
    <h1>{{\Fubber\trans("File Upload")}}</h1>
    <?php } ?>
    <div id='upload-area' >
        <div id="info" style="text-align: center;">
            <div id='begin-message'>
                <p><i class='fas fa-upload fa-4x'></i></p>
                <p>{{\Fubber\trans("Drag a file to this area to begin uploading.")}}</p>
            </div>
            <div id='progress-message' style='display:none'>
                <p><i class="fas fa-spinner fa-spin fa-4x"></i></p>
                <p><?=\Fubber\trans('<span id="pst">0%</span> complete'); ?></p>
            </div>
        </div>
    </div>
</div>
<input id="filebrowser" type="file" style="width: 5rem" <?php if($accept) { echo 'accept="'.htmlspecialchars($accept).'"'; } ?>>

<style>
    #filebrowser {
        position: fixed;
        top: -100rem;
    }
    #upload-area {
        border: 2px solid #ccc;
        position: absolute;
        top: <?=($state->dialog ? '3rem' : '6rem'); ?>;
        left: 3rem; 
        right: 3rem; 
        bottom: 6rem; 
        display: flex;
        align-items: center;
        justify-content: center;
        transition: all 0.4s;
        cursor: pointer;
    }
    body.is-uploading #upload-area {
        cursor: wait;
    }
    #upload-area.in {
        border: 5px solid #ccc;
    }
    #upload-area.hover, body:not(.is-uploading) #upload-area:hover {
        background-color: #eee;
    }
    #upload-area > div {
        color: #aaa;
        text-align: center;
        width: 20rem;
        transition: all 0.2s;
    }
    #upload-area.hover > div {
        color: #666;
    }
</style>
<script>
    var afterUpload = <?=json_encode($then); ?>;
    
    function uploadFailed(error) {
        <?php if($state->dialog) { ?>
        Theme.Toast.error(data.error, <?=json_encode(\Fubber\trans('Error'));?>);
        Theme.Dialog.close();
        <?php } else { ?>
        setTimeout(function() {
            location.href=location.href;
        }, 2000);
        <?php } ?>
    }
    
    function uploadDone(file) {
        <?php if($state->dialog) { ?>
        Theme.Toast.success(<?=json_encode(\Fubber\trans('Upload Complete'));?>);
        Theme.Dialog.close(file);
        <?php } else { ?>
        location.href=afterUpload;
        <?php } ?>
    }
    
    $(function() {
        $("#filebrowser").on('change', function(e) {
            $("#upload-area").fileupload('add', { files: this.files });
        });
        $("#upload-area")
            .on('click', function() {
                if($(document.body).hasClass('is-uploading')) return;
                $("#filebrowser").trigger('click');
            })
            .fileupload({
                dropZone: $("#upload-area"),
                dataType: "json",
                maxChunkSize: 5000000,
                url: <?=json_encode($trunk); ?>,
                add: function(e, data) {
                    if($(document.body).hasClass('is-uploading')) return;
                    $(document.body).addClass('is-uploading');
                    var submit = data.submit();
                    submit.fail(function(jqXHR, textStatus, errorThrown) {
                        if(jqXHR.responseJSON) {
                            uploadFailed(jqXHR.responseJSON);
                            $(document.body).removeClass('is-uploading').addClass('is-failed');
                        }
                    });
                    
                    submit.always(function(result, textStatus, jqXHR) {
                        if(textStatus == 'parsererror') {
                            Theme.Toast.error(<?=json_encode(\Fubber\trans('An error occurred while uploading'));?>, <?=json_encode(\Fubber\trans('Error'));?>);
                            uploadFailed({error: <?=json_encode(\Fubber\trans("Parser error"));?>});
                        }
                    });
                    
                    $("#begin-message").hide();
                },
                done: function(e, o) {
                    var data = o.jqXHR.responseJSON;
                    if(data.done) {
                        $(document.body).removeClass('is-uploading');
                        uploadDone(data.file);
                    } else if(data.error) {
                        uploadFailed(data);
                    } else {
                        uploadFailed({error: <?=json_encode(\Fubber\trans('An error occurred while uploading')); ?>});
                    }
                },
                progressall: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $("#progress-message").show();
                    $("#pst").text(progress + "%");
                }
            });
        $(document).bind('dragover', function(e) {
            var dropZones = $('#upload-area'),
                timeout = window.dropZoneTimeout;
            if (timeout) {
                clearTimeout(timeout);
            } else {
                dropZones.addClass('in');
            }
            var hoveredDropZone = $(e.target).closest(dropZones);
            dropZones.not(hoveredDropZone).removeClass('hover');
            hoveredDropZone.addClass('hover');
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZones.removeClass('in hover');
            }, 100);
        });
    });
    $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });
</script>
@stop