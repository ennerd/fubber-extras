@extends($state->dialog ? 'theme/dialog' : 'theme/default')
@section('title', 'Files')
@section('main')
<div class="padding">
    <?php if(!$state->dialog) { ?>
    <h1>Files</h1>
    <?php } else { ?>
    <br><br>
    <?php } ?>
    <div>
        <button onclick='upload();'>Upload</button>
    </div>
    <div id='files-area'>
        <ul class='icons'>
        <?php foreach($files as $file) { ?>
        <li>
            <div class="icon" data-file="{{json_encode($file)}}">
                <div class="figure">
                    <?php if($file->isImage()) { ?>
                    <img src="{{$file->getImageUrl(150,150,'max')}}" />
                    <?php } else { ?>
                    <i class="fas fa-4x fa-<?=$file->getIcon(); ?> blue"></i>
                    <?php } ?>
                </div>
                <div class="label">
                    {{$file->filename}}
                </div>
            </div>
        </li>
        <?php } ?>
        </ul>
    </div>
</div>
<script>
var isDialog = <?=json_encode(!empty($state->dialog)); ?>;
var isPicker = <?=json_encode(!empty($state->pick)); ?>;
var viewRoute = <?=json_encode($view_route); ?>;
var pickRoute = <?=json_encode($pick_route); ?>;
$(document.body).on('click', '*[data-file]', function() {
    var file = $(this).attr('data-file');
    file = JSON.parse(file);
    if(isPicker) {
        if(isDialog) {
            Theme.Dialog.close(file);
        } else {
            location.href=pickRoute.replace(':id', file.id);
        }
    } else {
        if(isDialog) {
            Theme.Dialog.open(viewRoute, 'tall', function(res) {
                reloadPart('#files-area');
            });
        } else {
            location.href=viewRoute.replace(':id', file.id);
        }
    }
});
function upload() {
    Theme.Dialog.open(<?=json_encode($upload_path); ?>, 'medium', function(res) {
        if(isPicker) {
            if(isDialog) {
                Theme.Dialog.close(res);
//                Theme.Dialog.close(file);
            } else {
                location.href=pickRoute.replace(':id', file.id);
            }
        } else {
            reloadPart('#files-area');
        }
    });
}
</script>
@stop
