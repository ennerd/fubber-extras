<?php
namespace EnnerdFileMgmt;
use EnnerdFileMgmt\Models\File;
use Fubber\Hooks;
use Fubber\Kernel;
use Fubber\Kernel\State;

use function Fubber\route;
use function Fubber\trans;

class FileController extends \Theme\CRUDController {
    public static bool $enabled = false;
    public static string $prefix = 'admin/files';
    public static string $model = File::class;
    public static bool $enable_rest_api = true;

    /*
    public static function index_title(State $state) {
        return trans('Files');
    }
    public static function index_text(State $state) {
        return trans('Uploaded files');
    }
    public static function index_add_button(State $state) {
        return trans('Upload a file');
    }
    */
    public static $index_cols = [
        'id' => ['left', 1],
        'filename' => ['left', 80],
        'created_by' => ['left', 19, 'filter'],
        ];
      
    public static $view_def = [
        ['field','id'],
        ['field','filename'],
        ['field', 'path'],
        ['cols', ['field','mimetype'], ['field', 'upload_state']],
        ['cols', ['field', 'created_by'], ['field', 'created_date']],
        ]; 
       
    public static $edit_def = [
        ['field','id'],
        ['field','filename'],
        ['field','mimetype'],
        ['cols', ['field', 'created_by'], ['field', 'created_date']],
        ]; 

    public static function delete_text(State $state) {
        return trans('Any direct links to this file will stop working, and if the file has been used elsewhere in the system - those links will be removed.');
    }
        
    public static function index(State $state) {
        $state->access(File::class)->assertPrivilege('List');
        $state->response->noCache();
        
        if(isset($state->get['pick'])) {
            $pick_route = $state->get['pick'];
            if(strpos($pick_route, '?')!==false)
                $pick_route .= '&picked_file=:id';
            else
                $pick_route .= '?picked_file=:id';
        } else {
            $pick_route = null;
        }
        
        $state->access->assertLoggedIn();
        
        if(!static::canList($state))
            throw new \Exceptions\AccessDeniedException();

        $files = static::getRows($state)->order('created_date', true);

        if(!empty($state->get['accept'])) {
            $accept = $state->get['accept'];
            $accepts = explode(",", $accept);
            $sqls = [];
            $vars = [];
            foreach($accepts as $accept) {
                $accept = trim($accept);
                if($accept[0]=='.') {
                    $sqls[] = 'path LIKE ?';
                    $vars[] = '%'.str_replace("*","%", $accept);
                } else {
                    $sqls[] = 'mimetype LIKE ?';
                    $vars[] = str_replace("*","%", $accept);
                }
            }
            $wheres = implode(' OR ', $sqls);
            $files->complexWhere($wheres, $vars);
        }
        $upload_path = '/admin/files/upload/dialog/'.(!empty($state->get->asArray()) ? '?'.http_build_query($state->get->asArray()) : '');

        return $state->view('files/index', [
            'files' => $files,
            'upload_path' => $upload_path,
            'view_route' => str_replace('12345', ':id', route(static::_getVal('prefix').'_view'.(!empty($state->dialog) ? '_dialog' : ''), 12345)),
            'pick_route' => $pick_route,
            ]);
    }
    
    /*
    public static function view(State $state, $id) {
        $file = File::load($id);
        $state->access($file)->assertPrivilege('View', $state->access->isLoggedIn());
        return $state->view('files/view', [
            'file' => $file,
            'variants' => $file->getVariants(),
            ]);
    }
    */
        
    public static function create_dialog(State $state) {
        $state->response->noCache();
        $qs = '';
        if(!empty($state->get->asArray()))
            $qs = '?'.http_build_query($state->get->asArray());
        return $state->redirect(route(static::_getVal('prefix').'_upload_dialog').$qs);
    }  
    
    public static function create(State $state) {
        $state->response->noCache();
        return $state->redirect(route(static::_getVal('prefix').'_upload', $file->id));
    }  

    public static function upload_dialog(State $state) {
        $state->dialog = true;
        return static::upload($state);
    }
    
    public static function upload(State $state) {
        $state->access(File::class)->assertPrivilege('Create');
        $state->response->noCache();
        $file = File::create($state);
        $res = $file->save();
        if($state->dialog) {
            $then = null;
        } else if(isset($state->get['then'])) {
            $then = $state->get['then'];
            if(strpos($then, '?')===false)
                $then .= '?file_id='.$file->id;
            else
                $then .= '&file_id='.$file->id;
        } else {
            $then = route(static::$prefix.'_view', $file->id);
        }
        
        return $state->view('files/upload', [
            'file_id' => $file->id,
            'trunk' => route('admin/files_upload_chunk', $file->id),
            'then' => $then,
            'accept' => isset($state->get['accept']) ? $state->get['accept'] : null,
            ]);
    }
    
    public static function upload_chunk(State $state, $id) {
        $state->json = true;
        $db = \Fubber\Db::create();
        $upload = File::load($id);
        $state->access(File::class)->assertPrivilege('Create');

        if(!$upload) throw new \Exceptions\AccessDeniedException(trans("No such file trunk"));
        if($upload->created_by != $state->user_id) throw new \Exceptions\AccessDeniedException(trans("Not your file trunk"));
        if($upload->upload_state == 'failed') throw new Exception(trans('Upload is failed'));
        if($upload->upload_state == 'ready') throw new Exception(trans("Upload is complete"));

        $file = $state->request->getUploadedFiles()['files'];
        $range = $state->request->getHeader('Content-Range');
        if(!$range) {
            // The file is smaller than chunk size, so we have the entire file immediately
            $upload->upload_state = 'ready';
            $upload->filesize = filesize($file['tmp_name'][0]);
            $upload->filename = \Normalizer::normalize($file['name'][0]);
            $upload->path = $upload->suggestPath();
            $upload->mimetype = mime_content_type($file['tmp_name'][0]);
            if($upload->save()) {
                if($upload->receiveUploadedFile($file['tmp_name'][0])) {
                    return $state->json([ 'done' => true, 'file' => $upload, ]);
                } else {
                    $upload->delete();
                    throw new \EnnerdFileMgmt\Exception(trans("Unable to move uploaded file"));
                }
            }
            throw new \EnnerdFileMgmt\Exception(trans("Unable to save upload"));
        }
        $range = $range[0];
        $range = substr($range, 6);
        list($range, $length) = explode("/", $range);
        list($start, $end) = explode("-", $range);

        if(!$upload->filename) {
            $upload->upload_state = 'uploading';
            $upload->filesize = $length;
            $upload->filename = \Normalizer::normalize($file['name'][0]);
            $upload->path = $upload->suggestPath();
            $upload->mimetype = 'application/octet-stream';
            $upload->save();
        }

        $row = $db->queryOne('SELECT * FROM efm_file_chunks WHERE file_id=? AND range_start=? AND range_end=?', [
            $upload->id,
            $start,
            $end
            ]);

        if($row) {
            $existed = true;
            // TODO: should delete previous chunk file
            throw new Exception(trans("Chunk already received"));
        } else {
            $existed = false;
        }
        
        $res = $db->exec('INSERT INTO efm_file_chunks (file_id, created_date, range_start, range_end) VALUES (?, ?, ?, ?)', [
            $upload->id,
            gmdate('Y-m-d H:i:s'),
            $start,
            $end,
            ]);
        $chunkId = $db->lastInsertId();
        
        $path = static::getTempRoot($upload->id);
        
        move_uploaded_file($file['tmp_name'][0], $path.'/'.$chunkId);

        if($end+1 == $upload->filesize) {
            // We should have every chunk available now
            $firstChunk = null;
            for($i = 0; $i < $upload->filesize; $i++) {
                $chunk = $db->queryOne('SELECT * FROM efm_file_chunks WHERE file_id=? AND range_start=? ORDER BY range_start LIMIT 1', [
                    $upload->id,
                    $i,
                    ]);
                    
                if(!$chunk) {
                    $upload->upload_state = 'failed';
                    $upload->save();
                    foreach(glob($path.'/*') as $file) /* */
                        unlink($file);
                    rmdir($path);
                    throw new Exception(trans("Upload failed"));
                } else {
                    if(!$firstChunk) {
                        $firstChunk = $chunk;
                    } else {
                        shell_exec('cat '.escapeshellarg($path.'/'.$chunk->id).' >> '.escapeshellarg($path.'/'.$firstChunk->id));
                        unlink($path.'/'.$chunk->id);
                        $db->exec('DELETE FROM efm_file_chunks WHERE id=?', [$chunk->id]);
                    }
                }
                $i = $chunk->range_end;
            }
            rmdir($path);
            $finished = true;
            $upload->upload_state = 'ready';
            $upload->filesize = filesize($path.'/'.$firstChunk->id);
            $upload->filename = \Normalizer::normalize($file['name'][0]);
            $upload->path = $upload->suggestPath();
            $upload->mimetype = mime_content_type($file['tmp_name'][0]);
            $upload->receiveFile($path.'/'.$firstChunk->id);
            $upload->save();
            $db->exec('DELETE FROM efm_file_chunks WHERE file_id=?', [$upload->id]);
        } else {
            $finished = false;
        }
        

        return $state->json([ 'done' => $finished, 'file' => $finished ? $upload : null, 'chunk_id' => $chunkId]);
     }

    public static function init(Kernel $kernel=null) {
        if (!static::_getVal('enabled')) {
            return;
        }
        parent::init();
        \Fubber\Hooks::listen('Theme.ControlPanel.Icons', [ static::class, 'theme_controlpanel_icons' ], 1);
        \Fubber\Hooks::listen('Kernel.Cron', [ static::class, 'cron' ]);
        //Hooks::listen(static::class.'.ConvertFile', [ FileTypeConverter::class, 'file_converter_hook']);
        //Hooks::listen(static::class.'.ConvertFile', [ FileTypeConverter::class, 'subs_file_converter']);
        //Hooks::listen(static::class.'.view', [ static::class, 'view_extra']);
    }
    
    public static function view_toolbar(State $state, $file) {
        $path = [];
        $path[] = ['html', '<img src="'.$file->getUrl().'@h=100.png" style="padding:0;margin:0">'];
        $path[] = ['html', '<a target="_blank" href="'.$file->getUrl().'">'.trans("Open in new window").'</a>'];
        return [
            ['toolbar', $path],
        ];
    }
    
    public static function view_extra(State $state, $file) {
        $state->access($file)->assertPrivilege('View');
        return $state->view('files/view', [
            'file' => $file,
            'variants' => $file->getVariants(),
            ]);
    }

    public static function routes($kernel = null) {
        if (!static::_getVal('enabled')) {
            return [];
        }
        $routes = [
            static::_getVal('prefix').'_upload GET /admin/files/upload/' => [static::class, 'upload'],
            static::_getVal('prefix').'_upload_dialog GET /admin/files/upload/dialog/' => [static::class, 'upload_dialog'],
            'admin/files_upload_chunk POST /admin/files/{id:\d+}/upload-chunk/' => [ static::class, 'upload_chunk' ],
            ] + parent::routes();
            
        $kernel = Kernel::init();
        $base_url = $kernel->env->baseUrl;
        $userfiles_url = $kernel->env->uploadUrl;
        if(strpos($userfiles_url, $base_url)===0) {
            // We can dynamically generate scaled images inside userfiles
            $dynFileRoot = rtrim(substr($userfiles_url, strlen($base_url)), '/');
            //$routes['admin/files_scaled_image GET '.$dynFileRoot.'/{path:[\d\/]+}/{filename}@{params}.{ext:jpg|gif|png|webp}'] = [ static::class, 'scaled_image' ];
            $routes['admin/files_convert_params GET '.$dynFileRoot.'/{path:[\d\/]+}/{filename}@{params}.{ext:.*}'] = [ static::class, 'files_convert_params' ];
            $routes['admin/files_convert GET '.$dynFileRoot.'/{path:[\d\/]+}/{filename}'] = [ static::class, 'files_convert' ];
        }
        
        return $routes;        
    }
    
    /**
     * Any request to file with params in format /path/to/file.ext@params=vals&params=vals.ext
     */
    public static function files_convert_params(State $state, $path, $filename, $params, $ext) {
        if(trim($params, '0123456789abcdefghijklmnopqrstuvwxyz&=,-') != '')
            throw new \Exceptions\NotFoundException('Illegal parameters provided. Lower case characters, numbers and - & = and , allowed.');

        $originalPath = "$path/$filename@$params.$ext";
        $kernel = Kernel::init();
        $userfiles = $kernel->env->uploadRoot;

        $filename = urldecode($filename);
        $sourceFile = "$userfiles/$path/$filename";
        if(!file_exists($sourceFile))
            throw new \Exceptions\NotFoundException("File $path/$filename not found");
        
        $destinationFile = "$userfiles/$path/$filename@$params.$ext";
        parse_str($params, $parsedParams);
        return static::do_files_convert($state, $sourceFile, $destinationFile, $parsedParams, $originalPath);
    }
    
    /**
     * Any request to file with params in format /path/to/file.ext.ext
     */
    public static function files_convert(State $state, $path, $filename) {
        $originalPath = "$path/$filename";
        $toExt = pathinfo($filename, PATHINFO_EXTENSION);
        $tFilename = pathinfo($filename, PATHINFO_FILENAME);
        $fromExt = pathinfo($tFilename, PATHINFO_EXTENSION);
        if($fromExt === '') {
            if(strpos($toExt, '@')!==false)
                throw new \Exceptions\NotFoundException("When converting with parameters, you must provide an extension. For example '"."$path/$filename.jpg".".");
            throw new \Exceptions\NotFoundException("File $path/$filename not found");
        }

        $kernel = Kernel::init();
        $userfiles = $kernel->env->uploadRoot;
        
        $sourceFile = $userfiles.'/'.$path.'/'.$tFilename;
        if(!file_exists($sourceFile))
            throw new \Exceptions\NotFoundException("File $path/$filename not found");
        $destinationFile = $userfiles.'/'.$path.'/'.$filename;
        return static::do_files_convert($state, $sourceFile, $destinationFile, [], $originalPath);
    }
    
    /**
     * Convert a file from $source to $dest using $params[] to adjust conversion.
     */
    public static function do_files_convert(State $state, $source, $dest, array $params, $originalPath='') {
        $targetExt = pathinfo($dest, PATHINFO_EXTENSION);
        if($targetExt !== strtolower($targetExt))
            throw new \Exceptions\NotFoundException("File $originalPath not found. Try using a lowercase extension.");
            
        // Listeners should check that they can handle params and both formats before accepting this hook.
        // 
        $holdPath = dirname($dest).'/.hold-'.basename($dest);
        if(file_exists($holdPath)) {
            // We seem to have this file in holding, so we'll use that instead of invoking regeneration
            rename($holdPath, $dest);
            $result = true;
        } else {
            $result = FileTypeConverter::convert($source, $dest, $params);
        }
//        $result = Hooks::dispatchToFirst(static::class.'.ConvertFile', $source, $dest, $params);
        
        if(!$result) {
            throw new \Exceptions\NotFoundException("File $originalPath not found, and no converter exists for ".pathinfo($source, PATHINFO_EXTENSION)." to ".pathinfo($dest, PATHINFO_EXTENSION)."");
        } else if($result === true) {
            /**
             * Scaling succeeded, so we should make sure this file does not become
             * orphaned
             */
            if(file_exists($dest)) {
                // The file was converted on-demand - so it is fast to regenerate this. We'll schedule it for deletion.
                $userfiles = Kernel::init()->env->uploadRoot;
                
                // For user files, we'll handle automatic renewal
                if(strpos($dest, $userfiles)===0) {
                    $sourcePath = substr($source, strlen($userfiles)+1);
                    $file = File::loadByPath($sourcePath);
                    $relPath = substr($dest, strlen($userfiles)+1);
                    
                    $db = \Fubber\Db::create();
                    $existing = $db->queryOne('SELECT * FROM efm_files_scaled WHERE file_id=? AND path=?', [$file->id, $relPath]);
                    if($existing) {
                        // This file was previously scaled. This indicates that the file is "popular". We'll ensure we wait for a longer time before we'll delete it again.
                        $db->exec('UPDATE efm_files_scaled SET delete_time=?,is_deleted=? WHERE id=?', [gmdate('Y-m-d H:i:s', time() + 60 * pow(2, $existing->delete_count)), '0', $existing->id]);
                    } else {
                        $deleteCount = 0;
                        $db->exec('INSERT INTO efm_files_scaled (file_id, path, delete_time, delete_count) VALUES (?,?,?,?)', [
                            $file->id,
                            $relPath,
                            gmdate('Y-m-d H:i:s', time() + 60 * pow(2, $deleteCount)),
                            0
                            ]);
                    }
                }
                $ct = mime_content_type($dest);
                $res = $state->plain(file_get_contents($dest), ['Content-Type' => $ct]);
                //unlink($dest);
                return $res;
            } else {
                // We expect the file to become available later. Return 202 Accepted
                return $state->plain('File is being processed', ['Content-Type' => 'text/plain'], 202, 'Accepted');
            }
        } else if($result) {
            return $state->redirect($result, 307);
        }
    }
    
    public static function cron($logger) {
        if (!static::_getVal('enabled')) {
            return;
        }
        
        // Remove temp files
        $logger->debug('- Deleting temp files older than 1 hour');
        $tempFiles = glob(sys_get_temp_dir().'/fubber-framework-temp-file-*');
        foreach($tempFiles as $tempFile) {
            if(filemtime($tempFile) < time()-3600) {
                $logger->debug('-- deleting :path', ['path' => $tempFile]);
                unlink($tempFile);
            }
        }

        // Remove old generated images
        /**
         * Images are deleted one hour after they were generated. If another request is made for the same file, we'll double the delete timeout.
         * 
         *
         */ 
        $db = \Fubber\Db::create();
        $toDelete = $db->query('SELECT * FROM efm_files_scaled WHERE is_deleted = "0" AND delete_time < ?', [gmdate('Y-m-d H:i:s')]);
        $possiblyEmptyFolders = [];
        $scaledPath = Kernel::init()->env->uploadRoot;
        $logger->debug('- Deleting expired scaled files');
        foreach($toDelete as $td) {
            $realPath = $scaledPath.'/'.$td->path;
            $possiblyEmptyFolders[$realPath] = $realPath;
            $realPath = realpath($realPath);
            
            if($realPath) {
                echo '- Deleting '.$td->path.'\n';
                $logger->debug('-- Deleting scaled file :path', [
                    'id' => $td->id,
                    'file_id' => $td->file_id,
                    'path' => $td->path,
                    'delete_time' => $td->delete_time,
                    'delete_count' => $td->delete_count,
                    ]);
                $holdPath = dirname($realPath).'/.hold-'.basename($realPath);
                if(file_exists($holdPath))
                    unlink($holdPath);
                rename($realPath, $holdPath);
                //unlink($realPath);
            }
            $db->exec('UPDATE efm_files_scaled SET is_deleted=?, delete_count=delete_count+1 WHERE id=?', ['1', $td->id]);
        }

        // Files that have been deleted for more than one week does not need to fill up the database
        $logger->debug('- Removing records for files deleted more than 14 days ago');
        foreach($db->query('SELECT * FROM efm_files_scaled WHERE is_deleted=? AND delete_time < ?', ['1', gmdate('Y-m-d H:i:s', time() - 86400*14)]) as $td) {
            $realPath = $scaledPath.'/'.$td->path;
            $holdPath = dirname($realPath).'/.hold-'.basename($realPath);
            $logger->debug('-- Expired converted file :id with path :path and hold path :hold_path deleted', ['id' => $td->id, 'path' => $realPath, 'hold_path' => $holdPath]);
            if(file_exists($holdPath)) {
                unlink($holdPath);
            }
            if(file_exists($realPath)) {
                unlink($realPath);
            }
            $db->exec('DELETE FROM efm_files_scaled WHERE id=?', [$td->id]);
        }
        //$db->exec('DELETE FROM efm_files_scaled WHERE is_deleted=? AND delete_time < ?', ['1', gmdate('Y-m-d H:i:s', time() - 86400*7)]);
    

        // Remove failed files. We consider all files that was created more than 24 hours ago and is not ready, to be failed
        $logger->debug('- Deleting failed uploads');
        $db = \Fubber\Db::create();
        $files = File::all()->where('upload_state','<>','ready')->where('upload_state','<>','processing')->where('created_date','<',gmdate('Y-m-d H:i:s', time()-86400));
        foreach($files as $file) {
            if($file->upload_state == 'uploading') {
                $lastChunk = $db->queryOne('SELECT * FROM efm_file_chunks WHERE file_id=? ORDER BY created_date DESC', [$file->id]);
                if(strtotime($lastChunk->created_date) > time()-43200) {
                    continue;
                }
                $logger->info('-- Deleting file :id with state :upload_state', [
                    'id' => $file->id,
                    'created_by' => $file->created_by,
                    'created_date' => $file->created_date,
                    'path' => $file->path,
                    'filename' => $file->filename,
                    'filesize' => $file->filesize,
                    'mimetype' => $file->mimetype,
                    'upload_state' => $file->upload_state,
                    'locked_until' => $file->locked_until,
                    ]);
                $file->delete();
            }
        }    
        
        // Remove upload temp files
        $logger->info('- Cleaning upload temp folder');
        foreach(glob(Kernel::init()->env->privRoot.'/tmp/uploads/*') as $path) {
            $id = basename($path);
            $file = false;
            
                try {
                    if(!File::exists($id))
                        $file = false;
                    else
                        $file = File::load($id);
                
                if(!$file || $file->upload_state=='ready') {
                    if(!$file) {
                        $logger->info('Cleaning upload temp path :path because file :id does not exist', ['id' => $id, 'path' => $path]);
                    } else {
                        $logger->info('Cleaning upload temp path :path because file :id is in ready state', ['id' => $id, 'path' => $path]);
                    }
                    foreach(glob($path.'/*') as $file) {
                        unlink($file);
                    }
                    rmdir($path);
                }
                } catch (\Exception $e) {
                    $logger->error('Unable to load file :id when looking for temp files', ['id' => $id]);
                }
        }
        
        $logger->info('- Removing files that are "new" but never started uploading');
        foreach(File::all()->where('upload_state','=','new')->where('created_date','<',gmdate('Y-m-d H:i:s', time()-86400)) as $file) {
            $logger->debug('-- Removing file id :id which was created :created_date', ['id' => $file->id, 'created_date' => $file->created_date]);
            $file->delete();
        }
        
        FileTypeConverter::cron($logger);
    }
    
    public static function theme_controlpanel_icons($icons, $state) {
        if(!$state->access(File::class)->hasPrivilege('List')) {
            return $icons;
	}
        $icon = new \Theme\Icon(trans('Files'), 'location.href="/admin/files/";', '<i class="fas fa-4x fa-file red"></i>');
        $icon->weight = 100;
        $icons[] = $icon;
        return $icons;
    }

    protected static function getTempRoot($uploadId) {
        $path = Kernel::init()->env->privRoot.'/tmp/uploads/'.$uploadId;
        if(!is_dir($path))
            mkdir($path, 0777, true);
        if(!is_dir($path))
            throw new \Exception("Unable to create path $path");
        return $path;
    }
    
    protected static function getRows(State $state) {
        $rows = parent::getRows($state);
        $rows->where('upload_state','=','ready')->where('variant_of','=',null);
        return $rows;
    }
    
    /**
     * Custom form field, since we want to be able to filter
     */
    public static function form_field(State $state, \Fubber\Forms\Form $form, $name, $attrs) {
        $meta = static::getMeta($state);
        $options = $form->getOptions($name);
        if(!empty($options)) {
            $pickRoute = $meta->getRoute('pick_dialog');
            if(static::canCreate($state))
                $createRoute = $meta->getRoute('create_dialog');
            else 
                $createRoute = null;
                
            $queryString = http_build_query($options);
            $attrs['data-picker'] = $pickRoute.'?'.$queryString;
            if($createRoute)
                $attrs['data-creater'] = $createRoute.'?'.$queryString;
        }
        return parent::form_field($state, $form, $name, $attrs);
    }
    /*
    public static function form_field(State $state, \Fubber\Forms\Form $form, $name, $attrs) {
        var_dump($name);
        var_dump($attrs);
        die();
    }
    */
    
    public static function ffmpeg_bin() {
        return 'ffmpeg';
        // ffmpeg no longer bundled. Assume it is available in path
        if(PHP_INT_SIZE == 8) {
            return dirname(dirname(__DIR__)).'/bin/ffmpeg-4.1.1-amd64-static/ffmpeg';
        } else {
            return dirname(dirname(__DIR__)).'/bin/ffmpeg-4.1.1-i686-static/ffmpeg';
        }
    }

    public static function ffprobe_bin() {
        return 'ffprobe';
        // ffmpeg no longer bundled. Assume it is available in path
        if(PHP_INT_SIZE == 8) {
            return dirname(dirname(__DIR__)).'/bin/ffmpeg-4.1.1-amd64-static/ffprobe';
        } else {
            return dirname(dirname(__DIR__)).'/bin/ffmpeg-4.1.1-i686-static/ffprobe';
        }
    }
}
