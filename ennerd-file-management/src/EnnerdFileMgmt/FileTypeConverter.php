<?php
namespace EnnerdFileMgmt;
use EnnerdFileMgmt\Models\File;
use Fubber\Hooks;

class FileTypeConverter {
    public static function convert($source, $finalPath, $params=[]) {
        // See if any of my converters will handle this file
        $t = microtime(true);
        if($res = static::image_file_converter($source, $finalPath, $params))
            return $res;
        if($res = static::subs_file_converter($source, $finalPath, $params))
            return $res;
        if($res = static::imagick_image_formats($source, $finalPath, $params))
            return $res;
        if($res = static::ffmpeg_video_formats($source, $finalPath, $params)) {
            return $res;
        }
        return;
    }
    
    public static function ffmpeg_video_formats($source, $finalPath, $params) {
        $targetExt = pathinfo($finalPath, PATHINFO_EXTENSION);
        $sourceFormat = strtolower(pathinfo($source, PATHINFO_EXTENSION));
        // Will only convert from mp4, mov, avi, mkv, flv
        if(!in_array($sourceFormat, ['mp4', 'mov', 'avi', 'mkv', 'flv']))
            return;

        // Will only convert to mp4
        if($targetExt != 'mp4')
            return;

        // Will only accept simple params
        if(sizeof($params)>1)
            return;
        $legalParams = ['360','480','720','1080','optimized'];
        $ok = false;
        foreach($legalParams as $p) {
            if(isset($params[$p])) {
                $ok = true;
                break;
            }
        }
        if(!$ok) return;

        // Will only convert uploaded files
        $file = File::loadByPath($source);
        if(!$file) return;

        $redirectToBestVariant = function($file) {
            foreach(['mp4-optimized', 'mp4-1080', 'mp4-720', 'mp4-480', 'mp4-360'] as $variant) {
                if($variant = $file->getVariant($variant)) {
                    if($variant->fileExists())
                        return ''.$variant->getUrl();
                    else
                        $variant->delete();
                }
            }
            return $file->getUrl();
        };
        
        // Check if file is already enqueued
        $db = \Fubber\Db::create();
        $row = $db->query('SELECT * FROM efm_file_converts WHERE efm_file_id=? AND final_path=?', [$file->id, $finalPath]);
        if($row) {
            if($sourceFormat == 'mp4') {
                // We can redirect to the mp4 as a temporary solution
                return $redirectToBestVariant($file);
            }
            return true;
        }
        $res = $db->exec('INSERT INTO efm_file_converts (efm_file_id, final_path, created_date, params) VALUES (?, ?, NOW(), ?)', [
            $file->id,
            $finalPath,
            serialize($params),
            ]);
        return $redirectToBestVariant($file);
    }

    /**
     * Provides an image API modelled after www.imgix.com CDN. Format of URL is slightly different:
     * 
     * /files/2018/08/20/myfile.jpg@w=500.jpg            Equivalent to imgix.com /files/2018/20/myfile.jpg?w=500&fm=jpg
     * 
     * The following parameters are supported:
     * 
     * https://docs.imgix.com/apis/url/adjustment/bri
     * https://docs.imgix.com/apis/url/adjustment/con
     * https://docs.imgix.com/apis/url/adjustment/invert
     * https://docs.imgix.com/apis/url/bg
     * https://docs.imgix.com/apis/url/format/fm                                Supports jpg, gif, png and webp. Specified by file extension.
     * https://docs.imgix.com/apis/url/rotation/flip
     * https://docs.imgix.com/apis/url/rotation/rot
     * https://docs.imgix.com/apis/url/size/crop                                Supports top,bottom,left,right and combinations
     * https://docs.imgix.com/apis/url/size/h
     * https://docs.imgix.com/apis/url/size/w
     * https://docs.imgix.com/apis/url/size/max-h
     * https://docs.imgix.com/apis/url/size/max-w
     * https://docs.imgix.com/apis/url/size/min-h
     * https://docs.imgix.com/apis/url/size/min-w
     * https://docs.imgix.com/apis/url/size/fit                                 Supports clip, crop, fill, fillmax, max, min and scale
     * https://docs.imgix.com/apis/url/size/rect
     * https://docs.imgix.com/apis/url/stylize/blur                             Similar blurring
     * https://docs.imgix.com/apis/url/stylize/px
     * https://docs.imgix.com/apis/url/stylize/sepia
     * 
     * The following is being considered:
     * 
     * https://docs.imgix.com/apis/url/auto                                     
     * https://docs.imgix.com/apis/url/border-and-padding/border
     * https://docs.imgix.com/apis/url/border-and-padding/border-radius-inner
     * https://docs.imgix.com/apis/url/border-and-padding/border-radius
     * https://docs.imgix.com/apis/url/border-and-padding/pad
     * https://docs.imgix.com/apis/url/color-palette
     * https://docs.imgix.com/apis/url/dpr
     * https://docs.imgix.com/apis/url/face-detection
     * https://docs.imgix.com/apis/url/focalpoint-crop
     * https://docs.imgix.com/apis/url/format/chromasub                         Probably via imagick
     * https://docs.imgix.com/apis/url/format/colorquant                        http://php.net/manual/en/function.imagetruecolortopalette.php
     * https://docs.imgix.com/apis/url/format/dl                                Force download
     * https://docs.imgix.com/apis/url/format/q
     * https://docs.imgix.com/apis/url/mask                                     Probably not
     * https://docs.imgix.com/apis/url/noise-reduction                          Probably via imagick
     * https://docs.imgix.com/apis/url/pdf-page-number   
     * https://docs.imgix.com/apis/url/stylize/htn
     * https://docs.imgix.com/apis/url/stylize/mono
     * 
     * 
     * The generated files are stored in the file system, so that the web server can quickly deliver the file without using PHP.
     * Files are deleted after 1 minute by cron. If files are accessed again, they will get 2 minutes before they are deleted.
     * If they need to be regenerated again, they get 4 minutes, and so on.
     * 
     * Files that have been deleted for more than 1 week will be removed from the database, and must start over with 1 minute grace
     * period.
     * 
     * This ensures that disk space does not get consumed by dead files.
     */
    public static function image_file_converter($source, $finalPath, $params) {
        $targetExt = pathinfo($finalPath, PATHINFO_EXTENSION);
        // We can only generate jpg, jpeg, png and gif files so far.
        $sourceFormat = strtolower(pathinfo($source, PATHINFO_EXTENSION));
        $formats = ['jpg', 'jpeg', 'png', 'gif', 'webp'];
        if(!(in_array($sourceFormat, $formats) && in_array($targetExt, $formats)))
            return;
        // We rely on the ImageProcessor, so we must find the file in the file archive
/*
        $config = \Fubber\Kernel::init()->config;
        $userfiles = $config['userfiles'];
        if(strpos($source, $userfiles)!==0)
            return;
        
        $relPath = substr($source, strlen($userfiles)+1);
        var_dump($source);die();
        $file = File::loadByPath($relPath);
        if(!$file)
            return;
        if(!$file->isImage())
            return;
*/
        $path = dirname($finalPath);
        if(!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $get = $params;
        //parse_str($params, $get);
        $paramsA = [];
        foreach($get as $k => $v) {
            if(in_array($k, ['w', 'h', 'rect', 'fit', 'crop', 'max-h', 'max-w', 'min-w', 'min-h', 'bg', 'bri', 'con', 'invert', 'sat', 'or', 'flip', 'rot', 'blur', 'px', 'sepia']))
                $paramsA[$k] = $v;
            else 
                throw new \Exceptions\NotFoundException('Invalid parameter '.$k.' encountered');
        }


        $processor = new ImageProcessor($source, isset($paramsA['or']) ? $paramsA['or'] : null);
        if(isset($paramsA['rect'])) {
            $paramsA['rect'] = explode(",", $paramsA['rect']);
            if(sizeof($paramsA['rect']) != 4)
                throw new \Exceptions\NotFoundException('Parameter rect: Exactly 4 values expected.');

            call_user_func_array([$processor, 'rect'], $paramsA['rect']);
        }
        if(isset($paramsA['flip'])) {
            
            switch($paramsA['flip']) {
                case 'h' :
                    $processor->flipHorizontal();
                    break;
                case 'v' :
                    $processor->flipVertical();
                    break;
                case 'vh' :
                case 'hv' :
                    $processor->flipHorizontal();
                    $processor->flipVertical();
                    break;
                default :
                    throw new \Exceptions\NotFoundException('Parameter flip: must be one of h, v or hv');
            }
        }
        
        if(isset($paramsA['w']) || isset($paramsA['h']) || isset($paramsA['fit']) || isset($paramsA['crop'])) {
            $processor->resizeTo(isset($paramsA['w']) ? $paramsA['w'] : null, isset($paramsA['h']) ? $paramsA['h'] : null, isset($paramsA['fit']) ? $paramsA['fit'] : null, isset($paramsA['crop']) ? $paramsA['crop'] : null, $paramsA);
        }
        
        if(isset($paramsA['con'])) {
            $processor->contrast($paramsA['con']);
        }

        if(isset($paramsA['bri'])) {
            $processor->brightness($paramsA['bri']);
        }
        
        if(isset($paramsA['invert'])) {
            if($paramsA['invert'] == 'true' || $paramsA['invert'] == '1')
                $processor->invert();
            else if(!($paramsA['invert'] == 'false' || $paramsA['invert'] == '0'))
                throw new \Exceptions\NotFoundException('Invalid value for invert');
        }
        
        if(isset($paramsA['sat'])) {
            $processor->saturation($paramsA['sat']);
        }
        
        if(isset($paramsA['rot'])) {
            $processor->rotate($paramsA['rot']);
        }
        
        if(isset($paramsA['px'])) {
            $processor->pixellate($paramsA['px']);
        }
        
        if(isset($paramsA['blur'])) {
            $processor->blur($paramsA['blur']);
        }
        
        if(isseT($paramsA['sepia'])) {
            $processor->sepia($paramsA['sepia']);
        }

        switch($targetExt) {
            case 'jpg' :
                $ct = 'image/jpeg';
                imagejpeg($processor->image(), $finalPath);
                break;
            case 'gif' :
                $ct = 'image/gif';
                imagegif($processor->image(), $finalPath);
                break;
            case 'png' :
                $ct = 'image/png';
                imagepng($processor->image(), $finalPath);
                break;
            case 'webp' :
                $ct = 'image/webp';
                imagewebp($processor->image(), $finalPath);
                break;
        }

        $processor->writeProfilesTo($finalPath);
        return true;

        $res = $state->plain(file_get_contents($finalPath), ['Content-Type' => $ct]);
        //unlink($finalPath);
        
        $db = \Fubber\Db::create();
        $existing = $db->queryOne('SELECT * FROM efm_files_scaled WHERE file_id=? AND path=?', [$file->id, $relPath]);
        if($existing) {
            $db->exec('UPDATE efm_files_scaled SET delete_time=?,is_deleted=? WHERE id=?', [gmdate('Y-m-d H:i:s', time() + 60 * pow(2, $existing->delete_count)), '0', $existing->id]);
        } else {
            $db->exec('INSERT INTO efm_files_scaled (file_id, path, delete_time, delete_count) VALUES (?,?,?,?)', [
                $file->id,
                $relPath,
                gmdate('Y-m-d H:i:s', time() + 60 * pow(2, $deleteCount)),
                0
                ]);
        }

        return $res;
    }
    
    /**
     * This should be a module. Lazy me integrated it directly.
     */
    public static function subs_file_converter($source, $finalPath, $params) {
        $fromExt = strtolower(pathinfo($source, PATHINFO_EXTENSION));
        
        // We don't know if this is a subtitle file and won't bother checking. (Perhaps wrong decision?)
        if($fromExt === 'json')
            return;
        
        $toExt = pathinfo($finalPath, PATHINFO_EXTENSION);

        $exts = [
            'srt' => 'Subrip',
            'vtt' => 'Webvtt',
            'json' => 'Json',
            'sbv' => 'SBV',
            'ssa' => 'Substationalpha',
            'ttml' => 'Ttml',
            ];

        if(!isset($exts[$fromExt]) || !isset($exts[$toExt]))
            return;

        // Triggers autoload, so we'll wait as long as possible for checking
        if(!class_exists(\Captioning\Converter::class))
            return;

        try {
            $sourceClass = 'Captioning\\Format\\'.$exts[$fromExt].'File';
            $convertTo = $exts[$toExt];
            
            $sourceConverter = new $sourceClass($source);
            $destConverter = $sourceConverter->convertTo($convertTo);
            $destConverter->save($finalPath);
            return true;
        } catch (\Exception $e) {
            var_dump($e);
            die();
        }
    }
    
    /**
     * Tries to convert from any of the formats that imagick supports to a format
     * that is supported by imageprocessor
     */
    public static function imagick_image_formats($source, $dest, $params) {
        if(!class_exists(\Imagick::class))
            return;
        // If we can't convert to this format, we'll quit immediately
        $destExt = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
        $destFormats = ['jpg', 'jpeg', 'png', 'gif', 'webp'];
        if(!in_array($destExt, $destFormats))
            return;
            
        // Is this a format that imagick can read?
        $formats = \Imagick::queryFormats();
        $fromExtension = strtolower(pathinfo($source, PATHINFO_EXTENSION));
        $formats = array_map('strtolower', $formats);

        if(!(in_array($fromExtension, $formats)))
            return false;

        $filesToCleanup = [];

        $tempFile = null;
        if($fromExtension == 'svg') {
            ob_start();
            $svgoPath = trim(`which svgo`);
            ob_end_clean();


            // We'll use svgo to optimize large SVGs, if we have svgo available
            // Need to find a way to bundle this.
            if(file_exists($svgoPath)) { //'/home/newlarerromdev/bin/svgo')) {
                $tempFile = tempnam(sys_get_temp_dir(), 'svgo');
                $tempFileT = $tempFile.'.svg';
                $filesToCleanup[] = $tempFile;
                $filesToCleanup[] = $tempFileT;
//                $cmd = '/home/newlarerromdev/bin/node /home/newlarerromdev/bin/svgo --enable=inlineStyles --config '.escapeshellarg(json_encode([
                $cmd = $svgoPath.' --enable=inlineStyles --config '.escapeshellarg(json_encode([
                    'plugins' => [
                        [
                            'inlineStyles' => [
                                'onlyMatchedOnce' => false
                                ]
                            ]
                        ]
                    ])).' -i '.escapeshellarg($source).' -o '.escapeshellarg($tempFileT);
                $res = shell_exec($cmd.' 2>&1');

                if(!file_exists($tempFileT) || filesize($tempFileT)===0) {
                    unlink($tempFileT);
                    unlink($tempFile);
                    $tempFile = null;
                }
            }
        }

        // Both formats seem to be supported
/* FRA LAREPENGER-SIDEN 2020-10-08

        try {
                if(file_exists('/usr/bin/convert')) {
                        if($tempFile)
                                $tempSource = $tempFileT;
                        else
                                $tempSource = $source;

                        $tmpFile = tempnam(sys_get_temp_dir(), 'filetypeconvert');
                        $filesToCleanup[] = $tmpFile;
                        shell_exec($cmd = '/usr/bin/convert  -channel rgba -background "rgba(0,0,0,0)" -density 500 '.escapeshellarg($tempSource).' '.escapeshellarg($tmpFile.'.png'));
                        $res = static::image_file_converter($tmpFile.'.png', $dest, $params);
                } else {
                    $im = new \Imagick();
                    $im->setBackgroundColor(new \ImagickPixel('rgba(0,0,0,0)'));
                    if($tempFile) {
                        //echo file_get_contents($tempFile);die();
                        $im->readimage($tempFileT);
                    } else {
                        $im->readimage($source);
                    }
                    $im->setResolution(4000,4000);
                        $im->scaleImage(2000,2000);
                    $im->setImageFormat('png32');
                    $tmpFile = tempnam(sys_get_temp_dir(), 'filetypeconvert');
                    $filesToCleanup[] = $tempFile;
                    $im->writeImage($tmpFile.'.png');
                    $res = static::image_file_converter($tmpFile.'.png', $dest, $params);
                }
        } catch (\Exception $e) {
             die("TROUBLE");
        }
*/
        try {
		if(file_exists('/usr/bin/convert')) {
			if($tempFile)
				$tempSource = $tempFileT;
			else
				$tempSource = $source;

		        $tmpFile = tempnam(sys_get_temp_dir(), 'filetypeconvert');
			$filesToCleanup[] = $tmpFile;
			shell_exec($cmd = '/usr/bin/convert  -channel rgba -background "rgba(0,0,0,0)" -density 500 '.escapeshellarg($tempSource).' '.escapeshellarg($tmpFile.'.png'));
			$res = static::image_file_converter($tmpFile.'.png', $dest, $params);
		} else {
	            $im = new \Imagick();
	            $im->setBackgroundColor(new \ImagickPixel('rgba(0,0,0,0)'));
	            if($tempFile) {
	                //echo file_get_contents($tempFile);die();
        	        $im->readimage($tempFileT);
	            } else {
	                $im->readimage($source);
	            }
	            $im->setResolution(4000,4000);
			$im->scaleImage(2000,2000);
	            $im->setImageFormat('png32');
	            $tmpFile = tempnam(sys_get_temp_dir(), 'filetypeconvert');
	            $filesToCleanup[] = $tempFile;
	            $im->writeImage($tmpFile.'.png');
	            $res = static::image_file_converter($tmpFile.'.png', $dest, $params);
		}
	} catch (\Exception $e) {
	     die("TROUBLE");
	}
	
	unlink($tmpFile.'.png');
	unlink($tmpFile);
	return $res;
   }
   
    public static function cron($logger) {
        $logger->info('- Checking for filetype convert jobs');
        $db = \Fubber\Db::create();
        $rows = $db->query('SELECT * FROM efm_file_converts WHERE started_date IS NULL LIMIT 1');
        foreach($rows as $row) {
            $logger->info('-- Converting file :final_path', ['final_path' => $row->final_path]);
            static::do_convert_job($row, $logger);
        }
    }
   
    public static function do_convert_job($row, $logger) {
        $file = File::load($row->efm_file_id);
        $db = \Fubber\Db::create();
        $res = $db->exec('UPDATE efm_file_converts SET started_date=NOW() WHERE id=? AND started_date IS NULL', [$row->id]);
        if(!$res) {
            // Todo: Check that the job has not expired or failed
            return false;
        }

        $targetExt = pathinfo($row->final_path, PATHINFO_EXTENSION);
        $sourceFormat = strtolower(pathinfo($file->path, PATHINFO_EXTENSION));
        $params = unserialize($row->params);

        if(in_array($sourceFormat, ['mp4', 'mov', 'avi', 'mkv', 'flv']) && $targetExt=='mp4') {
            $success = static::do_ffmpeg_convert($file, $params, $row->final_path, $row, $logger);
/* FRA LAREPENGER 2020-10-08
            if($success) {
                $db->exec('DELETE FROM efm_file_convers WHERE id=?', $row['id']);
            }
*/
        }
    }
    
    public static function do_ffmpeg_convert($file, $params, $finalPath, $row, $logger) {
        $sourceFile = $file->getTempFile();
        $cmd = FileController::ffmpeg_bin();
        //$cmd = '/usr/bin/ffmpeg';
        $cmd .= ' -threads 0 -hide_banner -loglevel panic -y -i '.escapeshellarg($sourceFile);
        $cmd .= ' -pix_fmt yuv420p -profile:v main -level 4.0 -preset veryfast';
        //$cmd .= ' -hide_banner -loglevel panic -y -i '.escapeshellarg($sourceFile);
        //$cmd .= ' -pix_fmt yuv420p -profile:v main -level 4.0 -b:a 128k';
        if(isset($params['360'])) {
            $variantName = 'mp4-360';
            $cmd .= ' -b:v 500k -b:a 128k -filter:v scale="trunc(oh*a/2)*2:360"';
        } else if(isset($params['480'])) {
            $variantName = 'mp4-480';
            $cmd .= ' -b:v 800k -b:a 128k -filter:v scale="trunc(oh*a/2)*2:480"';
        } else if(isset($params['720'])) {
            $variantName = 'mp4-720';
            $cmd .= ' -b:v 1200k -b:a 128k -filter:v scale="trunc(oh*a/2)*2:720"';
        } else if(isset($params['1080'])) {
            $variantName = 'mp4-1080';
            $cmd .= ' -b:v 1500k -b:a 128k -filter:v scale="trunc(oh*a/2)*2:1080"';
        } else if(isset($params['optimized'])) {
            $variantName = 'mp4-optimized';
//            $cmd .= ' -b:v 1000k -b:a 128k';
            $cmd .= ' -crf 30 -b:a 128k';
        }

        $cmd .= ' -movflags +faststart';
        $variant = $file->createVariant($variantName);
        $userfiles = \Fubber\Kernel::init()->config['userfiles'];
        $variant->path = substr($finalPath, strlen($userfiles)+1);
        $tempPath = dirname($finalPath).'/.working-'.basename($finalPath);
        $cmd .= ' '.escapeshellarg($tempPath);
        set_time_limit(0);
        $res = shell_exec($cmd.' 2>&1');
        $db = \Fubber\Db::create();
        if(file_exists($tempPath) && filesize($tempPath)>1000) {
            // Assuming everything was fine
            rename($tempPath, $finalPath);
            $variant->save();
            $db->exec('DELETE FROM efm_file_converts WHERE id=?', [$row->id]);
        } else {
            $db->exec('UPDATE efm_file_converts SET failed_date=NOW() WHERE id=?', [$row->id]);
            unlink($tempPath);
        }
        unlink($sourceFile);

    }
}
