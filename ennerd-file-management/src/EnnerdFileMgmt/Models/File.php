<?php
namespace EnnerdFileMgmt\Models;
use Fubber\Hooks;
use Fubber\Kernel\State;

use function Fubber\trans;

class File extends \Fubber\Table implements \JsonSerializable {
    use \Fubber\Util\OptionsTrait;
    
    public static $_table = 'efm_files';
    public static $_cols = ['id', 'created_by', 'created_date', 'path', 'filename', 'filesize', 'mimetype', 'upload_state', 'locked_until', 'variant_of', 'variant_name'];
    public static $_optionsTable = 'efm_file_options';
    public static $_optionsFK = 'file_id';
    
    public static function meta(State $state) {
        return [
            'names' => [
                'singular' => trans('File'),
                'plural' => trans('Files'),
                ],
            'cols' => [
                'id' => 'id',
                'created_by' => 'created_by',
                'created_date' => 'created_date',
                'path' => [
                    'caption' => trans('Path'),
                    'type' => 'text',
                    'readonly' => true,
                    ],
                'filename' => [
                    'caption' => trans('Filename'),
                    'type' => 'text',
                    ],
                'mimetype' => [
                    'readonly' => true,
                    'extends' => 'mimetype',
                    ],
                'upload_state' => [
                    'caption' => trans('Upload State'),
                    'type' => 'select',
                    'form_options' => [
                        'new' => trans("Not uploaded"), 
                        'uploading' => trans("Upload in progress"),
                        'failed' => trans("Upload failed"),
                        'processing' => trans("Processing variants"),
                        'ready' => trans("Ready"),
                        ],
                    ],
                'locked_until' => [
                    'caption' => trans('Locked Until'),
                    'type' => 'datetime',
                    ],
                'variant_of' => [
                    'caption' => trans('Variant of'),
                    'type' => static::class,
                    ],
                'variant_name' => [
                    'caption' => trans('Variant name'),
                    'type' => 'text',
                    ],
                ]
            ];
    }
    
    public function canEdit(State $state) {
        return $state->access->isSuperAdmin() || ($state->access->isLoggedIn() && $state->userId == $this->id);
    }
    
    public static function canList(State $state) {
        return $state->access->isLoggedIn();
    }
    
    public static function canCreate(State $state) {
        return $state->access->isSuperAdmin();
    }
    
    public function canView(State $state) {
        return true;
    }
    
    public function canDelete(State $state) {
        return $state->access->isSuperAdmin();
    }
    
    public static function create($state=null) {
        $file = new static();
        $file->upload_state = 'new';
        $file->created_date = gmdate('Y-m-d H:i:s');
        if($state) {
            $user = \User::getCurrent($state);
            if($user)
                $file->created_by = $user->id;
        }
        return $file;
    }
    
    public function receiveFile($path) {
        if(!$this->path) throw new Exception("Path must be set.");
        if(!$this->mimetype) {
            $this->mimetype = mime_content_type($path);
            if(!$this->mimetype) throw new Exception(var_export($this->mimetype, true));
        }
        $this->filesize = filesize($path);
        return rename($path, static::getFilesRoot().'/'.$this->path);
        
    }
    
    public function receiveUploadedFile($path) {
        if(!$this->path) throw new Exception("Path must be set.");
        if(!$this->mimetype) {
            $this->mimetype = mime_content_type($path);
            if(!$this->mimetype) throw new Exception(var_export($this->mimetype, true));
        }
        return move_uploaded_file($path, static::getFilesRoot().'/'.$this->path);
    }
    
	public function save(array $properties = null): bool {
        \Fubber\Kernel::$instance->triggerCronJobs(1);
        return parent::save($properties);
    }
    
    public function suggestPath() {
        if(!$this->filename)
            throw new \Fubber\CodingErrorException("Suggest path requires ->filename to be set");
        $root = static::getFilesRoot('/'.date('Y/m/d'));
        $relativeRoot = substr($root, strlen(static::getFilesRoot())+1);
        $bestFilename = \Normalizer::normalize($this->filename);
        $filename = $bestFilename;
        $idx = 1;
        $max = 100;
        // file must not exist in filesystem or in database
        while(file_exists($root.'/'.$filename) || static::all()->where('path','=',$relativeRoot.'/'.$filename)->count()>0) {
            if($max-- < 0) 
                $filename = mt_rand(10000,99999).'-'.$bestFilename;
            else
                $filename = ($idx++).'-'.$bestFilename;
        }
        return ltrim($relativeRoot.'/'.$filename, '/');
    }
    
    public function isImage() {
        if(
            $this->mimetype == 'image/jpeg' || 
            $this->mimetype == 'image/pjpeg' || 
            $this->mimetype == 'image/gif' || 
            $this->mimetype == 'image/png' || 
            $this->mimetype == 'image/webp' ||
            $this->mimetype == 'image/bmp' ||
            $this->mimetype == 'image/x-windows-bmp'
            ) 
            return true;
        return false;
    }
    
    public function isInvalid(): ?array {
        $e = new \Fubber\Util\Errors($this);
        $e->required('created_date');
        $e->datetime('created_date');
        $e->required('upload_state');
        if($this->upload_state != 'new') {
            $e->required('path');
            $e->required('filename');
            $e->required('filesize');
            $e->integer('filesize');
            $e->minVal('filesize', 0);
            $e->required('mimetype');
            $e->oneOf('upload_state', ['uploading','failed','processing','ready']);
            $e->datetime('locked_until');
        }
        return $e->isInvalid();
    }
    
    /**
     * Use getTempFile() instead of this - since files may not even exist locally
     */
    protected function getLocalPath() {
        if(!$this->path) return null;
        return rtrim(\Fubber\Kernel::init()->config['userfiles'], '/').'/'.$this->path;
    }

    public function fileExists() {
        $lp = $this->getLocalPath();
        if(!$lp) return false;
        return file_exists($lp);
    }


    public function getUrl() {
        $url = \Fubber\Kernel::init()->config['userfiles_url'];
        return new \Fubber\Util\Url($url . '/' . $this->path);
    }
    
    public function getVariants() {
        return static::all()->where('variant_of','=',$this->id);
    }
    
    public function getVariant($name) {
        if($this->variant_of) throw new \EnnerdFileMgmt\Exception("getVariant() of variant is bad practice. Variants should be fetched from original file object.");
        return $this->getVariants()->where('variant_name','=',$name)->one();
    }
    
    /**
     * Prepares a File object to be used as a variant of the original file. The object is returned but not saved.
     */
    public function createVariant($name, $extension=null) {
        if(trim($name, 'abcdefghijklmnopqrstuvwxyz0123456789-_')!=='')
            throw new \EnnerdFileMgmt\Exception("Invalid variant name '$name'. Only characters, numbers and - or _.");
        
        $variant = new static();
        $variant->created_by = $this->created_by;
        $variant->created_date = gmdate('Y-m-d H:i:s');
        $root = static::getFilesRoot();
        $path = $root.'/'.$this->path;
        $pi = pathinfo($path);
        $newPath = substr($pi['dirname'], strlen($root)+1);
        
        if($extension===null)
            $extension = $pi['extension'];
        
        $newPath .= '/'.$pi['filename'].'-'.$name.'.'.$extension;
        $variant->path = $newPath;
        $variant->filename = $pi['filename'].'-'.$name.'.'.$extension;
        $variant->upload_state = 'new';
        $variant->variant_of = $this->id;
        $variant->variant_name = $name;
        return $variant;
    }
    
    public function getTempFile() {
        $filename = tempnam(sys_get_temp_dir(), 'fubber-framework-temp-file-');
        copy($this->getLocalPath(), $filename);
        return $filename;
    }
    
    public function getImageUrl($width = null, $height = null, $fit=null, $crop=null, array $args=[]) {
        if(!$this->isImage())
            throw new \Exceptions\CodingErrorException("Not an image");
            
        $rootUrl = \Fubber\Kernel::init()->config['userfiles_url'].'/'.$this->path;
        $ext = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
        if($width === null && $height === null && $fit === null && $crop === null && empty($args))
            return $rootUrl.'.'.$ext;

        $components = [];

        if($width!==null)
            $components[] = 'w='.urlencode($width);

        if($height !== null)
            $components[] = 'h='.urlencode($height);

        if($fit !== null)
            $components[] = 'fit='.urlencode($fit);

        if($crop !== null)
            $components[] = 'crop='.urlencode($crop);

        foreach($args as $k => $v)
            $components[] = urlencode($k).'='.urlencode($v);

        return $rootUrl.'@'.implode("&", $components).'.'.$ext;
    }
    
    public function getThumbnail($width=150, $height=null) {
        $cache = \Fubber\Cache::create();
        if($this->isImage()) {
            if($res = $cache->get($cacheKey = static::class.":thumb:'.$this->id.':$width:$height")) {
                return $res;
            }
            $name = $width;
            if($height!==null)
                $name .= 'x'.$height;
            $variant = $this->getVariant($name);
            if(!$variant) {
                $variant = $this->createVariant($name, 'jpg');
                
                $gd = @imagecreatefromstring(file_get_contents($this->getLocalPath()));
                if($gd===false) return null;
                
                $sw = imagesx($gd);
                $sh = imagesy($gd);
                
                $height = $height === null ? ($sw * $width / $sh) : $height;

                // Don't create bigger images than the original
                if($height > $sh) {
                    $cache->set($cacheKey, $this->getUrl(), 3600);
                    return $this->getUrl();
                }
                $result = imagecreatetruecolor($width, $height);
                
                imagecopyresampled($result, $gd, 0, 0, 0, 0, $width, $height, $sw, $sh);
                if(imagejpeg($result, $variant->getLocalPath(), 80)) {
                    $variant->filesize = filesize($variant->getLocalPath());
                    $variant->mimetype = 'image/jpeg';
                    $variant->upload_state = 'ready';
                    $variant->save();
                    $cache->set($cacheKey, $variant->getUrl(), 3600);
                    return $variant->getUrl();
                }
                throw new \EnnerdFileMgmt\Exception('Unable to create thumbnail');
            }
            $cache->set($cacheKey, $variant->getUrl(), 3600);
            return $variant->getUrl();
        }
        return null;
    }
    
    public function getIcon() {
        $ext = strtolower(pathinfo($this->filename, PATHINFO_EXTENSION));
        $exts = [
            'ppt' => 'file-powerpoint',
            'pptx' => 'file-powerpoint',
            'doc' => 'file-word',
            'docx' => 'file-word',
            'xls' => 'file-excel',
            'xlsx' => 'file-excel',
            'zip' => 'file-archive',
            'gz' => 'file-archive',
            'tar' => 'file-archive',
            'rar' => 'file-archive',
            'lzh' => 'file-archive',
            ];
        if(isset($exts[$ext]))
            return $exts[$ext];
        $parts = explode("/", $this->mimetype);
        switch(strtolower($parts[0])) {
            case 'video' :
                return 'video';
            case 'image' :
                return 'image';
            case 'audio' :
                return 'music';
            case 'text' :
                switch($parts[1]) {
                    case 'html' :
                        return 'code';
                    case 'calendar' :
                        return 'calendar-alt';
                    case 'css' :
                        return 'css3';
                    case 'csv' :
                        return 'list-ul';
                    default :
                        return 'align-left';
                }
            case 'application' :
                switch($parts[1]) {
                    case 'pdf' :
                        return 'file-pdf';
                    case 'xml' :
                    case 'javascript' :
                    case 'ecmascript' :
                        return 'code';
                }
                break;
            case 'font' :
                return 'font';
        }
        return 'question';
    }
    
    public function delete(): bool {
        foreach($this->getVariants() as $variant) {
            $variant->delete();
        }
        
        $path = \Fubber\Kernel::init()->config['privfiles'].'/tmp/uploads/'.$this->id;
        if(is_dir($path)) {
            $chunks = glob($path.'/*');
            foreach($chunks as $chunk)
                unlink($chunk);
            rmdir($path);
        }

        $db = \Fubber\Db::create();      
        $db->exec('DELETE FROM efm_file_chunks WHERE file_id=?', [$this->id]);

        $path = $this->getLocalPath();
        $res = parent::delete();
        if($res && $path) {
            if(static::all()->where('path','=',$this->path)->count()==0) {
                if(file_exists($path))
                    unlink($path);
            }
        }
        return $res;
    }
    
    protected static function getFilesRoot($suffix = '') {
        $path = \Fubber\Kernel::init()->config['userfiles'].rtrim($suffix, '/');
        if(!is_dir($path))
            mkdir($path, 0777, true);
        if(!is_dir($path))
            throw new \Exception("Unable to create path $path");
        return $path;
    }
    
    public function jsonSerialize() {
        // Videos should be converted before
        if($this->mimetype=='video/mp4' && !$this->variant_name) {
            $url = $this->getUrl().'@optimized.mp4';
        } else {
            $url = $this->getUrl();
        }
        return [
            'id' => (int) $this->id,
            'created_by' => (int) $this->created_by,
            'created_date' => gmdate('c', strtotime($this->created_date)),
            'path' => $this->path,
            'filename' => $this->filename,
            'filesize' => $this->filesize,
            'mimetype' => $this->mimetype,
            '_string' => $this->filename,
            '_url' => $url,
            ];
    }
    
    public static function loadByPath($path) {
        if($path[0]=='/') {
            $userFiles = \Fubber\Kernel::init()->config['userfiles'];
            if(substr($path, 0, strlen($userFiles)) == $userFiles) {
                return static::all()->where('path','=',substr($path, strlen($userFiles)+1))->one();
            }
        }
        
        return static::all()->where('path','=',$path)->one();
    }
}
