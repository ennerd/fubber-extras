<?php
namespace EnnerdFileMgmt;
use Imagick;

class ImageProcessor {
    protected $_image;
    protected $_exifOrientation;
    protected $_imagick;
    protected $_profiles = [];
    protected $_path;
    
    public function __construct($path, $exifOrientation=null) {
        $this->_path = $path;
        
        /**
         * Extract ICC color profile, if we have imagick
         */
        if(class_exists(\Imagick::class)) {
            try {
                $im = new \Imagick($this->_path);
                $this->_profiles = $im->getImageProfiles('*', true);
            } catch (\Exception $e) {
                $this->_profiles = [];
            }
        }
        if($exifOrientation !== null) {
            static::assertInt('or', $exifOrientation);
            static::assertBetween('or', $exifOrientation, 1, 8);
        }
        $this->_exifOrientation = $exifOrientation;
    }
    
    public function writeProfilesTo($filename) {
        if(empty($this->_profiles)) return $this;
        if(class_exists(\Imagick::class)) {
            try {
                $im = new \Imagick($filename);
                foreach($this->_profiles as $name => $value) {
                    $im->profileImage($name, $value);
                }
                $im->writeImage($filename);
            } catch (\Exception $e) {}
        }
        return $this;
    }
    
    protected function getImagick() {
        if($this->_imagick)
            return $this->_imagick;
            
        static::assertImage();
        if(!class_exists(\Imagick::class)) return false;
        $tempFile = tempnam(sys_get_temp_dir(), 'fubber-file-processor');
        imagepng($this->image(), $tempFile);
        $this->_imagick = new \Imagick($tempFile);
        unlink($tempFile);
        $this->_image = [ "Should not be used" ];
        return $this->_imagick;
    }
    
    protected function commitImagick($newIm=false) {
        if(!$this->_imagick)
            return false;
        $tempFile = tempnam(sys_get_temp_dir(), 'fubber-file-processor');
        if($newIm)
            $this->_imagick = $newIm;
        $this->_imagick->writeImage('png:'.$tempFile);
        $this->_imagick = null;
        $this->_image = imagecreatefrompng($tempFile);
        return $this;
    }
    
    public function blur($radius) {
        static::assertInt('blur', $radius);
        static::assertBetween('blur', $radius, 1, 2000);
        $radius /= 3.333;
        $im = $this->getImagick();
        if(!$im) return $this;
        $im->blurImage($radius, $radius / 3);
        $this->commitImagick();
        return $this;
    }
    
    public function sepia($amount) {
        static::assertInt('sepia', $amount);
        static::assertBetween('sepia', $amount, 0, 100);
        if($amount < 1) return $this;
        $origIM = $this->getImagick();
        if(!$origIM) return $this;
        $amount = $amount / 100;
        $negAmount = 1-$amount;
        
        $im = $origIM->clone();
        $im->modulateImage(100, 0, 0);
        $im->brightnessContrastImage(-5,2);
        //$im->modulateImage(100, -50, 50);
/*
        $im = $origIM->fxImage('lightness', Imagick::CHANNEL_RED|Imagick::CHANNEL_GREEN|Imagick::CHANNEL_BLUE);
        $im->brightnessContrastImage(20,-20);
*/
        $tint = new \ImagickPixel("rgb(115,85,30)");
        $opacity = new \ImagickPixel("rgb(255, 255, 255, 0)");
        $im->tintImage($tint, $opacity, false);
/*
        //$origIM = $origIM->fxImage('0.1', \Imagick::CHANNEL_ALPHA);
        $im->transformImageColorspace(imagick::COLORSPACE_RGB);
  */
        $origIM->setImageOpacity($negAmount);
        $im->compositeImage($origIM, Imagick::COMPOSITE_OVER, 0, 0);

        $this->commitImagick($im);
        return $this;
    }
    
    public function pixellate($size) {
        static::assertInt('px', $size);
        static::assertBetween('px', $size, 0, 100);
        if($size == 0) return $this;
        
        $tw = 1 + (int) ($this->imageWidth() / $size);
        $th = 1 + (int) ($this->imageHeight() / $size);
        
        $tempImage = imagecreatetruecolor($tw, $th);
        imagecopyresampled($tempImage, $this->image(), 0, 0, 0, 0, $tw, $th, $this->imageWidth(), $this->imageHeight());
        imagecopyresized($this->image(), $tempImage, 0, 0, 0, 0, $tw * $size, $th * $size, $tw, $th);
        return $this;
    }
    
    public function assertImage() {
        static $isImage = null;
        if($isImage !== null) return $isImage;
        
        $res = imagecreatefromstring(file_get_contents($this->_path));
        if(!$res)
            throw new \Exceptions\NotFoundException('Not an image');
    }
    
    public static function assertMaxSize($value) {
        if($value > 3000) throw new \Exceptions\NotFoundException('Does not accept output dimensions above 3000. '.$value.' requested.');
    }
    
    public static function assertBetween($name, $value, $min, $max) {
        if($value < $min || $value > $max)
            throw new \Exceptions\NotFoundException('Value for "'.$name.'" must be between '.$min.' and '.$max.'. (Was '.$value.')');
    }

    public static function assertIntOr($name, $value, array $alternatives) {
        if(in_array($value, $alternatives)) return;
        try {
            static::assertInt($name, $value);
        } catch (\Exceptions\NotFoundException $e) {
            throw new \Exceptions\NotFoundException('Parameter "'.$name.'" must be integer or one of: '.implode(", ", $alternatives).'. (Was '.$value.')');
        }
    }
    
    public static function assertOneOf($name, $value, array $alternatives) {
        if(!in_array($value, $alternatives)) throw new \Exceptions\NotFoundException('Parameter "'.$name.'" must be one of: '.implode(", ", $alternatives).'. (Was '.$value.')');
    }
    
    public static function assertInt($name, $value) {
        if(intval($value).'' != $value) throw new \Exceptions\NotFoundException('Parameter "'.$name.'" is not an integer. (Was '.$value.')');
    }
    
    public static function assertFloat($name, $value) {
        if(intval($value).'' != $value && floatval($value).'' != $value && floatval($value).'' != '0'.$value)
            throw new \Exceptions\NotFoundException('Parameter "'.$name.'" is not a float. (Was '.$value.')');
    }
    
    public static function assertIntOrRatio($name, $value) {
        if(intval($value).'' == $value)
            return;
        if($value >= 0 && $value <= 1) {
            if(floatval($value).'' == $value || floatval($value).'' == '0'.$value)
                return;
        }
        throw new \Exceptions\NotFoundException('Parameter "'.$name.'" must be an integer or a float value between 0 and 1. (Was '.$value.')');
    }
    
    public static function assertPositive($name, $value) {
        if(floatval($value)<0)
            throw new \Exceptions\NotFoundException('Parameter "'.$name.'" must be bigger than 0.');
    }
    
    /**
     * Rotates contents, and keeps size of image
     */
    public function rotate($angle) {
        static::assertFloat('rot', $angle);
        $width = $this->imageWidth();
        $height = $this->imageHeight();
        $rect = static::cropRectForRectSize($width, $height, deg2rad($angle));
        $this->_image = imagerotate($this->image(), $angle, 0);
        $this->rect((int)$rect[0], (int)$rect[1], (int)$rect[2], (int)$rect[3]);
        $this->resizeTo($width, $height, 'crop');
        return $this;
    }
    
    public function flipHorizontal() {
        imageflip($this->image(), IMG_FLIP_HORIZONTAL);
        return $this;
    }
    
    public function flipVertical() {
        imageflip($this->image(), IMG_FLIP_VERTICAL);
        return $this;
    }
    
    public function rect($x, $y, $w, $h) {
        $this->assertImage();
        static::assertIntOr('x', $x, ['left','center','right']);
        static::assertIntOr('y', $y, ['top','middle','bottom']);
        static::assertInt('w', $w);
        static::assertInt('h', $h);
        static::assertPositive('w', $w);
        static::assertPositive('h', $h);

        if($w > $this->imageWidth()) $w = $this->imageWidth();
        if($h > $this->imageHeight()) $w = $this->imageHeight();

        if($x == 'left')
            $x = 0;
        else if($x == 'center')
            $x = floor(($this->imageWidth() - $w) / 2);
        else if($x == 'right')
            $x = $this->imageWidth() - $w;
        static::assertPositive('x', $x);

        if($y == 'top')
            $y = 0;
        else if($y == 'middle')
            $y = floor(($this->imageHeight() - $h) / 2);
        else if($y == 'bottom')
            $y = $this->imageWidth() - $w;
        static::assertPositive('y', $y);
            
        $this->_image = imagecrop($this->image(), ['x' => $x, 'y' => $y, 'width' => $w, 'height' => $h]);
        return $this;
    }
    
    public function resizeTo($width=null, $height=null, $fit=null, $crop=null, array $args=[]) {
        /**
         * Handles the case where one of width or height is not set. Also handles values between 0 and 1.
         */
        if($width === null && $height !== null) { // only height
            static::assertIntOrRatio('height', $height);
            if($height <= 1 && $height > 0)
                $height = ($this->imageWidth() * $height);
            $width = ($height * $this->aspect());
        } else if($width !== null && $height === null) { // only width
            static::assertIntOrRatio('width', $width);
            if($width <= 1 && $width > 0)
                $width = floor($this->imageWidth() * $width);
            $height = ($width / $this->aspect());
        } else if($height !== null && $width !== null) { // both height and width
            static::assertIntOrRatio('height', $height);
            static::assertIntOrRatio('width', $width);
            if($height <= 1 && $height > 0)
                $height = ($this->imageHeight() * $height);
            if($width <= 1 && $width > 0)
                $width = ($this->imageWidth() * $width);
        } else {
            $width = $this->imageWidth();
            $height = $this->imageHeight();
        }
        
        if(isset($args['max-h'])) {
            static::assertInt('max-h', $args['max-h']);
            if($height > $args['max-h'])
                $height = $args['max-h'];
        }
        if(isset($args['min-h'])) {
            static::assertInt('min-h', $args['min-h']);
            if($height < $args['min-h'])
                $height = $args['min-h'];
        }

        if(isset($args['max-w'])) {
            static::assertInt('max-w', $args['max-w']);
            if($width > $args['max-w'])
                $width = $args['max-w'];
        }
        if(isset($args['min-w'])) {
            static::assertInt('min-w', $args['min-w']);
            if($width < $args['min-w'])
                $width = $args['min-w'];
        }

        // default for imgix.com is clip
        if($fit === null) {
            $fit = 'clip';
        }
        // if crop is set, then fit must be = crop, and values top,bottom,left,right is allowed.
        if($crop !== null) {
            if($fit !== 'crop')
                throw new \Exceptions\NotFoundException('crop does not work unless fit=crop.');
            foreach(explode(",", $crop) as $c) {
                static::assertOneOf('crop', $c, ['top','bottom','left','right']);
            }
        }
        
        // support most of the modes that imgix.com supports.
        static::assertOneOf('fit', $fit, ['clip', 'crop', 'max', 'scale', 'min', 'fill', 'fillmax']);
        static::assertPositive('width', $width);
        static::assertPositive('height', $height);
        
        $sx = 0;
        $sy = 0;
        $sw = $this->imageWidth();
        $sh = $this->imageHeight();
        
        // If user forgets to set bg, then we fallback to clip - as does imgix.com
        if($fit == 'fill' && !isset($args['bg'])) 
            $fit = 'clip';
        if($fit == 'fillmax' && !isset($args['bg'])) 
            $fit = 'max';
        
        switch($fit) {
            case 'scale' :
                // Do nothing with aspect or rectangle
                break;
            case 'clip' :
                list($width, $height) = $this->fitClip($width, $height);
                break;
            case 'max' :
                list($width, $height) = $this->fitMax($width, $height);
                break;
            case 'crop' :
                list($width, $height, $sx, $sy, $sw, $sh) = $this->fitCrop($width, $height, $crop);
                break;
            case 'min' :
                list($width, $height, $sx, $sy, $sw, $sh) = $this->fitMin($width, $height);
                break;
            case 'fill' :
            case 'fillmax' :
                if($fit=='fill')
                    list($newWidth, $newHeight) = $this->fitClip($width, $height);
                else
                    list($newWidth, $newHeight) = $this->fitMax($width, $height);
                $newImage = imagecreatetruecolor($width, $height);
                imagesavealpha($newImage, true);
                $bg = static::allocateColor($newImage, $args['bg']);
                imagealphablending($newImage, false);
                imagefilledrectangle($newImage, 0, 0, $width, $height, $bg);
                imagealphablending($newImage, true);
                if($height > $newHeight) {
                    // fill top and bottom
                    imagecopyresampled($newImage, $this->image(), 0, $height / 2 - $newHeight / 2, $sx, $sy, $newWidth, $newHeight, $sw, $sh);
                    $this->_image = $newImage;
                    return $this;
                } else {
                    // fill left and right
                    imagecopyresampled($newImage, $this->image(), $width / 2 - $newWidth / 2, 0, $sx, $sy, $newWidth, $newHeight, $sw, $sh);
                    $this->_image = $newImage;
                    return $this;
                }
                break;
        }
        $newImage = imagecreatetruecolor($width, $height);
        $transparency = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
        imagefill($newImage, 0, 0, $transparency);
        imagesavealpha($newImage, true);
        imagecopyresampled($newImage, $this->image(), 0, 0, $sx, $sy, $width, $height, $sw, $sh);
//header("Content-Type: image/png"); imagepng($newImage);die();
        $this->_image = $newImage;
        return $this;
    }
    
    public function brightness($factor) {
        static::assertInt('bri', $factor);
        static::assertBetween('bri', $factor, -100, 100);
        $factor = (int) $factor;
        if($factor == 0) return $this;
        $factor = $factor * 255 / 100;
        imagefilter($this->image(), IMG_FILTER_BRIGHTNESS, $factor);
        return $this;
    }
    
    public function contrast($factor) {
        static::assertInt('bri', $factor);
        static::assertBetween('bri', $factor, -100, 100);
        $factor = (int) $factor;
        if($factor == 0) return $this;
        $factor = $factor * 255 / 100;
        imagefilter($this->image(), IMG_FILTER_CONTRAST, $factor);
        return $this;
    }
    
    public function saturation($percentage) {
        throw new \Exceptions\NotFoundException('Saturation not supported');
        static::assertInt('bri', $percentage);
        static::assertBetween('sat', $percentage, -100, 100);
        $width = $this->imageWidth();
        $height = $this->imageHeight();
        $image = $this->image();
    
        for($x = 0; $x < $width; $x++) {
            for($y = 0; $y < $height; $y++) {
                $rgb = imagecolorat($image, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;            
                //var_dump($r, $g, $b);
                $alpha = ($rgb & 0x7F000000) >> 24;
                list($h, $s, $l) = static::rgb2hsl($r, $g, $b);         
                //var_dump($h, $s, $l);die();
                $s = $s * (100 + $percentage) / 100;
                if($s > 1) $s = 1;
                list($r, $g, $b) = static::hsl2rgb($h, $s, $l);            
                imagesetpixel($image, $x, $y, imagecolorallocatealpha($image, $r, $g, $b, $alpha));
            }
        }
        return $this;
    }
    
    public function invert() {
        imagefilter($this->image(), IMG_FILTER_NEGATE);
        return $this;
    } 
    
    public function imageWidth() {
        return imagesx($this->image());
    }

    public function imageHeight() {
        return imagesy($this->image());
    }
    
    public function aspect() {
        return $this->imageWidth() / $this->imageHeight();
    }
    
    public function image() {
        if($this->_imagick)
            throw new \Exception("Can't access image while still in imagick mode");
        if($this->_image)
            return $this->_image;
        $this->assertImage();
        
        $this->_image = imagecreatefromstring(file_get_contents($this->_path));
        imagesavealpha($this->_image, true);
        imagealphablending($this->_image, true);
        $tc = imagecreatetruecolor($this->imageWidth(), $this->imageHeight());
        $transparency = imagecolorallocatealpha($tc, 0, 0, 0, 127);
        imagefill($tc, 0, 0, $transparency);
        imagealphablending($tc, true);
        imagesavealpha($tc, true);
        imagecopy($tc, $this->_image, 0, 0, 0, 0, $this->imageWidth(), $this->imageHeight());
        $this->_image = $tc;
        $tc = null;

        $orientation = null;
        if($this->_exifOrientation)
            $orientation = $this->_exifOrientation;
        else if(function_exists('exif_read_data')) {
            $exif = @exif_read_data($this->_path);
            if($exif && isset($exif['Orientation']))
                $orientation = $exif['Orientation'];
        }

        if($orientation !== null) {
            if($orientation === 1) {
                // do nothing
                
            } else if($orientation === 2) {
                // flip horizontal
                imageflip($this->_image, IMG_FLIP_HORIZONTAL);
                
            } else if($orientation === 3) {
                // rotate 180 degrees
                //$this->_image = imagerotate($this->_image, 180, 0);
                imageflip($this->_image, IMG_FLIP_HORIZONTAL);
                imageflip($this->_image, IMG_FLIP_VERTICAL);
                
            } else if($orientation === 4) {
                // rotate 180 degrees then flip
                //$this->_image = imagerotate($this->_image, 180, 0);
                //imageflip($this->_image, IMG_FLIP_HORIZONTAL);
                imageflip($this->_image, IMG_FLIP_VERTICAL);
                
            } else if($orientation === 5) {
                // rotate -90 degrees then flip
                $this->_image = imagerotate($this->_image, -90, 0);
                imageflip($this->_image, IMG_FLIP_HORIZONTAL);
                
            } else if($orientation === 6) {
                $this->_image = imagerotate($this->_image, -90, 0);
                
            } else if($orientation === 7) {
                $this->_image = imagerotate($this->_image, 90, 0);
                imageflip($this->_image, IMG_FLIP_HORIZONTAL);
                
            } else if($orientation === 8) {
                $this->_image = imagerotate($this->_image, 90, 0);
                
            }
        }
        
        return $this->_image;
    }

    /**
     * Checked OK
     * 
     * Preserves aspect, scales up or down to fit inside $width, $height
     */
    public function fitClip($width, $height) {
        // First make width exactly match
        $a = $this->aspect();
        
        $na = $width / $height;
        
        if($a < $na) {
            $newHeight = $height;
            $newWidth = $height * $this->aspect();
            return [$newWidth, $newHeight];
        } else {
            $newWidth = $width;
            $newHeight = $width / $this->aspect();
            return [$newWidth, $newHeight];
        }
    }
    
    /**
     * Checked OK
     * 
     * Preserves aspect, scales down to fit inside $width, $height.
     * Does not scale up
     */
    public function fitMax($width, $height) {
        // Same as fitClip, but does not scale up the image
        
        $a = $this->aspect();
        $newWidth = $this->imageWidth();
        $newHeight = $this->imageHeight();
        
        if($newWidth > $width) {
            $newWidth = $width;
            $newHeight = $newWidth / $a;
        }
        if($newHeight > $height) {
            $newHeight = $height;
            $newWidth = $newHeight * $a;
        }
        return [$newWidth, $newHeight];
    }
    
    public function fitMin($width, $height, $crop='center') {
        list($newWidth, $newHeight, $newSX, $newSY, $newSW, $newSH) = $this->fitCrop($width, $height, $crop);
        $a = $newWidth / $newHeight;
        if($newWidth > $this->imageWidth()) {
            $newWidth = $this->imageWidth();
            $newHeight = $this->imageWidth() / $a;
        }
        if($newHeight > $this->imageHeight()) {
            $newWidth = $this->imageHeight();
            $newHeight = $newWidth * $a;
        }
        return [$newWidth, $newHeight, $newSX, $newSY, $newSW, $newSH];
    }
    
    /*
    public function fitMin($width, $height) {
        $a = $width / $height;
        $newWidth = $this->imageWidth();
        $newHeight = $this->imageHeight();
        if($width > $newWidth) {
            $width = $newWidth;
            $height = $width / $a;
        }
        if($height > $newHeight) {
            $height = $newHeight;
            $width = $newHeight * $a;
        }
        return [$height, $width];
    }
    */
    
    public function fitCrop($width, $height, $crop='center') {
        $crops = explode(",", $crop);
        $crop = [];
        foreach($crops as $c)
            $crop[$c] = true;
        $a = $this->aspect();
        $newA = $width / $height;
        if($a < $newA) {
            if(!empty($crop['top'])) {
                $srcWidth = $this->imageWidth();
                $srcHeight = $srcWidth / $newA;
                // Cut bottom
                return [$width, $height, 0, 0, $srcWidth, $srcHeight];
            } else if(!empty($crop['bottom'])) {
                $srcWidth = $this->imageWidth();
                $srcHeight = $srcWidth / $newA;
                // Cut top
                return [$width, $height, 0, $this->imageHeight() - $srcHeight, $srcWidth, $srcHeight];
            } else {
                $srcWidth = $this->imageWidth();
                $srcHeight = $srcWidth / $newA;
                // Cut top and bottom
                return [$width, $height, 0, $this->imageHeight() / 2 - $srcHeight / 2, $srcWidth, $srcHeight];
            }
            die("CUT TOP");
        } else if($a > $newA) {
            if(!empty($crop['left'])) {
                $srcHeight = $this->imageHeight();
                $srcWidth = $srcHeight * $newA;
                // Cut right
                return [$width, $height, 0, 0, $srcWidth, $srcHeight];
            } else if(!empty($crop['right'])) {
                $srcHeight = $this->imageHeight();
                $srcWidth = $srcHeight * $newA;
                // Cut left
                return [$width, $height, $this->imageWidth() - $srcWidth, 0, $srcWidth, $srcHeight];
            } else {
                $srcHeight = $this->imageHeight();
                $srcWidth = $srcHeight * $newA;
                // Cut left and right
                return [$width, $height, $this->imageWidth() / 2 - $srcWidth / 2, 0, $srcWidth, $srcHeight];
            }
            // Cut left and right
            die("CUT SIDE");
        } else {
            // Cut nothing
            return [$width, $height, 0, 0, $this->imageWidth(), $this->imageHeight()];
        }
        
        
        return [$newWidth, $newHeight, $newSX, $newSY, $newSW, $newSH];
    }
    
    protected static function allocateColor($image, $c) {
        $l = strlen($c);
        if($l != 3 && $l != 4 && $l != 6 && $l != 8)
            throw new \Exceptions\NotFoundException('Colors must be specified in hexadecimal (RGB, ARGB, RRGGBB, AARRGGBB).');
        if(trim($c, '0123456789abcdef') != '')
            throw new \Exceptions\NotFoundException('Colors can only contain hexadecimal values.');

        if($l < 5) {
            $cc = '';
            for($i = 0; $i < strlen($c); $i++)
                $cc .= $c[$i].$c[$i];
            $c = $cc;
        }

        if(strlen($c) == 8) {
            $alpha = 127 - floor(hexdec(substr($c, 0, 2)) / 2);
            $c = substr($c, 2);
        } else {
            $alpha = 0;
        }
        
        $red = hexdec(substr($c, 0, 2));
        $green = hexdec(substr($c, 2, 2));
        $blue = hexdec(substr($c, 4, 2));
        
        return imagecolorallocatealpha($image, $red, $green, $blue, $alpha);
    }
    
    public function __destruct() {
        
    }

    public static function cropRectForRectSize($width, $height, $radians) {
        $rectSize = [$width, $height];
        $quadrant = (int) floor($radians / (pi() / 2)) & 3;
        $sign_alpha = (($quadrant & 1) == 0) ? $radians : pi() - $radians;
        $alpha = fmod((fmod($sign_alpha, pi()) + pi()), pi());
        
        $bb = [$rectSize[0] * cos($alpha) + $rectSize[1] * sin($alpha), $rectSize[0] * sin($alpha) + $rectSize[1] * cos($alpha)];

        $gamma = $rectSize[0] < $rectSize[1] ? atan2($bb[0], $bb[1]) : atan2($bb[1], $bb[0]);
        $delta = pi() - $alpha - $gamma;
        $length = $rectSize[0] < $rectSize[1] ? $rectSize[1] : $rectSize[0];
        $d = $length * cos($alpha);
        $a = $d * sin($alpha) / sin($delta);
        $y = $a * cos($gamma);
        $x = $y * tan($gamma);
        return [$x, $y, $bb[0] - 2 * $x, $bb[1] - 2 * $y];
    }
}