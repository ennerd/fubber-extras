<?php


use Phinx\Migration\AbstractMigration;

class AddProcessingFileState extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('efm_files')
            ->changeColumn('upload_state', 'enum', ['values' => [ 
                'new',                                                          // Uploading has not started
                'uploading',                                                    // We are receiving chunks
                'failed',                                                       // Upload has failed
                'processing',                                                   // File is ready, but processing needs to be done
                'ready'                                                         // File is ready to be used
                ], 'default' => 'new'])
            ->update();
    }
}
