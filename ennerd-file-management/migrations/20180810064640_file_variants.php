<?php


use Phinx\Migration\AbstractMigration;

class FileVariants extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('efm_files')
            ->addColumn('variant_of', 'integer', ['null' => true])
            ->addColumn('variant_name', 'string', ['limit' => 20, 'null' => true])
            ->addIndex(['variant_of', 'variant_name'], ['unique' => true])
            ->addForeignKey('variant_of', 'efm_files', 'id', ['update' => 'CASCADE', 'delete' => 'RESTRICT'])
            ->update();
    }
}
