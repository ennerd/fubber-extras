<?php


use Phinx\Migration\AbstractMigration;

class EfmFilesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('efm_files')
            ->addColumn('created_by', 'integer', ['null' => true])              // Who uploaded this file
            ->addColumn('created_date', 'datetime')                             // When was it uploaded
            ->addColumn('path', 'string', ['limit' => 200, 'null' => true])     // Location in filesystem for file
            ->addColumn('filename', 'string', ['limit' => 200, 'null' => true]) // The uploaded file name
            ->addColumn('filesize', 'integer', ['null' => true])                // The file size
            ->addColumn('mimetype', 'string', ['limit' => 100,'null' => true])  // Standard mime type of file
            ->addColumn('upload_state', 'enum', ['values' => [ 
                'new',                                                          // Uploading has not started
                'uploading',                                                    // We are receiving chunks
                'failed',                                                       // Upload has failed
                'ready'                                                         // File is ready to be used
                ], 'default' => 'new'])
            ->addColumn('locked_until', 'datetime', ['null' => true])           // Lock the file so that other processes don't modify or work with it at the same time
            ->addIndex(['created_by', 'created_date'])                        // For foreign key constraints
            ->addIndex(['created_date'])                                       // For sorting
            ->addIndex(['mimetype'])
            ->addForeignKey('created_by', 'users', 'id', ['delete' => 'SET_NULL', 'update' => 'CASCADE'])
            ->create();
        $this->table('efm_file_chunks')
            ->addColumn('file_id', 'integer')
            ->addColumn('created_date', 'datetime')
            ->addColumn('range_start', 'integer')
            ->addColumn('range_end', 'integer')
            ->addIndex(['file_id','range_start','range_end'])
            ->addForeignKey('file_id','efm_files','id',['delete' => 'RESTRICT', 'update' => 'CASCADE'])
            ->create();
        $this->table('efm_file_options')
            ->addColumn('file_id', 'integer')
            ->addColumn('name', 'string', ['limit' => 200])
            ->addColumn('value', 'blob', ['limit' => 65535])
            ->addIndex(['file_id', 'name'])
            ->addForeignKey('file_id','efm_files','id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->create();
    }
}
