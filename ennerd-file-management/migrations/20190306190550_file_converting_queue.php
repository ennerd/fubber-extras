<?php


use Phinx\Migration\AbstractMigration;

class FileConvertingQueue extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('efm_file_converts')
            ->addColumn('efm_file_id', 'integer')
            ->addColumn('final_path', 'string', ['limit' => 200])
            ->addColumn('created_date', 'datetime')
            ->addColumn('started_date', 'datetime', ['null' => true])
            ->addColumn('failed_date', 'datetime', ['null' => true])
            ->addColumn('params', 'blob')
            ->addIndex(['efm_file_id', 'final_path'], ['unique' => true])
            ->addIndex(['created_date'])
            ->addForeignKey('efm_file_id', 'efm_files', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->create();
    }
}
