<?php


use Phinx\Migration\AbstractMigration;

class ScaledFilesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('efm_files_scaled')
            ->addColumn('file_id', 'integer', ['null' => true])
            ->addColumn('path', 'string', ['limit' => 250])
            ->addColumn('delete_time', 'datetime')
            ->addColumn('delete_count', 'integer', ['default' => 0])
            ->addColumn('is_deleted', 'enum', ['values' => ['0','1'], 'default' => '0'])
            ->addIndex(['file_id'])
            ->addIndex(['is_deleted', 'delete_time'])
            ->addForeignKey('file_id', 'efm_files', 'id', ['update' => 'CASCADE', 'delete' => 'SET_NULL'])
            ->create();
    }
}
