<?php
namespace Fubber\JS;

use Fubber\Kernel;
use Fubber\Kernel\AbstractController;
use Fubber\Kernel\Environment\Environment;
use Fubber\Kernel\Filesystem\Filesystem;
use Fubber\NotFoundException;

/**
 * Build and transpile various assets such as .sass, .ts and other resources.
 * 
 * @package Fubber\JS
 */
class JSController extends AbstractController {

    /**
     * Expects information about where files can be accessed and stored.
     * 
     * @param string $documentRoot For example "/var/www/html"
     * @param string $localPath For example "/var/www/html/files".
     * @param string $publicUrl For example "https://www.mysite.com/files". 
     * @param string $prefix Prefic to paths inside `$publicUrl` and `$localPath`; for example 'dist'
     */
    public function __construct(
        private readonly string $documentRoot,
        private readonly string $localPath,
        private readonly string $publicUrl,
        private readonly string $prefix
    ) {}

    private function getEndpoint(): string {
        $url = \parse_url($this->publicUrl);
        return \rtrim($url['path'], '/') . '/' . $this->prefix;
    }

    public function init(Kernel $kernel): void {
        $endpoint = $this->getEndpoint();
        $kernel->router->get($endpoint . '{path:.+}', $this->endpointHandler(...));
    }

    public function endpointHandler(string $path) {
        $sourceFile = $this->documentRoot . $path;
        $targetExtension = \pathinfo($sourceFile, \PATHINFO_EXTENSION);
        $sourceDir = \dirname($sourceFile);
        $destFile = $this->localPath . '/' . $this->prefix . $path;
        $destDir = \dirname($destFile);

        $sourceBase = \substr($sourceFile, 0, -\strlen($targetExtension) - 1);

        if (\is_file($sourceFile)) {
            die("source $sourceFile already exists");
        }
        if (\is_file($sourceBase.'.ts')) {
            $this->buildTS($sourceBase.'.ts', $destFile);
            die("TS source");
        }

        die(<<<INFO
            <pre>
            sourceFile                  $sourceFile
            targetExtension             $targetExtension
            sourceDir                   $sourceDir
            destFile                    $destFile
            destDir                     $destDir
            sourceBase                  $sourceBase
            INFO);
    }

    private function buildTS(string $sourceFile, string $destFile) {
        $cmd = $this->getNodeBinCmd('tsc',
            '--allowJs', 'true', 
            '--allowSyntheticDefaultImports', 'true',
            '-m', 'amd',
            '-t', 'es3',
            '--forceConsistentCasingInFileNames', 'true',
            '--noResolve', 'true',
            '--outDir', \dirname($destFile),
            '--paths', \json_encode(['jquery' => 'node_modules/jquery/dist/jquery']),
            $sourceFile);

        echo "CMD: $cmd";
        die();
    }

    private function getNodeBinCmd(string $command, string ...$args): string {
        $binary = \dirname(__DIR__) . '/node_modules/.bin/' . $command;
        $cmd = $this->getNodePath() . ' ' . \escapeshellarg($binary);
        foreach ($args as $arg) {
            $cmd .= ' ' . \escapeshellarg($arg);
        }
        return $cmd;
    }

    private function getNodePath(): string {
        if (\is_executable($path = '/usr/bin/node')) {
            return $path;
        } elseif (\is_executable($path = '/usr/local/bin/node')) {
            return $path;
        } elseif (\is_executable($path = 'node')) {
            return $path;
        } elseif (\is_executable($path = Kernel::getComposerPath() . '/vendor/bin/node')) {
            return $path;
        } else {
            throw new LogicException("Could not find `node` executable");
        }
    }
}