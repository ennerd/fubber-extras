<?php
namespace Fubber\CommonJS;

use Fubber\Kernel;
use Fubber\Kernel\AbstractController;
use Fubber\Kernel\Environment\Environment;
use Fubber\Kernel\Filesystem\Filesystem;
use Fubber\Kernel\Http\Http;
use Fubber\Kernel\Router\Route;
use Fubber\LogicException;
use Fubber\NotFoundException;
use Fubber\Util\Url;
use Psr\Http\Message\ResponseInterface;

class CommonJSController extends AbstractController {

    const UNKNOWN_MODULE = 0;
    const AMD_MODULE = 1;
    const COMMONJS_MODULE = 2;
    const UMD_MODULE = 3;
    const ES_MODULE = 4;

    public function __construct(
        protected Filesystem $fs,
        protected Environment $env) {}

        public function init(Kernel $kernel): void {
            return; // deactivated

            $uploadRoute = $this->getDynRoute();
    
            // Routes for loading dynamically translated assets
            $kernel->router->add(
                new Route('GET', $uploadRoute.'/{component:.+}.ts.js', $this->get_ts_file(...)),
                new Route('GET', $uploadRoute.'/amd{path:[^@]+}@{component:.+}', $this->get_cjs_file(...)),
                new Route('GET', $uploadRoute.'/mjs{path:[^@]+}@{component:.+}', $this->get_mjs_file(...)),
            );
    
            // Routes for loading configuration data
            $kernel->router->add(
                //new Route('GET', '{path:.+}/node_modules.amd/{modulePath:.+}', $this->get_node_modules(...)),
                new Route('GET', '{path:.+}/node_modules.amd.js', $this->get_node_modules_amd(...)),
            );
        }
    
    public function get_ts_file() {
        die("TS");
    }


    private function getNodeCommand(): string {
        if (\is_executable($path = '/usr/bin/node')) {
            return $path;
        } elseif (\is_executable($path = '/usr/local/bin/node')) {
            return $path;
        } elseif (\is_executable($path = 'node')) {
            return $path;
        } elseif (\is_executable($path = Kernel::getComposerPath() . '/vendor/bin/node')) {
            return $path;
        } else {
            throw new LogicException("Could not find `node` executable");
        }
    }

    private function getDynRoot(): string {
        return $this->env->uploadRoot . '/ennerd-commonjs';
    }

    private function getDynUrl(): string {
        return $this->env->uploadUrl . '/ennerd-commonjs';
    }

    private function getDynRoute(): string {
        return \substr($this->getDynRoot(), \strlen($this->env->baseRoot));
    }

    protected function get_cjs_file(string $path, string $component, Http $http) {
        $basePath = $this->env->baseRoot . $path;
        if (!$this->fs->is_dir([$basePath, 'node_modules'])) {
            throw new NotFoundException("Not found");
        }
        $sourceFilePath = $basePath . $component;
        if (!\str_contains($component, '/node_modules/')) {
            throw new NotFoundException("Not found (node_modules)");
        }
        if (!$this->fs->is_file($sourceFilePath)) {
            throw new NotFoundException("Not found");
        }

        $moduleName = \substr($component, \strpos($component, 'node_modules/') + 13);
        [ $moduleName, $moduleFilePath ] = \explode("/", $moduleName, 2);

        $this->buildAmdModule(
            $this->fs->buildPath($basePath, 'node_modules', $moduleName),
            $this->fs->buildPath($this->getDynRoot(), 'amd', $path.'@', 'node_modules', $moduleName)
        );

        $expectedPath = $this->fs->buildPath($this->getDynRoot(), 'amd', $path.'@', $component);

        Kernel::debug("Serving `{expectedPath}`", ['expectedPath' => $expectedPath]);

        return $http->serveFile($expectedPath);
    }

    protected function get_mjs_file(string $path, string $component) {
        die("Path=$path File=$component MJS");
    }

    protected function get_node_modules_amd(string $path): ResponseInterface {
        $dynUrl = $this->getDynUrl() . '/amd' . $path . '@';
        $basePath = $this->env->baseRoot . $path;

        $packageLock = \json_decode(\file_get_contents($basePath . '/package-lock.json'), true);

        $mainPackage = $packageLock['packages'][''];
        
        $urlArgs = (string) $packageLock['lockfileVersion'];
        $packages = [];
        $problems = [];
        foreach ($mainPackage['dependencies'] as $dependencyName => $version) {
            $bower = null;
            $lockInfo = $packageLock['dependencies'][$dependencyName];
            $packagePath = $basePath . '/node_modules/' . $dependencyName;
            $packageInfoPath = $packagePath . '/package.json';
            $packageInfo = \json_decode(\file_get_contents($packageInfoPath), true);

            $packageMain = $packageInfo['browser'] ?: $packageInfo['jsdelivr'] ?: $packageInfo['unpkg'] ?: $packageInfo['main'] ?: $packageInfo['module'] ?: null;

            if ($packageMain === null && \file_exists($packagePath . '/bower.json')) {
                $bower = \json_decode(\file_get_contents($packagePath . '/bower.json'), true);
                $packageMain = $bower['browser'] ?: $bower['main'] ?: null;
            }

            if ($packageMain === null && \file_exists($packagePath . '/index.js')) {
                $packageMain = 'index.js';
            }

            if ($packageMain === null) {
                $problems[] = [
                    'name' => $dependencyName,
                    'problem' => "Could not find a main script for the package. Tried bower.json as well.",
                ];
                continue;
            }
            
            if (!\is_string($packageMain)) {
                $problems[] = [
                    'name' => $dependencyName,
                    'problem' => "The main script is not a path",
                ];
                continue;
            }

            if (\str_ends_with($packageMain, '.js')) {
                $packageMain = \substr($packageMain, 0, -3);
            }

            if ($this->fs->is_file($packagePath . '/' . $packageMain . '.min.js')) {
                $packageMain .= '.min';
            }

            $package = [
                'name' => $dependencyName,
                'location' => 'node_modules/' . $dependencyName,
                'main' => $packageMain,
                'version' => $packageInfo['version'] ?: '0.0',
            ];

            $packages[] = $package;
        }
        //echo "<pre>"; var_dump($packagesLock);die();

        $config = [
            "urlArgs" => $urlArgs,
            "nodeIdCompat" => true,
            "baseUrl" => $dynUrl,
            "packages" => $packages,
        ];

        $configJson = \json_encode($config, \JSON_PRETTY_PRINT);

        $pathJson = \json_encode($path);
        $problemsJson = \json_encode($problems, \JSON_PRETTY_PRINT);
        $packagesJson = \json_encode($packages, \JSON_PRETTY_PRINT);

        $content = <<<JS
            (() => {
            requirejs.config($configJson);

            /*
            function getPackage(name) {
                for (const p of config.packages) {
                    if (p.name == name) {
                        return p;
                    }
                }
            }

            function compareVersion(left, right) {
                let a = left.split('.'), b = right.split('.');
                while ((a[0] ? parseInt(a[0]) : 0) == (b[0] ? parseInt(b[0]) : 0) && a.length + b.length > 0) {
                    a = a.slice(1);
                    b = b.slice(1);
                }
                return (b[0] ? parseInt(b[0]) : 0) - (a[0] ? parseInt(a[0]) : 0);
            }

            // special handling of commonly loaded globals
            if (typeof jQuery != "undefined") {
                // jQuery is loaded as a global
                let jQueryInfo = getPackage('jquery');
                if (compareVersion(jQueryInfo.version, jQuery.fn.jquery) >= 0) {
                    // jQuery we got is newer or same
                    console.debug('Automatically using jQuery', jQuery.fn.jquery, 'global for requirejs');
                    define('jquery', () => jQuery);
                }
            }
            */
            })();
            JS;
        return $this->plain($content, ['Content-Type' => 'text/javascript']);
    }

    protected function get_node_modules(string $path, string $modulePath, Http $http) {
        $basePath = $this->fs->buildPath( $this->env->baseRoot, $path);
        $parts = \explode("/", $modulePath);

        if (\count($parts) === 1) {
            if (!\str_ends_with($parts[0], ".js")) {
                throw new NotFoundException("File not found");
            }
            $module = \substr($modulePath, 0, -3);
            $this->buildAmdModule($basePath, $module, $this->fs);

            $packageJson = \json_decode($this->fs->file_get_contents([$basePath, 'node_modules.amd', $module, 'package.json']), true);

            $main = $packageJson['browser'] ?: $packageJson['main'] ?: 'index.js';

            $file = $this->fs->buildPath($basePath, 'node_modules.amd', $module, $main);
            
            $relativePath = $this->fs->relativePath($this->env->baseRoot, $file);
            $url = Url::create()->withPath('/' . $relativePath);
            return $this->plain('', ['Location' => (string) $url], 302);
            $js = 'define([' . \json_encode($relativePath) . '], (mod) => mod);';

            return $this->plain($js, ['Content-Type' => 'text/javascript', 'Cache-Control' => 'no-cache']);

            die($js);


            die("Should serve the module, perhaps via redirect?");
        } else {
            $module = \array_shift($parts);
            $this->buildAmdModule($basePath, $module, $this->fs);
            $diskPath = $this->fs->buildPath($basePath, 'node_modules.amd', $modulePath);
            if ($this->fs->is_file($diskPath)) {
                return $http->serveFile($diskPath);
            }
            throw new NotFoundException("File `$modulePath` not found");
        }
    }

    protected function get_main(string $path, string $module) {
        $basePath = $this->fs->buildPath($this->env->baseRoot, $path);

        $fs = $this->fs->withPathPrefix($basePath, true);

        $modulePath = $fs->buildPath('node_modules', $module);
        $compiledPath = $fs->buildPath('node_modules.amd', $module);

        if ($fs->is_dir($compiledPath)) {
            $modulePath = $compiledPath;
        }

        if (!$fs->is_file($packageFile = $fs->buildPath($modulePath, 'package.json'))) {
            throw new NotFoundException("Module `$modulePath/package.json` was not found");
        }

        $package = \json_decode($fs->file_get_contents($packageFile), true);

        if (isset($package['main'])) {
            $mainFile = $package['main'];
        } else {
            $mainFile = 'main.js';
        }

        if (!$fs->is_file($modulePath . '/' . $mainFile)) {
            throw new LogicException("Module `$module` has no main file");
        }

        $body = $fs->file_get_contents($modulePath . '/' . $mainFile);

        switch ($this->identifyJSModuleType($body)) {
            case self::COMMONJS_MODULE:
                if ($this->translateModule($basePath, $modulePath, $compiledPath, $fs)) {
                    // converted to AMD
                    die("WE HAVE AMD");
                } else {
                    die("failed trans");
                }
                break;
            case self::AMD_MODULE:
            case self::UMD_MODULE:
                return $this->plain($body, ['Content-Type' => 'text/javascript']);
        }
        die("UNSUPPORTED MODULE TYPE ".$this->identifyJSModuleType($body));

        var_dump($this->identifyJSModuleType($body));

        echo "<pre>"; echo $body;die();


        if (\str_contains($mainFile, '/') || \str_contains($mainFile, '\\')) {
            return $this->plain(
                <<<JS
                define(['$path/node_modules/$module/$mainFile'], (res) => res);
                JS, 
                [ 'Content-Type' => 'text/javascript' ]);
        } else {
            return $this->plain(\file_get_contents($modulePath . '/' . $mainFile), [ 'Content-Type' => 'text/javascript' ]);
        }

        return $this->json($package);
    }

    protected function buildAmdModule(string $sourcePath, string $targetPath): void {
        $fs = $this->fs;
        if ($fs->is_dir($targetPath)) {
            // module is already built
            return;
        }
        if (!$fs->is_dir($sourcePath)) {
            throw new NotFoundException("Module `$moduleName` not found");
        }
        $r = $fs->buildPath(__DIR__, '../node_modules/requirejs/bin/r.js');

        $cmd = $this->getNodeCommand() . ' ' . $r . " -convert " . \escapeshellarg($sourcePath) . " " . \escapeshellarg($targetPath);

        $output = \shell_exec($cmd);

        if (trim($output) !== '') {
            $fs->rmdir($targetPath);
        }
    }

    protected function identifyJSModuleType(string $body): int {
        if (\str_contains($body, 'define.amd') || \str_contains($body, 'typeof define') || \str_contains($body, 'typeof exports')) {
            // define.amd indicates trying to detect the module type, which indicates UMD
            return self::UMD_MODULE;
        }
        if (\preg_match('/\bdefine\(/', $body)) {
            return self::AMD_MODULE;
        }
        if (\str_contains($body, 'module.exports') || \str_contains($body, 'require(') || \preg_match('/\bexports\./', $body)) {
            // module.exports is only used in commonjs
            return self::COMMONJS_MODULE;
        }
    }

}