<?php

use Fubber\JS\JSController;
use Fubber\Kernel\Bundles\Manifest;
use Fubber\Kernel\Controllers\Controllers;
use Fubber\Kernel\Environment\Environment;
use Fubber\Kernel\Filesystem\Filesystem;

 return new Manifest(__DIR__, 'fubber-js', 'Fubber\\JS\\', function(Controllers $controllers, Filesystem $fs, Environment $env) {
    $controllers->add(new JSController($env->baseRoot, $env->uploadRoot, $env->uploadUrl, 'js-dist'));
 });