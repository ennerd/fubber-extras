@extends('theme/default')
@section('title', 'Logged out')
@section('main')
<div class="padding">
    <h1>You have been logged out</h1>
    <p>You have logged out from the site. Thank you for visiting.</p>
</div>
@stop