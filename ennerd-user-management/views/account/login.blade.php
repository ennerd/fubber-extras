<?php use function Fubber\trans; ?>@extends('theme/default')
@section('title', trans('Login'))
@section('main')
<div class="padding">
<h1>{{trans("Login")}}</h1>
<?=$form->begin(); ?>
<div class='text field'>
    <?=$form->label('email', trans('E-mail Address:')); ?>
    <?=$form->text('email'); ?>
    <?=$form->hasError('email'); ?>
</div>
<div class='password field'>
    <?=$form->label('password', trans('Password')); ?>
    <?=$form->password('password'); ?>
</div>
<div class='buttons'>
    <?=$form->submit('login', trans('Login')); ?>
    <a href="/register/">{{trans("Create Account")}}</a>
    <a href="/login/forgot-password/">{{trans("Forgot Password?")}}</a>
    <a href='/tos/'>{{trans("Terms of Service")}}</a>
</div>
<?=$form->end(); ?>
</div>
@stop