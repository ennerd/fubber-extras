<?php use function Fubber\trans; ?>@extends('theme/default')
@section('title', trans('Create Account'))
@section('main')
<div class="padding">
    <h1>{{trans("Register a New Account")}}</h1>
    @form($form)
    @field('first_name', 'text', trans('First Name'))
    @field('last_name', 'text', trans('Last Name'))
    @field('email', 'email', trans('E-mail'))
    @field('password', 'password', trans('Password'))
    @field('password_confirm', 'password', trans('Confirm Password'))
    @field('tos', 'checkbox', trans('Accept the Terms and Conditions'))
    
    <div class='buttons'>
        <?=$form->submit('register', trans('Register')); ?>
        <a href="/login/">{{trans("Login")}}</a>
        <a href="/login/forgot-password/">{{trans("Forgot Password?")}}</a>
        <a href='/tos/'>{{trans("Terms of Service")}}</a>
    </div>
    @endform
</div>
@stop