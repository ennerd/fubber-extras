<?php use function Fubber\trans; ?>@extends('theme/default')
@section('title', 'Forgot password')
@section('main')
<div class="padding">
    <h1>{{trans("Forgot your password?")}}</h1>
    <p>{{trans("Provide your e-mail address below, and we will send you an e-mail to reset your password.")}}</p>
    <?=$form->begin(); ?>
    <div class='text field'>
        <?=$form->label('email', trans('E-mail Address:')); ?>
        <?=$form->text('email'); ?>
        <?=$form->hasError('email'); ?>
    </div>
    <div class='buttons'>
        <?=$form->submit('send', 'Send'); ?>
        <a href="/register/">{{trans("Create Account")}}</a>
        <a href="/login/forgot-password/">{{trans("Forgot Password?")}}</a>
        <a href='/tos/'>{{trans("Terms of Service")}}</a>
    </div>
    <?=$form->end(); ?>
</div>
@stop
