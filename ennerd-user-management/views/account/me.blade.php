<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('Your Account'))
@section('main')
<div class="padding">
    <h1>Account Page</h1>
    <p>You are currently logged in as {{User::getCurrent($state)->getDisplayName()}}.</p>
    <p><a class='button' href="/me/edit/"><i class="fas fa-edit"></i>{{trans("Edit Info")}}</a> <a class='button' href="/logout/"><i class="fas fa-sign-out-alt"></i>{{trans("Logout")}}</a></p>
</div>
@stop