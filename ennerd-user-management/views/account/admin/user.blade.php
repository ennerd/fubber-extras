<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('Editing user :user', ['user' => $user->getDisplayName()]))
@section('main')
<div class="padding">
<h1>{{$user->getDisplayName()}}</h1>
<?php if($user->is_admin) { ?>
<p>{{trans("Administrator account")}}</p>
<?php } ?>
<table class="form">
    <tbody>
        <tr>
            <th>{{trans("First Name:")}}</th><td>{{$user->first_name}}</td>
            <th>{{trans("Last Name:")}}</th><td>{{$user->last_name}}</td>
        </tr>
        <tr>
            <th>{{trans("E-mail:")}}</th><td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td>
            <th>{{trans("Last Login:")}}</th><td>{{$user->last_login}}</td>
        </tr>
    </tbody>
</table>
</div>
@stop