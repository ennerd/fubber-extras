<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('Users'))
@section('main')
<div class="padding">
<h1>{{trans("Manage Users")}}</h1>
<table data-tablesource="/api/tablesource/users/" data-rowwriter="users_row">
</table>
<script type="text/javascript">
    function users_row(row) {
        return '<tr data-href="/admin/users/' + this.id + '/"><td>' + this.first_name + '</td><td>' + this.last_name + '</td><td><a href="mailto:' + this.email + '">' + this.email + '</a></td><td>' + this.last_login + '</td></tr>';
    }
</script>
</div>
@stop