<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('Edit Account'))
@section('main')
<div class="padding">
    <h1>{{trans("Edit your Account")}}</h1>
    @form($form, ["autocomplete" => "new-password"])
    @field('first_name', 'text', trans('First Name'))
    @field('last_name', 'text', trans('Last Name'))
    @field('email', 'email', trans('E-mail'))
    @field('password', 'password', trans('Password'), ['autocomplete' => 'new-password'])
    @field('password_confirm', 'password', trans('Confirm Password'))
    
    <div class='buttons'>
        <?=$form->submit('save', trans('Save')); ?>
    </div>
    @endform
</div>
@stop