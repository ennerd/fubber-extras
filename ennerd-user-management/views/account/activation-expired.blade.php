@extends('theme/default')
@section('title', 'Link has expired')
@section('main')
<div class="padding">
<h1>Your account activation link has expired</h1>
<p>Your account activation link has expired. Unless you activated your account earlier, you will have to register again.</p>
</div>
@stop