<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('Set password'))
@section('main')
<div class="padding">
    <h1>{{trans("Create a new password")}}</h1>
    <p>{{trans("Please create a new password for your account:")}}</p>
    <?=$form->begin(); ?>
    <div class='password field'>
        <?=$form->label('password', trans('Password:')); ?>
        <?=$form->password('password'); ?>
        <?=$form->hasError('password'); ?>
    </div>
    <div class='password field'>
        <?=$form->label('password_confirm', trans('Confirm Password')); ?>
        <?=$form->password('password_confirm'); ?>
        <?=$form->hasError('password_confirm'); ?>
    </div>
    <div class='buttons'>
        <?=$form->submit('set_password', trans('Set Password')); ?>
        <a href="/register/">{{trans("Create Account")}}</a>
        <a href="/login/forgot-password/">{{trans("Forgot Password?")}}</a>
        <a href='/tos/'>{{trans("Terms of Service")}}</a>
    </div>
    <?=$form->end(); ?>
</div>
@stop
