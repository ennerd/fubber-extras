@extends('theme/default')
@section('title', 'Account Verification')
@section('main')
<div class="padding">
    <h1>Account Verification Required</h1>
    <p>Thank you for registering an account. Please check your e-mail for an activation e-mail. If you have not received it, check your spam folder.</p>
</div>
@stop