@extends('theme/mails/default')
@section('title', 'Password Recovery')
@section('contents')
<p>You've requested a password reset on {{$base_url}}. To reset your password, click the following link. </p>

<p><a href="{{$url}}">{{$url}}</a></p>

<p>If you did not request this, please ignore this e-mail.</p>
@stop