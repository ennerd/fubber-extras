@extends('theme/mails/default')
@section('title', 'Account Activation')
@section('contents')
<p>Thank you for activating your account</p>

<p>To activate your account, visit the following link:</p>

<p><a href="{{$url}}">{{$url}}</a></p>

<p>If you did not sign up, please ignore this e-mail.</p>
@stop