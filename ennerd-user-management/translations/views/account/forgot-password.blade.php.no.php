<?php return array (
  'Forotyourpasrd' => 'Glemt passordet?',
  'Prodeyouremailaddssbelowandwewillsendyouanemailtoresetyourpasrd' => 'Oppgi din e-postadresse nedenfor så sender vi deg en link som du kan bruke til å nullstille passordet ditt med.',
  'EmailAddss' => 'E-postadresse',
  'CreteAccnt' => 'Opprett konto',
  'ForotPasrd' => 'Glemt passordet?',
  'TermsofSerce' => 'Tjenestevilkår',
);