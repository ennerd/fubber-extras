<?php return array (
  'RegeraNewAccnt' => 'Opprett brukerkonto',
  'FirstName' => 'Fornavn',
  'LastName' => 'Etternavn',
  'Email' => 'E-postadresse',
  'Pasrd' => 'Passord',
  'ConrmPasrd' => 'Bekreft passord',
  'AccpttheTermsandConns' => 'Bekreft å ha lest tjenestevilkårene',
  'Reger' => 'Registrer',
  'Login' => 'Logg inn',
  'ForotPasrd' => 'Glemt passordet?',
  'TermsofSerce' => 'Tjenestevilkår',
  'CreteAccnt' => 'Opprett konto',
);