<?php return array (
  'UserAccts' => 'Brukerkontoer',
  'ID' => 'ID',
  'GivenName' => 'Fornavn',
  'FamlyName' => 'Etternavn',
  'Emailaddss' => 'E-postadresse',
  'LastLogin' => 'Sist logget inn',
  'GloalSysemAdmin' => 'Global superadministrator',
  'Emailaddssconed' => 'E-postadresse bekreftet',
  'UserAccnt' => 'Brukerkonto',
);
