<?php
namespace Exceptions;

class NotLoggedInException extends AccessDeniedException {

    public function getDefaultStatus(): array {
        return [403, "Not Authenticated"];
    }

};