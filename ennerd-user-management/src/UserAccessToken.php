<?php
use Fubber\Table;
use Fubber\Kernel\State;

use function Fubber\trans;

class UserAccessToken extends Table {
    public static $_table = 'user_accesstokens';
    public static $_cols = ['id', 'user_id', 'name', 'token', 'is_enabled', 'issued_date', 'expiration_date', 'privileges'];

    public $id, $user_id, $name, $token, $is_enabled, $issued_date, $expiration_date, $privileges;

    public static function meta(State $state) {
        return [
            'names' => [
                'singular' => trans('API-token'),
                'plural' => trans('API-tokens'),
                ],
            'displayName' => function($row, $state) {
                return trans("Access Token ':name'", (array) $row);
            },
            'cols' => [
                'id' => [
                    'caption' => trans('ID'),
                    'type' => 'primarykey',
                    'readonly' => true,
                ],
                'user_id' => [
                    'caption' => trans("User"),
                    'type' => User::class,
                    'readonly' => !$state->access->isSuperAdmin(),
                    ],
                'name' => [
                    'caption' => trans("Name"),
                    'type' => 'text',
                    ],
                'token' => [
                    'caption' => trans("Access Token"),
                    'type' => 'text',
                    'readonly' => true,
                    ],
                'is_enabled' => [
                    'caption' => trans("Enabled"),
                    'type' => 'checkbox',
                    'default' => 1,
                    ],
                'issued_date' => [
                    'caption' => trans('Issued Date'),
                    'type' => 'datetime',
                    'readonly' => true,
                    ],
                'expiration_date' => [
                    'caption' => trans('Expiration Date'),
                    'type' => 'datetime',
                    ],
                'privileges' => [
                    'caption' => trans('Privileges'),
                    'type' => 'textarea',
                    'readonly' => true,
                    ],
                ],
            ];
    }

    public static function loadByToken(string $token): ?UserAccessToken {
        return static::allUnsafe()->where('token','=',$token)->one();
    }

    public function beforeSave(array $properties=null) {
        if ($this->expiration_date == '') {
            $this->expiration_date = null;
        }
        if ($this->privileges === null) {
            $this->privileges = '';
        }
    }

    public function jsonSerialize() {
        return [
            'id' => intval($this->id),
            'user_id' => intval($this->user_id),
            'name' => $this->name,
            'is_enabled' => !!$this->is_enabled,
            'issued_date' => date('c', strtotime($this->issued_date)),
            'expiration_date' => $this->expiration_date !== null ? date('c', strtotime($this->expiration_date)) : null,
            'privileges' => explode(" ", $this->privileges),
            ];
    }

    public function isInvalid(): ?array {
        $errors = new Fubber\Util\Errors($this);
        $errors->required('user_id');
        $errors->oneOf('is_enabled', [0, 1]);
        $errors->required('name');
        $errors->required('token');
        $errors->minLen('token', 16);
        $errors->required('issued_date');
        return $errors->isInvalid();
    }


    public static function create(State $state=null): self {
        $token = new self();
        $token->token = str_replace(["+", "/"], ["-", "_"], base64_encode(random_bytes(12)));
        if ($state === null || !($user = User::getCurrent($state))) {
            return $token;
        }
        $token->user_id = $user->id;
        $token->issued_date = gmdate('Y-m-d H:i:s');
        $token->is_enabled = true;
        return $token;
    }

    public function canView(State $state) {
        return $state->access->isSuperAdmin();
        if (!$state->access->isLoggedIn()) {
            return false;
        }
        return $state->access->isSuperAdmin() || $state->user_id === $this->user_id;
    }

    public function canEdit(State $state) {
        return $state->access->isSuperAdmin();
        return $state->access->isSuperAdmin() || $state->user_id === $this->user_id;
    }

    public function canDelete(State $state) {
        return $state->access->isSuperAdmin();
        return $state->access->isSuperAdmin() || $state->user_id === $this->user_id;
    }

    public static function canCreate(State $state) {
        return $state->access->isSuperAdmin();
        return $state->access->isLoggedIn();
    }

    public static function canList(State $state) {
        return $state->access->isSuperAdmin();
    }

    public static function canManage(State $state) {
        return $state->access->isSuperAdmin();
    }
}
