<?php
namespace EnnerdUserMgmt;

use Fubber\Hooks;
use UserAccessToken;
use EnnerdAppTheme\Theme;
use Fubber\UnauthorizedException;
use Fubber\FlashMessage;

use function Fubber\trans;

class UserAccessTokenController extends \Theme\CRUDController {
    // Hooks::filter 'EnnerdUserMgmt\UserAccessTokenController::$enabled'
    public static bool $enabled = false;
    public static string $prefix = 'admin/users-accesstokens';
    public static string $model = UserAccessToken::class;
    public static bool $enable_rest_api = true;

    public static $index_cols = [
        'id' => ['left', 40],
        'user_id' => ['left', 250],
        'name' => ['left'],
        'issued_date' => ['center', 150],
        'expiration_date' => ['center', 150],
        ];


    public static $view_def = [
        ['field', 'id'],
        ['cols', [
            ['field', 'name'], ['field', 'user_id'],
            ]],
        ['cols',
            ['field', 'issued_date'], ['field', 'expiration_date'], ['field', 'is_enabled'],
            ],
        ['field', 'privileges'],
        ];

    public static function edit_def(State $state) {
        $edit_def = [
            ['field', 'id'],
            ['cols', [
                ['field', 'name'], ['field', 'user_id'],
                ]],
            ['cols',
                ['field', 'issued_date'], ['field', 'expiration_date'], ['field', 'is_enabled'],
                ],
            ['field', 'privileges'],
            ];

        if ($state && $state->access->isSuperAdmin()) {
            $edit_def[] = ['field', 'token'];
        }
        return $edit_def;
    }

    public static array $create_def = [
        ['field', 'token'],
        ['field', 'id'],
        ['cols', [
            ['field', 'name'], ['field', 'user_id'],
            ]],
        ['cols',
            ['field', 'issued_date'], ['field', 'expiration_date'], ['field', 'is_enabled'],
            ],
        ['field', 'privileges'],
        ];

    public static function delete_text(State $state) {
        return trans('Deleting this access token will prevent any applications that is using it from accessing the API.');;
    }

    public static function init() {
        if (!static::_getVal('enabled')) {
            return;
        }
        parent::init();
        Hooks::listen('User::getCurrent', function($user, $state) {
            $header = $state->request->getHeader('Authorization');
            if (isset($header[0])) {
                list($type, $token) = explode(" ", $header[0]);
                if ($type === 'Bearer') {
                    $token = UserAccessToken::loadByToken($token);
                    if ($token === null) {
                        throw new UnauthorizedException('Invalid access token', 1);
                    }
                    if (!$token->is_enabled) {
                        throw new UnauthorizedException('Disabled access token', 2);
                    }
                    if ($token->expiration_date && strtotime($token->expiration_date) < time()) {
                        throw new UnauthorizedException('Expired access token');
                    }
                    $user = \User::loadUnsafe($token->user_id);
                    if (!$user->isEnabled()) {
                        throw new UnauthorizedException('Account is disabled');
                    }
                    return $user;
                }
            }
        });
        Hooks::listen('Theme.ControlPanel.Icons', function($icons, $state) {
            if($state->access(\User::class)->hasPrivilege('Manage')) {
                $icons[] = Theme::icon($state, static::getMeta($state)->getPluralName(), '<i class="fas fa-4x fa-user-shield blue"></i>', [
                    "onclick" => "location.href='/admin/users-accesstokens/';"
                ]);
            }
            return $icons;
        });
    }
}
