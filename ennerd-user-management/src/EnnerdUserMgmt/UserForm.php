<?php
namespace EnnerdUserMgmt;
use Fubber\FlashMessage;
use Fubber\Forms\TableForm;
use Fubber\Kernel\State;
use User;

class UserForm extends TableForm {
    protected $_user;

    public function __construct(State $state, User $object) {
        parent::__construct($state, $object, null);
        $this->addField('password_confirm', false);
    }

    public function isInvalid(): ?array {
        $errors = new \Fubber\Util\Errors($this);
        $errors->addErrors(parent::isInvalid());
        $errors->required('password_confirm');
        if($this->password != $this->password_confirm) {
            $errors->addError('password_confirm', 'Passwords don\'t match');
        }
        if($this->password == "" && $this->password_confirm == "") {
            $errors->removeErrors(['password', 'password_confirm']);
        }
        return $errors->isInvalid();
    }
}
