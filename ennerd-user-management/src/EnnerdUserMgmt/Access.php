<?php
namespace EnnerdUserMgmt;

use User;
use Fubber\Hooks;
use Fubber\Kernel\State;

/**
 * Reference:
 * 
 * $objects usually have the privileges View, Edit and Delete
 * classes usually have the privileges Create, List, Manage
 * 
 * Generally, there are two methods to check access:
 * 
 *     $state->access->hasPrivilege() // returns true or false
 * 
 * and
 * 
 *     $state->access->assertPrivilege() // throw AccessDeniedException or returns true
 * 
 * Whenever a method requires access, we suggest using the assert-variant.
 *
 * Global privileges are implemented by filtering the Access.Privilege.<privilegeName> hook
 * 
 *     \Fubber\Hooks::listen('Access.Privilege.ViewOtherUsers', function($hasAccess, $state) {
 *         // Super Admins have all privileges by default. Here logged in users also get it.
 *         if($state->access->isLoggedIn())
 *             return true;
 *         return $hasAccess;
 *     });
 * 
 * and checked like this:
 * 
 *     if($state->access->hasPrivilege('ViewOtherUsers')) {
 *         // Access granted    
 *     }
 * 
 * 
 * Privileges on objects are implemented by creating the appropriate can*-method:
 * 
 *     class MyClass {
 *         public function canView(State $state) {
 *             return true;
 *         }
 *         public function canEdit(State $state) {
 *             return false;
 *         }
 *         public function canDelete(State $state) {
 *             return false;
 *         }
 *     }
 * 
 * optionally filtered like this:
 * 
 *     \Fubber\Hooks::listen('Privilege.MyClass.View', function($hasAccess, $state) {
 *         if($someCheck)
 *             return true;
 *         return $hasAccess;
 *     });
 * 
 * and checked like this:
 * 
 *     if($state->access($myInstance)->hasPrivilege('View')) {
 *         // Access granted    
 *     }
 *  
 * Privileges on classes are implemented by creating the appropriate static can*-method:
 * 
 *     class MyClass {
 *         public static function canCreate(State $state) {
 *             return false;
 *         }
 *         public function canList(State $state) {
 *             return true;
 *         }
 *         public function canManage(State $state) {
 *             return false;
 *         }
 *     }
 * 
 * optionally filtered like this:
 * 
 *     \Fubber\Hooks::listen('Privilege.MyClass.Create', function($hasAccess, $state) {
 *         if($someCheck)
 *             return true;
 *         return $hasAccess;
 *     });
 * 
 * and checked like this:
 * 
 *     if($state->access(MyClass::class)->hasPrivilege('Manage')) {
 *         // Access granted    
 *     }
 *
 */
class Access {
    
    protected $sudoLevel = 0;
    protected $entity;
    protected $state;
    
    public static function getCurrent(State $state) {
        if(isset($state->cache['currentAccess']))
            return $state->cache['currentAccess'];
        return $state->cache['currentAccess'] = new static($state);
    }
    
    /**
     * $entity is a class name or an object instance for which we want to inspect
     * access. If not provided, checks global privileges.
     */
    public function __construct($state, $entity=null) {
        $this->state = $state;
        $this->entity = $entity;
        if($entity && is_object($entity))
            $this->entityClass = get_class($entity);
        else
            $this->entityClass = $entity;
    }
    
    public function sudo(callable $callback) {
        if($this->entity)
            throw new \Fubber\CodingErrorException("Can only sudo on the root access object. You are calling sudo on a class or instance specific access object.");
        $this->sudoLevel++;
        $res = call_user_func($callback);
        $this->sudoLevel--;
        return $res;
    }
    
    /**
     * Allows $state->access(\SomeObject::class)->assertPrivilege()
     */
    public function __invoke($object) {
        return new static($this->state, $object);
    }
    
    public function assertPrivilege($nameOfPrivilege, $default=null) {
        if(!$this->hasPrivilege($nameOfPrivilege, $default)) {
            if($this->entity) {
                throw new \Exceptions\AccessDeniedException("You don't have the '".$nameOfPrivilege."' privilege on '".$this->entityClass."'.");
            }
            throw new \Exceptions\AccessDeniedException("You don't have the '".$nameOfPrivilege."' privilege.");
        }
    }
    
    /**
     * $default = null means superadmins default to null
     * if($this->entity) we'll check only for this entity
     */
    public function hasPrivilege($nameOfPrivilege, $default=null) {
        if($this->entity && $default !== null) {
            $e = new \Fubber\CodingErrorException("Don't specify \$default for Access::hasPrivilege or Access::assertPrivilege on objects or classes");
            $e->suggestion = 'Implement the method '.$this->entityClass.'::can'.$nameOfPrivilege.'(\$state) method';
            $className = $this->entity;
            throw $e;
        }

        // Prevent public caching, since request checks privileges
        $this->state->response->privateCache(3600);

        if($this->entity) {
            if(is_object($this->entity)) {
                $filter = 'Access.'.$this->entityClass.'.'.$nameOfPrivilege;
                if(method_exists($this->entity, $methodName = 'can'.$nameOfPrivilege)) {
                    // This check is fast, and prevents security bugs.
                    $mc = new \ReflectionMethod($this->entity, $methodName);
                    if(!$mc->isStatic()) {
                        return Hooks::filter($filter, $this->entity->$methodName($this->state), $this->state);
                    }
                }
                if(Hooks::hasListeners($filter))
                    return Hooks::filter($filter, $this->isSuperAdmin(), $this->state);
                throw new \Fubber\CodingErrorException("No method ".$this->entityClass."::can$nameOfPrivilege and no filter for '$filter' defined. (canView, canEdit, canDelete)");
            } else if(class_exists($this->entity)) {
                $filter = 'Access.'.$this->entity.'.'.$nameOfPrivilege;
                if(method_exists($this->entity, $methodName = 'can'.$nameOfPrivilege)) {
                    $mc = new \ReflectionMethod($this->entity, $methodName);
                    if($mc->isStatic()) {
                        $className = $this->entity;
                        return Hooks::filter($filter, $className::$methodName($this->state), $this->state);
                    }
                }
                if(Hooks::hasListeners($filter))
                    return Hooks::filter($filter, $this->isSuperAdmin(), $this->state);
                throw new \Fubber\CodingErrorException("No static method ".$this->entityClass."::can$nameOfPrivilege and no filter for '$filter' defined. (canCreate, canList, canManage)");
            } else {
                throw new \Fubber\CodingErrorException("Class ".$this->entity." not found when checking access.");
            }
        } else {
            if($default === null) {
                $default = $this->isSuperAdmin();
            }
                
            return Hooks::filter('Access.Privilege.'.$nameOfPrivilege, $default, $this->state);
        }
    }

    public function assertLoggedIn() {
        if(!$this->isLoggedIn())
            throw new \Exceptions\NotLoggedInException("You must be logged to access this.");
        return true;
    }
    
    public function assertSuperAdmin() {
        $this->assertLoggedIn();
        if(!$this->isSuperAdmin())
            throw new \Exceptions\AccessDeniedException("You don't have access to the requested resource");
        return true;
    }
    
    public function isLoggedIn() {
        // Prevent public caching, since request checks privileges
        $this->state->response->privateCache(1);
        return !!User::getCurrent($this->state);
    }
    
    public function isSuperAdmin() {
        // Sudoing should not trigger private caching, since it has nothing to do with who is logged in
        if($this->sudoLevel > 0)
            return true;
        $this->state->response->privateCache(1);
        $user = User::getCurrent($this->state);
        return !!($user && $user->isSuperAdmin());
    }
}