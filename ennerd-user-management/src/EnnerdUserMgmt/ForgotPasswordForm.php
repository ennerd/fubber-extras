<?php
namespace EnnerdUserMgmt;
use Fubber\FlashMessage;
use Fubber\Forms\Form;
use Fubber\Kernel\State;
use \User;

class ForgotPasswordForm extends Form {
    public function __construct(State $state) {
        parent::__construct($state, 'forgot-password');
        $this->addField('email', false);
        
        if($this->accept()) {
            if($this->isInvalid()) {
                new FlashMessage($state, 'Please provide your e-mail address');
            } else {
                $user = User::all()->where('email', '=', $this->email)->one();
                if($user) {
                    $this->success = !!$user->sendPasswordRecoveryEmail($state);
                    new FlashMessage($state, 'Password recovery e-mail was sent');
                } else {
                    new FlashMessage($state, 'Unknown e-mail address');
                }
            }
        }
    }
    
    public function isInvalid(): ?array {
        $errors = new \Fubber\Util\Errors($this);
        $errors->required('email');
        return $errors->isInvalid();
    }
}
