<?php
namespace EnnerdUserMgmt;
use Fubber\Kernel\State;

use User;

use function Fubber\trans;

class LoginForm extends \Fubber\Forms\Form {

    public User $user;

    public function __construct(State $state, $extraData=null) {
        parent::__construct($state, 'login-form', [
            'email' => '',
            'password' => '',
            ], $extraData);

        if($this->accept()) {
            /** @var ?User */
            $user = User::findByCredentials($this->email, $this->password);
            if(!$user) {
                new \Fubber\FlashMessage($state, 'Incorrect e-mail or password', \Fubber\FlashMessage::ERROR);
            } else {
                if(!$user->email_confirmed) {
                    new \Fubber\FlashMessage($state, trans("Your account has not been confirmed yet. Did you receive your activation e-mail?"), \Fubber\FlashMessage::INFO);
                    $user->sendActivationEmail();
                    return;
                }

                $user->login($state);
                $this->success = true;

            }
        }
    }

}
