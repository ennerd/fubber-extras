<?php
namespace EnnerdUserMgmt;

use Fubber\Kernel\State;
use Fubber\Hooks;
use User;
use EnnerdAppTheme\Theme;
use Exceptions\NotLoggedInException;
use Exceptions\AccessDeniedException;
use Fubber\FlashMessage;

use function Fubber\trans;

/**
 * This controller does:
 * 
 * 1. Alter the $state object with $state->user, $state->user_id and $state->access. 
 * 2. Adds routes:
 *      profile => /me/, 
 *      login => /login/, 
 *      logout => /logout/
 *      register => /register/, 
 *      forgot_password => /login/forgot-password/,
 *      /login/set-password/
 *      /me/account-confirmation/
 *      /me/edit/
 *      /me/{accountId}/activation/
 *      \Exceptions\NotLoggedInException::class (low priority - can be overridden)
 *      \Exceptions\AccessDeniedException::class (low priority)
 *      
 * Look up the \EnnerdUserMgmt\Access class and \User::class for documentation.
 */

class UserController extends \Theme\CRUDController {
    public static string $prefix = 'admin/users';
    public static string $model = \User::class;
    public static bool $enable_rest_api = true;
    public static $index_cols = [
        'id' => ['left', 50],
        'first_name' => ['left', 200],
        'last_name' => ['left', 200],
        'email' => ['left'],
        'last_login' => ['right', 150],
        ];

    public static $view_def = [
        ["field", "id"],
        ["cols", [
            ["field", "first_name"],
            ], [
            ["field", "last_name"],
            ]
        ],
        ["field", "email"],
        ["field", "last_login"],
        ["cols", [
            ["field", "is_admin"],
            ], [
            ["field", "email_confirmed"],
            ]]
        ];

    //public static $edit_title = 'Editing :first_name :last_name';
    public static function edit_def(State $state, $extraData) {
        $result = [
            ["field", "id", "readonly" => true],
            ["cols", [
                ["field", "first_name"],
                ], [
                ["field", "last_name"],
                ]
            ],
            ["field", "email"],
            ["field", "last_login"],
            ["cols", [
                ["field", "is_admin"],
                ], [
                ["field", "email_confirmed"],
                ]]
            ];
        if ($state->user_id && $state->user_id == (int) $extraData['id']) {
            $result[] = ['field', 'password'];
        }
        return $result;
    }

    public static array $create_def = [
        ["cols", [
            ["field", "first_name"],
            ], [
            ["field", "last_name"],
            ]
        ],
        ["field", "password"],
        ["field", "email"],
        ["field", "is_admin"],
        ["field", "email_confirmed"],
        ];



    public static function init() {
        parent::init();

        State::$onConstructed->listen(function($state) {
            $state->lazy('user_id', function($state) {
                if($user = $state->user)
                    return (int) $user->id;
                return false;
            });
            $state->lazy('user', User::getCurrent(...));
            $state->lazy('access', Access::getCurrent(...));    
        });

        Hooks::listen('Theme.Header.Nav', [ static::class, 'theme_header_nav' ]);
        Hooks::listen('Theme.ControlPanel.Icons', [ static::class, 'theme_controlpanel_icons' ]);
        Hooks::listen(\Theme\Meta\ColumnDefs::class.'.Def.created_by', function($state) {
            return [
                'caption' => trans('Created By'),
                'type' => \User::class,
                'readonly' => true,
                ];
        });
    }
    
    public static function routes() {
        $result = [
            'POST /rest-api/users/login/' => [ static::class, 'api_login' ],
            'POST /rest-api/users/logout/' => [ static::class, 'api_logout' ],
            'POST /rest-api/users/delete/' => [ static::class, 'api_delete' ],
            // 'GET|POST /admin/translate/' => [ \Fubber\I18n\I18nController::class, 'ui' ],
            'profile GET /me/' => [ static::class, 'me' ],
            'login GET|POST /login/' => [ static::class, 'login' ],
            'register GET|POST /register/' => [ static::class, 'account_register' ],
            'forgot_password GET|POST /login/forgot-password/' => [ static::class, 'forgot_password' ],
            'GET|POST /login/set-password/' => [ static::class, 'set_password_from_mail' ],
            'GET /me/account-confirmation/' => [ static::class, 'me_confirmation' ],
            'GET /me/{account:\d+}/activation/' => [ static::class, 'me_activation' ],
            'logout GET /logout/' => [ static::class, 'logout' ],
            'GET|POST /me/edit/' => [ static::class, 'me_edit' ],
//            'admin_users GET /admin/users/' => [ static::class, 'admin_users' ],
//            'admin_users_edit GET|POST /admin/userss/{userId:\d+}/' => [ static::class, 'admin_users_edit' ],
            'admin_users_impersonate GET /admin/users/{userId:\d+}/impersonate/' => [ static::class, 'admin_users_impersonate' ],
//            NotLoggedInException::class => [ static::class, 'not_logged_in_exception' ],
            100 => [
                NotLoggedInException::class => [ static::class, 'not_logged_in_exception' ],
                //AccessDeniedException::class => [ static::class, 'access_denied_exception' ],
                ],
            ] + parent::routes();
        return $result;
    }

    public static function theme_header_nav($header, $state) {
        $user = User::getCurrent($state);
        if($user) {
            $header[] = '<a href="/me/">'.htmlspecialchars($user->getDisplayName()).'</a>';
        } else {
            $header[] = '<a href="/me/">'.trans("Login").'</a>';
        }
        return $header;
    }

    public static function theme_controlpanel_icons( $icons, $state ) {
        if($state->access(\User::class)->hasPrivilege('Manage')) {
            $icons[] = Theme::icon($state, static::getMeta($state)->getPluralName(), '<i class="fas fa-4x fa-users-cog blue"></i>', [
                "onclick" => "location.href='/admin/users/';"
                ]);
        }
        return $icons;
    }

    public static function me(State $state) {
        $state->cacheability->privateCache(10);
        $user = User::getCurrent($state);
        if(!$user) {
            throw new NotLoggedInException();
        }
        return $state->view('account/me', ['user' => $user]);
    }

    public static function login(State $state) {
        // no need to cache the login page
        $state->response->noCache();
        $form = new LoginForm($state, isset($state->get['continue_to']) ? [ 'continue_to' => $state->get['continue_to'] ] : null);
        if($form->success) {
            new FlashMessage($state, trans("Logged in as :display_name", ["display_name" => User::getCurrent($state)->getDisplayName()]), FlashMessage::POSITIVE);
            $user = User::getCurrent($state);
            $user->last_login = gmdate('Y-m-d H:i:s');
            $user->save(['last_login']);
            $extraData = $form->getExtraData();
            if(isset($extraData['continue_to'])) {
                $url = $extraData['continue_to'];
                if(strpos($url, '?')===false)
                    $url .= '?user.login=1&loggedin=';
                else
                    $url .= '&user.login=1&loggedin=';
                $url .= microtime(true);
                return $state->redirect($url);
            }
                
            return $state->redirect('/me/?_='.microtime(true));
        }
        return $state->view('account/login', ['form' => $form]);
    }
    
    public static function account_register(State $state) {
        $state->response->noCache();
        $user = new User();
        try {
            $form = new SignupForm($state, $user);
            if($form->success) {
                new FlashMessage($state, trans("Thank you for registering. Please check your e-mail."), [], FlashMessage::POSITIVE);
                return $state->redirect('/me/account-confirmation/');
            }
        } catch (\Exception $e) {
            throw $e;
        }

        return $state->view('account/register', ['form' => $form]);
    }
    
    public static function me_confirmation(State $state) {
        $state->response->noCache();
        return $state->view('account/confirmation');
    }
    
    public static function me_activation(State $state, User $account) {
        $state->response->noCache();
        if($account->email_confirmed) {
            new FlashMessage($state, trans('Your account is already activated.'), [], FlashMessage::POSITIVE);
            return $state->redirect('/me/');
        }
        if($state->get['expires'] < time()) {
            return $state->view('account/activation-expired');
        } else {
            $account->email_confirmed = 1;
            $account->save(['email_confirmed']);
            new FlashMessage($state, trans('Thank you for activating your account.'), [], FlashMessage::POSITIVE);
            return $state->redirect('/me/');
        }
    }
    
    public static function forgot_password(State $state) {
        $state->response->noCache();
        $passwordForm = new ForgotPasswordForm($state, 'forgot-password');
        if($passwordForm->success) {
            new FlashMessage($state, trans('Check your e-mail'), FlashMessage::POSITIVE);
            return $state->redirect('/login/');
        }
        return $state->view('account/forgot-password', ['form' => $passwordForm]);
    }

    /**
     * Delete the current user
     *
     * Allows the current user to delete their own user account. When the account is deleted, there is no way to recover the user data.
     *
     * @CRUDController.action("delete_action", {"title": "Delete user account", "response": true, "description": "Deletes the user account of the currently logged in user. If no user is logged in, throws Fubber\\UnauthorizedException.", "method": "POST"})
     */
    public static function api_delete(State $state) {
        $state->json = true;
        $user = User::getCurrent($state);
        if (!$user) {
            throw new \Fubber\UnauthorizedException("No user is logged in");
        }
        if ($state->request->post['confirmed'] != '1') {
            throw new \Fubber\BadRequestException("Not confirmed");
        }
        try {
            $deleted = $user->delete();
        } catch (\Throwable $e) {
            return $state->json(false);
        }
        $state->response->noCache();
        $state->session->destroy();
        return $state->json($deleted);
    }

    /**
     * Logout the currently logged in user
     *
     * Allows users to logout from the system. The logout will be effective immediately.
     *
     * @CRUDController.action("logout", {"response": true})
     */
    public static function api_logout(State $state) {
        $state->json = true;
        $isLoggedIn = !!$state->user_id;
        $state->response->noCache();
        $state->session->destroy();
        return $state->json($isLoggedIn);
    }

    public static function api_login(State $state) {
        $state->json = true;
        if (!isset($state->request->post['email']) || !isset($state->request->post['password'])) {
            throw new \Fubber\BadRequestException("Missing email or password in request");
        }
        $state->response->noCache();
        $state->session->destroy();
        $user = User::findByCredentials($state->request->post['email'], $state->request->post['password']);
        if (!$user) {
            throw new \Fubber\UnauthorizedException("Incorrect email or password provided", 131802);
        }
        if (!$user->email_confirmed) {
            throw new \Fubber\UnauthorizedException("E-mail address not confirmed", 273892);
        }
        if (!$user->isEnabled()) {
            throw new \Fubber\UnauthorizedException("User account has not been activated.", 312512);
        }
        $result = [ 'result' => $user->login($state), 'user_id' => $user->id ];
        return $state->json($result);
    }

    public static function logout(State $state) {
        $state->response->noCache();
        if(!isset($state->get['done'])) {
            $state->session->destroy();
            return $state->redirect('/logout/?done=1');
        }
        return $state->view('account/logged-out');
    }
    
    public static function me_edit(State $state) {
        $state->response->noCache();
        if(!$state->user) throw new NotLoggedInException();
        $form = new UserForm($state, $state->user);
        if($form->success) {
            new FlashMessage($state, trans('Your details were updated'), FlashMessage::POSITIVE);
            return $state->redirect('/me/');
        }
        return $state->view('account/me_edit', [ 'user' => $state->user, 'form' => $form ]);
    }

    public static function admin_users(State $state) {
        $state->response->noCache();
        if(!$state->user) throw new NotLoggedInException();
        if(!$state->user->is_admin) throw new \Fubber\AccessDeniedException();
        return $state->view('account/admin/users');
    }

    public static function admin_users_impersonate(State $state, $userId) {
        $state->response->noCache();
        if(!$state->user) throw new NotLoggedInException();
        if(!$state->user->is_admin) throw new \Fubber\AccessDeniedException();
        $user = User::load($userId);
        $user->login($state);
        return $state->redirect('/');
    }

    public static function admin_users_edit(State $state, $userId) {
        $state->response->noCache();
        if(!$state->user) throw new NotLoggedInException();
        if(!$state->user->is_admin) throw new \Fubber\AccessDeniedException();

        $user = User::load($userId);

        return $state->view('account/admin/user', ['user' => $user]);
//frode        return $state->view('account/me_edit', [ 'user' => $state->user, 'form' => $form ]);
    }
    
    public static function not_logged_in_exception(State $state, \Throwable $exception) {
        $state->response->noCache();
        $path = $state->request->getUri();
        $state->response->noCache();
        return $state->redirect(
            '/login/?not_logged_in=1&continue_to='.rawurlencode($path),
            \Fubber\Redirect::TEMPORARY_REDIRECT, 'Login at', [
                'Cache-Control' => 'no-store, max-age=0, s-maxage=0',
            ]
        );
    }

    public static function access_denied_exception(State $state, $exception) {
        $state->access->assertLoggedIn();
        new FlashMessage($state, trans("You don't have access to the requested resource."), FlashMessage::POSITIVE);
        $path = $state->request->getUri();
        return $state->redirect('/login/?access_denied=1&continue_to='.rawurlencode($path));
    }

    public static function set_password_from_mail(State $state) {
        $state->response->noCache();
        if(!$state->getRequest()->getUrl()->quickCheck())
            throw new AccessDeniedException("Request must be signed");
        if($state->get['expires'] < time())
            throw new AccessDeniedException("Recovery e-mail has expired");
        $user = User::load($state->get['user_id']);
        if(!$user) throw new Exceptions\NotFoundException("That user was not found");
        $form = new SetPasswordForm($state, $user);
        if($form->success) {
            return $state->redirect("/login/");
        }
        return $state->view('account/set-password', ['form' => $form]);
    }
}
