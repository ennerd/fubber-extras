<?php
namespace EnnerdUserMgmt;
use User;

class SetPasswordForm extends \Fubber\Forms\Form {
    public function __construct(State $state, User $user) {
        parent::__construct($state, 'set-password-form', [
            'password' => '',
            'password_confirm' => '',
            ]);

        if($this->accept()) {
            if($this->isInvalid()) {
                new \Fubber\FlashMessage($state, 'Validation errors', \Fubber\FlashMessage::ERROR);
            } else {
                $user->password = $this->password;
                $this->success = $user->save();
            }
        }
    }

    public function isInvalid(): ?array {
        $errors = new \Fubber\Util\Errors($this);
        $errors->required('password');
        $errors->required('password_confirm');
        if($this->password != $this->password_confirm)
            $errors->addError('password_confirm', 'Passwords don\'t match');
        return $errors->isInvalid();
    }
}
