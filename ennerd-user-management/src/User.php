<?php

use Fubber\FubberAPI;
use Fubber\Kernel\State;
use Fubber\Table;
use Fubber\Hooks;
use Fubber\UnauthorizedException;
use Fubber\Mail;
use Fubber\Template;
use Fubber\Logger;
use Fubber\NotAuthenticatedException;

use function Fubber\trans;

/* @leavebehind */

/* @leavebehind */

class User extends Table {
    use FubberAPI;
    use \Fubber\Util\OptionsTrait;
    
    protected static $_optionsTable = 'user_options'; // for example users_options
    protected static $_optionsFK = 'user_id'; // for example 'user_id'
    
    public static $_table = 'users';
    public static $_cols = ['id','first_name','last_name','email','email_confirmed','password','last_login','is_admin'];

    public static function create(State $state=null) {
        $user = new static();
        $user->is_admin = 0;
        return $user;
    }
    
    public static function logger() {
        return new Logger('User');
    }

    /**
     * Returns meta data about the table, to be used for building user interfaces
     * and presenting data.
     */
    public static function meta(State $state) {

        return [
            'names' => [
                'plural' => trans('User Accounts'),
                'singular' => trans("User Account"),
                ],
            'cols' => [
                'id' => [
                    'caption' => trans('ID'),
                    'type' => 'primarykey',
                    'readonly' => true,
                    ],
                'first_name' => [
                    'caption' => trans('Given Name'),
                    'type' => 'text',
                    ],
                'last_name' => [
                    'caption' => trans('Family Name'),
                    'type' => 'text',
                    ],
                'email' => [
                    'caption' => trans('E-mail address'),
                    'type' => 'email',
                    ],
                'email_confirmed' => [
                    'caption' => trans('E-mail address confirmed'),
                    'type' => 'checkbox',
                    ],
                'password' => [
                    'caption' => trans('Password'),
                    'type' => 'password',
                    ],
                'last_login' => [
                    'caption' => trans('Last Login'),
                    'type' => 'datetime',
                    'readonly' => true,
                    ],
                'is_admin' => [
                    'caption' => trans('Global System Admin'),
                    'type' => 'checkbox',
                    // What are these, and what did they do?
                    // 'boolean' => true,
                    // 'default' => 0,
                    ],
                ],
            ];
    }

    public function isEnabled(): bool {
        // Hook is for backward compatability
        $result = Hooks::filter(static::class.'::isEnabled', !!$this->email_confirmed, $this);
        return Hooks::filter(__METHOD__.'()', $result, $this);
    }

    public function canView(State $state) {
        return $state->access->isSuperAdmin() || $state->user_id==$this->id;
    }

    public function canEdit(State $state) {
        if ($this->id === $state->user_id) {
            return true;
        }
        return $state->access->isSuperAdmin();
    }

    public function canDelete(State $state) {
        return $state->access->isSuperAdmin();
    }

    public static function canList(State $state) {
        return $state->access->isSuperAdmin();
    }

    public static function canManage(State $state) {
        return $state->access->isSuperAdmin();
    }

    public static function canCreate(State $state) {
        return !$state->access->isLoggedIn() || $state->access->isSuperAdmin();
    }

    public function jsonSerialize() {
        $res = [
            'id' => intval($this->id),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'last_login' => $this->last_login ? date('c', strtotime($this->last_login)) : null,
            'is_admin' => !!$this->is_admin,
            '_string' => $this->first_name.' '.$this->last_name,
            ];
        return Hooks::filter(static::class.'::jsonSerialize()', $res, $this);
    }

    public $id, $first_name, $last_name, $email, $email_confirmed=0, $password, $last_login, $is_admin=0;

    public function isSuperAdmin() {
        return Hooks::filter(__METHOD__.'()', $this->isEnabled() && !!$this->is_admin, $this);
        return $this->isEnabled() && !!$this->is_admin;
    }

    public static function findByCredentials($email, $password) {
        $user = self::all()->where('email','=',$email)->one();
        if(!$user) return null;

        // Check the password
        if($user->checkPassword($password)) {
            return $user;
        }
        return null;
    }
    
    public function isInvalid(): ?array {
        $errors = new Fubber\Util\Errors($this);
        $errors->required('first_name');
        $errors->required('last_name');
        $errors->required('email');
        $errors->required('password');
        $errors->password('password');
        $errors->email('email');
        $errors->unique('email', static::class, $this->id);
        $errors->oneOf('email_confirmed', [1, 0]);
        $errors->oneOf('is_admin', [0, 1]);
        if(!$this->id) {
            // Check that e-mail does not already exist
            $count = User::all()->where('email','=',$this->email)->count();
            if($count>0) {
                $errors->addError('email','E-mail address '.$this->email.' already registered');
            }
        }
        return $errors->isInvalid();
    }

    public static function getCurrent(State $state): ?static {
        if(isset($state->cache['currentUser'])) {
            return $state->cache['currentUser']->isEnabled() ? $state->cache['currentUser'] : null;
        }

        if ($user = Hooks::filter(static::class.'::getCurrent', null, $state)) {
            $state->cache['currentUser'] = $user;
            return $user->isEnabled() ? $user : null;
        }

        if(!$state->session->userId) {
            return null;
        }
        $state->cache['currentUser'] = self::load($state->session->userId);
        if ($state->cache['currentUser'] && $state->cache['currentUser']->isEnabled()) {
            return $state->cache['currentUser'];
        }
        return null;
    }

    public static function requireCurrent(State $state) {
        $user = static::getCurrent($state);
        if( !$user ) throw new UnauthorizedException('Not logged in');
        return $user;
    }
    
    public function login(State $state) {
        if (!$this->isEnabled()) {
            return false;
        }
        $state->session->userId = $this->id;
        $state->cache['currentUser'] = $this;
        $this->last_login = gmdate('Y-m-d H:i:s');
        $this->save(['last_login']);
/*
//        $db = \Fubber\Service::use(\Fubber\Db\IDb::class);
//        $db->exec('UPDATE users SET last_login=? WHERE id=?', [$this->last_login, $this->id]);
*/
        static::logger()->info('User :id (email=:email) logged in', [
            'id' => $this->id,
            'email' => $this->email,
            ]);
        return true;
    }
    
    public function getDisplayName() {
        return $this->first_name.' '.$this->last_name;
    }
    
    public function sendPasswordRecoveryEmail(State $state) {
        $url = Fubber\Util\Url::create()->setPath("/login/set-password/")->setParam('expires', (time()+86400))->setParam('user_id', $this->id);
        $url->quickSign();
        $mail = new Fubber\Mail('theme/mails/account/password-recovery', ['url' => $url, 'user' => $this]);
        $mail->setSubject(trans('Password Recovery'));
        static::logger()->info('Sent password recovery e-mail to :email for user :id', [
            'id' => $this->id,
            'email' => $this->email,
            ]);
        return $mail->send($this->email, $this->getDisplayName());
    }
    
    public function sendActivationEmail() {
        $url = Fubber\Util\Url::create()->setPath("/me/".$this->id."/activation/")->setParam('expires', (time()+86400));
        $url->quickSign();
        $mail = new Fubber\Mail('theme/mails/user-account-activation', ["url" => $url, "user" => $this]);
        $mail->setSubject(trans('Account Activation'));
        static::logger()->info('Sent activation e-mail to :email for user :id', [
            'id' => $this->id,
            'email' => $this->email,
            ]);
        return $mail->send($this->email, $this->getDisplayName());
    }

    public function checkPassword($password) {
        /*
        if ($password === $this->password) {
            $this->save(['password']);
            return true;
        }
        */
        $res = password_verify($password, $this->password);
        if(!$res) {
            static::logger()->warning("Incorrect password for user :id (email=:email)", [
                'id' => $this->id,
                'email' => $this->email,
                ]);
        }
        return $res;
    }
    
    public function doConfirm() {
        // This user account is now being confirmed
        if($this->email_confirmed) {
            return false;
        }

        $this->email_confirmed = 1;
        if($this->save(['email_confirmed'])) {
            Hooks::dispatch(__METHOD__, $this);
            return true;
        }
        $this->email_confirmed = 0;
        return false;
    }

    public function save(array $properties = null): bool {
        $isNew = empty($this->id);

        // Make sure password gets encrypted before save
        if ($properties === null || in_array('password', $properties)) {
            $oldPassword = null;
            if ($isNew) {
                if (strlen($this->password) > 30 && substr($this->password, 0, 1) === '$') {
                    // password is already hashed
                } else {
                    $this->password = password_hash($this->password, PASSWORD_DEFAULT);
                }
                static::logger()->info('Created user :id (email=:email)', [
                    'id' => $this->id,
                    'first_name' => $this->first_name,
                    'last_name' => $this->last_name,
                    'email' => $this->email,
                    ]);
                $this->sendActivationEmail();
            } else {
                $oldPassword = \Fubber\Db::create()->queryField('SELECT password FROM users WHERE id=?', [$this->id]);

                if (empty($this->password)) {
                    $this->password = $oldPassword;
                } elseif ( $this->password === $oldPassword && $this->password[0] === '$') {
                    // simply the old password being saved again
                } elseif (strlen($this->password) > 30 && substr($this->password, 0, 1) === '$') {
                    // password was already hashed                
                } elseif ( !password_verify($this->password, $oldPassword) ) {
                    // seems to be a changed password
                    $this->password = password_hash($this->password, PASSWORD_DEFAULT);
                    static::logger()->info('Password changed for user :id (email=:email)', [
                        'id' => $this->id,
                        'email' => $this->email,
                        ]);
                }
                static::logger()->info('Details changed for user :id (email=:email)', [
                    'id' => $this->id,
                    'email' => $this->email,
                ]);
            }
        }

        $res = parent::save($properties);
        return Hooks::filter(__METHOD__.'()', $res, $this);
    }

    public function getTimezone() {
        return new DateTimeZone( 'Europe/Oslo' );
    }

    public function getDateTimeFormat() {
        return 'Y-m-d H:i:s';
    }

    public function getLocaleCode() {
        return 'nb-NO';
    }
}
