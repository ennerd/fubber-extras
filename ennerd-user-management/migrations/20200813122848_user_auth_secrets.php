<?php

use Phinx\Migration\AbstractMigration;

class UserAuthSecrets extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('user_accesstokens')
            ->addColumn('user_id', 'integer', ['comment' => json_encode(User::class)])

            ->addColumn('name', 'string', ['limit' => 100, 'comment' => json_encode([
                'caption' => '!!Token Name',
                'type' => 'text',
            ])])

            ->addColumn('token', 'string', ['limit' => 100, 'comment' => json_encode([
                'caption' => '!!Token String',
                'type' => 'text',
            ])])

            ->addColumn('is_enabled', 'integer', ['limit' => 1, 'comment' => json_encode([
                'caption' => '!!Enabled',
                'type' => 'checkbox',
                'default' => 1,
            ])])

            ->addColumn('issued_date', 'datetime', ['comment' => json_encode('created_date')])

            ->addColumn('expiration_date', 'datetime', ['null' => true, 'comment' => json_encode([
                'caption' => '!!Expiration Date',
                'type' => 'datetime',
            ])])

            ->addColumn('privileges', 'text', ['limit' => 'TEXT_MEDIUM', 'comment' => json_encode([
                'caption' => '!!Privileges',
                'type' => 'textarea',
            ])])

            ->addIndex(['user_id','name'], ['unique' => true])
            ->addForeignKey('user_id','users','id',['delete' => 'CASCADE','update' => 'CASCADE'])
            ->create();
    }
}
