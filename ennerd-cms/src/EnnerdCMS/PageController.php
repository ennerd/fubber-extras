<?php
namespace EnnerdCMS;
use Theme\CRUDController;

use function Fubber\route;
use function Fubber\trans;

class PageController extends CRUDController {
    public static bool $enabled = false;

    public static function init() {
        if (!static::_getVal('enabled')) {
            return;
        }
        parent::init();
        \Fubber\Hooks::listen('Theme.ControlPanel.Icons', [ static::class, 'theme_controlpanel_icons' ], 100);
    }
    
    public static function theme_controlpanel_icons( $icons, $state ) {
	if($state->access(Models\Page::class)->hasPrivilege('List')) {
            $icons[] = \Theme::icon($state, static::getMeta($state)->getPluralName(), '<i class="fas fa-4x fa-book-open red"></i>', [
                "onclick" => "location.href='".route(static::_getVal('prefix'))."';"
            ]);
	}
        return $icons;
    }
    

    public static function model() {
        return Models\Page::class;
    }
    public static function prefix() {
        return 'admin/pages';
    }
    public static function enable_rest_api() {
        return true;
    }
    public static function index_cols(State $state) {
        return [
            'id' => ['left', 100],
            'title' => ['left'],
            'created_date' => ['left', 250,],
            'created_by' => ['left', 250],
            ];
    }
    public static function create_def(State $state) {
        return [
            ['field' ,'title'],
            ['field', 'slug'],
            ['accordion', 
                [
                    'title' => trans("Excerpt"),
                    ['field', 'excerpt'],
                    ],
                [
                    'title' => trans("Content"),
                    'active' => true,
                    ['field', 'content'],
                    ],
                [
                    'title' => trans("Meta"),
                    ['cols', ['field', 'created_by'], ['field', 'created_date'],],
                    ['field', 'weight'],
                    ],
                ],
            ];
    }
    public static function edit_def(State $state) {
        return [
            ['field' ,'title'],
            ['field', 'slug'],
            ['accordion',
                [
                    'title' => trans("Excerpt"),
                    ['field', 'excerpt'],
                    ],
                [
                    'title' => trans("Content"),
                    'active' => true,
                    ['field', 'content'],
                    ],
                [
                    'title' => trans("Meta"),
                    ['cols', ['field', 'created_by'], ['field', 'created_date'],],
                    ['field', 'weight'],
                    ],
                ],
            ];
    }
    public static function view_def(State $state) {
        return [
            ['field' ,'title'],
            ['field', 'slug'],
            ['accordion',
                [
                    'title' => trans("Excerpt"),
                    ['field', 'excerpt'],
                    ],
                [
                    'title' => trans("Content"),
                    'active' => true,
                    ['field', 'content'],
                    ],
                [
                    'title' => trans("Meta"),
                    ['cols', ['field', 'created_by'], ['field', 'created_date'],],
                    ['cols', ['field', 'modified_by'], ['field', 'modified_date'],],
                    ['field', 'weight'],
                    ],
                ],
            ];
    }
}
