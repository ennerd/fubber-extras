<?php
namespace EnnerdCMS\Models;

use Fubber\Kernel\State;

use function Fubber\trans;

class Page extends \Fubber\Table\Backends\DbTable {
    public static $_table = 'cms_pages';
    public static $_cols = ['id','title','content','excerpt','slug','weight','created_date','created_by','modified_date','modified_by','module','status','parent_id'];
    public static $_foreignKeys = ['created_by' => \User::class, 'modified_by' => \User::class, 'parent_id' => Page::class];
    
    
    const STATUS_DRAFT = 'draft';
    
    public static function create($state=null) {
        $res = new static();
        $res->weight = 0;
        if($state) {
            $res->created_by = $state->user_id;
            $res->created_date = gmdate('Y-m-d H:i:s');
            $res->modified_date = $res->created_date;
            $res->status = static::STATUS_DRAFT;
        }
        return $res;
    }
    
    public static function meta(State $state) {
        return [
            'names' => [
                'singular' => trans("Page"),
                'plural' => trans("Pages"),
                ],
            'description' => trans("Articles, contact forms, archive pages and other content entries on your website."),
            'cols' => [
                'id' => 'id',
                'title' => [
                    'caption' => trans("Title"),
                    'type' => 'text',
                    ],
                'excerpt' => [
                    'caption' => trans("Excerpt"),
                    'type' => 'textarea',
                    ],
                'content' => [
                    'caption' => trans("Content"),
                    'type' => 'htmlarea',
                    ],
                'slug' => [
                    'caption' => trans("Page URL name"),
                    'type' => 'text',
                    ],
                'weight' => [
                    'caption' => trans("Weight"),
                    'description' => trans("Increase or decrease the weight of the article, to push it up or down in listings."),
                    'type' => 'text',
                    ],
                'created_date' => 'created_date',
                'created_by' => 'created_by',
                'modified_date' => [
                    'caption' => trans('Modified Date'),
                    'type' => 'datetime',
                    ],
                'modified_by' => [
                    'caption' => trans("Modified By"),
                    'extends' => 'created_by',
                    ],
                'module' => [
                    'caption' => trans("Page Module"),
                    'type' => 'text',
                    ],
                'status' => [
                    'caption' => trans("Publish Status"),
                    'type' => 'text',
                    ],
                ],
            ];
    }
    public function canView(State $state) {
        return $state->access->isLoggedIn();
    }
    public function canEdit(State $state) {
        return $state->access->isSuperAdmin();
    }
    public function canDelete(State $state) {
        return $state->access->isSuperAdmin();
    }
    public static function canList(State $state) {
        return $state->access->isLoggedIn();
    }
    public static function canCreate(State $state) {
        return $state->access->isSuperAdmin();
    }
    public function isInvalid(): ?array {
        $e = new \Fubber\Util\Errors($this);
        $e->required('title');
        $e->required('slug');
        $e->slug('slug');
        return $e->isInvalid();
    }
    public function jsonSerialize() {
        $userFilesUrl = \Fubber\Kernel::init()->config['userfiles_url'];
        $urlParts = parse_url($userFilesUrl);
        $res = parent::jsonSerialize();
        $content = $res['content'];
        $o = 0;
        $find = [];
        $replace = [];
        $matches = [];
        while($o < strlen($content)) {
                $next = strpos($content, '"'.$urlParts['path'], $o);
                if($next !== false) {
                        $end = strpos($content, '"', $next+1);
                        if($end === false) break;
                        $match = substr($content, $next+1, $end-$next-1);
                        $o = $end+1;
                        $matches[] = $match;
                } else {
                        break;
                }
        }

        foreach($matches as $match) {
                $path = substr($match, strlen($urlParts['path'])+1);
                $extension = pathinfo($path, PATHINFO_EXTENSION);
                $file = \EnnerdFileMgmt\Models\File::loadByPath($path);
                if($file) {
                        $find[] = $match.'"';
                        $replace[] = $match.'@w=1200&h=1200.'.strtolower($extension).'"';
                }

                $content = str_replace($find, $replace, $content);
        }

        $res['content'] = $content;
        return $res;
    }
}
