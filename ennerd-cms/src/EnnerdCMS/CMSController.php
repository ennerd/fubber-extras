<?php
namespace EnnerdCMS;
use Fubber\Hooks;

class CMSController {
    public static function init() {
        Hooks::listen('Fubber\Forms\Form.Method.htmlarea', [static::class, 'form_htmlarea']);
        Hooks::listen('Theme\Meta\Filter.Html.htmlarea', [static::class, 'htmlfilter_htmlarea']);
    }
    
    public static function htmlfilter_htmlarea($value, $state, $object, $property) {
        //var_dump($value);die();
        return $value.'<!-- htmlfilter_htmlarea -->';
    }
    
    public static function form_htmlarea(State $state, \Fubber\Forms\Form $form, $name, $attrs) {
        $state->stylesheets['tinymce'] = '/app/extra/ennerd-cms/assets/tinymce.css';
        $state->scripts['tinymce'] = '/app/extra/ennerd-cms/assets/tinymce-dist/tinymce.min.js';
        $bu = \Fubber\Kernel::init()->config['base_url'];
        $tinyMceOptions = [
            'selector' => '#'.$form->getId($name),
            'theme' => 'modern',
            'branding' => false,
            'style' => 'border-width: 0',
            'plugins' => 'image contextmenu code link lists advlist',
            //'toolbar' => 'image',
            'image_advtab' => true,
            'contextmenu' => 'link image inserttable | cell row column deletetable',
            'document_base_url' => $bu,
            'relative_urls' => false,
            ];
        $state->inline_scripts[] = '
        (function(opts) {
            opts.file_browser_callback = function(field_name, url, type, win) {
                Theme.Dialog.open("/admin/files/pick-dialog/?accept=" + encodeURIComponent("image/jpeg,image/png,image/gif"), "tall", function(res) {
                    var url = res._url;
                    var t = url.indexOf("//" + document.location.host);
                    if(t>0) {
                        t = url.indexOf("/", t+3);
                        url = url.substring(t);
                    }
                    win.document.getElementById(field_name).value = url;
                });
            };
            tinymce.init(opts);
        })('.json_encode($tinyMceOptions).');
        ';
        return $form->textarea($name, ['class' => 'cms-htmlarea']);
    }
}