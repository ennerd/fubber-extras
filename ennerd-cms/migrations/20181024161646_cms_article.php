<?php


use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CmsArticle extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('cms_pages')
            ->addColumn('title', 'text')
            ->addColumn('content', 'text', ['limit' => MysqlAdapter::TEXT_LONG])
            ->addColumn('excerpt', 'text')
            ->addColumn('slug', 'string', ['limit' => 200])
            ->addColumn('weight', 'integer', ['default' => 0])
            ->addColumn('created_date', 'datetime')
            ->addColumn('created_by', 'integer')
            ->addColumn('modified_date', 'datetime')
            ->addColumn('modified_by', 'integer', ['null' => true])
            ->addColumn('module', 'string', ['limit' => 100, 'null' => true])
            ->addColumn('status', 'string', ['limit' => 20] )
            ->addColumn('parent_id', 'integer', ['null' => true])
            ->addIndex(['parent_id', 'slug'], ['unique' => true])
            ->addIndex(['created_by'])
            ->addIndex(['modified_by'])
            ->addIndex(['parent_id'])
            ->addForeignKey('created_by', 'users', 'id', ['update' => 'CASCADE', 'delete' => 'RESTRICT'])
            ->addForeignKey('modified_by', 'users', 'id', ['update' => 'CASCADE', 'delete' => 'SET_NULL'])
            ->addForeignKey('parent_id', 'cms_pages', 'id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->create();
            
        $this->table('cms_pages_meta')
            ->addColumn('page_id', 'integer')
            ->addColumn('name', 'string', ['limit' => 50])
            ->addColumn('value', 'blob', ['limit' => MysqlAdapter::BLOB_MEDIUM])
            ->addIndex(['page_id','name'], ['unique' => true])
            ->addForeignKey('page_id','cms_pages','id',['delete' => 'CASCADE','update' => 'CASCADE'])
            ->create();
    }
}
