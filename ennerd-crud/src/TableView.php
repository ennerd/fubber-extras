<?php
namespace Fubber\CRUD;

use Fubber\Psr\HttpMessage\Response;
use Fubber\Table\ITable;
use Fubber\Table\ITableSet;
use Fubber\View;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class TableView implements RequestHandlerInterface {

    protected ITableSet $table;

    public function __construct(ITableSet $table) {
        $this->table = $table;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface {
        return new View("fubber-crud/table", [ 'table' => $this->table ]);
    }

}