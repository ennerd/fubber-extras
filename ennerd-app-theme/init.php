<?php
use Fubber\Kernel;

/**
 * Create a dynamic table source for this query, which allows the current user to
 * access the data through ajax.
 * 
 * $cols = [
 *      "id" => [ 'ColTitle', 'left|right|center', 200], // Column title, text align, width in px. Arg 2 and 3 are optional
 *      ];
 */
function tablesource(\Fubber\Table\IBasicTableSet $table, array $cols, array $joins=[]) {
    $spec = [
        'cols' => $cols,
        'records' => $table,
        'joins' => $joins,
        ];
    $hashBase = serialize($spec);
    $salt = Kernel::init()->env->salt;
    $hash = hash_hmac('sha1', $hashBase, $salt);
    $cache = \Fubber\Cache::create();
    $cache->set($hash, $spec, 3600);
    return '/api/tablesource/'.$hash.'/';
}
Fubber\Hooks::listen('Theme.TableSource', function($data, $state, $name) {
    if($data)
        return $data;
    $cache = \Fubber\Cache::create();
    $data = $cache->get($name);
    return $data;
}, 1000);

$this->addController(\EnnerdAppTheme\ThemeController::class);
