function SimpleBind(object) {
    let res = new Proxy(object, SimpleBind);
    SimpleBind
}
SimpleBind.set = (target, property, value) => {
    target[property] = value;
    // Tiny optimization: The browser will filter the elements to only those that contain the property name
    let selector = '*[data-fubber-bind*="' + property + '"]';
    let els = document.querySelectorAll(selector);
    for(let i = els.length - 1; i >= 0; i--) {
        let el = els[i];
        SimpleBind._eval.call(el, el.getAttribute('data-fubber-bind'));
    }
};
SimpleBind.initialBinding = () => {
    let selector = '*[data-fubber-bind]';
    let els = document.querySelectorAll(selector);
    for(let i = els.length - 1; i >= 0; i--) {
        let el = els[i];
        SimpleBind._eval.call(el, el.getAttribute('data-fubber-bind'));
    }
};
SimpleBind._eval = function(code) {
    return eval(code);
};
