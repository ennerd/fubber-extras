import theme from "./assets/theme/index.js";
import Theme from "./assets/theme/Theme.js";

window.ennerdTheme = theme;

declare global {
    interface Window {
        ennerdTheme: Theme;
    }
}