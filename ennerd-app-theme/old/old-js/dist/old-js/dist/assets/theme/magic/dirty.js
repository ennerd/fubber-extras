"use strict";
define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function setupDirtyFormHandling() {
        $("body").on("change", "form input,select,textarea", function (e) {
            Theme.isDirty = true;
            $(this).addClass('dirty');
        });
        window.onbeforeunload = (event) => {
            if (Theme.isDirty) {
                event.preventDefault();
                event.returnValue = "Sure?";
            }
        };
        $(document.body).on('click', 'tr[data-href]', function (e) {
            if (e.target.tagName == "TD") {
                location.href = $(this).attr('data-href');
            }
        });
    }
    exports.default = setupDirtyFormHandling;
});
//# sourceMappingURL=dirty.js.map
//# sourceMappingURL=dirty.js.map