"use strict";
define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function setupMagicAttributes() {
        $(document.body).on('click', 'tr[data-href]', function (e) {
            if (e.target.tagName == "TD") {
                location.href = $(this).attr('data-href');
            }
        });
    }
    exports.default = setupMagicAttributes;
});
//# sourceMappingURL=magic-attributes.js.map
//# sourceMappingURL=magic-attributes.js.map