"use strict";
define(["./dialog", "require"], (Dialog, require) => {
    class Theme {
        construct(window) {
            this.window = window;
            this.dialog = new Dialog(this);
        }
        get Dialog() {
            return this.dialog;
        }
    }
    return new Theme(window);
});
//# sourceMappingURL=main-javascript-version.js.map