"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "./Theme.js"], function (require, exports, Theme_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    Theme_js_1 = __importDefault(Theme_js_1);
    exports.default = new Theme_js_1.default(window);
});
//# sourceMappingURL=index.js.map
//# sourceMappingURL=index.js.map