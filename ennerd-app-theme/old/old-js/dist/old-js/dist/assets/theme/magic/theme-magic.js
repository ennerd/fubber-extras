"use strict";
define(["require", "exports", "../../utils.js"], function (require, exports, utils_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.setupRepeater = exports.setupRendered = exports.setupAccordion = exports.setupDataPicker = exports.setupThemeMagic = exports.setupDirtyFormHandling = exports.setupTabHandling = void 0;
    /**
     * Handle tabs via url hash
     */
    function setupTabHandling(theme) {
        var activateTab = function (el) {
            var $tabs = $(el);
            $tabs.find('a.tab').each(function () {
                var _a;
                var $this = $(this);
                var id = (_a = $this.attr('href')) === null || _a === void 0 ? void 0 : _a.split("#")[1];
                if (!id)
                    return;
                id = id.split(":")[1];
                if (!$this.hasClass('active')) {
                    $(document.getElementById(id)).hide();
                }
                else {
                    $(document.getElementById(id)).show();
                }
            });
        };
        $(".tabs").each(function () {
            activateTab(this);
        });
        var updateAccordingToHash = function () {
            var _a;
            var hash = document.location.hash;
            var $activated = null;
            for (const el of $(".tabs .tab")) {
                if ($(el).attr('href') == hash) {
                    $activated = $(el);
                }
            }
            if ($activated) {
                $activated.parent().find('.tab').each(function () {
                    var _a;
                    const $this = $(this);
                    $this.removeClass('active');
                    var id = (_a = $this.attr('href')) === null || _a === void 0 ? void 0 : _a.split("#")[1];
                    if (id) {
                        id = id.split(":")[1];
                        if (id) {
                            $("#" + id).hide();
                        }
                    }
                });
                $activated.addClass('active');
                var id = (_a = $activated.attr('href')) === null || _a === void 0 ? void 0 : _a.split("#")[1];
                if (id) {
                    id = id.split(":")[1];
                    if (id) {
                        $("#" + id).show();
                    }
                }
            }
        };
        $(updateAccordingToHash);
        $(window).on('hashchange', updateAccordingToHash);
    }
    exports.setupTabHandling = setupTabHandling;
    var dirtyFormHandlingInitialized = false;
    function setupDirtyFormHandling(theme) {
        if (dirtyFormHandlingInitialized) {
            return;
        }
        dirtyFormHandlingInitialized = true;
        /**
         * Notice when stuff changes
         */
        $("body").on("change", "form input, form select, form textarea", function (e) {
            theme.formIsDirty = true;
            $(this).addClass('dirty');
        });
        /**
         * Notice when form is being submitted
         */
        $("body").on('submit', "form", function (e) {
            let found = false;
            $(this).find(".dirty").each((index, element) => {
                found = true;
            });
            if (found) {
                $(".dirty").removeClass("dirty");
                theme.formIsDirty = false;
            }
        });
        /**
         * Notice when leaving page that is dirty
         */
        window.onbeforeunload = (event) => {
            if (theme.formIsDirty) {
                event.preventDefault();
                event.returnValue = "Sure?";
            }
        };
    }
    exports.setupDirtyFormHandling = setupDirtyFormHandling;
    var themeMagicInitialized = false;
    function setupThemeMagic() {
        if (themeMagicInitialized) {
            return;
        }
        themeMagicInitialized = true;
        $(document.body).on('click', 'tr[data-href]', function (e) {
            if (e.target.tagName == "TD") {
                window.location.href = $(this).attr('data-href');
            }
        });
        $(window).on('submit', function () {
            $('form').on('submit', function () {
                return false;
            });
        });
        $(window).on('resize', function () {
            if ((0, utils_js_1.isMobile)()) {
                $(document.body).addClass('mobile');
            }
            else {
                $(document.body).removeClass('mobile');
            }
        });
        $(window).on('scroll', function () {
            var st = $(window).scrollTop();
            if (st == 0) {
                $(document.body).removeClass('scrolled');
            }
            else {
                $(document.body).addClass('scrolled');
            }
        });
    }
    exports.setupThemeMagic = setupThemeMagic;
    var dataPickerInitialized = false;
    function setupDataPicker(theme) {
        $("input[data-picker]:not(.eap-init").each(function () {
            var _a, _b, _c;
            $(document.body).on('click', '*[data-view-route]', function () {
                var _a;
                var $this = $(this);
                var $hidden = $this.parent().find('*[data-the-hidden]').first();
                var hiddenVal = $hidden === null || $hidden === void 0 ? void 0 : $hidden.val();
                if (hiddenVal && hiddenVal != '') {
                    var url = (_a = $this.attr('data-view-route')) === null || _a === void 0 ? void 0 : _a.replace(':id', hiddenVal);
                    theme.dialogs.open(url, 'tall', function (res) {
                        console.log(res);
                    });
                }
            });
            var $this = $(this);
            $this.attr('readonly', "readonly");
            var $hidden = $('<input />').attr('data-the-hidden', 'true').attr('type', 'hidden');
            $hidden.attr('name', (_a = $this.attr('name')) !== null && _a !== void 0 ? _a : '');
            $hidden.val((_b = $this.val()) !== null && _b !== void 0 ? _b : '');
            if ($this.attr('data-current-string') == '' && $this.val() != '')
                $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
            else
                $this.val((_c = $this.attr('data-current-string')) !== null && _c !== void 0 ? _c : '');
            $this.removeAttr('name');
            $this.after($hidden);
            $this.addClass('eap-init');
            var $button = $("<button />").html('<i class="fas fa-search"></i>').addClass('f-picker').attr('type', 'button');
            $this.before($button);
            if ($this.attr('data-creater')) {
                var $createButton = $("<button />").html('<i class="fas fa-plus"></i>').addClass('f-creater').attr('type', 'button');
                $(document.body).on('click', '.f-creater', function () {
                    var $this = $(this).parent().find('*[data-creater]').first();
                    var $hidden = $(this).parent().find('*[data-the-hidden]').first();
                    theme.dialogs.open($this.attr('data-creater'), 'tall', function (res) {
                        if (res) {
                            if (!res['_string'])
                                $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
                            else
                                $this.val(res._string);
                            $hidden.val(res.id);
                        }
                    });
                });
                $this.before($createButton);
            }
        });
        if (!dataPickerInitialized) {
            dataPickerInitialized = true;
            $(document.body).on('click', '.f-picker', function () {
                var $this = $(this).parent().find('*[data-picker]').first();
                var $hidden = $(this).parent().find('*[data-the-hidden]').first();
                theme.dialogs.open($this.attr('data-picker'), 'tall', function (res) {
                    if (res) {
                        if (!res['_string'])
                            $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
                        else
                            $this.val(res._string);
                        $hidden.val(res.id);
                    }
                });
            });
        }
    }
    exports.setupDataPicker = setupDataPicker;
    var accordionInitialized = false;
    function setupAccordion() {
        if (accordionInitialized) {
            return;
        }
        accordionInitialized = true;
        jQuery(function ($) {
            $(document.body).on('click', '.rendered-accordion-title', function () {
                var $this = $(this);
                var $pane = $this.parent('.rendered-accordion-pane');
                $pane.toggleClass('rendered-accordion-active');
            });
        });
        (0, utils_js_1.loadCss)(utils_js_1.baseURL + '/assets/css/theme-accordion.css');
    }
    exports.setupAccordion = setupAccordion;
    var renderedInitialized = false;
    function setupRendered() {
        if (renderedInitialized) {
            return;
        }
        renderedInitialized = true;
        (0, utils_js_1.loadCss)(utils_js_1.baseURL + '/assets/css/theme-rendered.css');
    }
    exports.setupRendered = setupRendered;
    var repeaterInitialized = false;
    function setupRepeater(theme) {
        if (!repeaterInitialized) {
            (0, utils_js_1.loadCss)(utils_js_1.baseURL + '/assets/css/theme-repeater.css');
        }
        repeaterInitialized = true;
        jQuery(function ($) {
            $(".rendered-repeater > div.rendered-repeater-blocks").sortable({ axis: "y", handle: ".rendered-repeater-handle", update: function () {
                } });
            $(document.body).on('focus mousedown', '.rendered-repeater-adder *', function () {
                //if($(this).is('button')) return false;
                var $template = $(this).closest('.rendered-repeater-adder').find(".rendered-repeater-template");
                var $newTemplate = $template.clone();
                $template.removeClass('rendered-repeater-template');
                $newTemplate.addClass('rendered-repeater-template');
                $newTemplate.insertAfter($template);
                $template.appendTo($(this).closest('.rendered-repeater').find('.rendered-repeater-blocks').first());
                $(this).focus();
                //renumber($(this).closest('form')[0]);
                setTimeout(function () {
                    theme.init();
                }, 50);
                return false;
            });
            var trashTimeout;
            $(document.body).on('click', '.rendered-repeater-trash-button', function () {
                var $this = $(this);
                if ($this.is('.must-confirm')) {
                    $this.closest('.rendered-repeater-block').remove();
                    return;
                }
                $this.closest('.rendered-repeater').find('.must-confirm').each(function () {
                    $(this).removeClass('must-confirm');
                    $(this).find('i').removeClass('fa-question').addClass('fa-trash');
                });
                $this.find('i').removeClass('fa-trash').addClass('fa-question');
                $this.addClass('must-confirm');
                clearTimeout(trashTimeout);
                trashTimeout = setTimeout(function () {
                    if ($this.is('.must-confirm')) {
                        $this.removeClass('must-confirm');
                        $this.find('i').removeClass('fa-question').addClass('fa-trash');
                    }
                }, 2000);
            });
            $(document.body).on('submit', function () {
                $(this).find('input[type=checkbox]').each(function () {
                    var $this = $(this);
                    var checked = this.checked;
                    $(this).attr('data-was-checkbox', this.value);
                    $(this).attr('type', 'text');
                    this.value = checked ? this.value : '';
                });
                var $template = $(".rendered-repeater-template");
                var $parent = $template.parent();
                $template.remove();
                setTimeout(function () {
                    $parent.append($template);
                    $(document.body).find('input[data-was-checkbox]').each(function () {
                        var $this = $(this);
                        var checked = this.value != '';
                        this.value = $this.attr('data-was-checkbox');
                        $(this).removeAttr('data-was-checkbox');
                        $(this).attr('type', 'checkbox');
                        this.checked = checked;
                    });
                }, 0);
                return true;
            });
        });
    }
    exports.setupRepeater = setupRepeater;
});
//# sourceMappingURL=theme-magic.js.map
//# sourceMappingURL=theme-magic.js.map