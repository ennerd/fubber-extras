"use strict";
define(["require", "exports", "../utils.js"], function (require, exports, utils_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function setupThemeMagic() {
        $(document.body).on('click', 'tr[data-href]', function (e) {
            if (e.target.tagName == "TD") {
                window.location.href = $(this).attr('data-href');
            }
        });
        $(window).on('submit', function () {
            $('form').on('submit', function () {
                return false;
            });
        });
        $(window).on('resize', function () {
            if ((0, utils_js_1.isMobile)()) {
                $(document.body).addClass('mobile');
            }
            else {
                $(document.body).removeClass('mobile');
            }
        });
        $(window).on('scroll', function () {
            var st = $(window).scrollTop();
            if (st == 0) {
                $(document.body).removeClass('scrolled');
            }
            else {
                $(document.body).addClass('scrolled');
            }
        });
    }
    exports.default = setupThemeMagic;
});
//# sourceMappingURL=theme-magic.js.map
//# sourceMappingURL=theme-magic.js.map