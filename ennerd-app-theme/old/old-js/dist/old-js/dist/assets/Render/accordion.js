"use strict";
jQuery(function ($) {
    $(document.body).on('click', '.rendered-accordion-title', function () {
        var $this = $(this);
        var $pane = $this.parent('.rendered-accordion-pane');
        $pane.toggleClass('rendered-accordion-active');
    });
});
//# sourceMappingURL=accordion.js.map