"use strict";
define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.baseURL = exports.isMobile = exports.parseString = exports.loadCss = void 0;
    function loadCss(path) {
        const el = document.createElement('link');
        el.setAttribute('href', path);
        el.setAttribute('rel', 'stylesheet');
        el.setAttribute('type', 'text/css');
        document.head.appendChild(el);
    }
    exports.loadCss = loadCss;
    ;
    function parseString(pattern, vars) {
        var res = '' + pattern;
        for (var i in vars) {
            res = res.replace('{' + i + '}', vars[i]);
        }
        return res;
    }
    exports.parseString = parseString;
    function isMobile() {
        return !window.matchMedia("(min-width: 700px)").matches;
    }
    exports.isMobile = isMobile;
    ;
    exports.baseURL = "/app/extra/ennerd-app-theme";
});
//# sourceMappingURL=utils.js.map
//# sourceMappingURL=utils.js.map