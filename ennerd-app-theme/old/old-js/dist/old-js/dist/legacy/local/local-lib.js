"use strict";
var Lib = {
    Util: {
        /**
         * Use this when you want to display user times in the same time zone that the server does.
         * The resulting Date object is invalid (has the wrong time, but will render the correct times).
         */
        fixTimezone: function (date) {
            return new Date(date.getTime() + date.getTimezoneOffset() * 60000 - timezoneOffset * 60000);
        },
        currentTime: function () {
            return new Date(new Date().getTime() + timeInfo.server.getTime() - timeInfo.client.getTime());
        },
        hhmmss: function (date) {
            var y = date.getFullYear(), m = date.getMonth(), d = date.getDate(), h = date.getHours(), i = date.getMinutes(), s = date.getSeconds();
            var fix = function (t) {
                if (t < 10)
                    return "0" + t;
                return t;
            };
            return fix(h) + ":" + fix(i) + ":" + fix(s);
        }
    }
};
//# sourceMappingURL=local-lib.js.map