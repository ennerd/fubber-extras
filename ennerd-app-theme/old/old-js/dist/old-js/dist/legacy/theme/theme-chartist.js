"use strict";
jQuery(function ($) {
    $('.f-chart').each(function () {
        var $this = $(this);
        var type = $this.attr('data-type');
        var data = JSON.parse($this.attr('data-data'));
        var options = JSON.parse($this.attr('data-options'));
        console.log(type, data, options);
        Chartist[type](this, data, options);
    });
});
//# sourceMappingURL=theme-chartist.js.map