"use strict";
if (typeof CCL === "undefined")
    CCL = {};
if (typeof CCL.logEvent === "undefined")
    (function () {
        var uniqueID = (new Date()).getTime() + "-" + Math.floor(Math.random() * 10000);
        var l = 100; // max number of events to send per page view
        var p = null; // previous event
        function send(data) {
            if (l-- <= 0)
                return;
            var url = location.protocol + '//events.companycast.live/l?' + encodeURIComponent(JSON.stringify(data));
            // don't send same event twice in a row
            if (url === p)
                return;
            p = url;
            var s = document.createElement('SCRIPT');
            s.setAttribute('src', url);
            document.body.appendChild(s);
            setTimeout(function () {
                s.parentElement.removeChild(s);
            }, 0);
        }
        CCL.logEvent = function (name, data) {
            var eventData = { e: name, d: data, uniqueID: uniqueID };
            send(eventData);
        };
        window.addEventListener('error', function (e) {
            CCL.logEvent('error', {
                url: location.href,
                title: document.title,
                f: e.filename,
                l: e.lineno,
                m: e.message,
                t: e.type
            });
        });
        window.addEventListener('unload', function () { CCL.logEvent('unload'); });
        var first = {
            t: document.title,
            u: location.href,
            a: navigator.userAgent,
            ref: document.referrer
        };
        CCL.logEvent('page-view', first);
    })();
//# sourceMappingURL=events.js.map