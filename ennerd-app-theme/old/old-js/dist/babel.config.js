"use strict";
module.exports = {
    "presets": [
        ["@babel/preset-env", {
                modules: true
            }]
    ],
    "plugins": [
    /*
            ["babel-plugin-transform-async-to-promises", {
                inlineHelpers: true
            }],
    */
    /*
            ["transform-vue-template", {
            }],
            {
                // fix imports that does not request a .js file
                visitor: {
                    ImportDeclaration: function (path, state) {
                        if (
                            path.node.source.value.substr(-3).toLowerCase() != '.js' &&
                            path.node.source.value.substr(-4).toLowerCase() != '.mjs'
                        ) {
                            if (path.node.source.value.indexOf("/") == -1) {
                                path.node.source.value = '/app/unpkg.com/' + path.node.source.value + '/main.js';
                            } else {
                                path.node.source.value += ".js";
                            }
                        }
                    }
                }
            }
    */
    ]
};
//# sourceMappingURL=babel.config.js.map