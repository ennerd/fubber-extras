var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "./assets/theme/index.js"], function (require, exports, index_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    index_js_1 = __importDefault(index_js_1);
    window.ennerdTheme = index_js_1.default;
});
//# sourceMappingURL=index.js.map