"use strict";
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) { return typeof obj; };
}
else {
    _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
} return _typeof(obj); }
var luxon = function (exports) {
    'use strict';
    function _defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor)
                descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    function _createClass(Constructor, protoProps, staticProps) {
        if (protoProps)
            _defineProperties(Constructor.prototype, protoProps);
        if (staticProps)
            _defineProperties(Constructor, staticProps);
        return Constructor;
    }
    function _inheritsLoose(subClass, superClass) {
        subClass.prototype = Object.create(superClass.prototype);
        subClass.prototype.constructor = subClass;
        subClass.__proto__ = superClass;
    }
    function _getPrototypeOf(o) {
        _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
            return o.__proto__ || Object.getPrototypeOf(o);
        };
        return _getPrototypeOf(o);
    }
    function _setPrototypeOf(o, p) {
        _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
            o.__proto__ = p;
            return o;
        };
        return _setPrototypeOf(o, p);
    }
    function isNativeReflectConstruct() {
        if (typeof Reflect === "undefined" || !Reflect.construct)
            return false;
        if (Reflect.construct.sham)
            return false;
        if (typeof Proxy === "function")
            return true;
        try {
            Date.prototype.toString.call(Reflect.construct(Date, [], function () { }));
            return true;
        }
        catch (e) {
            return false;
        }
    }
    function _construct(Parent, args, Class) {
        if (isNativeReflectConstruct()) {
            _construct = Reflect.construct;
        }
        else {
            _construct = function _construct(Parent, args, Class) {
                var a = [null];
                a.push.apply(a, args);
                var Constructor = Function.bind.apply(Parent, a);
                var instance = new Constructor();
                if (Class)
                    _setPrototypeOf(instance, Class.prototype);
                return instance;
            };
        }
        return _construct.apply(null, arguments);
    }
    function _isNativeFunction(fn) {
        return Function.toString.call(fn).indexOf("[native code]") !== -1;
    }
    function _wrapNativeSuper(Class) {
        var _cache = typeof Map === "function" ? new Map() : undefined;
        _wrapNativeSuper = function _wrapNativeSuper(Class) {
            if (Class === null || !_isNativeFunction(Class))
                return Class;
            if (typeof Class !== "function") {
                throw new TypeError("Super expression must either be null or a function");
            }
            if (typeof _cache !== "undefined") {
                if (_cache.has(Class))
                    return _cache.get(Class);
                _cache.set(Class, Wrapper);
            }
            function Wrapper() {
                return _construct(Class, arguments, _getPrototypeOf(this).constructor);
            }
            Wrapper.prototype = Object.create(Class.prototype, {
                constructor: {
                    value: Wrapper,
                    enumerable: false,
                    writable: true,
                    configurable: true
                }
            });
            return _setPrototypeOf(Wrapper, Class);
        };
        return _wrapNativeSuper(Class);
    } // these aren't really private, but nor are they really useful to document
    /**
     * @private
     */
    var LuxonError = 
    /*#__PURE__*/
    function (_Error) {
        _inheritsLoose(LuxonError, _Error);
        function LuxonError() {
            return _Error.apply(this, arguments) || this;
        }
        return LuxonError;
    }(_wrapNativeSuper(Error));
    /**
     * @private
     */
    var InvalidDateTimeError = 
    /*#__PURE__*/
    function (_LuxonError) {
        _inheritsLoose(InvalidDateTimeError, _LuxonError);
        function InvalidDateTimeError(reason) {
            return _LuxonError.call(this, "Invalid DateTime: " + reason.toMessage()) || this;
        }
        return InvalidDateTimeError;
    }(LuxonError);
    /**
     * @private
     */
    var InvalidIntervalError = 
    /*#__PURE__*/
    function (_LuxonError2) {
        _inheritsLoose(InvalidIntervalError, _LuxonError2);
        function InvalidIntervalError(reason) {
            return _LuxonError2.call(this, "Invalid Interval: " + reason.toMessage()) || this;
        }
        return InvalidIntervalError;
    }(LuxonError);
    /**
     * @private
     */
    var InvalidDurationError = 
    /*#__PURE__*/
    function (_LuxonError3) {
        _inheritsLoose(InvalidDurationError, _LuxonError3);
        function InvalidDurationError(reason) {
            return _LuxonError3.call(this, "Invalid Duration: " + reason.toMessage()) || this;
        }
        return InvalidDurationError;
    }(LuxonError);
    /**
     * @private
     */
    var ConflictingSpecificationError = 
    /*#__PURE__*/
    function (_LuxonError4) {
        _inheritsLoose(ConflictingSpecificationError, _LuxonError4);
        function ConflictingSpecificationError() {
            return _LuxonError4.apply(this, arguments) || this;
        }
        return ConflictingSpecificationError;
    }(LuxonError);
    /**
     * @private
     */
    var InvalidUnitError = 
    /*#__PURE__*/
    function (_LuxonError5) {
        _inheritsLoose(InvalidUnitError, _LuxonError5);
        function InvalidUnitError(unit) {
            return _LuxonError5.call(this, "Invalid unit " + unit) || this;
        }
        return InvalidUnitError;
    }(LuxonError);
    /**
     * @private
     */
    var InvalidArgumentError = 
    /*#__PURE__*/
    function (_LuxonError6) {
        _inheritsLoose(InvalidArgumentError, _LuxonError6);
        function InvalidArgumentError() {
            return _LuxonError6.apply(this, arguments) || this;
        }
        return InvalidArgumentError;
    }(LuxonError);
    /**
     * @private
     */
    var ZoneIsAbstractError = 
    /*#__PURE__*/
    function (_LuxonError7) {
        _inheritsLoose(ZoneIsAbstractError, _LuxonError7);
        function ZoneIsAbstractError() {
            return _LuxonError7.call(this, "Zone is an abstract class") || this;
        }
        return ZoneIsAbstractError;
    }(LuxonError);
    /**
     * @private
     */
    var n = "numeric", s = "short", l = "long";
    var DATE_SHORT = {
        year: n,
        month: n,
        day: n
    };
    var DATE_MED = {
        year: n,
        month: s,
        day: n
    };
    var DATE_FULL = {
        year: n,
        month: l,
        day: n
    };
    var DATE_HUGE = {
        year: n,
        month: l,
        day: n,
        weekday: l
    };
    var TIME_SIMPLE = {
        hour: n,
        minute: n
    };
    var TIME_WITH_SECONDS = {
        hour: n,
        minute: n,
        second: n
    };
    var TIME_WITH_SHORT_OFFSET = {
        hour: n,
        minute: n,
        second: n,
        timeZoneName: s
    };
    var TIME_WITH_LONG_OFFSET = {
        hour: n,
        minute: n,
        second: n,
        timeZoneName: l
    };
    var TIME_24_SIMPLE = {
        hour: n,
        minute: n,
        hour12: false
    };
    /**
     * {@link toLocaleString}; format like '09:30:23', always 24-hour.
     */
    var TIME_24_WITH_SECONDS = {
        hour: n,
        minute: n,
        second: n,
        hour12: false
    };
    /**
     * {@link toLocaleString}; format like '09:30:23 EDT', always 24-hour.
     */
    var TIME_24_WITH_SHORT_OFFSET = {
        hour: n,
        minute: n,
        second: n,
        hour12: false,
        timeZoneName: s
    };
    /**
     * {@link toLocaleString}; format like '09:30:23 Eastern Daylight Time', always 24-hour.
     */
    var TIME_24_WITH_LONG_OFFSET = {
        hour: n,
        minute: n,
        second: n,
        hour12: false,
        timeZoneName: l
    };
    /**
     * {@link toLocaleString}; format like '10/14/1983, 9:30 AM'. Only 12-hour if the locale is.
     */
    var DATETIME_SHORT = {
        year: n,
        month: n,
        day: n,
        hour: n,
        minute: n
    };
    /**
     * {@link toLocaleString}; format like '10/14/1983, 9:30:33 AM'. Only 12-hour if the locale is.
     */
    var DATETIME_SHORT_WITH_SECONDS = {
        year: n,
        month: n,
        day: n,
        hour: n,
        minute: n,
        second: n
    };
    var DATETIME_MED = {
        year: n,
        month: s,
        day: n,
        hour: n,
        minute: n
    };
    var DATETIME_MED_WITH_SECONDS = {
        year: n,
        month: s,
        day: n,
        hour: n,
        minute: n,
        second: n
    };
    var DATETIME_MED_WITH_WEEKDAY = {
        year: n,
        month: s,
        day: n,
        weekday: s,
        hour: n,
        minute: n
    };
    var DATETIME_FULL = {
        year: n,
        month: l,
        day: n,
        hour: n,
        minute: n,
        timeZoneName: s
    };
    var DATETIME_FULL_WITH_SECONDS = {
        year: n,
        month: l,
        day: n,
        hour: n,
        minute: n,
        second: n,
        timeZoneName: s
    };
    var DATETIME_HUGE = {
        year: n,
        month: l,
        day: n,
        weekday: l,
        hour: n,
        minute: n,
        timeZoneName: l
    };
    var DATETIME_HUGE_WITH_SECONDS = {
        year: n,
        month: l,
        day: n,
        weekday: l,
        hour: n,
        minute: n,
        second: n,
        timeZoneName: l
    };
    /*
      This is just a junk drawer, containing anything used across multiple classes.
      Because Luxon is small(ish), this should stay small and we won't worry about splitting
      it up into, say, parsingUtil.js and basicUtil.js and so on. But they are divided up by feature area.
    */
    /**
     * @private
     */
    // TYPES
    function isUndefined(o) {
        return typeof o === "undefined";
    }
    function isNumber(o) {
        return typeof o === "number";
    }
    function isInteger(o) {
        return typeof o === "number" && o % 1 === 0;
    }
    function isString(o) {
        return typeof o === "string";
    }
    function isDate(o) {
        return Object.prototype.toString.call(o) === "[object Date]";
    } // CAPABILITIES
    function hasIntl() {
        try {
            return typeof Intl !== "undefined" && Intl.DateTimeFormat;
        }
        catch (e) {
            return false;
        }
    }
    function hasFormatToParts() {
        return !isUndefined(Intl.DateTimeFormat.prototype.formatToParts);
    }
    function hasRelative() {
        try {
            return typeof Intl !== "undefined" && !!Intl.RelativeTimeFormat;
        }
        catch (e) {
            return false;
        }
    } // OBJECTS AND ARRAYS
    function maybeArray(thing) {
        return Array.isArray(thing) ? thing : [thing];
    }
    function bestBy(arr, by, compare) {
        if (arr.length === 0) {
            return undefined;
        }
        return arr.reduce(function (best, next) {
            var pair = [by(next), next];
            if (!best) {
                return pair;
            }
            else if (compare(best[0], pair[0]) === best[0]) {
                return best;
            }
            else {
                return pair;
            }
        }, null)[1];
    }
    function pick(obj, keys) {
        return keys.reduce(function (a, k) {
            a[k] = obj[k];
            return a;
        }, {});
    }
    function hasOwnProperty(obj, prop) {
        return Object.prototype.hasOwnProperty.call(obj, prop);
    } // NUMBERS AND STRINGS
    function integerBetween(thing, bottom, top) {
        return isInteger(thing) && thing >= bottom && thing <= top;
    } // x % n but takes the sign of n instead of x
    function floorMod(x, n) {
        return x - n * Math.floor(x / n);
    }
    function padStart(input, n) {
        if (n === void 0) {
            n = 2;
        }
        if (input.toString().length < n) {
            return ("0".repeat(n) + input).slice(-n);
        }
        else {
            return input.toString();
        }
    }
    function parseInteger(string) {
        if (isUndefined(string) || string === null || string === "") {
            return undefined;
        }
        else {
            return parseInt(string, 10);
        }
    }
    function parseMillis(fraction) {
        // Return undefined (instead of 0) in these cases, where fraction is not set
        if (isUndefined(fraction) || fraction === null || fraction === "") {
            return undefined;
        }
        else {
            var f = parseFloat("0." + fraction) * 1000;
            return Math.floor(f);
        }
    }
    function roundTo(number, digits, towardZero) {
        if (towardZero === void 0) {
            towardZero = false;
        }
        var factor = Math.pow(10, digits), rounder = towardZero ? Math.trunc : Math.round;
        return rounder(number * factor) / factor;
    } // DATE BASICS
    function isLeapYear(year) {
        return year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0);
    }
    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }
    function daysInMonth(year, month) {
        var modMonth = floorMod(month - 1, 12) + 1, modYear = year + (month - modMonth) / 12;
        if (modMonth === 2) {
            return isLeapYear(modYear) ? 29 : 28;
        }
        else {
            return [31, null, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][modMonth - 1];
        }
    } // covert a calendar object to a local timestamp (epoch, but with the offset baked in)
    function objToLocalTS(obj) {
        var d = Date.UTC(obj.year, obj.month - 1, obj.day, obj.hour, obj.minute, obj.second, obj.millisecond); // for legacy reasons, years between 0 and 99 are interpreted as 19XX; revert that
        if (obj.year < 100 && obj.year >= 0) {
            d = new Date(d);
            d.setUTCFullYear(d.getUTCFullYear() - 1900);
        }
        return +d;
    }
    function weeksInWeekYear(weekYear) {
        var p1 = (weekYear + Math.floor(weekYear / 4) - Math.floor(weekYear / 100) + Math.floor(weekYear / 400)) % 7, last = weekYear - 1, p2 = (last + Math.floor(last / 4) - Math.floor(last / 100) + Math.floor(last / 400)) % 7;
        return p1 === 4 || p2 === 3 ? 53 : 52;
    }
    function untruncateYear(year) {
        if (year > 99) {
            return year;
        }
        else
            return year > 60 ? 1900 + year : 2000 + year;
    } // PARSING
    function parseZoneInfo(ts, offsetFormat, locale, timeZone) {
        if (timeZone === void 0) {
            timeZone = null;
        }
        var date = new Date(ts), intlOpts = {
            hour12: false,
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit"
        };
        if (timeZone) {
            intlOpts.timeZone = timeZone;
        }
        var modified = Object.assign({
            timeZoneName: offsetFormat
        }, intlOpts), intl = hasIntl();
        if (intl && hasFormatToParts()) {
            var parsed = new Intl.DateTimeFormat(locale, modified).formatToParts(date).find(function (m) {
                return m.type.toLowerCase() === "timezonename";
            });
            return parsed ? parsed.value : null;
        }
        else if (intl) {
            // this probably doesn't work for all locales
            var without = new Intl.DateTimeFormat(locale, intlOpts).format(date), included = new Intl.DateTimeFormat(locale, modified).format(date), diffed = included.substring(without.length), trimmed = diffed.replace(/^[, \u200e]+/, "");
            return trimmed;
        }
        else {
            return null;
        }
    } // signedOffset('-5', '30') -> -330
    function signedOffset(offHourStr, offMinuteStr) {
        var offHour = parseInt(offHourStr, 10); // don't || this because we want to preserve -0
        if (Number.isNaN(offHour)) {
            offHour = 0;
        }
        var offMin = parseInt(offMinuteStr, 10) || 0, offMinSigned = offHour < 0 || Object.is(offHour, -0) ? -offMin : offMin;
        return offHour * 60 + offMinSigned;
    } // COERCION
    function asNumber(value) {
        var numericValue = Number(value);
        if (typeof value === "boolean" || value === "" || Number.isNaN(numericValue))
            throw new InvalidArgumentError("Invalid unit value " + value);
        return numericValue;
    }
    function normalizeObject(obj, normalizer, nonUnitKeys) {
        var normalized = {};
        for (var u in obj) {
            if (hasOwnProperty(obj, u)) {
                if (nonUnitKeys.indexOf(u) >= 0)
                    continue;
                var v = obj[u];
                if (v === undefined || v === null)
                    continue;
                normalized[normalizer(u)] = asNumber(v);
            }
        }
        return normalized;
    }
    function formatOffset(offset, format) {
        var hours = Math.trunc(offset / 60), minutes = Math.abs(offset % 60), sign = hours >= 0 && !Object.is(hours, -0) ? "+" : "-", base = "" + sign + Math.abs(hours);
        switch (format) {
            case "short":
                return "" + sign + padStart(Math.abs(hours), 2) + ":" + padStart(minutes, 2);
            case "narrow":
                return minutes > 0 ? base + ":" + minutes : base;
            case "techie":
                return "" + sign + padStart(Math.abs(hours), 2) + padStart(minutes, 2);
            default:
                throw new RangeError("Value format " + format + " is out of range for property format");
        }
    }
    function timeObject(obj) {
        return pick(obj, ["hour", "minute", "second", "millisecond"]);
    }
    var ianaRegex = /[A-Za-z_+-]{1,256}(:?\/[A-Za-z_+-]{1,256}(\/[A-Za-z_+-]{1,256})?)?/;
    function stringify(obj) {
        return JSON.stringify(obj, Object.keys(obj).sort());
    }
    /**
     * @private
     */
    var monthsLong = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var monthsShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var monthsNarrow = ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"];
    function months(length) {
        switch (length) {
            case "narrow":
                return monthsNarrow;
            case "short":
                return monthsShort;
            case "long":
                return monthsLong;
            case "numeric":
                return ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            case "2-digit":
                return ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
            default:
                return null;
        }
    }
    var weekdaysLong = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    var weekdaysShort = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
    var weekdaysNarrow = ["M", "T", "W", "T", "F", "S", "S"];
    function weekdays(length) {
        switch (length) {
            case "narrow":
                return weekdaysNarrow;
            case "short":
                return weekdaysShort;
            case "long":
                return weekdaysLong;
            case "numeric":
                return ["1", "2", "3", "4", "5", "6", "7"];
            default:
                return null;
        }
    }
    var meridiems = ["AM", "PM"];
    var erasLong = ["Before Christ", "Anno Domini"];
    var erasShort = ["BC", "AD"];
    var erasNarrow = ["B", "A"];
    function eras(length) {
        switch (length) {
            case "narrow":
                return erasNarrow;
            case "short":
                return erasShort;
            case "long":
                return erasLong;
            default:
                return null;
        }
    }
    function meridiemForDateTime(dt) {
        return meridiems[dt.hour < 12 ? 0 : 1];
    }
    function weekdayForDateTime(dt, length) {
        return weekdays(length)[dt.weekday - 1];
    }
    function monthForDateTime(dt, length) {
        return months(length)[dt.month - 1];
    }
    function eraForDateTime(dt, length) {
        return eras(length)[dt.year < 0 ? 0 : 1];
    }
    function formatRelativeTime(unit, count, numeric, narrow) {
        if (numeric === void 0) {
            numeric = "always";
        }
        if (narrow === void 0) {
            narrow = false;
        }
        var units = {
            years: ["year", "yr."],
            quarters: ["quarter", "qtr."],
            months: ["month", "mo."],
            weeks: ["week", "wk."],
            days: ["day", "day", "days"],
            hours: ["hour", "hr."],
            minutes: ["minute", "min."],
            seconds: ["second", "sec."]
        };
        var lastable = ["hours", "minutes", "seconds"].indexOf(unit) === -1;
        if (numeric === "auto" && lastable) {
            var isDay = unit === "days";
            switch (count) {
                case 1:
                    return isDay ? "tomorrow" : "next " + units[unit][0];
                case -1:
                    return isDay ? "yesterday" : "last " + units[unit][0];
                case 0:
                    return isDay ? "today" : "this " + units[unit][0];
                default: // fall through
            }
        }
        var isInPast = Object.is(count, -0) || count < 0, fmtValue = Math.abs(count), singular = fmtValue === 1, lilUnits = units[unit], fmtUnit = narrow ? singular ? lilUnits[1] : lilUnits[2] || lilUnits[1] : singular ? units[unit][0] : unit;
        return isInPast ? fmtValue + " " + fmtUnit + " ago" : "in " + fmtValue + " " + fmtUnit;
    }
    function formatString(knownFormat) {
        // these all have the offsets removed because we don't have access to them
        // without all the intl stuff this is backfilling
        var filtered = pick(knownFormat, ["weekday", "era", "year", "month", "day", "hour", "minute", "second", "timeZoneName", "hour12"]), key = stringify(filtered), dateTimeHuge = "EEEE, LLLL d, yyyy, h:mm a";
        switch (key) {
            case stringify(DATE_SHORT):
                return "M/d/yyyy";
            case stringify(DATE_MED):
                return "LLL d, yyyy";
            case stringify(DATE_FULL):
                return "LLLL d, yyyy";
            case stringify(DATE_HUGE):
                return "EEEE, LLLL d, yyyy";
            case stringify(TIME_SIMPLE):
                return "h:mm a";
            case stringify(TIME_WITH_SECONDS):
                return "h:mm:ss a";
            case stringify(TIME_WITH_SHORT_OFFSET):
                return "h:mm a";
            case stringify(TIME_WITH_LONG_OFFSET):
                return "h:mm a";
            case stringify(TIME_24_SIMPLE):
                return "HH:mm";
            case stringify(TIME_24_WITH_SECONDS):
                return "HH:mm:ss";
            case stringify(TIME_24_WITH_SHORT_OFFSET):
                return "HH:mm";
            case stringify(TIME_24_WITH_LONG_OFFSET):
                return "HH:mm";
            case stringify(DATETIME_SHORT):
                return "M/d/yyyy, h:mm a";
            case stringify(DATETIME_MED):
                return "LLL d, yyyy, h:mm a";
            case stringify(DATETIME_FULL):
                return "LLLL d, yyyy, h:mm a";
            case stringify(DATETIME_HUGE):
                return dateTimeHuge;
            case stringify(DATETIME_SHORT_WITH_SECONDS):
                return "M/d/yyyy, h:mm:ss a";
            case stringify(DATETIME_MED_WITH_SECONDS):
                return "LLL d, yyyy, h:mm:ss a";
            case stringify(DATETIME_MED_WITH_WEEKDAY):
                return "EEE, d LLL yyyy, h:mm a";
            case stringify(DATETIME_FULL_WITH_SECONDS):
                return "LLLL d, yyyy, h:mm:ss a";
            case stringify(DATETIME_HUGE_WITH_SECONDS):
                return "EEEE, LLLL d, yyyy, h:mm:ss a";
            default:
                return dateTimeHuge;
        }
    }
    function stringifyTokens(splits, tokenToString) {
        var s = "";
        for (var _iterator = splits, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
            var _ref;
            if (_isArray) {
                if (_i >= _iterator.length)
                    break;
                _ref = _iterator[_i++];
            }
            else {
                _i = _iterator.next();
                if (_i.done)
                    break;
                _ref = _i.value;
            }
            var token = _ref;
            if (token.literal) {
                s += token.val;
            }
            else {
                s += tokenToString(token.val);
            }
        }
        return s;
    }
    var _macroTokenToFormatOpts = {
        D: DATE_SHORT,
        DD: DATE_MED,
        DDD: DATE_FULL,
        DDDD: DATE_HUGE,
        t: TIME_SIMPLE,
        tt: TIME_WITH_SECONDS,
        ttt: TIME_WITH_SHORT_OFFSET,
        tttt: TIME_WITH_LONG_OFFSET,
        T: TIME_24_SIMPLE,
        TT: TIME_24_WITH_SECONDS,
        TTT: TIME_24_WITH_SHORT_OFFSET,
        TTTT: TIME_24_WITH_LONG_OFFSET,
        f: DATETIME_SHORT,
        ff: DATETIME_MED,
        fff: DATETIME_FULL,
        ffff: DATETIME_HUGE,
        F: DATETIME_SHORT_WITH_SECONDS,
        FF: DATETIME_MED_WITH_SECONDS,
        FFF: DATETIME_FULL_WITH_SECONDS,
        FFFF: DATETIME_HUGE_WITH_SECONDS
    };
    /**
     * @private
     */
    var Formatter = 
    /*#__PURE__*/
    function () {
        Formatter.create = function create(locale, opts) {
            if (opts === void 0) {
                opts = {};
            }
            return new Formatter(locale, opts);
        };
        Formatter.parseFormat = function parseFormat(fmt) {
            var current = null, currentFull = "", bracketed = false;
            var splits = [];
            for (var i = 0; i < fmt.length; i++) {
                var c = fmt.charAt(i);
                if (c === "'") {
                    if (currentFull.length > 0) {
                        splits.push({
                            literal: bracketed,
                            val: currentFull
                        });
                    }
                    current = null;
                    currentFull = "";
                    bracketed = !bracketed;
                }
                else if (bracketed) {
                    currentFull += c;
                }
                else if (c === current) {
                    currentFull += c;
                }
                else {
                    if (currentFull.length > 0) {
                        splits.push({
                            literal: false,
                            val: currentFull
                        });
                    }
                    currentFull = c;
                    current = c;
                }
            }
            if (currentFull.length > 0) {
                splits.push({
                    literal: bracketed,
                    val: currentFull
                });
            }
            return splits;
        };
        Formatter.macroTokenToFormatOpts = function macroTokenToFormatOpts(token) {
            return _macroTokenToFormatOpts[token];
        };
        function Formatter(locale, formatOpts) {
            this.opts = formatOpts;
            this.loc = locale;
            this.systemLoc = null;
        }
        var _proto = Formatter.prototype;
        _proto.formatWithSystemDefault = function formatWithSystemDefault(dt, opts) {
            if (this.systemLoc === null) {
                this.systemLoc = this.loc.redefaultToSystem();
            }
            var df = this.systemLoc.dtFormatter(dt, Object.assign({}, this.opts, opts));
            return df.format();
        };
        _proto.formatDateTime = function formatDateTime(dt, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var df = this.loc.dtFormatter(dt, Object.assign({}, this.opts, opts));
            return df.format();
        };
        _proto.formatDateTimeParts = function formatDateTimeParts(dt, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var df = this.loc.dtFormatter(dt, Object.assign({}, this.opts, opts));
            return df.formatToParts();
        };
        _proto.resolvedOptions = function resolvedOptions(dt, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var df = this.loc.dtFormatter(dt, Object.assign({}, this.opts, opts));
            return df.resolvedOptions();
        };
        _proto.num = function num(n, p) {
            if (p === void 0) {
                p = 0;
            } // we get some perf out of doing this here, annoyingly
            if (this.opts.forceSimple) {
                return padStart(n, p);
            }
            var opts = Object.assign({}, this.opts);
            if (p > 0) {
                opts.padTo = p;
            }
            return this.loc.numberFormatter(opts).format(n);
        };
        _proto.formatDateTimeFromString = function formatDateTimeFromString(dt, fmt) {
            var _this = this;
            var knownEnglish = this.loc.listingMode() === "en", useDateTimeFormatter = this.loc.outputCalendar && this.loc.outputCalendar !== "gregory" && hasFormatToParts(), string = function string(opts, extract) {
                return _this.loc.extract(dt, opts, extract);
            }, formatOffset = function formatOffset(opts) {
                if (dt.isOffsetFixed && dt.offset === 0 && opts.allowZ) {
                    return "Z";
                }
                return dt.isValid ? dt.zone.formatOffset(dt.ts, opts.format) : "";
            }, meridiem = function meridiem() {
                return knownEnglish ? meridiemForDateTime(dt) : string({
                    hour: "numeric",
                    hour12: true
                }, "dayperiod");
            }, month = function month(length, standalone) {
                return knownEnglish ? monthForDateTime(dt, length) : string(standalone ? {
                    month: length
                } : {
                    month: length,
                    day: "numeric"
                }, "month");
            }, weekday = function weekday(length, standalone) {
                return knownEnglish ? weekdayForDateTime(dt, length) : string(standalone ? {
                    weekday: length
                } : {
                    weekday: length,
                    month: "long",
                    day: "numeric"
                }, "weekday");
            }, maybeMacro = function maybeMacro(token) {
                var formatOpts = Formatter.macroTokenToFormatOpts(token);
                if (formatOpts) {
                    return _this.formatWithSystemDefault(dt, formatOpts);
                }
                else {
                    return token;
                }
            }, era = function era(length) {
                return knownEnglish ? eraForDateTime(dt, length) : string({
                    era: length
                }, "era");
            }, tokenToString = function tokenToString(token) {
                // Where possible: http://cldr.unicode.org/translation/date-time#TOC-Stand-Alone-vs.-Format-Styles
                switch (token) {
                    // ms
                    case "S":
                        return _this.num(dt.millisecond);
                    case "u": // falls through
                    case "SSS":
                        return _this.num(dt.millisecond, 3);
                    // seconds
                    case "s":
                        return _this.num(dt.second);
                    case "ss":
                        return _this.num(dt.second, 2);
                    // minutes
                    case "m":
                        return _this.num(dt.minute);
                    case "mm":
                        return _this.num(dt.minute, 2);
                    // hours
                    case "h":
                        return _this.num(dt.hour % 12 === 0 ? 12 : dt.hour % 12);
                    case "hh":
                        return _this.num(dt.hour % 12 === 0 ? 12 : dt.hour % 12, 2);
                    case "H":
                        return _this.num(dt.hour);
                    case "HH":
                        return _this.num(dt.hour, 2);
                    // offset
                    case "Z":
                        // like +6
                        return formatOffset({
                            format: "narrow",
                            allowZ: _this.opts.allowZ
                        });
                    case "ZZ":
                        // like +06:00
                        return formatOffset({
                            format: "short",
                            allowZ: _this.opts.allowZ
                        });
                    case "ZZZ":
                        // like +0600
                        return formatOffset({
                            format: "techie",
                            allowZ: false
                        });
                    case "ZZZZ":
                        // like EST
                        return dt.zone.offsetName(dt.ts, {
                            format: "short",
                            locale: _this.loc.locale
                        });
                    case "ZZZZZ":
                        // like Eastern Standard Time
                        return dt.zone.offsetName(dt.ts, {
                            format: "long",
                            locale: _this.loc.locale
                        });
                    // zone
                    case "z":
                        // like America/New_York
                        return dt.zoneName;
                    // meridiems
                    case "a":
                        return meridiem();
                    // dates
                    case "d":
                        return useDateTimeFormatter ? string({
                            day: "numeric"
                        }, "day") : _this.num(dt.day);
                    case "dd":
                        return useDateTimeFormatter ? string({
                            day: "2-digit"
                        }, "day") : _this.num(dt.day, 2);
                    // weekdays - standalone
                    case "c":
                        // like 1
                        return _this.num(dt.weekday);
                    case "ccc":
                        // like 'Tues'
                        return weekday("short", true);
                    case "cccc":
                        // like 'Tuesday'
                        return weekday("long", true);
                    case "ccccc":
                        // like 'T'
                        return weekday("narrow", true);
                    // weekdays - format
                    case "E":
                        // like 1
                        return _this.num(dt.weekday);
                    case "EEE":
                        // like 'Tues'
                        return weekday("short", false);
                    case "EEEE":
                        // like 'Tuesday'
                        return weekday("long", false);
                    case "EEEEE":
                        // like 'T'
                        return weekday("narrow", false);
                    // months - standalone
                    case "L":
                        // like 1
                        return useDateTimeFormatter ? string({
                            month: "numeric",
                            day: "numeric"
                        }, "month") : _this.num(dt.month);
                    case "LL":
                        // like 01, doesn't seem to work
                        return useDateTimeFormatter ? string({
                            month: "2-digit",
                            day: "numeric"
                        }, "month") : _this.num(dt.month, 2);
                    case "LLL":
                        // like Jan
                        return month("short", true);
                    case "LLLL":
                        // like January
                        return month("long", true);
                    case "LLLLL":
                        // like J
                        return month("narrow", true);
                    // months - format
                    case "M":
                        // like 1
                        return useDateTimeFormatter ? string({
                            month: "numeric"
                        }, "month") : _this.num(dt.month);
                    case "MM":
                        // like 01
                        return useDateTimeFormatter ? string({
                            month: "2-digit"
                        }, "month") : _this.num(dt.month, 2);
                    case "MMM":
                        // like Jan
                        return month("short", false);
                    case "MMMM":
                        // like January
                        return month("long", false);
                    case "MMMMM":
                        // like J
                        return month("narrow", false);
                    // years
                    case "y":
                        // like 2014
                        return useDateTimeFormatter ? string({
                            year: "numeric"
                        }, "year") : _this.num(dt.year);
                    case "yy":
                        // like 14
                        return useDateTimeFormatter ? string({
                            year: "2-digit"
                        }, "year") : _this.num(dt.year.toString().slice(-2), 2);
                    case "yyyy":
                        // like 0012
                        return useDateTimeFormatter ? string({
                            year: "numeric"
                        }, "year") : _this.num(dt.year, 4);
                    case "yyyyyy":
                        // like 000012
                        return useDateTimeFormatter ? string({
                            year: "numeric"
                        }, "year") : _this.num(dt.year, 6);
                    // eras
                    case "G":
                        // like AD
                        return era("short");
                    case "GG":
                        // like Anno Domini
                        return era("long");
                    case "GGGGG":
                        return era("narrow");
                    case "kk":
                        return _this.num(dt.weekYear.toString().slice(-2), 2);
                    case "kkkk":
                        return _this.num(dt.weekYear, 4);
                    case "W":
                        return _this.num(dt.weekNumber);
                    case "WW":
                        return _this.num(dt.weekNumber, 2);
                    case "o":
                        return _this.num(dt.ordinal);
                    case "ooo":
                        return _this.num(dt.ordinal, 3);
                    case "q":
                        // like 1
                        return _this.num(dt.quarter);
                    case "qq":
                        // like 01
                        return _this.num(dt.quarter, 2);
                    case "X":
                        return _this.num(Math.floor(dt.ts / 1000));
                    case "x":
                        return _this.num(dt.ts);
                    default:
                        return maybeMacro(token);
                }
            };
            return stringifyTokens(Formatter.parseFormat(fmt), tokenToString);
        };
        _proto.formatDurationFromString = function formatDurationFromString(dur, fmt) {
            var _this2 = this;
            var tokenToField = function tokenToField(token) {
                switch (token[0]) {
                    case "S":
                        return "millisecond";
                    case "s":
                        return "second";
                    case "m":
                        return "minute";
                    case "h":
                        return "hour";
                    case "d":
                        return "day";
                    case "M":
                        return "month";
                    case "y":
                        return "year";
                    default:
                        return null;
                }
            }, tokenToString = function tokenToString(lildur) {
                return function (token) {
                    var mapped = tokenToField(token);
                    if (mapped) {
                        return _this2.num(lildur.get(mapped), token.length);
                    }
                    else {
                        return token;
                    }
                };
            }, tokens = Formatter.parseFormat(fmt), realTokens = tokens.reduce(function (found, _ref2) {
                var literal = _ref2.literal, val = _ref2.val;
                return literal ? found : found.concat(val);
            }, []), collapsed = dur.shiftTo.apply(dur, realTokens.map(tokenToField).filter(function (t) {
                return t;
            }));
            return stringifyTokens(tokens, tokenToString(collapsed));
        };
        return Formatter;
    }();
    var Invalid = 
    /*#__PURE__*/
    function () {
        function Invalid(reason, explanation) {
            this.reason = reason;
            this.explanation = explanation;
        }
        var _proto = Invalid.prototype;
        _proto.toMessage = function toMessage() {
            if (this.explanation) {
                return this.reason + ": " + this.explanation;
            }
            else {
                return this.reason;
            }
        };
        return Invalid;
    }();
    /**
     * @interface
     */
    var Zone = 
    /*#__PURE__*/
    function () {
        function Zone() { }
        var _proto = Zone.prototype;
        /**
         * Returns the offset's common name (such as EST) at the specified timestamp
         * @abstract
         * @param {number} ts - Epoch milliseconds for which to get the name
         * @param {Object} opts - Options to affect the format
         * @param {string} opts.format - What style of offset to return. Accepts 'long' or 'short'.
         * @param {string} opts.locale - What locale to return the offset name in.
         * @return {string}
         */
        _proto.offsetName = function offsetName(ts, opts) {
            throw new ZoneIsAbstractError();
        };
        _proto.formatOffset = function formatOffset(ts, format) {
            throw new ZoneIsAbstractError();
        };
        _proto.offset = function offset(ts) {
            throw new ZoneIsAbstractError();
        };
        _proto.equals = function equals(otherZone) {
            throw new ZoneIsAbstractError();
        };
        _createClass(Zone, [{
                key: "type",
                /**
                 * The type of zone
                 * @abstract
                 * @type {string}
                 */
                get: function get() {
                    throw new ZoneIsAbstractError();
                }
                /**
                 * The name of this zone.
                 * @abstract
                 * @type {string}
                 */
            }, {
                key: "name",
                get: function get() {
                    throw new ZoneIsAbstractError();
                }
                /**
                 * Returns whether the offset is known to be fixed for the whole year.
                 * @abstract
                 * @type {boolean}
                 */
            }, {
                key: "universal",
                get: function get() {
                    throw new ZoneIsAbstractError();
                }
            }, {
                key: "isValid",
                get: function get() {
                    throw new ZoneIsAbstractError();
                }
            }]);
        return Zone;
    }();
    var singleton = null;
    /**
     * Represents the local zone for this Javascript environment.
     * @implements {Zone}
     */
    var LocalZone = 
    /*#__PURE__*/
    function (_Zone) {
        _inheritsLoose(LocalZone, _Zone);
        function LocalZone() {
            return _Zone.apply(this, arguments) || this;
        }
        var _proto = LocalZone.prototype;
        /** @override **/
        _proto.offsetName = function offsetName(ts, _ref) {
            var format = _ref.format, locale = _ref.locale;
            return parseZoneInfo(ts, format, locale);
        };
        _proto.formatOffset = function formatOffset$1(ts, format) {
            return formatOffset(this.offset(ts), format);
        };
        _proto.offset = function offset(ts) {
            return -new Date(ts).getTimezoneOffset();
        };
        _proto.equals = function equals(otherZone) {
            return otherZone.type === "local";
        };
        _createClass(LocalZone, [{
                key: "type",
                /** @override **/
                get: function get() {
                    return "local";
                }
                /** @override **/
            }, {
                key: "name",
                get: function get() {
                    if (hasIntl()) {
                        return new Intl.DateTimeFormat().resolvedOptions().timeZone;
                    }
                    else
                        return "local";
                }
                /** @override **/
            }, {
                key: "universal",
                get: function get() {
                    return false;
                }
            }, {
                key: "isValid",
                get: function get() {
                    return true;
                }
            }], [{
                key: "instance",
                /**
                 * Get a singleton instance of the local zone
                 * @return {LocalZone}
                 */
                get: function get() {
                    if (singleton === null) {
                        singleton = new LocalZone();
                    }
                    return singleton;
                }
            }]);
        return LocalZone;
    }(Zone);
    var matchingRegex = RegExp("^" + ianaRegex.source + "$");
    var dtfCache = {};
    function makeDTF(zone) {
        if (!dtfCache[zone]) {
            dtfCache[zone] = new Intl.DateTimeFormat("en-US", {
                hour12: false,
                timeZone: zone,
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit"
            });
        }
        return dtfCache[zone];
    }
    var typeToPos = {
        year: 0,
        month: 1,
        day: 2,
        hour: 3,
        minute: 4,
        second: 5
    };
    function hackyOffset(dtf, date) {
        var formatted = dtf.format(date).replace(/\u200E/g, ""), parsed = /(\d+)\/(\d+)\/(\d+),? (\d+):(\d+):(\d+)/.exec(formatted), fMonth = parsed[1], fDay = parsed[2], fYear = parsed[3], fHour = parsed[4], fMinute = parsed[5], fSecond = parsed[6];
        return [fYear, fMonth, fDay, fHour, fMinute, fSecond];
    }
    function partsOffset(dtf, date) {
        var formatted = dtf.formatToParts(date), filled = [];
        for (var i = 0; i < formatted.length; i++) {
            var _formatted$i = formatted[i], type = _formatted$i.type, value = _formatted$i.value, pos = typeToPos[type];
            if (!isUndefined(pos)) {
                filled[pos] = parseInt(value, 10);
            }
        }
        return filled;
    }
    var ianaZoneCache = {};
    /**
     * A zone identified by an IANA identifier, like America/New_York
     * @implements {Zone}
     */
    var IANAZone = 
    /*#__PURE__*/
    function (_Zone) {
        _inheritsLoose(IANAZone, _Zone);
        /**
         * @param {string} name - Zone name
         * @return {IANAZone}
         */
        IANAZone.create = function create(name) {
            if (!ianaZoneCache[name]) {
                ianaZoneCache[name] = new IANAZone(name);
            }
            return ianaZoneCache[name];
        };
        IANAZone.resetCache = function resetCache() {
            ianaZoneCache = {};
            dtfCache = {};
        };
        IANAZone.isValidSpecifier = function isValidSpecifier(s) {
            return !!(s && s.match(matchingRegex));
        };
        IANAZone.isValidZone = function isValidZone(zone) {
            try {
                new Intl.DateTimeFormat("en-US", {
                    timeZone: zone
                }).format();
                return true;
            }
            catch (e) {
                return false;
            }
        } // Etc/GMT+8 -> -480
        ;
        IANAZone.parseGMTOffset = function parseGMTOffset(specifier) {
            if (specifier) {
                var match = specifier.match(/^Etc\/GMT([+-]\d{1,2})$/i);
                if (match) {
                    return -60 * parseInt(match[1]);
                }
            }
            return null;
        };
        function IANAZone(name) {
            var _this;
            _this = _Zone.call(this) || this;
            /** @private **/
            _this.zoneName = name;
            /** @private **/
            _this.valid = IANAZone.isValidZone(name);
            return _this;
        }
        /** @override **/
        var _proto = IANAZone.prototype;
        /** @override **/
        _proto.offsetName = function offsetName(ts, _ref) {
            var format = _ref.format, locale = _ref.locale;
            return parseZoneInfo(ts, format, locale, this.name);
        };
        _proto.formatOffset = function formatOffset$1(ts, format) {
            return formatOffset(this.offset(ts), format);
        };
        _proto.offset = function offset(ts) {
            var date = new Date(ts), dtf = makeDTF(this.name), _ref2 = dtf.formatToParts ? partsOffset(dtf, date) : hackyOffset(dtf, date), year = _ref2[0], month = _ref2[1], day = _ref2[2], hour = _ref2[3], minute = _ref2[4], second = _ref2[5], adjustedHour = hour === 24 ? 0 : hour;
            var asUTC = objToLocalTS({
                year: year,
                month: month,
                day: day,
                hour: adjustedHour,
                minute: minute,
                second: second,
                millisecond: 0
            });
            var asTS = date.valueOf();
            asTS -= asTS % 1000;
            return (asUTC - asTS) / (60 * 1000);
        };
        _proto.equals = function equals(otherZone) {
            return otherZone.type === "iana" && otherZone.name === this.name;
        };
        _createClass(IANAZone, [{
                key: "type",
                get: function get() {
                    return "iana";
                }
                /** @override **/
            }, {
                key: "name",
                get: function get() {
                    return this.zoneName;
                }
                /** @override **/
            }, {
                key: "universal",
                get: function get() {
                    return false;
                }
            }, {
                key: "isValid",
                get: function get() {
                    return this.valid;
                }
            }]);
        return IANAZone;
    }(Zone);
    var singleton$1 = null;
    /**
     * A zone with a fixed offset (meaning no DST)
     * @implements {Zone}
     */
    var FixedOffsetZone = 
    /*#__PURE__*/
    function (_Zone) {
        _inheritsLoose(FixedOffsetZone, _Zone);
        /**
         * Get an instance with a specified offset
         * @param {number} offset - The offset in minutes
         * @return {FixedOffsetZone}
         */
        FixedOffsetZone.instance = function instance(offset) {
            return offset === 0 ? FixedOffsetZone.utcInstance : new FixedOffsetZone(offset);
        };
        FixedOffsetZone.parseSpecifier = function parseSpecifier(s) {
            if (s) {
                var r = s.match(/^utc(?:([+-]\d{1,2})(?::(\d{2}))?)?$/i);
                if (r) {
                    return new FixedOffsetZone(signedOffset(r[1], r[2]));
                }
            }
            return null;
        };
        _createClass(FixedOffsetZone, null, [{
                key: "utcInstance",
                /**
                 * Get a singleton instance of UTC
                 * @return {FixedOffsetZone}
                 */
                get: function get() {
                    if (singleton$1 === null) {
                        singleton$1 = new FixedOffsetZone(0);
                    }
                    return singleton$1;
                }
            }]);
        function FixedOffsetZone(offset) {
            var _this;
            _this = _Zone.call(this) || this;
            /** @private **/
            _this.fixed = offset;
            return _this;
        }
        /** @override **/
        var _proto = FixedOffsetZone.prototype;
        /** @override **/
        _proto.offsetName = function offsetName() {
            return this.name;
        };
        _proto.formatOffset = function formatOffset$1(ts, format) {
            return formatOffset(this.fixed, format);
        };
        /** @override **/
        _proto.offset = function offset() {
            return this.fixed;
        };
        _proto.equals = function equals(otherZone) {
            return otherZone.type === "fixed" && otherZone.fixed === this.fixed;
        };
        _createClass(FixedOffsetZone, [{
                key: "type",
                get: function get() {
                    return "fixed";
                }
                /** @override **/
            }, {
                key: "name",
                get: function get() {
                    return this.fixed === 0 ? "UTC" : "UTC" + formatOffset(this.fixed, "narrow");
                }
            }, {
                key: "universal",
                get: function get() {
                    return true;
                }
            }, {
                key: "isValid",
                get: function get() {
                    return true;
                }
            }]);
        return FixedOffsetZone;
    }(Zone);
    /**
     * A zone that failed to parse. You should never need to instantiate this.
     * @implements {Zone}
     */
    var InvalidZone = 
    /*#__PURE__*/
    function (_Zone) {
        _inheritsLoose(InvalidZone, _Zone);
        function InvalidZone(zoneName) {
            var _this;
            _this = _Zone.call(this) || this;
            /**  @private */
            _this.zoneName = zoneName;
            return _this;
        }
        /** @override **/
        var _proto = InvalidZone.prototype;
        /** @override **/
        _proto.offsetName = function offsetName() {
            return null;
        };
        _proto.formatOffset = function formatOffset() {
            return "";
        };
        _proto.offset = function offset() {
            return NaN;
        };
        _proto.equals = function equals() {
            return false;
        };
        _createClass(InvalidZone, [{
                key: "type",
                get: function get() {
                    return "invalid";
                }
                /** @override **/
            }, {
                key: "name",
                get: function get() {
                    return this.zoneName;
                }
                /** @override **/
            }, {
                key: "universal",
                get: function get() {
                    return false;
                }
            }, {
                key: "isValid",
                get: function get() {
                    return false;
                }
            }]);
        return InvalidZone;
    }(Zone);
    /**
     * @private
     */
    function normalizeZone(input, defaultZone) {
        var offset;
        if (isUndefined(input) || input === null) {
            return defaultZone;
        }
        else if (input instanceof Zone) {
            return input;
        }
        else if (isString(input)) {
            var lowered = input.toLowerCase();
            if (lowered === "local")
                return defaultZone;
            else if (lowered === "utc" || lowered === "gmt")
                return FixedOffsetZone.utcInstance;
            else if ((offset = IANAZone.parseGMTOffset(input)) != null) {
                // handle Etc/GMT-4, which V8 chokes on
                return FixedOffsetZone.instance(offset);
            }
            else if (IANAZone.isValidSpecifier(lowered))
                return IANAZone.create(input);
            else
                return FixedOffsetZone.parseSpecifier(lowered) || new InvalidZone(input);
        }
        else if (isNumber(input)) {
            return FixedOffsetZone.instance(input);
        }
        else if (_typeof(input) === "object" && input.offset && typeof input.offset === "number") {
            // This is dumb, but the instanceof check above doesn't seem to really work
            // so we're duck checking it
            return input;
        }
        else {
            return new InvalidZone(input);
        }
    }
    var now = function now() {
        return Date.now();
    }, defaultZone = null, 
    // not setting this directly to LocalZone.instance bc loading order issues
    defaultLocale = null, defaultNumberingSystem = null, defaultOutputCalendar = null, throwOnInvalid = false;
    /**
     * Settings contains static getters and setters that control Luxon's overall behavior. Luxon is a simple library with few options, but the ones it does have live here.
     */
    var Settings = 
    /*#__PURE__*/
    function () {
        function Settings() { }
        /**
         * Reset Luxon's global caches. Should only be necessary in testing scenarios.
         * @return {void}
         */
        Settings.resetCaches = function resetCaches() {
            Locale.resetCache();
            IANAZone.resetCache();
        };
        _createClass(Settings, null, [{
                key: "now",
                /**
                 * Get the callback for returning the current timestamp.
                 * @type {function}
                 */
                get: function get() {
                    return now;
                }
                /**
                 * Set the callback for returning the current timestamp.
                 * The function should return a number, which will be interpreted as an Epoch millisecond count
                 * @type {function}
                 * @example Settings.now = () => Date.now() + 3000 // pretend it is 3 seconds in the future
                 * @example Settings.now = () => 0 // always pretend it's Jan 1, 1970 at midnight in UTC time
                 */
                ,
                set: function set(n) {
                    now = n;
                }
                /**
                 * Get the default time zone to create DateTimes in.
                 * @type {string}
                 */
            }, {
                key: "defaultZoneName",
                get: function get() {
                    return Settings.defaultZone.name;
                }
                /**
                 * Set the default time zone to create DateTimes in. Does not affect existing instances.
                 * @type {string}
                 */
                ,
                set: function set(z) {
                    if (!z) {
                        defaultZone = null;
                    }
                    else {
                        defaultZone = normalizeZone(z);
                    }
                }
                /**
                 * Get the default time zone object to create DateTimes in. Does not affect existing instances.
                 * @type {Zone}
                 */
            }, {
                key: "defaultZone",
                get: function get() {
                    return defaultZone || LocalZone.instance;
                }
                /**
                 * Get the default locale to create DateTimes with. Does not affect existing instances.
                 * @type {string}
                 */
            }, {
                key: "defaultLocale",
                get: function get() {
                    return defaultLocale;
                }
                /**
                 * Set the default locale to create DateTimes with. Does not affect existing instances.
                 * @type {string}
                 */
                ,
                set: function set(locale) {
                    defaultLocale = locale;
                }
                /**
                 * Get the default numbering system to create DateTimes with. Does not affect existing instances.
                 * @type {string}
                 */
            }, {
                key: "defaultNumberingSystem",
                get: function get() {
                    return defaultNumberingSystem;
                }
                /**
                 * Set the default numbering system to create DateTimes with. Does not affect existing instances.
                 * @type {string}
                 */
                ,
                set: function set(numberingSystem) {
                    defaultNumberingSystem = numberingSystem;
                }
                /**
                 * Get the default output calendar to create DateTimes with. Does not affect existing instances.
                 * @type {string}
                 */
            }, {
                key: "defaultOutputCalendar",
                get: function get() {
                    return defaultOutputCalendar;
                }
                /**
                 * Set the default output calendar to create DateTimes with. Does not affect existing instances.
                 * @type {string}
                 */
                ,
                set: function set(outputCalendar) {
                    defaultOutputCalendar = outputCalendar;
                }
                /**
                 * Get whether Luxon will throw when it encounters invalid DateTimes, Durations, or Intervals
                 * @type {boolean}
                 */
            }, {
                key: "throwOnInvalid",
                get: function get() {
                    return throwOnInvalid;
                }
                /**
                 * Set whether Luxon will throw when it encounters invalid DateTimes, Durations, or Intervals
                 * @type {boolean}
                 */
                ,
                set: function set(t) {
                    throwOnInvalid = t;
                }
            }]);
        return Settings;
    }();
    var intlDTCache = {};
    function getCachedDTF(locString, opts) {
        if (opts === void 0) {
            opts = {};
        }
        var key = JSON.stringify([locString, opts]);
        var dtf = intlDTCache[key];
        if (!dtf) {
            dtf = new Intl.DateTimeFormat(locString, opts);
            intlDTCache[key] = dtf;
        }
        return dtf;
    }
    var intlNumCache = {};
    function getCachedINF(locString, opts) {
        if (opts === void 0) {
            opts = {};
        }
        var key = JSON.stringify([locString, opts]);
        var inf = intlNumCache[key];
        if (!inf) {
            inf = new Intl.NumberFormat(locString, opts);
            intlNumCache[key] = inf;
        }
        return inf;
    }
    var intlRelCache = {};
    function getCachedRTF(locString, opts) {
        if (opts === void 0) {
            opts = {};
        }
        var key = JSON.stringify([locString, opts]);
        var inf = intlRelCache[key];
        if (!inf) {
            inf = new Intl.RelativeTimeFormat(locString, opts);
            intlRelCache[key] = inf;
        }
        return inf;
    }
    var sysLocaleCache = null;
    function systemLocale() {
        if (sysLocaleCache) {
            return sysLocaleCache;
        }
        else if (hasIntl()) {
            var computedSys = new Intl.DateTimeFormat().resolvedOptions().locale; // node sometimes defaults to "und". Override that because that is dumb
            sysLocaleCache = !computedSys || computedSys === "und" ? "en-US" : computedSys;
            return sysLocaleCache;
        }
        else {
            sysLocaleCache = "en-US";
            return sysLocaleCache;
        }
    }
    function parseLocaleString(localeStr) {
        // I really want to avoid writing a BCP 47 parser
        // see, e.g. https://github.com/wooorm/bcp-47
        // Instead, we'll do this:
        // a) if the string has no -u extensions, just leave it alone
        // b) if it does, use Intl to resolve everything
        // c) if Intl fails, try again without the -u
        var uIndex = localeStr.indexOf("-u-");
        if (uIndex === -1) {
            return [localeStr];
        }
        else {
            var options;
            var smaller = localeStr.substring(0, uIndex);
            try {
                options = getCachedDTF(localeStr).resolvedOptions();
            }
            catch (e) {
                options = getCachedDTF(smaller).resolvedOptions();
            }
            var _options = options, numberingSystem = _options.numberingSystem, calendar = _options.calendar; // return the smaller one so that we can append the calendar and numbering overrides to it
            return [smaller, numberingSystem, calendar];
        }
    }
    function intlConfigString(localeStr, numberingSystem, outputCalendar) {
        if (hasIntl()) {
            if (outputCalendar || numberingSystem) {
                localeStr += "-u";
                if (outputCalendar) {
                    localeStr += "-ca-" + outputCalendar;
                }
                if (numberingSystem) {
                    localeStr += "-nu-" + numberingSystem;
                }
                return localeStr;
            }
            else {
                return localeStr;
            }
        }
        else {
            return [];
        }
    }
    function mapMonths(f) {
        var ms = [];
        for (var i = 1; i <= 12; i++) {
            var dt = DateTime.utc(2016, i, 1);
            ms.push(f(dt));
        }
        return ms;
    }
    function mapWeekdays(f) {
        var ms = [];
        for (var i = 1; i <= 7; i++) {
            var dt = DateTime.utc(2016, 11, 13 + i);
            ms.push(f(dt));
        }
        return ms;
    }
    function listStuff(loc, length, defaultOK, englishFn, intlFn) {
        var mode = loc.listingMode(defaultOK);
        if (mode === "error") {
            return null;
        }
        else if (mode === "en") {
            return englishFn(length);
        }
        else {
            return intlFn(length);
        }
    }
    function supportsFastNumbers(loc) {
        if (loc.numberingSystem && loc.numberingSystem !== "latn") {
            return false;
        }
        else {
            return loc.numberingSystem === "latn" || !loc.locale || loc.locale.startsWith("en") || hasIntl() && new Intl.DateTimeFormat(loc.intl).resolvedOptions().numberingSystem === "latn";
        }
    }
    /**
     * @private
     */
    var PolyNumberFormatter = 
    /*#__PURE__*/
    function () {
        function PolyNumberFormatter(intl, forceSimple, opts) {
            this.padTo = opts.padTo || 0;
            this.floor = opts.floor || false;
            if (!forceSimple && hasIntl()) {
                var intlOpts = {
                    useGrouping: false
                };
                if (opts.padTo > 0)
                    intlOpts.minimumIntegerDigits = opts.padTo;
                this.inf = getCachedINF(intl, intlOpts);
            }
        }
        var _proto = PolyNumberFormatter.prototype;
        _proto.format = function format(i) {
            if (this.inf) {
                var fixed = this.floor ? Math.floor(i) : i;
                return this.inf.format(fixed);
            }
            else {
                // to match the browser's numberformatter defaults
                var _fixed = this.floor ? Math.floor(i) : roundTo(i, 3);
                return padStart(_fixed, this.padTo);
            }
        };
        return PolyNumberFormatter;
    }();
    /**
     * @private
     */
    var PolyDateFormatter = 
    /*#__PURE__*/
    function () {
        function PolyDateFormatter(dt, intl, opts) {
            this.opts = opts;
            this.hasIntl = hasIntl();
            var z;
            if (dt.zone.universal && this.hasIntl) {
                // Chromium doesn't support fixed-offset zones like Etc/GMT+8 in its formatter,
                // See https://bugs.chromium.org/p/chromium/issues/detail?id=364374.
                // So we have to make do. Two cases:
                // 1. The format options tell us to show the zone. We can't do that, so the best
                // we can do is format the date in UTC.
                // 2. The format options don't tell us to show the zone. Then we can adjust them
                // the time and tell the formatter to show it to us in UTC, so that the time is right
                // and the bad zone doesn't show up.
                // We can clean all this up when Chrome fixes this.
                z = "UTC";
                if (opts.timeZoneName) {
                    this.dt = dt;
                }
                else {
                    this.dt = dt.offset === 0 ? dt : DateTime.fromMillis(dt.ts + dt.offset * 60 * 1000);
                }
            }
            else if (dt.zone.type === "local") {
                this.dt = dt;
            }
            else {
                this.dt = dt;
                z = dt.zone.name;
            }
            if (this.hasIntl) {
                var intlOpts = Object.assign({}, this.opts);
                if (z) {
                    intlOpts.timeZone = z;
                }
                this.dtf = getCachedDTF(intl, intlOpts);
            }
        }
        var _proto2 = PolyDateFormatter.prototype;
        _proto2.format = function format() {
            if (this.hasIntl) {
                return this.dtf.format(this.dt.toJSDate());
            }
            else {
                var tokenFormat = formatString(this.opts), loc = Locale.create("en-US");
                return Formatter.create(loc).formatDateTimeFromString(this.dt, tokenFormat);
            }
        };
        _proto2.formatToParts = function formatToParts() {
            if (this.hasIntl && hasFormatToParts()) {
                return this.dtf.formatToParts(this.dt.toJSDate());
            }
            else {
                // This is kind of a cop out. We actually could do this for English. However, we couldn't do it for intl strings
                // and IMO it's too weird to have an uncanny valley like that
                return [];
            }
        };
        _proto2.resolvedOptions = function resolvedOptions() {
            if (this.hasIntl) {
                return this.dtf.resolvedOptions();
            }
            else {
                return {
                    locale: "en-US",
                    numberingSystem: "latn",
                    outputCalendar: "gregory"
                };
            }
        };
        return PolyDateFormatter;
    }();
    /**
     * @private
     */
    var PolyRelFormatter = 
    /*#__PURE__*/
    function () {
        function PolyRelFormatter(intl, isEnglish, opts) {
            this.opts = Object.assign({
                style: "long"
            }, opts);
            if (!isEnglish && hasRelative()) {
                this.rtf = getCachedRTF(intl, opts);
            }
        }
        var _proto3 = PolyRelFormatter.prototype;
        _proto3.format = function format(count, unit) {
            if (this.rtf) {
                return this.rtf.format(count, unit);
            }
            else {
                return formatRelativeTime(unit, count, this.opts.numeric, this.opts.style !== "long");
            }
        };
        _proto3.formatToParts = function formatToParts(count, unit) {
            if (this.rtf) {
                return this.rtf.formatToParts(count, unit);
            }
            else {
                return [];
            }
        };
        return PolyRelFormatter;
    }();
    /**
     * @private
     */
    var Locale = 
    /*#__PURE__*/
    function () {
        Locale.fromOpts = function fromOpts(opts) {
            return Locale.create(opts.locale, opts.numberingSystem, opts.outputCalendar, opts.defaultToEN);
        };
        Locale.create = function create(locale, numberingSystem, outputCalendar, defaultToEN) {
            if (defaultToEN === void 0) {
                defaultToEN = false;
            }
            var specifiedLocale = locale || Settings.defaultLocale, 
            // the system locale is useful for human readable strings but annoying for parsing/formatting known formats
            localeR = specifiedLocale || (defaultToEN ? "en-US" : systemLocale()), numberingSystemR = numberingSystem || Settings.defaultNumberingSystem, outputCalendarR = outputCalendar || Settings.defaultOutputCalendar;
            return new Locale(localeR, numberingSystemR, outputCalendarR, specifiedLocale);
        };
        Locale.resetCache = function resetCache() {
            sysLocaleCache = null;
            intlDTCache = {};
            intlNumCache = {};
            intlRelCache = {};
        };
        Locale.fromObject = function fromObject(_temp) {
            var _ref = _temp === void 0 ? {} : _temp, locale = _ref.locale, numberingSystem = _ref.numberingSystem, outputCalendar = _ref.outputCalendar;
            return Locale.create(locale, numberingSystem, outputCalendar);
        };
        function Locale(locale, numbering, outputCalendar, specifiedLocale) {
            var _parseLocaleString = parseLocaleString(locale), parsedLocale = _parseLocaleString[0], parsedNumberingSystem = _parseLocaleString[1], parsedOutputCalendar = _parseLocaleString[2];
            this.locale = parsedLocale;
            this.numberingSystem = numbering || parsedNumberingSystem || null;
            this.outputCalendar = outputCalendar || parsedOutputCalendar || null;
            this.intl = intlConfigString(this.locale, this.numberingSystem, this.outputCalendar);
            this.weekdaysCache = {
                format: {},
                standalone: {}
            };
            this.monthsCache = {
                format: {},
                standalone: {}
            };
            this.meridiemCache = null;
            this.eraCache = {};
            this.specifiedLocale = specifiedLocale;
            this.fastNumbersCached = null;
        }
        var _proto4 = Locale.prototype;
        _proto4.listingMode = function listingMode(defaultOK) {
            if (defaultOK === void 0) {
                defaultOK = true;
            }
            var intl = hasIntl(), hasFTP = intl && hasFormatToParts(), isActuallyEn = this.isEnglish(), hasNoWeirdness = (this.numberingSystem === null || this.numberingSystem === "latn") && (this.outputCalendar === null || this.outputCalendar === "gregory");
            if (!hasFTP && !(isActuallyEn && hasNoWeirdness) && !defaultOK) {
                return "error";
            }
            else if (!hasFTP || isActuallyEn && hasNoWeirdness) {
                return "en";
            }
            else {
                return "intl";
            }
        };
        _proto4.clone = function clone(alts) {
            if (!alts || Object.getOwnPropertyNames(alts).length === 0) {
                return this;
            }
            else {
                return Locale.create(alts.locale || this.specifiedLocale, alts.numberingSystem || this.numberingSystem, alts.outputCalendar || this.outputCalendar, alts.defaultToEN || false);
            }
        };
        _proto4.redefaultToEN = function redefaultToEN(alts) {
            if (alts === void 0) {
                alts = {};
            }
            return this.clone(Object.assign({}, alts, {
                defaultToEN: true
            }));
        };
        _proto4.redefaultToSystem = function redefaultToSystem(alts) {
            if (alts === void 0) {
                alts = {};
            }
            return this.clone(Object.assign({}, alts, {
                defaultToEN: false
            }));
        };
        _proto4.months = function months$1(length, format, defaultOK) {
            var _this = this;
            if (format === void 0) {
                format = false;
            }
            if (defaultOK === void 0) {
                defaultOK = true;
            }
            return listStuff(this, length, defaultOK, months, function () {
                var intl = format ? {
                    month: length,
                    day: "numeric"
                } : {
                    month: length
                }, formatStr = format ? "format" : "standalone";
                if (!_this.monthsCache[formatStr][length]) {
                    _this.monthsCache[formatStr][length] = mapMonths(function (dt) {
                        return _this.extract(dt, intl, "month");
                    });
                }
                return _this.monthsCache[formatStr][length];
            });
        };
        _proto4.weekdays = function weekdays$1(length, format, defaultOK) {
            var _this2 = this;
            if (format === void 0) {
                format = false;
            }
            if (defaultOK === void 0) {
                defaultOK = true;
            }
            return listStuff(this, length, defaultOK, weekdays, function () {
                var intl = format ? {
                    weekday: length,
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                } : {
                    weekday: length
                }, formatStr = format ? "format" : "standalone";
                if (!_this2.weekdaysCache[formatStr][length]) {
                    _this2.weekdaysCache[formatStr][length] = mapWeekdays(function (dt) {
                        return _this2.extract(dt, intl, "weekday");
                    });
                }
                return _this2.weekdaysCache[formatStr][length];
            });
        };
        _proto4.meridiems = function meridiems$1(defaultOK) {
            var _this3 = this;
            if (defaultOK === void 0) {
                defaultOK = true;
            }
            return listStuff(this, undefined, defaultOK, function () {
                return meridiems;
            }, function () {
                // In theory there could be aribitrary day periods. We're gonna assume there are exactly two
                // for AM and PM. This is probably wrong, but it's makes parsing way easier.
                if (!_this3.meridiemCache) {
                    var intl = {
                        hour: "numeric",
                        hour12: true
                    };
                    _this3.meridiemCache = [DateTime.utc(2016, 11, 13, 9), DateTime.utc(2016, 11, 13, 19)].map(function (dt) {
                        return _this3.extract(dt, intl, "dayperiod");
                    });
                }
                return _this3.meridiemCache;
            });
        };
        _proto4.eras = function eras$1(length, defaultOK) {
            var _this4 = this;
            if (defaultOK === void 0) {
                defaultOK = true;
            }
            return listStuff(this, length, defaultOK, eras, function () {
                var intl = {
                    era: length
                }; // This is utter bullshit. Different calendars are going to define eras totally differently. What I need is the minimum set of dates
                // to definitely enumerate them.
                if (!_this4.eraCache[length]) {
                    _this4.eraCache[length] = [DateTime.utc(-40, 1, 1), DateTime.utc(2017, 1, 1)].map(function (dt) {
                        return _this4.extract(dt, intl, "era");
                    });
                }
                return _this4.eraCache[length];
            });
        };
        _proto4.extract = function extract(dt, intlOpts, field) {
            var df = this.dtFormatter(dt, intlOpts), results = df.formatToParts(), matching = results.find(function (m) {
                return m.type.toLowerCase() === field;
            });
            return matching ? matching.value : null;
        };
        _proto4.numberFormatter = function numberFormatter(opts) {
            if (opts === void 0) {
                opts = {};
            } // this forcesimple option is never used (the only caller short-circuits on it, but it seems safer to leave)
            // (in contrast, the rest of the condition is used heavily)
            return new PolyNumberFormatter(this.intl, opts.forceSimple || this.fastNumbers, opts);
        };
        _proto4.dtFormatter = function dtFormatter(dt, intlOpts) {
            if (intlOpts === void 0) {
                intlOpts = {};
            }
            return new PolyDateFormatter(dt, this.intl, intlOpts);
        };
        _proto4.relFormatter = function relFormatter(opts) {
            if (opts === void 0) {
                opts = {};
            }
            return new PolyRelFormatter(this.intl, this.isEnglish(), opts);
        };
        _proto4.isEnglish = function isEnglish() {
            return this.locale === "en" || this.locale.toLowerCase() === "en-us" || hasIntl() && new Intl.DateTimeFormat(this.intl).resolvedOptions().locale.startsWith("en-us");
        };
        _proto4.equals = function equals(other) {
            return this.locale === other.locale && this.numberingSystem === other.numberingSystem && this.outputCalendar === other.outputCalendar;
        };
        _createClass(Locale, [{
                key: "fastNumbers",
                get: function get() {
                    if (this.fastNumbersCached == null) {
                        this.fastNumbersCached = supportsFastNumbers(this);
                    }
                    return this.fastNumbersCached;
                }
            }]);
        return Locale;
    }();
    /*
     * This file handles parsing for well-specified formats. Here's how it works:
     * Two things go into parsing: a regex to match with and an extractor to take apart the groups in the match.
     * An extractor is just a function that takes a regex match array and returns a { year: ..., month: ... } object
     * parse() does the work of executing the regex and applying the extractor. It takes multiple regex/extractor pairs to try in sequence.
     * Extractors can take a "cursor" representing the offset in the match to look at. This makes it easy to combine extractors.
     * combineExtractors() does the work of combining them, keeping track of the cursor through multiple extractions.
     * Some extractions are super dumb and simpleParse and fromStrings help DRY them.
     */
    function combineRegexes() {
        for (var _len = arguments.length, regexes = new Array(_len), _key = 0; _key < _len; _key++) {
            regexes[_key] = arguments[_key];
        }
        var full = regexes.reduce(function (f, r) {
            return f + r.source;
        }, "");
        return RegExp("^" + full + "$");
    }
    function combineExtractors() {
        for (var _len2 = arguments.length, extractors = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            extractors[_key2] = arguments[_key2];
        }
        return function (m) {
            return extractors.reduce(function (_ref, ex) {
                var mergedVals = _ref[0], mergedZone = _ref[1], cursor = _ref[2];
                var _ex = ex(m, cursor), val = _ex[0], zone = _ex[1], next = _ex[2];
                return [Object.assign(mergedVals, val), mergedZone || zone, next];
            }, [{}, null, 1]).slice(0, 2);
        };
    }
    function parse(s) {
        if (s == null) {
            return [null, null];
        }
        for (var _len3 = arguments.length, patterns = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
            patterns[_key3 - 1] = arguments[_key3];
        }
        for (var _i = 0, _patterns = patterns; _i < _patterns.length; _i++) {
            var _patterns$_i = _patterns[_i], regex = _patterns$_i[0], extractor = _patterns$_i[1];
            var m = regex.exec(s);
            if (m) {
                return extractor(m);
            }
        }
        return [null, null];
    }
    function simpleParse() {
        for (var _len4 = arguments.length, keys = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
            keys[_key4] = arguments[_key4];
        }
        return function (match, cursor) {
            var ret = {};
            var i;
            for (i = 0; i < keys.length; i++) {
                ret[keys[i]] = parseInteger(match[cursor + i]);
            }
            return [ret, null, cursor + i];
        };
    } // ISO and SQL parsing
    var offsetRegex = /(?:(Z)|([+-]\d\d)(?::?(\d\d))?)/, isoTimeBaseRegex = /(\d\d)(?::?(\d\d)(?::?(\d\d)(?:[.,](\d{1,9}))?)?)?/, isoTimeRegex = RegExp("" + isoTimeBaseRegex.source + offsetRegex.source + "?"), isoTimeExtensionRegex = RegExp("(?:T" + isoTimeRegex.source + ")?"), isoYmdRegex = /([+-]\d{6}|\d{4})(?:-?(\d\d)(?:-?(\d\d))?)?/, isoWeekRegex = /(\d{4})-?W(\d\d)(?:-?(\d))?/, isoOrdinalRegex = /(\d{4})-?(\d{3})/, extractISOWeekData = simpleParse("weekYear", "weekNumber", "weekDay"), extractISOOrdinalData = simpleParse("year", "ordinal"), sqlYmdRegex = /(\d{4})-(\d\d)-(\d\d)/, 
    // dumbed-down version of the ISO one
    sqlTimeRegex = RegExp(isoTimeBaseRegex.source + " ?(?:" + offsetRegex.source + "|(" + ianaRegex.source + "))?"), sqlTimeExtensionRegex = RegExp("(?: " + sqlTimeRegex.source + ")?");
    function _int(match, pos, fallback) {
        var m = match[pos];
        return isUndefined(m) ? fallback : parseInteger(m);
    }
    function extractISOYmd(match, cursor) {
        var item = {
            year: _int(match, cursor),
            month: _int(match, cursor + 1, 1),
            day: _int(match, cursor + 2, 1)
        };
        return [item, null, cursor + 3];
    }
    function extractISOTime(match, cursor) {
        var item = {
            hour: _int(match, cursor, 0),
            minute: _int(match, cursor + 1, 0),
            second: _int(match, cursor + 2, 0),
            millisecond: parseMillis(match[cursor + 3])
        };
        return [item, null, cursor + 4];
    }
    function extractISOOffset(match, cursor) {
        var local = !match[cursor] && !match[cursor + 1], fullOffset = signedOffset(match[cursor + 1], match[cursor + 2]), zone = local ? null : FixedOffsetZone.instance(fullOffset);
        return [{}, zone, cursor + 3];
    }
    function extractIANAZone(match, cursor) {
        var zone = match[cursor] ? IANAZone.create(match[cursor]) : null;
        return [{}, zone, cursor + 1];
    } // ISO duration parsing
    var isoDuration = /^P(?:(?:(-?\d{1,9})Y)?(?:(-?\d{1,9})M)?(?:(-?\d{1,9})W)?(?:(-?\d{1,9})D)?(?:T(?:(-?\d{1,9})H)?(?:(-?\d{1,9})M)?(?:(-?\d{1,9})(?:[.,](-?\d{1,9}))?S)?)?)$/;
    function extractISODuration(match) {
        var yearStr = match[1], monthStr = match[2], weekStr = match[3], dayStr = match[4], hourStr = match[5], minuteStr = match[6], secondStr = match[7], millisecondsStr = match[8];
        return [{
                years: parseInteger(yearStr),
                months: parseInteger(monthStr),
                weeks: parseInteger(weekStr),
                days: parseInteger(dayStr),
                hours: parseInteger(hourStr),
                minutes: parseInteger(minuteStr),
                seconds: parseInteger(secondStr),
                milliseconds: parseMillis(millisecondsStr)
            }];
    } // These are a little braindead. EDT *should* tell us that we're in, say, America/New_York
    // and not just that we're in -240 *right now*. But since I don't think these are used that often
    // I'm just going to ignore that
    var obsOffsets = {
        GMT: 0,
        EDT: -4 * 60,
        EST: -5 * 60,
        CDT: -5 * 60,
        CST: -6 * 60,
        MDT: -6 * 60,
        MST: -7 * 60,
        PDT: -7 * 60,
        PST: -8 * 60
    };
    function fromStrings(weekdayStr, yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
        var result = {
            year: yearStr.length === 2 ? untruncateYear(parseInteger(yearStr)) : parseInteger(yearStr),
            month: monthsShort.indexOf(monthStr) + 1,
            day: parseInteger(dayStr),
            hour: parseInteger(hourStr),
            minute: parseInteger(minuteStr)
        };
        if (secondStr)
            result.second = parseInteger(secondStr);
        if (weekdayStr) {
            result.weekday = weekdayStr.length > 3 ? weekdaysLong.indexOf(weekdayStr) + 1 : weekdaysShort.indexOf(weekdayStr) + 1;
        }
        return result;
    } // RFC 2822/5322
    var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|(?:([+-]\d\d)(\d\d)))$/;
    function extractRFC2822(match) {
        var weekdayStr = match[1], dayStr = match[2], monthStr = match[3], yearStr = match[4], hourStr = match[5], minuteStr = match[6], secondStr = match[7], obsOffset = match[8], milOffset = match[9], offHourStr = match[10], offMinuteStr = match[11], result = fromStrings(weekdayStr, yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr);
        var offset;
        if (obsOffset) {
            offset = obsOffsets[obsOffset];
        }
        else if (milOffset) {
            offset = 0;
        }
        else {
            offset = signedOffset(offHourStr, offMinuteStr);
        }
        return [result, new FixedOffsetZone(offset)];
    }
    function preprocessRFC2822(s) {
        // Remove comments and folding whitespace and replace multiple-spaces with a single space
        return s.replace(/\([^)]*\)|[\n\t]/g, " ").replace(/(\s\s+)/g, " ").trim();
    } // http date
    var rfc1123 = /^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\d\d) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) (\d{4}) (\d\d):(\d\d):(\d\d) GMT$/, rfc850 = /^(Monday|Tuesday|Wedsday|Thursday|Friday|Saturday|Sunday), (\d\d)-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\d\d) (\d\d):(\d\d):(\d\d) GMT$/, ascii = /^(Mon|Tue|Wed|Thu|Fri|Sat|Sun) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( \d|\d\d) (\d\d):(\d\d):(\d\d) (\d{4})$/;
    function extractRFC1123Or850(match) {
        var weekdayStr = match[1], dayStr = match[2], monthStr = match[3], yearStr = match[4], hourStr = match[5], minuteStr = match[6], secondStr = match[7], result = fromStrings(weekdayStr, yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr);
        return [result, FixedOffsetZone.utcInstance];
    }
    function extractASCII(match) {
        var weekdayStr = match[1], monthStr = match[2], dayStr = match[3], hourStr = match[4], minuteStr = match[5], secondStr = match[6], yearStr = match[7], result = fromStrings(weekdayStr, yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr);
        return [result, FixedOffsetZone.utcInstance];
    }
    var isoYmdWithTimeExtensionRegex = combineRegexes(isoYmdRegex, isoTimeExtensionRegex);
    var isoWeekWithTimeExtensionRegex = combineRegexes(isoWeekRegex, isoTimeExtensionRegex);
    var isoOrdinalWithTimeExtensionRegex = combineRegexes(isoOrdinalRegex, isoTimeExtensionRegex);
    var isoTimeCombinedRegex = combineRegexes(isoTimeRegex);
    var extractISOYmdTimeAndOffset = combineExtractors(extractISOYmd, extractISOTime, extractISOOffset);
    var extractISOWeekTimeAndOffset = combineExtractors(extractISOWeekData, extractISOTime, extractISOOffset);
    var extractISOOrdinalDataAndTime = combineExtractors(extractISOOrdinalData, extractISOTime);
    var extractISOTimeAndOffset = combineExtractors(extractISOTime, extractISOOffset);
    /**
     * @private
     */
    function parseISODate(s) {
        return parse(s, [isoYmdWithTimeExtensionRegex, extractISOYmdTimeAndOffset], [isoWeekWithTimeExtensionRegex, extractISOWeekTimeAndOffset], [isoOrdinalWithTimeExtensionRegex, extractISOOrdinalDataAndTime], [isoTimeCombinedRegex, extractISOTimeAndOffset]);
    }
    function parseRFC2822Date(s) {
        return parse(preprocessRFC2822(s), [rfc2822, extractRFC2822]);
    }
    function parseHTTPDate(s) {
        return parse(s, [rfc1123, extractRFC1123Or850], [rfc850, extractRFC1123Or850], [ascii, extractASCII]);
    }
    function parseISODuration(s) {
        return parse(s, [isoDuration, extractISODuration]);
    }
    var sqlYmdWithTimeExtensionRegex = combineRegexes(sqlYmdRegex, sqlTimeExtensionRegex);
    var sqlTimeCombinedRegex = combineRegexes(sqlTimeRegex);
    var extractISOYmdTimeOffsetAndIANAZone = combineExtractors(extractISOYmd, extractISOTime, extractISOOffset, extractIANAZone);
    var extractISOTimeOffsetAndIANAZone = combineExtractors(extractISOTime, extractISOOffset, extractIANAZone);
    function parseSQL(s) {
        return parse(s, [sqlYmdWithTimeExtensionRegex, extractISOYmdTimeOffsetAndIANAZone], [sqlTimeCombinedRegex, extractISOTimeOffsetAndIANAZone]);
    }
    var INVALID = "Invalid Duration"; // unit conversion constants
    var lowOrderMatrix = {
        weeks: {
            days: 7,
            hours: 7 * 24,
            minutes: 7 * 24 * 60,
            seconds: 7 * 24 * 60 * 60,
            milliseconds: 7 * 24 * 60 * 60 * 1000
        },
        days: {
            hours: 24,
            minutes: 24 * 60,
            seconds: 24 * 60 * 60,
            milliseconds: 24 * 60 * 60 * 1000
        },
        hours: {
            minutes: 60,
            seconds: 60 * 60,
            milliseconds: 60 * 60 * 1000
        },
        minutes: {
            seconds: 60,
            milliseconds: 60 * 1000
        },
        seconds: {
            milliseconds: 1000
        }
    }, casualMatrix = Object.assign({
        years: {
            months: 12,
            weeks: 52,
            days: 365,
            hours: 365 * 24,
            minutes: 365 * 24 * 60,
            seconds: 365 * 24 * 60 * 60,
            milliseconds: 365 * 24 * 60 * 60 * 1000
        },
        quarters: {
            months: 3,
            weeks: 13,
            days: 91,
            hours: 91 * 24,
            minutes: 91 * 24 * 60,
            milliseconds: 91 * 24 * 60 * 60 * 1000
        },
        months: {
            weeks: 4,
            days: 30,
            hours: 30 * 24,
            minutes: 30 * 24 * 60,
            seconds: 30 * 24 * 60 * 60,
            milliseconds: 30 * 24 * 60 * 60 * 1000
        }
    }, lowOrderMatrix), daysInYearAccurate = 146097.0 / 400, daysInMonthAccurate = 146097.0 / 4800, accurateMatrix = Object.assign({
        years: {
            months: 12,
            weeks: daysInYearAccurate / 7,
            days: daysInYearAccurate,
            hours: daysInYearAccurate * 24,
            minutes: daysInYearAccurate * 24 * 60,
            seconds: daysInYearAccurate * 24 * 60 * 60,
            milliseconds: daysInYearAccurate * 24 * 60 * 60 * 1000
        },
        quarters: {
            months: 3,
            weeks: daysInYearAccurate / 28,
            days: daysInYearAccurate / 4,
            hours: daysInYearAccurate * 24 / 4,
            minutes: daysInYearAccurate * 24 * 60 / 4,
            seconds: daysInYearAccurate * 24 * 60 * 60 / 4,
            milliseconds: daysInYearAccurate * 24 * 60 * 60 * 1000 / 4
        },
        months: {
            weeks: daysInMonthAccurate / 7,
            days: daysInMonthAccurate,
            hours: daysInMonthAccurate * 24,
            minutes: daysInMonthAccurate * 24 * 60,
            seconds: daysInMonthAccurate * 24 * 60 * 60,
            milliseconds: daysInMonthAccurate * 24 * 60 * 60 * 1000
        }
    }, lowOrderMatrix); // units ordered by size
    var orderedUnits = ["years", "quarters", "months", "weeks", "days", "hours", "minutes", "seconds", "milliseconds"];
    var reverseUnits = orderedUnits.slice(0).reverse(); // clone really means "create another instance just like this one, but with these changes"
    function clone(dur, alts, clear) {
        if (clear === void 0) {
            clear = false;
        } // deep merge for vals
        var conf = {
            values: clear ? alts.values : Object.assign({}, dur.values, alts.values || {}),
            loc: dur.loc.clone(alts.loc),
            conversionAccuracy: alts.conversionAccuracy || dur.conversionAccuracy
        };
        return new Duration(conf);
    }
    function antiTrunc(n) {
        return n < 0 ? Math.floor(n) : Math.ceil(n);
    } // NB: mutates parameters
    function convert(matrix, fromMap, fromUnit, toMap, toUnit) {
        var conv = matrix[toUnit][fromUnit], raw = fromMap[fromUnit] / conv, sameSign = Math.sign(raw) === Math.sign(toMap[toUnit]), 
        // ok, so this is wild, but see the matrix in the tests
        added = !sameSign && toMap[toUnit] !== 0 && Math.abs(raw) <= 1 ? antiTrunc(raw) : Math.trunc(raw);
        toMap[toUnit] += added;
        fromMap[fromUnit] -= added * conv;
    } // NB: mutates parameters
    function normalizeValues(matrix, vals) {
        reverseUnits.reduce(function (previous, current) {
            if (!isUndefined(vals[current])) {
                if (previous) {
                    convert(matrix, vals, previous, vals, current);
                }
                return current;
            }
            else {
                return previous;
            }
        }, null);
    }
    /**
     * A Duration object represents a period of time, like "2 months" or "1 day, 1 hour". Conceptually, it's just a map of units to their quantities, accompanied by some additional configuration and methods for creating, parsing, interrogating, transforming, and formatting them. They can be used on their own or in conjunction with other Luxon types; for example, you can use {@link DateTime.plus} to add a Duration object to a DateTime, producing another DateTime.
     *
     * Here is a brief overview of commonly used methods and getters in Duration:
     *
     * * **Creation** To create a Duration, use {@link Duration.fromMillis}, {@link Duration.fromObject}, or {@link Duration.fromISO}.
     * * **Unit values** See the {@link Duration.years}, {@link Duration.months}, {@link Duration.weeks}, {@link Duration.days}, {@link Duration.hours}, {@link Duration.minutes}, {@link Duration.seconds}, {@link Duration.milliseconds} accessors.
     * * **Configuration** See  {@link Duration.locale} and {@link Duration.numberingSystem} accessors.
     * * **Transformation** To create new Durations out of old ones use {@link Duration.plus}, {@link Duration.minus}, {@link Duration.normalize}, {@link Duration.set}, {@link Duration.reconfigure}, {@link Duration.shiftTo}, and {@link Duration.negate}.
     * * **Output** To convert the Duration into other representations, see {@link Duration.as}, {@link Duration.toISO}, {@link Duration.toFormat}, and {@link Duration.toJSON}
     *
     * There's are more methods documented below. In addition, for more information on subtler topics like internationalization and validity, see the external documentation.
     */
    var Duration = 
    /*#__PURE__*/
    function () {
        /**
         * @private
         */
        function Duration(config) {
            var accurate = config.conversionAccuracy === "longterm" || false;
            /**
             * @access private
             */
            this.values = config.values;
            /**
             * @access private
             */
            this.loc = config.loc || Locale.create();
            /**
             * @access private
             */
            this.conversionAccuracy = accurate ? "longterm" : "casual";
            /**
             * @access private
             */
            this.invalid = config.invalid || null;
            /**
             * @access private
             */
            this.matrix = accurate ? accurateMatrix : casualMatrix;
            /**
             * @access private
             */
            this.isLuxonDuration = true;
        }
        /**
         * Create Duration from a number of milliseconds.
         * @param {number} count of milliseconds
         * @param {Object} opts - options for parsing
         * @param {string} [opts.locale='en-US'] - the locale to use
         * @param {string} opts.numberingSystem - the numbering system to use
         * @param {string} [opts.conversionAccuracy='casual'] - the conversion system to use
         * @return {Duration}
         */
        Duration.fromMillis = function fromMillis(count, opts) {
            return Duration.fromObject(Object.assign({
                milliseconds: count
            }, opts));
        };
        Duration.fromObject = function fromObject(obj) {
            if (obj == null || _typeof(obj) !== "object") {
                throw new InvalidArgumentError("Duration.fromObject: argument expected to be an object, got " + (obj === null ? "null" : _typeof(obj)));
            }
            return new Duration({
                values: normalizeObject(obj, Duration.normalizeUnit, ["locale", "numberingSystem", "conversionAccuracy", "zone" // a bit of debt; it's super inconvenient internally not to be able to blindly pass this
                ]),
                loc: Locale.fromObject(obj),
                conversionAccuracy: obj.conversionAccuracy
            });
        };
        Duration.fromISO = function fromISO(text, opts) {
            var _parseISODuration = parseISODuration(text), parsed = _parseISODuration[0];
            if (parsed) {
                var obj = Object.assign(parsed, opts);
                return Duration.fromObject(obj);
            }
            else {
                return Duration.invalid("unparsable", "the input \"" + text + "\" can't be parsed as ISO 8601");
            }
        };
        Duration.invalid = function invalid(reason, explanation) {
            if (explanation === void 0) {
                explanation = null;
            }
            if (!reason) {
                throw new InvalidArgumentError("need to specify a reason the Duration is invalid");
            }
            var invalid = reason instanceof Invalid ? reason : new Invalid(reason, explanation);
            if (Settings.throwOnInvalid) {
                throw new InvalidDurationError(invalid);
            }
            else {
                return new Duration({
                    invalid: invalid
                });
            }
        };
        Duration.normalizeUnit = function normalizeUnit(unit) {
            var normalized = {
                year: "years",
                years: "years",
                quarter: "quarters",
                quarters: "quarters",
                month: "months",
                months: "months",
                week: "weeks",
                weeks: "weeks",
                day: "days",
                days: "days",
                hour: "hours",
                hours: "hours",
                minute: "minutes",
                minutes: "minutes",
                second: "seconds",
                seconds: "seconds",
                millisecond: "milliseconds",
                milliseconds: "milliseconds"
            }[unit ? unit.toLowerCase() : unit];
            if (!normalized)
                throw new InvalidUnitError(unit);
            return normalized;
        };
        Duration.isDuration = function isDuration(o) {
            return o && o.isLuxonDuration || false;
        };
        var _proto = Duration.prototype;
        /**
         * Returns a string representation of this Duration formatted according to the specified format string. You may use these tokens:
         * * `S` for milliseconds
         * * `s` for seconds
         * * `m` for minutes
         * * `h` for hours
         * * `d` for days
         * * `M` for months
         * * `y` for years
         * Notes:
         * * Add padding by repeating the token, e.g. "yy" pads the years to two digits, "hhhh" pads the hours out to four digits
         * * The duration will be converted to the set of units in the format string using {@link Duration.shiftTo} and the Durations's conversion accuracy setting.
         * @param {string} fmt - the format string
         * @param {Object} opts - options
         * @param {boolean} [opts.floor=true] - floor numerical values
         * @example Duration.fromObject({ years: 1, days: 6, seconds: 2 }).toFormat("y d s") //=> "1 6 2"
         * @example Duration.fromObject({ years: 1, days: 6, seconds: 2 }).toFormat("yy dd sss") //=> "01 06 002"
         * @example Duration.fromObject({ years: 1, days: 6, seconds: 2 }).toFormat("M S") //=> "12 518402000"
         * @return {string}
         */
        _proto.toFormat = function toFormat(fmt, opts) {
            if (opts === void 0) {
                opts = {};
            } // reverse-compat since 1.2; we always round down now, never up, and we do it by default
            var fmtOpts = Object.assign({}, opts, {
                floor: opts.round !== false && opts.floor !== false
            });
            return this.isValid ? Formatter.create(this.loc, fmtOpts).formatDurationFromString(this, fmt) : INVALID;
        };
        _proto.toObject = function toObject(opts) {
            if (opts === void 0) {
                opts = {};
            }
            if (!this.isValid)
                return {};
            var base = Object.assign({}, this.values);
            if (opts.includeConfig) {
                base.conversionAccuracy = this.conversionAccuracy;
                base.numberingSystem = this.loc.numberingSystem;
                base.locale = this.loc.locale;
            }
            return base;
        };
        _proto.toISO = function toISO() {
            // we could use the formatter, but this is an easier way to get the minimum string
            if (!this.isValid)
                return null;
            var s = "P";
            if (this.years !== 0)
                s += this.years + "Y";
            if (this.months !== 0 || this.quarters !== 0)
                s += this.months + this.quarters * 3 + "M";
            if (this.weeks !== 0)
                s += this.weeks + "W";
            if (this.days !== 0)
                s += this.days + "D";
            if (this.hours !== 0 || this.minutes !== 0 || this.seconds !== 0 || this.milliseconds !== 0)
                s += "T";
            if (this.hours !== 0)
                s += this.hours + "H";
            if (this.minutes !== 0)
                s += this.minutes + "M";
            if (this.seconds !== 0 || this.milliseconds !== 0) // this will handle "floating point madness" by removing extra decimal places
                // https://stackoverflow.com/questions/588004/is-floating-point-math-broken
                s += roundTo(this.seconds + this.milliseconds / 1000, 3) + "S";
            if (s === "P")
                s += "T0S";
            return s;
        };
        _proto.toJSON = function toJSON() {
            return this.toISO();
        };
        _proto.toString = function toString() {
            return this.toISO();
        };
        _proto.valueOf = function valueOf() {
            return this.as("milliseconds");
        };
        _proto.plus = function plus(duration) {
            if (!this.isValid)
                return this;
            var dur = friendlyDuration(duration), result = {};
            for (var _i = 0, _orderedUnits = orderedUnits; _i < _orderedUnits.length; _i++) {
                var k = _orderedUnits[_i];
                if (hasOwnProperty(dur.values, k) || hasOwnProperty(this.values, k)) {
                    result[k] = dur.get(k) + this.get(k);
                }
            }
            return clone(this, {
                values: result
            }, true);
        };
        _proto.minus = function minus(duration) {
            if (!this.isValid)
                return this;
            var dur = friendlyDuration(duration);
            return this.plus(dur.negate());
        };
        _proto.mapUnits = function mapUnits(fn) {
            if (!this.isValid)
                return this;
            var result = {};
            for (var _i2 = 0, _Object$keys = Object.keys(this.values); _i2 < _Object$keys.length; _i2++) {
                var k = _Object$keys[_i2];
                result[k] = asNumber(fn(this.values[k], k));
            }
            return clone(this, {
                values: result
            }, true);
        };
        _proto.get = function get(unit) {
            return this[Duration.normalizeUnit(unit)];
        };
        _proto.set = function set(values) {
            if (!this.isValid)
                return this;
            var mixed = Object.assign(this.values, normalizeObject(values, Duration.normalizeUnit, []));
            return clone(this, {
                values: mixed
            });
        };
        _proto.reconfigure = function reconfigure(_temp) {
            var _ref = _temp === void 0 ? {} : _temp, locale = _ref.locale, numberingSystem = _ref.numberingSystem, conversionAccuracy = _ref.conversionAccuracy;
            var loc = this.loc.clone({
                locale: locale,
                numberingSystem: numberingSystem
            }), opts = {
                loc: loc
            };
            if (conversionAccuracy) {
                opts.conversionAccuracy = conversionAccuracy;
            }
            return clone(this, opts);
        };
        _proto.as = function as(unit) {
            return this.isValid ? this.shiftTo(unit).get(unit) : NaN;
        };
        _proto.normalize = function normalize() {
            if (!this.isValid)
                return this;
            var vals = this.toObject();
            normalizeValues(this.matrix, vals);
            return clone(this, {
                values: vals
            }, true);
        };
        _proto.shiftTo = function shiftTo() {
            for (var _len = arguments.length, units = new Array(_len), _key = 0; _key < _len; _key++) {
                units[_key] = arguments[_key];
            }
            if (!this.isValid)
                return this;
            if (units.length === 0) {
                return this;
            }
            units = units.map(function (u) {
                return Duration.normalizeUnit(u);
            });
            var built = {}, accumulated = {}, vals = this.toObject();
            var lastUnit;
            normalizeValues(this.matrix, vals);
            for (var _i3 = 0, _orderedUnits2 = orderedUnits; _i3 < _orderedUnits2.length; _i3++) {
                var k = _orderedUnits2[_i3];
                if (units.indexOf(k) >= 0) {
                    lastUnit = k;
                    var own = 0; // anything we haven't boiled down yet should get boiled to this unit
                    for (var ak in accumulated) {
                        own += this.matrix[ak][k] * accumulated[ak];
                        accumulated[ak] = 0;
                    } // plus anything that's already in this unit
                    if (isNumber(vals[k])) {
                        own += vals[k];
                    }
                    var i = Math.trunc(own);
                    built[k] = i;
                    accumulated[k] = own - i; // we'd like to absorb these fractions in another unit
                    // plus anything further down the chain that should be rolled up in to this
                    for (var down in vals) {
                        if (orderedUnits.indexOf(down) > orderedUnits.indexOf(k)) {
                            convert(this.matrix, vals, down, built, k);
                        }
                    } // otherwise, keep it in the wings to boil it later
                }
                else if (isNumber(vals[k])) {
                    accumulated[k] = vals[k];
                }
            } // anything leftover becomes the decimal for the last unit
            // lastUnit must be defined since units is not empty
            for (var key in accumulated) {
                if (accumulated[key] !== 0) {
                    built[lastUnit] += key === lastUnit ? accumulated[key] : accumulated[key] / this.matrix[lastUnit][key];
                }
            }
            return clone(this, {
                values: built
            }, true).normalize();
        };
        _proto.negate = function negate() {
            if (!this.isValid)
                return this;
            var negated = {};
            for (var _i4 = 0, _Object$keys2 = Object.keys(this.values); _i4 < _Object$keys2.length; _i4++) {
                var k = _Object$keys2[_i4];
                negated[k] = -this.values[k];
            }
            return clone(this, {
                values: negated
            }, true);
        };
        /**
         * Equality check
         * Two Durations are equal iff they have the same units and the same values for each unit.
         * @param {Duration} other
         * @return {boolean}
         */
        _proto.equals = function equals(other) {
            if (!this.isValid || !other.isValid) {
                return false;
            }
            if (!this.loc.equals(other.loc)) {
                return false;
            }
            for (var _i5 = 0, _orderedUnits3 = orderedUnits; _i5 < _orderedUnits3.length; _i5++) {
                var u = _orderedUnits3[_i5];
                if (this.values[u] !== other.values[u]) {
                    return false;
                }
            }
            return true;
        };
        _createClass(Duration, [{
                key: "locale",
                get: function get() {
                    return this.isValid ? this.loc.locale : null;
                }
                /**
                 * Get the numbering system of a Duration, such 'beng'. The numbering system is used when formatting the Duration
                 *
                 * @type {string}
                 */
            }, {
                key: "numberingSystem",
                get: function get() {
                    return this.isValid ? this.loc.numberingSystem : null;
                }
            }, {
                key: "years",
                get: function get() {
                    return this.isValid ? this.values.years || 0 : NaN;
                }
                /**
                 * Get the quarters.
                 * @type {number}
                 */
            }, {
                key: "quarters",
                get: function get() {
                    return this.isValid ? this.values.quarters || 0 : NaN;
                }
                /**
                 * Get the months.
                 * @type {number}
                 */
            }, {
                key: "months",
                get: function get() {
                    return this.isValid ? this.values.months || 0 : NaN;
                }
                /**
                 * Get the weeks
                 * @type {number}
                 */
            }, {
                key: "weeks",
                get: function get() {
                    return this.isValid ? this.values.weeks || 0 : NaN;
                }
                /**
                 * Get the days.
                 * @type {number}
                 */
            }, {
                key: "days",
                get: function get() {
                    return this.isValid ? this.values.days || 0 : NaN;
                }
                /**
                 * Get the hours.
                 * @type {number}
                 */
            }, {
                key: "hours",
                get: function get() {
                    return this.isValid ? this.values.hours || 0 : NaN;
                }
                /**
                 * Get the minutes.
                 * @type {number}
                 */
            }, {
                key: "minutes",
                get: function get() {
                    return this.isValid ? this.values.minutes || 0 : NaN;
                }
                /**
                 * Get the seconds.
                 * @return {number}
                 */
            }, {
                key: "seconds",
                get: function get() {
                    return this.isValid ? this.values.seconds || 0 : NaN;
                }
                /**
                 * Get the milliseconds.
                 * @return {number}
                 */
            }, {
                key: "milliseconds",
                get: function get() {
                    return this.isValid ? this.values.milliseconds || 0 : NaN;
                }
                /**
                 * Returns whether the Duration is invalid. Invalid durations are returned by diff operations
                 * on invalid DateTimes or Intervals.
                 * @return {boolean}
                 */
            }, {
                key: "isValid",
                get: function get() {
                    return this.invalid === null;
                }
                /**
                 * Returns an error code if this Duration became invalid, or null if the Duration is valid
                 * @return {string}
                 */
            }, {
                key: "invalidReason",
                get: function get() {
                    return this.invalid ? this.invalid.reason : null;
                }
                /**
                 * Returns an explanation of why this Duration became invalid, or null if the Duration is valid
                 * @type {string}
                 */
            }, {
                key: "invalidExplanation",
                get: function get() {
                    return this.invalid ? this.invalid.explanation : null;
                }
            }]);
        return Duration;
    }();
    function friendlyDuration(durationish) {
        if (isNumber(durationish)) {
            return Duration.fromMillis(durationish);
        }
        else if (Duration.isDuration(durationish)) {
            return durationish;
        }
        else if (_typeof(durationish) === "object") {
            return Duration.fromObject(durationish);
        }
        else {
            throw new InvalidArgumentError("Unknown duration argument " + durationish + " of type " + _typeof(durationish));
        }
    }
    var INVALID$1 = "Invalid Interval"; // checks if the start is equal to or before the end
    function validateStartEnd(start, end) {
        if (!start || !start.isValid) {
            return Interval.invalid("missing or invalid start");
        }
        else if (!end || !end.isValid) {
            return Interval.invalid("missing or invalid end");
        }
        else if (end < start) {
            return Interval.invalid("end before start", "The end of an interval must be after its start, but you had start=" + start.toISO() + " and end=" + end.toISO());
        }
        else {
            return null;
        }
    }
    /**
     * An Interval object represents a half-open interval of time, where each endpoint is a {@link DateTime}. Conceptually, it's a container for those two endpoints, accompanied by methods for creating, parsing, interrogating, comparing, transforming, and formatting them.
     *
     * Here is a brief overview of the most commonly used methods and getters in Interval:
     *
     * * **Creation** To create an Interval, use {@link fromDateTimes}, {@link after}, {@link before}, or {@link fromISO}.
     * * **Accessors** Use {@link start} and {@link end} to get the start and end.
     * * **Interrogation** To analyze the Interval, use {@link count}, {@link length}, {@link hasSame}, {@link contains}, {@link isAfter}, or {@link isBefore}.
     * * **Transformation** To create other Intervals out of this one, use {@link set}, {@link splitAt}, {@link splitBy}, {@link divideEqually}, {@link merge}, {@link xor}, {@link union}, {@link intersection}, or {@link difference}.
     * * **Comparison** To compare this Interval to another one, use {@link equals}, {@link overlaps}, {@link abutsStart}, {@link abutsEnd}, {@link engulfs}
     * * **Output** To convert the Interval into other representations, see {@link toString}, {@link toISO}, {@link toISODate}, {@link toISOTime}, {@link toFormat}, and {@link toDuration}.
     */
    var Interval = 
    /*#__PURE__*/
    function () {
        /**
         * @private
         */
        function Interval(config) {
            /**
             * @access private
             */
            this.s = config.start;
            /**
             * @access private
             */
            this.e = config.end;
            /**
             * @access private
             */
            this.invalid = config.invalid || null;
            /**
             * @access private
             */
            this.isLuxonInterval = true;
        }
        /**
         * Create an invalid Interval.
         * @param {string} reason - simple string of why this Interval is invalid. Should not contain parameters or anything else data-dependent
         * @param {string} [explanation=null] - longer explanation, may include parameters and other useful debugging information
         * @return {Interval}
         */
        Interval.invalid = function invalid(reason, explanation) {
            if (explanation === void 0) {
                explanation = null;
            }
            if (!reason) {
                throw new InvalidArgumentError("need to specify a reason the Interval is invalid");
            }
            var invalid = reason instanceof Invalid ? reason : new Invalid(reason, explanation);
            if (Settings.throwOnInvalid) {
                throw new InvalidIntervalError(invalid);
            }
            else {
                return new Interval({
                    invalid: invalid
                });
            }
        };
        Interval.fromDateTimes = function fromDateTimes(start, end) {
            var builtStart = friendlyDateTime(start), builtEnd = friendlyDateTime(end);
            var validateError = validateStartEnd(builtStart, builtEnd);
            if (validateError == null) {
                return new Interval({
                    start: builtStart,
                    end: builtEnd
                });
            }
            else {
                return validateError;
            }
        };
        Interval.after = function after(start, duration) {
            var dur = friendlyDuration(duration), dt = friendlyDateTime(start);
            return Interval.fromDateTimes(dt, dt.plus(dur));
        };
        Interval.before = function before(end, duration) {
            var dur = friendlyDuration(duration), dt = friendlyDateTime(end);
            return Interval.fromDateTimes(dt.minus(dur), dt);
        };
        Interval.fromISO = function fromISO(text, opts) {
            var _split = (text || "").split("/", 2), s = _split[0], e = _split[1];
            if (s && e) {
                var start = DateTime.fromISO(s, opts), end = DateTime.fromISO(e, opts);
                if (start.isValid && end.isValid) {
                    return Interval.fromDateTimes(start, end);
                }
                if (start.isValid) {
                    var dur = Duration.fromISO(e, opts);
                    if (dur.isValid) {
                        return Interval.after(start, dur);
                    }
                }
                else if (end.isValid) {
                    var _dur = Duration.fromISO(s, opts);
                    if (_dur.isValid) {
                        return Interval.before(end, _dur);
                    }
                }
            }
            return Interval.invalid("unparsable", "the input \"" + text + "\" can't be parsed asISO 8601");
        };
        Interval.isInterval = function isInterval(o) {
            return o && o.isLuxonInterval || false;
        };
        var _proto = Interval.prototype;
        /**
         * Returns the length of the Interval in the specified unit.
         * @param {string} unit - the unit (such as 'hours' or 'days') to return the length in.
         * @return {number}
         */
        _proto.length = function length(unit) {
            if (unit === void 0) {
                unit = "milliseconds";
            }
            return this.isValid ? this.toDuration.apply(this, [unit]).get(unit) : NaN;
        };
        _proto.count = function count(unit) {
            if (unit === void 0) {
                unit = "milliseconds";
            }
            if (!this.isValid)
                return NaN;
            var start = this.start.startOf(unit), end = this.end.startOf(unit);
            return Math.floor(end.diff(start, unit).get(unit)) + 1;
        };
        _proto.hasSame = function hasSame(unit) {
            return this.isValid ? this.e.minus(1).hasSame(this.s, unit) : false;
        };
        _proto.isEmpty = function isEmpty() {
            return this.s.valueOf() === this.e.valueOf();
        };
        _proto.isAfter = function isAfter(dateTime) {
            if (!this.isValid)
                return false;
            return this.s > dateTime;
        };
        _proto.isBefore = function isBefore(dateTime) {
            if (!this.isValid)
                return false;
            return this.e <= dateTime;
        };
        _proto.contains = function contains(dateTime) {
            if (!this.isValid)
                return false;
            return this.s <= dateTime && this.e > dateTime;
        };
        _proto.set = function set(_temp) {
            var _ref = _temp === void 0 ? {} : _temp, start = _ref.start, end = _ref.end;
            if (!this.isValid)
                return this;
            return Interval.fromDateTimes(start || this.s, end || this.e);
        };
        _proto.splitAt = function splitAt() {
            var _this = this;
            if (!this.isValid)
                return [];
            for (var _len = arguments.length, dateTimes = new Array(_len), _key = 0; _key < _len; _key++) {
                dateTimes[_key] = arguments[_key];
            }
            var sorted = dateTimes.map(friendlyDateTime).filter(function (d) {
                return _this.contains(d);
            }).sort(), results = [];
            var s = this.s, i = 0;
            while (s < this.e) {
                var added = sorted[i] || this.e, next = +added > +this.e ? this.e : added;
                results.push(Interval.fromDateTimes(s, next));
                s = next;
                i += 1;
            }
            return results;
        };
        _proto.splitBy = function splitBy(duration) {
            var dur = friendlyDuration(duration);
            if (!this.isValid || !dur.isValid || dur.as("milliseconds") === 0) {
                return [];
            }
            var s = this.s, added, next;
            var results = [];
            while (s < this.e) {
                added = s.plus(dur);
                next = +added > +this.e ? this.e : added;
                results.push(Interval.fromDateTimes(s, next));
                s = next;
            }
            return results;
        };
        _proto.divideEqually = function divideEqually(numberOfParts) {
            if (!this.isValid)
                return [];
            return this.splitBy(this.length() / numberOfParts).slice(0, numberOfParts);
        };
        _proto.overlaps = function overlaps(other) {
            return this.e > other.s && this.s < other.e;
        };
        _proto.abutsStart = function abutsStart(other) {
            if (!this.isValid)
                return false;
            return +this.e === +other.s;
        };
        _proto.abutsEnd = function abutsEnd(other) {
            if (!this.isValid)
                return false;
            return +other.e === +this.s;
        };
        _proto.engulfs = function engulfs(other) {
            if (!this.isValid)
                return false;
            return this.s <= other.s && this.e >= other.e;
        };
        _proto.equals = function equals(other) {
            if (!this.isValid || !other.isValid) {
                return false;
            }
            return this.s.equals(other.s) && this.e.equals(other.e);
        };
        _proto.intersection = function intersection(other) {
            if (!this.isValid)
                return this;
            var s = this.s > other.s ? this.s : other.s, e = this.e < other.e ? this.e : other.e;
            if (s > e) {
                return null;
            }
            else {
                return Interval.fromDateTimes(s, e);
            }
        };
        _proto.union = function union(other) {
            if (!this.isValid)
                return this;
            var s = this.s < other.s ? this.s : other.s, e = this.e > other.e ? this.e : other.e;
            return Interval.fromDateTimes(s, e);
        };
        Interval.merge = function merge(intervals) {
            var _intervals$sort$reduc = intervals.sort(function (a, b) {
                return a.s - b.s;
            }).reduce(function (_ref2, item) {
                var sofar = _ref2[0], current = _ref2[1];
                if (!current) {
                    return [sofar, item];
                }
                else if (current.overlaps(item) || current.abutsStart(item)) {
                    return [sofar, current.union(item)];
                }
                else {
                    return [sofar.concat([current]), item];
                }
            }, [[], null]), found = _intervals$sort$reduc[0], _final = _intervals$sort$reduc[1];
            if (_final) {
                found.push(_final);
            }
            return found;
        };
        Interval.xor = function xor(intervals) {
            var _Array$prototype;
            var start = null, currentCount = 0;
            var results = [], ends = intervals.map(function (i) {
                return [{
                        time: i.s,
                        type: "s"
                    }, {
                        time: i.e,
                        type: "e"
                    }];
            }), flattened = (_Array$prototype = Array.prototype).concat.apply(_Array$prototype, ends), arr = flattened.sort(function (a, b) {
                return a.time - b.time;
            });
            for (var _iterator = arr, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
                var _ref3;
                if (_isArray) {
                    if (_i >= _iterator.length)
                        break;
                    _ref3 = _iterator[_i++];
                }
                else {
                    _i = _iterator.next();
                    if (_i.done)
                        break;
                    _ref3 = _i.value;
                }
                var i = _ref3;
                currentCount += i.type === "s" ? 1 : -1;
                if (currentCount === 1) {
                    start = i.time;
                }
                else {
                    if (start && +start !== +i.time) {
                        results.push(Interval.fromDateTimes(start, i.time));
                    }
                    start = null;
                }
            }
            return Interval.merge(results);
        };
        _proto.difference = function difference() {
            var _this2 = this;
            for (var _len2 = arguments.length, intervals = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                intervals[_key2] = arguments[_key2];
            }
            return Interval.xor([this].concat(intervals)).map(function (i) {
                return _this2.intersection(i);
            }).filter(function (i) {
                return i && !i.isEmpty();
            });
        };
        _proto.toString = function toString() {
            if (!this.isValid)
                return INVALID$1;
            return "[" + this.s.toISO() + " \u2013 " + this.e.toISO() + ")";
        };
        _proto.toISO = function toISO(opts) {
            if (!this.isValid)
                return INVALID$1;
            return this.s.toISO(opts) + "/" + this.e.toISO(opts);
        };
        _proto.toISODate = function toISODate() {
            if (!this.isValid)
                return INVALID$1;
            return this.s.toISODate() + "/" + this.e.toISODate();
        };
        _proto.toISOTime = function toISOTime(opts) {
            if (!this.isValid)
                return INVALID$1;
            return this.s.toISOTime(opts) + "/" + this.e.toISOTime(opts);
        };
        _proto.toFormat = function toFormat(dateFormat, _temp2) {
            var _ref4 = _temp2 === void 0 ? {} : _temp2, _ref4$separator = _ref4.separator, separator = _ref4$separator === void 0 ? " – " : _ref4$separator;
            if (!this.isValid)
                return INVALID$1;
            return "" + this.s.toFormat(dateFormat) + separator + this.e.toFormat(dateFormat);
        };
        _proto.toDuration = function toDuration(unit, opts) {
            if (!this.isValid) {
                return Duration.invalid(this.invalidReason);
            }
            return this.e.diff(this.s, unit, opts);
        };
        _proto.mapEndpoints = function mapEndpoints(mapFn) {
            return Interval.fromDateTimes(mapFn(this.s), mapFn(this.e));
        };
        _createClass(Interval, [{
                key: "start",
                get: function get() {
                    return this.isValid ? this.s : null;
                }
                /**
                 * Returns the end of the Interval
                 * @type {DateTime}
                 */
            }, {
                key: "end",
                get: function get() {
                    return this.isValid ? this.e : null;
                }
                /**
                 * Returns whether this Interval's end is at least its start, meaning that the Interval isn't 'backwards'.
                 * @type {boolean}
                 */
            }, {
                key: "isValid",
                get: function get() {
                    return this.invalidReason === null;
                }
                /**
                 * Returns an error code if this Interval is invalid, or null if the Interval is valid
                 * @type {string}
                 */
            }, {
                key: "invalidReason",
                get: function get() {
                    return this.invalid ? this.invalid.reason : null;
                }
                /**
                 * Returns an explanation of why this Interval became invalid, or null if the Interval is valid
                 * @type {string}
                 */
            }, {
                key: "invalidExplanation",
                get: function get() {
                    return this.invalid ? this.invalid.explanation : null;
                }
            }]);
        return Interval;
    }();
    /**
     * The Info class contains static methods for retrieving general time and date related data. For example, it has methods for finding out if a time zone has a DST, for listing the months in any supported locale, and for discovering which of Luxon features are available in the current environment.
     */
    var Info = 
    /*#__PURE__*/
    function () {
        function Info() { }
        /**
         * Return whether the specified zone contains a DST.
         * @param {string|Zone} [zone='local'] - Zone to check. Defaults to the environment's local zone.
         * @return {boolean}
         */
        Info.hasDST = function hasDST(zone) {
            if (zone === void 0) {
                zone = Settings.defaultZone;
            }
            var proto = DateTime.local().setZone(zone).set({
                month: 12
            });
            return !zone.universal && proto.offset !== proto.set({
                month: 6
            }).offset;
        };
        Info.isValidIANAZone = function isValidIANAZone(zone) {
            return IANAZone.isValidSpecifier(zone) && IANAZone.isValidZone(zone);
        };
        Info.normalizeZone = function normalizeZone$1(input) {
            return normalizeZone(input, Settings.defaultZone);
        };
        Info.months = function months(length, _temp) {
            if (length === void 0) {
                length = "long";
            }
            var _ref = _temp === void 0 ? {} : _temp, _ref$locale = _ref.locale, locale = _ref$locale === void 0 ? null : _ref$locale, _ref$numberingSystem = _ref.numberingSystem, numberingSystem = _ref$numberingSystem === void 0 ? null : _ref$numberingSystem, _ref$outputCalendar = _ref.outputCalendar, outputCalendar = _ref$outputCalendar === void 0 ? "gregory" : _ref$outputCalendar;
            return Locale.create(locale, numberingSystem, outputCalendar).months(length);
        };
        Info.monthsFormat = function monthsFormat(length, _temp2) {
            if (length === void 0) {
                length = "long";
            }
            var _ref2 = _temp2 === void 0 ? {} : _temp2, _ref2$locale = _ref2.locale, locale = _ref2$locale === void 0 ? null : _ref2$locale, _ref2$numberingSystem = _ref2.numberingSystem, numberingSystem = _ref2$numberingSystem === void 0 ? null : _ref2$numberingSystem, _ref2$outputCalendar = _ref2.outputCalendar, outputCalendar = _ref2$outputCalendar === void 0 ? "gregory" : _ref2$outputCalendar;
            return Locale.create(locale, numberingSystem, outputCalendar).months(length, true);
        };
        Info.weekdays = function weekdays(length, _temp3) {
            if (length === void 0) {
                length = "long";
            }
            var _ref3 = _temp3 === void 0 ? {} : _temp3, _ref3$locale = _ref3.locale, locale = _ref3$locale === void 0 ? null : _ref3$locale, _ref3$numberingSystem = _ref3.numberingSystem, numberingSystem = _ref3$numberingSystem === void 0 ? null : _ref3$numberingSystem;
            return Locale.create(locale, numberingSystem, null).weekdays(length);
        };
        Info.weekdaysFormat = function weekdaysFormat(length, _temp4) {
            if (length === void 0) {
                length = "long";
            }
            var _ref4 = _temp4 === void 0 ? {} : _temp4, _ref4$locale = _ref4.locale, locale = _ref4$locale === void 0 ? null : _ref4$locale, _ref4$numberingSystem = _ref4.numberingSystem, numberingSystem = _ref4$numberingSystem === void 0 ? null : _ref4$numberingSystem;
            return Locale.create(locale, numberingSystem, null).weekdays(length, true);
        };
        Info.meridiems = function meridiems(_temp5) {
            var _ref5 = _temp5 === void 0 ? {} : _temp5, _ref5$locale = _ref5.locale, locale = _ref5$locale === void 0 ? null : _ref5$locale;
            return Locale.create(locale).meridiems();
        };
        Info.eras = function eras(length, _temp6) {
            if (length === void 0) {
                length = "short";
            }
            var _ref6 = _temp6 === void 0 ? {} : _temp6, _ref6$locale = _ref6.locale, locale = _ref6$locale === void 0 ? null : _ref6$locale;
            return Locale.create(locale, null, "gregory").eras(length);
        };
        Info.features = function features() {
            var intl = false, intlTokens = false, zones = false, relative = false;
            if (hasIntl()) {
                intl = true;
                intlTokens = hasFormatToParts();
                relative = hasRelative();
                try {
                    zones = new Intl.DateTimeFormat("en", {
                        timeZone: "America/New_York"
                    }).resolvedOptions().timeZone === "America/New_York";
                }
                catch (e) {
                    zones = false;
                }
            }
            return {
                intl: intl,
                intlTokens: intlTokens,
                zones: zones,
                relative: relative
            };
        };
        return Info;
    }();
    function dayDiff(earlier, later) {
        var utcDayStart = function utcDayStart(dt) {
            return dt.toUTC(0, {
                keepLocalTime: true
            }).startOf("day").valueOf();
        }, ms = utcDayStart(later) - utcDayStart(earlier);
        return Math.floor(Duration.fromMillis(ms).as("days"));
    }
    function highOrderDiffs(cursor, later, units) {
        var differs = [["years", function (a, b) {
                    return b.year - a.year;
                }], ["months", function (a, b) {
                    return b.month - a.month + (b.year - a.year) * 12;
                }], ["weeks", function (a, b) {
                    var days = dayDiff(a, b);
                    return (days - days % 7) / 7;
                }], ["days", dayDiff]];
        var results = {};
        var lowestOrder, highWater;
        for (var _i = 0, _differs = differs; _i < _differs.length; _i++) {
            var _differs$_i = _differs[_i], unit = _differs$_i[0], differ = _differs$_i[1];
            if (units.indexOf(unit) >= 0) {
                var _cursor$plus;
                lowestOrder = unit;
                var delta = differ(cursor, later);
                highWater = cursor.plus((_cursor$plus = {}, _cursor$plus[unit] = delta, _cursor$plus));
                if (highWater > later) {
                    var _cursor$plus2;
                    cursor = cursor.plus((_cursor$plus2 = {}, _cursor$plus2[unit] = delta - 1, _cursor$plus2));
                    delta -= 1;
                }
                else {
                    cursor = highWater;
                }
                results[unit] = delta;
            }
        }
        return [cursor, results, highWater, lowestOrder];
    }
    function _diff(earlier, later, units, opts) {
        var _highOrderDiffs = highOrderDiffs(earlier, later, units), cursor = _highOrderDiffs[0], results = _highOrderDiffs[1], highWater = _highOrderDiffs[2], lowestOrder = _highOrderDiffs[3];
        var remainingMillis = later - cursor;
        var lowerOrderUnits = units.filter(function (u) {
            return ["hours", "minutes", "seconds", "milliseconds"].indexOf(u) >= 0;
        });
        if (lowerOrderUnits.length === 0) {
            if (highWater < later) {
                var _cursor$plus3;
                highWater = cursor.plus((_cursor$plus3 = {}, _cursor$plus3[lowestOrder] = 1, _cursor$plus3));
            }
            if (highWater !== cursor) {
                results[lowestOrder] = (results[lowestOrder] || 0) + remainingMillis / (highWater - cursor);
            }
        }
        var duration = Duration.fromObject(Object.assign(results, opts));
        if (lowerOrderUnits.length > 0) {
            var _Duration$fromMillis;
            return (_Duration$fromMillis = Duration.fromMillis(remainingMillis, opts)).shiftTo.apply(_Duration$fromMillis, lowerOrderUnits).plus(duration);
        }
        else {
            return duration;
        }
    }
    var numberingSystems = {
        arab: "[\u0660-\u0669]",
        arabext: "[\u06F0-\u06F9]",
        bali: "[\u1B50-\u1B59]",
        beng: "[\u09E6-\u09EF]",
        deva: "[\u0966-\u096F]",
        fullwide: "[\uFF10-\uFF19]",
        gujr: "[\u0AE6-\u0AEF]",
        hanidec: "[〇|一|二|三|四|五|六|七|八|九]",
        khmr: "[\u17E0-\u17E9]",
        knda: "[\u0CE6-\u0CEF]",
        laoo: "[\u0ED0-\u0ED9]",
        limb: "[\u1946-\u194F]",
        mlym: "[\u0D66-\u0D6F]",
        mong: "[\u1810-\u1819]",
        mymr: "[\u1040-\u1049]",
        orya: "[\u0B66-\u0B6F]",
        tamldec: "[\u0BE6-\u0BEF]",
        telu: "[\u0C66-\u0C6F]",
        thai: "[\u0E50-\u0E59]",
        tibt: "[\u0F20-\u0F29]",
        latn: "\\d"
    };
    var numberingSystemsUTF16 = {
        arab: [1632, 1641],
        arabext: [1776, 1785],
        bali: [6992, 7001],
        beng: [2534, 2543],
        deva: [2406, 2415],
        fullwide: [65296, 65303],
        gujr: [2790, 2799],
        khmr: [6112, 6121],
        knda: [3302, 3311],
        laoo: [3792, 3801],
        limb: [6470, 6479],
        mlym: [3430, 3439],
        mong: [6160, 6169],
        mymr: [4160, 4169],
        orya: [2918, 2927],
        tamldec: [3046, 3055],
        telu: [3174, 3183],
        thai: [3664, 3673],
        tibt: [3872, 3881]
    }; // eslint-disable-next-line
    var hanidecChars = numberingSystems.hanidec.replace(/[\[|\]]/g, "").split("");
    function parseDigits(str) {
        var value = parseInt(str, 10);
        if (isNaN(value)) {
            value = "";
            for (var i = 0; i < str.length; i++) {
                var code = str.charCodeAt(i);
                if (str[i].search(numberingSystems.hanidec) !== -1) {
                    value += hanidecChars.indexOf(str[i]);
                }
                else {
                    for (var key in numberingSystemsUTF16) {
                        var _numberingSystemsUTF = numberingSystemsUTF16[key], min = _numberingSystemsUTF[0], max = _numberingSystemsUTF[1];
                        if (code >= min && code <= max) {
                            value += code - min;
                        }
                    }
                }
            }
            return parseInt(value, 10);
        }
        else {
            return value;
        }
    }
    function digitRegex(_ref, append) {
        var numberingSystem = _ref.numberingSystem;
        if (append === void 0) {
            append = "";
        }
        return new RegExp("" + numberingSystems[numberingSystem || "latn"] + append);
    }
    var MISSING_FTP = "missing Intl.DateTimeFormat.formatToParts support";
    function intUnit(regex, post) {
        if (post === void 0) {
            post = function post(i) {
                return i;
            };
        }
        return {
            regex: regex,
            deser: function deser(_ref) {
                var s = _ref[0];
                return post(parseDigits(s));
            }
        };
    }
    function fixListRegex(s) {
        // make dots optional and also make them literal
        return s.replace(/\./, "\\.?");
    }
    function stripInsensitivities(s) {
        return s.replace(/\./, "").toLowerCase();
    }
    function oneOf(strings, startIndex) {
        if (strings === null) {
            return null;
        }
        else {
            return {
                regex: RegExp(strings.map(fixListRegex).join("|")),
                deser: function deser(_ref2) {
                    var s = _ref2[0];
                    return strings.findIndex(function (i) {
                        return stripInsensitivities(s) === stripInsensitivities(i);
                    }) + startIndex;
                }
            };
        }
    }
    function offset(regex, groups) {
        return {
            regex: regex,
            deser: function deser(_ref3) {
                var h = _ref3[1], m = _ref3[2];
                return signedOffset(h, m);
            },
            groups: groups
        };
    }
    function simple(regex) {
        return {
            regex: regex,
            deser: function deser(_ref4) {
                var s = _ref4[0];
                return s;
            }
        };
    }
    function escapeToken(value) {
        // eslint-disable-next-line no-useless-escape
        return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
    }
    function unitForToken(token, loc) {
        var one = digitRegex(loc), two = digitRegex(loc, "{2}"), three = digitRegex(loc, "{3}"), four = digitRegex(loc, "{4}"), six = digitRegex(loc, "{6}"), oneOrTwo = digitRegex(loc, "{1,2}"), oneToThree = digitRegex(loc, "{1,3}"), oneToSix = digitRegex(loc, "{1,6}"), oneToNine = digitRegex(loc, "{1,9}"), twoToFour = digitRegex(loc, "{2,4}"), fourToSix = digitRegex(loc, "{4,6}"), literal = function literal(t) {
            return {
                regex: RegExp(escapeToken(t.val)),
                deser: function deser(_ref5) {
                    var s = _ref5[0];
                    return s;
                },
                literal: true
            };
        }, unitate = function unitate(t) {
            if (token.literal) {
                return literal(t);
            }
            switch (t.val) {
                // era
                case "G":
                    return oneOf(loc.eras("short", false), 0);
                case "GG":
                    return oneOf(loc.eras("long", false), 0);
                // years
                case "y":
                    return intUnit(oneToSix);
                case "yy":
                    return intUnit(twoToFour, untruncateYear);
                case "yyyy":
                    return intUnit(four);
                case "yyyyy":
                    return intUnit(fourToSix);
                case "yyyyyy":
                    return intUnit(six);
                // months
                case "M":
                    return intUnit(oneOrTwo);
                case "MM":
                    return intUnit(two);
                case "MMM":
                    return oneOf(loc.months("short", true, false), 1);
                case "MMMM":
                    return oneOf(loc.months("long", true, false), 1);
                case "L":
                    return intUnit(oneOrTwo);
                case "LL":
                    return intUnit(two);
                case "LLL":
                    return oneOf(loc.months("short", false, false), 1);
                case "LLLL":
                    return oneOf(loc.months("long", false, false), 1);
                // dates
                case "d":
                    return intUnit(oneOrTwo);
                case "dd":
                    return intUnit(two);
                // ordinals
                case "o":
                    return intUnit(oneToThree);
                case "ooo":
                    return intUnit(three);
                // time
                case "HH":
                    return intUnit(two);
                case "H":
                    return intUnit(oneOrTwo);
                case "hh":
                    return intUnit(two);
                case "h":
                    return intUnit(oneOrTwo);
                case "mm":
                    return intUnit(two);
                case "m":
                    return intUnit(oneOrTwo);
                case "q":
                    return intUnit(oneOrTwo);
                case "qq":
                    return intUnit(two);
                case "s":
                    return intUnit(oneOrTwo);
                case "ss":
                    return intUnit(two);
                case "S":
                    return intUnit(oneToThree);
                case "SSS":
                    return intUnit(three);
                case "u":
                    return simple(oneToNine);
                // meridiem
                case "a":
                    return oneOf(loc.meridiems(), 0);
                // weekYear (k)
                case "kkkk":
                    return intUnit(four);
                case "kk":
                    return intUnit(twoToFour, untruncateYear);
                // weekNumber (W)
                case "W":
                    return intUnit(oneOrTwo);
                case "WW":
                    return intUnit(two);
                // weekdays
                case "E":
                case "c":
                    return intUnit(one);
                case "EEE":
                    return oneOf(loc.weekdays("short", false, false), 1);
                case "EEEE":
                    return oneOf(loc.weekdays("long", false, false), 1);
                case "ccc":
                    return oneOf(loc.weekdays("short", true, false), 1);
                case "cccc":
                    return oneOf(loc.weekdays("long", true, false), 1);
                // offset/zone
                case "Z":
                case "ZZ":
                    return offset(new RegExp("([+-]" + oneOrTwo.source + ")(?::(" + two.source + "))?"), 2);
                case "ZZZ":
                    return offset(new RegExp("([+-]" + oneOrTwo.source + ")(" + two.source + ")?"), 2);
                // we don't support ZZZZ (PST) or ZZZZZ (Pacific Standard Time) in parsing
                // because we don't have any way to figure out what they are
                case "z":
                    return simple(/[a-z_+-/]{1,256}?/i);
                default:
                    return literal(t);
            }
        };
        var unit = unitate(token) || {
            invalidReason: MISSING_FTP
        };
        unit.token = token;
        return unit;
    }
    var partTypeStyleToTokenVal = {
        year: {
            "2-digit": "yy",
            numeric: "yyyyy"
        },
        month: {
            numeric: "M",
            "2-digit": "MM",
            "short": "MMM",
            "long": "MMMM"
        },
        day: {
            numeric: "d",
            "2-digit": "dd"
        },
        weekday: {
            "short": "EEE",
            "long": "EEEE"
        },
        dayperiod: "a",
        dayPeriod: "a",
        hour: {
            numeric: "h",
            "2-digit": "hh"
        },
        minute: {
            numeric: "m",
            "2-digit": "mm"
        },
        second: {
            numeric: "s",
            "2-digit": "ss"
        }
    };
    function tokenForPart(part, locale, formatOpts) {
        var type = part.type, value = part.value;
        if (type === "literal") {
            return {
                literal: true,
                val: value
            };
        }
        var style = formatOpts[type];
        var val = partTypeStyleToTokenVal[type];
        if (_typeof(val) === "object") {
            val = val[style];
        }
        if (val) {
            return {
                literal: false,
                val: val
            };
        }
        return undefined;
    }
    function buildRegex(units) {
        var re = units.map(function (u) {
            return u.regex;
        }).reduce(function (f, r) {
            return f + "(" + r.source + ")";
        }, "");
        return ["^" + re + "$", units];
    }
    function match(input, regex, handlers) {
        var matches = input.match(regex);
        if (matches) {
            var all = {};
            var matchIndex = 1;
            for (var i in handlers) {
                if (hasOwnProperty(handlers, i)) {
                    var h = handlers[i], groups = h.groups ? h.groups + 1 : 1;
                    if (!h.literal && h.token) {
                        all[h.token.val[0]] = h.deser(matches.slice(matchIndex, matchIndex + groups));
                    }
                    matchIndex += groups;
                }
            }
            return [matches, all];
        }
        else {
            return [matches, {}];
        }
    }
    function dateTimeFromMatches(matches) {
        var toField = function toField(token) {
            switch (token) {
                case "S":
                    return "millisecond";
                case "s":
                    return "second";
                case "m":
                    return "minute";
                case "h":
                case "H":
                    return "hour";
                case "d":
                    return "day";
                case "o":
                    return "ordinal";
                case "L":
                case "M":
                    return "month";
                case "y":
                    return "year";
                case "E":
                case "c":
                    return "weekday";
                case "W":
                    return "weekNumber";
                case "k":
                    return "weekYear";
                case "q":
                    return "quarter";
                default:
                    return null;
            }
        };
        var zone;
        if (!isUndefined(matches.Z)) {
            zone = new FixedOffsetZone(matches.Z);
        }
        else if (!isUndefined(matches.z)) {
            zone = IANAZone.create(matches.z);
        }
        else {
            zone = null;
        }
        if (!isUndefined(matches.q)) {
            matches.M = (matches.q - 1) * 3 + 1;
        }
        if (!isUndefined(matches.h)) {
            if (matches.h < 12 && matches.a === 1) {
                matches.h += 12;
            }
            else if (matches.h === 12 && matches.a === 0) {
                matches.h = 0;
            }
        }
        if (matches.G === 0 && matches.y) {
            matches.y = -matches.y;
        }
        if (!isUndefined(matches.u)) {
            matches.S = parseMillis(matches.u);
        }
        var vals = Object.keys(matches).reduce(function (r, k) {
            var f = toField(k);
            if (f) {
                r[f] = matches[k];
            }
            return r;
        }, {});
        return [vals, zone];
    }
    var dummyDateTimeCache = null;
    function getDummyDateTime() {
        if (!dummyDateTimeCache) {
            dummyDateTimeCache = DateTime.fromMillis(1555555555555);
        }
        return dummyDateTimeCache;
    }
    function maybeExpandMacroToken(token, locale) {
        if (token.literal) {
            return token;
        }
        var formatOpts = Formatter.macroTokenToFormatOpts(token.val);
        if (!formatOpts) {
            return token;
        }
        var formatter = Formatter.create(locale, formatOpts);
        var parts = formatter.formatDateTimeParts(getDummyDateTime());
        var tokens = parts.map(function (p) {
            return tokenForPart(p, locale, formatOpts);
        });
        if (tokens.includes(undefined)) {
            return token;
        }
        return tokens;
    }
    function expandMacroTokens(tokens, locale) {
        var _Array$prototype;
        return (_Array$prototype = Array.prototype).concat.apply(_Array$prototype, tokens.map(function (t) {
            return maybeExpandMacroToken(t, locale);
        }));
    }
    /**
     * @private
     */
    function explainFromTokens(locale, input, format) {
        var tokens = expandMacroTokens(Formatter.parseFormat(format), locale), units = tokens.map(function (t) {
            return unitForToken(t, locale);
        }), disqualifyingUnit = units.find(function (t) {
            return t.invalidReason;
        });
        if (disqualifyingUnit) {
            return {
                input: input,
                tokens: tokens,
                invalidReason: disqualifyingUnit.invalidReason
            };
        }
        else {
            var _buildRegex = buildRegex(units), regexString = _buildRegex[0], handlers = _buildRegex[1], regex = RegExp(regexString, "i"), _match = match(input, regex, handlers), rawMatches = _match[0], matches = _match[1], _ref6 = matches ? dateTimeFromMatches(matches) : [null, null], result = _ref6[0], zone = _ref6[1];
            return {
                input: input,
                tokens: tokens,
                regex: regex,
                rawMatches: rawMatches,
                matches: matches,
                result: result,
                zone: zone
            };
        }
    }
    function parseFromTokens(locale, input, format) {
        var _explainFromTokens = explainFromTokens(locale, input, format), result = _explainFromTokens.result, zone = _explainFromTokens.zone, invalidReason = _explainFromTokens.invalidReason;
        return [result, zone, invalidReason];
    }
    var nonLeapLadder = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334], leapLadder = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335];
    function unitOutOfRange(unit, value) {
        return new Invalid("unit out of range", "you specified " + value + " (of type " + _typeof(value) + ") as a " + unit + ", which is invalid");
    }
    function dayOfWeek(year, month, day) {
        var js = new Date(Date.UTC(year, month - 1, day)).getUTCDay();
        return js === 0 ? 7 : js;
    }
    function computeOrdinal(year, month, day) {
        return day + (isLeapYear(year) ? leapLadder : nonLeapLadder)[month - 1];
    }
    function uncomputeOrdinal(year, ordinal) {
        var table = isLeapYear(year) ? leapLadder : nonLeapLadder, month0 = table.findIndex(function (i) {
            return i < ordinal;
        }), day = ordinal - table[month0];
        return {
            month: month0 + 1,
            day: day
        };
    }
    /**
     * @private
     */
    function gregorianToWeek(gregObj) {
        var year = gregObj.year, month = gregObj.month, day = gregObj.day, ordinal = computeOrdinal(year, month, day), weekday = dayOfWeek(year, month, day);
        var weekNumber = Math.floor((ordinal - weekday + 10) / 7), weekYear;
        if (weekNumber < 1) {
            weekYear = year - 1;
            weekNumber = weeksInWeekYear(weekYear);
        }
        else if (weekNumber > weeksInWeekYear(year)) {
            weekYear = year + 1;
            weekNumber = 1;
        }
        else {
            weekYear = year;
        }
        return Object.assign({
            weekYear: weekYear,
            weekNumber: weekNumber,
            weekday: weekday
        }, timeObject(gregObj));
    }
    function weekToGregorian(weekData) {
        var weekYear = weekData.weekYear, weekNumber = weekData.weekNumber, weekday = weekData.weekday, weekdayOfJan4 = dayOfWeek(weekYear, 1, 4), yearInDays = daysInYear(weekYear);
        var ordinal = weekNumber * 7 + weekday - weekdayOfJan4 - 3, year;
        if (ordinal < 1) {
            year = weekYear - 1;
            ordinal += daysInYear(year);
        }
        else if (ordinal > yearInDays) {
            year = weekYear + 1;
            ordinal -= daysInYear(weekYear);
        }
        else {
            year = weekYear;
        }
        var _uncomputeOrdinal = uncomputeOrdinal(year, ordinal), month = _uncomputeOrdinal.month, day = _uncomputeOrdinal.day;
        return Object.assign({
            year: year,
            month: month,
            day: day
        }, timeObject(weekData));
    }
    function gregorianToOrdinal(gregData) {
        var year = gregData.year, month = gregData.month, day = gregData.day, ordinal = computeOrdinal(year, month, day);
        return Object.assign({
            year: year,
            ordinal: ordinal
        }, timeObject(gregData));
    }
    function ordinalToGregorian(ordinalData) {
        var year = ordinalData.year, ordinal = ordinalData.ordinal, _uncomputeOrdinal2 = uncomputeOrdinal(year, ordinal), month = _uncomputeOrdinal2.month, day = _uncomputeOrdinal2.day;
        return Object.assign({
            year: year,
            month: month,
            day: day
        }, timeObject(ordinalData));
    }
    function hasInvalidWeekData(obj) {
        var validYear = isInteger(obj.weekYear), validWeek = integerBetween(obj.weekNumber, 1, weeksInWeekYear(obj.weekYear)), validWeekday = integerBetween(obj.weekday, 1, 7);
        if (!validYear) {
            return unitOutOfRange("weekYear", obj.weekYear);
        }
        else if (!validWeek) {
            return unitOutOfRange("week", obj.week);
        }
        else if (!validWeekday) {
            return unitOutOfRange("weekday", obj.weekday);
        }
        else
            return false;
    }
    function hasInvalidOrdinalData(obj) {
        var validYear = isInteger(obj.year), validOrdinal = integerBetween(obj.ordinal, 1, daysInYear(obj.year));
        if (!validYear) {
            return unitOutOfRange("year", obj.year);
        }
        else if (!validOrdinal) {
            return unitOutOfRange("ordinal", obj.ordinal);
        }
        else
            return false;
    }
    function hasInvalidGregorianData(obj) {
        var validYear = isInteger(obj.year), validMonth = integerBetween(obj.month, 1, 12), validDay = integerBetween(obj.day, 1, daysInMonth(obj.year, obj.month));
        if (!validYear) {
            return unitOutOfRange("year", obj.year);
        }
        else if (!validMonth) {
            return unitOutOfRange("month", obj.month);
        }
        else if (!validDay) {
            return unitOutOfRange("day", obj.day);
        }
        else
            return false;
    }
    function hasInvalidTimeData(obj) {
        var hour = obj.hour, minute = obj.minute, second = obj.second, millisecond = obj.millisecond;
        var validHour = integerBetween(hour, 0, 23) || hour === 24 && minute === 0 && second === 0 && millisecond === 0, validMinute = integerBetween(minute, 0, 59), validSecond = integerBetween(second, 0, 59), validMillisecond = integerBetween(millisecond, 0, 999);
        if (!validHour) {
            return unitOutOfRange("hour", hour);
        }
        else if (!validMinute) {
            return unitOutOfRange("minute", minute);
        }
        else if (!validSecond) {
            return unitOutOfRange("second", second);
        }
        else if (!validMillisecond) {
            return unitOutOfRange("millisecond", millisecond);
        }
        else
            return false;
    }
    var INVALID$2 = "Invalid DateTime";
    var MAX_DATE = 8.64e15;
    function unsupportedZone(zone) {
        return new Invalid("unsupported zone", "the zone \"" + zone.name + "\" is not supported");
    } // we cache week data on the DT object and this intermediates the cache
    function possiblyCachedWeekData(dt) {
        if (dt.weekData === null) {
            dt.weekData = gregorianToWeek(dt.c);
        }
        return dt.weekData;
    } // clone really means, "make a new object with these modifications". all "setters" really use this
    // to create a new object while only changing some of the properties
    function clone$1(inst, alts) {
        var current = {
            ts: inst.ts,
            zone: inst.zone,
            c: inst.c,
            o: inst.o,
            loc: inst.loc,
            invalid: inst.invalid
        };
        return new DateTime(Object.assign({}, current, alts, {
            old: current
        }));
    } // find the right offset a given local time. The o input is our guess, which determines which
    // offset we'll pick in ambiguous cases (e.g. there are two 3 AMs b/c Fallback DST)
    function fixOffset(localTS, o, tz) {
        // Our UTC time is just a guess because our offset is just a guess
        var utcGuess = localTS - o * 60 * 1000; // Test whether the zone matches the offset for this ts
        var o2 = tz.offset(utcGuess); // If so, offset didn't change and we're done
        if (o === o2) {
            return [utcGuess, o];
        } // If not, change the ts by the difference in the offset
        utcGuess -= (o2 - o) * 60 * 1000; // If that gives us the local time we want, we're done
        var o3 = tz.offset(utcGuess);
        if (o2 === o3) {
            return [utcGuess, o2];
        } // If it's different, we're in a hole time. The offset has changed, but the we don't adjust the time
        return [localTS - Math.min(o2, o3) * 60 * 1000, Math.max(o2, o3)];
    } // convert an epoch timestamp into a calendar object with the given offset
    function tsToObj(ts, offset) {
        ts += offset * 60 * 1000;
        var d = new Date(ts);
        return {
            year: d.getUTCFullYear(),
            month: d.getUTCMonth() + 1,
            day: d.getUTCDate(),
            hour: d.getUTCHours(),
            minute: d.getUTCMinutes(),
            second: d.getUTCSeconds(),
            millisecond: d.getUTCMilliseconds()
        };
    } // convert a calendar object to a epoch timestamp
    function objToTS(obj, offset, zone) {
        return fixOffset(objToLocalTS(obj), offset, zone);
    } // create a new DT instance by adding a duration, adjusting for DSTs
    function adjustTime(inst, dur) {
        var _dur;
        var keys = Object.keys(dur.values);
        if (keys.indexOf("milliseconds") === -1) {
            keys.push("milliseconds");
        }
        dur = (_dur = dur).shiftTo.apply(_dur, keys);
        var oPre = inst.o, year = inst.c.year + dur.years, month = inst.c.month + dur.months + dur.quarters * 3, c = Object.assign({}, inst.c, {
            year: year,
            month: month,
            day: Math.min(inst.c.day, daysInMonth(year, month)) + dur.days + dur.weeks * 7
        }), millisToAdd = Duration.fromObject({
            hours: dur.hours,
            minutes: dur.minutes,
            seconds: dur.seconds,
            milliseconds: dur.milliseconds
        }).as("milliseconds"), localTS = objToLocalTS(c);
        var _fixOffset = fixOffset(localTS, oPre, inst.zone), ts = _fixOffset[0], o = _fixOffset[1];
        if (millisToAdd !== 0) {
            ts += millisToAdd; // that could have changed the offset by going over a DST, but we want to keep the ts the same
            o = inst.zone.offset(ts);
        }
        return {
            ts: ts,
            o: o
        };
    } // helper useful in turning the results of parsing into real dates
    // by handling the zone options
    function parseDataToDateTime(parsed, parsedZone, opts, format, text) {
        var setZone = opts.setZone, zone = opts.zone;
        if (parsed && Object.keys(parsed).length !== 0) {
            var interpretationZone = parsedZone || zone, inst = DateTime.fromObject(Object.assign(parsed, opts, {
                zone: interpretationZone,
                // setZone is a valid option in the calling methods, but not in fromObject
                setZone: undefined
            }));
            return setZone ? inst : inst.setZone(zone);
        }
        else {
            return DateTime.invalid(new Invalid("unparsable", "the input \"" + text + "\" can't be parsed as " + format));
        }
    } // if you want to output a technical format (e.g. RFC 2822), this helper
    // helps handle the details
    function toTechFormat(dt, format) {
        return dt.isValid ? Formatter.create(Locale.create("en-US"), {
            allowZ: true,
            forceSimple: true
        }).formatDateTimeFromString(dt, format) : null;
    } // technical time formats (e.g. the time part of ISO 8601), take some options
    // and this commonizes their handling
    function toTechTimeFormat(dt, _ref) {
        var _ref$suppressSeconds = _ref.suppressSeconds, suppressSeconds = _ref$suppressSeconds === void 0 ? false : _ref$suppressSeconds, _ref$suppressMillisec = _ref.suppressMilliseconds, suppressMilliseconds = _ref$suppressMillisec === void 0 ? false : _ref$suppressMillisec, includeOffset = _ref.includeOffset, _ref$includeZone = _ref.includeZone, includeZone = _ref$includeZone === void 0 ? false : _ref$includeZone, _ref$spaceZone = _ref.spaceZone, spaceZone = _ref$spaceZone === void 0 ? false : _ref$spaceZone;
        var fmt = "HH:mm";
        if (!suppressSeconds || dt.second !== 0 || dt.millisecond !== 0) {
            fmt += ":ss";
            if (!suppressMilliseconds || dt.millisecond !== 0) {
                fmt += ".SSS";
            }
        }
        if ((includeZone || includeOffset) && spaceZone) {
            fmt += " ";
        }
        if (includeZone) {
            fmt += "z";
        }
        else if (includeOffset) {
            fmt += "ZZ";
        }
        return toTechFormat(dt, fmt);
    } // defaults for unspecified units in the supported calendars
    var defaultUnitValues = {
        month: 1,
        day: 1,
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0
    }, defaultWeekUnitValues = {
        weekNumber: 1,
        weekday: 1,
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0
    }, defaultOrdinalUnitValues = {
        ordinal: 1,
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0
    }; // Units in the supported calendars, sorted by bigness
    var orderedUnits$1 = ["year", "month", "day", "hour", "minute", "second", "millisecond"], orderedWeekUnits = ["weekYear", "weekNumber", "weekday", "hour", "minute", "second", "millisecond"], orderedOrdinalUnits = ["year", "ordinal", "hour", "minute", "second", "millisecond"]; // standardize case and plurality in units
    function normalizeUnit(unit) {
        var normalized = {
            year: "year",
            years: "year",
            month: "month",
            months: "month",
            day: "day",
            days: "day",
            hour: "hour",
            hours: "hour",
            minute: "minute",
            minutes: "minute",
            quarter: "quarter",
            quarters: "quarter",
            second: "second",
            seconds: "second",
            millisecond: "millisecond",
            milliseconds: "millisecond",
            weekday: "weekday",
            weekdays: "weekday",
            weeknumber: "weekNumber",
            weeksnumber: "weekNumber",
            weeknumbers: "weekNumber",
            weekyear: "weekYear",
            weekyears: "weekYear",
            ordinal: "ordinal"
        }[unit.toLowerCase()];
        if (!normalized)
            throw new InvalidUnitError(unit);
        return normalized;
    } // this is a dumbed down version of fromObject() that runs about 60% faster
    // but doesn't do any validation, makes a bunch of assumptions about what units
    // are present, and so on.
    function quickDT(obj, zone) {
        // assume we have the higher-order units
        for (var _i = 0, _orderedUnits = orderedUnits$1; _i < _orderedUnits.length; _i++) {
            var u = _orderedUnits[_i];
            if (isUndefined(obj[u])) {
                obj[u] = defaultUnitValues[u];
            }
        }
        var invalid = hasInvalidGregorianData(obj) || hasInvalidTimeData(obj);
        if (invalid) {
            return DateTime.invalid(invalid);
        }
        var tsNow = Settings.now(), offsetProvis = zone.offset(tsNow), _objToTS = objToTS(obj, offsetProvis, zone), ts = _objToTS[0], o = _objToTS[1];
        return new DateTime({
            ts: ts,
            zone: zone,
            o: o
        });
    }
    function diffRelative(start, end, opts) {
        var round = isUndefined(opts.round) ? true : opts.round, format = function format(c, unit) {
            c = roundTo(c, round || opts.calendary ? 0 : 2, true);
            var formatter = end.loc.clone(opts).relFormatter(opts);
            return formatter.format(c, unit);
        }, differ = function differ(unit) {
            if (opts.calendary) {
                if (!end.hasSame(start, unit)) {
                    return end.startOf(unit).diff(start.startOf(unit), unit).get(unit);
                }
                else
                    return 0;
            }
            else {
                return end.diff(start, unit).get(unit);
            }
        };
        if (opts.unit) {
            return format(differ(opts.unit), opts.unit);
        }
        for (var _iterator = opts.units, _isArray = Array.isArray(_iterator), _i2 = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
            var _ref2;
            if (_isArray) {
                if (_i2 >= _iterator.length)
                    break;
                _ref2 = _iterator[_i2++];
            }
            else {
                _i2 = _iterator.next();
                if (_i2.done)
                    break;
                _ref2 = _i2.value;
            }
            var unit = _ref2;
            var count = differ(unit);
            if (Math.abs(count) >= 1) {
                return format(count, unit);
            }
        }
        return format(0, opts.units[opts.units.length - 1]);
    }
    /**
     * A DateTime is an immutable data structure representing a specific date and time and accompanying methods. It contains class and instance methods for creating, parsing, interrogating, transforming, and formatting them.
     *
     * A DateTime comprises of:
     * * A timestamp. Each DateTime instance refers to a specific millisecond of the Unix epoch.
     * * A time zone. Each instance is considered in the context of a specific zone (by default the local system's zone).
     * * Configuration properties that effect how output strings are formatted, such as `locale`, `numberingSystem`, and `outputCalendar`.
     *
     * Here is a brief overview of the most commonly used functionality it provides:
     *
     * * **Creation**: To create a DateTime from its components, use one of its factory class methods: {@link local}, {@link utc}, and (most flexibly) {@link fromObject}. To create one from a standard string format, use {@link fromISO}, {@link fromHTTP}, and {@link fromRFC2822}. To create one from a custom string format, use {@link fromFormat}. To create one from a native JS date, use {@link fromJSDate}.
     * * **Gregorian calendar and time**: To examine the Gregorian properties of a DateTime individually (i.e as opposed to collectively through {@link toObject}), use the {@link year}, {@link month},
     * {@link day}, {@link hour}, {@link minute}, {@link second}, {@link millisecond} accessors.
     * * **Week calendar**: For ISO week calendar attributes, see the {@link weekYear}, {@link weekNumber}, and {@link weekday} accessors.
     * * **Configuration** See the {@link locale} and {@link numberingSystem} accessors.
     * * **Transformation**: To transform the DateTime into other DateTimes, use {@link set}, {@link reconfigure}, {@link setZone}, {@link setLocale}, {@link plus}, {@link minus}, {@link endOf}, {@link startOf}, {@link toUTC}, and {@link toLocal}.
     * * **Output**: To convert the DateTime to other representations, use the {@link toRelative}, {@link toRelativeCalendar}, {@link toJSON}, {@link toISO}, {@link toHTTP}, {@link toObject}, {@link toRFC2822}, {@link toString}, {@link toLocaleString}, {@link toFormat}, {@link toMillis} and {@link toJSDate}.
     *
     * There's plenty others documented below. In addition, for more information on subtler topics like internationalization, time zones, alternative calendars, validity, and so on, see the external documentation.
     */
    var DateTime = 
    /*#__PURE__*/
    function () {
        /**
         * @access private
         */
        function DateTime(config) {
            var zone = config.zone || Settings.defaultZone;
            var invalid = config.invalid || (Number.isNaN(config.ts) ? new Invalid("invalid input") : null) || (!zone.isValid ? unsupportedZone(zone) : null);
            /**
             * @access private
             */
            this.ts = isUndefined(config.ts) ? Settings.now() : config.ts;
            var c = null, o = null;
            if (!invalid) {
                var unchanged = config.old && config.old.ts === this.ts && config.old.zone.equals(zone);
                if (unchanged) {
                    var _ref3 = [config.old.c, config.old.o];
                    c = _ref3[0];
                    o = _ref3[1];
                }
                else {
                    c = tsToObj(this.ts, zone.offset(this.ts));
                    invalid = Number.isNaN(c.year) ? new Invalid("invalid input") : null;
                    c = invalid ? null : c;
                    o = invalid ? null : zone.offset(this.ts);
                }
            }
            /**
             * @access private
             */
            this._zone = zone;
            /**
             * @access private
             */
            this.loc = config.loc || Locale.create();
            /**
             * @access private
             */
            this.invalid = invalid;
            /**
             * @access private
             */
            this.weekData = null;
            /**
             * @access private
             */
            this.c = c;
            /**
             * @access private
             */
            this.o = o;
            /**
             * @access private
             */
            this.isLuxonDateTime = true;
        } // CONSTRUCT
        /**
         * Create a local DateTime
         * @param {number} [year] - The calendar year. If omitted (as in, call `local()` with no arguments), the current time will be used
         * @param {number} [month=1] - The month, 1-indexed
         * @param {number} [day=1] - The day of the month
         * @param {number} [hour=0] - The hour of the day, in 24-hour time
         * @param {number} [minute=0] - The minute of the hour, meaning a number between 0 and 59
         * @param {number} [second=0] - The second of the minute, meaning a number between 0 and 59
         * @param {number} [millisecond=0] - The millisecond of the second, meaning a number between 0 and 999
         * @example DateTime.local()                            //~> now
         * @example DateTime.local(2017)                        //~> 2017-01-01T00:00:00
         * @example DateTime.local(2017, 3)                     //~> 2017-03-01T00:00:00
         * @example DateTime.local(2017, 3, 12)                 //~> 2017-03-12T00:00:00
         * @example DateTime.local(2017, 3, 12, 5)              //~> 2017-03-12T05:00:00
         * @example DateTime.local(2017, 3, 12, 5, 45)          //~> 2017-03-12T05:45:00
         * @example DateTime.local(2017, 3, 12, 5, 45, 10)      //~> 2017-03-12T05:45:10
         * @example DateTime.local(2017, 3, 12, 5, 45, 10, 765) //~> 2017-03-12T05:45:10.765
         * @return {DateTime}
         */
        DateTime.local = function local(year, month, day, hour, minute, second, millisecond) {
            if (isUndefined(year)) {
                return new DateTime({
                    ts: Settings.now()
                });
            }
            else {
                return quickDT({
                    year: year,
                    month: month,
                    day: day,
                    hour: hour,
                    minute: minute,
                    second: second,
                    millisecond: millisecond
                }, Settings.defaultZone);
            }
        };
        DateTime.utc = function utc(year, month, day, hour, minute, second, millisecond) {
            if (isUndefined(year)) {
                return new DateTime({
                    ts: Settings.now(),
                    zone: FixedOffsetZone.utcInstance
                });
            }
            else {
                return quickDT({
                    year: year,
                    month: month,
                    day: day,
                    hour: hour,
                    minute: minute,
                    second: second,
                    millisecond: millisecond
                }, FixedOffsetZone.utcInstance);
            }
        };
        DateTime.fromJSDate = function fromJSDate(date, options) {
            if (options === void 0) {
                options = {};
            }
            var ts = isDate(date) ? date.valueOf() : NaN;
            if (Number.isNaN(ts)) {
                return DateTime.invalid("invalid input");
            }
            var zoneToUse = normalizeZone(options.zone, Settings.defaultZone);
            if (!zoneToUse.isValid) {
                return DateTime.invalid(unsupportedZone(zoneToUse));
            }
            return new DateTime({
                ts: ts,
                zone: zoneToUse,
                loc: Locale.fromObject(options)
            });
        };
        DateTime.fromMillis = function fromMillis(milliseconds, options) {
            if (options === void 0) {
                options = {};
            }
            if (!isNumber(milliseconds)) {
                throw new InvalidArgumentError("fromMillis requires a numerical input");
            }
            else if (milliseconds < -MAX_DATE || milliseconds > MAX_DATE) {
                // this isn't perfect because because we can still end up out of range because of additional shifting, but it's a start
                return DateTime.invalid("Timestamp out of range");
            }
            else {
                return new DateTime({
                    ts: milliseconds,
                    zone: normalizeZone(options.zone, Settings.defaultZone),
                    loc: Locale.fromObject(options)
                });
            }
        };
        DateTime.fromSeconds = function fromSeconds(seconds, options) {
            if (options === void 0) {
                options = {};
            }
            if (!isNumber(seconds)) {
                throw new InvalidArgumentError("fromSeconds requires a numerical input");
            }
            else {
                return new DateTime({
                    ts: seconds * 1000,
                    zone: normalizeZone(options.zone, Settings.defaultZone),
                    loc: Locale.fromObject(options)
                });
            }
        };
        DateTime.fromObject = function fromObject(obj) {
            var zoneToUse = normalizeZone(obj.zone, Settings.defaultZone);
            if (!zoneToUse.isValid) {
                return DateTime.invalid(unsupportedZone(zoneToUse));
            }
            var tsNow = Settings.now(), offsetProvis = zoneToUse.offset(tsNow), normalized = normalizeObject(obj, normalizeUnit, ["zone", "locale", "outputCalendar", "numberingSystem"]), containsOrdinal = !isUndefined(normalized.ordinal), containsGregorYear = !isUndefined(normalized.year), containsGregorMD = !isUndefined(normalized.month) || !isUndefined(normalized.day), containsGregor = containsGregorYear || containsGregorMD, definiteWeekDef = normalized.weekYear || normalized.weekNumber, loc = Locale.fromObject(obj); // cases:
            // just a weekday -> this week's instance of that weekday, no worries
            // (gregorian data or ordinal) + (weekYear or weekNumber) -> error
            // (gregorian month or day) + ordinal -> error
            // otherwise just use weeks or ordinals or gregorian, depending on what's specified
            if ((containsGregor || containsOrdinal) && definiteWeekDef) {
                throw new ConflictingSpecificationError("Can't mix weekYear/weekNumber units with year/month/day or ordinals");
            }
            if (containsGregorMD && containsOrdinal) {
                throw new ConflictingSpecificationError("Can't mix ordinal dates with month/day");
            }
            var useWeekData = definiteWeekDef || normalized.weekday && !containsGregor; // configure ourselves to deal with gregorian dates or week stuff
            var units, defaultValues, objNow = tsToObj(tsNow, offsetProvis);
            if (useWeekData) {
                units = orderedWeekUnits;
                defaultValues = defaultWeekUnitValues;
                objNow = gregorianToWeek(objNow);
            }
            else if (containsOrdinal) {
                units = orderedOrdinalUnits;
                defaultValues = defaultOrdinalUnitValues;
                objNow = gregorianToOrdinal(objNow);
            }
            else {
                units = orderedUnits$1;
                defaultValues = defaultUnitValues;
            } // set default values for missing stuff
            var foundFirst = false;
            for (var _iterator2 = units, _isArray2 = Array.isArray(_iterator2), _i3 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
                var _ref4;
                if (_isArray2) {
                    if (_i3 >= _iterator2.length)
                        break;
                    _ref4 = _iterator2[_i3++];
                }
                else {
                    _i3 = _iterator2.next();
                    if (_i3.done)
                        break;
                    _ref4 = _i3.value;
                }
                var u = _ref4;
                var v = normalized[u];
                if (!isUndefined(v)) {
                    foundFirst = true;
                }
                else if (foundFirst) {
                    normalized[u] = defaultValues[u];
                }
                else {
                    normalized[u] = objNow[u];
                }
            } // make sure the values we have are in range
            var higherOrderInvalid = useWeekData ? hasInvalidWeekData(normalized) : containsOrdinal ? hasInvalidOrdinalData(normalized) : hasInvalidGregorianData(normalized), invalid = higherOrderInvalid || hasInvalidTimeData(normalized);
            if (invalid) {
                return DateTime.invalid(invalid);
            } // compute the actual time
            var gregorian = useWeekData ? weekToGregorian(normalized) : containsOrdinal ? ordinalToGregorian(normalized) : normalized, _objToTS2 = objToTS(gregorian, offsetProvis, zoneToUse), tsFinal = _objToTS2[0], offsetFinal = _objToTS2[1], inst = new DateTime({
                ts: tsFinal,
                zone: zoneToUse,
                o: offsetFinal,
                loc: loc
            }); // gregorian data + weekday serves only to validate
            if (normalized.weekday && containsGregor && obj.weekday !== inst.weekday) {
                return DateTime.invalid("mismatched weekday", "you can't specify both a weekday of " + normalized.weekday + " and a date of " + inst.toISO());
            }
            return inst;
        };
        DateTime.fromISO = function fromISO(text, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var _parseISODate = parseISODate(text), vals = _parseISODate[0], parsedZone = _parseISODate[1];
            return parseDataToDateTime(vals, parsedZone, opts, "ISO 8601", text);
        };
        DateTime.fromRFC2822 = function fromRFC2822(text, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var _parseRFC2822Date = parseRFC2822Date(text), vals = _parseRFC2822Date[0], parsedZone = _parseRFC2822Date[1];
            return parseDataToDateTime(vals, parsedZone, opts, "RFC 2822", text);
        };
        DateTime.fromHTTP = function fromHTTP(text, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var _parseHTTPDate = parseHTTPDate(text), vals = _parseHTTPDate[0], parsedZone = _parseHTTPDate[1];
            return parseDataToDateTime(vals, parsedZone, opts, "HTTP", opts);
        };
        DateTime.fromFormat = function fromFormat(text, fmt, opts) {
            if (opts === void 0) {
                opts = {};
            }
            if (isUndefined(text) || isUndefined(fmt)) {
                throw new InvalidArgumentError("fromFormat requires an input string and a format");
            }
            var _opts = opts, _opts$locale = _opts.locale, locale = _opts$locale === void 0 ? null : _opts$locale, _opts$numberingSystem = _opts.numberingSystem, numberingSystem = _opts$numberingSystem === void 0 ? null : _opts$numberingSystem, localeToUse = Locale.fromOpts({
                locale: locale,
                numberingSystem: numberingSystem,
                defaultToEN: true
            }), _parseFromTokens = parseFromTokens(localeToUse, text, fmt), vals = _parseFromTokens[0], parsedZone = _parseFromTokens[1], invalid = _parseFromTokens[2];
            if (invalid) {
                return DateTime.invalid(invalid);
            }
            else {
                return parseDataToDateTime(vals, parsedZone, opts, "format " + fmt, text);
            }
        };
        DateTime.fromString = function fromString(text, fmt, opts) {
            if (opts === void 0) {
                opts = {};
            }
            return DateTime.fromFormat(text, fmt, opts);
        };
        DateTime.fromSQL = function fromSQL(text, opts) {
            if (opts === void 0) {
                opts = {};
            }
            var _parseSQL = parseSQL(text), vals = _parseSQL[0], parsedZone = _parseSQL[1];
            return parseDataToDateTime(vals, parsedZone, opts, "SQL", text);
        };
        DateTime.invalid = function invalid(reason, explanation) {
            if (explanation === void 0) {
                explanation = null;
            }
            if (!reason) {
                throw new InvalidArgumentError("need to specify a reason the DateTime is invalid");
            }
            var invalid = reason instanceof Invalid ? reason : new Invalid(reason, explanation);
            if (Settings.throwOnInvalid) {
                throw new InvalidDateTimeError(invalid);
            }
            else {
                return new DateTime({
                    invalid: invalid
                });
            }
        };
        DateTime.isDateTime = function isDateTime(o) {
            return o && o.isLuxonDateTime || false;
        } // INFO
        ;
        var _proto = DateTime.prototype;
        _proto.get = function get(unit) {
            return this[unit];
        };
        /**
         * Returns the resolved Intl options for this DateTime.
         * This is useful in understanding the behavior of formatting methods
         * @param {Object} opts - the same options as toLocaleString
         * @return {Object}
         */
        _proto.resolvedLocaleOpts = function resolvedLocaleOpts(opts) {
            if (opts === void 0) {
                opts = {};
            }
            var _Formatter$create$res = Formatter.create(this.loc.clone(opts), opts).resolvedOptions(this), locale = _Formatter$create$res.locale, numberingSystem = _Formatter$create$res.numberingSystem, calendar = _Formatter$create$res.calendar;
            return {
                locale: locale,
                numberingSystem: numberingSystem,
                outputCalendar: calendar
            };
        } // TRANSFORM
        ;
        _proto.toUTC = function toUTC(offset, opts) {
            if (offset === void 0) {
                offset = 0;
            }
            if (opts === void 0) {
                opts = {};
            }
            return this.setZone(FixedOffsetZone.instance(offset), opts);
        };
        _proto.toLocal = function toLocal() {
            return this.setZone(Settings.defaultZone);
        };
        _proto.setZone = function setZone(zone, _temp) {
            var _ref5 = _temp === void 0 ? {} : _temp, _ref5$keepLocalTime = _ref5.keepLocalTime, keepLocalTime = _ref5$keepLocalTime === void 0 ? false : _ref5$keepLocalTime, _ref5$keepCalendarTim = _ref5.keepCalendarTime, keepCalendarTime = _ref5$keepCalendarTim === void 0 ? false : _ref5$keepCalendarTim;
            zone = normalizeZone(zone, Settings.defaultZone);
            if (zone.equals(this.zone)) {
                return this;
            }
            else if (!zone.isValid) {
                return DateTime.invalid(unsupportedZone(zone));
            }
            else {
                var newTS = this.ts;
                if (keepLocalTime || keepCalendarTime) {
                    var offsetGuess = this.o - zone.offset(this.ts);
                    var asObj = this.toObject();
                    var _objToTS3 = objToTS(asObj, offsetGuess, zone);
                    newTS = _objToTS3[0];
                }
                return clone$1(this, {
                    ts: newTS,
                    zone: zone
                });
            }
        };
        _proto.reconfigure = function reconfigure(_temp2) {
            var _ref6 = _temp2 === void 0 ? {} : _temp2, locale = _ref6.locale, numberingSystem = _ref6.numberingSystem, outputCalendar = _ref6.outputCalendar;
            var loc = this.loc.clone({
                locale: locale,
                numberingSystem: numberingSystem,
                outputCalendar: outputCalendar
            });
            return clone$1(this, {
                loc: loc
            });
        };
        _proto.setLocale = function setLocale(locale) {
            return this.reconfigure({
                locale: locale
            });
        };
        _proto.set = function set(values) {
            if (!this.isValid)
                return this;
            var normalized = normalizeObject(values, normalizeUnit, []), settingWeekStuff = !isUndefined(normalized.weekYear) || !isUndefined(normalized.weekNumber) || !isUndefined(normalized.weekday);
            var mixed;
            if (settingWeekStuff) {
                mixed = weekToGregorian(Object.assign(gregorianToWeek(this.c), normalized));
            }
            else if (!isUndefined(normalized.ordinal)) {
                mixed = ordinalToGregorian(Object.assign(gregorianToOrdinal(this.c), normalized));
            }
            else {
                mixed = Object.assign(this.toObject(), normalized); // if we didn't set the day but we ended up on an overflow date,
                // use the last day of the right month
                if (isUndefined(normalized.day)) {
                    mixed.day = Math.min(daysInMonth(mixed.year, mixed.month), mixed.day);
                }
            }
            var _objToTS4 = objToTS(mixed, this.o, this.zone), ts = _objToTS4[0], o = _objToTS4[1];
            return clone$1(this, {
                ts: ts,
                o: o
            });
        };
        _proto.plus = function plus(duration) {
            if (!this.isValid)
                return this;
            var dur = friendlyDuration(duration);
            return clone$1(this, adjustTime(this, dur));
        };
        _proto.minus = function minus(duration) {
            if (!this.isValid)
                return this;
            var dur = friendlyDuration(duration).negate();
            return clone$1(this, adjustTime(this, dur));
        };
        _proto.startOf = function startOf(unit) {
            if (!this.isValid)
                return this;
            var o = {}, normalizedUnit = Duration.normalizeUnit(unit);
            switch (normalizedUnit) {
                case "years":
                    o.month = 1;
                // falls through
                case "quarters":
                case "months":
                    o.day = 1;
                // falls through
                case "weeks":
                case "days":
                    o.hour = 0;
                // falls through
                case "hours":
                    o.minute = 0;
                // falls through
                case "minutes":
                    o.second = 0;
                // falls through
                case "seconds":
                    o.millisecond = 0;
                    break;
                case "milliseconds":
                    break;
                // no default, invalid units throw in normalizeUnit()
            }
            if (normalizedUnit === "weeks") {
                o.weekday = 1;
            }
            if (normalizedUnit === "quarters") {
                var q = Math.ceil(this.month / 3);
                o.month = (q - 1) * 3 + 1;
            }
            return this.set(o);
        };
        _proto.endOf = function endOf(unit) {
            var _this$plus;
            return this.isValid ? this.plus((_this$plus = {}, _this$plus[unit] = 1, _this$plus)).startOf(unit).minus(1) : this;
        } // OUTPUT
        ;
        _proto.toFormat = function toFormat(fmt, opts) {
            if (opts === void 0) {
                opts = {};
            }
            return this.isValid ? Formatter.create(this.loc.redefaultToEN(opts)).formatDateTimeFromString(this, fmt) : INVALID$2;
        };
        _proto.toLocaleString = function toLocaleString(opts) {
            if (opts === void 0) {
                opts = DATE_SHORT;
            }
            return this.isValid ? Formatter.create(this.loc.clone(opts), opts).formatDateTime(this) : INVALID$2;
        };
        _proto.toLocaleParts = function toLocaleParts(opts) {
            if (opts === void 0) {
                opts = {};
            }
            return this.isValid ? Formatter.create(this.loc.clone(opts), opts).formatDateTimeParts(this) : [];
        };
        _proto.toISO = function toISO(opts) {
            if (opts === void 0) {
                opts = {};
            }
            if (!this.isValid) {
                return null;
            }
            return this.toISODate() + "T" + this.toISOTime(opts);
        };
        _proto.toISODate = function toISODate() {
            var format = "yyyy-MM-dd";
            if (this.year > 9999) {
                format = "+" + format;
            }
            return toTechFormat(this, format);
        };
        _proto.toISOWeekDate = function toISOWeekDate() {
            return toTechFormat(this, "kkkk-'W'WW-c");
        };
        _proto.toISOTime = function toISOTime(_temp3) {
            var _ref7 = _temp3 === void 0 ? {} : _temp3, _ref7$suppressMillise = _ref7.suppressMilliseconds, suppressMilliseconds = _ref7$suppressMillise === void 0 ? false : _ref7$suppressMillise, _ref7$suppressSeconds = _ref7.suppressSeconds, suppressSeconds = _ref7$suppressSeconds === void 0 ? false : _ref7$suppressSeconds, _ref7$includeOffset = _ref7.includeOffset, includeOffset = _ref7$includeOffset === void 0 ? true : _ref7$includeOffset;
            return toTechTimeFormat(this, {
                suppressSeconds: suppressSeconds,
                suppressMilliseconds: suppressMilliseconds,
                includeOffset: includeOffset
            });
        };
        _proto.toRFC2822 = function toRFC2822() {
            return toTechFormat(this, "EEE, dd LLL yyyy HH:mm:ss ZZZ");
        };
        _proto.toHTTP = function toHTTP() {
            return toTechFormat(this.toUTC(), "EEE, dd LLL yyyy HH:mm:ss 'GMT'");
        };
        _proto.toSQLDate = function toSQLDate() {
            return toTechFormat(this, "yyyy-MM-dd");
        };
        _proto.toSQLTime = function toSQLTime(_temp4) {
            var _ref8 = _temp4 === void 0 ? {} : _temp4, _ref8$includeOffset = _ref8.includeOffset, includeOffset = _ref8$includeOffset === void 0 ? true : _ref8$includeOffset, _ref8$includeZone = _ref8.includeZone, includeZone = _ref8$includeZone === void 0 ? false : _ref8$includeZone;
            return toTechTimeFormat(this, {
                includeOffset: includeOffset,
                includeZone: includeZone,
                spaceZone: true
            });
        };
        _proto.toSQL = function toSQL(opts) {
            if (opts === void 0) {
                opts = {};
            }
            if (!this.isValid) {
                return null;
            }
            return this.toSQLDate() + " " + this.toSQLTime(opts);
        };
        _proto.toString = function toString() {
            return this.isValid ? this.toISO() : INVALID$2;
        };
        _proto.valueOf = function valueOf() {
            return this.toMillis();
        };
        _proto.toMillis = function toMillis() {
            return this.isValid ? this.ts : NaN;
        };
        _proto.toSeconds = function toSeconds() {
            return this.isValid ? this.ts / 1000 : NaN;
        };
        _proto.toJSON = function toJSON() {
            return this.toISO();
        };
        _proto.toBSON = function toBSON() {
            return this.toJSDate();
        };
        _proto.toObject = function toObject(opts) {
            if (opts === void 0) {
                opts = {};
            }
            if (!this.isValid)
                return {};
            var base = Object.assign({}, this.c);
            if (opts.includeConfig) {
                base.outputCalendar = this.outputCalendar;
                base.numberingSystem = this.loc.numberingSystem;
                base.locale = this.loc.locale;
            }
            return base;
        };
        _proto.toJSDate = function toJSDate() {
            return new Date(this.isValid ? this.ts : NaN);
        } // COMPARE
        ;
        _proto.diff = function diff(otherDateTime, unit, opts) {
            if (unit === void 0) {
                unit = "milliseconds";
            }
            if (opts === void 0) {
                opts = {};
            }
            if (!this.isValid || !otherDateTime.isValid) {
                return Duration.invalid(this.invalid || otherDateTime.invalid, "created by diffing an invalid DateTime");
            }
            var durOpts = Object.assign({
                locale: this.locale,
                numberingSystem: this.numberingSystem
            }, opts);
            var units = maybeArray(unit).map(Duration.normalizeUnit), otherIsLater = otherDateTime.valueOf() > this.valueOf(), earlier = otherIsLater ? this : otherDateTime, later = otherIsLater ? otherDateTime : this, diffed = _diff(earlier, later, units, durOpts);
            return otherIsLater ? diffed.negate() : diffed;
        };
        _proto.diffNow = function diffNow(unit, opts) {
            if (unit === void 0) {
                unit = "milliseconds";
            }
            if (opts === void 0) {
                opts = {};
            }
            return this.diff(DateTime.local(), unit, opts);
        };
        _proto.until = function until(otherDateTime) {
            return this.isValid ? Interval.fromDateTimes(this, otherDateTime) : this;
        };
        _proto.hasSame = function hasSame(otherDateTime, unit) {
            if (!this.isValid)
                return false;
            if (unit === "millisecond") {
                return this.valueOf() === otherDateTime.valueOf();
            }
            else {
                var inputMs = otherDateTime.valueOf();
                return this.startOf(unit) <= inputMs && inputMs <= this.endOf(unit);
            }
        };
        _proto.equals = function equals(other) {
            return this.isValid && other.isValid && this.valueOf() === other.valueOf() && this.zone.equals(other.zone) && this.loc.equals(other.loc);
        };
        _proto.toRelative = function toRelative(options) {
            if (options === void 0) {
                options = {};
            }
            if (!this.isValid)
                return null;
            var base = options.base || DateTime.fromObject({
                zone: this.zone
            }), padding = options.padding ? this < base ? -options.padding : options.padding : 0;
            return diffRelative(base, this.plus(padding), Object.assign(options, {
                numeric: "always",
                units: ["years", "months", "days", "hours", "minutes", "seconds"]
            }));
        };
        _proto.toRelativeCalendar = function toRelativeCalendar(options) {
            if (options === void 0) {
                options = {};
            }
            if (!this.isValid)
                return null;
            return diffRelative(options.base || DateTime.fromObject({
                zone: this.zone
            }), this, Object.assign(options, {
                numeric: "auto",
                units: ["years", "months", "days"],
                calendary: true
            }));
        };
        DateTime.min = function min() {
            for (var _len = arguments.length, dateTimes = new Array(_len), _key = 0; _key < _len; _key++) {
                dateTimes[_key] = arguments[_key];
            }
            if (!dateTimes.every(DateTime.isDateTime)) {
                throw new InvalidArgumentError("min requires all arguments be DateTimes");
            }
            return bestBy(dateTimes, function (i) {
                return i.valueOf();
            }, Math.min);
        };
        DateTime.max = function max() {
            for (var _len2 = arguments.length, dateTimes = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                dateTimes[_key2] = arguments[_key2];
            }
            if (!dateTimes.every(DateTime.isDateTime)) {
                throw new InvalidArgumentError("max requires all arguments be DateTimes");
            }
            return bestBy(dateTimes, function (i) {
                return i.valueOf();
            }, Math.max);
        } // MISC
        ;
        DateTime.fromFormatExplain = function fromFormatExplain(text, fmt, options) {
            if (options === void 0) {
                options = {};
            }
            var _options = options, _options$locale = _options.locale, locale = _options$locale === void 0 ? null : _options$locale, _options$numberingSys = _options.numberingSystem, numberingSystem = _options$numberingSys === void 0 ? null : _options$numberingSys, localeToUse = Locale.fromOpts({
                locale: locale,
                numberingSystem: numberingSystem,
                defaultToEN: true
            });
            return explainFromTokens(localeToUse, text, fmt);
        };
        DateTime.fromStringExplain = function fromStringExplain(text, fmt, options) {
            if (options === void 0) {
                options = {};
            }
            return DateTime.fromFormatExplain(text, fmt, options);
        } // FORMAT PRESETS
        ;
        _createClass(DateTime, [{
                key: "isValid",
                get: function get() {
                    return this.invalid === null;
                }
                /**
                 * Returns an error code if this DateTime is invalid, or null if the DateTime is valid
                 * @type {string}
                 */
            }, {
                key: "invalidReason",
                get: function get() {
                    return this.invalid ? this.invalid.reason : null;
                }
                /**
                 * Returns an explanation of why this DateTime became invalid, or null if the DateTime is valid
                 * @type {string}
                 */
            }, {
                key: "invalidExplanation",
                get: function get() {
                    return this.invalid ? this.invalid.explanation : null;
                }
                /**
                 * Get the locale of a DateTime, such 'en-GB'. The locale is used when formatting the DateTime
                 *
                 * @type {string}
                 */
            }, {
                key: "locale",
                get: function get() {
                    return this.isValid ? this.loc.locale : null;
                }
                /**
                 * Get the numbering system of a DateTime, such 'beng'. The numbering system is used when formatting the DateTime
                 *
                 * @type {string}
                 */
            }, {
                key: "numberingSystem",
                get: function get() {
                    return this.isValid ? this.loc.numberingSystem : null;
                }
                /**
                 * Get the output calendar of a DateTime, such 'islamic'. The output calendar is used when formatting the DateTime
                 *
                 * @type {string}
                 */
            }, {
                key: "outputCalendar",
                get: function get() {
                    return this.isValid ? this.loc.outputCalendar : null;
                }
                /**
                 * Get the time zone associated with this DateTime.
                 * @type {Zone}
                 */
            }, {
                key: "zone",
                get: function get() {
                    return this._zone;
                }
                /**
                 * Get the name of the time zone.
                 * @type {string}
                 */
            }, {
                key: "zoneName",
                get: function get() {
                    return this.isValid ? this.zone.name : null;
                }
                /**
                 * Get the year
                 * @example DateTime.local(2017, 5, 25).year //=> 2017
                 * @type {number}
                 */
            }, {
                key: "year",
                get: function get() {
                    return this.isValid ? this.c.year : NaN;
                }
                /**
                 * Get the quarter
                 * @example DateTime.local(2017, 5, 25).quarter //=> 2
                 * @type {number}
                 */
            }, {
                key: "quarter",
                get: function get() {
                    return this.isValid ? Math.ceil(this.c.month / 3) : NaN;
                }
                /**
                 * Get the month (1-12).
                 * @example DateTime.local(2017, 5, 25).month //=> 5
                 * @type {number}
                 */
            }, {
                key: "month",
                get: function get() {
                    return this.isValid ? this.c.month : NaN;
                }
                /**
                 * Get the day of the month (1-30ish).
                 * @example DateTime.local(2017, 5, 25).day //=> 25
                 * @type {number}
                 */
            }, {
                key: "day",
                get: function get() {
                    return this.isValid ? this.c.day : NaN;
                }
                /**
                 * Get the hour of the day (0-23).
                 * @example DateTime.local(2017, 5, 25, 9).hour //=> 9
                 * @type {number}
                 */
            }, {
                key: "hour",
                get: function get() {
                    return this.isValid ? this.c.hour : NaN;
                }
                /**
                 * Get the minute of the hour (0-59).
                 * @example DateTime.local(2017, 5, 25, 9, 30).minute //=> 30
                 * @type {number}
                 */
            }, {
                key: "minute",
                get: function get() {
                    return this.isValid ? this.c.minute : NaN;
                }
                /**
                 * Get the second of the minute (0-59).
                 * @example DateTime.local(2017, 5, 25, 9, 30, 52).second //=> 52
                 * @type {number}
                 */
            }, {
                key: "second",
                get: function get() {
                    return this.isValid ? this.c.second : NaN;
                }
                /**
                 * Get the millisecond of the second (0-999).
                 * @example DateTime.local(2017, 5, 25, 9, 30, 52, 654).millisecond //=> 654
                 * @type {number}
                 */
            }, {
                key: "millisecond",
                get: function get() {
                    return this.isValid ? this.c.millisecond : NaN;
                }
                /**
                 * Get the week year
                 * @see https://en.wikipedia.org/wiki/ISO_week_date
                 * @example DateTime.local(2014, 11, 31).weekYear //=> 2015
                 * @type {number}
                 */
            }, {
                key: "weekYear",
                get: function get() {
                    return this.isValid ? possiblyCachedWeekData(this).weekYear : NaN;
                }
                /**
                 * Get the week number of the week year (1-52ish).
                 * @see https://en.wikipedia.org/wiki/ISO_week_date
                 * @example DateTime.local(2017, 5, 25).weekNumber //=> 21
                 * @type {number}
                 */
            }, {
                key: "weekNumber",
                get: function get() {
                    return this.isValid ? possiblyCachedWeekData(this).weekNumber : NaN;
                }
                /**
                 * Get the day of the week.
                 * 1 is Monday and 7 is Sunday
                 * @see https://en.wikipedia.org/wiki/ISO_week_date
                 * @example DateTime.local(2014, 11, 31).weekday //=> 4
                 * @type {number}
                 */
            }, {
                key: "weekday",
                get: function get() {
                    return this.isValid ? possiblyCachedWeekData(this).weekday : NaN;
                }
                /**
                 * Get the ordinal (meaning the day of the year)
                 * @example DateTime.local(2017, 5, 25).ordinal //=> 145
                 * @type {number|DateTime}
                 */
            }, {
                key: "ordinal",
                get: function get() {
                    return this.isValid ? gregorianToOrdinal(this.c).ordinal : NaN;
                }
                /**
                 * Get the human readable short month name, such as 'Oct'.
                 * Defaults to the system's locale if no locale has been specified
                 * @example DateTime.local(2017, 10, 30).monthShort //=> Oct
                 * @type {string}
                 */
            }, {
                key: "monthShort",
                get: function get() {
                    return this.isValid ? Info.months("short", {
                        locale: this.locale
                    })[this.month - 1] : null;
                }
                /**
                 * Get the human readable long month name, such as 'October'.
                 * Defaults to the system's locale if no locale has been specified
                 * @example DateTime.local(2017, 10, 30).monthLong //=> October
                 * @type {string}
                 */
            }, {
                key: "monthLong",
                get: function get() {
                    return this.isValid ? Info.months("long", {
                        locale: this.locale
                    })[this.month - 1] : null;
                }
                /**
                 * Get the human readable short weekday, such as 'Mon'.
                 * Defaults to the system's locale if no locale has been specified
                 * @example DateTime.local(2017, 10, 30).weekdayShort //=> Mon
                 * @type {string}
                 */
            }, {
                key: "weekdayShort",
                get: function get() {
                    return this.isValid ? Info.weekdays("short", {
                        locale: this.locale
                    })[this.weekday - 1] : null;
                }
                /**
                 * Get the human readable long weekday, such as 'Monday'.
                 * Defaults to the system's locale if no locale has been specified
                 * @example DateTime.local(2017, 10, 30).weekdayLong //=> Monday
                 * @type {string}
                 */
            }, {
                key: "weekdayLong",
                get: function get() {
                    return this.isValid ? Info.weekdays("long", {
                        locale: this.locale
                    })[this.weekday - 1] : null;
                }
                /**
                 * Get the UTC offset of this DateTime in minutes
                 * @example DateTime.local().offset //=> -240
                 * @example DateTime.utc().offset //=> 0
                 * @type {number}
                 */
            }, {
                key: "offset",
                get: function get() {
                    return this.isValid ? +this.o : NaN;
                }
                /**
                 * Get the short human name for the zone's current offset, for example "EST" or "EDT".
                 * Defaults to the system's locale if no locale has been specified
                 * @type {string}
                 */
            }, {
                key: "offsetNameShort",
                get: function get() {
                    if (this.isValid) {
                        return this.zone.offsetName(this.ts, {
                            format: "short",
                            locale: this.locale
                        });
                    }
                    else {
                        return null;
                    }
                }
                /**
                 * Get the long human name for the zone's current offset, for example "Eastern Standard Time" or "Eastern Daylight Time".
                 * Defaults to the system's locale if no locale has been specified
                 * @type {string}
                 */
            }, {
                key: "offsetNameLong",
                get: function get() {
                    if (this.isValid) {
                        return this.zone.offsetName(this.ts, {
                            format: "long",
                            locale: this.locale
                        });
                    }
                    else {
                        return null;
                    }
                }
                /**
                 * Get whether this zone's offset ever changes, as in a DST.
                 * @type {boolean}
                 */
            }, {
                key: "isOffsetFixed",
                get: function get() {
                    return this.isValid ? this.zone.universal : null;
                }
                /**
                 * Get whether the DateTime is in a DST.
                 * @type {boolean}
                 */
            }, {
                key: "isInDST",
                get: function get() {
                    if (this.isOffsetFixed) {
                        return false;
                    }
                    else {
                        return this.offset > this.set({
                            month: 1
                        }).offset || this.offset > this.set({
                            month: 5
                        }).offset;
                    }
                }
                /**
                 * Returns true if this DateTime is in a leap year, false otherwise
                 * @example DateTime.local(2016).isInLeapYear //=> true
                 * @example DateTime.local(2013).isInLeapYear //=> false
                 * @type {boolean}
                 */
            }, {
                key: "isInLeapYear",
                get: function get() {
                    return isLeapYear(this.year);
                }
                /**
                 * Returns the number of days in this DateTime's month
                 * @example DateTime.local(2016, 2).daysInMonth //=> 29
                 * @example DateTime.local(2016, 3).daysInMonth //=> 31
                 * @type {number}
                 */
            }, {
                key: "daysInMonth",
                get: function get() {
                    return daysInMonth(this.year, this.month);
                }
                /**
                 * Returns the number of days in this DateTime's year
                 * @example DateTime.local(2016).daysInYear //=> 366
                 * @example DateTime.local(2013).daysInYear //=> 365
                 * @type {number}
                 */
            }, {
                key: "daysInYear",
                get: function get() {
                    return this.isValid ? daysInYear(this.year) : NaN;
                }
                /**
                 * Returns the number of weeks in this DateTime's year
                 * @see https://en.wikipedia.org/wiki/ISO_week_date
                 * @example DateTime.local(2004).weeksInWeekYear //=> 53
                 * @example DateTime.local(2013).weeksInWeekYear //=> 52
                 * @type {number}
                 */
            }, {
                key: "weeksInWeekYear",
                get: function get() {
                    return this.isValid ? weeksInWeekYear(this.weekYear) : NaN;
                }
            }], [{
                key: "DATE_SHORT",
                get: function get() {
                    return DATE_SHORT;
                }
                /**
                 * {@link toLocaleString} format like 'Oct 14, 1983'
                 * @type {Object}
                 */
            }, {
                key: "DATE_MED",
                get: function get() {
                    return DATE_MED;
                }
                /**
                 * {@link toLocaleString} format like 'October 14, 1983'
                 * @type {Object}
                 */
            }, {
                key: "DATE_FULL",
                get: function get() {
                    return DATE_FULL;
                }
                /**
                 * {@link toLocaleString} format like 'Tuesday, October 14, 1983'
                 * @type {Object}
                 */
            }, {
                key: "DATE_HUGE",
                get: function get() {
                    return DATE_HUGE;
                }
                /**
                 * {@link toLocaleString} format like '09:30 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "TIME_SIMPLE",
                get: function get() {
                    return TIME_SIMPLE;
                }
                /**
                 * {@link toLocaleString} format like '09:30:23 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "TIME_WITH_SECONDS",
                get: function get() {
                    return TIME_WITH_SECONDS;
                }
                /**
                 * {@link toLocaleString} format like '09:30:23 AM EDT'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "TIME_WITH_SHORT_OFFSET",
                get: function get() {
                    return TIME_WITH_SHORT_OFFSET;
                }
                /**
                 * {@link toLocaleString} format like '09:30:23 AM Eastern Daylight Time'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "TIME_WITH_LONG_OFFSET",
                get: function get() {
                    return TIME_WITH_LONG_OFFSET;
                }
                /**
                 * {@link toLocaleString} format like '09:30', always 24-hour.
                 * @type {Object}
                 */
            }, {
                key: "TIME_24_SIMPLE",
                get: function get() {
                    return TIME_24_SIMPLE;
                }
                /**
                 * {@link toLocaleString} format like '09:30:23', always 24-hour.
                 * @type {Object}
                 */
            }, {
                key: "TIME_24_WITH_SECONDS",
                get: function get() {
                    return TIME_24_WITH_SECONDS;
                }
                /**
                 * {@link toLocaleString} format like '09:30:23 EDT', always 24-hour.
                 * @type {Object}
                 */
            }, {
                key: "TIME_24_WITH_SHORT_OFFSET",
                get: function get() {
                    return TIME_24_WITH_SHORT_OFFSET;
                }
                /**
                 * {@link toLocaleString} format like '09:30:23 Eastern Daylight Time', always 24-hour.
                 * @type {Object}
                 */
            }, {
                key: "TIME_24_WITH_LONG_OFFSET",
                get: function get() {
                    return TIME_24_WITH_LONG_OFFSET;
                }
                /**
                 * {@link toLocaleString} format like '10/14/1983, 9:30 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_SHORT",
                get: function get() {
                    return DATETIME_SHORT;
                }
                /**
                 * {@link toLocaleString} format like '10/14/1983, 9:30:33 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_SHORT_WITH_SECONDS",
                get: function get() {
                    return DATETIME_SHORT_WITH_SECONDS;
                }
                /**
                 * {@link toLocaleString} format like 'Oct 14, 1983, 9:30 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_MED",
                get: function get() {
                    return DATETIME_MED;
                }
                /**
                 * {@link toLocaleString} format like 'Oct 14, 1983, 9:30:33 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_MED_WITH_SECONDS",
                get: function get() {
                    return DATETIME_MED_WITH_SECONDS;
                }
                /**
                 * {@link toLocaleString} format like 'Fri, 14 Oct 1983, 9:30 AM'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_MED_WITH_WEEKDAY",
                get: function get() {
                    return DATETIME_MED_WITH_WEEKDAY;
                }
                /**
                 * {@link toLocaleString} format like 'October 14, 1983, 9:30 AM EDT'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_FULL",
                get: function get() {
                    return DATETIME_FULL;
                }
                /**
                 * {@link toLocaleString} format like 'October 14, 1983, 9:30:33 AM EDT'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_FULL_WITH_SECONDS",
                get: function get() {
                    return DATETIME_FULL_WITH_SECONDS;
                }
                /**
                 * {@link toLocaleString} format like 'Friday, October 14, 1983, 9:30 AM Eastern Daylight Time'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_HUGE",
                get: function get() {
                    return DATETIME_HUGE;
                }
                /**
                 * {@link toLocaleString} format like 'Friday, October 14, 1983, 9:30:33 AM Eastern Daylight Time'. Only 12-hour if the locale is.
                 * @type {Object}
                 */
            }, {
                key: "DATETIME_HUGE_WITH_SECONDS",
                get: function get() {
                    return DATETIME_HUGE_WITH_SECONDS;
                }
            }]);
        return DateTime;
    }();
    function friendlyDateTime(dateTimeish) {
        if (DateTime.isDateTime(dateTimeish)) {
            return dateTimeish;
        }
        else if (dateTimeish && dateTimeish.valueOf && isNumber(dateTimeish.valueOf())) {
            return DateTime.fromJSDate(dateTimeish);
        }
        else if (dateTimeish && _typeof(dateTimeish) === "object") {
            return DateTime.fromObject(dateTimeish);
        }
        else {
            throw new InvalidArgumentError("Unknown datetime argument: " + dateTimeish + ", of type " + _typeof(dateTimeish));
        }
    }
    exports.DateTime = DateTime;
    exports.Duration = Duration;
    exports.FixedOffsetZone = FixedOffsetZone;
    exports.IANAZone = IANAZone;
    exports.Info = Info;
    exports.Interval = Interval;
    exports.InvalidZone = InvalidZone;
    exports.LocalZone = LocalZone;
    exports.Settings = Settings;
    exports.Zone = Zone;
    return exports;
}({});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0ZGluIl0sIm5hbWVzIjpbImx1eG9uIiwiZXhwb3J0cyIsIl9kZWZpbmVQcm9wZXJ0aWVzIiwidGFyZ2V0IiwicHJvcHMiLCJpIiwibGVuZ3RoIiwiZGVzY3JpcHRvciIsImVudW1lcmFibGUiLCJjb25maWd1cmFibGUiLCJ3cml0YWJsZSIsIk9iamVjdCIsImRlZmluZVByb3BlcnR5Iiwia2V5IiwiX2NyZWF0ZUNsYXNzIiwiQ29uc3RydWN0b3IiLCJwcm90b1Byb3BzIiwic3RhdGljUHJvcHMiLCJwcm90b3R5cGUiLCJfaW5oZXJpdHNMb29zZSIsInN1YkNsYXNzIiwic3VwZXJDbGFzcyIsImNyZWF0ZSIsImNvbnN0cnVjdG9yIiwiX19wcm90b19fIiwiX2dldFByb3RvdHlwZU9mIiwibyIsInNldFByb3RvdHlwZU9mIiwiZ2V0UHJvdG90eXBlT2YiLCJfc2V0UHJvdG90eXBlT2YiLCJwIiwiaXNOYXRpdmVSZWZsZWN0Q29uc3RydWN0IiwiUmVmbGVjdCIsImNvbnN0cnVjdCIsInNoYW0iLCJQcm94eSIsIkRhdGUiLCJ0b1N0cmluZyIsImNhbGwiLCJlIiwiX2NvbnN0cnVjdCIsIlBhcmVudCIsImFyZ3MiLCJDbGFzcyIsImEiLCJwdXNoIiwiYXBwbHkiLCJGdW5jdGlvbiIsImJpbmQiLCJpbnN0YW5jZSIsImFyZ3VtZW50cyIsIl9pc05hdGl2ZUZ1bmN0aW9uIiwiZm4iLCJpbmRleE9mIiwiX3dyYXBOYXRpdmVTdXBlciIsIl9jYWNoZSIsIk1hcCIsInVuZGVmaW5lZCIsIlR5cGVFcnJvciIsImhhcyIsImdldCIsInNldCIsIldyYXBwZXIiLCJ2YWx1ZSIsIkx1eG9uRXJyb3IiLCJfRXJyb3IiLCJFcnJvciIsIkludmFsaWREYXRlVGltZUVycm9yIiwiX0x1eG9uRXJyb3IiLCJyZWFzb24iLCJ0b01lc3NhZ2UiLCJJbnZhbGlkSW50ZXJ2YWxFcnJvciIsIl9MdXhvbkVycm9yMiIsIkludmFsaWREdXJhdGlvbkVycm9yIiwiX0x1eG9uRXJyb3IzIiwiQ29uZmxpY3RpbmdTcGVjaWZpY2F0aW9uRXJyb3IiLCJfTHV4b25FcnJvcjQiLCJJbnZhbGlkVW5pdEVycm9yIiwiX0x1eG9uRXJyb3I1IiwidW5pdCIsIkludmFsaWRBcmd1bWVudEVycm9yIiwiX0x1eG9uRXJyb3I2IiwiWm9uZUlzQWJzdHJhY3RFcnJvciIsIl9MdXhvbkVycm9yNyIsIm4iLCJzIiwibCIsIkRBVEVfU0hPUlQiLCJ5ZWFyIiwibW9udGgiLCJkYXkiLCJEQVRFX01FRCIsIkRBVEVfRlVMTCIsIkRBVEVfSFVHRSIsIndlZWtkYXkiLCJUSU1FX1NJTVBMRSIsImhvdXIiLCJtaW51dGUiLCJUSU1FX1dJVEhfU0VDT05EUyIsInNlY29uZCIsIlRJTUVfV0lUSF9TSE9SVF9PRkZTRVQiLCJ0aW1lWm9uZU5hbWUiLCJUSU1FX1dJVEhfTE9OR19PRkZTRVQiLCJUSU1FXzI0X1NJTVBMRSIsImhvdXIxMiIsIlRJTUVfMjRfV0lUSF9TRUNPTkRTIiwiVElNRV8yNF9XSVRIX1NIT1JUX09GRlNFVCIsIlRJTUVfMjRfV0lUSF9MT05HX09GRlNFVCIsIkRBVEVUSU1FX1NIT1JUIiwiREFURVRJTUVfU0hPUlRfV0lUSF9TRUNPTkRTIiwiREFURVRJTUVfTUVEIiwiREFURVRJTUVfTUVEX1dJVEhfU0VDT05EUyIsIkRBVEVUSU1FX01FRF9XSVRIX1dFRUtEQVkiLCJEQVRFVElNRV9GVUxMIiwiREFURVRJTUVfRlVMTF9XSVRIX1NFQ09ORFMiLCJEQVRFVElNRV9IVUdFIiwiREFURVRJTUVfSFVHRV9XSVRIX1NFQ09ORFMiLCJpc1VuZGVmaW5lZCIsImlzTnVtYmVyIiwiaXNJbnRlZ2VyIiwiaXNTdHJpbmciLCJpc0RhdGUiLCJoYXNJbnRsIiwiSW50bCIsIkRhdGVUaW1lRm9ybWF0IiwiaGFzRm9ybWF0VG9QYXJ0cyIsImZvcm1hdFRvUGFydHMiLCJoYXNSZWxhdGl2ZSIsIlJlbGF0aXZlVGltZUZvcm1hdCIsIm1heWJlQXJyYXkiLCJ0aGluZyIsIkFycmF5IiwiaXNBcnJheSIsImJlc3RCeSIsImFyciIsImJ5IiwiY29tcGFyZSIsInJlZHVjZSIsImJlc3QiLCJuZXh0IiwicGFpciIsInBpY2siLCJvYmoiLCJrZXlzIiwiayIsImhhc093blByb3BlcnR5IiwicHJvcCIsImludGVnZXJCZXR3ZWVuIiwiYm90dG9tIiwidG9wIiwiZmxvb3JNb2QiLCJ4IiwiTWF0aCIsImZsb29yIiwicGFkU3RhcnQiLCJpbnB1dCIsInJlcGVhdCIsInNsaWNlIiwicGFyc2VJbnRlZ2VyIiwic3RyaW5nIiwicGFyc2VJbnQiLCJwYXJzZU1pbGxpcyIsImZyYWN0aW9uIiwiZiIsInBhcnNlRmxvYXQiLCJyb3VuZFRvIiwibnVtYmVyIiwiZGlnaXRzIiwidG93YXJkWmVybyIsImZhY3RvciIsInBvdyIsInJvdW5kZXIiLCJ0cnVuYyIsInJvdW5kIiwiaXNMZWFwWWVhciIsImRheXNJblllYXIiLCJkYXlzSW5Nb250aCIsIm1vZE1vbnRoIiwibW9kWWVhciIsIm9ialRvTG9jYWxUUyIsImQiLCJVVEMiLCJtaWxsaXNlY29uZCIsInNldFVUQ0Z1bGxZZWFyIiwiZ2V0VVRDRnVsbFllYXIiLCJ3ZWVrc0luV2Vla1llYXIiLCJ3ZWVrWWVhciIsInAxIiwibGFzdCIsInAyIiwidW50cnVuY2F0ZVllYXIiLCJwYXJzZVpvbmVJbmZvIiwidHMiLCJvZmZzZXRGb3JtYXQiLCJsb2NhbGUiLCJ0aW1lWm9uZSIsImRhdGUiLCJpbnRsT3B0cyIsIm1vZGlmaWVkIiwiYXNzaWduIiwiaW50bCIsInBhcnNlZCIsImZpbmQiLCJtIiwidHlwZSIsInRvTG93ZXJDYXNlIiwid2l0aG91dCIsImZvcm1hdCIsImluY2x1ZGVkIiwiZGlmZmVkIiwic3Vic3RyaW5nIiwidHJpbW1lZCIsInJlcGxhY2UiLCJzaWduZWRPZmZzZXQiLCJvZmZIb3VyU3RyIiwib2ZmTWludXRlU3RyIiwib2ZmSG91ciIsIk51bWJlciIsImlzTmFOIiwib2ZmTWluIiwib2ZmTWluU2lnbmVkIiwiaXMiLCJhc051bWJlciIsIm51bWVyaWNWYWx1ZSIsIm5vcm1hbGl6ZU9iamVjdCIsIm5vcm1hbGl6ZXIiLCJub25Vbml0S2V5cyIsIm5vcm1hbGl6ZWQiLCJ1IiwidiIsImZvcm1hdE9mZnNldCIsIm9mZnNldCIsImhvdXJzIiwibWludXRlcyIsImFicyIsInNpZ24iLCJiYXNlIiwiUmFuZ2VFcnJvciIsInRpbWVPYmplY3QiLCJpYW5hUmVnZXgiLCJzdHJpbmdpZnkiLCJKU09OIiwic29ydCIsIm1vbnRoc0xvbmciLCJtb250aHNTaG9ydCIsIm1vbnRoc05hcnJvdyIsIm1vbnRocyIsIndlZWtkYXlzTG9uZyIsIndlZWtkYXlzU2hvcnQiLCJ3ZWVrZGF5c05hcnJvdyIsIndlZWtkYXlzIiwibWVyaWRpZW1zIiwiZXJhc0xvbmciLCJlcmFzU2hvcnQiLCJlcmFzTmFycm93IiwiZXJhcyIsIm1lcmlkaWVtRm9yRGF0ZVRpbWUiLCJkdCIsIndlZWtkYXlGb3JEYXRlVGltZSIsIm1vbnRoRm9yRGF0ZVRpbWUiLCJlcmFGb3JEYXRlVGltZSIsImZvcm1hdFJlbGF0aXZlVGltZSIsImNvdW50IiwibnVtZXJpYyIsIm5hcnJvdyIsInVuaXRzIiwieWVhcnMiLCJxdWFydGVycyIsIndlZWtzIiwiZGF5cyIsInNlY29uZHMiLCJsYXN0YWJsZSIsImlzRGF5IiwiaXNJblBhc3QiLCJmbXRWYWx1ZSIsInNpbmd1bGFyIiwibGlsVW5pdHMiLCJmbXRVbml0IiwiZm9ybWF0U3RyaW5nIiwia25vd25Gb3JtYXQiLCJmaWx0ZXJlZCIsImRhdGVUaW1lSHVnZSIsInN0cmluZ2lmeVRva2VucyIsInNwbGl0cyIsInRva2VuVG9TdHJpbmciLCJfaXRlcmF0b3IiLCJfaXNBcnJheSIsIl9pIiwiU3ltYm9sIiwiaXRlcmF0b3IiLCJfcmVmIiwiZG9uZSIsInRva2VuIiwibGl0ZXJhbCIsInZhbCIsIl9tYWNyb1Rva2VuVG9Gb3JtYXRPcHRzIiwiRCIsIkREIiwiREREIiwiRERERCIsInQiLCJ0dCIsInR0dCIsInR0dHQiLCJUIiwiVFQiLCJUVFQiLCJUVFRUIiwiZmYiLCJmZmYiLCJmZmZmIiwiRiIsIkZGIiwiRkZGIiwiRkZGRiIsIkZvcm1hdHRlciIsIm9wdHMiLCJwYXJzZUZvcm1hdCIsImZtdCIsImN1cnJlbnQiLCJjdXJyZW50RnVsbCIsImJyYWNrZXRlZCIsImMiLCJjaGFyQXQiLCJtYWNyb1Rva2VuVG9Gb3JtYXRPcHRzIiwiZm9ybWF0T3B0cyIsImxvYyIsInN5c3RlbUxvYyIsIl9wcm90byIsImZvcm1hdFdpdGhTeXN0ZW1EZWZhdWx0IiwicmVkZWZhdWx0VG9TeXN0ZW0iLCJkZiIsImR0Rm9ybWF0dGVyIiwiZm9ybWF0RGF0ZVRpbWUiLCJmb3JtYXREYXRlVGltZVBhcnRzIiwicmVzb2x2ZWRPcHRpb25zIiwibnVtIiwiZm9yY2VTaW1wbGUiLCJwYWRUbyIsIm51bWJlckZvcm1hdHRlciIsImZvcm1hdERhdGVUaW1lRnJvbVN0cmluZyIsIl90aGlzIiwia25vd25FbmdsaXNoIiwibGlzdGluZ01vZGUiLCJ1c2VEYXRlVGltZUZvcm1hdHRlciIsIm91dHB1dENhbGVuZGFyIiwiZXh0cmFjdCIsImlzT2Zmc2V0Rml4ZWQiLCJhbGxvd1oiLCJpc1ZhbGlkIiwiem9uZSIsIm1lcmlkaWVtIiwic3RhbmRhbG9uZSIsIm1heWJlTWFjcm8iLCJlcmEiLCJvZmZzZXROYW1lIiwiem9uZU5hbWUiLCJ3ZWVrTnVtYmVyIiwib3JkaW5hbCIsInF1YXJ0ZXIiLCJmb3JtYXREdXJhdGlvbkZyb21TdHJpbmciLCJkdXIiLCJfdGhpczIiLCJ0b2tlblRvRmllbGQiLCJsaWxkdXIiLCJtYXBwZWQiLCJ0b2tlbnMiLCJyZWFsVG9rZW5zIiwiZm91bmQiLCJfcmVmMiIsImNvbmNhdCIsImNvbGxhcHNlZCIsInNoaWZ0VG8iLCJtYXAiLCJmaWx0ZXIiLCJJbnZhbGlkIiwiZXhwbGFuYXRpb24iLCJab25lIiwiZXF1YWxzIiwib3RoZXJab25lIiwic2luZ2xldG9uIiwiTG9jYWxab25lIiwiX1pvbmUiLCJmb3JtYXRPZmZzZXQkMSIsImdldFRpbWV6b25lT2Zmc2V0IiwibWF0Y2hpbmdSZWdleCIsIlJlZ0V4cCIsInNvdXJjZSIsImR0ZkNhY2hlIiwibWFrZURURiIsInR5cGVUb1BvcyIsImhhY2t5T2Zmc2V0IiwiZHRmIiwiZm9ybWF0dGVkIiwiZXhlYyIsImZNb250aCIsImZEYXkiLCJmWWVhciIsImZIb3VyIiwiZk1pbnV0ZSIsImZTZWNvbmQiLCJwYXJ0c09mZnNldCIsImZpbGxlZCIsIl9mb3JtYXR0ZWQkaSIsInBvcyIsImlhbmFab25lQ2FjaGUiLCJJQU5BWm9uZSIsIm5hbWUiLCJyZXNldENhY2hlIiwiaXNWYWxpZFNwZWNpZmllciIsIm1hdGNoIiwiaXNWYWxpZFpvbmUiLCJwYXJzZUdNVE9mZnNldCIsInNwZWNpZmllciIsInZhbGlkIiwiYWRqdXN0ZWRIb3VyIiwiYXNVVEMiLCJhc1RTIiwidmFsdWVPZiIsInNpbmdsZXRvbiQxIiwiRml4ZWRPZmZzZXRab25lIiwidXRjSW5zdGFuY2UiLCJwYXJzZVNwZWNpZmllciIsInIiLCJmaXhlZCIsIkludmFsaWRab25lIiwiTmFOIiwibm9ybWFsaXplWm9uZSIsImRlZmF1bHRab25lIiwibG93ZXJlZCIsIm5vdyIsImRlZmF1bHRMb2NhbGUiLCJkZWZhdWx0TnVtYmVyaW5nU3lzdGVtIiwiZGVmYXVsdE91dHB1dENhbGVuZGFyIiwidGhyb3dPbkludmFsaWQiLCJTZXR0aW5ncyIsInJlc2V0Q2FjaGVzIiwiTG9jYWxlIiwieiIsIm51bWJlcmluZ1N5c3RlbSIsImludGxEVENhY2hlIiwiZ2V0Q2FjaGVkRFRGIiwibG9jU3RyaW5nIiwiaW50bE51bUNhY2hlIiwiZ2V0Q2FjaGVkSU5GIiwiaW5mIiwiTnVtYmVyRm9ybWF0IiwiaW50bFJlbENhY2hlIiwiZ2V0Q2FjaGVkUlRGIiwic3lzTG9jYWxlQ2FjaGUiLCJzeXN0ZW1Mb2NhbGUiLCJjb21wdXRlZFN5cyIsInBhcnNlTG9jYWxlU3RyaW5nIiwibG9jYWxlU3RyIiwidUluZGV4Iiwib3B0aW9ucyIsInNtYWxsZXIiLCJfb3B0aW9ucyIsImNhbGVuZGFyIiwiaW50bENvbmZpZ1N0cmluZyIsIm1hcE1vbnRocyIsIm1zIiwiRGF0ZVRpbWUiLCJ1dGMiLCJtYXBXZWVrZGF5cyIsImxpc3RTdHVmZiIsImRlZmF1bHRPSyIsImVuZ2xpc2hGbiIsImludGxGbiIsIm1vZGUiLCJzdXBwb3J0c0Zhc3ROdW1iZXJzIiwic3RhcnRzV2l0aCIsIlBvbHlOdW1iZXJGb3JtYXR0ZXIiLCJ1c2VHcm91cGluZyIsIm1pbmltdW1JbnRlZ2VyRGlnaXRzIiwiX2ZpeGVkIiwiUG9seURhdGVGb3JtYXR0ZXIiLCJ1bml2ZXJzYWwiLCJmcm9tTWlsbGlzIiwiX3Byb3RvMiIsInRvSlNEYXRlIiwidG9rZW5Gb3JtYXQiLCJQb2x5UmVsRm9ybWF0dGVyIiwiaXNFbmdsaXNoIiwic3R5bGUiLCJydGYiLCJfcHJvdG8zIiwiZnJvbU9wdHMiLCJkZWZhdWx0VG9FTiIsInNwZWNpZmllZExvY2FsZSIsImxvY2FsZVIiLCJudW1iZXJpbmdTeXN0ZW1SIiwib3V0cHV0Q2FsZW5kYXJSIiwiZnJvbU9iamVjdCIsIl90ZW1wIiwibnVtYmVyaW5nIiwiX3BhcnNlTG9jYWxlU3RyaW5nIiwicGFyc2VkTG9jYWxlIiwicGFyc2VkTnVtYmVyaW5nU3lzdGVtIiwicGFyc2VkT3V0cHV0Q2FsZW5kYXIiLCJ3ZWVrZGF5c0NhY2hlIiwibW9udGhzQ2FjaGUiLCJtZXJpZGllbUNhY2hlIiwiZXJhQ2FjaGUiLCJmYXN0TnVtYmVyc0NhY2hlZCIsIl9wcm90bzQiLCJoYXNGVFAiLCJpc0FjdHVhbGx5RW4iLCJoYXNOb1dlaXJkbmVzcyIsImNsb25lIiwiYWx0cyIsImdldE93blByb3BlcnR5TmFtZXMiLCJyZWRlZmF1bHRUb0VOIiwibW9udGhzJDEiLCJmb3JtYXRTdHIiLCJ3ZWVrZGF5cyQxIiwibWVyaWRpZW1zJDEiLCJfdGhpczMiLCJlcmFzJDEiLCJfdGhpczQiLCJmaWVsZCIsInJlc3VsdHMiLCJtYXRjaGluZyIsImZhc3ROdW1iZXJzIiwicmVsRm9ybWF0dGVyIiwib3RoZXIiLCJjb21iaW5lUmVnZXhlcyIsIl9sZW4iLCJyZWdleGVzIiwiX2tleSIsImZ1bGwiLCJjb21iaW5lRXh0cmFjdG9ycyIsIl9sZW4yIiwiZXh0cmFjdG9ycyIsIl9rZXkyIiwiZXgiLCJtZXJnZWRWYWxzIiwibWVyZ2VkWm9uZSIsImN1cnNvciIsIl9leCIsInBhcnNlIiwiX2xlbjMiLCJwYXR0ZXJucyIsIl9rZXkzIiwiX3BhdHRlcm5zIiwiX3BhdHRlcm5zJF9pIiwicmVnZXgiLCJleHRyYWN0b3IiLCJzaW1wbGVQYXJzZSIsIl9sZW40IiwiX2tleTQiLCJyZXQiLCJvZmZzZXRSZWdleCIsImlzb1RpbWVCYXNlUmVnZXgiLCJpc29UaW1lUmVnZXgiLCJpc29UaW1lRXh0ZW5zaW9uUmVnZXgiLCJpc29ZbWRSZWdleCIsImlzb1dlZWtSZWdleCIsImlzb09yZGluYWxSZWdleCIsImV4dHJhY3RJU09XZWVrRGF0YSIsImV4dHJhY3RJU09PcmRpbmFsRGF0YSIsInNxbFltZFJlZ2V4Iiwic3FsVGltZVJlZ2V4Iiwic3FsVGltZUV4dGVuc2lvblJlZ2V4IiwiaW50IiwiZmFsbGJhY2siLCJleHRyYWN0SVNPWW1kIiwiaXRlbSIsImV4dHJhY3RJU09UaW1lIiwiZXh0cmFjdElTT09mZnNldCIsImxvY2FsIiwiZnVsbE9mZnNldCIsImV4dHJhY3RJQU5BWm9uZSIsImlzb0R1cmF0aW9uIiwiZXh0cmFjdElTT0R1cmF0aW9uIiwieWVhclN0ciIsIm1vbnRoU3RyIiwid2Vla1N0ciIsImRheVN0ciIsImhvdXJTdHIiLCJtaW51dGVTdHIiLCJzZWNvbmRTdHIiLCJtaWxsaXNlY29uZHNTdHIiLCJtaWxsaXNlY29uZHMiLCJvYnNPZmZzZXRzIiwiR01UIiwiRURUIiwiRVNUIiwiQ0RUIiwiQ1NUIiwiTURUIiwiTVNUIiwiUERUIiwiUFNUIiwiZnJvbVN0cmluZ3MiLCJ3ZWVrZGF5U3RyIiwicmVzdWx0IiwicmZjMjgyMiIsImV4dHJhY3RSRkMyODIyIiwib2JzT2Zmc2V0IiwibWlsT2Zmc2V0IiwicHJlcHJvY2Vzc1JGQzI4MjIiLCJ0cmltIiwicmZjMTEyMyIsInJmYzg1MCIsImFzY2lpIiwiZXh0cmFjdFJGQzExMjNPcjg1MCIsImV4dHJhY3RBU0NJSSIsImlzb1ltZFdpdGhUaW1lRXh0ZW5zaW9uUmVnZXgiLCJpc29XZWVrV2l0aFRpbWVFeHRlbnNpb25SZWdleCIsImlzb09yZGluYWxXaXRoVGltZUV4dGVuc2lvblJlZ2V4IiwiaXNvVGltZUNvbWJpbmVkUmVnZXgiLCJleHRyYWN0SVNPWW1kVGltZUFuZE9mZnNldCIsImV4dHJhY3RJU09XZWVrVGltZUFuZE9mZnNldCIsImV4dHJhY3RJU09PcmRpbmFsRGF0YUFuZFRpbWUiLCJleHRyYWN0SVNPVGltZUFuZE9mZnNldCIsInBhcnNlSVNPRGF0ZSIsInBhcnNlUkZDMjgyMkRhdGUiLCJwYXJzZUhUVFBEYXRlIiwicGFyc2VJU09EdXJhdGlvbiIsInNxbFltZFdpdGhUaW1lRXh0ZW5zaW9uUmVnZXgiLCJzcWxUaW1lQ29tYmluZWRSZWdleCIsImV4dHJhY3RJU09ZbWRUaW1lT2Zmc2V0QW5kSUFOQVpvbmUiLCJleHRyYWN0SVNPVGltZU9mZnNldEFuZElBTkFab25lIiwicGFyc2VTUUwiLCJJTlZBTElEIiwibG93T3JkZXJNYXRyaXgiLCJjYXN1YWxNYXRyaXgiLCJkYXlzSW5ZZWFyQWNjdXJhdGUiLCJkYXlzSW5Nb250aEFjY3VyYXRlIiwiYWNjdXJhdGVNYXRyaXgiLCJvcmRlcmVkVW5pdHMiLCJyZXZlcnNlVW5pdHMiLCJyZXZlcnNlIiwiY2xlYXIiLCJjb25mIiwidmFsdWVzIiwiY29udmVyc2lvbkFjY3VyYWN5IiwiRHVyYXRpb24iLCJhbnRpVHJ1bmMiLCJjZWlsIiwiY29udmVydCIsIm1hdHJpeCIsImZyb21NYXAiLCJmcm9tVW5pdCIsInRvTWFwIiwidG9Vbml0IiwiY29udiIsInJhdyIsInNhbWVTaWduIiwiYWRkZWQiLCJub3JtYWxpemVWYWx1ZXMiLCJ2YWxzIiwicHJldmlvdXMiLCJjb25maWciLCJhY2N1cmF0ZSIsImludmFsaWQiLCJpc0x1eG9uRHVyYXRpb24iLCJub3JtYWxpemVVbml0IiwiZnJvbUlTTyIsInRleHQiLCJfcGFyc2VJU09EdXJhdGlvbiIsIndlZWsiLCJpc0R1cmF0aW9uIiwidG9Gb3JtYXQiLCJmbXRPcHRzIiwidG9PYmplY3QiLCJpbmNsdWRlQ29uZmlnIiwidG9JU08iLCJ0b0pTT04iLCJhcyIsInBsdXMiLCJkdXJhdGlvbiIsImZyaWVuZGx5RHVyYXRpb24iLCJfb3JkZXJlZFVuaXRzIiwibWludXMiLCJuZWdhdGUiLCJtYXBVbml0cyIsIl9pMiIsIl9PYmplY3Qka2V5cyIsIm1peGVkIiwicmVjb25maWd1cmUiLCJub3JtYWxpemUiLCJidWlsdCIsImFjY3VtdWxhdGVkIiwibGFzdFVuaXQiLCJfaTMiLCJfb3JkZXJlZFVuaXRzMiIsIm93biIsImFrIiwiZG93biIsIm5lZ2F0ZWQiLCJfaTQiLCJfT2JqZWN0JGtleXMyIiwiX2k1IiwiX29yZGVyZWRVbml0czMiLCJkdXJhdGlvbmlzaCIsIklOVkFMSUQkMSIsInZhbGlkYXRlU3RhcnRFbmQiLCJzdGFydCIsImVuZCIsIkludGVydmFsIiwiaXNMdXhvbkludGVydmFsIiwiZnJvbURhdGVUaW1lcyIsImJ1aWx0U3RhcnQiLCJmcmllbmRseURhdGVUaW1lIiwiYnVpbHRFbmQiLCJ2YWxpZGF0ZUVycm9yIiwiYWZ0ZXIiLCJiZWZvcmUiLCJfc3BsaXQiLCJzcGxpdCIsIl9kdXIiLCJpc0ludGVydmFsIiwidG9EdXJhdGlvbiIsInN0YXJ0T2YiLCJkaWZmIiwiaGFzU2FtZSIsImlzRW1wdHkiLCJpc0FmdGVyIiwiZGF0ZVRpbWUiLCJpc0JlZm9yZSIsImNvbnRhaW5zIiwic3BsaXRBdCIsImRhdGVUaW1lcyIsInNvcnRlZCIsInNwbGl0QnkiLCJkaXZpZGVFcXVhbGx5IiwibnVtYmVyT2ZQYXJ0cyIsIm92ZXJsYXBzIiwiYWJ1dHNTdGFydCIsImFidXRzRW5kIiwiZW5ndWxmcyIsImludGVyc2VjdGlvbiIsInVuaW9uIiwibWVyZ2UiLCJpbnRlcnZhbHMiLCJfaW50ZXJ2YWxzJHNvcnQkcmVkdWMiLCJiIiwic29mYXIiLCJmaW5hbCIsInhvciIsIl9BcnJheSRwcm90b3R5cGUiLCJjdXJyZW50Q291bnQiLCJlbmRzIiwidGltZSIsImZsYXR0ZW5lZCIsIl9yZWYzIiwiZGlmZmVyZW5jZSIsInRvSVNPRGF0ZSIsInRvSVNPVGltZSIsImRhdGVGb3JtYXQiLCJfdGVtcDIiLCJfcmVmNCIsIl9yZWY0JHNlcGFyYXRvciIsInNlcGFyYXRvciIsImludmFsaWRSZWFzb24iLCJtYXBFbmRwb2ludHMiLCJtYXBGbiIsIkluZm8iLCJoYXNEU1QiLCJwcm90byIsInNldFpvbmUiLCJpc1ZhbGlkSUFOQVpvbmUiLCJub3JtYWxpemVab25lJDEiLCJfcmVmJGxvY2FsZSIsIl9yZWYkbnVtYmVyaW5nU3lzdGVtIiwiX3JlZiRvdXRwdXRDYWxlbmRhciIsIm1vbnRoc0Zvcm1hdCIsIl9yZWYyJGxvY2FsZSIsIl9yZWYyJG51bWJlcmluZ1N5c3RlbSIsIl9yZWYyJG91dHB1dENhbGVuZGFyIiwiX3RlbXAzIiwiX3JlZjMkbG9jYWxlIiwiX3JlZjMkbnVtYmVyaW5nU3lzdGVtIiwid2Vla2RheXNGb3JtYXQiLCJfdGVtcDQiLCJfcmVmNCRsb2NhbGUiLCJfcmVmNCRudW1iZXJpbmdTeXN0ZW0iLCJfdGVtcDUiLCJfcmVmNSIsIl9yZWY1JGxvY2FsZSIsIl90ZW1wNiIsIl9yZWY2IiwiX3JlZjYkbG9jYWxlIiwiZmVhdHVyZXMiLCJpbnRsVG9rZW5zIiwiem9uZXMiLCJyZWxhdGl2ZSIsImRheURpZmYiLCJlYXJsaWVyIiwibGF0ZXIiLCJ1dGNEYXlTdGFydCIsInRvVVRDIiwia2VlcExvY2FsVGltZSIsImhpZ2hPcmRlckRpZmZzIiwiZGlmZmVycyIsImxvd2VzdE9yZGVyIiwiaGlnaFdhdGVyIiwiX2RpZmZlcnMiLCJfZGlmZmVycyRfaSIsImRpZmZlciIsIl9jdXJzb3IkcGx1cyIsImRlbHRhIiwiX2N1cnNvciRwbHVzMiIsIl9kaWZmIiwiX2hpZ2hPcmRlckRpZmZzIiwicmVtYWluaW5nTWlsbGlzIiwibG93ZXJPcmRlclVuaXRzIiwiX2N1cnNvciRwbHVzMyIsIl9EdXJhdGlvbiRmcm9tTWlsbGlzIiwibnVtYmVyaW5nU3lzdGVtcyIsImFyYWIiLCJhcmFiZXh0IiwiYmFsaSIsImJlbmciLCJkZXZhIiwiZnVsbHdpZGUiLCJndWpyIiwiaGFuaWRlYyIsImtobXIiLCJrbmRhIiwibGFvbyIsImxpbWIiLCJtbHltIiwibW9uZyIsIm15bXIiLCJvcnlhIiwidGFtbGRlYyIsInRlbHUiLCJ0aGFpIiwidGlidCIsImxhdG4iLCJudW1iZXJpbmdTeXN0ZW1zVVRGMTYiLCJoYW5pZGVjQ2hhcnMiLCJwYXJzZURpZ2l0cyIsInN0ciIsImNvZGUiLCJjaGFyQ29kZUF0Iiwic2VhcmNoIiwiX251bWJlcmluZ1N5c3RlbXNVVEYiLCJtaW4iLCJtYXgiLCJkaWdpdFJlZ2V4IiwiYXBwZW5kIiwiTUlTU0lOR19GVFAiLCJpbnRVbml0IiwicG9zdCIsImRlc2VyIiwiZml4TGlzdFJlZ2V4Iiwic3RyaXBJbnNlbnNpdGl2aXRpZXMiLCJvbmVPZiIsInN0cmluZ3MiLCJzdGFydEluZGV4Iiwiam9pbiIsImZpbmRJbmRleCIsImdyb3VwcyIsImgiLCJzaW1wbGUiLCJlc2NhcGVUb2tlbiIsInVuaXRGb3JUb2tlbiIsIm9uZSIsInR3byIsInRocmVlIiwiZm91ciIsInNpeCIsIm9uZU9yVHdvIiwib25lVG9UaHJlZSIsIm9uZVRvU2l4Iiwib25lVG9OaW5lIiwidHdvVG9Gb3VyIiwiZm91clRvU2l4IiwidW5pdGF0ZSIsInBhcnRUeXBlU3R5bGVUb1Rva2VuVmFsIiwiZGF5cGVyaW9kIiwiZGF5UGVyaW9kIiwidG9rZW5Gb3JQYXJ0IiwicGFydCIsImJ1aWxkUmVnZXgiLCJyZSIsImhhbmRsZXJzIiwibWF0Y2hlcyIsImFsbCIsIm1hdGNoSW5kZXgiLCJkYXRlVGltZUZyb21NYXRjaGVzIiwidG9GaWVsZCIsIloiLCJxIiwiTSIsIkciLCJ5IiwiUyIsImR1bW15RGF0ZVRpbWVDYWNoZSIsImdldER1bW15RGF0ZVRpbWUiLCJtYXliZUV4cGFuZE1hY3JvVG9rZW4iLCJmb3JtYXR0ZXIiLCJwYXJ0cyIsImluY2x1ZGVzIiwiZXhwYW5kTWFjcm9Ub2tlbnMiLCJleHBsYWluRnJvbVRva2VucyIsImRpc3F1YWxpZnlpbmdVbml0IiwiX2J1aWxkUmVnZXgiLCJyZWdleFN0cmluZyIsIl9tYXRjaCIsInJhd01hdGNoZXMiLCJwYXJzZUZyb21Ub2tlbnMiLCJfZXhwbGFpbkZyb21Ub2tlbnMiLCJub25MZWFwTGFkZGVyIiwibGVhcExhZGRlciIsInVuaXRPdXRPZlJhbmdlIiwiZGF5T2ZXZWVrIiwianMiLCJnZXRVVENEYXkiLCJjb21wdXRlT3JkaW5hbCIsInVuY29tcHV0ZU9yZGluYWwiLCJ0YWJsZSIsIm1vbnRoMCIsImdyZWdvcmlhblRvV2VlayIsImdyZWdPYmoiLCJ3ZWVrVG9HcmVnb3JpYW4iLCJ3ZWVrRGF0YSIsIndlZWtkYXlPZkphbjQiLCJ5ZWFySW5EYXlzIiwiX3VuY29tcHV0ZU9yZGluYWwiLCJncmVnb3JpYW5Ub09yZGluYWwiLCJncmVnRGF0YSIsIm9yZGluYWxUb0dyZWdvcmlhbiIsIm9yZGluYWxEYXRhIiwiX3VuY29tcHV0ZU9yZGluYWwyIiwiaGFzSW52YWxpZFdlZWtEYXRhIiwidmFsaWRZZWFyIiwidmFsaWRXZWVrIiwidmFsaWRXZWVrZGF5IiwiaGFzSW52YWxpZE9yZGluYWxEYXRhIiwidmFsaWRPcmRpbmFsIiwiaGFzSW52YWxpZEdyZWdvcmlhbkRhdGEiLCJ2YWxpZE1vbnRoIiwidmFsaWREYXkiLCJoYXNJbnZhbGlkVGltZURhdGEiLCJ2YWxpZEhvdXIiLCJ2YWxpZE1pbnV0ZSIsInZhbGlkU2Vjb25kIiwidmFsaWRNaWxsaXNlY29uZCIsIklOVkFMSUQkMiIsIk1BWF9EQVRFIiwidW5zdXBwb3J0ZWRab25lIiwicG9zc2libHlDYWNoZWRXZWVrRGF0YSIsImNsb25lJDEiLCJpbnN0Iiwib2xkIiwiZml4T2Zmc2V0IiwibG9jYWxUUyIsInR6IiwidXRjR3Vlc3MiLCJvMiIsIm8zIiwidHNUb09iaiIsImdldFVUQ01vbnRoIiwiZ2V0VVRDRGF0ZSIsImdldFVUQ0hvdXJzIiwiZ2V0VVRDTWludXRlcyIsImdldFVUQ1NlY29uZHMiLCJnZXRVVENNaWxsaXNlY29uZHMiLCJvYmpUb1RTIiwiYWRqdXN0VGltZSIsIm9QcmUiLCJtaWxsaXNUb0FkZCIsIl9maXhPZmZzZXQiLCJwYXJzZURhdGFUb0RhdGVUaW1lIiwicGFyc2VkWm9uZSIsImludGVycHJldGF0aW9uWm9uZSIsInRvVGVjaEZvcm1hdCIsInRvVGVjaFRpbWVGb3JtYXQiLCJfcmVmJHN1cHByZXNzU2Vjb25kcyIsInN1cHByZXNzU2Vjb25kcyIsIl9yZWYkc3VwcHJlc3NNaWxsaXNlYyIsInN1cHByZXNzTWlsbGlzZWNvbmRzIiwiaW5jbHVkZU9mZnNldCIsIl9yZWYkaW5jbHVkZVpvbmUiLCJpbmNsdWRlWm9uZSIsIl9yZWYkc3BhY2Vab25lIiwic3BhY2Vab25lIiwiZGVmYXVsdFVuaXRWYWx1ZXMiLCJkZWZhdWx0V2Vla1VuaXRWYWx1ZXMiLCJkZWZhdWx0T3JkaW5hbFVuaXRWYWx1ZXMiLCJvcmRlcmVkVW5pdHMkMSIsIm9yZGVyZWRXZWVrVW5pdHMiLCJvcmRlcmVkT3JkaW5hbFVuaXRzIiwid2Vla251bWJlciIsIndlZWtzbnVtYmVyIiwid2Vla251bWJlcnMiLCJ3ZWVreWVhciIsIndlZWt5ZWFycyIsInF1aWNrRFQiLCJ0c05vdyIsIm9mZnNldFByb3ZpcyIsIl9vYmpUb1RTIiwiZGlmZlJlbGF0aXZlIiwiY2FsZW5kYXJ5IiwidW5jaGFuZ2VkIiwiX3pvbmUiLCJpc0x1eG9uRGF0ZVRpbWUiLCJmcm9tSlNEYXRlIiwiem9uZVRvVXNlIiwiZnJvbVNlY29uZHMiLCJjb250YWluc09yZGluYWwiLCJjb250YWluc0dyZWdvclllYXIiLCJjb250YWluc0dyZWdvck1EIiwiY29udGFpbnNHcmVnb3IiLCJkZWZpbml0ZVdlZWtEZWYiLCJ1c2VXZWVrRGF0YSIsImRlZmF1bHRWYWx1ZXMiLCJvYmpOb3ciLCJmb3VuZEZpcnN0IiwiX2l0ZXJhdG9yMiIsIl9pc0FycmF5MiIsImhpZ2hlck9yZGVySW52YWxpZCIsImdyZWdvcmlhbiIsIl9vYmpUb1RTMiIsInRzRmluYWwiLCJvZmZzZXRGaW5hbCIsIl9wYXJzZUlTT0RhdGUiLCJmcm9tUkZDMjgyMiIsIl9wYXJzZVJGQzI4MjJEYXRlIiwiZnJvbUhUVFAiLCJfcGFyc2VIVFRQRGF0ZSIsImZyb21Gb3JtYXQiLCJfb3B0cyIsIl9vcHRzJGxvY2FsZSIsIl9vcHRzJG51bWJlcmluZ1N5c3RlbSIsImxvY2FsZVRvVXNlIiwiX3BhcnNlRnJvbVRva2VucyIsImZyb21TdHJpbmciLCJmcm9tU1FMIiwiX3BhcnNlU1FMIiwiaXNEYXRlVGltZSIsInJlc29sdmVkTG9jYWxlT3B0cyIsIl9Gb3JtYXR0ZXIkY3JlYXRlJHJlcyIsInRvTG9jYWwiLCJfcmVmNSRrZWVwTG9jYWxUaW1lIiwiX3JlZjUka2VlcENhbGVuZGFyVGltIiwia2VlcENhbGVuZGFyVGltZSIsIm5ld1RTIiwib2Zmc2V0R3Vlc3MiLCJhc09iaiIsIl9vYmpUb1RTMyIsInNldExvY2FsZSIsInNldHRpbmdXZWVrU3R1ZmYiLCJfb2JqVG9UUzQiLCJub3JtYWxpemVkVW5pdCIsImVuZE9mIiwiX3RoaXMkcGx1cyIsInRvTG9jYWxlU3RyaW5nIiwidG9Mb2NhbGVQYXJ0cyIsInRvSVNPV2Vla0RhdGUiLCJfcmVmNyIsIl9yZWY3JHN1cHByZXNzTWlsbGlzZSIsIl9yZWY3JHN1cHByZXNzU2Vjb25kcyIsIl9yZWY3JGluY2x1ZGVPZmZzZXQiLCJ0b1JGQzI4MjIiLCJ0b0hUVFAiLCJ0b1NRTERhdGUiLCJ0b1NRTFRpbWUiLCJfcmVmOCIsIl9yZWY4JGluY2x1ZGVPZmZzZXQiLCJfcmVmOCRpbmNsdWRlWm9uZSIsInRvU1FMIiwidG9NaWxsaXMiLCJ0b1NlY29uZHMiLCJ0b0JTT04iLCJvdGhlckRhdGVUaW1lIiwiZHVyT3B0cyIsIm90aGVySXNMYXRlciIsImRpZmZOb3ciLCJ1bnRpbCIsImlucHV0TXMiLCJ0b1JlbGF0aXZlIiwicGFkZGluZyIsInRvUmVsYXRpdmVDYWxlbmRhciIsImV2ZXJ5IiwiZnJvbUZvcm1hdEV4cGxhaW4iLCJfb3B0aW9ucyRsb2NhbGUiLCJfb3B0aW9ucyRudW1iZXJpbmdTeXMiLCJmcm9tU3RyaW5nRXhwbGFpbiIsImRhdGVUaW1laXNoIl0sIm1hcHBpbmdzIjoiOztBQUFBLElBQUlBLEtBQUssR0FBSSxVQUFVQyxPQUFWLEVBQW1CO0FBQzlCOztBQUVBLFdBQVNDLGlCQUFULENBQTJCQyxNQUEzQixFQUFtQ0MsS0FBbkMsRUFBMEM7QUFDeEMsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRCxLQUFLLENBQUNFLE1BQTFCLEVBQWtDRCxDQUFDLEVBQW5DLEVBQXVDO0FBQ3JDLFVBQUlFLFVBQVUsR0FBR0gsS0FBSyxDQUFDQyxDQUFELENBQXRCO0FBQ0FFLE1BQUFBLFVBQVUsQ0FBQ0MsVUFBWCxHQUF3QkQsVUFBVSxDQUFDQyxVQUFYLElBQXlCLEtBQWpEO0FBQ0FELE1BQUFBLFVBQVUsQ0FBQ0UsWUFBWCxHQUEwQixJQUExQjtBQUNBLFVBQUksV0FBV0YsVUFBZixFQUEyQkEsVUFBVSxDQUFDRyxRQUFYLEdBQXNCLElBQXRCO0FBQzNCQyxNQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0JULE1BQXRCLEVBQThCSSxVQUFVLENBQUNNLEdBQXpDLEVBQThDTixVQUE5QztBQUNEO0FBQ0Y7O0FBRUQsV0FBU08sWUFBVCxDQUFzQkMsV0FBdEIsRUFBbUNDLFVBQW5DLEVBQStDQyxXQUEvQyxFQUE0RDtBQUMxRCxRQUFJRCxVQUFKLEVBQWdCZCxpQkFBaUIsQ0FBQ2EsV0FBVyxDQUFDRyxTQUFiLEVBQXdCRixVQUF4QixDQUFqQjtBQUNoQixRQUFJQyxXQUFKLEVBQWlCZixpQkFBaUIsQ0FBQ2EsV0FBRCxFQUFjRSxXQUFkLENBQWpCO0FBQ2pCLFdBQU9GLFdBQVA7QUFDRDs7QUFFRCxXQUFTSSxjQUFULENBQXdCQyxRQUF4QixFQUFrQ0MsVUFBbEMsRUFBOEM7QUFDNUNELElBQUFBLFFBQVEsQ0FBQ0YsU0FBVCxHQUFxQlAsTUFBTSxDQUFDVyxNQUFQLENBQWNELFVBQVUsQ0FBQ0gsU0FBekIsQ0FBckI7QUFDQUUsSUFBQUEsUUFBUSxDQUFDRixTQUFULENBQW1CSyxXQUFuQixHQUFpQ0gsUUFBakM7QUFDQUEsSUFBQUEsUUFBUSxDQUFDSSxTQUFULEdBQXFCSCxVQUFyQjtBQUNEOztBQUVELFdBQVNJLGVBQVQsQ0FBeUJDLENBQXpCLEVBQTRCO0FBQzFCRCxJQUFBQSxlQUFlLEdBQUdkLE1BQU0sQ0FBQ2dCLGNBQVAsR0FBd0JoQixNQUFNLENBQUNpQixjQUEvQixHQUFnRCxTQUFTSCxlQUFULENBQXlCQyxDQUF6QixFQUE0QjtBQUM1RixhQUFPQSxDQUFDLENBQUNGLFNBQUYsSUFBZWIsTUFBTSxDQUFDaUIsY0FBUCxDQUFzQkYsQ0FBdEIsQ0FBdEI7QUFDRCxLQUZEO0FBR0EsV0FBT0QsZUFBZSxDQUFDQyxDQUFELENBQXRCO0FBQ0Q7O0FBRUQsV0FBU0csZUFBVCxDQUF5QkgsQ0FBekIsRUFBNEJJLENBQTVCLEVBQStCO0FBQzdCRCxJQUFBQSxlQUFlLEdBQUdsQixNQUFNLENBQUNnQixjQUFQLElBQXlCLFNBQVNFLGVBQVQsQ0FBeUJILENBQXpCLEVBQTRCSSxDQUE1QixFQUErQjtBQUN4RUosTUFBQUEsQ0FBQyxDQUFDRixTQUFGLEdBQWNNLENBQWQ7QUFDQSxhQUFPSixDQUFQO0FBQ0QsS0FIRDs7QUFLQSxXQUFPRyxlQUFlLENBQUNILENBQUQsRUFBSUksQ0FBSixDQUF0QjtBQUNEOztBQUVELFdBQVNDLHdCQUFULEdBQW9DO0FBQ2xDLFFBQUksT0FBT0MsT0FBUCxLQUFtQixXQUFuQixJQUFrQyxDQUFDQSxPQUFPLENBQUNDLFNBQS9DLEVBQTBELE9BQU8sS0FBUDtBQUMxRCxRQUFJRCxPQUFPLENBQUNDLFNBQVIsQ0FBa0JDLElBQXRCLEVBQTRCLE9BQU8sS0FBUDtBQUM1QixRQUFJLE9BQU9DLEtBQVAsS0FBaUIsVUFBckIsRUFBaUMsT0FBTyxJQUFQOztBQUVqQyxRQUFJO0FBQ0ZDLE1BQUFBLElBQUksQ0FBQ2xCLFNBQUwsQ0FBZW1CLFFBQWYsQ0FBd0JDLElBQXhCLENBQTZCTixPQUFPLENBQUNDLFNBQVIsQ0FBa0JHLElBQWxCLEVBQXdCLEVBQXhCLEVBQTRCLFlBQVksQ0FBRSxDQUExQyxDQUE3QjtBQUNBLGFBQU8sSUFBUDtBQUNELEtBSEQsQ0FHRSxPQUFPRyxDQUFQLEVBQVU7QUFDVixhQUFPLEtBQVA7QUFDRDtBQUNGOztBQUVELFdBQVNDLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCQyxJQUE1QixFQUFrQ0MsS0FBbEMsRUFBeUM7QUFDdkMsUUFBSVosd0JBQXdCLEVBQTVCLEVBQWdDO0FBQzlCUyxNQUFBQSxVQUFVLEdBQUdSLE9BQU8sQ0FBQ0MsU0FBckI7QUFDRCxLQUZELE1BRU87QUFDTE8sTUFBQUEsVUFBVSxHQUFHLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCQyxJQUE1QixFQUFrQ0MsS0FBbEMsRUFBeUM7QUFDcEQsWUFBSUMsQ0FBQyxHQUFHLENBQUMsSUFBRCxDQUFSO0FBQ0FBLFFBQUFBLENBQUMsQ0FBQ0MsSUFBRixDQUFPQyxLQUFQLENBQWFGLENBQWIsRUFBZ0JGLElBQWhCO0FBQ0EsWUFBSTNCLFdBQVcsR0FBR2dDLFFBQVEsQ0FBQ0MsSUFBVCxDQUFjRixLQUFkLENBQW9CTCxNQUFwQixFQUE0QkcsQ0FBNUIsQ0FBbEI7QUFDQSxZQUFJSyxRQUFRLEdBQUcsSUFBSWxDLFdBQUosRUFBZjtBQUNBLFlBQUk0QixLQUFKLEVBQVdkLGVBQWUsQ0FBQ29CLFFBQUQsRUFBV04sS0FBSyxDQUFDekIsU0FBakIsQ0FBZjtBQUNYLGVBQU8rQixRQUFQO0FBQ0QsT0FQRDtBQVFEOztBQUVELFdBQU9ULFVBQVUsQ0FBQ00sS0FBWCxDQUFpQixJQUFqQixFQUF1QkksU0FBdkIsQ0FBUDtBQUNEOztBQUVELFdBQVNDLGlCQUFULENBQTJCQyxFQUEzQixFQUErQjtBQUM3QixXQUFPTCxRQUFRLENBQUNWLFFBQVQsQ0FBa0JDLElBQWxCLENBQXVCYyxFQUF2QixFQUEyQkMsT0FBM0IsQ0FBbUMsZUFBbkMsTUFBd0QsQ0FBQyxDQUFoRTtBQUNEOztBQUVELFdBQVNDLGdCQUFULENBQTBCWCxLQUExQixFQUFpQztBQUMvQixRQUFJWSxNQUFNLEdBQUcsT0FBT0MsR0FBUCxLQUFlLFVBQWYsR0FBNEIsSUFBSUEsR0FBSixFQUE1QixHQUF3Q0MsU0FBckQ7O0FBRUFILElBQUFBLGdCQUFnQixHQUFHLFNBQVNBLGdCQUFULENBQTBCWCxLQUExQixFQUFpQztBQUNsRCxVQUFJQSxLQUFLLEtBQUssSUFBVixJQUFrQixDQUFDUSxpQkFBaUIsQ0FBQ1IsS0FBRCxDQUF4QyxFQUFpRCxPQUFPQSxLQUFQOztBQUVqRCxVQUFJLE9BQU9BLEtBQVAsS0FBaUIsVUFBckIsRUFBaUM7QUFDL0IsY0FBTSxJQUFJZSxTQUFKLENBQWMsb0RBQWQsQ0FBTjtBQUNEOztBQUVELFVBQUksT0FBT0gsTUFBUCxLQUFrQixXQUF0QixFQUFtQztBQUNqQyxZQUFJQSxNQUFNLENBQUNJLEdBQVAsQ0FBV2hCLEtBQVgsQ0FBSixFQUF1QixPQUFPWSxNQUFNLENBQUNLLEdBQVAsQ0FBV2pCLEtBQVgsQ0FBUDs7QUFFdkJZLFFBQUFBLE1BQU0sQ0FBQ00sR0FBUCxDQUFXbEIsS0FBWCxFQUFrQm1CLE9BQWxCO0FBQ0Q7O0FBRUQsZUFBU0EsT0FBVCxHQUFtQjtBQUNqQixlQUFPdEIsVUFBVSxDQUFDRyxLQUFELEVBQVFPLFNBQVIsRUFBbUJ6QixlQUFlLENBQUMsSUFBRCxDQUFmLENBQXNCRixXQUF6QyxDQUFqQjtBQUNEOztBQUVEdUMsTUFBQUEsT0FBTyxDQUFDNUMsU0FBUixHQUFvQlAsTUFBTSxDQUFDVyxNQUFQLENBQWNxQixLQUFLLENBQUN6QixTQUFwQixFQUErQjtBQUNqREssUUFBQUEsV0FBVyxFQUFFO0FBQ1h3QyxVQUFBQSxLQUFLLEVBQUVELE9BREk7QUFFWHRELFVBQUFBLFVBQVUsRUFBRSxLQUZEO0FBR1hFLFVBQUFBLFFBQVEsRUFBRSxJQUhDO0FBSVhELFVBQUFBLFlBQVksRUFBRTtBQUpIO0FBRG9DLE9BQS9CLENBQXBCO0FBUUEsYUFBT29CLGVBQWUsQ0FBQ2lDLE9BQUQsRUFBVW5CLEtBQVYsQ0FBdEI7QUFDRCxLQTFCRDs7QUE0QkEsV0FBT1csZ0JBQWdCLENBQUNYLEtBQUQsQ0FBdkI7QUFDRCxHQTNHNkIsQ0E2RzlCOztBQUVBOzs7OztBQUdBLE1BQUlxQixVQUFVO0FBQ2Q7QUFDQSxZQUFVQyxNQUFWLEVBQWtCO0FBQ2hCOUMsSUFBQUEsY0FBYyxDQUFDNkMsVUFBRCxFQUFhQyxNQUFiLENBQWQ7O0FBRUEsYUFBU0QsVUFBVCxHQUFzQjtBQUNwQixhQUFPQyxNQUFNLENBQUNuQixLQUFQLENBQWEsSUFBYixFQUFtQkksU0FBbkIsS0FBaUMsSUFBeEM7QUFDRDs7QUFFRCxXQUFPYyxVQUFQO0FBQ0QsR0FSRCxDQVFFVixnQkFBZ0IsQ0FBQ1ksS0FBRCxDQVJsQixDQUZBO0FBV0E7Ozs7O0FBS0EsTUFBSUMsb0JBQW9CO0FBQ3hCO0FBQ0EsWUFBVUMsV0FBVixFQUF1QjtBQUNyQmpELElBQUFBLGNBQWMsQ0FBQ2dELG9CQUFELEVBQXVCQyxXQUF2QixDQUFkOztBQUVBLGFBQVNELG9CQUFULENBQThCRSxNQUE5QixFQUFzQztBQUNwQyxhQUFPRCxXQUFXLENBQUM5QixJQUFaLENBQWlCLElBQWpCLEVBQXVCLHVCQUF1QitCLE1BQU0sQ0FBQ0MsU0FBUCxFQUE5QyxLQUFxRSxJQUE1RTtBQUNEOztBQUVELFdBQU9ILG9CQUFQO0FBQ0QsR0FSRCxDQVFFSCxVQVJGLENBRkE7QUFXQTs7Ozs7QUFJQSxNQUFJTyxvQkFBb0I7QUFDeEI7QUFDQSxZQUFVQyxZQUFWLEVBQXdCO0FBQ3RCckQsSUFBQUEsY0FBYyxDQUFDb0Qsb0JBQUQsRUFBdUJDLFlBQXZCLENBQWQ7O0FBRUEsYUFBU0Qsb0JBQVQsQ0FBOEJGLE1BQTlCLEVBQXNDO0FBQ3BDLGFBQU9HLFlBQVksQ0FBQ2xDLElBQWIsQ0FBa0IsSUFBbEIsRUFBd0IsdUJBQXVCK0IsTUFBTSxDQUFDQyxTQUFQLEVBQS9DLEtBQXNFLElBQTdFO0FBQ0Q7O0FBRUQsV0FBT0Msb0JBQVA7QUFDRCxHQVJELENBUUVQLFVBUkYsQ0FGQTtBQVdBOzs7OztBQUlBLE1BQUlTLG9CQUFvQjtBQUN4QjtBQUNBLFlBQVVDLFlBQVYsRUFBd0I7QUFDdEJ2RCxJQUFBQSxjQUFjLENBQUNzRCxvQkFBRCxFQUF1QkMsWUFBdkIsQ0FBZDs7QUFFQSxhQUFTRCxvQkFBVCxDQUE4QkosTUFBOUIsRUFBc0M7QUFDcEMsYUFBT0ssWUFBWSxDQUFDcEMsSUFBYixDQUFrQixJQUFsQixFQUF3Qix1QkFBdUIrQixNQUFNLENBQUNDLFNBQVAsRUFBL0MsS0FBc0UsSUFBN0U7QUFDRDs7QUFFRCxXQUFPRyxvQkFBUDtBQUNELEdBUkQsQ0FRRVQsVUFSRixDQUZBO0FBV0E7Ozs7O0FBSUEsTUFBSVcsNkJBQTZCO0FBQ2pDO0FBQ0EsWUFBVUMsWUFBVixFQUF3QjtBQUN0QnpELElBQUFBLGNBQWMsQ0FBQ3dELDZCQUFELEVBQWdDQyxZQUFoQyxDQUFkOztBQUVBLGFBQVNELDZCQUFULEdBQXlDO0FBQ3ZDLGFBQU9DLFlBQVksQ0FBQzlCLEtBQWIsQ0FBbUIsSUFBbkIsRUFBeUJJLFNBQXpCLEtBQXVDLElBQTlDO0FBQ0Q7O0FBRUQsV0FBT3lCLDZCQUFQO0FBQ0QsR0FSRCxDQVFFWCxVQVJGLENBRkE7QUFXQTs7Ozs7QUFJQSxNQUFJYSxnQkFBZ0I7QUFDcEI7QUFDQSxZQUFVQyxZQUFWLEVBQXdCO0FBQ3RCM0QsSUFBQUEsY0FBYyxDQUFDMEQsZ0JBQUQsRUFBbUJDLFlBQW5CLENBQWQ7O0FBRUEsYUFBU0QsZ0JBQVQsQ0FBMEJFLElBQTFCLEVBQWdDO0FBQzlCLGFBQU9ELFlBQVksQ0FBQ3hDLElBQWIsQ0FBa0IsSUFBbEIsRUFBd0Isa0JBQWtCeUMsSUFBMUMsS0FBbUQsSUFBMUQ7QUFDRDs7QUFFRCxXQUFPRixnQkFBUDtBQUNELEdBUkQsQ0FRRWIsVUFSRixDQUZBO0FBV0E7Ozs7O0FBSUEsTUFBSWdCLG9CQUFvQjtBQUN4QjtBQUNBLFlBQVVDLFlBQVYsRUFBd0I7QUFDdEI5RCxJQUFBQSxjQUFjLENBQUM2RCxvQkFBRCxFQUF1QkMsWUFBdkIsQ0FBZDs7QUFFQSxhQUFTRCxvQkFBVCxHQUFnQztBQUM5QixhQUFPQyxZQUFZLENBQUNuQyxLQUFiLENBQW1CLElBQW5CLEVBQXlCSSxTQUF6QixLQUF1QyxJQUE5QztBQUNEOztBQUVELFdBQU84QixvQkFBUDtBQUNELEdBUkQsQ0FRRWhCLFVBUkYsQ0FGQTtBQVdBOzs7OztBQUlBLE1BQUlrQixtQkFBbUI7QUFDdkI7QUFDQSxZQUFVQyxZQUFWLEVBQXdCO0FBQ3RCaEUsSUFBQUEsY0FBYyxDQUFDK0QsbUJBQUQsRUFBc0JDLFlBQXRCLENBQWQ7O0FBRUEsYUFBU0QsbUJBQVQsR0FBK0I7QUFDN0IsYUFBT0MsWUFBWSxDQUFDN0MsSUFBYixDQUFrQixJQUFsQixFQUF3QiwyQkFBeEIsS0FBd0QsSUFBL0Q7QUFDRDs7QUFFRCxXQUFPNEMsbUJBQVA7QUFDRCxHQVJELENBUUVsQixVQVJGLENBRkE7QUFZQTs7Ozs7QUFHQSxNQUFJb0IsQ0FBQyxHQUFHLFNBQVI7QUFBQSxNQUNJQyxDQUFDLEdBQUcsT0FEUjtBQUFBLE1BRUlDLENBQUMsR0FBRyxNQUZSO0FBR0EsTUFBSUMsVUFBVSxHQUFHO0FBQ2ZDLElBQUFBLElBQUksRUFBRUosQ0FEUztBQUVmSyxJQUFBQSxLQUFLLEVBQUVMLENBRlE7QUFHZk0sSUFBQUEsR0FBRyxFQUFFTjtBQUhVLEdBQWpCO0FBS0EsTUFBSU8sUUFBUSxHQUFHO0FBQ2JILElBQUFBLElBQUksRUFBRUosQ0FETztBQUViSyxJQUFBQSxLQUFLLEVBQUVKLENBRk07QUFHYkssSUFBQUEsR0FBRyxFQUFFTjtBQUhRLEdBQWY7QUFLQSxNQUFJUSxTQUFTLEdBQUc7QUFDZEosSUFBQUEsSUFBSSxFQUFFSixDQURRO0FBRWRLLElBQUFBLEtBQUssRUFBRUgsQ0FGTztBQUdkSSxJQUFBQSxHQUFHLEVBQUVOO0FBSFMsR0FBaEI7QUFLQSxNQUFJUyxTQUFTLEdBQUc7QUFDZEwsSUFBQUEsSUFBSSxFQUFFSixDQURRO0FBRWRLLElBQUFBLEtBQUssRUFBRUgsQ0FGTztBQUdkSSxJQUFBQSxHQUFHLEVBQUVOLENBSFM7QUFJZFUsSUFBQUEsT0FBTyxFQUFFUjtBQUpLLEdBQWhCO0FBTUEsTUFBSVMsV0FBVyxHQUFHO0FBQ2hCQyxJQUFBQSxJQUFJLEVBQUVaLENBRFU7QUFFaEJhLElBQUFBLE1BQU0sRUFBRWI7QUFGUSxHQUFsQjtBQUlBLE1BQUljLGlCQUFpQixHQUFHO0FBQ3RCRixJQUFBQSxJQUFJLEVBQUVaLENBRGdCO0FBRXRCYSxJQUFBQSxNQUFNLEVBQUViLENBRmM7QUFHdEJlLElBQUFBLE1BQU0sRUFBRWY7QUFIYyxHQUF4QjtBQUtBLE1BQUlnQixzQkFBc0IsR0FBRztBQUMzQkosSUFBQUEsSUFBSSxFQUFFWixDQURxQjtBQUUzQmEsSUFBQUEsTUFBTSxFQUFFYixDQUZtQjtBQUczQmUsSUFBQUEsTUFBTSxFQUFFZixDQUhtQjtBQUkzQmlCLElBQUFBLFlBQVksRUFBRWhCO0FBSmEsR0FBN0I7QUFNQSxNQUFJaUIscUJBQXFCLEdBQUc7QUFDMUJOLElBQUFBLElBQUksRUFBRVosQ0FEb0I7QUFFMUJhLElBQUFBLE1BQU0sRUFBRWIsQ0FGa0I7QUFHMUJlLElBQUFBLE1BQU0sRUFBRWYsQ0FIa0I7QUFJMUJpQixJQUFBQSxZQUFZLEVBQUVmO0FBSlksR0FBNUI7QUFNQSxNQUFJaUIsY0FBYyxHQUFHO0FBQ25CUCxJQUFBQSxJQUFJLEVBQUVaLENBRGE7QUFFbkJhLElBQUFBLE1BQU0sRUFBRWIsQ0FGVztBQUduQm9CLElBQUFBLE1BQU0sRUFBRTtBQUhXLEdBQXJCO0FBS0E7Ozs7QUFJQSxNQUFJQyxvQkFBb0IsR0FBRztBQUN6QlQsSUFBQUEsSUFBSSxFQUFFWixDQURtQjtBQUV6QmEsSUFBQUEsTUFBTSxFQUFFYixDQUZpQjtBQUd6QmUsSUFBQUEsTUFBTSxFQUFFZixDQUhpQjtBQUl6Qm9CLElBQUFBLE1BQU0sRUFBRTtBQUppQixHQUEzQjtBQU1BOzs7O0FBSUEsTUFBSUUseUJBQXlCLEdBQUc7QUFDOUJWLElBQUFBLElBQUksRUFBRVosQ0FEd0I7QUFFOUJhLElBQUFBLE1BQU0sRUFBRWIsQ0FGc0I7QUFHOUJlLElBQUFBLE1BQU0sRUFBRWYsQ0FIc0I7QUFJOUJvQixJQUFBQSxNQUFNLEVBQUUsS0FKc0I7QUFLOUJILElBQUFBLFlBQVksRUFBRWhCO0FBTGdCLEdBQWhDO0FBT0E7Ozs7QUFJQSxNQUFJc0Isd0JBQXdCLEdBQUc7QUFDN0JYLElBQUFBLElBQUksRUFBRVosQ0FEdUI7QUFFN0JhLElBQUFBLE1BQU0sRUFBRWIsQ0FGcUI7QUFHN0JlLElBQUFBLE1BQU0sRUFBRWYsQ0FIcUI7QUFJN0JvQixJQUFBQSxNQUFNLEVBQUUsS0FKcUI7QUFLN0JILElBQUFBLFlBQVksRUFBRWY7QUFMZSxHQUEvQjtBQU9BOzs7O0FBSUEsTUFBSXNCLGNBQWMsR0FBRztBQUNuQnBCLElBQUFBLElBQUksRUFBRUosQ0FEYTtBQUVuQkssSUFBQUEsS0FBSyxFQUFFTCxDQUZZO0FBR25CTSxJQUFBQSxHQUFHLEVBQUVOLENBSGM7QUFJbkJZLElBQUFBLElBQUksRUFBRVosQ0FKYTtBQUtuQmEsSUFBQUEsTUFBTSxFQUFFYjtBQUxXLEdBQXJCO0FBT0E7Ozs7QUFJQSxNQUFJeUIsMkJBQTJCLEdBQUc7QUFDaENyQixJQUFBQSxJQUFJLEVBQUVKLENBRDBCO0FBRWhDSyxJQUFBQSxLQUFLLEVBQUVMLENBRnlCO0FBR2hDTSxJQUFBQSxHQUFHLEVBQUVOLENBSDJCO0FBSWhDWSxJQUFBQSxJQUFJLEVBQUVaLENBSjBCO0FBS2hDYSxJQUFBQSxNQUFNLEVBQUViLENBTHdCO0FBTWhDZSxJQUFBQSxNQUFNLEVBQUVmO0FBTndCLEdBQWxDO0FBUUEsTUFBSTBCLFlBQVksR0FBRztBQUNqQnRCLElBQUFBLElBQUksRUFBRUosQ0FEVztBQUVqQkssSUFBQUEsS0FBSyxFQUFFSixDQUZVO0FBR2pCSyxJQUFBQSxHQUFHLEVBQUVOLENBSFk7QUFJakJZLElBQUFBLElBQUksRUFBRVosQ0FKVztBQUtqQmEsSUFBQUEsTUFBTSxFQUFFYjtBQUxTLEdBQW5CO0FBT0EsTUFBSTJCLHlCQUF5QixHQUFHO0FBQzlCdkIsSUFBQUEsSUFBSSxFQUFFSixDQUR3QjtBQUU5QkssSUFBQUEsS0FBSyxFQUFFSixDQUZ1QjtBQUc5QkssSUFBQUEsR0FBRyxFQUFFTixDQUh5QjtBQUk5QlksSUFBQUEsSUFBSSxFQUFFWixDQUp3QjtBQUs5QmEsSUFBQUEsTUFBTSxFQUFFYixDQUxzQjtBQU05QmUsSUFBQUEsTUFBTSxFQUFFZjtBQU5zQixHQUFoQztBQVFBLE1BQUk0Qix5QkFBeUIsR0FBRztBQUM5QnhCLElBQUFBLElBQUksRUFBRUosQ0FEd0I7QUFFOUJLLElBQUFBLEtBQUssRUFBRUosQ0FGdUI7QUFHOUJLLElBQUFBLEdBQUcsRUFBRU4sQ0FIeUI7QUFJOUJVLElBQUFBLE9BQU8sRUFBRVQsQ0FKcUI7QUFLOUJXLElBQUFBLElBQUksRUFBRVosQ0FMd0I7QUFNOUJhLElBQUFBLE1BQU0sRUFBRWI7QUFOc0IsR0FBaEM7QUFRQSxNQUFJNkIsYUFBYSxHQUFHO0FBQ2xCekIsSUFBQUEsSUFBSSxFQUFFSixDQURZO0FBRWxCSyxJQUFBQSxLQUFLLEVBQUVILENBRlc7QUFHbEJJLElBQUFBLEdBQUcsRUFBRU4sQ0FIYTtBQUlsQlksSUFBQUEsSUFBSSxFQUFFWixDQUpZO0FBS2xCYSxJQUFBQSxNQUFNLEVBQUViLENBTFU7QUFNbEJpQixJQUFBQSxZQUFZLEVBQUVoQjtBQU5JLEdBQXBCO0FBUUEsTUFBSTZCLDBCQUEwQixHQUFHO0FBQy9CMUIsSUFBQUEsSUFBSSxFQUFFSixDQUR5QjtBQUUvQkssSUFBQUEsS0FBSyxFQUFFSCxDQUZ3QjtBQUcvQkksSUFBQUEsR0FBRyxFQUFFTixDQUgwQjtBQUkvQlksSUFBQUEsSUFBSSxFQUFFWixDQUp5QjtBQUsvQmEsSUFBQUEsTUFBTSxFQUFFYixDQUx1QjtBQU0vQmUsSUFBQUEsTUFBTSxFQUFFZixDQU51QjtBQU8vQmlCLElBQUFBLFlBQVksRUFBRWhCO0FBUGlCLEdBQWpDO0FBU0EsTUFBSThCLGFBQWEsR0FBRztBQUNsQjNCLElBQUFBLElBQUksRUFBRUosQ0FEWTtBQUVsQkssSUFBQUEsS0FBSyxFQUFFSCxDQUZXO0FBR2xCSSxJQUFBQSxHQUFHLEVBQUVOLENBSGE7QUFJbEJVLElBQUFBLE9BQU8sRUFBRVIsQ0FKUztBQUtsQlUsSUFBQUEsSUFBSSxFQUFFWixDQUxZO0FBTWxCYSxJQUFBQSxNQUFNLEVBQUViLENBTlU7QUFPbEJpQixJQUFBQSxZQUFZLEVBQUVmO0FBUEksR0FBcEI7QUFTQSxNQUFJOEIsMEJBQTBCLEdBQUc7QUFDL0I1QixJQUFBQSxJQUFJLEVBQUVKLENBRHlCO0FBRS9CSyxJQUFBQSxLQUFLLEVBQUVILENBRndCO0FBRy9CSSxJQUFBQSxHQUFHLEVBQUVOLENBSDBCO0FBSS9CVSxJQUFBQSxPQUFPLEVBQUVSLENBSnNCO0FBSy9CVSxJQUFBQSxJQUFJLEVBQUVaLENBTHlCO0FBTS9CYSxJQUFBQSxNQUFNLEVBQUViLENBTnVCO0FBTy9CZSxJQUFBQSxNQUFNLEVBQUVmLENBUHVCO0FBUS9CaUIsSUFBQUEsWUFBWSxFQUFFZjtBQVJpQixHQUFqQztBQVdBOzs7Ozs7QUFLQTs7O0FBR0E7O0FBRUEsV0FBUytCLFdBQVQsQ0FBcUIzRixDQUFyQixFQUF3QjtBQUN0QixXQUFPLE9BQU9BLENBQVAsS0FBYSxXQUFwQjtBQUNEOztBQUNELFdBQVM0RixRQUFULENBQWtCNUYsQ0FBbEIsRUFBcUI7QUFDbkIsV0FBTyxPQUFPQSxDQUFQLEtBQWEsUUFBcEI7QUFDRDs7QUFDRCxXQUFTNkYsU0FBVCxDQUFtQjdGLENBQW5CLEVBQXNCO0FBQ3BCLFdBQU8sT0FBT0EsQ0FBUCxLQUFhLFFBQWIsSUFBeUJBLENBQUMsR0FBRyxDQUFKLEtBQVUsQ0FBMUM7QUFDRDs7QUFDRCxXQUFTOEYsUUFBVCxDQUFrQjlGLENBQWxCLEVBQXFCO0FBQ25CLFdBQU8sT0FBT0EsQ0FBUCxLQUFhLFFBQXBCO0FBQ0Q7O0FBQ0QsV0FBUytGLE1BQVQsQ0FBZ0IvRixDQUFoQixFQUFtQjtBQUNqQixXQUFPZixNQUFNLENBQUNPLFNBQVAsQ0FBaUJtQixRQUFqQixDQUEwQkMsSUFBMUIsQ0FBK0JaLENBQS9CLE1BQXNDLGVBQTdDO0FBQ0QsR0F4YTZCLENBd2E1Qjs7O0FBRUYsV0FBU2dHLE9BQVQsR0FBbUI7QUFDakIsUUFBSTtBQUNGLGFBQU8sT0FBT0MsSUFBUCxLQUFnQixXQUFoQixJQUErQkEsSUFBSSxDQUFDQyxjQUEzQztBQUNELEtBRkQsQ0FFRSxPQUFPckYsQ0FBUCxFQUFVO0FBQ1YsYUFBTyxLQUFQO0FBQ0Q7QUFDRjs7QUFDRCxXQUFTc0YsZ0JBQVQsR0FBNEI7QUFDMUIsV0FBTyxDQUFDUixXQUFXLENBQUNNLElBQUksQ0FBQ0MsY0FBTCxDQUFvQjFHLFNBQXBCLENBQThCNEcsYUFBL0IsQ0FBbkI7QUFDRDs7QUFDRCxXQUFTQyxXQUFULEdBQXVCO0FBQ3JCLFFBQUk7QUFDRixhQUFPLE9BQU9KLElBQVAsS0FBZ0IsV0FBaEIsSUFBK0IsQ0FBQyxDQUFDQSxJQUFJLENBQUNLLGtCQUE3QztBQUNELEtBRkQsQ0FFRSxPQUFPekYsQ0FBUCxFQUFVO0FBQ1YsYUFBTyxLQUFQO0FBQ0Q7QUFDRixHQTFiNkIsQ0EwYjVCOzs7QUFFRixXQUFTMEYsVUFBVCxDQUFvQkMsS0FBcEIsRUFBMkI7QUFDekIsV0FBT0MsS0FBSyxDQUFDQyxPQUFOLENBQWNGLEtBQWQsSUFBdUJBLEtBQXZCLEdBQStCLENBQUNBLEtBQUQsQ0FBdEM7QUFDRDs7QUFDRCxXQUFTRyxNQUFULENBQWdCQyxHQUFoQixFQUFxQkMsRUFBckIsRUFBeUJDLE9BQXpCLEVBQWtDO0FBQ2hDLFFBQUlGLEdBQUcsQ0FBQ2hJLE1BQUosS0FBZSxDQUFuQixFQUFzQjtBQUNwQixhQUFPbUQsU0FBUDtBQUNEOztBQUVELFdBQU82RSxHQUFHLENBQUNHLE1BQUosQ0FBVyxVQUFVQyxJQUFWLEVBQWdCQyxJQUFoQixFQUFzQjtBQUN0QyxVQUFJQyxJQUFJLEdBQUcsQ0FBQ0wsRUFBRSxDQUFDSSxJQUFELENBQUgsRUFBV0EsSUFBWCxDQUFYOztBQUVBLFVBQUksQ0FBQ0QsSUFBTCxFQUFXO0FBQ1QsZUFBT0UsSUFBUDtBQUNELE9BRkQsTUFFTyxJQUFJSixPQUFPLENBQUNFLElBQUksQ0FBQyxDQUFELENBQUwsRUFBVUUsSUFBSSxDQUFDLENBQUQsQ0FBZCxDQUFQLEtBQThCRixJQUFJLENBQUMsQ0FBRCxDQUF0QyxFQUEyQztBQUNoRCxlQUFPQSxJQUFQO0FBQ0QsT0FGTSxNQUVBO0FBQ0wsZUFBT0UsSUFBUDtBQUNEO0FBQ0YsS0FWTSxFQVVKLElBVkksRUFVRSxDQVZGLENBQVA7QUFXRDs7QUFDRCxXQUFTQyxJQUFULENBQWNDLEdBQWQsRUFBbUJDLElBQW5CLEVBQXlCO0FBQ3ZCLFdBQU9BLElBQUksQ0FBQ04sTUFBTCxDQUFZLFVBQVU3RixDQUFWLEVBQWFvRyxDQUFiLEVBQWdCO0FBQ2pDcEcsTUFBQUEsQ0FBQyxDQUFDb0csQ0FBRCxDQUFELEdBQU9GLEdBQUcsQ0FBQ0UsQ0FBRCxDQUFWO0FBQ0EsYUFBT3BHLENBQVA7QUFDRCxLQUhNLEVBR0osRUFISSxDQUFQO0FBSUQ7O0FBQ0QsV0FBU3FHLGNBQVQsQ0FBd0JILEdBQXhCLEVBQTZCSSxJQUE3QixFQUFtQztBQUNqQyxXQUFPdkksTUFBTSxDQUFDTyxTQUFQLENBQWlCK0gsY0FBakIsQ0FBZ0MzRyxJQUFoQyxDQUFxQ3dHLEdBQXJDLEVBQTBDSSxJQUExQyxDQUFQO0FBQ0QsR0F4ZDZCLENBd2Q1Qjs7O0FBRUYsV0FBU0MsY0FBVCxDQUF3QmpCLEtBQXhCLEVBQStCa0IsTUFBL0IsRUFBdUNDLEdBQXZDLEVBQTRDO0FBQzFDLFdBQU85QixTQUFTLENBQUNXLEtBQUQsQ0FBVCxJQUFvQkEsS0FBSyxJQUFJa0IsTUFBN0IsSUFBdUNsQixLQUFLLElBQUltQixHQUF2RDtBQUNELEdBNWQ2QixDQTRkNUI7OztBQUVGLFdBQVNDLFFBQVQsQ0FBa0JDLENBQWxCLEVBQXFCbkUsQ0FBckIsRUFBd0I7QUFDdEIsV0FBT21FLENBQUMsR0FBR25FLENBQUMsR0FBR29FLElBQUksQ0FBQ0MsS0FBTCxDQUFXRixDQUFDLEdBQUduRSxDQUFmLENBQWY7QUFDRDs7QUFDRCxXQUFTc0UsUUFBVCxDQUFrQkMsS0FBbEIsRUFBeUJ2RSxDQUF6QixFQUE0QjtBQUMxQixRQUFJQSxDQUFDLEtBQUssS0FBSyxDQUFmLEVBQWtCO0FBQ2hCQSxNQUFBQSxDQUFDLEdBQUcsQ0FBSjtBQUNEOztBQUVELFFBQUl1RSxLQUFLLENBQUN0SCxRQUFOLEdBQWlCL0IsTUFBakIsR0FBMEI4RSxDQUE5QixFQUFpQztBQUMvQixhQUFPLENBQUMsSUFBSXdFLE1BQUosQ0FBV3hFLENBQVgsSUFBZ0J1RSxLQUFqQixFQUF3QkUsS0FBeEIsQ0FBOEIsQ0FBQ3pFLENBQS9CLENBQVA7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPdUUsS0FBSyxDQUFDdEgsUUFBTixFQUFQO0FBQ0Q7QUFDRjs7QUFDRCxXQUFTeUgsWUFBVCxDQUFzQkMsTUFBdEIsRUFBOEI7QUFDNUIsUUFBSTFDLFdBQVcsQ0FBQzBDLE1BQUQsQ0FBWCxJQUF1QkEsTUFBTSxLQUFLLElBQWxDLElBQTBDQSxNQUFNLEtBQUssRUFBekQsRUFBNkQ7QUFDM0QsYUFBT3RHLFNBQVA7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPdUcsUUFBUSxDQUFDRCxNQUFELEVBQVMsRUFBVCxDQUFmO0FBQ0Q7QUFDRjs7QUFDRCxXQUFTRSxXQUFULENBQXFCQyxRQUFyQixFQUErQjtBQUM3QjtBQUNBLFFBQUk3QyxXQUFXLENBQUM2QyxRQUFELENBQVgsSUFBeUJBLFFBQVEsS0FBSyxJQUF0QyxJQUE4Q0EsUUFBUSxLQUFLLEVBQS9ELEVBQW1FO0FBQ2pFLGFBQU96RyxTQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSTBHLENBQUMsR0FBR0MsVUFBVSxDQUFDLE9BQU9GLFFBQVIsQ0FBVixHQUE4QixJQUF0QztBQUNBLGFBQU9WLElBQUksQ0FBQ0MsS0FBTCxDQUFXVSxDQUFYLENBQVA7QUFDRDtBQUNGOztBQUNELFdBQVNFLE9BQVQsQ0FBaUJDLE1BQWpCLEVBQXlCQyxNQUF6QixFQUFpQ0MsVUFBakMsRUFBNkM7QUFDM0MsUUFBSUEsVUFBVSxLQUFLLEtBQUssQ0FBeEIsRUFBMkI7QUFDekJBLE1BQUFBLFVBQVUsR0FBRyxLQUFiO0FBQ0Q7O0FBRUQsUUFBSUMsTUFBTSxHQUFHakIsSUFBSSxDQUFDa0IsR0FBTCxDQUFTLEVBQVQsRUFBYUgsTUFBYixDQUFiO0FBQUEsUUFDSUksT0FBTyxHQUFHSCxVQUFVLEdBQUdoQixJQUFJLENBQUNvQixLQUFSLEdBQWdCcEIsSUFBSSxDQUFDcUIsS0FEN0M7QUFFQSxXQUFPRixPQUFPLENBQUNMLE1BQU0sR0FBR0csTUFBVixDQUFQLEdBQTJCQSxNQUFsQztBQUNELEdBcGdCNkIsQ0FvZ0I1Qjs7O0FBRUYsV0FBU0ssVUFBVCxDQUFvQnRGLElBQXBCLEVBQTBCO0FBQ3hCLFdBQU9BLElBQUksR0FBRyxDQUFQLEtBQWEsQ0FBYixLQUFtQkEsSUFBSSxHQUFHLEdBQVAsS0FBZSxDQUFmLElBQW9CQSxJQUFJLEdBQUcsR0FBUCxLQUFlLENBQXRELENBQVA7QUFDRDs7QUFDRCxXQUFTdUYsVUFBVCxDQUFvQnZGLElBQXBCLEVBQTBCO0FBQ3hCLFdBQU9zRixVQUFVLENBQUN0RixJQUFELENBQVYsR0FBbUIsR0FBbkIsR0FBeUIsR0FBaEM7QUFDRDs7QUFDRCxXQUFTd0YsV0FBVCxDQUFxQnhGLElBQXJCLEVBQTJCQyxLQUEzQixFQUFrQztBQUNoQyxRQUFJd0YsUUFBUSxHQUFHM0IsUUFBUSxDQUFDN0QsS0FBSyxHQUFHLENBQVQsRUFBWSxFQUFaLENBQVIsR0FBMEIsQ0FBekM7QUFBQSxRQUNJeUYsT0FBTyxHQUFHMUYsSUFBSSxHQUFHLENBQUNDLEtBQUssR0FBR3dGLFFBQVQsSUFBcUIsRUFEMUM7O0FBR0EsUUFBSUEsUUFBUSxLQUFLLENBQWpCLEVBQW9CO0FBQ2xCLGFBQU9ILFVBQVUsQ0FBQ0ksT0FBRCxDQUFWLEdBQXNCLEVBQXRCLEdBQTJCLEVBQWxDO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBTyxDQUFDLEVBQUQsRUFBSyxJQUFMLEVBQVcsRUFBWCxFQUFlLEVBQWYsRUFBbUIsRUFBbkIsRUFBdUIsRUFBdkIsRUFBMkIsRUFBM0IsRUFBK0IsRUFBL0IsRUFBbUMsRUFBbkMsRUFBdUMsRUFBdkMsRUFBMkMsRUFBM0MsRUFBK0MsRUFBL0MsRUFBbURELFFBQVEsR0FBRyxDQUE5RCxDQUFQO0FBQ0Q7QUFDRixHQXJoQjZCLENBcWhCNUI7OztBQUVGLFdBQVNFLFlBQVQsQ0FBc0JyQyxHQUF0QixFQUEyQjtBQUN6QixRQUFJc0MsQ0FBQyxHQUFHaEosSUFBSSxDQUFDaUosR0FBTCxDQUFTdkMsR0FBRyxDQUFDdEQsSUFBYixFQUFtQnNELEdBQUcsQ0FBQ3JELEtBQUosR0FBWSxDQUEvQixFQUFrQ3FELEdBQUcsQ0FBQ3BELEdBQXRDLEVBQTJDb0QsR0FBRyxDQUFDOUMsSUFBL0MsRUFBcUQ4QyxHQUFHLENBQUM3QyxNQUF6RCxFQUFpRTZDLEdBQUcsQ0FBQzNDLE1BQXJFLEVBQTZFMkMsR0FBRyxDQUFDd0MsV0FBakYsQ0FBUixDQUR5QixDQUM4RTs7QUFFdkcsUUFBSXhDLEdBQUcsQ0FBQ3RELElBQUosR0FBVyxHQUFYLElBQWtCc0QsR0FBRyxDQUFDdEQsSUFBSixJQUFZLENBQWxDLEVBQXFDO0FBQ25DNEYsTUFBQUEsQ0FBQyxHQUFHLElBQUloSixJQUFKLENBQVNnSixDQUFULENBQUo7QUFDQUEsTUFBQUEsQ0FBQyxDQUFDRyxjQUFGLENBQWlCSCxDQUFDLENBQUNJLGNBQUYsS0FBcUIsSUFBdEM7QUFDRDs7QUFFRCxXQUFPLENBQUNKLENBQVI7QUFDRDs7QUFDRCxXQUFTSyxlQUFULENBQXlCQyxRQUF6QixFQUFtQztBQUNqQyxRQUFJQyxFQUFFLEdBQUcsQ0FBQ0QsUUFBUSxHQUFHbEMsSUFBSSxDQUFDQyxLQUFMLENBQVdpQyxRQUFRLEdBQUcsQ0FBdEIsQ0FBWCxHQUFzQ2xDLElBQUksQ0FBQ0MsS0FBTCxDQUFXaUMsUUFBUSxHQUFHLEdBQXRCLENBQXRDLEdBQW1FbEMsSUFBSSxDQUFDQyxLQUFMLENBQVdpQyxRQUFRLEdBQUcsR0FBdEIsQ0FBcEUsSUFBa0csQ0FBM0c7QUFBQSxRQUNJRSxJQUFJLEdBQUdGLFFBQVEsR0FBRyxDQUR0QjtBQUFBLFFBRUlHLEVBQUUsR0FBRyxDQUFDRCxJQUFJLEdBQUdwQyxJQUFJLENBQUNDLEtBQUwsQ0FBV21DLElBQUksR0FBRyxDQUFsQixDQUFQLEdBQThCcEMsSUFBSSxDQUFDQyxLQUFMLENBQVdtQyxJQUFJLEdBQUcsR0FBbEIsQ0FBOUIsR0FBdURwQyxJQUFJLENBQUNDLEtBQUwsQ0FBV21DLElBQUksR0FBRyxHQUFsQixDQUF4RCxJQUFrRixDQUYzRjtBQUdBLFdBQU9ELEVBQUUsS0FBSyxDQUFQLElBQVlFLEVBQUUsS0FBSyxDQUFuQixHQUF1QixFQUF2QixHQUE0QixFQUFuQztBQUNEOztBQUNELFdBQVNDLGNBQVQsQ0FBd0J0RyxJQUF4QixFQUE4QjtBQUM1QixRQUFJQSxJQUFJLEdBQUcsRUFBWCxFQUFlO0FBQ2IsYUFBT0EsSUFBUDtBQUNELEtBRkQsTUFFTyxPQUFPQSxJQUFJLEdBQUcsRUFBUCxHQUFZLE9BQU9BLElBQW5CLEdBQTBCLE9BQU9BLElBQXhDO0FBQ1IsR0EzaUI2QixDQTJpQjVCOzs7QUFFRixXQUFTdUcsYUFBVCxDQUF1QkMsRUFBdkIsRUFBMkJDLFlBQTNCLEVBQXlDQyxNQUF6QyxFQUFpREMsUUFBakQsRUFBMkQ7QUFDekQsUUFBSUEsUUFBUSxLQUFLLEtBQUssQ0FBdEIsRUFBeUI7QUFDdkJBLE1BQUFBLFFBQVEsR0FBRyxJQUFYO0FBQ0Q7O0FBRUQsUUFBSUMsSUFBSSxHQUFHLElBQUloSyxJQUFKLENBQVM0SixFQUFULENBQVg7QUFBQSxRQUNJSyxRQUFRLEdBQUc7QUFDYjdGLE1BQUFBLE1BQU0sRUFBRSxLQURLO0FBRWJoQixNQUFBQSxJQUFJLEVBQUUsU0FGTztBQUdiQyxNQUFBQSxLQUFLLEVBQUUsU0FITTtBQUliQyxNQUFBQSxHQUFHLEVBQUUsU0FKUTtBQUtiTSxNQUFBQSxJQUFJLEVBQUUsU0FMTztBQU1iQyxNQUFBQSxNQUFNLEVBQUU7QUFOSyxLQURmOztBQVVBLFFBQUlrRyxRQUFKLEVBQWM7QUFDWkUsTUFBQUEsUUFBUSxDQUFDRixRQUFULEdBQW9CQSxRQUFwQjtBQUNEOztBQUVELFFBQUlHLFFBQVEsR0FBRzNMLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYztBQUMzQmxHLE1BQUFBLFlBQVksRUFBRTRGO0FBRGEsS0FBZCxFQUVaSSxRQUZZLENBQWY7QUFBQSxRQUdJRyxJQUFJLEdBQUc5RSxPQUFPLEVBSGxCOztBQUtBLFFBQUk4RSxJQUFJLElBQUkzRSxnQkFBZ0IsRUFBNUIsRUFBZ0M7QUFDOUIsVUFBSTRFLE1BQU0sR0FBRyxJQUFJOUUsSUFBSSxDQUFDQyxjQUFULENBQXdCc0UsTUFBeEIsRUFBZ0NJLFFBQWhDLEVBQTBDeEUsYUFBMUMsQ0FBd0RzRSxJQUF4RCxFQUE4RE0sSUFBOUQsQ0FBbUUsVUFBVUMsQ0FBVixFQUFhO0FBQzNGLGVBQU9BLENBQUMsQ0FBQ0MsSUFBRixDQUFPQyxXQUFQLE9BQXlCLGNBQWhDO0FBQ0QsT0FGWSxDQUFiO0FBR0EsYUFBT0osTUFBTSxHQUFHQSxNQUFNLENBQUMxSSxLQUFWLEdBQWtCLElBQS9CO0FBQ0QsS0FMRCxNQUtPLElBQUl5SSxJQUFKLEVBQVU7QUFDZjtBQUNBLFVBQUlNLE9BQU8sR0FBRyxJQUFJbkYsSUFBSSxDQUFDQyxjQUFULENBQXdCc0UsTUFBeEIsRUFBZ0NHLFFBQWhDLEVBQTBDVSxNQUExQyxDQUFpRFgsSUFBakQsQ0FBZDtBQUFBLFVBQ0lZLFFBQVEsR0FBRyxJQUFJckYsSUFBSSxDQUFDQyxjQUFULENBQXdCc0UsTUFBeEIsRUFBZ0NJLFFBQWhDLEVBQTBDUyxNQUExQyxDQUFpRFgsSUFBakQsQ0FEZjtBQUFBLFVBRUlhLE1BQU0sR0FBR0QsUUFBUSxDQUFDRSxTQUFULENBQW1CSixPQUFPLENBQUN4TSxNQUEzQixDQUZiO0FBQUEsVUFHSTZNLE9BQU8sR0FBR0YsTUFBTSxDQUFDRyxPQUFQLENBQWUsY0FBZixFQUErQixFQUEvQixDQUhkO0FBSUEsYUFBT0QsT0FBUDtBQUNELEtBUE0sTUFPQTtBQUNMLGFBQU8sSUFBUDtBQUNEO0FBQ0YsR0FwbEI2QixDQW9sQjVCOzs7QUFFRixXQUFTRSxZQUFULENBQXNCQyxVQUF0QixFQUFrQ0MsWUFBbEMsRUFBZ0Q7QUFDOUMsUUFBSUMsT0FBTyxHQUFHeEQsUUFBUSxDQUFDc0QsVUFBRCxFQUFhLEVBQWIsQ0FBdEIsQ0FEOEMsQ0FDTjs7QUFFeEMsUUFBSUcsTUFBTSxDQUFDQyxLQUFQLENBQWFGLE9BQWIsQ0FBSixFQUEyQjtBQUN6QkEsTUFBQUEsT0FBTyxHQUFHLENBQVY7QUFDRDs7QUFFRCxRQUFJRyxNQUFNLEdBQUczRCxRQUFRLENBQUN1RCxZQUFELEVBQWUsRUFBZixDQUFSLElBQThCLENBQTNDO0FBQUEsUUFDSUssWUFBWSxHQUFHSixPQUFPLEdBQUcsQ0FBVixJQUFlN00sTUFBTSxDQUFDa04sRUFBUCxDQUFVTCxPQUFWLEVBQW1CLENBQUMsQ0FBcEIsQ0FBZixHQUF3QyxDQUFDRyxNQUF6QyxHQUFrREEsTUFEckU7QUFFQSxXQUFPSCxPQUFPLEdBQUcsRUFBVixHQUFlSSxZQUF0QjtBQUNELEdBaG1CNkIsQ0FnbUI1Qjs7O0FBRUYsV0FBU0UsUUFBVCxDQUFrQi9KLEtBQWxCLEVBQXlCO0FBQ3ZCLFFBQUlnSyxZQUFZLEdBQUdOLE1BQU0sQ0FBQzFKLEtBQUQsQ0FBekI7QUFDQSxRQUFJLE9BQU9BLEtBQVAsS0FBaUIsU0FBakIsSUFBOEJBLEtBQUssS0FBSyxFQUF4QyxJQUE4QzBKLE1BQU0sQ0FBQ0MsS0FBUCxDQUFhSyxZQUFiLENBQWxELEVBQThFLE1BQU0sSUFBSS9JLG9CQUFKLENBQXlCLHdCQUF3QmpCLEtBQWpELENBQU47QUFDOUUsV0FBT2dLLFlBQVA7QUFDRDs7QUFDRCxXQUFTQyxlQUFULENBQXlCbEYsR0FBekIsRUFBOEJtRixVQUE5QixFQUEwQ0MsV0FBMUMsRUFBdUQ7QUFDckQsUUFBSUMsVUFBVSxHQUFHLEVBQWpCOztBQUVBLFNBQUssSUFBSUMsQ0FBVCxJQUFjdEYsR0FBZCxFQUFtQjtBQUNqQixVQUFJRyxjQUFjLENBQUNILEdBQUQsRUFBTXNGLENBQU4sQ0FBbEIsRUFBNEI7QUFDMUIsWUFBSUYsV0FBVyxDQUFDN0ssT0FBWixDQUFvQitLLENBQXBCLEtBQTBCLENBQTlCLEVBQWlDO0FBQ2pDLFlBQUlDLENBQUMsR0FBR3ZGLEdBQUcsQ0FBQ3NGLENBQUQsQ0FBWDtBQUNBLFlBQUlDLENBQUMsS0FBSzVLLFNBQU4sSUFBbUI0SyxDQUFDLEtBQUssSUFBN0IsRUFBbUM7QUFDbkNGLFFBQUFBLFVBQVUsQ0FBQ0YsVUFBVSxDQUFDRyxDQUFELENBQVgsQ0FBVixHQUE0Qk4sUUFBUSxDQUFDTyxDQUFELENBQXBDO0FBQ0Q7QUFDRjs7QUFFRCxXQUFPRixVQUFQO0FBQ0Q7O0FBQ0QsV0FBU0csWUFBVCxDQUFzQkMsTUFBdEIsRUFBOEJ4QixNQUE5QixFQUFzQztBQUNwQyxRQUFJeUIsS0FBSyxHQUFHaEYsSUFBSSxDQUFDb0IsS0FBTCxDQUFXMkQsTUFBTSxHQUFHLEVBQXBCLENBQVo7QUFBQSxRQUNJRSxPQUFPLEdBQUdqRixJQUFJLENBQUNrRixHQUFMLENBQVNILE1BQU0sR0FBRyxFQUFsQixDQURkO0FBQUEsUUFFSUksSUFBSSxHQUFHSCxLQUFLLElBQUksQ0FBVCxJQUFjLENBQUM3TixNQUFNLENBQUNrTixFQUFQLENBQVVXLEtBQVYsRUFBaUIsQ0FBQyxDQUFsQixDQUFmLEdBQXNDLEdBQXRDLEdBQTRDLEdBRnZEO0FBQUEsUUFHSUksSUFBSSxHQUFHLEtBQUtELElBQUwsR0FBWW5GLElBQUksQ0FBQ2tGLEdBQUwsQ0FBU0YsS0FBVCxDQUh2Qjs7QUFLQSxZQUFRekIsTUFBUjtBQUNFLFdBQUssT0FBTDtBQUNFLGVBQU8sS0FBSzRCLElBQUwsR0FBWWpGLFFBQVEsQ0FBQ0YsSUFBSSxDQUFDa0YsR0FBTCxDQUFTRixLQUFULENBQUQsRUFBa0IsQ0FBbEIsQ0FBcEIsR0FBMkMsR0FBM0MsR0FBaUQ5RSxRQUFRLENBQUMrRSxPQUFELEVBQVUsQ0FBVixDQUFoRTs7QUFFRixXQUFLLFFBQUw7QUFDRSxlQUFPQSxPQUFPLEdBQUcsQ0FBVixHQUFjRyxJQUFJLEdBQUcsR0FBUCxHQUFhSCxPQUEzQixHQUFxQ0csSUFBNUM7O0FBRUYsV0FBSyxRQUFMO0FBQ0UsZUFBTyxLQUFLRCxJQUFMLEdBQVlqRixRQUFRLENBQUNGLElBQUksQ0FBQ2tGLEdBQUwsQ0FBU0YsS0FBVCxDQUFELEVBQWtCLENBQWxCLENBQXBCLEdBQTJDOUUsUUFBUSxDQUFDK0UsT0FBRCxFQUFVLENBQVYsQ0FBMUQ7O0FBRUY7QUFDRSxjQUFNLElBQUlJLFVBQUosQ0FBZSxrQkFBa0I5QixNQUFsQixHQUEyQixzQ0FBMUMsQ0FBTjtBQVhKO0FBYUQ7O0FBQ0QsV0FBUytCLFVBQVQsQ0FBb0JoRyxHQUFwQixFQUF5QjtBQUN2QixXQUFPRCxJQUFJLENBQUNDLEdBQUQsRUFBTSxDQUFDLE1BQUQsRUFBUyxRQUFULEVBQW1CLFFBQW5CLEVBQTZCLGFBQTdCLENBQU4sQ0FBWDtBQUNEOztBQUNELE1BQUlpRyxTQUFTLEdBQUcsb0VBQWhCOztBQUVBLFdBQVNDLFNBQVQsQ0FBbUJsRyxHQUFuQixFQUF3QjtBQUN0QixXQUFPbUcsSUFBSSxDQUFDRCxTQUFMLENBQWVsRyxHQUFmLEVBQW9CbkksTUFBTSxDQUFDb0ksSUFBUCxDQUFZRCxHQUFaLEVBQWlCb0csSUFBakIsRUFBcEIsQ0FBUDtBQUNEO0FBQ0Q7Ozs7O0FBS0EsTUFBSUMsVUFBVSxHQUFHLENBQUMsU0FBRCxFQUFZLFVBQVosRUFBd0IsT0FBeEIsRUFBaUMsT0FBakMsRUFBMEMsS0FBMUMsRUFBaUQsTUFBakQsRUFBeUQsTUFBekQsRUFBaUUsUUFBakUsRUFBMkUsV0FBM0UsRUFBd0YsU0FBeEYsRUFBbUcsVUFBbkcsRUFBK0csVUFBL0csQ0FBakI7QUFDQSxNQUFJQyxXQUFXLEdBQUcsQ0FBQyxLQUFELEVBQVEsS0FBUixFQUFlLEtBQWYsRUFBc0IsS0FBdEIsRUFBNkIsS0FBN0IsRUFBb0MsS0FBcEMsRUFBMkMsS0FBM0MsRUFBa0QsS0FBbEQsRUFBeUQsS0FBekQsRUFBZ0UsS0FBaEUsRUFBdUUsS0FBdkUsRUFBOEUsS0FBOUUsQ0FBbEI7QUFDQSxNQUFJQyxZQUFZLEdBQUcsQ0FBQyxHQUFELEVBQU0sR0FBTixFQUFXLEdBQVgsRUFBZ0IsR0FBaEIsRUFBcUIsR0FBckIsRUFBMEIsR0FBMUIsRUFBK0IsR0FBL0IsRUFBb0MsR0FBcEMsRUFBeUMsR0FBekMsRUFBOEMsR0FBOUMsRUFBbUQsR0FBbkQsRUFBd0QsR0FBeEQsQ0FBbkI7O0FBQ0EsV0FBU0MsTUFBVCxDQUFnQmhQLE1BQWhCLEVBQXdCO0FBQ3RCLFlBQVFBLE1BQVI7QUFDRSxXQUFLLFFBQUw7QUFDRSxlQUFPK08sWUFBUDs7QUFFRixXQUFLLE9BQUw7QUFDRSxlQUFPRCxXQUFQOztBQUVGLFdBQUssTUFBTDtBQUNFLGVBQU9ELFVBQVA7O0FBRUYsV0FBSyxTQUFMO0FBQ0UsZUFBTyxDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVcsR0FBWCxFQUFnQixHQUFoQixFQUFxQixHQUFyQixFQUEwQixHQUExQixFQUErQixHQUEvQixFQUFvQyxHQUFwQyxFQUF5QyxHQUF6QyxFQUE4QyxJQUE5QyxFQUFvRCxJQUFwRCxFQUEwRCxJQUExRCxDQUFQOztBQUVGLFdBQUssU0FBTDtBQUNFLGVBQU8sQ0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLElBQWIsRUFBbUIsSUFBbkIsRUFBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUMsSUFBckMsRUFBMkMsSUFBM0MsRUFBaUQsSUFBakQsRUFBdUQsSUFBdkQsRUFBNkQsSUFBN0QsRUFBbUUsSUFBbkUsQ0FBUDs7QUFFRjtBQUNFLGVBQU8sSUFBUDtBQWpCSjtBQW1CRDs7QUFDRCxNQUFJSSxZQUFZLEdBQUcsQ0FBQyxRQUFELEVBQVcsU0FBWCxFQUFzQixXQUF0QixFQUFtQyxVQUFuQyxFQUErQyxRQUEvQyxFQUF5RCxVQUF6RCxFQUFxRSxRQUFyRSxDQUFuQjtBQUNBLE1BQUlDLGFBQWEsR0FBRyxDQUFDLEtBQUQsRUFBUSxLQUFSLEVBQWUsS0FBZixFQUFzQixLQUF0QixFQUE2QixLQUE3QixFQUFvQyxLQUFwQyxFQUEyQyxLQUEzQyxDQUFwQjtBQUNBLE1BQUlDLGNBQWMsR0FBRyxDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVcsR0FBWCxFQUFnQixHQUFoQixFQUFxQixHQUFyQixFQUEwQixHQUExQixFQUErQixHQUEvQixDQUFyQjs7QUFDQSxXQUFTQyxRQUFULENBQWtCcFAsTUFBbEIsRUFBMEI7QUFDeEIsWUFBUUEsTUFBUjtBQUNFLFdBQUssUUFBTDtBQUNFLGVBQU9tUCxjQUFQOztBQUVGLFdBQUssT0FBTDtBQUNFLGVBQU9ELGFBQVA7O0FBRUYsV0FBSyxNQUFMO0FBQ0UsZUFBT0QsWUFBUDs7QUFFRixXQUFLLFNBQUw7QUFDRSxlQUFPLENBQUMsR0FBRCxFQUFNLEdBQU4sRUFBVyxHQUFYLEVBQWdCLEdBQWhCLEVBQXFCLEdBQXJCLEVBQTBCLEdBQTFCLEVBQStCLEdBQS9CLENBQVA7O0FBRUY7QUFDRSxlQUFPLElBQVA7QUFkSjtBQWdCRDs7QUFDRCxNQUFJSSxTQUFTLEdBQUcsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQUFoQjtBQUNBLE1BQUlDLFFBQVEsR0FBRyxDQUFDLGVBQUQsRUFBa0IsYUFBbEIsQ0FBZjtBQUNBLE1BQUlDLFNBQVMsR0FBRyxDQUFDLElBQUQsRUFBTyxJQUFQLENBQWhCO0FBQ0EsTUFBSUMsVUFBVSxHQUFHLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FBakI7O0FBQ0EsV0FBU0MsSUFBVCxDQUFjelAsTUFBZCxFQUFzQjtBQUNwQixZQUFRQSxNQUFSO0FBQ0UsV0FBSyxRQUFMO0FBQ0UsZUFBT3dQLFVBQVA7O0FBRUYsV0FBSyxPQUFMO0FBQ0UsZUFBT0QsU0FBUDs7QUFFRixXQUFLLE1BQUw7QUFDRSxlQUFPRCxRQUFQOztBQUVGO0FBQ0UsZUFBTyxJQUFQO0FBWEo7QUFhRDs7QUFDRCxXQUFTSSxtQkFBVCxDQUE2QkMsRUFBN0IsRUFBaUM7QUFDL0IsV0FBT04sU0FBUyxDQUFDTSxFQUFFLENBQUNqSyxJQUFILEdBQVUsRUFBVixHQUFlLENBQWYsR0FBbUIsQ0FBcEIsQ0FBaEI7QUFDRDs7QUFDRCxXQUFTa0ssa0JBQVQsQ0FBNEJELEVBQTVCLEVBQWdDM1AsTUFBaEMsRUFBd0M7QUFDdEMsV0FBT29QLFFBQVEsQ0FBQ3BQLE1BQUQsQ0FBUixDQUFpQjJQLEVBQUUsQ0FBQ25LLE9BQUgsR0FBYSxDQUE5QixDQUFQO0FBQ0Q7O0FBQ0QsV0FBU3FLLGdCQUFULENBQTBCRixFQUExQixFQUE4QjNQLE1BQTlCLEVBQXNDO0FBQ3BDLFdBQU9nUCxNQUFNLENBQUNoUCxNQUFELENBQU4sQ0FBZTJQLEVBQUUsQ0FBQ3hLLEtBQUgsR0FBVyxDQUExQixDQUFQO0FBQ0Q7O0FBQ0QsV0FBUzJLLGNBQVQsQ0FBd0JILEVBQXhCLEVBQTRCM1AsTUFBNUIsRUFBb0M7QUFDbEMsV0FBT3lQLElBQUksQ0FBQ3pQLE1BQUQsQ0FBSixDQUFhMlAsRUFBRSxDQUFDekssSUFBSCxHQUFVLENBQVYsR0FBYyxDQUFkLEdBQWtCLENBQS9CLENBQVA7QUFDRDs7QUFDRCxXQUFTNkssa0JBQVQsQ0FBNEJ0TCxJQUE1QixFQUFrQ3VMLEtBQWxDLEVBQXlDQyxPQUF6QyxFQUFrREMsTUFBbEQsRUFBMEQ7QUFDeEQsUUFBSUQsT0FBTyxLQUFLLEtBQUssQ0FBckIsRUFBd0I7QUFDdEJBLE1BQUFBLE9BQU8sR0FBRyxRQUFWO0FBQ0Q7O0FBRUQsUUFBSUMsTUFBTSxLQUFLLEtBQUssQ0FBcEIsRUFBdUI7QUFDckJBLE1BQUFBLE1BQU0sR0FBRyxLQUFUO0FBQ0Q7O0FBRUQsUUFBSUMsS0FBSyxHQUFHO0FBQ1ZDLE1BQUFBLEtBQUssRUFBRSxDQUFDLE1BQUQsRUFBUyxLQUFULENBREc7QUFFVkMsTUFBQUEsUUFBUSxFQUFFLENBQUMsU0FBRCxFQUFZLE1BQVosQ0FGQTtBQUdWckIsTUFBQUEsTUFBTSxFQUFFLENBQUMsT0FBRCxFQUFVLEtBQVYsQ0FIRTtBQUlWc0IsTUFBQUEsS0FBSyxFQUFFLENBQUMsTUFBRCxFQUFTLEtBQVQsQ0FKRztBQUtWQyxNQUFBQSxJQUFJLEVBQUUsQ0FBQyxLQUFELEVBQVEsS0FBUixFQUFlLE1BQWYsQ0FMSTtBQU1WckMsTUFBQUEsS0FBSyxFQUFFLENBQUMsTUFBRCxFQUFTLEtBQVQsQ0FORztBQU9WQyxNQUFBQSxPQUFPLEVBQUUsQ0FBQyxRQUFELEVBQVcsTUFBWCxDQVBDO0FBUVZxQyxNQUFBQSxPQUFPLEVBQUUsQ0FBQyxRQUFELEVBQVcsTUFBWDtBQVJDLEtBQVo7QUFVQSxRQUFJQyxRQUFRLEdBQUcsQ0FBQyxPQUFELEVBQVUsU0FBVixFQUFxQixTQUFyQixFQUFnQzFOLE9BQWhDLENBQXdDMEIsSUFBeEMsTUFBa0QsQ0FBQyxDQUFsRTs7QUFFQSxRQUFJd0wsT0FBTyxLQUFLLE1BQVosSUFBc0JRLFFBQTFCLEVBQW9DO0FBQ2xDLFVBQUlDLEtBQUssR0FBR2pNLElBQUksS0FBSyxNQUFyQjs7QUFFQSxjQUFRdUwsS0FBUjtBQUNFLGFBQUssQ0FBTDtBQUNFLGlCQUFPVSxLQUFLLEdBQUcsVUFBSCxHQUFnQixVQUFVUCxLQUFLLENBQUMxTCxJQUFELENBQUwsQ0FBWSxDQUFaLENBQXRDOztBQUVGLGFBQUssQ0FBQyxDQUFOO0FBQ0UsaUJBQU9pTSxLQUFLLEdBQUcsV0FBSCxHQUFpQixVQUFVUCxLQUFLLENBQUMxTCxJQUFELENBQUwsQ0FBWSxDQUFaLENBQXZDOztBQUVGLGFBQUssQ0FBTDtBQUNFLGlCQUFPaU0sS0FBSyxHQUFHLE9BQUgsR0FBYSxVQUFVUCxLQUFLLENBQUMxTCxJQUFELENBQUwsQ0FBWSxDQUFaLENBQW5DOztBQUVGLGdCQVZGLENBVVc7O0FBVlg7QUFhRDs7QUFFRCxRQUFJa00sUUFBUSxHQUFHdFEsTUFBTSxDQUFDa04sRUFBUCxDQUFVeUMsS0FBVixFQUFpQixDQUFDLENBQWxCLEtBQXdCQSxLQUFLLEdBQUcsQ0FBL0M7QUFBQSxRQUNJWSxRQUFRLEdBQUcxSCxJQUFJLENBQUNrRixHQUFMLENBQVM0QixLQUFULENBRGY7QUFBQSxRQUVJYSxRQUFRLEdBQUdELFFBQVEsS0FBSyxDQUY1QjtBQUFBLFFBR0lFLFFBQVEsR0FBR1gsS0FBSyxDQUFDMUwsSUFBRCxDQUhwQjtBQUFBLFFBSUlzTSxPQUFPLEdBQUdiLE1BQU0sR0FBR1csUUFBUSxHQUFHQyxRQUFRLENBQUMsQ0FBRCxDQUFYLEdBQWlCQSxRQUFRLENBQUMsQ0FBRCxDQUFSLElBQWVBLFFBQVEsQ0FBQyxDQUFELENBQW5ELEdBQXlERCxRQUFRLEdBQUdWLEtBQUssQ0FBQzFMLElBQUQsQ0FBTCxDQUFZLENBQVosQ0FBSCxHQUFvQkEsSUFKekc7QUFLQSxXQUFPa00sUUFBUSxHQUFHQyxRQUFRLEdBQUcsR0FBWCxHQUFpQkcsT0FBakIsR0FBMkIsTUFBOUIsR0FBdUMsUUFBUUgsUUFBUixHQUFtQixHQUFuQixHQUF5QkcsT0FBL0U7QUFDRDs7QUFDRCxXQUFTQyxZQUFULENBQXNCQyxXQUF0QixFQUFtQztBQUNqQztBQUNBO0FBQ0EsUUFBSUMsUUFBUSxHQUFHM0ksSUFBSSxDQUFDMEksV0FBRCxFQUFjLENBQUMsU0FBRCxFQUFZLEtBQVosRUFBbUIsTUFBbkIsRUFBMkIsT0FBM0IsRUFBb0MsS0FBcEMsRUFBMkMsTUFBM0MsRUFBbUQsUUFBbkQsRUFBNkQsUUFBN0QsRUFBdUUsY0FBdkUsRUFBdUYsUUFBdkYsQ0FBZCxDQUFuQjtBQUFBLFFBQ0kxUSxHQUFHLEdBQUdtTyxTQUFTLENBQUN3QyxRQUFELENBRG5CO0FBQUEsUUFFSUMsWUFBWSxHQUFHLDRCQUZuQjs7QUFJQSxZQUFRNVEsR0FBUjtBQUNFLFdBQUttTyxTQUFTLENBQUN6SixVQUFELENBQWQ7QUFDRSxlQUFPLFVBQVA7O0FBRUYsV0FBS3lKLFNBQVMsQ0FBQ3JKLFFBQUQsQ0FBZDtBQUNFLGVBQU8sYUFBUDs7QUFFRixXQUFLcUosU0FBUyxDQUFDcEosU0FBRCxDQUFkO0FBQ0UsZUFBTyxjQUFQOztBQUVGLFdBQUtvSixTQUFTLENBQUNuSixTQUFELENBQWQ7QUFDRSxlQUFPLG9CQUFQOztBQUVGLFdBQUttSixTQUFTLENBQUNqSixXQUFELENBQWQ7QUFDRSxlQUFPLFFBQVA7O0FBRUYsV0FBS2lKLFNBQVMsQ0FBQzlJLGlCQUFELENBQWQ7QUFDRSxlQUFPLFdBQVA7O0FBRUYsV0FBSzhJLFNBQVMsQ0FBQzVJLHNCQUFELENBQWQ7QUFDRSxlQUFPLFFBQVA7O0FBRUYsV0FBSzRJLFNBQVMsQ0FBQzFJLHFCQUFELENBQWQ7QUFDRSxlQUFPLFFBQVA7O0FBRUYsV0FBSzBJLFNBQVMsQ0FBQ3pJLGNBQUQsQ0FBZDtBQUNFLGVBQU8sT0FBUDs7QUFFRixXQUFLeUksU0FBUyxDQUFDdkksb0JBQUQsQ0FBZDtBQUNFLGVBQU8sVUFBUDs7QUFFRixXQUFLdUksU0FBUyxDQUFDdEkseUJBQUQsQ0FBZDtBQUNFLGVBQU8sT0FBUDs7QUFFRixXQUFLc0ksU0FBUyxDQUFDckksd0JBQUQsQ0FBZDtBQUNFLGVBQU8sT0FBUDs7QUFFRixXQUFLcUksU0FBUyxDQUFDcEksY0FBRCxDQUFkO0FBQ0UsZUFBTyxrQkFBUDs7QUFFRixXQUFLb0ksU0FBUyxDQUFDbEksWUFBRCxDQUFkO0FBQ0UsZUFBTyxxQkFBUDs7QUFFRixXQUFLa0ksU0FBUyxDQUFDL0gsYUFBRCxDQUFkO0FBQ0UsZUFBTyxzQkFBUDs7QUFFRixXQUFLK0gsU0FBUyxDQUFDN0gsYUFBRCxDQUFkO0FBQ0UsZUFBT3NLLFlBQVA7O0FBRUYsV0FBS3pDLFNBQVMsQ0FBQ25JLDJCQUFELENBQWQ7QUFDRSxlQUFPLHFCQUFQOztBQUVGLFdBQUttSSxTQUFTLENBQUNqSSx5QkFBRCxDQUFkO0FBQ0UsZUFBTyx3QkFBUDs7QUFFRixXQUFLaUksU0FBUyxDQUFDaEkseUJBQUQsQ0FBZDtBQUNFLGVBQU8seUJBQVA7O0FBRUYsV0FBS2dJLFNBQVMsQ0FBQzlILDBCQUFELENBQWQ7QUFDRSxlQUFPLHlCQUFQOztBQUVGLFdBQUs4SCxTQUFTLENBQUM1SCwwQkFBRCxDQUFkO0FBQ0UsZUFBTywrQkFBUDs7QUFFRjtBQUNFLGVBQU9xSyxZQUFQO0FBakVKO0FBbUVEOztBQUVELFdBQVNDLGVBQVQsQ0FBeUJDLE1BQXpCLEVBQWlDQyxhQUFqQyxFQUFnRDtBQUM5QyxRQUFJdk0sQ0FBQyxHQUFHLEVBQVI7O0FBRUEsU0FBSyxJQUFJd00sU0FBUyxHQUFHRixNQUFoQixFQUF3QkcsUUFBUSxHQUFHM0osS0FBSyxDQUFDQyxPQUFOLENBQWN5SixTQUFkLENBQW5DLEVBQTZERSxFQUFFLEdBQUcsQ0FBbEUsRUFBcUVGLFNBQVMsR0FBR0MsUUFBUSxHQUFHRCxTQUFILEdBQWVBLFNBQVMsQ0FBQ0csTUFBTSxDQUFDQyxRQUFSLENBQVQsRUFBN0csSUFBNkk7QUFDM0ksVUFBSUMsSUFBSjs7QUFFQSxVQUFJSixRQUFKLEVBQWM7QUFDWixZQUFJQyxFQUFFLElBQUlGLFNBQVMsQ0FBQ3ZSLE1BQXBCLEVBQTRCO0FBQzVCNFIsUUFBQUEsSUFBSSxHQUFHTCxTQUFTLENBQUNFLEVBQUUsRUFBSCxDQUFoQjtBQUNELE9BSEQsTUFHTztBQUNMQSxRQUFBQSxFQUFFLEdBQUdGLFNBQVMsQ0FBQ2xKLElBQVYsRUFBTDtBQUNBLFlBQUlvSixFQUFFLENBQUNJLElBQVAsRUFBYTtBQUNiRCxRQUFBQSxJQUFJLEdBQUdILEVBQUUsQ0FBQ2hPLEtBQVY7QUFDRDs7QUFFRCxVQUFJcU8sS0FBSyxHQUFHRixJQUFaOztBQUVBLFVBQUlFLEtBQUssQ0FBQ0MsT0FBVixFQUFtQjtBQUNqQmhOLFFBQUFBLENBQUMsSUFBSStNLEtBQUssQ0FBQ0UsR0FBWDtBQUNELE9BRkQsTUFFTztBQUNMak4sUUFBQUEsQ0FBQyxJQUFJdU0sYUFBYSxDQUFDUSxLQUFLLENBQUNFLEdBQVAsQ0FBbEI7QUFDRDtBQUNGOztBQUVELFdBQU9qTixDQUFQO0FBQ0Q7O0FBRUQsTUFBSWtOLHVCQUF1QixHQUFHO0FBQzVCQyxJQUFBQSxDQUFDLEVBQUVqTixVQUR5QjtBQUU1QmtOLElBQUFBLEVBQUUsRUFBRTlNLFFBRndCO0FBRzVCK00sSUFBQUEsR0FBRyxFQUFFOU0sU0FIdUI7QUFJNUIrTSxJQUFBQSxJQUFJLEVBQUU5TSxTQUpzQjtBQUs1QitNLElBQUFBLENBQUMsRUFBRTdNLFdBTHlCO0FBTTVCOE0sSUFBQUEsRUFBRSxFQUFFM00saUJBTndCO0FBTzVCNE0sSUFBQUEsR0FBRyxFQUFFMU0sc0JBUHVCO0FBUTVCMk0sSUFBQUEsSUFBSSxFQUFFek0scUJBUnNCO0FBUzVCME0sSUFBQUEsQ0FBQyxFQUFFek0sY0FUeUI7QUFVNUIwTSxJQUFBQSxFQUFFLEVBQUV4TSxvQkFWd0I7QUFXNUJ5TSxJQUFBQSxHQUFHLEVBQUV4TSx5QkFYdUI7QUFZNUJ5TSxJQUFBQSxJQUFJLEVBQUV4TSx3QkFac0I7QUFhNUJ3RCxJQUFBQSxDQUFDLEVBQUV2RCxjQWJ5QjtBQWM1QndNLElBQUFBLEVBQUUsRUFBRXRNLFlBZHdCO0FBZTVCdU0sSUFBQUEsR0FBRyxFQUFFcE0sYUFmdUI7QUFnQjVCcU0sSUFBQUEsSUFBSSxFQUFFbk0sYUFoQnNCO0FBaUI1Qm9NLElBQUFBLENBQUMsRUFBRTFNLDJCQWpCeUI7QUFrQjVCMk0sSUFBQUEsRUFBRSxFQUFFek0seUJBbEJ3QjtBQW1CNUIwTSxJQUFBQSxHQUFHLEVBQUV2TSwwQkFuQnVCO0FBb0I1QndNLElBQUFBLElBQUksRUFBRXRNO0FBcEJzQixHQUE5QjtBQXNCQTs7OztBQUlBLE1BQUl1TSxTQUFTO0FBQ2I7QUFDQSxjQUFZO0FBQ1ZBLElBQUFBLFNBQVMsQ0FBQ3JTLE1BQVYsR0FBbUIsU0FBU0EsTUFBVCxDQUFnQjRLLE1BQWhCLEVBQXdCMEgsSUFBeEIsRUFBOEI7QUFDL0MsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsYUFBTyxJQUFJRCxTQUFKLENBQWN6SCxNQUFkLEVBQXNCMEgsSUFBdEIsQ0FBUDtBQUNELEtBTkQ7O0FBUUFELElBQUFBLFNBQVMsQ0FBQ0UsV0FBVixHQUF3QixTQUFTQSxXQUFULENBQXFCQyxHQUFyQixFQUEwQjtBQUNoRCxVQUFJQyxPQUFPLEdBQUcsSUFBZDtBQUFBLFVBQ0lDLFdBQVcsR0FBRyxFQURsQjtBQUFBLFVBRUlDLFNBQVMsR0FBRyxLQUZoQjtBQUdBLFVBQUl0QyxNQUFNLEdBQUcsRUFBYjs7QUFFQSxXQUFLLElBQUl0UixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHeVQsR0FBRyxDQUFDeFQsTUFBeEIsRUFBZ0NELENBQUMsRUFBakMsRUFBcUM7QUFDbkMsWUFBSTZULENBQUMsR0FBR0osR0FBRyxDQUFDSyxNQUFKLENBQVc5VCxDQUFYLENBQVI7O0FBRUEsWUFBSTZULENBQUMsS0FBSyxHQUFWLEVBQWU7QUFDYixjQUFJRixXQUFXLENBQUMxVCxNQUFaLEdBQXFCLENBQXpCLEVBQTRCO0FBQzFCcVIsWUFBQUEsTUFBTSxDQUFDOU8sSUFBUCxDQUFZO0FBQ1Z3UCxjQUFBQSxPQUFPLEVBQUU0QixTQURDO0FBRVYzQixjQUFBQSxHQUFHLEVBQUUwQjtBQUZLLGFBQVo7QUFJRDs7QUFFREQsVUFBQUEsT0FBTyxHQUFHLElBQVY7QUFDQUMsVUFBQUEsV0FBVyxHQUFHLEVBQWQ7QUFDQUMsVUFBQUEsU0FBUyxHQUFHLENBQUNBLFNBQWI7QUFDRCxTQVhELE1BV08sSUFBSUEsU0FBSixFQUFlO0FBQ3BCRCxVQUFBQSxXQUFXLElBQUlFLENBQWY7QUFDRCxTQUZNLE1BRUEsSUFBSUEsQ0FBQyxLQUFLSCxPQUFWLEVBQW1CO0FBQ3hCQyxVQUFBQSxXQUFXLElBQUlFLENBQWY7QUFDRCxTQUZNLE1BRUE7QUFDTCxjQUFJRixXQUFXLENBQUMxVCxNQUFaLEdBQXFCLENBQXpCLEVBQTRCO0FBQzFCcVIsWUFBQUEsTUFBTSxDQUFDOU8sSUFBUCxDQUFZO0FBQ1Z3UCxjQUFBQSxPQUFPLEVBQUUsS0FEQztBQUVWQyxjQUFBQSxHQUFHLEVBQUUwQjtBQUZLLGFBQVo7QUFJRDs7QUFFREEsVUFBQUEsV0FBVyxHQUFHRSxDQUFkO0FBQ0FILFVBQUFBLE9BQU8sR0FBR0csQ0FBVjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUYsV0FBVyxDQUFDMVQsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUMxQnFSLFFBQUFBLE1BQU0sQ0FBQzlPLElBQVAsQ0FBWTtBQUNWd1AsVUFBQUEsT0FBTyxFQUFFNEIsU0FEQztBQUVWM0IsVUFBQUEsR0FBRyxFQUFFMEI7QUFGSyxTQUFaO0FBSUQ7O0FBRUQsYUFBT3JDLE1BQVA7QUFDRCxLQTdDRDs7QUErQ0FnQyxJQUFBQSxTQUFTLENBQUNTLHNCQUFWLEdBQW1DLFNBQVNBLHNCQUFULENBQWdDaEMsS0FBaEMsRUFBdUM7QUFDeEUsYUFBT0csdUJBQXVCLENBQUNILEtBQUQsQ0FBOUI7QUFDRCxLQUZEOztBQUlBLGFBQVN1QixTQUFULENBQW1CekgsTUFBbkIsRUFBMkJtSSxVQUEzQixFQUF1QztBQUNyQyxXQUFLVCxJQUFMLEdBQVlTLFVBQVo7QUFDQSxXQUFLQyxHQUFMLEdBQVdwSSxNQUFYO0FBQ0EsV0FBS3FJLFNBQUwsR0FBaUIsSUFBakI7QUFDRDs7QUFFRCxRQUFJQyxNQUFNLEdBQUdiLFNBQVMsQ0FBQ3pTLFNBQXZCOztBQUVBc1QsSUFBQUEsTUFBTSxDQUFDQyx1QkFBUCxHQUFpQyxTQUFTQSx1QkFBVCxDQUFpQ3hFLEVBQWpDLEVBQXFDMkQsSUFBckMsRUFBMkM7QUFDMUUsVUFBSSxLQUFLVyxTQUFMLEtBQW1CLElBQXZCLEVBQTZCO0FBQzNCLGFBQUtBLFNBQUwsR0FBaUIsS0FBS0QsR0FBTCxDQUFTSSxpQkFBVCxFQUFqQjtBQUNEOztBQUVELFVBQUlDLEVBQUUsR0FBRyxLQUFLSixTQUFMLENBQWVLLFdBQWYsQ0FBMkIzRSxFQUEzQixFQUErQnRQLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUtxSCxJQUF2QixFQUE2QkEsSUFBN0IsQ0FBL0IsQ0FBVDtBQUNBLGFBQU9lLEVBQUUsQ0FBQzVILE1BQUgsRUFBUDtBQUNELEtBUEQ7O0FBU0F5SCxJQUFBQSxNQUFNLENBQUNLLGNBQVAsR0FBd0IsU0FBU0EsY0FBVCxDQUF3QjVFLEVBQXhCLEVBQTRCMkQsSUFBNUIsRUFBa0M7QUFDeEQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsVUFBSWUsRUFBRSxHQUFHLEtBQUtMLEdBQUwsQ0FBU00sV0FBVCxDQUFxQjNFLEVBQXJCLEVBQXlCdFAsTUFBTSxDQUFDNEwsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS3FILElBQXZCLEVBQTZCQSxJQUE3QixDQUF6QixDQUFUO0FBQ0EsYUFBT2UsRUFBRSxDQUFDNUgsTUFBSCxFQUFQO0FBQ0QsS0FQRDs7QUFTQXlILElBQUFBLE1BQU0sQ0FBQ00sbUJBQVAsR0FBNkIsU0FBU0EsbUJBQVQsQ0FBNkI3RSxFQUE3QixFQUFpQzJELElBQWpDLEVBQXVDO0FBQ2xFLFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELFVBQUllLEVBQUUsR0FBRyxLQUFLTCxHQUFMLENBQVNNLFdBQVQsQ0FBcUIzRSxFQUFyQixFQUF5QnRQLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUtxSCxJQUF2QixFQUE2QkEsSUFBN0IsQ0FBekIsQ0FBVDtBQUNBLGFBQU9lLEVBQUUsQ0FBQzdNLGFBQUgsRUFBUDtBQUNELEtBUEQ7O0FBU0EwTSxJQUFBQSxNQUFNLENBQUNPLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxDQUF5QjlFLEVBQXpCLEVBQTZCMkQsSUFBN0IsRUFBbUM7QUFDMUQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsVUFBSWUsRUFBRSxHQUFHLEtBQUtMLEdBQUwsQ0FBU00sV0FBVCxDQUFxQjNFLEVBQXJCLEVBQXlCdFAsTUFBTSxDQUFDNEwsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS3FILElBQXZCLEVBQTZCQSxJQUE3QixDQUF6QixDQUFUO0FBQ0EsYUFBT2UsRUFBRSxDQUFDSSxlQUFILEVBQVA7QUFDRCxLQVBEOztBQVNBUCxJQUFBQSxNQUFNLENBQUNRLEdBQVAsR0FBYSxTQUFTQSxHQUFULENBQWE1UCxDQUFiLEVBQWdCdEQsQ0FBaEIsRUFBbUI7QUFDOUIsVUFBSUEsQ0FBQyxLQUFLLEtBQUssQ0FBZixFQUFrQjtBQUNoQkEsUUFBQUEsQ0FBQyxHQUFHLENBQUo7QUFDRCxPQUg2QixDQUs5Qjs7O0FBQ0EsVUFBSSxLQUFLOFIsSUFBTCxDQUFVcUIsV0FBZCxFQUEyQjtBQUN6QixlQUFPdkwsUUFBUSxDQUFDdEUsQ0FBRCxFQUFJdEQsQ0FBSixDQUFmO0FBQ0Q7O0FBRUQsVUFBSThSLElBQUksR0FBR2pULE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUtxSCxJQUF2QixDQUFYOztBQUVBLFVBQUk5UixDQUFDLEdBQUcsQ0FBUixFQUFXO0FBQ1Q4UixRQUFBQSxJQUFJLENBQUNzQixLQUFMLEdBQWFwVCxDQUFiO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLd1MsR0FBTCxDQUFTYSxlQUFULENBQXlCdkIsSUFBekIsRUFBK0I3RyxNQUEvQixDQUFzQzNILENBQXRDLENBQVA7QUFDRCxLQWpCRDs7QUFtQkFvUCxJQUFBQSxNQUFNLENBQUNZLHdCQUFQLEdBQWtDLFNBQVNBLHdCQUFULENBQWtDbkYsRUFBbEMsRUFBc0M2RCxHQUF0QyxFQUEyQztBQUMzRSxVQUFJdUIsS0FBSyxHQUFHLElBQVo7O0FBRUEsVUFBSUMsWUFBWSxHQUFHLEtBQUtoQixHQUFMLENBQVNpQixXQUFULE9BQTJCLElBQTlDO0FBQUEsVUFDSUMsb0JBQW9CLEdBQUcsS0FBS2xCLEdBQUwsQ0FBU21CLGNBQVQsSUFBMkIsS0FBS25CLEdBQUwsQ0FBU21CLGNBQVQsS0FBNEIsU0FBdkQsSUFBb0U1TixnQkFBZ0IsRUFEL0c7QUFBQSxVQUVJa0MsTUFBTSxHQUFHLFNBQVNBLE1BQVQsQ0FBZ0I2SixJQUFoQixFQUFzQjhCLE9BQXRCLEVBQStCO0FBQzFDLGVBQU9MLEtBQUssQ0FBQ2YsR0FBTixDQUFVb0IsT0FBVixDQUFrQnpGLEVBQWxCLEVBQXNCMkQsSUFBdEIsRUFBNEI4QixPQUE1QixDQUFQO0FBQ0QsT0FKRDtBQUFBLFVBS0lwSCxZQUFZLEdBQUcsU0FBU0EsWUFBVCxDQUFzQnNGLElBQXRCLEVBQTRCO0FBQzdDLFlBQUkzRCxFQUFFLENBQUMwRixhQUFILElBQW9CMUYsRUFBRSxDQUFDMUIsTUFBSCxLQUFjLENBQWxDLElBQXVDcUYsSUFBSSxDQUFDZ0MsTUFBaEQsRUFBd0Q7QUFDdEQsaUJBQU8sR0FBUDtBQUNEOztBQUVELGVBQU8zRixFQUFFLENBQUM0RixPQUFILEdBQWE1RixFQUFFLENBQUM2RixJQUFILENBQVF4SCxZQUFSLENBQXFCMkIsRUFBRSxDQUFDakUsRUFBeEIsRUFBNEI0SCxJQUFJLENBQUM3RyxNQUFqQyxDQUFiLEdBQXdELEVBQS9EO0FBQ0QsT0FYRDtBQUFBLFVBWUlnSixRQUFRLEdBQUcsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQyxlQUFPVCxZQUFZLEdBQUd0RixtQkFBbUIsQ0FBQ0MsRUFBRCxDQUF0QixHQUE2QmxHLE1BQU0sQ0FBQztBQUNyRC9ELFVBQUFBLElBQUksRUFBRSxTQUQrQztBQUVyRFEsVUFBQUEsTUFBTSxFQUFFO0FBRjZDLFNBQUQsRUFHbkQsV0FIbUQsQ0FBdEQ7QUFJRCxPQWpCRDtBQUFBLFVBa0JJZixLQUFLLEdBQUcsU0FBU0EsS0FBVCxDQUFlbkYsTUFBZixFQUF1QjBWLFVBQXZCLEVBQW1DO0FBQzdDLGVBQU9WLFlBQVksR0FBR25GLGdCQUFnQixDQUFDRixFQUFELEVBQUszUCxNQUFMLENBQW5CLEdBQWtDeUosTUFBTSxDQUFDaU0sVUFBVSxHQUFHO0FBQ3ZFdlEsVUFBQUEsS0FBSyxFQUFFbkY7QUFEZ0UsU0FBSCxHQUVsRTtBQUNGbUYsVUFBQUEsS0FBSyxFQUFFbkYsTUFETDtBQUVGb0YsVUFBQUEsR0FBRyxFQUFFO0FBRkgsU0FGdUQsRUFLeEQsT0FMd0QsQ0FBM0Q7QUFNRCxPQXpCRDtBQUFBLFVBMEJJSSxPQUFPLEdBQUcsU0FBU0EsT0FBVCxDQUFpQnhGLE1BQWpCLEVBQXlCMFYsVUFBekIsRUFBcUM7QUFDakQsZUFBT1YsWUFBWSxHQUFHcEYsa0JBQWtCLENBQUNELEVBQUQsRUFBSzNQLE1BQUwsQ0FBckIsR0FBb0N5SixNQUFNLENBQUNpTSxVQUFVLEdBQUc7QUFDekVsUSxVQUFBQSxPQUFPLEVBQUV4RjtBQURnRSxTQUFILEdBRXBFO0FBQ0Z3RixVQUFBQSxPQUFPLEVBQUV4RixNQURQO0FBRUZtRixVQUFBQSxLQUFLLEVBQUUsTUFGTDtBQUdGQyxVQUFBQSxHQUFHLEVBQUU7QUFISCxTQUZ5RCxFQU0xRCxTQU4wRCxDQUE3RDtBQU9ELE9BbENEO0FBQUEsVUFtQ0l1USxVQUFVLEdBQUcsU0FBU0EsVUFBVCxDQUFvQjdELEtBQXBCLEVBQTJCO0FBQzFDLFlBQUlpQyxVQUFVLEdBQUdWLFNBQVMsQ0FBQ1Msc0JBQVYsQ0FBaUNoQyxLQUFqQyxDQUFqQjs7QUFFQSxZQUFJaUMsVUFBSixFQUFnQjtBQUNkLGlCQUFPZ0IsS0FBSyxDQUFDWix1QkFBTixDQUE4QnhFLEVBQTlCLEVBQWtDb0UsVUFBbEMsQ0FBUDtBQUNELFNBRkQsTUFFTztBQUNMLGlCQUFPakMsS0FBUDtBQUNEO0FBQ0YsT0EzQ0Q7QUFBQSxVQTRDSThELEdBQUcsR0FBRyxTQUFTQSxHQUFULENBQWE1VixNQUFiLEVBQXFCO0FBQzdCLGVBQU9nVixZQUFZLEdBQUdsRixjQUFjLENBQUNILEVBQUQsRUFBSzNQLE1BQUwsQ0FBakIsR0FBZ0N5SixNQUFNLENBQUM7QUFDeERtTSxVQUFBQSxHQUFHLEVBQUU1VjtBQURtRCxTQUFELEVBRXRELEtBRnNELENBQXpEO0FBR0QsT0FoREQ7QUFBQSxVQWlESXNSLGFBQWEsR0FBRyxTQUFTQSxhQUFULENBQXVCUSxLQUF2QixFQUE4QjtBQUNoRDtBQUNBLGdCQUFRQSxLQUFSO0FBQ0U7QUFDQSxlQUFLLEdBQUw7QUFDRSxtQkFBT2lELEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDM0UsV0FBYixDQUFQOztBQUVGLGVBQUssR0FBTCxDQUxGLENBS1k7O0FBRVYsZUFBSyxLQUFMO0FBQ0UsbUJBQU8rSixLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQzNFLFdBQWIsRUFBMEIsQ0FBMUIsQ0FBUDtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFLG1CQUFPK0osS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUM5SixNQUFiLENBQVA7O0FBRUYsZUFBSyxJQUFMO0FBQ0UsbUJBQU9rUCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQzlKLE1BQWIsRUFBcUIsQ0FBckIsQ0FBUDtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFLG1CQUFPa1AsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNoSyxNQUFiLENBQVA7O0FBRUYsZUFBSyxJQUFMO0FBQ0UsbUJBQU9vUCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ2hLLE1BQWIsRUFBcUIsQ0FBckIsQ0FBUDtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFLG1CQUFPb1AsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNqSyxJQUFILEdBQVUsRUFBVixLQUFpQixDQUFqQixHQUFxQixFQUFyQixHQUEwQmlLLEVBQUUsQ0FBQ2pLLElBQUgsR0FBVSxFQUE5QyxDQUFQOztBQUVGLGVBQUssSUFBTDtBQUNFLG1CQUFPcVAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNqSyxJQUFILEdBQVUsRUFBVixLQUFpQixDQUFqQixHQUFxQixFQUFyQixHQUEwQmlLLEVBQUUsQ0FBQ2pLLElBQUgsR0FBVSxFQUE5QyxFQUFrRCxDQUFsRCxDQUFQOztBQUVGLGVBQUssR0FBTDtBQUNFLG1CQUFPcVAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNqSyxJQUFiLENBQVA7O0FBRUYsZUFBSyxJQUFMO0FBQ0UsbUJBQU9xUCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ2pLLElBQWIsRUFBbUIsQ0FBbkIsQ0FBUDtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFO0FBQ0EsbUJBQU9zSSxZQUFZLENBQUM7QUFDbEJ2QixjQUFBQSxNQUFNLEVBQUUsUUFEVTtBQUVsQjZJLGNBQUFBLE1BQU0sRUFBRVAsS0FBSyxDQUFDekIsSUFBTixDQUFXZ0M7QUFGRCxhQUFELENBQW5COztBQUtGLGVBQUssSUFBTDtBQUNFO0FBQ0EsbUJBQU90SCxZQUFZLENBQUM7QUFDbEJ2QixjQUFBQSxNQUFNLEVBQUUsT0FEVTtBQUVsQjZJLGNBQUFBLE1BQU0sRUFBRVAsS0FBSyxDQUFDekIsSUFBTixDQUFXZ0M7QUFGRCxhQUFELENBQW5COztBQUtGLGVBQUssS0FBTDtBQUNFO0FBQ0EsbUJBQU90SCxZQUFZLENBQUM7QUFDbEJ2QixjQUFBQSxNQUFNLEVBQUUsUUFEVTtBQUVsQjZJLGNBQUFBLE1BQU0sRUFBRTtBQUZVLGFBQUQsQ0FBbkI7O0FBS0YsZUFBSyxNQUFMO0FBQ0U7QUFDQSxtQkFBTzNGLEVBQUUsQ0FBQzZGLElBQUgsQ0FBUUssVUFBUixDQUFtQmxHLEVBQUUsQ0FBQ2pFLEVBQXRCLEVBQTBCO0FBQy9CZSxjQUFBQSxNQUFNLEVBQUUsT0FEdUI7QUFFL0JiLGNBQUFBLE1BQU0sRUFBRW1KLEtBQUssQ0FBQ2YsR0FBTixDQUFVcEk7QUFGYSxhQUExQixDQUFQOztBQUtGLGVBQUssT0FBTDtBQUNFO0FBQ0EsbUJBQU8rRCxFQUFFLENBQUM2RixJQUFILENBQVFLLFVBQVIsQ0FBbUJsRyxFQUFFLENBQUNqRSxFQUF0QixFQUEwQjtBQUMvQmUsY0FBQUEsTUFBTSxFQUFFLE1BRHVCO0FBRS9CYixjQUFBQSxNQUFNLEVBQUVtSixLQUFLLENBQUNmLEdBQU4sQ0FBVXBJO0FBRmEsYUFBMUIsQ0FBUDtBQUlGOztBQUVBLGVBQUssR0FBTDtBQUNFO0FBQ0EsbUJBQU8rRCxFQUFFLENBQUNtRyxRQUFWO0FBQ0Y7O0FBRUEsZUFBSyxHQUFMO0FBQ0UsbUJBQU9MLFFBQVEsRUFBZjtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFLG1CQUFPUCxvQkFBb0IsR0FBR3pMLE1BQU0sQ0FBQztBQUNuQ3JFLGNBQUFBLEdBQUcsRUFBRTtBQUQ4QixhQUFELEVBRWpDLEtBRmlDLENBQVQsR0FFZjJQLEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDdkssR0FBYixDQUZaOztBQUlGLGVBQUssSUFBTDtBQUNFLG1CQUFPOFAsb0JBQW9CLEdBQUd6TCxNQUFNLENBQUM7QUFDbkNyRSxjQUFBQSxHQUFHLEVBQUU7QUFEOEIsYUFBRCxFQUVqQyxLQUZpQyxDQUFULEdBRWYyUCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ3ZLLEdBQWIsRUFBa0IsQ0FBbEIsQ0FGWjtBQUdGOztBQUVBLGVBQUssR0FBTDtBQUNFO0FBQ0EsbUJBQU8yUCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ25LLE9BQWIsQ0FBUDs7QUFFRixlQUFLLEtBQUw7QUFDRTtBQUNBLG1CQUFPQSxPQUFPLENBQUMsT0FBRCxFQUFVLElBQVYsQ0FBZDs7QUFFRixlQUFLLE1BQUw7QUFDRTtBQUNBLG1CQUFPQSxPQUFPLENBQUMsTUFBRCxFQUFTLElBQVQsQ0FBZDs7QUFFRixlQUFLLE9BQUw7QUFDRTtBQUNBLG1CQUFPQSxPQUFPLENBQUMsUUFBRCxFQUFXLElBQVgsQ0FBZDtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFO0FBQ0EsbUJBQU91UCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ25LLE9BQWIsQ0FBUDs7QUFFRixlQUFLLEtBQUw7QUFDRTtBQUNBLG1CQUFPQSxPQUFPLENBQUMsT0FBRCxFQUFVLEtBQVYsQ0FBZDs7QUFFRixlQUFLLE1BQUw7QUFDRTtBQUNBLG1CQUFPQSxPQUFPLENBQUMsTUFBRCxFQUFTLEtBQVQsQ0FBZDs7QUFFRixlQUFLLE9BQUw7QUFDRTtBQUNBLG1CQUFPQSxPQUFPLENBQUMsUUFBRCxFQUFXLEtBQVgsQ0FBZDtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFO0FBQ0EsbUJBQU8wUCxvQkFBb0IsR0FBR3pMLE1BQU0sQ0FBQztBQUNuQ3RFLGNBQUFBLEtBQUssRUFBRSxTQUQ0QjtBQUVuQ0MsY0FBQUEsR0FBRyxFQUFFO0FBRjhCLGFBQUQsRUFHakMsT0FIaUMsQ0FBVCxHQUdiMlAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUN4SyxLQUFiLENBSGQ7O0FBS0YsZUFBSyxJQUFMO0FBQ0U7QUFDQSxtQkFBTytQLG9CQUFvQixHQUFHekwsTUFBTSxDQUFDO0FBQ25DdEUsY0FBQUEsS0FBSyxFQUFFLFNBRDRCO0FBRW5DQyxjQUFBQSxHQUFHLEVBQUU7QUFGOEIsYUFBRCxFQUdqQyxPQUhpQyxDQUFULEdBR2IyUCxLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ3hLLEtBQWIsRUFBb0IsQ0FBcEIsQ0FIZDs7QUFLRixlQUFLLEtBQUw7QUFDRTtBQUNBLG1CQUFPQSxLQUFLLENBQUMsT0FBRCxFQUFVLElBQVYsQ0FBWjs7QUFFRixlQUFLLE1BQUw7QUFDRTtBQUNBLG1CQUFPQSxLQUFLLENBQUMsTUFBRCxFQUFTLElBQVQsQ0FBWjs7QUFFRixlQUFLLE9BQUw7QUFDRTtBQUNBLG1CQUFPQSxLQUFLLENBQUMsUUFBRCxFQUFXLElBQVgsQ0FBWjtBQUNGOztBQUVBLGVBQUssR0FBTDtBQUNFO0FBQ0EsbUJBQU8rUCxvQkFBb0IsR0FBR3pMLE1BQU0sQ0FBQztBQUNuQ3RFLGNBQUFBLEtBQUssRUFBRTtBQUQ0QixhQUFELEVBRWpDLE9BRmlDLENBQVQsR0FFYjRQLEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDeEssS0FBYixDQUZkOztBQUlGLGVBQUssSUFBTDtBQUNFO0FBQ0EsbUJBQU8rUCxvQkFBb0IsR0FBR3pMLE1BQU0sQ0FBQztBQUNuQ3RFLGNBQUFBLEtBQUssRUFBRTtBQUQ0QixhQUFELEVBRWpDLE9BRmlDLENBQVQsR0FFYjRQLEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDeEssS0FBYixFQUFvQixDQUFwQixDQUZkOztBQUlGLGVBQUssS0FBTDtBQUNFO0FBQ0EsbUJBQU9BLEtBQUssQ0FBQyxPQUFELEVBQVUsS0FBVixDQUFaOztBQUVGLGVBQUssTUFBTDtBQUNFO0FBQ0EsbUJBQU9BLEtBQUssQ0FBQyxNQUFELEVBQVMsS0FBVCxDQUFaOztBQUVGLGVBQUssT0FBTDtBQUNFO0FBQ0EsbUJBQU9BLEtBQUssQ0FBQyxRQUFELEVBQVcsS0FBWCxDQUFaO0FBQ0Y7O0FBRUEsZUFBSyxHQUFMO0FBQ0U7QUFDQSxtQkFBTytQLG9CQUFvQixHQUFHekwsTUFBTSxDQUFDO0FBQ25DdkUsY0FBQUEsSUFBSSxFQUFFO0FBRDZCLGFBQUQsRUFFakMsTUFGaUMsQ0FBVCxHQUVkNlAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUN6SyxJQUFiLENBRmI7O0FBSUYsZUFBSyxJQUFMO0FBQ0U7QUFDQSxtQkFBT2dRLG9CQUFvQixHQUFHekwsTUFBTSxDQUFDO0FBQ25DdkUsY0FBQUEsSUFBSSxFQUFFO0FBRDZCLGFBQUQsRUFFakMsTUFGaUMsQ0FBVCxHQUVkNlAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUN6SyxJQUFILENBQVFuRCxRQUFSLEdBQW1Cd0gsS0FBbkIsQ0FBeUIsQ0FBQyxDQUExQixDQUFWLEVBQXdDLENBQXhDLENBRmI7O0FBSUYsZUFBSyxNQUFMO0FBQ0U7QUFDQSxtQkFBTzJMLG9CQUFvQixHQUFHekwsTUFBTSxDQUFDO0FBQ25DdkUsY0FBQUEsSUFBSSxFQUFFO0FBRDZCLGFBQUQsRUFFakMsTUFGaUMsQ0FBVCxHQUVkNlAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUN6SyxJQUFiLEVBQW1CLENBQW5CLENBRmI7O0FBSUYsZUFBSyxRQUFMO0FBQ0U7QUFDQSxtQkFBT2dRLG9CQUFvQixHQUFHekwsTUFBTSxDQUFDO0FBQ25DdkUsY0FBQUEsSUFBSSxFQUFFO0FBRDZCLGFBQUQsRUFFakMsTUFGaUMsQ0FBVCxHQUVkNlAsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUN6SyxJQUFiLEVBQW1CLENBQW5CLENBRmI7QUFHRjs7QUFFQSxlQUFLLEdBQUw7QUFDRTtBQUNBLG1CQUFPMFEsR0FBRyxDQUFDLE9BQUQsQ0FBVjs7QUFFRixlQUFLLElBQUw7QUFDRTtBQUNBLG1CQUFPQSxHQUFHLENBQUMsTUFBRCxDQUFWOztBQUVGLGVBQUssT0FBTDtBQUNFLG1CQUFPQSxHQUFHLENBQUMsUUFBRCxDQUFWOztBQUVGLGVBQUssSUFBTDtBQUNFLG1CQUFPYixLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ3ZFLFFBQUgsQ0FBWXJKLFFBQVosR0FBdUJ3SCxLQUF2QixDQUE2QixDQUFDLENBQTlCLENBQVYsRUFBNEMsQ0FBNUMsQ0FBUDs7QUFFRixlQUFLLE1BQUw7QUFDRSxtQkFBT3dMLEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDdkUsUUFBYixFQUF1QixDQUF2QixDQUFQOztBQUVGLGVBQUssR0FBTDtBQUNFLG1CQUFPMkosS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNvRyxVQUFiLENBQVA7O0FBRUYsZUFBSyxJQUFMO0FBQ0UsbUJBQU9oQixLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ29HLFVBQWIsRUFBeUIsQ0FBekIsQ0FBUDs7QUFFRixlQUFLLEdBQUw7QUFDRSxtQkFBT2hCLEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDcUcsT0FBYixDQUFQOztBQUVGLGVBQUssS0FBTDtBQUNFLG1CQUFPakIsS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNxRyxPQUFiLEVBQXNCLENBQXRCLENBQVA7O0FBRUYsZUFBSyxHQUFMO0FBQ0U7QUFDQSxtQkFBT2pCLEtBQUssQ0FBQ0wsR0FBTixDQUFVL0UsRUFBRSxDQUFDc0csT0FBYixDQUFQOztBQUVGLGVBQUssSUFBTDtBQUNFO0FBQ0EsbUJBQU9sQixLQUFLLENBQUNMLEdBQU4sQ0FBVS9FLEVBQUUsQ0FBQ3NHLE9BQWIsRUFBc0IsQ0FBdEIsQ0FBUDs7QUFFRixlQUFLLEdBQUw7QUFDRSxtQkFBT2xCLEtBQUssQ0FBQ0wsR0FBTixDQUFVeEwsSUFBSSxDQUFDQyxLQUFMLENBQVd3RyxFQUFFLENBQUNqRSxFQUFILEdBQVEsSUFBbkIsQ0FBVixDQUFQOztBQUVGLGVBQUssR0FBTDtBQUNFLG1CQUFPcUosS0FBSyxDQUFDTCxHQUFOLENBQVUvRSxFQUFFLENBQUNqRSxFQUFiLENBQVA7O0FBRUY7QUFDRSxtQkFBT2lLLFVBQVUsQ0FBQzdELEtBQUQsQ0FBakI7QUF6UEo7QUEyUEQsT0E5U0Q7O0FBZ1RBLGFBQU9WLGVBQWUsQ0FBQ2lDLFNBQVMsQ0FBQ0UsV0FBVixDQUFzQkMsR0FBdEIsQ0FBRCxFQUE2QmxDLGFBQTdCLENBQXRCO0FBQ0QsS0FwVEQ7O0FBc1RBNEMsSUFBQUEsTUFBTSxDQUFDZ0Msd0JBQVAsR0FBa0MsU0FBU0Esd0JBQVQsQ0FBa0NDLEdBQWxDLEVBQXVDM0MsR0FBdkMsRUFBNEM7QUFDNUUsVUFBSTRDLE1BQU0sR0FBRyxJQUFiOztBQUVBLFVBQUlDLFlBQVksR0FBRyxTQUFTQSxZQUFULENBQXNCdkUsS0FBdEIsRUFBNkI7QUFDOUMsZ0JBQVFBLEtBQUssQ0FBQyxDQUFELENBQWI7QUFDRSxlQUFLLEdBQUw7QUFDRSxtQkFBTyxhQUFQOztBQUVGLGVBQUssR0FBTDtBQUNFLG1CQUFPLFFBQVA7O0FBRUYsZUFBSyxHQUFMO0FBQ0UsbUJBQU8sUUFBUDs7QUFFRixlQUFLLEdBQUw7QUFDRSxtQkFBTyxNQUFQOztBQUVGLGVBQUssR0FBTDtBQUNFLG1CQUFPLEtBQVA7O0FBRUYsZUFBSyxHQUFMO0FBQ0UsbUJBQU8sT0FBUDs7QUFFRixlQUFLLEdBQUw7QUFDRSxtQkFBTyxNQUFQOztBQUVGO0FBQ0UsbUJBQU8sSUFBUDtBQXZCSjtBQXlCRCxPQTFCRDtBQUFBLFVBMkJJUixhQUFhLEdBQUcsU0FBU0EsYUFBVCxDQUF1QmdGLE1BQXZCLEVBQStCO0FBQ2pELGVBQU8sVUFBVXhFLEtBQVYsRUFBaUI7QUFDdEIsY0FBSXlFLE1BQU0sR0FBR0YsWUFBWSxDQUFDdkUsS0FBRCxDQUF6Qjs7QUFFQSxjQUFJeUUsTUFBSixFQUFZO0FBQ1YsbUJBQU9ILE1BQU0sQ0FBQzFCLEdBQVAsQ0FBVzRCLE1BQU0sQ0FBQ2hULEdBQVAsQ0FBV2lULE1BQVgsQ0FBWCxFQUErQnpFLEtBQUssQ0FBQzlSLE1BQXJDLENBQVA7QUFDRCxXQUZELE1BRU87QUFDTCxtQkFBTzhSLEtBQVA7QUFDRDtBQUNGLFNBUkQ7QUFTRCxPQXJDRDtBQUFBLFVBc0NJMEUsTUFBTSxHQUFHbkQsU0FBUyxDQUFDRSxXQUFWLENBQXNCQyxHQUF0QixDQXRDYjtBQUFBLFVBdUNJaUQsVUFBVSxHQUFHRCxNQUFNLENBQUNyTyxNQUFQLENBQWMsVUFBVXVPLEtBQVYsRUFBaUJDLEtBQWpCLEVBQXdCO0FBQ3JELFlBQUk1RSxPQUFPLEdBQUc0RSxLQUFLLENBQUM1RSxPQUFwQjtBQUFBLFlBQ0lDLEdBQUcsR0FBRzJFLEtBQUssQ0FBQzNFLEdBRGhCO0FBRUEsZUFBT0QsT0FBTyxHQUFHMkUsS0FBSCxHQUFXQSxLQUFLLENBQUNFLE1BQU4sQ0FBYTVFLEdBQWIsQ0FBekI7QUFDRCxPQUpnQixFQUlkLEVBSmMsQ0F2Q2pCO0FBQUEsVUE0Q0k2RSxTQUFTLEdBQUdWLEdBQUcsQ0FBQ1csT0FBSixDQUFZdFUsS0FBWixDQUFrQjJULEdBQWxCLEVBQXVCTSxVQUFVLENBQUNNLEdBQVgsQ0FBZVYsWUFBZixFQUE2QlcsTUFBN0IsQ0FBb0MsVUFBVTFFLENBQVYsRUFBYTtBQUN0RixlQUFPQSxDQUFQO0FBQ0QsT0FGc0MsQ0FBdkIsQ0E1Q2hCOztBQWdEQSxhQUFPbEIsZUFBZSxDQUFDb0YsTUFBRCxFQUFTbEYsYUFBYSxDQUFDdUYsU0FBRCxDQUF0QixDQUF0QjtBQUNELEtBcEREOztBQXNEQSxXQUFPeEQsU0FBUDtBQUNELEdBeGVELEVBRkE7O0FBNGVBLE1BQUk0RCxPQUFPO0FBQ1g7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsT0FBVCxDQUFpQmxULE1BQWpCLEVBQXlCbVQsV0FBekIsRUFBc0M7QUFDcEMsV0FBS25ULE1BQUwsR0FBY0EsTUFBZDtBQUNBLFdBQUttVCxXQUFMLEdBQW1CQSxXQUFuQjtBQUNEOztBQUVELFFBQUloRCxNQUFNLEdBQUcrQyxPQUFPLENBQUNyVyxTQUFyQjs7QUFFQXNULElBQUFBLE1BQU0sQ0FBQ2xRLFNBQVAsR0FBbUIsU0FBU0EsU0FBVCxHQUFxQjtBQUN0QyxVQUFJLEtBQUtrVCxXQUFULEVBQXNCO0FBQ3BCLGVBQU8sS0FBS25ULE1BQUwsR0FBYyxJQUFkLEdBQXFCLEtBQUttVCxXQUFqQztBQUNELE9BRkQsTUFFTztBQUNMLGVBQU8sS0FBS25ULE1BQVo7QUFDRDtBQUNGLEtBTkQ7O0FBUUEsV0FBT2tULE9BQVA7QUFDRCxHQWpCRCxFQUZBO0FBcUJBOzs7OztBQUlBLE1BQUlFLElBQUk7QUFDUjtBQUNBLGNBQVk7QUFDVixhQUFTQSxJQUFULEdBQWdCLENBQUU7O0FBRWxCLFFBQUlqRCxNQUFNLEdBQUdpRCxJQUFJLENBQUN2VyxTQUFsQjtBQUVBOzs7Ozs7Ozs7O0FBU0FzVCxJQUFBQSxNQUFNLENBQUMyQixVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0JuSyxFQUFwQixFQUF3QjRILElBQXhCLEVBQThCO0FBQ2hELFlBQU0sSUFBSTFPLG1CQUFKLEVBQU47QUFDRDtBQUNEOzs7Ozs7OztBQUhBOztBQWFBc1AsSUFBQUEsTUFBTSxDQUFDbEcsWUFBUCxHQUFzQixTQUFTQSxZQUFULENBQXNCdEMsRUFBdEIsRUFBMEJlLE1BQTFCLEVBQWtDO0FBQ3RELFlBQU0sSUFBSTdILG1CQUFKLEVBQU47QUFDRDtBQUNEOzs7Ozs7QUFIQTs7QUFXQXNQLElBQUFBLE1BQU0sQ0FBQ2pHLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxDQUFnQnZDLEVBQWhCLEVBQW9CO0FBQ2xDLFlBQU0sSUFBSTlHLG1CQUFKLEVBQU47QUFDRDtBQUNEOzs7Ozs7QUFIQTs7QUFXQXNQLElBQUFBLE1BQU0sQ0FBQ2tELE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxDQUFnQkMsU0FBaEIsRUFBMkI7QUFDekMsWUFBTSxJQUFJelMsbUJBQUosRUFBTjtBQUNEO0FBQ0Q7Ozs7O0FBSEE7O0FBVUFwRSxJQUFBQSxZQUFZLENBQUMyVyxJQUFELEVBQU8sQ0FBQztBQUNsQjVXLE1BQUFBLEdBQUcsRUFBRSxNQURhOztBQUdsQjs7Ozs7QUFLQStDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsY0FBTSxJQUFJc0IsbUJBQUosRUFBTjtBQUNEO0FBQ0Q7Ozs7OztBQVhrQixLQUFELEVBaUJoQjtBQUNEckUsTUFBQUEsR0FBRyxFQUFFLE1BREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsY0FBTSxJQUFJc0IsbUJBQUosRUFBTjtBQUNEO0FBQ0Q7Ozs7OztBQUxDLEtBakJnQixFQTRCaEI7QUFDRHJFLE1BQUFBLEdBQUcsRUFBRSxXQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGNBQU0sSUFBSXNCLG1CQUFKLEVBQU47QUFDRDtBQUpBLEtBNUJnQixFQWlDaEI7QUFDRHJFLE1BQUFBLEdBQUcsRUFBRSxTQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGNBQU0sSUFBSXNCLG1CQUFKLEVBQU47QUFDRDtBQUpBLEtBakNnQixDQUFQLENBQVo7O0FBd0NBLFdBQU91UyxJQUFQO0FBQ0QsR0FwR0QsRUFGQTs7QUF3R0EsTUFBSUcsU0FBUyxHQUFHLElBQWhCO0FBQ0E7Ozs7O0FBS0EsTUFBSUMsU0FBUztBQUNiO0FBQ0EsWUFBVUMsS0FBVixFQUFpQjtBQUNmM1csSUFBQUEsY0FBYyxDQUFDMFcsU0FBRCxFQUFZQyxLQUFaLENBQWQ7O0FBRUEsYUFBU0QsU0FBVCxHQUFxQjtBQUNuQixhQUFPQyxLQUFLLENBQUNoVixLQUFOLENBQVksSUFBWixFQUFrQkksU0FBbEIsS0FBZ0MsSUFBdkM7QUFDRDs7QUFFRCxRQUFJc1IsTUFBTSxHQUFHcUQsU0FBUyxDQUFDM1csU0FBdkI7QUFFQTs7QUFDQXNULElBQUFBLE1BQU0sQ0FBQzJCLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQm5LLEVBQXBCLEVBQXdCa0csSUFBeEIsRUFBOEI7QUFDaEQsVUFBSW5GLE1BQU0sR0FBR21GLElBQUksQ0FBQ25GLE1BQWxCO0FBQUEsVUFDSWIsTUFBTSxHQUFHZ0csSUFBSSxDQUFDaEcsTUFEbEI7QUFFQSxhQUFPSCxhQUFhLENBQUNDLEVBQUQsRUFBS2UsTUFBTCxFQUFhYixNQUFiLENBQXBCO0FBQ0Q7QUFDRDtBQUxBOztBQVFBc0ksSUFBQUEsTUFBTSxDQUFDbEcsWUFBUCxHQUFzQixTQUFTeUosY0FBVCxDQUF3Qi9MLEVBQXhCLEVBQTRCZSxNQUE1QixFQUFvQztBQUN4RCxhQUFPdUIsWUFBWSxDQUFDLEtBQUtDLE1BQUwsQ0FBWXZDLEVBQVosQ0FBRCxFQUFrQmUsTUFBbEIsQ0FBbkI7QUFDRDtBQUNEO0FBSEE7O0FBTUF5SCxJQUFBQSxNQUFNLENBQUNqRyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0J2QyxFQUFoQixFQUFvQjtBQUNsQyxhQUFPLENBQUMsSUFBSTVKLElBQUosQ0FBUzRKLEVBQVQsRUFBYWdNLGlCQUFiLEVBQVI7QUFDRDtBQUNEO0FBSEE7O0FBTUF4RCxJQUFBQSxNQUFNLENBQUNrRCxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JDLFNBQWhCLEVBQTJCO0FBQ3pDLGFBQU9BLFNBQVMsQ0FBQy9LLElBQVYsS0FBbUIsT0FBMUI7QUFDRDtBQUNEO0FBSEE7O0FBTUE5TCxJQUFBQSxZQUFZLENBQUMrVyxTQUFELEVBQVksQ0FBQztBQUN2QmhYLE1BQUFBLEdBQUcsRUFBRSxNQURrQjs7QUFHdkI7QUFDQStDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxPQUFQO0FBQ0Q7QUFDRDs7QUFQdUIsS0FBRCxFQVNyQjtBQUNEL0MsTUFBQUEsR0FBRyxFQUFFLE1BREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsWUFBSThELE9BQU8sRUFBWCxFQUFlO0FBQ2IsaUJBQU8sSUFBSUMsSUFBSSxDQUFDQyxjQUFULEdBQTBCbU4sZUFBMUIsR0FBNEM1SSxRQUFuRDtBQUNELFNBRkQsTUFFTyxPQUFPLE9BQVA7QUFDUjtBQUNEOztBQVBDLEtBVHFCLEVBa0JyQjtBQUNEdEwsTUFBQUEsR0FBRyxFQUFFLFdBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFQO0FBQ0Q7QUFKQSxLQWxCcUIsRUF1QnJCO0FBQ0QvQyxNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLElBQVA7QUFDRDtBQUpBLEtBdkJxQixDQUFaLEVBNEJSLENBQUM7QUFDSC9DLE1BQUFBLEdBQUcsRUFBRSxVQURGOztBQUdIOzs7O0FBSUErQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLFlBQUlnVSxTQUFTLEtBQUssSUFBbEIsRUFBd0I7QUFDdEJBLFVBQUFBLFNBQVMsR0FBRyxJQUFJQyxTQUFKLEVBQVo7QUFDRDs7QUFFRCxlQUFPRCxTQUFQO0FBQ0Q7QUFiRSxLQUFELENBNUJRLENBQVo7O0FBNENBLFdBQU9DLFNBQVA7QUFDRCxHQWpGRCxDQWlGRUosSUFqRkYsQ0FGQTs7QUFxRkEsTUFBSVEsYUFBYSxHQUFHQyxNQUFNLENBQUMsTUFBTW5KLFNBQVMsQ0FBQ29KLE1BQWhCLEdBQXlCLEdBQTFCLENBQTFCO0FBQ0EsTUFBSUMsUUFBUSxHQUFHLEVBQWY7O0FBRUEsV0FBU0MsT0FBVCxDQUFpQnZDLElBQWpCLEVBQXVCO0FBQ3JCLFFBQUksQ0FBQ3NDLFFBQVEsQ0FBQ3RDLElBQUQsQ0FBYixFQUFxQjtBQUNuQnNDLE1BQUFBLFFBQVEsQ0FBQ3RDLElBQUQsQ0FBUixHQUFpQixJQUFJbk8sSUFBSSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLEVBQWlDO0FBQ2hEcEIsUUFBQUEsTUFBTSxFQUFFLEtBRHdDO0FBRWhEMkYsUUFBQUEsUUFBUSxFQUFFMkosSUFGc0M7QUFHaER0USxRQUFBQSxJQUFJLEVBQUUsU0FIMEM7QUFJaERDLFFBQUFBLEtBQUssRUFBRSxTQUp5QztBQUtoREMsUUFBQUEsR0FBRyxFQUFFLFNBTDJDO0FBTWhETSxRQUFBQSxJQUFJLEVBQUUsU0FOMEM7QUFPaERDLFFBQUFBLE1BQU0sRUFBRSxTQVB3QztBQVFoREUsUUFBQUEsTUFBTSxFQUFFO0FBUndDLE9BQWpDLENBQWpCO0FBVUQ7O0FBRUQsV0FBT2lTLFFBQVEsQ0FBQ3RDLElBQUQsQ0FBZjtBQUNEOztBQUVELE1BQUl3QyxTQUFTLEdBQUc7QUFDZDlTLElBQUFBLElBQUksRUFBRSxDQURRO0FBRWRDLElBQUFBLEtBQUssRUFBRSxDQUZPO0FBR2RDLElBQUFBLEdBQUcsRUFBRSxDQUhTO0FBSWRNLElBQUFBLElBQUksRUFBRSxDQUpRO0FBS2RDLElBQUFBLE1BQU0sRUFBRSxDQUxNO0FBTWRFLElBQUFBLE1BQU0sRUFBRTtBQU5NLEdBQWhCOztBQVNBLFdBQVNvUyxXQUFULENBQXFCQyxHQUFyQixFQUEwQnBNLElBQTFCLEVBQWdDO0FBQzlCLFFBQUlxTSxTQUFTLEdBQUdELEdBQUcsQ0FBQ3pMLE1BQUosQ0FBV1gsSUFBWCxFQUFpQmdCLE9BQWpCLENBQXlCLFNBQXpCLEVBQW9DLEVBQXBDLENBQWhCO0FBQUEsUUFDSVgsTUFBTSxHQUFHLDBDQUEwQ2lNLElBQTFDLENBQStDRCxTQUEvQyxDQURiO0FBQUEsUUFFSUUsTUFBTSxHQUFHbE0sTUFBTSxDQUFDLENBQUQsQ0FGbkI7QUFBQSxRQUdJbU0sSUFBSSxHQUFHbk0sTUFBTSxDQUFDLENBQUQsQ0FIakI7QUFBQSxRQUlJb00sS0FBSyxHQUFHcE0sTUFBTSxDQUFDLENBQUQsQ0FKbEI7QUFBQSxRQUtJcU0sS0FBSyxHQUFHck0sTUFBTSxDQUFDLENBQUQsQ0FMbEI7QUFBQSxRQU1Jc00sT0FBTyxHQUFHdE0sTUFBTSxDQUFDLENBQUQsQ0FOcEI7QUFBQSxRQU9JdU0sT0FBTyxHQUFHdk0sTUFBTSxDQUFDLENBQUQsQ0FQcEI7QUFRQSxXQUFPLENBQUNvTSxLQUFELEVBQVFGLE1BQVIsRUFBZ0JDLElBQWhCLEVBQXNCRSxLQUF0QixFQUE2QkMsT0FBN0IsRUFBc0NDLE9BQXRDLENBQVA7QUFDRDs7QUFFRCxXQUFTQyxXQUFULENBQXFCVCxHQUFyQixFQUEwQnBNLElBQTFCLEVBQWdDO0FBQzlCLFFBQUlxTSxTQUFTLEdBQUdELEdBQUcsQ0FBQzFRLGFBQUosQ0FBa0JzRSxJQUFsQixDQUFoQjtBQUFBLFFBQ0k4TSxNQUFNLEdBQUcsRUFEYjs7QUFHQSxTQUFLLElBQUk3WSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHb1ksU0FBUyxDQUFDblksTUFBOUIsRUFBc0NELENBQUMsRUFBdkMsRUFBMkM7QUFDekMsVUFBSThZLFlBQVksR0FBR1YsU0FBUyxDQUFDcFksQ0FBRCxDQUE1QjtBQUFBLFVBQ0l1TSxJQUFJLEdBQUd1TSxZQUFZLENBQUN2TSxJQUR4QjtBQUFBLFVBRUk3SSxLQUFLLEdBQUdvVixZQUFZLENBQUNwVixLQUZ6QjtBQUFBLFVBR0lxVixHQUFHLEdBQUdkLFNBQVMsQ0FBQzFMLElBQUQsQ0FIbkI7O0FBS0EsVUFBSSxDQUFDdkYsV0FBVyxDQUFDK1IsR0FBRCxDQUFoQixFQUF1QjtBQUNyQkYsUUFBQUEsTUFBTSxDQUFDRSxHQUFELENBQU4sR0FBY3BQLFFBQVEsQ0FBQ2pHLEtBQUQsRUFBUSxFQUFSLENBQXRCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFPbVYsTUFBUDtBQUNEOztBQUVELE1BQUlHLGFBQWEsR0FBRyxFQUFwQjtBQUNBOzs7OztBQUtBLE1BQUlDLFFBQVE7QUFDWjtBQUNBLFlBQVV4QixLQUFWLEVBQWlCO0FBQ2YzVyxJQUFBQSxjQUFjLENBQUNtWSxRQUFELEVBQVd4QixLQUFYLENBQWQ7QUFFQTs7Ozs7O0FBSUF3QixJQUFBQSxRQUFRLENBQUNoWSxNQUFULEdBQWtCLFNBQVNBLE1BQVQsQ0FBZ0JpWSxJQUFoQixFQUFzQjtBQUN0QyxVQUFJLENBQUNGLGFBQWEsQ0FBQ0UsSUFBRCxDQUFsQixFQUEwQjtBQUN4QkYsUUFBQUEsYUFBYSxDQUFDRSxJQUFELENBQWIsR0FBc0IsSUFBSUQsUUFBSixDQUFhQyxJQUFiLENBQXRCO0FBQ0Q7O0FBRUQsYUFBT0YsYUFBYSxDQUFDRSxJQUFELENBQXBCO0FBQ0Q7QUFDRDs7OztBQVBBOztBQWFBRCxJQUFBQSxRQUFRLENBQUNFLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxHQUFzQjtBQUMxQ0gsTUFBQUEsYUFBYSxHQUFHLEVBQWhCO0FBQ0FqQixNQUFBQSxRQUFRLEdBQUcsRUFBWDtBQUNEO0FBQ0Q7Ozs7Ozs7O0FBSkE7O0FBY0FrQixJQUFBQSxRQUFRLENBQUNHLGdCQUFULEdBQTRCLFNBQVNBLGdCQUFULENBQTBCcFUsQ0FBMUIsRUFBNkI7QUFDdkQsYUFBTyxDQUFDLEVBQUVBLENBQUMsSUFBSUEsQ0FBQyxDQUFDcVUsS0FBRixDQUFRekIsYUFBUixDQUFQLENBQVI7QUFDRDtBQUNEOzs7Ozs7OztBQUhBOztBQWFBcUIsSUFBQUEsUUFBUSxDQUFDSyxXQUFULEdBQXVCLFNBQVNBLFdBQVQsQ0FBcUI3RCxJQUFyQixFQUEyQjtBQUNoRCxVQUFJO0FBQ0YsWUFBSW5PLElBQUksQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixFQUFpQztBQUMvQnVFLFVBQUFBLFFBQVEsRUFBRTJKO0FBRHFCLFNBQWpDLEVBRUcvSSxNQUZIO0FBR0EsZUFBTyxJQUFQO0FBQ0QsT0FMRCxDQUtFLE9BQU94SyxDQUFQLEVBQVU7QUFDVixlQUFPLEtBQVA7QUFDRDtBQUNGLEtBVEQsQ0FTRTs7QUFFRjtBQVhBOztBQWNBK1csSUFBQUEsUUFBUSxDQUFDTSxjQUFULEdBQTBCLFNBQVNBLGNBQVQsQ0FBd0JDLFNBQXhCLEVBQW1DO0FBQzNELFVBQUlBLFNBQUosRUFBZTtBQUNiLFlBQUlILEtBQUssR0FBR0csU0FBUyxDQUFDSCxLQUFWLENBQWdCLDBCQUFoQixDQUFaOztBQUVBLFlBQUlBLEtBQUosRUFBVztBQUNULGlCQUFPLENBQUMsRUFBRCxHQUFNMVAsUUFBUSxDQUFDMFAsS0FBSyxDQUFDLENBQUQsQ0FBTixDQUFyQjtBQUNEO0FBQ0Y7O0FBRUQsYUFBTyxJQUFQO0FBQ0QsS0FWRDs7QUFZQSxhQUFTSixRQUFULENBQWtCQyxJQUFsQixFQUF3QjtBQUN0QixVQUFJbEUsS0FBSjs7QUFFQUEsTUFBQUEsS0FBSyxHQUFHeUMsS0FBSyxDQUFDeFYsSUFBTixDQUFXLElBQVgsS0FBb0IsSUFBNUI7QUFDQTs7QUFFQStTLE1BQUFBLEtBQUssQ0FBQ2UsUUFBTixHQUFpQm1ELElBQWpCO0FBQ0E7O0FBRUFsRSxNQUFBQSxLQUFLLENBQUN5RSxLQUFOLEdBQWNSLFFBQVEsQ0FBQ0ssV0FBVCxDQUFxQkosSUFBckIsQ0FBZDtBQUNBLGFBQU9sRSxLQUFQO0FBQ0Q7QUFDRDs7O0FBR0EsUUFBSWIsTUFBTSxHQUFHOEUsUUFBUSxDQUFDcFksU0FBdEI7QUFFQTs7QUFDQXNULElBQUFBLE1BQU0sQ0FBQzJCLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQm5LLEVBQXBCLEVBQXdCa0csSUFBeEIsRUFBOEI7QUFDaEQsVUFBSW5GLE1BQU0sR0FBR21GLElBQUksQ0FBQ25GLE1BQWxCO0FBQUEsVUFDSWIsTUFBTSxHQUFHZ0csSUFBSSxDQUFDaEcsTUFEbEI7QUFFQSxhQUFPSCxhQUFhLENBQUNDLEVBQUQsRUFBS2UsTUFBTCxFQUFhYixNQUFiLEVBQXFCLEtBQUtxTixJQUExQixDQUFwQjtBQUNEO0FBQ0Q7QUFMQTs7QUFRQS9FLElBQUFBLE1BQU0sQ0FBQ2xHLFlBQVAsR0FBc0IsU0FBU3lKLGNBQVQsQ0FBd0IvTCxFQUF4QixFQUE0QmUsTUFBNUIsRUFBb0M7QUFDeEQsYUFBT3VCLFlBQVksQ0FBQyxLQUFLQyxNQUFMLENBQVl2QyxFQUFaLENBQUQsRUFBa0JlLE1BQWxCLENBQW5CO0FBQ0Q7QUFDRDtBQUhBOztBQU1BeUgsSUFBQUEsTUFBTSxDQUFDakcsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCdkMsRUFBaEIsRUFBb0I7QUFDbEMsVUFBSUksSUFBSSxHQUFHLElBQUloSyxJQUFKLENBQVM0SixFQUFULENBQVg7QUFBQSxVQUNJd00sR0FBRyxHQUFHSCxPQUFPLENBQUMsS0FBS2tCLElBQU4sQ0FEakI7QUFBQSxVQUVJdEMsS0FBSyxHQUFHdUIsR0FBRyxDQUFDMVEsYUFBSixHQUFvQm1SLFdBQVcsQ0FBQ1QsR0FBRCxFQUFNcE0sSUFBTixDQUEvQixHQUE2Q21NLFdBQVcsQ0FBQ0MsR0FBRCxFQUFNcE0sSUFBTixDQUZwRTtBQUFBLFVBR0k1RyxJQUFJLEdBQUd5UixLQUFLLENBQUMsQ0FBRCxDQUhoQjtBQUFBLFVBSUl4UixLQUFLLEdBQUd3UixLQUFLLENBQUMsQ0FBRCxDQUpqQjtBQUFBLFVBS0l2UixHQUFHLEdBQUd1UixLQUFLLENBQUMsQ0FBRCxDQUxmO0FBQUEsVUFNSWpSLElBQUksR0FBR2lSLEtBQUssQ0FBQyxDQUFELENBTmhCO0FBQUEsVUFPSWhSLE1BQU0sR0FBR2dSLEtBQUssQ0FBQyxDQUFELENBUGxCO0FBQUEsVUFRSTlRLE1BQU0sR0FBRzhRLEtBQUssQ0FBQyxDQUFELENBUmxCO0FBQUEsVUFTSThDLFlBQVksR0FBRy9ULElBQUksS0FBSyxFQUFULEdBQWMsQ0FBZCxHQUFrQkEsSUFUckM7O0FBV0EsVUFBSWdVLEtBQUssR0FBRzdPLFlBQVksQ0FBQztBQUN2QjNGLFFBQUFBLElBQUksRUFBRUEsSUFEaUI7QUFFdkJDLFFBQUFBLEtBQUssRUFBRUEsS0FGZ0I7QUFHdkJDLFFBQUFBLEdBQUcsRUFBRUEsR0FIa0I7QUFJdkJNLFFBQUFBLElBQUksRUFBRStULFlBSmlCO0FBS3ZCOVQsUUFBQUEsTUFBTSxFQUFFQSxNQUxlO0FBTXZCRSxRQUFBQSxNQUFNLEVBQUVBLE1BTmU7QUFPdkJtRixRQUFBQSxXQUFXLEVBQUU7QUFQVSxPQUFELENBQXhCO0FBU0EsVUFBSTJPLElBQUksR0FBRzdOLElBQUksQ0FBQzhOLE9BQUwsRUFBWDtBQUNBRCxNQUFBQSxJQUFJLElBQUlBLElBQUksR0FBRyxJQUFmO0FBQ0EsYUFBTyxDQUFDRCxLQUFLLEdBQUdDLElBQVQsS0FBa0IsS0FBSyxJQUF2QixDQUFQO0FBQ0Q7QUFDRDtBQXpCQTs7QUE0QkF6RixJQUFBQSxNQUFNLENBQUNrRCxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JDLFNBQWhCLEVBQTJCO0FBQ3pDLGFBQU9BLFNBQVMsQ0FBQy9LLElBQVYsS0FBbUIsTUFBbkIsSUFBNkIrSyxTQUFTLENBQUM0QixJQUFWLEtBQW1CLEtBQUtBLElBQTVEO0FBQ0Q7QUFDRDtBQUhBOztBQU1BelksSUFBQUEsWUFBWSxDQUFDd1ksUUFBRCxFQUFXLENBQUM7QUFDdEJ6WSxNQUFBQSxHQUFHLEVBQUUsTUFEaUI7QUFFdEIrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sTUFBUDtBQUNEO0FBQ0Q7O0FBTHNCLEtBQUQsRUFPcEI7QUFDRC9DLE1BQUFBLEdBQUcsRUFBRSxNQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS3dTLFFBQVo7QUFDRDtBQUNEOztBQUxDLEtBUG9CLEVBY3BCO0FBQ0R2VixNQUFBQSxHQUFHLEVBQUUsV0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQVA7QUFDRDtBQUpBLEtBZG9CLEVBbUJwQjtBQUNEL0MsTUFBQUEsR0FBRyxFQUFFLFNBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLa1csS0FBWjtBQUNEO0FBSkEsS0FuQm9CLENBQVgsQ0FBWjs7QUEwQkEsV0FBT1IsUUFBUDtBQUNELEdBdEtELENBc0tFN0IsSUF0S0YsQ0FGQTs7QUEwS0EsTUFBSTBDLFdBQVcsR0FBRyxJQUFsQjtBQUNBOzs7OztBQUtBLE1BQUlDLGVBQWU7QUFDbkI7QUFDQSxZQUFVdEMsS0FBVixFQUFpQjtBQUNmM1csSUFBQUEsY0FBYyxDQUFDaVosZUFBRCxFQUFrQnRDLEtBQWxCLENBQWQ7QUFFQTs7Ozs7OztBQUtBc0MsSUFBQUEsZUFBZSxDQUFDblgsUUFBaEIsR0FBMkIsU0FBU0EsUUFBVCxDQUFrQnNMLE1BQWxCLEVBQTBCO0FBQ25ELGFBQU9BLE1BQU0sS0FBSyxDQUFYLEdBQWU2TCxlQUFlLENBQUNDLFdBQS9CLEdBQTZDLElBQUlELGVBQUosQ0FBb0I3TCxNQUFwQixDQUFwRDtBQUNEO0FBQ0Q7Ozs7Ozs7O0FBSEE7O0FBYUE2TCxJQUFBQSxlQUFlLENBQUNFLGNBQWhCLEdBQWlDLFNBQVNBLGNBQVQsQ0FBd0JqVixDQUF4QixFQUEyQjtBQUMxRCxVQUFJQSxDQUFKLEVBQU87QUFDTCxZQUFJa1YsQ0FBQyxHQUFHbFYsQ0FBQyxDQUFDcVUsS0FBRixDQUFRLHVDQUFSLENBQVI7O0FBRUEsWUFBSWEsQ0FBSixFQUFPO0FBQ0wsaUJBQU8sSUFBSUgsZUFBSixDQUFvQi9NLFlBQVksQ0FBQ2tOLENBQUMsQ0FBQyxDQUFELENBQUYsRUFBT0EsQ0FBQyxDQUFDLENBQUQsQ0FBUixDQUFoQyxDQUFQO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPLElBQVA7QUFDRCxLQVZEOztBQVlBelosSUFBQUEsWUFBWSxDQUFDc1osZUFBRCxFQUFrQixJQUFsQixFQUF3QixDQUFDO0FBQ25DdlosTUFBQUEsR0FBRyxFQUFFLGFBRDhCOztBQUduQzs7OztBQUlBK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixZQUFJdVcsV0FBVyxLQUFLLElBQXBCLEVBQTBCO0FBQ3hCQSxVQUFBQSxXQUFXLEdBQUcsSUFBSUMsZUFBSixDQUFvQixDQUFwQixDQUFkO0FBQ0Q7O0FBRUQsZUFBT0QsV0FBUDtBQUNEO0FBYmtDLEtBQUQsQ0FBeEIsQ0FBWjs7QUFnQkEsYUFBU0MsZUFBVCxDQUF5QjdMLE1BQXpCLEVBQWlDO0FBQy9CLFVBQUk4RyxLQUFKOztBQUVBQSxNQUFBQSxLQUFLLEdBQUd5QyxLQUFLLENBQUN4VixJQUFOLENBQVcsSUFBWCxLQUFvQixJQUE1QjtBQUNBOztBQUVBK1MsTUFBQUEsS0FBSyxDQUFDbUYsS0FBTixHQUFjak0sTUFBZDtBQUNBLGFBQU84RyxLQUFQO0FBQ0Q7QUFDRDs7O0FBR0EsUUFBSWIsTUFBTSxHQUFHNEYsZUFBZSxDQUFDbFosU0FBN0I7QUFFQTs7QUFDQXNULElBQUFBLE1BQU0sQ0FBQzJCLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxHQUFzQjtBQUN4QyxhQUFPLEtBQUtvRCxJQUFaO0FBQ0Q7QUFDRDtBQUhBOztBQU1BL0UsSUFBQUEsTUFBTSxDQUFDbEcsWUFBUCxHQUFzQixTQUFTeUosY0FBVCxDQUF3Qi9MLEVBQXhCLEVBQTRCZSxNQUE1QixFQUFvQztBQUN4RCxhQUFPdUIsWUFBWSxDQUFDLEtBQUtrTSxLQUFOLEVBQWF6TixNQUFiLENBQW5CO0FBQ0Q7QUFDRDtBQUhBO0FBTUE7OztBQUNBeUgsSUFBQUEsTUFBTSxDQUFDakcsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLGFBQU8sS0FBS2lNLEtBQVo7QUFDRDtBQUNEO0FBSEE7O0FBTUFoRyxJQUFBQSxNQUFNLENBQUNrRCxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JDLFNBQWhCLEVBQTJCO0FBQ3pDLGFBQU9BLFNBQVMsQ0FBQy9LLElBQVYsS0FBbUIsT0FBbkIsSUFBOEIrSyxTQUFTLENBQUM2QyxLQUFWLEtBQW9CLEtBQUtBLEtBQTlEO0FBQ0Q7QUFDRDtBQUhBOztBQU1BMVosSUFBQUEsWUFBWSxDQUFDc1osZUFBRCxFQUFrQixDQUFDO0FBQzdCdlosTUFBQUEsR0FBRyxFQUFFLE1BRHdCO0FBRTdCK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLE9BQVA7QUFDRDtBQUNEOztBQUw2QixLQUFELEVBTzNCO0FBQ0QvQyxNQUFBQSxHQUFHLEVBQUUsTUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUs0VyxLQUFMLEtBQWUsQ0FBZixHQUFtQixLQUFuQixHQUEyQixRQUFRbE0sWUFBWSxDQUFDLEtBQUtrTSxLQUFOLEVBQWEsUUFBYixDQUF0RDtBQUNEO0FBSkEsS0FQMkIsRUFZM0I7QUFDRDNaLE1BQUFBLEdBQUcsRUFBRSxXQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sSUFBUDtBQUNEO0FBSkEsS0FaMkIsRUFpQjNCO0FBQ0QvQyxNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLElBQVA7QUFDRDtBQUpBLEtBakIyQixDQUFsQixDQUFaOztBQXdCQSxXQUFPd1csZUFBUDtBQUNELEdBbEhELENBa0hFM0MsSUFsSEYsQ0FGQTtBQXNIQTs7Ozs7O0FBS0EsTUFBSWdELFdBQVc7QUFDZjtBQUNBLFlBQVUzQyxLQUFWLEVBQWlCO0FBQ2YzVyxJQUFBQSxjQUFjLENBQUNzWixXQUFELEVBQWMzQyxLQUFkLENBQWQ7O0FBRUEsYUFBUzJDLFdBQVQsQ0FBcUJyRSxRQUFyQixFQUErQjtBQUM3QixVQUFJZixLQUFKOztBQUVBQSxNQUFBQSxLQUFLLEdBQUd5QyxLQUFLLENBQUN4VixJQUFOLENBQVcsSUFBWCxLQUFvQixJQUE1QjtBQUNBOztBQUVBK1MsTUFBQUEsS0FBSyxDQUFDZSxRQUFOLEdBQWlCQSxRQUFqQjtBQUNBLGFBQU9mLEtBQVA7QUFDRDtBQUNEOzs7QUFHQSxRQUFJYixNQUFNLEdBQUdpRyxXQUFXLENBQUN2WixTQUF6QjtBQUVBOztBQUNBc1QsSUFBQUEsTUFBTSxDQUFDMkIsVUFBUCxHQUFvQixTQUFTQSxVQUFULEdBQXNCO0FBQ3hDLGFBQU8sSUFBUDtBQUNEO0FBQ0Q7QUFIQTs7QUFNQTNCLElBQUFBLE1BQU0sQ0FBQ2xHLFlBQVAsR0FBc0IsU0FBU0EsWUFBVCxHQUF3QjtBQUM1QyxhQUFPLEVBQVA7QUFDRDtBQUNEO0FBSEE7O0FBTUFrRyxJQUFBQSxNQUFNLENBQUNqRyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsR0FBa0I7QUFDaEMsYUFBT21NLEdBQVA7QUFDRDtBQUNEO0FBSEE7O0FBTUFsRyxJQUFBQSxNQUFNLENBQUNrRCxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsR0FBa0I7QUFDaEMsYUFBTyxLQUFQO0FBQ0Q7QUFDRDtBQUhBOztBQU1BNVcsSUFBQUEsWUFBWSxDQUFDMlosV0FBRCxFQUFjLENBQUM7QUFDekI1WixNQUFBQSxHQUFHLEVBQUUsTUFEb0I7QUFFekIrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sU0FBUDtBQUNEO0FBQ0Q7O0FBTHlCLEtBQUQsRUFPdkI7QUFDRC9DLE1BQUFBLEdBQUcsRUFBRSxNQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS3dTLFFBQVo7QUFDRDtBQUNEOztBQUxDLEtBUHVCLEVBY3ZCO0FBQ0R2VixNQUFBQSxHQUFHLEVBQUUsV0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQVA7QUFDRDtBQUpBLEtBZHVCLEVBbUJ2QjtBQUNEL0MsTUFBQUEsR0FBRyxFQUFFLFNBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFQO0FBQ0Q7QUFKQSxLQW5CdUIsQ0FBZCxDQUFaOztBQTBCQSxXQUFPNlcsV0FBUDtBQUNELEdBckVELENBcUVFaEQsSUFyRUYsQ0FGQTtBQXlFQTs7Ozs7QUFHQSxXQUFTa0QsYUFBVCxDQUF1QmhSLEtBQXZCLEVBQThCaVIsV0FBOUIsRUFBMkM7QUFDekMsUUFBSXJNLE1BQUo7O0FBRUEsUUFBSWxILFdBQVcsQ0FBQ3NDLEtBQUQsQ0FBWCxJQUFzQkEsS0FBSyxLQUFLLElBQXBDLEVBQTBDO0FBQ3hDLGFBQU9pUixXQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUlqUixLQUFLLFlBQVk4TixJQUFyQixFQUEyQjtBQUNoQyxhQUFPOU4sS0FBUDtBQUNELEtBRk0sTUFFQSxJQUFJbkMsUUFBUSxDQUFDbUMsS0FBRCxDQUFaLEVBQXFCO0FBQzFCLFVBQUlrUixPQUFPLEdBQUdsUixLQUFLLENBQUNrRCxXQUFOLEVBQWQ7QUFDQSxVQUFJZ08sT0FBTyxLQUFLLE9BQWhCLEVBQXlCLE9BQU9ELFdBQVAsQ0FBekIsS0FBaUQsSUFBSUMsT0FBTyxLQUFLLEtBQVosSUFBcUJBLE9BQU8sS0FBSyxLQUFyQyxFQUE0QyxPQUFPVCxlQUFlLENBQUNDLFdBQXZCLENBQTVDLEtBQW9GLElBQUksQ0FBQzlMLE1BQU0sR0FBRytLLFFBQVEsQ0FBQ00sY0FBVCxDQUF3QmpRLEtBQXhCLENBQVYsS0FBNkMsSUFBakQsRUFBdUQ7QUFDMUw7QUFDQSxlQUFPeVEsZUFBZSxDQUFDblgsUUFBaEIsQ0FBeUJzTCxNQUF6QixDQUFQO0FBQ0QsT0FIb0ksTUFHOUgsSUFBSStLLFFBQVEsQ0FBQ0csZ0JBQVQsQ0FBMEJvQixPQUExQixDQUFKLEVBQXdDLE9BQU92QixRQUFRLENBQUNoWSxNQUFULENBQWdCcUksS0FBaEIsQ0FBUCxDQUF4QyxLQUEyRSxPQUFPeVEsZUFBZSxDQUFDRSxjQUFoQixDQUErQk8sT0FBL0IsS0FBMkMsSUFBSUosV0FBSixDQUFnQjlRLEtBQWhCLENBQWxEO0FBQ25GLEtBTk0sTUFNQSxJQUFJckMsUUFBUSxDQUFDcUMsS0FBRCxDQUFaLEVBQXFCO0FBQzFCLGFBQU95USxlQUFlLENBQUNuWCxRQUFoQixDQUF5QjBHLEtBQXpCLENBQVA7QUFDRCxLQUZNLE1BRUEsSUFBSSxRQUFPQSxLQUFQLE1BQWlCLFFBQWpCLElBQTZCQSxLQUFLLENBQUM0RSxNQUFuQyxJQUE2QyxPQUFPNUUsS0FBSyxDQUFDNEUsTUFBYixLQUF3QixRQUF6RSxFQUFtRjtBQUN4RjtBQUNBO0FBQ0EsYUFBTzVFLEtBQVA7QUFDRCxLQUpNLE1BSUE7QUFDTCxhQUFPLElBQUk4USxXQUFKLENBQWdCOVEsS0FBaEIsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsTUFBSW1SLEdBQUcsR0FBRyxTQUFTQSxHQUFULEdBQWU7QUFDdkIsV0FBTzFZLElBQUksQ0FBQzBZLEdBQUwsRUFBUDtBQUNELEdBRkQ7QUFBQSxNQUdJRixXQUFXLEdBQUcsSUFIbEI7QUFBQSxNQUlJO0FBQ0pHLEVBQUFBLGFBQWEsR0FBRyxJQUxoQjtBQUFBLE1BTUlDLHNCQUFzQixHQUFHLElBTjdCO0FBQUEsTUFPSUMscUJBQXFCLEdBQUcsSUFQNUI7QUFBQSxNQVFJQyxjQUFjLEdBQUcsS0FSckI7QUFTQTs7Ozs7QUFLQSxNQUFJQyxRQUFRO0FBQ1o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsUUFBVCxHQUFvQixDQUFFO0FBRXRCOzs7Ozs7QUFJQUEsSUFBQUEsUUFBUSxDQUFDQyxXQUFULEdBQXVCLFNBQVNBLFdBQVQsR0FBdUI7QUFDNUNDLE1BQUFBLE1BQU0sQ0FBQzdCLFVBQVA7QUFDQUYsTUFBQUEsUUFBUSxDQUFDRSxVQUFUO0FBQ0QsS0FIRDs7QUFLQTFZLElBQUFBLFlBQVksQ0FBQ3FhLFFBQUQsRUFBVyxJQUFYLEVBQWlCLENBQUM7QUFDNUJ0YSxNQUFBQSxHQUFHLEVBQUUsS0FEdUI7O0FBRzVCOzs7O0FBSUErQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9rWCxHQUFQO0FBQ0Q7QUFDRDs7Ozs7OztBQVY0QjtBQWtCNUJqWCxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxDQUFhdUIsQ0FBYixFQUFnQjtBQUNuQjBWLFFBQUFBLEdBQUcsR0FBRzFWLENBQU47QUFDRDtBQUNEOzs7OztBQXJCNEIsS0FBRCxFQTBCMUI7QUFDRHZFLE1BQUFBLEdBQUcsRUFBRSxpQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPdVgsUUFBUSxDQUFDUCxXQUFULENBQXFCckIsSUFBNUI7QUFDRDtBQUNEOzs7O0FBTEM7QUFVRDFWLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULENBQWF5WCxDQUFiLEVBQWdCO0FBQ25CLFlBQUksQ0FBQ0EsQ0FBTCxFQUFRO0FBQ05WLFVBQUFBLFdBQVcsR0FBRyxJQUFkO0FBQ0QsU0FGRCxNQUVPO0FBQ0xBLFVBQUFBLFdBQVcsR0FBR0QsYUFBYSxDQUFDVyxDQUFELENBQTNCO0FBQ0Q7QUFDRjtBQUNEOzs7OztBQWpCQyxLQTFCMEIsRUFnRDFCO0FBQ0R6YSxNQUFBQSxHQUFHLEVBQUUsYUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPZ1gsV0FBVyxJQUFJL0MsU0FBUyxDQUFDNVUsUUFBaEM7QUFDRDtBQUNEOzs7OztBQUxDLEtBaEQwQixFQTBEMUI7QUFDRHBDLE1BQUFBLEdBQUcsRUFBRSxlQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9tWCxhQUFQO0FBQ0Q7QUFDRDs7OztBQUxDO0FBVURsWCxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxDQUFhcUksTUFBYixFQUFxQjtBQUN4QjZPLFFBQUFBLGFBQWEsR0FBRzdPLE1BQWhCO0FBQ0Q7QUFDRDs7Ozs7QUFiQyxLQTFEMEIsRUE0RTFCO0FBQ0RyTCxNQUFBQSxHQUFHLEVBQUUsd0JBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT29YLHNCQUFQO0FBQ0Q7QUFDRDs7OztBQUxDO0FBVURuWCxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxDQUFhMFgsZUFBYixFQUE4QjtBQUNqQ1AsUUFBQUEsc0JBQXNCLEdBQUdPLGVBQXpCO0FBQ0Q7QUFDRDs7Ozs7QUFiQyxLQTVFMEIsRUE4RjFCO0FBQ0QxYSxNQUFBQSxHQUFHLEVBQUUsdUJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3FYLHFCQUFQO0FBQ0Q7QUFDRDs7OztBQUxDO0FBVURwWCxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxDQUFhNFIsY0FBYixFQUE2QjtBQUNoQ3dGLFFBQUFBLHFCQUFxQixHQUFHeEYsY0FBeEI7QUFDRDtBQUNEOzs7OztBQWJDLEtBOUYwQixFQWdIMUI7QUFDRDVVLE1BQUFBLEdBQUcsRUFBRSxnQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPc1gsY0FBUDtBQUNEO0FBQ0Q7Ozs7QUFMQztBQVVEclgsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsQ0FBYStPLENBQWIsRUFBZ0I7QUFDbkJzSSxRQUFBQSxjQUFjLEdBQUd0SSxDQUFqQjtBQUNEO0FBWkEsS0FoSDBCLENBQWpCLENBQVo7O0FBK0hBLFdBQU91SSxRQUFQO0FBQ0QsR0E1SUQsRUFGQTs7QUFnSkEsTUFBSUssV0FBVyxHQUFHLEVBQWxCOztBQUVBLFdBQVNDLFlBQVQsQ0FBc0JDLFNBQXRCLEVBQWlDOUgsSUFBakMsRUFBdUM7QUFDckMsUUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLE1BQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsUUFBSS9TLEdBQUcsR0FBR29PLElBQUksQ0FBQ0QsU0FBTCxDQUFlLENBQUMwTSxTQUFELEVBQVk5SCxJQUFaLENBQWYsQ0FBVjtBQUNBLFFBQUk0RSxHQUFHLEdBQUdnRCxXQUFXLENBQUMzYSxHQUFELENBQXJCOztBQUVBLFFBQUksQ0FBQzJYLEdBQUwsRUFBVTtBQUNSQSxNQUFBQSxHQUFHLEdBQUcsSUFBSTdRLElBQUksQ0FBQ0MsY0FBVCxDQUF3QjhULFNBQXhCLEVBQW1DOUgsSUFBbkMsQ0FBTjtBQUNBNEgsTUFBQUEsV0FBVyxDQUFDM2EsR0FBRCxDQUFYLEdBQW1CMlgsR0FBbkI7QUFDRDs7QUFFRCxXQUFPQSxHQUFQO0FBQ0Q7O0FBRUQsTUFBSW1ELFlBQVksR0FBRyxFQUFuQjs7QUFFQSxXQUFTQyxZQUFULENBQXNCRixTQUF0QixFQUFpQzlILElBQWpDLEVBQXVDO0FBQ3JDLFFBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxNQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELFFBQUkvUyxHQUFHLEdBQUdvTyxJQUFJLENBQUNELFNBQUwsQ0FBZSxDQUFDME0sU0FBRCxFQUFZOUgsSUFBWixDQUFmLENBQVY7QUFDQSxRQUFJaUksR0FBRyxHQUFHRixZQUFZLENBQUM5YSxHQUFELENBQXRCOztBQUVBLFFBQUksQ0FBQ2diLEdBQUwsRUFBVTtBQUNSQSxNQUFBQSxHQUFHLEdBQUcsSUFBSWxVLElBQUksQ0FBQ21VLFlBQVQsQ0FBc0JKLFNBQXRCLEVBQWlDOUgsSUFBakMsQ0FBTjtBQUNBK0gsTUFBQUEsWUFBWSxDQUFDOWEsR0FBRCxDQUFaLEdBQW9CZ2IsR0FBcEI7QUFDRDs7QUFFRCxXQUFPQSxHQUFQO0FBQ0Q7O0FBRUQsTUFBSUUsWUFBWSxHQUFHLEVBQW5COztBQUVBLFdBQVNDLFlBQVQsQ0FBc0JOLFNBQXRCLEVBQWlDOUgsSUFBakMsRUFBdUM7QUFDckMsUUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLE1BQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsUUFBSS9TLEdBQUcsR0FBR29PLElBQUksQ0FBQ0QsU0FBTCxDQUFlLENBQUMwTSxTQUFELEVBQVk5SCxJQUFaLENBQWYsQ0FBVjtBQUNBLFFBQUlpSSxHQUFHLEdBQUdFLFlBQVksQ0FBQ2xiLEdBQUQsQ0FBdEI7O0FBRUEsUUFBSSxDQUFDZ2IsR0FBTCxFQUFVO0FBQ1JBLE1BQUFBLEdBQUcsR0FBRyxJQUFJbFUsSUFBSSxDQUFDSyxrQkFBVCxDQUE0QjBULFNBQTVCLEVBQXVDOUgsSUFBdkMsQ0FBTjtBQUNBbUksTUFBQUEsWUFBWSxDQUFDbGIsR0FBRCxDQUFaLEdBQW9CZ2IsR0FBcEI7QUFDRDs7QUFFRCxXQUFPQSxHQUFQO0FBQ0Q7O0FBRUQsTUFBSUksY0FBYyxHQUFHLElBQXJCOztBQUVBLFdBQVNDLFlBQVQsR0FBd0I7QUFDdEIsUUFBSUQsY0FBSixFQUFvQjtBQUNsQixhQUFPQSxjQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUl2VSxPQUFPLEVBQVgsRUFBZTtBQUNwQixVQUFJeVUsV0FBVyxHQUFHLElBQUl4VSxJQUFJLENBQUNDLGNBQVQsR0FBMEJtTixlQUExQixHQUE0QzdJLE1BQTlELENBRG9CLENBQ2tEOztBQUV0RStQLE1BQUFBLGNBQWMsR0FBRyxDQUFDRSxXQUFELElBQWdCQSxXQUFXLEtBQUssS0FBaEMsR0FBd0MsT0FBeEMsR0FBa0RBLFdBQW5FO0FBQ0EsYUFBT0YsY0FBUDtBQUNELEtBTE0sTUFLQTtBQUNMQSxNQUFBQSxjQUFjLEdBQUcsT0FBakI7QUFDQSxhQUFPQSxjQUFQO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTRyxpQkFBVCxDQUEyQkMsU0FBM0IsRUFBc0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBSUMsTUFBTSxHQUFHRCxTQUFTLENBQUNoWixPQUFWLENBQWtCLEtBQWxCLENBQWI7O0FBRUEsUUFBSWlaLE1BQU0sS0FBSyxDQUFDLENBQWhCLEVBQW1CO0FBQ2pCLGFBQU8sQ0FBQ0QsU0FBRCxDQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSUUsT0FBSjtBQUNBLFVBQUlDLE9BQU8sR0FBR0gsU0FBUyxDQUFDblAsU0FBVixDQUFvQixDQUFwQixFQUF1Qm9QLE1BQXZCLENBQWQ7O0FBRUEsVUFBSTtBQUNGQyxRQUFBQSxPQUFPLEdBQUdkLFlBQVksQ0FBQ1ksU0FBRCxDQUFaLENBQXdCdEgsZUFBeEIsRUFBVjtBQUNELE9BRkQsQ0FFRSxPQUFPeFMsQ0FBUCxFQUFVO0FBQ1ZnYSxRQUFBQSxPQUFPLEdBQUdkLFlBQVksQ0FBQ2UsT0FBRCxDQUFaLENBQXNCekgsZUFBdEIsRUFBVjtBQUNEOztBQUVELFVBQUkwSCxRQUFRLEdBQUdGLE9BQWY7QUFBQSxVQUNJaEIsZUFBZSxHQUFHa0IsUUFBUSxDQUFDbEIsZUFEL0I7QUFBQSxVQUVJbUIsUUFBUSxHQUFHRCxRQUFRLENBQUNDLFFBRnhCLENBVkssQ0FZNkI7O0FBRWxDLGFBQU8sQ0FBQ0YsT0FBRCxFQUFVakIsZUFBVixFQUEyQm1CLFFBQTNCLENBQVA7QUFDRDtBQUNGOztBQUVELFdBQVNDLGdCQUFULENBQTBCTixTQUExQixFQUFxQ2QsZUFBckMsRUFBc0Q5RixjQUF0RCxFQUFzRTtBQUNwRSxRQUFJL04sT0FBTyxFQUFYLEVBQWU7QUFDYixVQUFJK04sY0FBYyxJQUFJOEYsZUFBdEIsRUFBdUM7QUFDckNjLFFBQUFBLFNBQVMsSUFBSSxJQUFiOztBQUVBLFlBQUk1RyxjQUFKLEVBQW9CO0FBQ2xCNEcsVUFBQUEsU0FBUyxJQUFJLFNBQVM1RyxjQUF0QjtBQUNEOztBQUVELFlBQUk4RixlQUFKLEVBQXFCO0FBQ25CYyxVQUFBQSxTQUFTLElBQUksU0FBU2QsZUFBdEI7QUFDRDs7QUFFRCxlQUFPYyxTQUFQO0FBQ0QsT0FaRCxNQVlPO0FBQ0wsZUFBT0EsU0FBUDtBQUNEO0FBQ0YsS0FoQkQsTUFnQk87QUFDTCxhQUFPLEVBQVA7QUFDRDtBQUNGOztBQUVELFdBQVNPLFNBQVQsQ0FBbUJ6UyxDQUFuQixFQUFzQjtBQUNwQixRQUFJMFMsRUFBRSxHQUFHLEVBQVQ7O0FBRUEsU0FBSyxJQUFJeGMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsSUFBSSxFQUFyQixFQUF5QkEsQ0FBQyxFQUExQixFQUE4QjtBQUM1QixVQUFJNFAsRUFBRSxHQUFHNk0sUUFBUSxDQUFDQyxHQUFULENBQWEsSUFBYixFQUFtQjFjLENBQW5CLEVBQXNCLENBQXRCLENBQVQ7QUFDQXdjLE1BQUFBLEVBQUUsQ0FBQ2hhLElBQUgsQ0FBUXNILENBQUMsQ0FBQzhGLEVBQUQsQ0FBVDtBQUNEOztBQUVELFdBQU80TSxFQUFQO0FBQ0Q7O0FBRUQsV0FBU0csV0FBVCxDQUFxQjdTLENBQXJCLEVBQXdCO0FBQ3RCLFFBQUkwUyxFQUFFLEdBQUcsRUFBVDs7QUFFQSxTQUFLLElBQUl4YyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxJQUFJLENBQXJCLEVBQXdCQSxDQUFDLEVBQXpCLEVBQTZCO0FBQzNCLFVBQUk0UCxFQUFFLEdBQUc2TSxRQUFRLENBQUNDLEdBQVQsQ0FBYSxJQUFiLEVBQW1CLEVBQW5CLEVBQXVCLEtBQUsxYyxDQUE1QixDQUFUO0FBQ0F3YyxNQUFBQSxFQUFFLENBQUNoYSxJQUFILENBQVFzSCxDQUFDLENBQUM4RixFQUFELENBQVQ7QUFDRDs7QUFFRCxXQUFPNE0sRUFBUDtBQUNEOztBQUVELFdBQVNJLFNBQVQsQ0FBbUIzSSxHQUFuQixFQUF3QmhVLE1BQXhCLEVBQWdDNGMsU0FBaEMsRUFBMkNDLFNBQTNDLEVBQXNEQyxNQUF0RCxFQUE4RDtBQUM1RCxRQUFJQyxJQUFJLEdBQUcvSSxHQUFHLENBQUNpQixXQUFKLENBQWdCMkgsU0FBaEIsQ0FBWDs7QUFFQSxRQUFJRyxJQUFJLEtBQUssT0FBYixFQUFzQjtBQUNwQixhQUFPLElBQVA7QUFDRCxLQUZELE1BRU8sSUFBSUEsSUFBSSxLQUFLLElBQWIsRUFBbUI7QUFDeEIsYUFBT0YsU0FBUyxDQUFDN2MsTUFBRCxDQUFoQjtBQUNELEtBRk0sTUFFQTtBQUNMLGFBQU84YyxNQUFNLENBQUM5YyxNQUFELENBQWI7QUFDRDtBQUNGOztBQUVELFdBQVNnZCxtQkFBVCxDQUE2QmhKLEdBQTdCLEVBQWtDO0FBQ2hDLFFBQUlBLEdBQUcsQ0FBQ2lILGVBQUosSUFBdUJqSCxHQUFHLENBQUNpSCxlQUFKLEtBQXdCLE1BQW5ELEVBQTJEO0FBQ3pELGFBQU8sS0FBUDtBQUNELEtBRkQsTUFFTztBQUNMLGFBQU9qSCxHQUFHLENBQUNpSCxlQUFKLEtBQXdCLE1BQXhCLElBQWtDLENBQUNqSCxHQUFHLENBQUNwSSxNQUF2QyxJQUFpRG9JLEdBQUcsQ0FBQ3BJLE1BQUosQ0FBV3FSLFVBQVgsQ0FBc0IsSUFBdEIsQ0FBakQsSUFBZ0Y3VixPQUFPLE1BQU0sSUFBSUMsSUFBSSxDQUFDQyxjQUFULENBQXdCME0sR0FBRyxDQUFDOUgsSUFBNUIsRUFBa0N1SSxlQUFsQyxHQUFvRHdHLGVBQXBELEtBQXdFLE1BQTVLO0FBQ0Q7QUFDRjtBQUNEOzs7OztBQUtBLE1BQUlpQyxtQkFBbUI7QUFDdkI7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsbUJBQVQsQ0FBNkJoUixJQUE3QixFQUFtQ3lJLFdBQW5DLEVBQWdEckIsSUFBaEQsRUFBc0Q7QUFDcEQsV0FBS3NCLEtBQUwsR0FBYXRCLElBQUksQ0FBQ3NCLEtBQUwsSUFBYyxDQUEzQjtBQUNBLFdBQUt6TCxLQUFMLEdBQWFtSyxJQUFJLENBQUNuSyxLQUFMLElBQWMsS0FBM0I7O0FBRUEsVUFBSSxDQUFDd0wsV0FBRCxJQUFnQnZOLE9BQU8sRUFBM0IsRUFBK0I7QUFDN0IsWUFBSTJFLFFBQVEsR0FBRztBQUNib1IsVUFBQUEsV0FBVyxFQUFFO0FBREEsU0FBZjtBQUdBLFlBQUk3SixJQUFJLENBQUNzQixLQUFMLEdBQWEsQ0FBakIsRUFBb0I3SSxRQUFRLENBQUNxUixvQkFBVCxHQUFnQzlKLElBQUksQ0FBQ3NCLEtBQXJDO0FBQ3BCLGFBQUsyRyxHQUFMLEdBQVdELFlBQVksQ0FBQ3BQLElBQUQsRUFBT0gsUUFBUCxDQUF2QjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSW1JLE1BQU0sR0FBR2dKLG1CQUFtQixDQUFDdGMsU0FBakM7O0FBRUFzVCxJQUFBQSxNQUFNLENBQUN6SCxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0IxTSxDQUFoQixFQUFtQjtBQUNqQyxVQUFJLEtBQUt3YixHQUFULEVBQWM7QUFDWixZQUFJckIsS0FBSyxHQUFHLEtBQUsvUSxLQUFMLEdBQWFELElBQUksQ0FBQ0MsS0FBTCxDQUFXcEosQ0FBWCxDQUFiLEdBQTZCQSxDQUF6QztBQUNBLGVBQU8sS0FBS3diLEdBQUwsQ0FBUzlPLE1BQVQsQ0FBZ0J5TixLQUFoQixDQUFQO0FBQ0QsT0FIRCxNQUdPO0FBQ0w7QUFDQSxZQUFJbUQsTUFBTSxHQUFHLEtBQUtsVSxLQUFMLEdBQWFELElBQUksQ0FBQ0MsS0FBTCxDQUFXcEosQ0FBWCxDQUFiLEdBQTZCZ0ssT0FBTyxDQUFDaEssQ0FBRCxFQUFJLENBQUosQ0FBakQ7O0FBRUEsZUFBT3FKLFFBQVEsQ0FBQ2lVLE1BQUQsRUFBUyxLQUFLekksS0FBZCxDQUFmO0FBQ0Q7QUFDRixLQVZEOztBQVlBLFdBQU9zSSxtQkFBUDtBQUNELEdBN0JELEVBRkE7QUFnQ0E7Ozs7O0FBS0EsTUFBSUksaUJBQWlCO0FBQ3JCO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLGlCQUFULENBQTJCM04sRUFBM0IsRUFBK0J6RCxJQUEvQixFQUFxQ29ILElBQXJDLEVBQTJDO0FBQ3pDLFdBQUtBLElBQUwsR0FBWUEsSUFBWjtBQUNBLFdBQUtsTSxPQUFMLEdBQWVBLE9BQU8sRUFBdEI7QUFDQSxVQUFJNFQsQ0FBSjs7QUFFQSxVQUFJckwsRUFBRSxDQUFDNkYsSUFBSCxDQUFRK0gsU0FBUixJQUFxQixLQUFLblcsT0FBOUIsRUFBdUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E0VCxRQUFBQSxDQUFDLEdBQUcsS0FBSjs7QUFFQSxZQUFJMUgsSUFBSSxDQUFDdk4sWUFBVCxFQUF1QjtBQUNyQixlQUFLNEosRUFBTCxHQUFVQSxFQUFWO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsZUFBS0EsRUFBTCxHQUFVQSxFQUFFLENBQUMxQixNQUFILEtBQWMsQ0FBZCxHQUFrQjBCLEVBQWxCLEdBQXVCNk0sUUFBUSxDQUFDZ0IsVUFBVCxDQUFvQjdOLEVBQUUsQ0FBQ2pFLEVBQUgsR0FBUWlFLEVBQUUsQ0FBQzFCLE1BQUgsR0FBWSxFQUFaLEdBQWlCLElBQTdDLENBQWpDO0FBQ0Q7QUFDRixPQWpCRCxNQWlCTyxJQUFJMEIsRUFBRSxDQUFDNkYsSUFBSCxDQUFRbEosSUFBUixLQUFpQixPQUFyQixFQUE4QjtBQUNuQyxhQUFLcUQsRUFBTCxHQUFVQSxFQUFWO0FBQ0QsT0FGTSxNQUVBO0FBQ0wsYUFBS0EsRUFBTCxHQUFVQSxFQUFWO0FBQ0FxTCxRQUFBQSxDQUFDLEdBQUdyTCxFQUFFLENBQUM2RixJQUFILENBQVF5RCxJQUFaO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLN1IsT0FBVCxFQUFrQjtBQUNoQixZQUFJMkUsUUFBUSxHQUFHMUwsTUFBTSxDQUFDNEwsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBS3FILElBQXZCLENBQWY7O0FBRUEsWUFBSTBILENBQUosRUFBTztBQUNMalAsVUFBQUEsUUFBUSxDQUFDRixRQUFULEdBQW9CbVAsQ0FBcEI7QUFDRDs7QUFFRCxhQUFLOUMsR0FBTCxHQUFXaUQsWUFBWSxDQUFDalAsSUFBRCxFQUFPSCxRQUFQLENBQXZCO0FBQ0Q7QUFDRjs7QUFFRCxRQUFJMFIsT0FBTyxHQUFHSCxpQkFBaUIsQ0FBQzFjLFNBQWhDOztBQUVBNmMsSUFBQUEsT0FBTyxDQUFDaFIsTUFBUixHQUFpQixTQUFTQSxNQUFULEdBQWtCO0FBQ2pDLFVBQUksS0FBS3JGLE9BQVQsRUFBa0I7QUFDaEIsZUFBTyxLQUFLOFEsR0FBTCxDQUFTekwsTUFBVCxDQUFnQixLQUFLa0QsRUFBTCxDQUFRK04sUUFBUixFQUFoQixDQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsWUFBSUMsV0FBVyxHQUFHM00sWUFBWSxDQUFDLEtBQUtzQyxJQUFOLENBQTlCO0FBQUEsWUFDSVUsR0FBRyxHQUFHK0csTUFBTSxDQUFDL1osTUFBUCxDQUFjLE9BQWQsQ0FEVjtBQUVBLGVBQU9xUyxTQUFTLENBQUNyUyxNQUFWLENBQWlCZ1QsR0FBakIsRUFBc0JjLHdCQUF0QixDQUErQyxLQUFLbkYsRUFBcEQsRUFBd0RnTyxXQUF4RCxDQUFQO0FBQ0Q7QUFDRixLQVJEOztBQVVBRixJQUFBQSxPQUFPLENBQUNqVyxhQUFSLEdBQXdCLFNBQVNBLGFBQVQsR0FBeUI7QUFDL0MsVUFBSSxLQUFLSixPQUFMLElBQWdCRyxnQkFBZ0IsRUFBcEMsRUFBd0M7QUFDdEMsZUFBTyxLQUFLMlEsR0FBTCxDQUFTMVEsYUFBVCxDQUF1QixLQUFLbUksRUFBTCxDQUFRK04sUUFBUixFQUF2QixDQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0w7QUFDQTtBQUNBLGVBQU8sRUFBUDtBQUNEO0FBQ0YsS0FSRDs7QUFVQUQsSUFBQUEsT0FBTyxDQUFDaEosZUFBUixHQUEwQixTQUFTQSxlQUFULEdBQTJCO0FBQ25ELFVBQUksS0FBS3JOLE9BQVQsRUFBa0I7QUFDaEIsZUFBTyxLQUFLOFEsR0FBTCxDQUFTekQsZUFBVCxFQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBTztBQUNMN0ksVUFBQUEsTUFBTSxFQUFFLE9BREg7QUFFTHFQLFVBQUFBLGVBQWUsRUFBRSxNQUZaO0FBR0w5RixVQUFBQSxjQUFjLEVBQUU7QUFIWCxTQUFQO0FBS0Q7QUFDRixLQVZEOztBQVlBLFdBQU9tSSxpQkFBUDtBQUNELEdBNUVELEVBRkE7QUErRUE7Ozs7O0FBS0EsTUFBSU0sZ0JBQWdCO0FBQ3BCO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLGdCQUFULENBQTBCMVIsSUFBMUIsRUFBZ0MyUixTQUFoQyxFQUEyQ3ZLLElBQTNDLEVBQWlEO0FBQy9DLFdBQUtBLElBQUwsR0FBWWpULE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYztBQUN4QjZSLFFBQUFBLEtBQUssRUFBRTtBQURpQixPQUFkLEVBRVR4SyxJQUZTLENBQVo7O0FBSUEsVUFBSSxDQUFDdUssU0FBRCxJQUFjcFcsV0FBVyxFQUE3QixFQUFpQztBQUMvQixhQUFLc1csR0FBTCxHQUFXckMsWUFBWSxDQUFDeFAsSUFBRCxFQUFPb0gsSUFBUCxDQUF2QjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSTBLLE9BQU8sR0FBR0osZ0JBQWdCLENBQUNoZCxTQUEvQjs7QUFFQW9kLElBQUFBLE9BQU8sQ0FBQ3ZSLE1BQVIsR0FBaUIsU0FBU0EsTUFBVCxDQUFnQnVELEtBQWhCLEVBQXVCdkwsSUFBdkIsRUFBNkI7QUFDNUMsVUFBSSxLQUFLc1osR0FBVCxFQUFjO0FBQ1osZUFBTyxLQUFLQSxHQUFMLENBQVN0UixNQUFULENBQWdCdUQsS0FBaEIsRUFBdUJ2TCxJQUF2QixDQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBT3NMLGtCQUFrQixDQUFDdEwsSUFBRCxFQUFPdUwsS0FBUCxFQUFjLEtBQUtzRCxJQUFMLENBQVVyRCxPQUF4QixFQUFpQyxLQUFLcUQsSUFBTCxDQUFVd0ssS0FBVixLQUFvQixNQUFyRCxDQUF6QjtBQUNEO0FBQ0YsS0FORDs7QUFRQUUsSUFBQUEsT0FBTyxDQUFDeFcsYUFBUixHQUF3QixTQUFTQSxhQUFULENBQXVCd0ksS0FBdkIsRUFBOEJ2TCxJQUE5QixFQUFvQztBQUMxRCxVQUFJLEtBQUtzWixHQUFULEVBQWM7QUFDWixlQUFPLEtBQUtBLEdBQUwsQ0FBU3ZXLGFBQVQsQ0FBdUJ3SSxLQUF2QixFQUE4QnZMLElBQTlCLENBQVA7QUFDRCxPQUZELE1BRU87QUFDTCxlQUFPLEVBQVA7QUFDRDtBQUNGLEtBTkQ7O0FBUUEsV0FBT21aLGdCQUFQO0FBQ0QsR0E5QkQsRUFGQTtBQWlDQTs7Ozs7QUFLQSxNQUFJN0MsTUFBTTtBQUNWO0FBQ0EsY0FBWTtBQUNWQSxJQUFBQSxNQUFNLENBQUNrRCxRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBa0IzSyxJQUFsQixFQUF3QjtBQUN4QyxhQUFPeUgsTUFBTSxDQUFDL1osTUFBUCxDQUFjc1MsSUFBSSxDQUFDMUgsTUFBbkIsRUFBMkIwSCxJQUFJLENBQUMySCxlQUFoQyxFQUFpRDNILElBQUksQ0FBQzZCLGNBQXRELEVBQXNFN0IsSUFBSSxDQUFDNEssV0FBM0UsQ0FBUDtBQUNELEtBRkQ7O0FBSUFuRCxJQUFBQSxNQUFNLENBQUMvWixNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0I0SyxNQUFoQixFQUF3QnFQLGVBQXhCLEVBQXlDOUYsY0FBekMsRUFBeUQrSSxXQUF6RCxFQUFzRTtBQUNwRixVQUFJQSxXQUFXLEtBQUssS0FBSyxDQUF6QixFQUE0QjtBQUMxQkEsUUFBQUEsV0FBVyxHQUFHLEtBQWQ7QUFDRDs7QUFFRCxVQUFJQyxlQUFlLEdBQUd2UyxNQUFNLElBQUlpUCxRQUFRLENBQUNKLGFBQXpDO0FBQUEsVUFDSTtBQUNKMkQsTUFBQUEsT0FBTyxHQUFHRCxlQUFlLEtBQUtELFdBQVcsR0FBRyxPQUFILEdBQWF0QyxZQUFZLEVBQXpDLENBRnpCO0FBQUEsVUFHSXlDLGdCQUFnQixHQUFHcEQsZUFBZSxJQUFJSixRQUFRLENBQUNILHNCQUhuRDtBQUFBLFVBSUk0RCxlQUFlLEdBQUduSixjQUFjLElBQUkwRixRQUFRLENBQUNGLHFCQUpqRDtBQUtBLGFBQU8sSUFBSUksTUFBSixDQUFXcUQsT0FBWCxFQUFvQkMsZ0JBQXBCLEVBQXNDQyxlQUF0QyxFQUF1REgsZUFBdkQsQ0FBUDtBQUNELEtBWEQ7O0FBYUFwRCxJQUFBQSxNQUFNLENBQUM3QixVQUFQLEdBQW9CLFNBQVNBLFVBQVQsR0FBc0I7QUFDeEN5QyxNQUFBQSxjQUFjLEdBQUcsSUFBakI7QUFDQVQsTUFBQUEsV0FBVyxHQUFHLEVBQWQ7QUFDQUcsTUFBQUEsWUFBWSxHQUFHLEVBQWY7QUFDQUksTUFBQUEsWUFBWSxHQUFHLEVBQWY7QUFDRCxLQUxEOztBQU9BVixJQUFBQSxNQUFNLENBQUN3RCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0JDLEtBQXBCLEVBQTJCO0FBQzdDLFVBQUk1TSxJQUFJLEdBQUc0TSxLQUFLLEtBQUssS0FBSyxDQUFmLEdBQW1CLEVBQW5CLEdBQXdCQSxLQUFuQztBQUFBLFVBQ0k1UyxNQUFNLEdBQUdnRyxJQUFJLENBQUNoRyxNQURsQjtBQUFBLFVBRUlxUCxlQUFlLEdBQUdySixJQUFJLENBQUNxSixlQUYzQjtBQUFBLFVBR0k5RixjQUFjLEdBQUd2RCxJQUFJLENBQUN1RCxjQUgxQjs7QUFLQSxhQUFPNEYsTUFBTSxDQUFDL1osTUFBUCxDQUFjNEssTUFBZCxFQUFzQnFQLGVBQXRCLEVBQXVDOUYsY0FBdkMsQ0FBUDtBQUNELEtBUEQ7O0FBU0EsYUFBUzRGLE1BQVQsQ0FBZ0JuUCxNQUFoQixFQUF3QjZTLFNBQXhCLEVBQW1DdEosY0FBbkMsRUFBbURnSixlQUFuRCxFQUFvRTtBQUNsRSxVQUFJTyxrQkFBa0IsR0FBRzVDLGlCQUFpQixDQUFDbFEsTUFBRCxDQUExQztBQUFBLFVBQ0krUyxZQUFZLEdBQUdELGtCQUFrQixDQUFDLENBQUQsQ0FEckM7QUFBQSxVQUVJRSxxQkFBcUIsR0FBR0Ysa0JBQWtCLENBQUMsQ0FBRCxDQUY5QztBQUFBLFVBR0lHLG9CQUFvQixHQUFHSCxrQkFBa0IsQ0FBQyxDQUFELENBSDdDOztBQUtBLFdBQUs5UyxNQUFMLEdBQWMrUyxZQUFkO0FBQ0EsV0FBSzFELGVBQUwsR0FBdUJ3RCxTQUFTLElBQUlHLHFCQUFiLElBQXNDLElBQTdEO0FBQ0EsV0FBS3pKLGNBQUwsR0FBc0JBLGNBQWMsSUFBSTBKLG9CQUFsQixJQUEwQyxJQUFoRTtBQUNBLFdBQUszUyxJQUFMLEdBQVltUSxnQkFBZ0IsQ0FBQyxLQUFLelEsTUFBTixFQUFjLEtBQUtxUCxlQUFuQixFQUFvQyxLQUFLOUYsY0FBekMsQ0FBNUI7QUFDQSxXQUFLMkosYUFBTCxHQUFxQjtBQUNuQnJTLFFBQUFBLE1BQU0sRUFBRSxFQURXO0FBRW5CaUosUUFBQUEsVUFBVSxFQUFFO0FBRk8sT0FBckI7QUFJQSxXQUFLcUosV0FBTCxHQUFtQjtBQUNqQnRTLFFBQUFBLE1BQU0sRUFBRSxFQURTO0FBRWpCaUosUUFBQUEsVUFBVSxFQUFFO0FBRkssT0FBbkI7QUFJQSxXQUFLc0osYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxXQUFLZCxlQUFMLEdBQXVCQSxlQUF2QjtBQUNBLFdBQUtlLGlCQUFMLEdBQXlCLElBQXpCO0FBQ0Q7O0FBRUQsUUFBSUMsT0FBTyxHQUFHcEUsTUFBTSxDQUFDbmEsU0FBckI7O0FBRUF1ZSxJQUFBQSxPQUFPLENBQUNsSyxXQUFSLEdBQXNCLFNBQVNBLFdBQVQsQ0FBcUIySCxTQUFyQixFQUFnQztBQUNwRCxVQUFJQSxTQUFTLEtBQUssS0FBSyxDQUF2QixFQUEwQjtBQUN4QkEsUUFBQUEsU0FBUyxHQUFHLElBQVo7QUFDRDs7QUFFRCxVQUFJMVEsSUFBSSxHQUFHOUUsT0FBTyxFQUFsQjtBQUFBLFVBQ0lnWSxNQUFNLEdBQUdsVCxJQUFJLElBQUkzRSxnQkFBZ0IsRUFEckM7QUFBQSxVQUVJOFgsWUFBWSxHQUFHLEtBQUt4QixTQUFMLEVBRm5CO0FBQUEsVUFHSXlCLGNBQWMsR0FBRyxDQUFDLEtBQUtyRSxlQUFMLEtBQXlCLElBQXpCLElBQWlDLEtBQUtBLGVBQUwsS0FBeUIsTUFBM0QsTUFBdUUsS0FBSzlGLGNBQUwsS0FBd0IsSUFBeEIsSUFBZ0MsS0FBS0EsY0FBTCxLQUF3QixTQUEvSCxDQUhyQjs7QUFLQSxVQUFJLENBQUNpSyxNQUFELElBQVcsRUFBRUMsWUFBWSxJQUFJQyxjQUFsQixDQUFYLElBQWdELENBQUMxQyxTQUFyRCxFQUFnRTtBQUM5RCxlQUFPLE9BQVA7QUFDRCxPQUZELE1BRU8sSUFBSSxDQUFDd0MsTUFBRCxJQUFXQyxZQUFZLElBQUlDLGNBQS9CLEVBQStDO0FBQ3BELGVBQU8sSUFBUDtBQUNELE9BRk0sTUFFQTtBQUNMLGVBQU8sTUFBUDtBQUNEO0FBQ0YsS0FqQkQ7O0FBbUJBSCxJQUFBQSxPQUFPLENBQUNJLEtBQVIsR0FBZ0IsU0FBU0EsS0FBVCxDQUFlQyxJQUFmLEVBQXFCO0FBQ25DLFVBQUksQ0FBQ0EsSUFBRCxJQUFTbmYsTUFBTSxDQUFDb2YsbUJBQVAsQ0FBMkJELElBQTNCLEVBQWlDeGYsTUFBakMsS0FBNEMsQ0FBekQsRUFBNEQ7QUFDMUQsZUFBTyxJQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBTythLE1BQU0sQ0FBQy9aLE1BQVAsQ0FBY3dlLElBQUksQ0FBQzVULE1BQUwsSUFBZSxLQUFLdVMsZUFBbEMsRUFBbURxQixJQUFJLENBQUN2RSxlQUFMLElBQXdCLEtBQUtBLGVBQWhGLEVBQWlHdUUsSUFBSSxDQUFDckssY0FBTCxJQUF1QixLQUFLQSxjQUE3SCxFQUE2SXFLLElBQUksQ0FBQ3RCLFdBQUwsSUFBb0IsS0FBakssQ0FBUDtBQUNEO0FBQ0YsS0FORDs7QUFRQWlCLElBQUFBLE9BQU8sQ0FBQ08sYUFBUixHQUF3QixTQUFTQSxhQUFULENBQXVCRixJQUF2QixFQUE2QjtBQUNuRCxVQUFJQSxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUtELEtBQUwsQ0FBV2xmLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYyxFQUFkLEVBQWtCdVQsSUFBbEIsRUFBd0I7QUFDeEN0QixRQUFBQSxXQUFXLEVBQUU7QUFEMkIsT0FBeEIsQ0FBWCxDQUFQO0FBR0QsS0FSRDs7QUFVQWlCLElBQUFBLE9BQU8sQ0FBQy9LLGlCQUFSLEdBQTRCLFNBQVNBLGlCQUFULENBQTJCb0wsSUFBM0IsRUFBaUM7QUFDM0QsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLRCxLQUFMLENBQVdsZixNQUFNLENBQUM0TCxNQUFQLENBQWMsRUFBZCxFQUFrQnVULElBQWxCLEVBQXdCO0FBQ3hDdEIsUUFBQUEsV0FBVyxFQUFFO0FBRDJCLE9BQXhCLENBQVgsQ0FBUDtBQUdELEtBUkQ7O0FBVUFpQixJQUFBQSxPQUFPLENBQUNuUSxNQUFSLEdBQWlCLFNBQVMyUSxRQUFULENBQWtCM2YsTUFBbEIsRUFBMEJ5TSxNQUExQixFQUFrQ21RLFNBQWxDLEVBQTZDO0FBQzVELFVBQUk3SCxLQUFLLEdBQUcsSUFBWjs7QUFFQSxVQUFJdEksTUFBTSxLQUFLLEtBQUssQ0FBcEIsRUFBdUI7QUFDckJBLFFBQUFBLE1BQU0sR0FBRyxLQUFUO0FBQ0Q7O0FBRUQsVUFBSW1RLFNBQVMsS0FBSyxLQUFLLENBQXZCLEVBQTBCO0FBQ3hCQSxRQUFBQSxTQUFTLEdBQUcsSUFBWjtBQUNEOztBQUVELGFBQU9ELFNBQVMsQ0FBQyxJQUFELEVBQU8zYyxNQUFQLEVBQWU0YyxTQUFmLEVBQTBCNU4sTUFBMUIsRUFBa0MsWUFBWTtBQUM1RCxZQUFJOUMsSUFBSSxHQUFHTyxNQUFNLEdBQUc7QUFDbEJ0SCxVQUFBQSxLQUFLLEVBQUVuRixNQURXO0FBRWxCb0YsVUFBQUEsR0FBRyxFQUFFO0FBRmEsU0FBSCxHQUdiO0FBQ0ZELFVBQUFBLEtBQUssRUFBRW5GO0FBREwsU0FISjtBQUFBLFlBTUk0ZixTQUFTLEdBQUduVCxNQUFNLEdBQUcsUUFBSCxHQUFjLFlBTnBDOztBQVFBLFlBQUksQ0FBQ3NJLEtBQUssQ0FBQ2dLLFdBQU4sQ0FBa0JhLFNBQWxCLEVBQTZCNWYsTUFBN0IsQ0FBTCxFQUEyQztBQUN6QytVLFVBQUFBLEtBQUssQ0FBQ2dLLFdBQU4sQ0FBa0JhLFNBQWxCLEVBQTZCNWYsTUFBN0IsSUFBdUNzYyxTQUFTLENBQUMsVUFBVTNNLEVBQVYsRUFBYztBQUM3RCxtQkFBT29GLEtBQUssQ0FBQ0ssT0FBTixDQUFjekYsRUFBZCxFQUFrQnpELElBQWxCLEVBQXdCLE9BQXhCLENBQVA7QUFDRCxXQUYrQyxDQUFoRDtBQUdEOztBQUVELGVBQU82SSxLQUFLLENBQUNnSyxXQUFOLENBQWtCYSxTQUFsQixFQUE2QjVmLE1BQTdCLENBQVA7QUFDRCxPQWhCZSxDQUFoQjtBQWlCRCxLQTVCRDs7QUE4QkFtZixJQUFBQSxPQUFPLENBQUMvUCxRQUFSLEdBQW1CLFNBQVN5USxVQUFULENBQW9CN2YsTUFBcEIsRUFBNEJ5TSxNQUE1QixFQUFvQ21RLFNBQXBDLEVBQStDO0FBQ2hFLFVBQUl4RyxNQUFNLEdBQUcsSUFBYjs7QUFFQSxVQUFJM0osTUFBTSxLQUFLLEtBQUssQ0FBcEIsRUFBdUI7QUFDckJBLFFBQUFBLE1BQU0sR0FBRyxLQUFUO0FBQ0Q7O0FBRUQsVUFBSW1RLFNBQVMsS0FBSyxLQUFLLENBQXZCLEVBQTBCO0FBQ3hCQSxRQUFBQSxTQUFTLEdBQUcsSUFBWjtBQUNEOztBQUVELGFBQU9ELFNBQVMsQ0FBQyxJQUFELEVBQU8zYyxNQUFQLEVBQWU0YyxTQUFmLEVBQTBCeE4sUUFBMUIsRUFBb0MsWUFBWTtBQUM5RCxZQUFJbEQsSUFBSSxHQUFHTyxNQUFNLEdBQUc7QUFDbEJqSCxVQUFBQSxPQUFPLEVBQUV4RixNQURTO0FBRWxCa0YsVUFBQUEsSUFBSSxFQUFFLFNBRlk7QUFHbEJDLFVBQUFBLEtBQUssRUFBRSxNQUhXO0FBSWxCQyxVQUFBQSxHQUFHLEVBQUU7QUFKYSxTQUFILEdBS2I7QUFDRkksVUFBQUEsT0FBTyxFQUFFeEY7QUFEUCxTQUxKO0FBQUEsWUFRSTRmLFNBQVMsR0FBR25ULE1BQU0sR0FBRyxRQUFILEdBQWMsWUFScEM7O0FBVUEsWUFBSSxDQUFDMkosTUFBTSxDQUFDMEksYUFBUCxDQUFxQmMsU0FBckIsRUFBZ0M1ZixNQUFoQyxDQUFMLEVBQThDO0FBQzVDb1csVUFBQUEsTUFBTSxDQUFDMEksYUFBUCxDQUFxQmMsU0FBckIsRUFBZ0M1ZixNQUFoQyxJQUEwQzBjLFdBQVcsQ0FBQyxVQUFVL00sRUFBVixFQUFjO0FBQ2xFLG1CQUFPeUcsTUFBTSxDQUFDaEIsT0FBUCxDQUFlekYsRUFBZixFQUFtQnpELElBQW5CLEVBQXlCLFNBQXpCLENBQVA7QUFDRCxXQUZvRCxDQUFyRDtBQUdEOztBQUVELGVBQU9rSyxNQUFNLENBQUMwSSxhQUFQLENBQXFCYyxTQUFyQixFQUFnQzVmLE1BQWhDLENBQVA7QUFDRCxPQWxCZSxDQUFoQjtBQW1CRCxLQTlCRDs7QUFnQ0FtZixJQUFBQSxPQUFPLENBQUM5UCxTQUFSLEdBQW9CLFNBQVN5USxXQUFULENBQXFCbEQsU0FBckIsRUFBZ0M7QUFDbEQsVUFBSW1ELE1BQU0sR0FBRyxJQUFiOztBQUVBLFVBQUluRCxTQUFTLEtBQUssS0FBSyxDQUF2QixFQUEwQjtBQUN4QkEsUUFBQUEsU0FBUyxHQUFHLElBQVo7QUFDRDs7QUFFRCxhQUFPRCxTQUFTLENBQUMsSUFBRCxFQUFPeFosU0FBUCxFQUFrQnlaLFNBQWxCLEVBQTZCLFlBQVk7QUFDdkQsZUFBT3ZOLFNBQVA7QUFDRCxPQUZlLEVBRWIsWUFBWTtBQUNiO0FBQ0E7QUFDQSxZQUFJLENBQUMwUSxNQUFNLENBQUNmLGFBQVosRUFBMkI7QUFDekIsY0FBSTlTLElBQUksR0FBRztBQUNUeEcsWUFBQUEsSUFBSSxFQUFFLFNBREc7QUFFVFEsWUFBQUEsTUFBTSxFQUFFO0FBRkMsV0FBWDtBQUlBNlosVUFBQUEsTUFBTSxDQUFDZixhQUFQLEdBQXVCLENBQUN4QyxRQUFRLENBQUNDLEdBQVQsQ0FBYSxJQUFiLEVBQW1CLEVBQW5CLEVBQXVCLEVBQXZCLEVBQTJCLENBQTNCLENBQUQsRUFBZ0NELFFBQVEsQ0FBQ0MsR0FBVCxDQUFhLElBQWIsRUFBbUIsRUFBbkIsRUFBdUIsRUFBdkIsRUFBMkIsRUFBM0IsQ0FBaEMsRUFBZ0UxRixHQUFoRSxDQUFvRSxVQUFVcEgsRUFBVixFQUFjO0FBQ3ZHLG1CQUFPb1EsTUFBTSxDQUFDM0ssT0FBUCxDQUFlekYsRUFBZixFQUFtQnpELElBQW5CLEVBQXlCLFdBQXpCLENBQVA7QUFDRCxXQUZzQixDQUF2QjtBQUdEOztBQUVELGVBQU82VCxNQUFNLENBQUNmLGFBQWQ7QUFDRCxPQWhCZSxDQUFoQjtBQWlCRCxLQXhCRDs7QUEwQkFHLElBQUFBLE9BQU8sQ0FBQzFQLElBQVIsR0FBZSxTQUFTdVEsTUFBVCxDQUFnQmhnQixNQUFoQixFQUF3QjRjLFNBQXhCLEVBQW1DO0FBQ2hELFVBQUlxRCxNQUFNLEdBQUcsSUFBYjs7QUFFQSxVQUFJckQsU0FBUyxLQUFLLEtBQUssQ0FBdkIsRUFBMEI7QUFDeEJBLFFBQUFBLFNBQVMsR0FBRyxJQUFaO0FBQ0Q7O0FBRUQsYUFBT0QsU0FBUyxDQUFDLElBQUQsRUFBTzNjLE1BQVAsRUFBZTRjLFNBQWYsRUFBMEJuTixJQUExQixFQUFnQyxZQUFZO0FBQzFELFlBQUl2RCxJQUFJLEdBQUc7QUFDVDBKLFVBQUFBLEdBQUcsRUFBRTVWO0FBREksU0FBWCxDQUQwRCxDQUd2RDtBQUNIOztBQUVBLFlBQUksQ0FBQ2lnQixNQUFNLENBQUNoQixRQUFQLENBQWdCamYsTUFBaEIsQ0FBTCxFQUE4QjtBQUM1QmlnQixVQUFBQSxNQUFNLENBQUNoQixRQUFQLENBQWdCamYsTUFBaEIsSUFBMEIsQ0FBQ3djLFFBQVEsQ0FBQ0MsR0FBVCxDQUFhLENBQUMsRUFBZCxFQUFrQixDQUFsQixFQUFxQixDQUFyQixDQUFELEVBQTBCRCxRQUFRLENBQUNDLEdBQVQsQ0FBYSxJQUFiLEVBQW1CLENBQW5CLEVBQXNCLENBQXRCLENBQTFCLEVBQW9EMUYsR0FBcEQsQ0FBd0QsVUFBVXBILEVBQVYsRUFBYztBQUM5RixtQkFBT3NRLE1BQU0sQ0FBQzdLLE9BQVAsQ0FBZXpGLEVBQWYsRUFBbUJ6RCxJQUFuQixFQUF5QixLQUF6QixDQUFQO0FBQ0QsV0FGeUIsQ0FBMUI7QUFHRDs7QUFFRCxlQUFPK1QsTUFBTSxDQUFDaEIsUUFBUCxDQUFnQmpmLE1BQWhCLENBQVA7QUFDRCxPQWJlLENBQWhCO0FBY0QsS0FyQkQ7O0FBdUJBbWYsSUFBQUEsT0FBTyxDQUFDL0osT0FBUixHQUFrQixTQUFTQSxPQUFULENBQWlCekYsRUFBakIsRUFBcUI1RCxRQUFyQixFQUErQm1VLEtBQS9CLEVBQXNDO0FBQ3RELFVBQUk3TCxFQUFFLEdBQUcsS0FBS0MsV0FBTCxDQUFpQjNFLEVBQWpCLEVBQXFCNUQsUUFBckIsQ0FBVDtBQUFBLFVBQ0lvVSxPQUFPLEdBQUc5TCxFQUFFLENBQUM3TSxhQUFILEVBRGQ7QUFBQSxVQUVJNFksUUFBUSxHQUFHRCxPQUFPLENBQUMvVCxJQUFSLENBQWEsVUFBVUMsQ0FBVixFQUFhO0FBQ3ZDLGVBQU9BLENBQUMsQ0FBQ0MsSUFBRixDQUFPQyxXQUFQLE9BQXlCMlQsS0FBaEM7QUFDRCxPQUZjLENBRmY7QUFLQSxhQUFPRSxRQUFRLEdBQUdBLFFBQVEsQ0FBQzNjLEtBQVosR0FBb0IsSUFBbkM7QUFDRCxLQVBEOztBQVNBMGIsSUFBQUEsT0FBTyxDQUFDdEssZUFBUixHQUEwQixTQUFTQSxlQUFULENBQXlCdkIsSUFBekIsRUFBK0I7QUFDdkQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0QsT0FIc0QsQ0FLdkQ7QUFDQTs7O0FBQ0EsYUFBTyxJQUFJNEosbUJBQUosQ0FBd0IsS0FBS2hSLElBQTdCLEVBQW1Db0gsSUFBSSxDQUFDcUIsV0FBTCxJQUFvQixLQUFLMEwsV0FBNUQsRUFBeUUvTSxJQUF6RSxDQUFQO0FBQ0QsS0FSRDs7QUFVQTZMLElBQUFBLE9BQU8sQ0FBQzdLLFdBQVIsR0FBc0IsU0FBU0EsV0FBVCxDQUFxQjNFLEVBQXJCLEVBQXlCNUQsUUFBekIsRUFBbUM7QUFDdkQsVUFBSUEsUUFBUSxLQUFLLEtBQUssQ0FBdEIsRUFBeUI7QUFDdkJBLFFBQUFBLFFBQVEsR0FBRyxFQUFYO0FBQ0Q7O0FBRUQsYUFBTyxJQUFJdVIsaUJBQUosQ0FBc0IzTixFQUF0QixFQUEwQixLQUFLekQsSUFBL0IsRUFBcUNILFFBQXJDLENBQVA7QUFDRCxLQU5EOztBQVFBb1QsSUFBQUEsT0FBTyxDQUFDbUIsWUFBUixHQUF1QixTQUFTQSxZQUFULENBQXNCaE4sSUFBdEIsRUFBNEI7QUFDakQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsYUFBTyxJQUFJc0ssZ0JBQUosQ0FBcUIsS0FBSzFSLElBQTFCLEVBQWdDLEtBQUsyUixTQUFMLEVBQWhDLEVBQWtEdkssSUFBbEQsQ0FBUDtBQUNELEtBTkQ7O0FBUUE2TCxJQUFBQSxPQUFPLENBQUN0QixTQUFSLEdBQW9CLFNBQVNBLFNBQVQsR0FBcUI7QUFDdkMsYUFBTyxLQUFLalMsTUFBTCxLQUFnQixJQUFoQixJQUF3QixLQUFLQSxNQUFMLENBQVlXLFdBQVosT0FBOEIsT0FBdEQsSUFBaUVuRixPQUFPLE1BQU0sSUFBSUMsSUFBSSxDQUFDQyxjQUFULENBQXdCLEtBQUs0RSxJQUE3QixFQUFtQ3VJLGVBQW5DLEdBQXFEN0ksTUFBckQsQ0FBNERxUixVQUE1RCxDQUF1RSxPQUF2RSxDQUFyRjtBQUNELEtBRkQ7O0FBSUFrQyxJQUFBQSxPQUFPLENBQUMvSCxNQUFSLEdBQWlCLFNBQVNBLE1BQVQsQ0FBZ0JtSixLQUFoQixFQUF1QjtBQUN0QyxhQUFPLEtBQUszVSxNQUFMLEtBQWdCMlUsS0FBSyxDQUFDM1UsTUFBdEIsSUFBZ0MsS0FBS3FQLGVBQUwsS0FBeUJzRixLQUFLLENBQUN0RixlQUEvRCxJQUFrRixLQUFLOUYsY0FBTCxLQUF3Qm9MLEtBQUssQ0FBQ3BMLGNBQXZIO0FBQ0QsS0FGRDs7QUFJQTNVLElBQUFBLFlBQVksQ0FBQ3VhLE1BQUQsRUFBUyxDQUFDO0FBQ3BCeGEsTUFBQUEsR0FBRyxFQUFFLGFBRGU7QUFFcEIrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLFlBQUksS0FBSzRiLGlCQUFMLElBQTBCLElBQTlCLEVBQW9DO0FBQ2xDLGVBQUtBLGlCQUFMLEdBQXlCbEMsbUJBQW1CLENBQUMsSUFBRCxDQUE1QztBQUNEOztBQUVELGVBQU8sS0FBS2tDLGlCQUFaO0FBQ0Q7QUFSbUIsS0FBRCxDQUFULENBQVo7O0FBV0EsV0FBT25FLE1BQVA7QUFDRCxHQWpSRCxFQUZBO0FBcVJBOzs7Ozs7Ozs7OztBQVVBLFdBQVN5RixjQUFULEdBQTBCO0FBQ3hCLFNBQUssSUFBSUMsSUFBSSxHQUFHN2QsU0FBUyxDQUFDNUMsTUFBckIsRUFBNkIwZ0IsT0FBTyxHQUFHLElBQUk3WSxLQUFKLENBQVU0WSxJQUFWLENBQXZDLEVBQXdERSxJQUFJLEdBQUcsQ0FBcEUsRUFBdUVBLElBQUksR0FBR0YsSUFBOUUsRUFBb0ZFLElBQUksRUFBeEYsRUFBNEY7QUFDMUZELE1BQUFBLE9BQU8sQ0FBQ0MsSUFBRCxDQUFQLEdBQWdCL2QsU0FBUyxDQUFDK2QsSUFBRCxDQUF6QjtBQUNEOztBQUVELFFBQUlDLElBQUksR0FBR0YsT0FBTyxDQUFDdlksTUFBUixDQUFlLFVBQVUwQixDQUFWLEVBQWFvUSxDQUFiLEVBQWdCO0FBQ3hDLGFBQU9wUSxDQUFDLEdBQUdvUSxDQUFDLENBQUNwQyxNQUFiO0FBQ0QsS0FGVSxFQUVSLEVBRlEsQ0FBWDtBQUdBLFdBQU9ELE1BQU0sQ0FBQyxNQUFNZ0osSUFBTixHQUFhLEdBQWQsQ0FBYjtBQUNEOztBQUVELFdBQVNDLGlCQUFULEdBQTZCO0FBQzNCLFNBQUssSUFBSUMsS0FBSyxHQUFHbGUsU0FBUyxDQUFDNUMsTUFBdEIsRUFBOEIrZ0IsVUFBVSxHQUFHLElBQUlsWixLQUFKLENBQVVpWixLQUFWLENBQTNDLEVBQTZERSxLQUFLLEdBQUcsQ0FBMUUsRUFBNkVBLEtBQUssR0FBR0YsS0FBckYsRUFBNEZFLEtBQUssRUFBakcsRUFBcUc7QUFDbkdELE1BQUFBLFVBQVUsQ0FBQ0MsS0FBRCxDQUFWLEdBQW9CcGUsU0FBUyxDQUFDb2UsS0FBRCxDQUE3QjtBQUNEOztBQUVELFdBQU8sVUFBVTNVLENBQVYsRUFBYTtBQUNsQixhQUFPMFUsVUFBVSxDQUFDNVksTUFBWCxDQUFrQixVQUFVeUosSUFBVixFQUFnQnFQLEVBQWhCLEVBQW9CO0FBQzNDLFlBQUlDLFVBQVUsR0FBR3RQLElBQUksQ0FBQyxDQUFELENBQXJCO0FBQUEsWUFDSXVQLFVBQVUsR0FBR3ZQLElBQUksQ0FBQyxDQUFELENBRHJCO0FBQUEsWUFFSXdQLE1BQU0sR0FBR3hQLElBQUksQ0FBQyxDQUFELENBRmpCOztBQUlBLFlBQUl5UCxHQUFHLEdBQUdKLEVBQUUsQ0FBQzVVLENBQUQsRUFBSStVLE1BQUosQ0FBWjtBQUFBLFlBQ0lwUCxHQUFHLEdBQUdxUCxHQUFHLENBQUMsQ0FBRCxDQURiO0FBQUEsWUFFSTdMLElBQUksR0FBRzZMLEdBQUcsQ0FBQyxDQUFELENBRmQ7QUFBQSxZQUdJaFosSUFBSSxHQUFHZ1osR0FBRyxDQUFDLENBQUQsQ0FIZDs7QUFLQSxlQUFPLENBQUNoaEIsTUFBTSxDQUFDNEwsTUFBUCxDQUFjaVYsVUFBZCxFQUEwQmxQLEdBQTFCLENBQUQsRUFBaUNtUCxVQUFVLElBQUkzTCxJQUEvQyxFQUFxRG5OLElBQXJELENBQVA7QUFDRCxPQVhNLEVBV0osQ0FBQyxFQUFELEVBQUssSUFBTCxFQUFXLENBQVgsQ0FYSSxFQVdXa0IsS0FYWCxDQVdpQixDQVhqQixFQVdvQixDQVhwQixDQUFQO0FBWUQsS0FiRDtBQWNEOztBQUVELFdBQVMrWCxLQUFULENBQWV2YyxDQUFmLEVBQWtCO0FBQ2hCLFFBQUlBLENBQUMsSUFBSSxJQUFULEVBQWU7QUFDYixhQUFPLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FBUDtBQUNEOztBQUVELFNBQUssSUFBSXdjLEtBQUssR0FBRzNlLFNBQVMsQ0FBQzVDLE1BQXRCLEVBQThCd2hCLFFBQVEsR0FBRyxJQUFJM1osS0FBSixDQUFVMFosS0FBSyxHQUFHLENBQVIsR0FBWUEsS0FBSyxHQUFHLENBQXBCLEdBQXdCLENBQWxDLENBQXpDLEVBQStFRSxLQUFLLEdBQUcsQ0FBNUYsRUFBK0ZBLEtBQUssR0FBR0YsS0FBdkcsRUFBOEdFLEtBQUssRUFBbkgsRUFBdUg7QUFDckhELE1BQUFBLFFBQVEsQ0FBQ0MsS0FBSyxHQUFHLENBQVQsQ0FBUixHQUFzQjdlLFNBQVMsQ0FBQzZlLEtBQUQsQ0FBL0I7QUFDRDs7QUFFRCxTQUFLLElBQUloUSxFQUFFLEdBQUcsQ0FBVCxFQUFZaVEsU0FBUyxHQUFHRixRQUE3QixFQUF1Qy9QLEVBQUUsR0FBR2lRLFNBQVMsQ0FBQzFoQixNQUF0RCxFQUE4RHlSLEVBQUUsRUFBaEUsRUFBb0U7QUFDbEUsVUFBSWtRLFlBQVksR0FBR0QsU0FBUyxDQUFDalEsRUFBRCxDQUE1QjtBQUFBLFVBQ0ltUSxLQUFLLEdBQUdELFlBQVksQ0FBQyxDQUFELENBRHhCO0FBQUEsVUFFSUUsU0FBUyxHQUFHRixZQUFZLENBQUMsQ0FBRCxDQUY1QjtBQUdBLFVBQUl0VixDQUFDLEdBQUd1VixLQUFLLENBQUN4SixJQUFOLENBQVdyVCxDQUFYLENBQVI7O0FBRUEsVUFBSXNILENBQUosRUFBTztBQUNMLGVBQU93VixTQUFTLENBQUN4VixDQUFELENBQWhCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFPLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FBUDtBQUNEOztBQUVELFdBQVN5VixXQUFULEdBQXVCO0FBQ3JCLFNBQUssSUFBSUMsS0FBSyxHQUFHbmYsU0FBUyxDQUFDNUMsTUFBdEIsRUFBOEJ5SSxJQUFJLEdBQUcsSUFBSVosS0FBSixDQUFVa2EsS0FBVixDQUFyQyxFQUF1REMsS0FBSyxHQUFHLENBQXBFLEVBQXVFQSxLQUFLLEdBQUdELEtBQS9FLEVBQXNGQyxLQUFLLEVBQTNGLEVBQStGO0FBQzdGdlosTUFBQUEsSUFBSSxDQUFDdVosS0FBRCxDQUFKLEdBQWNwZixTQUFTLENBQUNvZixLQUFELENBQXZCO0FBQ0Q7O0FBRUQsV0FBTyxVQUFVNUksS0FBVixFQUFpQmdJLE1BQWpCLEVBQXlCO0FBQzlCLFVBQUlhLEdBQUcsR0FBRyxFQUFWO0FBQ0EsVUFBSWxpQixDQUFKOztBQUVBLFdBQUtBLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBRzBJLElBQUksQ0FBQ3pJLE1BQXJCLEVBQTZCRCxDQUFDLEVBQTlCLEVBQWtDO0FBQ2hDa2lCLFFBQUFBLEdBQUcsQ0FBQ3haLElBQUksQ0FBQzFJLENBQUQsQ0FBTCxDQUFILEdBQWV5SixZQUFZLENBQUM0UCxLQUFLLENBQUNnSSxNQUFNLEdBQUdyaEIsQ0FBVixDQUFOLENBQTNCO0FBQ0Q7O0FBRUQsYUFBTyxDQUFDa2lCLEdBQUQsRUFBTSxJQUFOLEVBQVliLE1BQU0sR0FBR3JoQixDQUFyQixDQUFQO0FBQ0QsS0FURDtBQVVELEdBbDNGNkIsQ0FrM0Y1Qjs7O0FBR0YsTUFBSW1pQixXQUFXLEdBQUcsaUNBQWxCO0FBQUEsTUFDSUMsZ0JBQWdCLEdBQUcsb0RBRHZCO0FBQUEsTUFFSUMsWUFBWSxHQUFHeEssTUFBTSxDQUFDLEtBQUt1SyxnQkFBZ0IsQ0FBQ3RLLE1BQXRCLEdBQStCcUssV0FBVyxDQUFDckssTUFBM0MsR0FBb0QsR0FBckQsQ0FGekI7QUFBQSxNQUdJd0sscUJBQXFCLEdBQUd6SyxNQUFNLENBQUMsU0FBU3dLLFlBQVksQ0FBQ3ZLLE1BQXRCLEdBQStCLElBQWhDLENBSGxDO0FBQUEsTUFJSXlLLFdBQVcsR0FBRyw2Q0FKbEI7QUFBQSxNQUtJQyxZQUFZLEdBQUcsNkJBTG5CO0FBQUEsTUFNSUMsZUFBZSxHQUFHLGtCQU50QjtBQUFBLE1BT0lDLGtCQUFrQixHQUFHWCxXQUFXLENBQUMsVUFBRCxFQUFhLFlBQWIsRUFBMkIsU0FBM0IsQ0FQcEM7QUFBQSxNQVFJWSxxQkFBcUIsR0FBR1osV0FBVyxDQUFDLE1BQUQsRUFBUyxTQUFULENBUnZDO0FBQUEsTUFTSWEsV0FBVyxHQUFHLHVCQVRsQjtBQUFBLE1BVUk7QUFDSkMsRUFBQUEsWUFBWSxHQUFHaEwsTUFBTSxDQUFDdUssZ0JBQWdCLENBQUN0SyxNQUFqQixHQUEwQixPQUExQixHQUFvQ3FLLFdBQVcsQ0FBQ3JLLE1BQWhELEdBQXlELElBQXpELEdBQWdFcEosU0FBUyxDQUFDb0osTUFBMUUsR0FBbUYsS0FBcEYsQ0FYckI7QUFBQSxNQVlJZ0wscUJBQXFCLEdBQUdqTCxNQUFNLENBQUMsU0FBU2dMLFlBQVksQ0FBQy9LLE1BQXRCLEdBQStCLElBQWhDLENBWmxDOztBQWNBLFdBQVNpTCxJQUFULENBQWExSixLQUFiLEVBQW9CTixHQUFwQixFQUF5QmlLLFFBQXpCLEVBQW1DO0FBQ2pDLFFBQUkxVyxDQUFDLEdBQUcrTSxLQUFLLENBQUNOLEdBQUQsQ0FBYjtBQUNBLFdBQU8vUixXQUFXLENBQUNzRixDQUFELENBQVgsR0FBaUIwVyxRQUFqQixHQUE0QnZaLFlBQVksQ0FBQzZDLENBQUQsQ0FBL0M7QUFDRDs7QUFFRCxXQUFTMlcsYUFBVCxDQUF1QjVKLEtBQXZCLEVBQThCZ0ksTUFBOUIsRUFBc0M7QUFDcEMsUUFBSTZCLElBQUksR0FBRztBQUNUL2QsTUFBQUEsSUFBSSxFQUFFNGQsSUFBRyxDQUFDMUosS0FBRCxFQUFRZ0ksTUFBUixDQURBO0FBRVRqYyxNQUFBQSxLQUFLLEVBQUUyZCxJQUFHLENBQUMxSixLQUFELEVBQVFnSSxNQUFNLEdBQUcsQ0FBakIsRUFBb0IsQ0FBcEIsQ0FGRDtBQUdUaGMsTUFBQUEsR0FBRyxFQUFFMGQsSUFBRyxDQUFDMUosS0FBRCxFQUFRZ0ksTUFBTSxHQUFHLENBQWpCLEVBQW9CLENBQXBCO0FBSEMsS0FBWDtBQUtBLFdBQU8sQ0FBQzZCLElBQUQsRUFBTyxJQUFQLEVBQWE3QixNQUFNLEdBQUcsQ0FBdEIsQ0FBUDtBQUNEOztBQUVELFdBQVM4QixjQUFULENBQXdCOUosS0FBeEIsRUFBK0JnSSxNQUEvQixFQUF1QztBQUNyQyxRQUFJNkIsSUFBSSxHQUFHO0FBQ1R2ZCxNQUFBQSxJQUFJLEVBQUVvZCxJQUFHLENBQUMxSixLQUFELEVBQVFnSSxNQUFSLEVBQWdCLENBQWhCLENBREE7QUFFVHpiLE1BQUFBLE1BQU0sRUFBRW1kLElBQUcsQ0FBQzFKLEtBQUQsRUFBUWdJLE1BQU0sR0FBRyxDQUFqQixFQUFvQixDQUFwQixDQUZGO0FBR1R2YixNQUFBQSxNQUFNLEVBQUVpZCxJQUFHLENBQUMxSixLQUFELEVBQVFnSSxNQUFNLEdBQUcsQ0FBakIsRUFBb0IsQ0FBcEIsQ0FIRjtBQUlUcFcsTUFBQUEsV0FBVyxFQUFFckIsV0FBVyxDQUFDeVAsS0FBSyxDQUFDZ0ksTUFBTSxHQUFHLENBQVYsQ0FBTjtBQUpmLEtBQVg7QUFNQSxXQUFPLENBQUM2QixJQUFELEVBQU8sSUFBUCxFQUFhN0IsTUFBTSxHQUFHLENBQXRCLENBQVA7QUFDRDs7QUFFRCxXQUFTK0IsZ0JBQVQsQ0FBMEIvSixLQUExQixFQUFpQ2dJLE1BQWpDLEVBQXlDO0FBQ3ZDLFFBQUlnQyxLQUFLLEdBQUcsQ0FBQ2hLLEtBQUssQ0FBQ2dJLE1BQUQsQ0FBTixJQUFrQixDQUFDaEksS0FBSyxDQUFDZ0ksTUFBTSxHQUFHLENBQVYsQ0FBcEM7QUFBQSxRQUNJaUMsVUFBVSxHQUFHdFcsWUFBWSxDQUFDcU0sS0FBSyxDQUFDZ0ksTUFBTSxHQUFHLENBQVYsQ0FBTixFQUFvQmhJLEtBQUssQ0FBQ2dJLE1BQU0sR0FBRyxDQUFWLENBQXpCLENBRDdCO0FBQUEsUUFFSTVMLElBQUksR0FBRzROLEtBQUssR0FBRyxJQUFILEdBQVV0SixlQUFlLENBQUNuWCxRQUFoQixDQUF5QjBnQixVQUF6QixDQUYxQjtBQUdBLFdBQU8sQ0FBQyxFQUFELEVBQUs3TixJQUFMLEVBQVc0TCxNQUFNLEdBQUcsQ0FBcEIsQ0FBUDtBQUNEOztBQUVELFdBQVNrQyxlQUFULENBQXlCbEssS0FBekIsRUFBZ0NnSSxNQUFoQyxFQUF3QztBQUN0QyxRQUFJNUwsSUFBSSxHQUFHNEQsS0FBSyxDQUFDZ0ksTUFBRCxDQUFMLEdBQWdCcEksUUFBUSxDQUFDaFksTUFBVCxDQUFnQm9ZLEtBQUssQ0FBQ2dJLE1BQUQsQ0FBckIsQ0FBaEIsR0FBaUQsSUFBNUQ7QUFDQSxXQUFPLENBQUMsRUFBRCxFQUFLNUwsSUFBTCxFQUFXNEwsTUFBTSxHQUFHLENBQXBCLENBQVA7QUFDRCxHQXI2RjZCLENBcTZGNUI7OztBQUdGLE1BQUltQyxXQUFXLEdBQUcsMEpBQWxCOztBQUVBLFdBQVNDLGtCQUFULENBQTRCcEssS0FBNUIsRUFBbUM7QUFDakMsUUFBSXFLLE9BQU8sR0FBR3JLLEtBQUssQ0FBQyxDQUFELENBQW5CO0FBQUEsUUFDSXNLLFFBQVEsR0FBR3RLLEtBQUssQ0FBQyxDQUFELENBRHBCO0FBQUEsUUFFSXVLLE9BQU8sR0FBR3ZLLEtBQUssQ0FBQyxDQUFELENBRm5CO0FBQUEsUUFHSXdLLE1BQU0sR0FBR3hLLEtBQUssQ0FBQyxDQUFELENBSGxCO0FBQUEsUUFJSXlLLE9BQU8sR0FBR3pLLEtBQUssQ0FBQyxDQUFELENBSm5CO0FBQUEsUUFLSTBLLFNBQVMsR0FBRzFLLEtBQUssQ0FBQyxDQUFELENBTHJCO0FBQUEsUUFNSTJLLFNBQVMsR0FBRzNLLEtBQUssQ0FBQyxDQUFELENBTnJCO0FBQUEsUUFPSTRLLGVBQWUsR0FBRzVLLEtBQUssQ0FBQyxDQUFELENBUDNCO0FBUUEsV0FBTyxDQUFDO0FBQ05oSixNQUFBQSxLQUFLLEVBQUU1RyxZQUFZLENBQUNpYSxPQUFELENBRGI7QUFFTnpVLE1BQUFBLE1BQU0sRUFBRXhGLFlBQVksQ0FBQ2thLFFBQUQsQ0FGZDtBQUdOcFQsTUFBQUEsS0FBSyxFQUFFOUcsWUFBWSxDQUFDbWEsT0FBRCxDQUhiO0FBSU5wVCxNQUFBQSxJQUFJLEVBQUUvRyxZQUFZLENBQUNvYSxNQUFELENBSlo7QUFLTjFWLE1BQUFBLEtBQUssRUFBRTFFLFlBQVksQ0FBQ3FhLE9BQUQsQ0FMYjtBQU1OMVYsTUFBQUEsT0FBTyxFQUFFM0UsWUFBWSxDQUFDc2EsU0FBRCxDQU5mO0FBT050VCxNQUFBQSxPQUFPLEVBQUVoSCxZQUFZLENBQUN1YSxTQUFELENBUGY7QUFRTkUsTUFBQUEsWUFBWSxFQUFFdGEsV0FBVyxDQUFDcWEsZUFBRDtBQVJuQixLQUFELENBQVA7QUFVRCxHQTc3RjZCLENBNjdGNUI7QUFDRjtBQUNBOzs7QUFHQSxNQUFJRSxVQUFVLEdBQUc7QUFDZkMsSUFBQUEsR0FBRyxFQUFFLENBRFU7QUFFZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBRks7QUFHZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBSEs7QUFJZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBSks7QUFLZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBTEs7QUFNZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBTks7QUFPZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBUEs7QUFRZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLLEVBUks7QUFTZkMsSUFBQUEsR0FBRyxFQUFFLENBQUMsQ0FBRCxHQUFLO0FBVEssR0FBakI7O0FBWUEsV0FBU0MsV0FBVCxDQUFxQkMsVUFBckIsRUFBaUNwQixPQUFqQyxFQUEwQ0MsUUFBMUMsRUFBb0RFLE1BQXBELEVBQTREQyxPQUE1RCxFQUFxRUMsU0FBckUsRUFBZ0ZDLFNBQWhGLEVBQTJGO0FBQ3pGLFFBQUllLE1BQU0sR0FBRztBQUNYNWYsTUFBQUEsSUFBSSxFQUFFdWUsT0FBTyxDQUFDempCLE1BQVIsS0FBbUIsQ0FBbkIsR0FBdUJ3TCxjQUFjLENBQUNoQyxZQUFZLENBQUNpYSxPQUFELENBQWIsQ0FBckMsR0FBK0RqYSxZQUFZLENBQUNpYSxPQUFELENBRHRFO0FBRVh0ZSxNQUFBQSxLQUFLLEVBQUUySixXQUFXLENBQUMvTCxPQUFaLENBQW9CMmdCLFFBQXBCLElBQWdDLENBRjVCO0FBR1h0ZSxNQUFBQSxHQUFHLEVBQUVvRSxZQUFZLENBQUNvYSxNQUFELENBSE47QUFJWGxlLE1BQUFBLElBQUksRUFBRThELFlBQVksQ0FBQ3FhLE9BQUQsQ0FKUDtBQUtYbGUsTUFBQUEsTUFBTSxFQUFFNkQsWUFBWSxDQUFDc2EsU0FBRDtBQUxULEtBQWI7QUFPQSxRQUFJQyxTQUFKLEVBQWVlLE1BQU0sQ0FBQ2pmLE1BQVAsR0FBZ0IyRCxZQUFZLENBQUN1YSxTQUFELENBQTVCOztBQUVmLFFBQUljLFVBQUosRUFBZ0I7QUFDZEMsTUFBQUEsTUFBTSxDQUFDdGYsT0FBUCxHQUFpQnFmLFVBQVUsQ0FBQzdrQixNQUFYLEdBQW9CLENBQXBCLEdBQXdCaVAsWUFBWSxDQUFDbE0sT0FBYixDQUFxQjhoQixVQUFyQixJQUFtQyxDQUEzRCxHQUErRDNWLGFBQWEsQ0FBQ25NLE9BQWQsQ0FBc0I4aEIsVUFBdEIsSUFBb0MsQ0FBcEg7QUFDRDs7QUFFRCxXQUFPQyxNQUFQO0FBQ0QsR0E3OUY2QixDQTY5RjVCOzs7QUFHRixNQUFJQyxPQUFPLEdBQUcsaU1BQWQ7O0FBRUEsV0FBU0MsY0FBVCxDQUF3QjVMLEtBQXhCLEVBQStCO0FBQzdCLFFBQUl5TCxVQUFVLEdBQUd6TCxLQUFLLENBQUMsQ0FBRCxDQUF0QjtBQUFBLFFBQ0l3SyxNQUFNLEdBQUd4SyxLQUFLLENBQUMsQ0FBRCxDQURsQjtBQUFBLFFBRUlzSyxRQUFRLEdBQUd0SyxLQUFLLENBQUMsQ0FBRCxDQUZwQjtBQUFBLFFBR0lxSyxPQUFPLEdBQUdySyxLQUFLLENBQUMsQ0FBRCxDQUhuQjtBQUFBLFFBSUl5SyxPQUFPLEdBQUd6SyxLQUFLLENBQUMsQ0FBRCxDQUpuQjtBQUFBLFFBS0kwSyxTQUFTLEdBQUcxSyxLQUFLLENBQUMsQ0FBRCxDQUxyQjtBQUFBLFFBTUkySyxTQUFTLEdBQUczSyxLQUFLLENBQUMsQ0FBRCxDQU5yQjtBQUFBLFFBT0k2TCxTQUFTLEdBQUc3TCxLQUFLLENBQUMsQ0FBRCxDQVByQjtBQUFBLFFBUUk4TCxTQUFTLEdBQUc5TCxLQUFLLENBQUMsQ0FBRCxDQVJyQjtBQUFBLFFBU0lwTSxVQUFVLEdBQUdvTSxLQUFLLENBQUMsRUFBRCxDQVR0QjtBQUFBLFFBVUluTSxZQUFZLEdBQUdtTSxLQUFLLENBQUMsRUFBRCxDQVZ4QjtBQUFBLFFBV0kwTCxNQUFNLEdBQUdGLFdBQVcsQ0FBQ0MsVUFBRCxFQUFhcEIsT0FBYixFQUFzQkMsUUFBdEIsRUFBZ0NFLE1BQWhDLEVBQXdDQyxPQUF4QyxFQUFpREMsU0FBakQsRUFBNERDLFNBQTVELENBWHhCO0FBWUEsUUFBSTlWLE1BQUo7O0FBRUEsUUFBSWdYLFNBQUosRUFBZTtBQUNiaFgsTUFBQUEsTUFBTSxHQUFHaVcsVUFBVSxDQUFDZSxTQUFELENBQW5CO0FBQ0QsS0FGRCxNQUVPLElBQUlDLFNBQUosRUFBZTtBQUNwQmpYLE1BQUFBLE1BQU0sR0FBRyxDQUFUO0FBQ0QsS0FGTSxNQUVBO0FBQ0xBLE1BQUFBLE1BQU0sR0FBR2xCLFlBQVksQ0FBQ0MsVUFBRCxFQUFhQyxZQUFiLENBQXJCO0FBQ0Q7O0FBRUQsV0FBTyxDQUFDNlgsTUFBRCxFQUFTLElBQUloTCxlQUFKLENBQW9CN0wsTUFBcEIsQ0FBVCxDQUFQO0FBQ0Q7O0FBRUQsV0FBU2tYLGlCQUFULENBQTJCcGdCLENBQTNCLEVBQThCO0FBQzVCO0FBQ0EsV0FBT0EsQ0FBQyxDQUFDK0gsT0FBRixDQUFVLG1CQUFWLEVBQStCLEdBQS9CLEVBQW9DQSxPQUFwQyxDQUE0QyxVQUE1QyxFQUF3RCxHQUF4RCxFQUE2RHNZLElBQTdELEVBQVA7QUFDRCxHQS8vRjZCLENBKy9GNUI7OztBQUdGLE1BQUlDLE9BQU8sR0FBRyw0SEFBZDtBQUFBLE1BQ0lDLE1BQU0sR0FBRyxzSkFEYjtBQUFBLE1BRUlDLEtBQUssR0FBRywySEFGWjs7QUFJQSxXQUFTQyxtQkFBVCxDQUE2QnBNLEtBQTdCLEVBQW9DO0FBQ2xDLFFBQUl5TCxVQUFVLEdBQUd6TCxLQUFLLENBQUMsQ0FBRCxDQUF0QjtBQUFBLFFBQ0l3SyxNQUFNLEdBQUd4SyxLQUFLLENBQUMsQ0FBRCxDQURsQjtBQUFBLFFBRUlzSyxRQUFRLEdBQUd0SyxLQUFLLENBQUMsQ0FBRCxDQUZwQjtBQUFBLFFBR0lxSyxPQUFPLEdBQUdySyxLQUFLLENBQUMsQ0FBRCxDQUhuQjtBQUFBLFFBSUl5SyxPQUFPLEdBQUd6SyxLQUFLLENBQUMsQ0FBRCxDQUpuQjtBQUFBLFFBS0kwSyxTQUFTLEdBQUcxSyxLQUFLLENBQUMsQ0FBRCxDQUxyQjtBQUFBLFFBTUkySyxTQUFTLEdBQUczSyxLQUFLLENBQUMsQ0FBRCxDQU5yQjtBQUFBLFFBT0kwTCxNQUFNLEdBQUdGLFdBQVcsQ0FBQ0MsVUFBRCxFQUFhcEIsT0FBYixFQUFzQkMsUUFBdEIsRUFBZ0NFLE1BQWhDLEVBQXdDQyxPQUF4QyxFQUFpREMsU0FBakQsRUFBNERDLFNBQTVELENBUHhCO0FBUUEsV0FBTyxDQUFDZSxNQUFELEVBQVNoTCxlQUFlLENBQUNDLFdBQXpCLENBQVA7QUFDRDs7QUFFRCxXQUFTMEwsWUFBVCxDQUFzQnJNLEtBQXRCLEVBQTZCO0FBQzNCLFFBQUl5TCxVQUFVLEdBQUd6TCxLQUFLLENBQUMsQ0FBRCxDQUF0QjtBQUFBLFFBQ0lzSyxRQUFRLEdBQUd0SyxLQUFLLENBQUMsQ0FBRCxDQURwQjtBQUFBLFFBRUl3SyxNQUFNLEdBQUd4SyxLQUFLLENBQUMsQ0FBRCxDQUZsQjtBQUFBLFFBR0l5SyxPQUFPLEdBQUd6SyxLQUFLLENBQUMsQ0FBRCxDQUhuQjtBQUFBLFFBSUkwSyxTQUFTLEdBQUcxSyxLQUFLLENBQUMsQ0FBRCxDQUpyQjtBQUFBLFFBS0kySyxTQUFTLEdBQUczSyxLQUFLLENBQUMsQ0FBRCxDQUxyQjtBQUFBLFFBTUlxSyxPQUFPLEdBQUdySyxLQUFLLENBQUMsQ0FBRCxDQU5uQjtBQUFBLFFBT0kwTCxNQUFNLEdBQUdGLFdBQVcsQ0FBQ0MsVUFBRCxFQUFhcEIsT0FBYixFQUFzQkMsUUFBdEIsRUFBZ0NFLE1BQWhDLEVBQXdDQyxPQUF4QyxFQUFpREMsU0FBakQsRUFBNERDLFNBQTVELENBUHhCO0FBUUEsV0FBTyxDQUFDZSxNQUFELEVBQVNoTCxlQUFlLENBQUNDLFdBQXpCLENBQVA7QUFDRDs7QUFFRCxNQUFJMkwsNEJBQTRCLEdBQUdsRixjQUFjLENBQUM4QixXQUFELEVBQWNELHFCQUFkLENBQWpEO0FBQ0EsTUFBSXNELDZCQUE2QixHQUFHbkYsY0FBYyxDQUFDK0IsWUFBRCxFQUFlRixxQkFBZixDQUFsRDtBQUNBLE1BQUl1RCxnQ0FBZ0MsR0FBR3BGLGNBQWMsQ0FBQ2dDLGVBQUQsRUFBa0JILHFCQUFsQixDQUFyRDtBQUNBLE1BQUl3RCxvQkFBb0IsR0FBR3JGLGNBQWMsQ0FBQzRCLFlBQUQsQ0FBekM7QUFDQSxNQUFJMEQsMEJBQTBCLEdBQUdqRixpQkFBaUIsQ0FBQ21DLGFBQUQsRUFBZ0JFLGNBQWhCLEVBQWdDQyxnQkFBaEMsQ0FBbEQ7QUFDQSxNQUFJNEMsMkJBQTJCLEdBQUdsRixpQkFBaUIsQ0FBQzRCLGtCQUFELEVBQXFCUyxjQUFyQixFQUFxQ0MsZ0JBQXJDLENBQW5EO0FBQ0EsTUFBSTZDLDRCQUE0QixHQUFHbkYsaUJBQWlCLENBQUM2QixxQkFBRCxFQUF3QlEsY0FBeEIsQ0FBcEQ7QUFDQSxNQUFJK0MsdUJBQXVCLEdBQUdwRixpQkFBaUIsQ0FBQ3FDLGNBQUQsRUFBaUJDLGdCQUFqQixDQUEvQztBQUNBOzs7O0FBSUEsV0FBUytDLFlBQVQsQ0FBc0JuaEIsQ0FBdEIsRUFBeUI7QUFDdkIsV0FBT3VjLEtBQUssQ0FBQ3ZjLENBQUQsRUFBSSxDQUFDMmdCLDRCQUFELEVBQStCSSwwQkFBL0IsQ0FBSixFQUFnRSxDQUFDSCw2QkFBRCxFQUFnQ0ksMkJBQWhDLENBQWhFLEVBQThILENBQUNILGdDQUFELEVBQW1DSSw0QkFBbkMsQ0FBOUgsRUFBZ00sQ0FBQ0gsb0JBQUQsRUFBdUJJLHVCQUF2QixDQUFoTSxDQUFaO0FBQ0Q7O0FBQ0QsV0FBU0UsZ0JBQVQsQ0FBMEJwaEIsQ0FBMUIsRUFBNkI7QUFDM0IsV0FBT3VjLEtBQUssQ0FBQzZELGlCQUFpQixDQUFDcGdCLENBQUQsQ0FBbEIsRUFBdUIsQ0FBQ2dnQixPQUFELEVBQVVDLGNBQVYsQ0FBdkIsQ0FBWjtBQUNEOztBQUNELFdBQVNvQixhQUFULENBQXVCcmhCLENBQXZCLEVBQTBCO0FBQ3hCLFdBQU91YyxLQUFLLENBQUN2YyxDQUFELEVBQUksQ0FBQ3NnQixPQUFELEVBQVVHLG1CQUFWLENBQUosRUFBb0MsQ0FBQ0YsTUFBRCxFQUFTRSxtQkFBVCxDQUFwQyxFQUFtRSxDQUFDRCxLQUFELEVBQVFFLFlBQVIsQ0FBbkUsQ0FBWjtBQUNEOztBQUNELFdBQVNZLGdCQUFULENBQTBCdGhCLENBQTFCLEVBQTZCO0FBQzNCLFdBQU91YyxLQUFLLENBQUN2YyxDQUFELEVBQUksQ0FBQ3dlLFdBQUQsRUFBY0Msa0JBQWQsQ0FBSixDQUFaO0FBQ0Q7O0FBQ0QsTUFBSThDLDRCQUE0QixHQUFHOUYsY0FBYyxDQUFDbUMsV0FBRCxFQUFjRSxxQkFBZCxDQUFqRDtBQUNBLE1BQUkwRCxvQkFBb0IsR0FBRy9GLGNBQWMsQ0FBQ29DLFlBQUQsQ0FBekM7QUFDQSxNQUFJNEQsa0NBQWtDLEdBQUczRixpQkFBaUIsQ0FBQ21DLGFBQUQsRUFBZ0JFLGNBQWhCLEVBQWdDQyxnQkFBaEMsRUFBa0RHLGVBQWxELENBQTFEO0FBQ0EsTUFBSW1ELCtCQUErQixHQUFHNUYsaUJBQWlCLENBQUNxQyxjQUFELEVBQWlCQyxnQkFBakIsRUFBbUNHLGVBQW5DLENBQXZEOztBQUNBLFdBQVNvRCxRQUFULENBQWtCM2hCLENBQWxCLEVBQXFCO0FBQ25CLFdBQU91YyxLQUFLLENBQUN2YyxDQUFELEVBQUksQ0FBQ3VoQiw0QkFBRCxFQUErQkUsa0NBQS9CLENBQUosRUFBd0UsQ0FBQ0Qsb0JBQUQsRUFBdUJFLCtCQUF2QixDQUF4RSxDQUFaO0FBQ0Q7O0FBRUQsTUFBSUUsT0FBTyxHQUFHLGtCQUFkLENBOWpHOEIsQ0E4akdJOztBQUVsQyxNQUFJQyxjQUFjLEdBQUc7QUFDbkJ0VyxJQUFBQSxLQUFLLEVBQUU7QUFDTEMsTUFBQUEsSUFBSSxFQUFFLENBREQ7QUFFTHJDLE1BQUFBLEtBQUssRUFBRSxJQUFJLEVBRk47QUFHTEMsTUFBQUEsT0FBTyxFQUFFLElBQUksRUFBSixHQUFTLEVBSGI7QUFJTHFDLE1BQUFBLE9BQU8sRUFBRSxJQUFJLEVBQUosR0FBUyxFQUFULEdBQWMsRUFKbEI7QUFLTHlULE1BQUFBLFlBQVksRUFBRSxJQUFJLEVBQUosR0FBUyxFQUFULEdBQWMsRUFBZCxHQUFtQjtBQUw1QixLQURZO0FBUW5CMVQsSUFBQUEsSUFBSSxFQUFFO0FBQ0pyQyxNQUFBQSxLQUFLLEVBQUUsRUFESDtBQUVKQyxNQUFBQSxPQUFPLEVBQUUsS0FBSyxFQUZWO0FBR0pxQyxNQUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFMLEdBQVUsRUFIZjtBQUlKeVQsTUFBQUEsWUFBWSxFQUFFLEtBQUssRUFBTCxHQUFVLEVBQVYsR0FBZTtBQUp6QixLQVJhO0FBY25CL1YsSUFBQUEsS0FBSyxFQUFFO0FBQ0xDLE1BQUFBLE9BQU8sRUFBRSxFQURKO0FBRUxxQyxNQUFBQSxPQUFPLEVBQUUsS0FBSyxFQUZUO0FBR0x5VCxNQUFBQSxZQUFZLEVBQUUsS0FBSyxFQUFMLEdBQVU7QUFIbkIsS0FkWTtBQW1CbkI5VixJQUFBQSxPQUFPLEVBQUU7QUFDUHFDLE1BQUFBLE9BQU8sRUFBRSxFQURGO0FBRVB5VCxNQUFBQSxZQUFZLEVBQUUsS0FBSztBQUZaLEtBbkJVO0FBdUJuQnpULElBQUFBLE9BQU8sRUFBRTtBQUNQeVQsTUFBQUEsWUFBWSxFQUFFO0FBRFA7QUF2QlUsR0FBckI7QUFBQSxNQTJCSTRDLFlBQVksR0FBR3htQixNQUFNLENBQUM0TCxNQUFQLENBQWM7QUFDL0JtRSxJQUFBQSxLQUFLLEVBQUU7QUFDTHBCLE1BQUFBLE1BQU0sRUFBRSxFQURIO0FBRUxzQixNQUFBQSxLQUFLLEVBQUUsRUFGRjtBQUdMQyxNQUFBQSxJQUFJLEVBQUUsR0FIRDtBQUlMckMsTUFBQUEsS0FBSyxFQUFFLE1BQU0sRUFKUjtBQUtMQyxNQUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFOLEdBQVcsRUFMZjtBQU1McUMsTUFBQUEsT0FBTyxFQUFFLE1BQU0sRUFBTixHQUFXLEVBQVgsR0FBZ0IsRUFOcEI7QUFPTHlULE1BQUFBLFlBQVksRUFBRSxNQUFNLEVBQU4sR0FBVyxFQUFYLEdBQWdCLEVBQWhCLEdBQXFCO0FBUDlCLEtBRHdCO0FBVS9CNVQsSUFBQUEsUUFBUSxFQUFFO0FBQ1JyQixNQUFBQSxNQUFNLEVBQUUsQ0FEQTtBQUVSc0IsTUFBQUEsS0FBSyxFQUFFLEVBRkM7QUFHUkMsTUFBQUEsSUFBSSxFQUFFLEVBSEU7QUFJUnJDLE1BQUFBLEtBQUssRUFBRSxLQUFLLEVBSko7QUFLUkMsTUFBQUEsT0FBTyxFQUFFLEtBQUssRUFBTCxHQUFVLEVBTFg7QUFNUjhWLE1BQUFBLFlBQVksRUFBRSxLQUFLLEVBQUwsR0FBVSxFQUFWLEdBQWUsRUFBZixHQUFvQjtBQU4xQixLQVZxQjtBQWtCL0JqVixJQUFBQSxNQUFNLEVBQUU7QUFDTnNCLE1BQUFBLEtBQUssRUFBRSxDQUREO0FBRU5DLE1BQUFBLElBQUksRUFBRSxFQUZBO0FBR05yQyxNQUFBQSxLQUFLLEVBQUUsS0FBSyxFQUhOO0FBSU5DLE1BQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUwsR0FBVSxFQUpiO0FBS05xQyxNQUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFMLEdBQVUsRUFBVixHQUFlLEVBTGxCO0FBTU55VCxNQUFBQSxZQUFZLEVBQUUsS0FBSyxFQUFMLEdBQVUsRUFBVixHQUFlLEVBQWYsR0FBb0I7QUFONUI7QUFsQnVCLEdBQWQsRUEwQmhCMkMsY0ExQmdCLENBM0JuQjtBQUFBLE1Bc0RJRSxrQkFBa0IsR0FBRyxXQUFXLEdBdERwQztBQUFBLE1BdURJQyxtQkFBbUIsR0FBRyxXQUFXLElBdkRyQztBQUFBLE1Bd0RJQyxjQUFjLEdBQUczbUIsTUFBTSxDQUFDNEwsTUFBUCxDQUFjO0FBQ2pDbUUsSUFBQUEsS0FBSyxFQUFFO0FBQ0xwQixNQUFBQSxNQUFNLEVBQUUsRUFESDtBQUVMc0IsTUFBQUEsS0FBSyxFQUFFd1csa0JBQWtCLEdBQUcsQ0FGdkI7QUFHTHZXLE1BQUFBLElBQUksRUFBRXVXLGtCQUhEO0FBSUw1WSxNQUFBQSxLQUFLLEVBQUU0WSxrQkFBa0IsR0FBRyxFQUp2QjtBQUtMM1ksTUFBQUEsT0FBTyxFQUFFMlksa0JBQWtCLEdBQUcsRUFBckIsR0FBMEIsRUFMOUI7QUFNTHRXLE1BQUFBLE9BQU8sRUFBRXNXLGtCQUFrQixHQUFHLEVBQXJCLEdBQTBCLEVBQTFCLEdBQStCLEVBTm5DO0FBT0w3QyxNQUFBQSxZQUFZLEVBQUU2QyxrQkFBa0IsR0FBRyxFQUFyQixHQUEwQixFQUExQixHQUErQixFQUEvQixHQUFvQztBQVA3QyxLQUQwQjtBQVVqQ3pXLElBQUFBLFFBQVEsRUFBRTtBQUNSckIsTUFBQUEsTUFBTSxFQUFFLENBREE7QUFFUnNCLE1BQUFBLEtBQUssRUFBRXdXLGtCQUFrQixHQUFHLEVBRnBCO0FBR1J2VyxNQUFBQSxJQUFJLEVBQUV1VyxrQkFBa0IsR0FBRyxDQUhuQjtBQUlSNVksTUFBQUEsS0FBSyxFQUFFNFksa0JBQWtCLEdBQUcsRUFBckIsR0FBMEIsQ0FKekI7QUFLUjNZLE1BQUFBLE9BQU8sRUFBRTJZLGtCQUFrQixHQUFHLEVBQXJCLEdBQTBCLEVBQTFCLEdBQStCLENBTGhDO0FBTVJ0VyxNQUFBQSxPQUFPLEVBQUVzVyxrQkFBa0IsR0FBRyxFQUFyQixHQUEwQixFQUExQixHQUErQixFQUEvQixHQUFvQyxDQU5yQztBQU9SN0MsTUFBQUEsWUFBWSxFQUFFNkMsa0JBQWtCLEdBQUcsRUFBckIsR0FBMEIsRUFBMUIsR0FBK0IsRUFBL0IsR0FBb0MsSUFBcEMsR0FBMkM7QUFQakQsS0FWdUI7QUFtQmpDOVgsSUFBQUEsTUFBTSxFQUFFO0FBQ05zQixNQUFBQSxLQUFLLEVBQUV5VyxtQkFBbUIsR0FBRyxDQUR2QjtBQUVOeFcsTUFBQUEsSUFBSSxFQUFFd1csbUJBRkE7QUFHTjdZLE1BQUFBLEtBQUssRUFBRTZZLG1CQUFtQixHQUFHLEVBSHZCO0FBSU41WSxNQUFBQSxPQUFPLEVBQUU0WSxtQkFBbUIsR0FBRyxFQUF0QixHQUEyQixFQUo5QjtBQUtOdlcsTUFBQUEsT0FBTyxFQUFFdVcsbUJBQW1CLEdBQUcsRUFBdEIsR0FBMkIsRUFBM0IsR0FBZ0MsRUFMbkM7QUFNTjlDLE1BQUFBLFlBQVksRUFBRThDLG1CQUFtQixHQUFHLEVBQXRCLEdBQTJCLEVBQTNCLEdBQWdDLEVBQWhDLEdBQXFDO0FBTjdDO0FBbkJ5QixHQUFkLEVBMkJsQkgsY0EzQmtCLENBeERyQixDQWhrRzhCLENBbXBHVjs7QUFFcEIsTUFBSUssWUFBWSxHQUFHLENBQUMsT0FBRCxFQUFVLFVBQVYsRUFBc0IsUUFBdEIsRUFBZ0MsT0FBaEMsRUFBeUMsTUFBekMsRUFBaUQsT0FBakQsRUFBMEQsU0FBMUQsRUFBcUUsU0FBckUsRUFBZ0YsY0FBaEYsQ0FBbkI7QUFDQSxNQUFJQyxZQUFZLEdBQUdELFlBQVksQ0FBQzFkLEtBQWIsQ0FBbUIsQ0FBbkIsRUFBc0I0ZCxPQUF0QixFQUFuQixDQXRwRzhCLENBc3BHc0I7O0FBRXBELFdBQVM1SCxLQUFULENBQWVwSixHQUFmLEVBQW9CcUosSUFBcEIsRUFBMEI0SCxLQUExQixFQUFpQztBQUMvQixRQUFJQSxLQUFLLEtBQUssS0FBSyxDQUFuQixFQUFzQjtBQUNwQkEsTUFBQUEsS0FBSyxHQUFHLEtBQVI7QUFDRCxLQUg4QixDQUsvQjs7O0FBQ0EsUUFBSUMsSUFBSSxHQUFHO0FBQ1RDLE1BQUFBLE1BQU0sRUFBRUYsS0FBSyxHQUFHNUgsSUFBSSxDQUFDOEgsTUFBUixHQUFpQmpuQixNQUFNLENBQUM0TCxNQUFQLENBQWMsRUFBZCxFQUFrQmtLLEdBQUcsQ0FBQ21SLE1BQXRCLEVBQThCOUgsSUFBSSxDQUFDOEgsTUFBTCxJQUFlLEVBQTdDLENBRHJCO0FBRVR0VCxNQUFBQSxHQUFHLEVBQUVtQyxHQUFHLENBQUNuQyxHQUFKLENBQVF1TCxLQUFSLENBQWNDLElBQUksQ0FBQ3hMLEdBQW5CLENBRkk7QUFHVHVULE1BQUFBLGtCQUFrQixFQUFFL0gsSUFBSSxDQUFDK0gsa0JBQUwsSUFBMkJwUixHQUFHLENBQUNvUjtBQUgxQyxLQUFYO0FBS0EsV0FBTyxJQUFJQyxRQUFKLENBQWFILElBQWIsQ0FBUDtBQUNEOztBQUVELFdBQVNJLFNBQVQsQ0FBbUIzaUIsQ0FBbkIsRUFBc0I7QUFDcEIsV0FBT0EsQ0FBQyxHQUFHLENBQUosR0FBUW9FLElBQUksQ0FBQ0MsS0FBTCxDQUFXckUsQ0FBWCxDQUFSLEdBQXdCb0UsSUFBSSxDQUFDd2UsSUFBTCxDQUFVNWlCLENBQVYsQ0FBL0I7QUFDRCxHQXhxRzZCLENBd3FHNUI7OztBQUdGLFdBQVM2aUIsT0FBVCxDQUFpQkMsTUFBakIsRUFBeUJDLE9BQXpCLEVBQWtDQyxRQUFsQyxFQUE0Q0MsS0FBNUMsRUFBbURDLE1BQW5ELEVBQTJEO0FBQ3pELFFBQUlDLElBQUksR0FBR0wsTUFBTSxDQUFDSSxNQUFELENBQU4sQ0FBZUYsUUFBZixDQUFYO0FBQUEsUUFDSUksR0FBRyxHQUFHTCxPQUFPLENBQUNDLFFBQUQsQ0FBUCxHQUFvQkcsSUFEOUI7QUFBQSxRQUVJRSxRQUFRLEdBQUdqZixJQUFJLENBQUNtRixJQUFMLENBQVU2WixHQUFWLE1BQW1CaGYsSUFBSSxDQUFDbUYsSUFBTCxDQUFVMFosS0FBSyxDQUFDQyxNQUFELENBQWYsQ0FGbEM7QUFBQSxRQUdJO0FBQ0pJLElBQUFBLEtBQUssR0FBRyxDQUFDRCxRQUFELElBQWFKLEtBQUssQ0FBQ0MsTUFBRCxDQUFMLEtBQWtCLENBQS9CLElBQW9DOWUsSUFBSSxDQUFDa0YsR0FBTCxDQUFTOFosR0FBVCxLQUFpQixDQUFyRCxHQUF5RFQsU0FBUyxDQUFDUyxHQUFELENBQWxFLEdBQTBFaGYsSUFBSSxDQUFDb0IsS0FBTCxDQUFXNGQsR0FBWCxDQUpsRjtBQUtBSCxJQUFBQSxLQUFLLENBQUNDLE1BQUQsQ0FBTCxJQUFpQkksS0FBakI7QUFDQVAsSUFBQUEsT0FBTyxDQUFDQyxRQUFELENBQVAsSUFBcUJNLEtBQUssR0FBR0gsSUFBN0I7QUFDRCxHQW5yRzZCLENBbXJHNUI7OztBQUdGLFdBQVNJLGVBQVQsQ0FBeUJULE1BQXpCLEVBQWlDVSxJQUFqQyxFQUF1QztBQUNyQ3BCLElBQUFBLFlBQVksQ0FBQy9lLE1BQWIsQ0FBb0IsVUFBVW9nQixRQUFWLEVBQW9COVUsT0FBcEIsRUFBNkI7QUFDL0MsVUFBSSxDQUFDMU0sV0FBVyxDQUFDdWhCLElBQUksQ0FBQzdVLE9BQUQsQ0FBTCxDQUFoQixFQUFpQztBQUMvQixZQUFJOFUsUUFBSixFQUFjO0FBQ1paLFVBQUFBLE9BQU8sQ0FBQ0MsTUFBRCxFQUFTVSxJQUFULEVBQWVDLFFBQWYsRUFBeUJELElBQXpCLEVBQStCN1UsT0FBL0IsQ0FBUDtBQUNEOztBQUVELGVBQU9BLE9BQVA7QUFDRCxPQU5ELE1BTU87QUFDTCxlQUFPOFUsUUFBUDtBQUNEO0FBQ0YsS0FWRCxFQVVHLElBVkg7QUFXRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7QUFlQSxNQUFJZixRQUFRO0FBQ1o7QUFDQSxjQUFZO0FBQ1Y7OztBQUdBLGFBQVNBLFFBQVQsQ0FBa0JnQixNQUFsQixFQUEwQjtBQUN4QixVQUFJQyxRQUFRLEdBQUdELE1BQU0sQ0FBQ2pCLGtCQUFQLEtBQThCLFVBQTlCLElBQTRDLEtBQTNEO0FBQ0E7Ozs7QUFJQSxXQUFLRCxNQUFMLEdBQWNrQixNQUFNLENBQUNsQixNQUFyQjtBQUNBOzs7O0FBSUEsV0FBS3RULEdBQUwsR0FBV3dVLE1BQU0sQ0FBQ3hVLEdBQVAsSUFBYytHLE1BQU0sQ0FBQy9aLE1BQVAsRUFBekI7QUFDQTs7OztBQUlBLFdBQUt1bUIsa0JBQUwsR0FBMEJrQixRQUFRLEdBQUcsVUFBSCxHQUFnQixRQUFsRDtBQUNBOzs7O0FBSUEsV0FBS0MsT0FBTCxHQUFlRixNQUFNLENBQUNFLE9BQVAsSUFBa0IsSUFBakM7QUFDQTs7OztBQUlBLFdBQUtkLE1BQUwsR0FBY2EsUUFBUSxHQUFHekIsY0FBSCxHQUFvQkgsWUFBMUM7QUFDQTs7OztBQUlBLFdBQUs4QixlQUFMLEdBQXVCLElBQXZCO0FBQ0Q7QUFDRDs7Ozs7Ozs7Ozs7QUFXQW5CLElBQUFBLFFBQVEsQ0FBQ2hLLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQnhOLEtBQXBCLEVBQTJCc0QsSUFBM0IsRUFBaUM7QUFDckQsYUFBT2tVLFFBQVEsQ0FBQ2pKLFVBQVQsQ0FBb0JsZSxNQUFNLENBQUM0TCxNQUFQLENBQWM7QUFDdkNnWSxRQUFBQSxZQUFZLEVBQUVqVTtBQUR5QixPQUFkLEVBRXhCc0QsSUFGd0IsQ0FBcEIsQ0FBUDtBQUdEO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUxBOztBQXlCQWtVLElBQUFBLFFBQVEsQ0FBQ2pKLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQi9WLEdBQXBCLEVBQXlCO0FBQzdDLFVBQUlBLEdBQUcsSUFBSSxJQUFQLElBQWUsUUFBT0EsR0FBUCxNQUFlLFFBQWxDLEVBQTRDO0FBQzFDLGNBQU0sSUFBSTlELG9CQUFKLENBQXlCLGtFQUFrRThELEdBQUcsS0FBSyxJQUFSLEdBQWUsTUFBZixXQUErQkEsR0FBL0IsQ0FBbEUsQ0FBekIsQ0FBTjtBQUNEOztBQUVELGFBQU8sSUFBSWdmLFFBQUosQ0FBYTtBQUNsQkYsUUFBQUEsTUFBTSxFQUFFNVosZUFBZSxDQUFDbEYsR0FBRCxFQUFNZ2YsUUFBUSxDQUFDb0IsYUFBZixFQUE4QixDQUFDLFFBQUQsRUFBVyxpQkFBWCxFQUE4QixvQkFBOUIsRUFBb0QsTUFBcEQsQ0FBMkQ7QUFBM0QsU0FBOUIsQ0FETDtBQUdsQjVVLFFBQUFBLEdBQUcsRUFBRStHLE1BQU0sQ0FBQ3dELFVBQVAsQ0FBa0IvVixHQUFsQixDQUhhO0FBSWxCK2UsUUFBQUEsa0JBQWtCLEVBQUUvZSxHQUFHLENBQUMrZTtBQUpOLE9BQWIsQ0FBUDtBQU1EO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7QUFaQTs7QUEyQkFDLElBQUFBLFFBQVEsQ0FBQ3FCLE9BQVQsR0FBbUIsU0FBU0EsT0FBVCxDQUFpQkMsSUFBakIsRUFBdUJ4VixJQUF2QixFQUE2QjtBQUM5QyxVQUFJeVYsaUJBQWlCLEdBQUcxQyxnQkFBZ0IsQ0FBQ3lDLElBQUQsQ0FBeEM7QUFBQSxVQUNJM2MsTUFBTSxHQUFHNGMsaUJBQWlCLENBQUMsQ0FBRCxDQUQ5Qjs7QUFHQSxVQUFJNWMsTUFBSixFQUFZO0FBQ1YsWUFBSTNELEdBQUcsR0FBR25JLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBY0UsTUFBZCxFQUFzQm1ILElBQXRCLENBQVY7QUFDQSxlQUFPa1UsUUFBUSxDQUFDakosVUFBVCxDQUFvQi9WLEdBQXBCLENBQVA7QUFDRCxPQUhELE1BR087QUFDTCxlQUFPZ2YsUUFBUSxDQUFDa0IsT0FBVCxDQUFpQixZQUFqQixFQUErQixpQkFBaUJJLElBQWpCLEdBQXdCLGdDQUF2RCxDQUFQO0FBQ0Q7QUFDRjtBQUNEOzs7Ozs7QUFYQTs7QUFtQkF0QixJQUFBQSxRQUFRLENBQUNrQixPQUFULEdBQW1CLFNBQVNBLE9BQVQsQ0FBaUIza0IsTUFBakIsRUFBeUJtVCxXQUF6QixFQUFzQztBQUN2RCxVQUFJQSxXQUFXLEtBQUssS0FBSyxDQUF6QixFQUE0QjtBQUMxQkEsUUFBQUEsV0FBVyxHQUFHLElBQWQ7QUFDRDs7QUFFRCxVQUFJLENBQUNuVCxNQUFMLEVBQWE7QUFDWCxjQUFNLElBQUlXLG9CQUFKLENBQXlCLGtEQUF6QixDQUFOO0FBQ0Q7O0FBRUQsVUFBSWdrQixPQUFPLEdBQUcza0IsTUFBTSxZQUFZa1QsT0FBbEIsR0FBNEJsVCxNQUE1QixHQUFxQyxJQUFJa1QsT0FBSixDQUFZbFQsTUFBWixFQUFvQm1ULFdBQXBCLENBQW5EOztBQUVBLFVBQUkyRCxRQUFRLENBQUNELGNBQWIsRUFBNkI7QUFDM0IsY0FBTSxJQUFJelcsb0JBQUosQ0FBeUJ1a0IsT0FBekIsQ0FBTjtBQUNELE9BRkQsTUFFTztBQUNMLGVBQU8sSUFBSWxCLFFBQUosQ0FBYTtBQUNsQmtCLFVBQUFBLE9BQU8sRUFBRUE7QUFEUyxTQUFiLENBQVA7QUFHRDtBQUNGO0FBQ0Q7OztBQW5CQTs7QUF3QkFsQixJQUFBQSxRQUFRLENBQUNvQixhQUFULEdBQXlCLFNBQVNBLGFBQVQsQ0FBdUJua0IsSUFBdkIsRUFBNkI7QUFDcEQsVUFBSW9KLFVBQVUsR0FBRztBQUNmM0ksUUFBQUEsSUFBSSxFQUFFLE9BRFM7QUFFZmtMLFFBQUFBLEtBQUssRUFBRSxPQUZRO0FBR2Y2RixRQUFBQSxPQUFPLEVBQUUsVUFITTtBQUlmNUYsUUFBQUEsUUFBUSxFQUFFLFVBSks7QUFLZmxMLFFBQUFBLEtBQUssRUFBRSxRQUxRO0FBTWY2SixRQUFBQSxNQUFNLEVBQUUsUUFOTztBQU9mZ2EsUUFBQUEsSUFBSSxFQUFFLE9BUFM7QUFRZjFZLFFBQUFBLEtBQUssRUFBRSxPQVJRO0FBU2ZsTCxRQUFBQSxHQUFHLEVBQUUsTUFUVTtBQVVmbUwsUUFBQUEsSUFBSSxFQUFFLE1BVlM7QUFXZjdLLFFBQUFBLElBQUksRUFBRSxPQVhTO0FBWWZ3SSxRQUFBQSxLQUFLLEVBQUUsT0FaUTtBQWFmdkksUUFBQUEsTUFBTSxFQUFFLFNBYk87QUFjZndJLFFBQUFBLE9BQU8sRUFBRSxTQWRNO0FBZWZ0SSxRQUFBQSxNQUFNLEVBQUUsU0FmTztBQWdCZjJLLFFBQUFBLE9BQU8sRUFBRSxTQWhCTTtBQWlCZnhGLFFBQUFBLFdBQVcsRUFBRSxjQWpCRTtBQWtCZmlaLFFBQUFBLFlBQVksRUFBRTtBQWxCQyxRQW1CZnhmLElBQUksR0FBR0EsSUFBSSxDQUFDOEgsV0FBTCxFQUFILEdBQXdCOUgsSUFuQmIsQ0FBakI7QUFvQkEsVUFBSSxDQUFDb0osVUFBTCxFQUFpQixNQUFNLElBQUl0SixnQkFBSixDQUFxQkUsSUFBckIsQ0FBTjtBQUNqQixhQUFPb0osVUFBUDtBQUNEO0FBQ0Q7Ozs7O0FBeEJBOztBQStCQTJaLElBQUFBLFFBQVEsQ0FBQ3lCLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQjduQixDQUFwQixFQUF1QjtBQUMzQyxhQUFPQSxDQUFDLElBQUlBLENBQUMsQ0FBQ3VuQixlQUFQLElBQTBCLEtBQWpDO0FBQ0Q7QUFDRDs7OztBQUhBOztBQVNBLFFBQUl6VSxNQUFNLEdBQUdzVCxRQUFRLENBQUM1bUIsU0FBdEI7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBc1QsSUFBQUEsTUFBTSxDQUFDZ1YsUUFBUCxHQUFrQixTQUFTQSxRQUFULENBQWtCMVYsR0FBbEIsRUFBdUJGLElBQXZCLEVBQTZCO0FBQzdDLFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNELE9BSDRDLENBSzdDOzs7QUFDQSxVQUFJNlYsT0FBTyxHQUFHOW9CLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYyxFQUFkLEVBQWtCcUgsSUFBbEIsRUFBd0I7QUFDcENuSyxRQUFBQSxLQUFLLEVBQUVtSyxJQUFJLENBQUMvSSxLQUFMLEtBQWUsS0FBZixJQUF3QitJLElBQUksQ0FBQ25LLEtBQUwsS0FBZTtBQURWLE9BQXhCLENBQWQ7QUFHQSxhQUFPLEtBQUtvTSxPQUFMLEdBQWVsQyxTQUFTLENBQUNyUyxNQUFWLENBQWlCLEtBQUtnVCxHQUF0QixFQUEyQm1WLE9BQTNCLEVBQW9DalQsd0JBQXBDLENBQTZELElBQTdELEVBQW1FMUMsR0FBbkUsQ0FBZixHQUF5Rm1ULE9BQWhHO0FBQ0Q7QUFDRDs7Ozs7OztBQVhBOztBQW9CQXpTLElBQUFBLE1BQU0sQ0FBQ2tWLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxDQUFrQjlWLElBQWxCLEVBQXdCO0FBQ3hDLFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLaUMsT0FBVixFQUFtQixPQUFPLEVBQVA7QUFDbkIsVUFBSWpILElBQUksR0FBR2pPLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYyxFQUFkLEVBQWtCLEtBQUtxYixNQUF2QixDQUFYOztBQUVBLFVBQUloVSxJQUFJLENBQUMrVixhQUFULEVBQXdCO0FBQ3RCL2EsUUFBQUEsSUFBSSxDQUFDaVosa0JBQUwsR0FBMEIsS0FBS0Esa0JBQS9CO0FBQ0FqWixRQUFBQSxJQUFJLENBQUMyTSxlQUFMLEdBQXVCLEtBQUtqSCxHQUFMLENBQVNpSCxlQUFoQztBQUNBM00sUUFBQUEsSUFBSSxDQUFDMUMsTUFBTCxHQUFjLEtBQUtvSSxHQUFMLENBQVNwSSxNQUF2QjtBQUNEOztBQUVELGFBQU8wQyxJQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7OztBQWhCQTs7QUE0QkE0RixJQUFBQSxNQUFNLENBQUNvVixLQUFQLEdBQWUsU0FBU0EsS0FBVCxHQUFpQjtBQUM5QjtBQUNBLFVBQUksQ0FBQyxLQUFLL1QsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSXhRLENBQUMsR0FBRyxHQUFSO0FBQ0EsVUFBSSxLQUFLcUwsS0FBTCxLQUFlLENBQW5CLEVBQXNCckwsQ0FBQyxJQUFJLEtBQUtxTCxLQUFMLEdBQWEsR0FBbEI7QUFDdEIsVUFBSSxLQUFLcEIsTUFBTCxLQUFnQixDQUFoQixJQUFxQixLQUFLcUIsUUFBTCxLQUFrQixDQUEzQyxFQUE4Q3RMLENBQUMsSUFBSSxLQUFLaUssTUFBTCxHQUFjLEtBQUtxQixRQUFMLEdBQWdCLENBQTlCLEdBQWtDLEdBQXZDO0FBQzlDLFVBQUksS0FBS0MsS0FBTCxLQUFlLENBQW5CLEVBQXNCdkwsQ0FBQyxJQUFJLEtBQUt1TCxLQUFMLEdBQWEsR0FBbEI7QUFDdEIsVUFBSSxLQUFLQyxJQUFMLEtBQWMsQ0FBbEIsRUFBcUJ4TCxDQUFDLElBQUksS0FBS3dMLElBQUwsR0FBWSxHQUFqQjtBQUNyQixVQUFJLEtBQUtyQyxLQUFMLEtBQWUsQ0FBZixJQUFvQixLQUFLQyxPQUFMLEtBQWlCLENBQXJDLElBQTBDLEtBQUtxQyxPQUFMLEtBQWlCLENBQTNELElBQWdFLEtBQUt5VCxZQUFMLEtBQXNCLENBQTFGLEVBQTZGbGYsQ0FBQyxJQUFJLEdBQUw7QUFDN0YsVUFBSSxLQUFLbUosS0FBTCxLQUFlLENBQW5CLEVBQXNCbkosQ0FBQyxJQUFJLEtBQUttSixLQUFMLEdBQWEsR0FBbEI7QUFDdEIsVUFBSSxLQUFLQyxPQUFMLEtBQWlCLENBQXJCLEVBQXdCcEosQ0FBQyxJQUFJLEtBQUtvSixPQUFMLEdBQWUsR0FBcEI7QUFDeEIsVUFBSSxLQUFLcUMsT0FBTCxLQUFpQixDQUFqQixJQUFzQixLQUFLeVQsWUFBTCxLQUFzQixDQUFoRCxFQUFtRDtBQUNqRDtBQUNBbGYsUUFBQUEsQ0FBQyxJQUFJZ0YsT0FBTyxDQUFDLEtBQUt5RyxPQUFMLEdBQWUsS0FBS3lULFlBQUwsR0FBb0IsSUFBcEMsRUFBMEMsQ0FBMUMsQ0FBUCxHQUFzRCxHQUEzRDtBQUNGLFVBQUlsZixDQUFDLEtBQUssR0FBVixFQUFlQSxDQUFDLElBQUksS0FBTDtBQUNmLGFBQU9BLENBQVA7QUFDRDtBQUNEOzs7O0FBakJBOztBQXVCQW1QLElBQUFBLE1BQU0sQ0FBQ3FWLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxhQUFPLEtBQUtELEtBQUwsRUFBUDtBQUNEO0FBQ0Q7Ozs7QUFIQTs7QUFTQXBWLElBQUFBLE1BQU0sQ0FBQ25TLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxHQUFvQjtBQUNwQyxhQUFPLEtBQUt1bkIsS0FBTCxFQUFQO0FBQ0Q7QUFDRDs7OztBQUhBOztBQVNBcFYsSUFBQUEsTUFBTSxDQUFDMEYsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDLGFBQU8sS0FBSzRQLEVBQUwsQ0FBUSxjQUFSLENBQVA7QUFDRDtBQUNEOzs7OztBQUhBOztBQVVBdFYsSUFBQUEsTUFBTSxDQUFDdVYsSUFBUCxHQUFjLFNBQVNBLElBQVQsQ0FBY0MsUUFBZCxFQUF3QjtBQUNwQyxVQUFJLENBQUMsS0FBS25VLE9BQVYsRUFBbUIsT0FBTyxJQUFQO0FBQ25CLFVBQUlZLEdBQUcsR0FBR3dULGdCQUFnQixDQUFDRCxRQUFELENBQTFCO0FBQUEsVUFDSTVFLE1BQU0sR0FBRyxFQURiOztBQUdBLFdBQUssSUFBSXJULEVBQUUsR0FBRyxDQUFULEVBQVltWSxhQUFhLEdBQUczQyxZQUFqQyxFQUErQ3hWLEVBQUUsR0FBR21ZLGFBQWEsQ0FBQzVwQixNQUFsRSxFQUEwRXlSLEVBQUUsRUFBNUUsRUFBZ0Y7QUFDOUUsWUFBSS9JLENBQUMsR0FBR2toQixhQUFhLENBQUNuWSxFQUFELENBQXJCOztBQUVBLFlBQUk5SSxjQUFjLENBQUN3TixHQUFHLENBQUNtUixNQUFMLEVBQWE1ZSxDQUFiLENBQWQsSUFBaUNDLGNBQWMsQ0FBQyxLQUFLMmUsTUFBTixFQUFjNWUsQ0FBZCxDQUFuRCxFQUFxRTtBQUNuRW9jLFVBQUFBLE1BQU0sQ0FBQ3BjLENBQUQsQ0FBTixHQUFZeU4sR0FBRyxDQUFDN1MsR0FBSixDQUFRb0YsQ0FBUixJQUFhLEtBQUtwRixHQUFMLENBQVNvRixDQUFULENBQXpCO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPNlcsS0FBSyxDQUFDLElBQUQsRUFBTztBQUNqQitILFFBQUFBLE1BQU0sRUFBRXhDO0FBRFMsT0FBUCxFQUVULElBRlMsQ0FBWjtBQUdEO0FBQ0Q7Ozs7O0FBakJBOztBQXdCQTVRLElBQUFBLE1BQU0sQ0FBQzJWLEtBQVAsR0FBZSxTQUFTQSxLQUFULENBQWVILFFBQWYsRUFBeUI7QUFDdEMsVUFBSSxDQUFDLEtBQUtuVSxPQUFWLEVBQW1CLE9BQU8sSUFBUDtBQUNuQixVQUFJWSxHQUFHLEdBQUd3VCxnQkFBZ0IsQ0FBQ0QsUUFBRCxDQUExQjtBQUNBLGFBQU8sS0FBS0QsSUFBTCxDQUFVdFQsR0FBRyxDQUFDMlQsTUFBSixFQUFWLENBQVA7QUFDRDtBQUNEOzs7Ozs7O0FBTEE7O0FBY0E1VixJQUFBQSxNQUFNLENBQUM2VixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBa0JqbkIsRUFBbEIsRUFBc0I7QUFDdEMsVUFBSSxDQUFDLEtBQUt5UyxPQUFWLEVBQW1CLE9BQU8sSUFBUDtBQUNuQixVQUFJdVAsTUFBTSxHQUFHLEVBQWI7O0FBRUEsV0FBSyxJQUFJa0YsR0FBRyxHQUFHLENBQVYsRUFBYUMsWUFBWSxHQUFHNXBCLE1BQU0sQ0FBQ29JLElBQVAsQ0FBWSxLQUFLNmUsTUFBakIsQ0FBakMsRUFBMkQwQyxHQUFHLEdBQUdDLFlBQVksQ0FBQ2pxQixNQUE5RSxFQUFzRmdxQixHQUFHLEVBQXpGLEVBQTZGO0FBQzNGLFlBQUl0aEIsQ0FBQyxHQUFHdWhCLFlBQVksQ0FBQ0QsR0FBRCxDQUFwQjtBQUNBbEYsUUFBQUEsTUFBTSxDQUFDcGMsQ0FBRCxDQUFOLEdBQVk4RSxRQUFRLENBQUMxSyxFQUFFLENBQUMsS0FBS3drQixNQUFMLENBQVk1ZSxDQUFaLENBQUQsRUFBaUJBLENBQWpCLENBQUgsQ0FBcEI7QUFDRDs7QUFFRCxhQUFPNlcsS0FBSyxDQUFDLElBQUQsRUFBTztBQUNqQitILFFBQUFBLE1BQU0sRUFBRXhDO0FBRFMsT0FBUCxFQUVULElBRlMsQ0FBWjtBQUdEO0FBQ0Q7Ozs7Ozs7O0FBYkE7O0FBdUJBNVEsSUFBQUEsTUFBTSxDQUFDNVEsR0FBUCxHQUFhLFNBQVNBLEdBQVQsQ0FBYW1CLElBQWIsRUFBbUI7QUFDOUIsYUFBTyxLQUFLK2lCLFFBQVEsQ0FBQ29CLGFBQVQsQ0FBdUJua0IsSUFBdkIsQ0FBTCxDQUFQO0FBQ0Q7QUFDRDs7Ozs7OztBQUhBOztBQVlBeVAsSUFBQUEsTUFBTSxDQUFDM1EsR0FBUCxHQUFhLFNBQVNBLEdBQVQsQ0FBYStqQixNQUFiLEVBQXFCO0FBQ2hDLFVBQUksQ0FBQyxLQUFLL1IsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSTJVLEtBQUssR0FBRzdwQixNQUFNLENBQUM0TCxNQUFQLENBQWMsS0FBS3FiLE1BQW5CLEVBQTJCNVosZUFBZSxDQUFDNFosTUFBRCxFQUFTRSxRQUFRLENBQUNvQixhQUFsQixFQUFpQyxFQUFqQyxDQUExQyxDQUFaO0FBQ0EsYUFBT3JKLEtBQUssQ0FBQyxJQUFELEVBQU87QUFDakIrSCxRQUFBQSxNQUFNLEVBQUU0QztBQURTLE9BQVAsQ0FBWjtBQUdEO0FBQ0Q7Ozs7O0FBUEE7O0FBY0FoVyxJQUFBQSxNQUFNLENBQUNpVyxXQUFQLEdBQXFCLFNBQVNBLFdBQVQsQ0FBcUIzTCxLQUFyQixFQUE0QjtBQUMvQyxVQUFJNU0sSUFBSSxHQUFHNE0sS0FBSyxLQUFLLEtBQUssQ0FBZixHQUFtQixFQUFuQixHQUF3QkEsS0FBbkM7QUFBQSxVQUNJNVMsTUFBTSxHQUFHZ0csSUFBSSxDQUFDaEcsTUFEbEI7QUFBQSxVQUVJcVAsZUFBZSxHQUFHckosSUFBSSxDQUFDcUosZUFGM0I7QUFBQSxVQUdJc00sa0JBQWtCLEdBQUczVixJQUFJLENBQUMyVixrQkFIOUI7O0FBS0EsVUFBSXZULEdBQUcsR0FBRyxLQUFLQSxHQUFMLENBQVN1TCxLQUFULENBQWU7QUFDdkIzVCxRQUFBQSxNQUFNLEVBQUVBLE1BRGU7QUFFdkJxUCxRQUFBQSxlQUFlLEVBQUVBO0FBRk0sT0FBZixDQUFWO0FBQUEsVUFJSTNILElBQUksR0FBRztBQUNUVSxRQUFBQSxHQUFHLEVBQUVBO0FBREksT0FKWDs7QUFRQSxVQUFJdVQsa0JBQUosRUFBd0I7QUFDdEJqVSxRQUFBQSxJQUFJLENBQUNpVSxrQkFBTCxHQUEwQkEsa0JBQTFCO0FBQ0Q7O0FBRUQsYUFBT2hJLEtBQUssQ0FBQyxJQUFELEVBQU9qTSxJQUFQLENBQVo7QUFDRDtBQUNEOzs7Ozs7OztBQXBCQTs7QUE4QkFZLElBQUFBLE1BQU0sQ0FBQ3NWLEVBQVAsR0FBWSxTQUFTQSxFQUFULENBQVkva0IsSUFBWixFQUFrQjtBQUM1QixhQUFPLEtBQUs4USxPQUFMLEdBQWUsS0FBS3VCLE9BQUwsQ0FBYXJTLElBQWIsRUFBbUJuQixHQUFuQixDQUF1Qm1CLElBQXZCLENBQWYsR0FBOEMyVixHQUFyRDtBQUNEO0FBQ0Q7Ozs7OztBQUhBOztBQVdBbEcsSUFBQUEsTUFBTSxDQUFDa1csU0FBUCxHQUFtQixTQUFTQSxTQUFULEdBQXFCO0FBQ3RDLFVBQUksQ0FBQyxLQUFLN1UsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSStTLElBQUksR0FBRyxLQUFLYyxRQUFMLEVBQVg7QUFDQWYsTUFBQUEsZUFBZSxDQUFDLEtBQUtULE1BQU4sRUFBY1UsSUFBZCxDQUFmO0FBQ0EsYUFBTy9JLEtBQUssQ0FBQyxJQUFELEVBQU87QUFDakIrSCxRQUFBQSxNQUFNLEVBQUVnQjtBQURTLE9BQVAsRUFFVCxJQUZTLENBQVo7QUFHRDtBQUNEOzs7OztBQVJBOztBQWVBcFUsSUFBQUEsTUFBTSxDQUFDNEMsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDLFdBQUssSUFBSTJKLElBQUksR0FBRzdkLFNBQVMsQ0FBQzVDLE1BQXJCLEVBQTZCbVEsS0FBSyxHQUFHLElBQUl0SSxLQUFKLENBQVU0WSxJQUFWLENBQXJDLEVBQXNERSxJQUFJLEdBQUcsQ0FBbEUsRUFBcUVBLElBQUksR0FBR0YsSUFBNUUsRUFBa0ZFLElBQUksRUFBdEYsRUFBMEY7QUFDeEZ4USxRQUFBQSxLQUFLLENBQUN3USxJQUFELENBQUwsR0FBYy9kLFNBQVMsQ0FBQytkLElBQUQsQ0FBdkI7QUFDRDs7QUFFRCxVQUFJLENBQUMsS0FBS3BMLE9BQVYsRUFBbUIsT0FBTyxJQUFQOztBQUVuQixVQUFJcEYsS0FBSyxDQUFDblEsTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUN0QixlQUFPLElBQVA7QUFDRDs7QUFFRG1RLE1BQUFBLEtBQUssR0FBR0EsS0FBSyxDQUFDNEcsR0FBTixDQUFVLFVBQVVqSixDQUFWLEVBQWE7QUFDN0IsZUFBTzBaLFFBQVEsQ0FBQ29CLGFBQVQsQ0FBdUI5YSxDQUF2QixDQUFQO0FBQ0QsT0FGTyxDQUFSO0FBR0EsVUFBSXVjLEtBQUssR0FBRyxFQUFaO0FBQUEsVUFDSUMsV0FBVyxHQUFHLEVBRGxCO0FBQUEsVUFFSWhDLElBQUksR0FBRyxLQUFLYyxRQUFMLEVBRlg7QUFHQSxVQUFJbUIsUUFBSjtBQUNBbEMsTUFBQUEsZUFBZSxDQUFDLEtBQUtULE1BQU4sRUFBY1UsSUFBZCxDQUFmOztBQUVBLFdBQUssSUFBSWtDLEdBQUcsR0FBRyxDQUFWLEVBQWFDLGNBQWMsR0FBR3hELFlBQW5DLEVBQWlEdUQsR0FBRyxHQUFHQyxjQUFjLENBQUN6cUIsTUFBdEUsRUFBOEV3cUIsR0FBRyxFQUFqRixFQUFxRjtBQUNuRixZQUFJOWhCLENBQUMsR0FBRytoQixjQUFjLENBQUNELEdBQUQsQ0FBdEI7O0FBRUEsWUFBSXJhLEtBQUssQ0FBQ3BOLE9BQU4sQ0FBYzJGLENBQWQsS0FBb0IsQ0FBeEIsRUFBMkI7QUFDekI2aEIsVUFBQUEsUUFBUSxHQUFHN2hCLENBQVg7QUFDQSxjQUFJZ2lCLEdBQUcsR0FBRyxDQUFWLENBRnlCLENBRVo7O0FBRWIsZUFBSyxJQUFJQyxFQUFULElBQWVMLFdBQWYsRUFBNEI7QUFDMUJJLFlBQUFBLEdBQUcsSUFBSSxLQUFLOUMsTUFBTCxDQUFZK0MsRUFBWixFQUFnQmppQixDQUFoQixJQUFxQjRoQixXQUFXLENBQUNLLEVBQUQsQ0FBdkM7QUFDQUwsWUFBQUEsV0FBVyxDQUFDSyxFQUFELENBQVgsR0FBa0IsQ0FBbEI7QUFDRCxXQVB3QixDQU92Qjs7O0FBR0YsY0FBSTNqQixRQUFRLENBQUNzaEIsSUFBSSxDQUFDNWYsQ0FBRCxDQUFMLENBQVosRUFBdUI7QUFDckJnaUIsWUFBQUEsR0FBRyxJQUFJcEMsSUFBSSxDQUFDNWYsQ0FBRCxDQUFYO0FBQ0Q7O0FBRUQsY0FBSTNJLENBQUMsR0FBR21KLElBQUksQ0FBQ29CLEtBQUwsQ0FBV29nQixHQUFYLENBQVI7QUFDQUwsVUFBQUEsS0FBSyxDQUFDM2hCLENBQUQsQ0FBTCxHQUFXM0ksQ0FBWDtBQUNBdXFCLFVBQUFBLFdBQVcsQ0FBQzVoQixDQUFELENBQVgsR0FBaUJnaUIsR0FBRyxHQUFHM3FCLENBQXZCLENBaEJ5QixDQWdCQztBQUMxQjs7QUFFQSxlQUFLLElBQUk2cUIsSUFBVCxJQUFpQnRDLElBQWpCLEVBQXVCO0FBQ3JCLGdCQUFJckIsWUFBWSxDQUFDbGtCLE9BQWIsQ0FBcUI2bkIsSUFBckIsSUFBNkIzRCxZQUFZLENBQUNsa0IsT0FBYixDQUFxQjJGLENBQXJCLENBQWpDLEVBQTBEO0FBQ3hEaWYsY0FBQUEsT0FBTyxDQUFDLEtBQUtDLE1BQU4sRUFBY1UsSUFBZCxFQUFvQnNDLElBQXBCLEVBQTBCUCxLQUExQixFQUFpQzNoQixDQUFqQyxDQUFQO0FBQ0Q7QUFDRixXQXZCd0IsQ0F1QnZCOztBQUVILFNBekJELE1BeUJPLElBQUkxQixRQUFRLENBQUNzaEIsSUFBSSxDQUFDNWYsQ0FBRCxDQUFMLENBQVosRUFBdUI7QUFDNUI0aEIsVUFBQUEsV0FBVyxDQUFDNWhCLENBQUQsQ0FBWCxHQUFpQjRmLElBQUksQ0FBQzVmLENBQUQsQ0FBckI7QUFDRDtBQUNGLE9BbkRpQyxDQW1EaEM7QUFDRjs7O0FBR0EsV0FBSyxJQUFJbkksR0FBVCxJQUFnQitwQixXQUFoQixFQUE2QjtBQUMzQixZQUFJQSxXQUFXLENBQUMvcEIsR0FBRCxDQUFYLEtBQXFCLENBQXpCLEVBQTRCO0FBQzFCOHBCLFVBQUFBLEtBQUssQ0FBQ0UsUUFBRCxDQUFMLElBQW1CaHFCLEdBQUcsS0FBS2dxQixRQUFSLEdBQW1CRCxXQUFXLENBQUMvcEIsR0FBRCxDQUE5QixHQUFzQytwQixXQUFXLENBQUMvcEIsR0FBRCxDQUFYLEdBQW1CLEtBQUtxbkIsTUFBTCxDQUFZMkMsUUFBWixFQUFzQmhxQixHQUF0QixDQUE1RTtBQUNEO0FBQ0Y7O0FBRUQsYUFBT2dmLEtBQUssQ0FBQyxJQUFELEVBQU87QUFDakIrSCxRQUFBQSxNQUFNLEVBQUUrQztBQURTLE9BQVAsRUFFVCxJQUZTLENBQUwsQ0FFRUQsU0FGRixFQUFQO0FBR0Q7QUFDRDs7Ozs7QUFqRUE7O0FBd0VBbFcsSUFBQUEsTUFBTSxDQUFDNFYsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFVBQUksQ0FBQyxLQUFLdlUsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSXNWLE9BQU8sR0FBRyxFQUFkOztBQUVBLFdBQUssSUFBSUMsR0FBRyxHQUFHLENBQVYsRUFBYUMsYUFBYSxHQUFHMXFCLE1BQU0sQ0FBQ29JLElBQVAsQ0FBWSxLQUFLNmUsTUFBakIsQ0FBbEMsRUFBNER3RCxHQUFHLEdBQUdDLGFBQWEsQ0FBQy9xQixNQUFoRixFQUF3RjhxQixHQUFHLEVBQTNGLEVBQStGO0FBQzdGLFlBQUlwaUIsQ0FBQyxHQUFHcWlCLGFBQWEsQ0FBQ0QsR0FBRCxDQUFyQjtBQUNBRCxRQUFBQSxPQUFPLENBQUNuaUIsQ0FBRCxDQUFQLEdBQWEsQ0FBQyxLQUFLNGUsTUFBTCxDQUFZNWUsQ0FBWixDQUFkO0FBQ0Q7O0FBRUQsYUFBTzZXLEtBQUssQ0FBQyxJQUFELEVBQU87QUFDakIrSCxRQUFBQSxNQUFNLEVBQUV1RDtBQURTLE9BQVAsRUFFVCxJQUZTLENBQVo7QUFHRDtBQUNEOzs7O0FBYkE7QUFtQkE7Ozs7Ozs7O0FBTUEzVyxJQUFBQSxNQUFNLENBQUNrRCxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JtSixLQUFoQixFQUF1QjtBQUNyQyxVQUFJLENBQUMsS0FBS2hMLE9BQU4sSUFBaUIsQ0FBQ2dMLEtBQUssQ0FBQ2hMLE9BQTVCLEVBQXFDO0FBQ25DLGVBQU8sS0FBUDtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLdkIsR0FBTCxDQUFTb0QsTUFBVCxDQUFnQm1KLEtBQUssQ0FBQ3ZNLEdBQXRCLENBQUwsRUFBaUM7QUFDL0IsZUFBTyxLQUFQO0FBQ0Q7O0FBRUQsV0FBSyxJQUFJZ1gsR0FBRyxHQUFHLENBQVYsRUFBYUMsY0FBYyxHQUFHaEUsWUFBbkMsRUFBaUQrRCxHQUFHLEdBQUdDLGNBQWMsQ0FBQ2pyQixNQUF0RSxFQUE4RWdyQixHQUFHLEVBQWpGLEVBQXFGO0FBQ25GLFlBQUlsZCxDQUFDLEdBQUdtZCxjQUFjLENBQUNELEdBQUQsQ0FBdEI7O0FBRUEsWUFBSSxLQUFLMUQsTUFBTCxDQUFZeFosQ0FBWixNQUFtQnlTLEtBQUssQ0FBQytHLE1BQU4sQ0FBYXhaLENBQWIsQ0FBdkIsRUFBd0M7QUFDdEMsaUJBQU8sS0FBUDtBQUNEO0FBQ0Y7O0FBRUQsYUFBTyxJQUFQO0FBQ0QsS0FsQkQ7O0FBb0JBdE4sSUFBQUEsWUFBWSxDQUFDZ25CLFFBQUQsRUFBVyxDQUFDO0FBQ3RCam5CLE1BQUFBLEdBQUcsRUFBRSxRQURpQjtBQUV0QitDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUt2QixHQUFMLENBQVNwSSxNQUF4QixHQUFpQyxJQUF4QztBQUNEO0FBQ0Q7Ozs7OztBQUxzQixLQUFELEVBV3BCO0FBQ0RyTCxNQUFBQSxHQUFHLEVBQUUsaUJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUt2QixHQUFMLENBQVNpSCxlQUF4QixHQUEwQyxJQUFqRDtBQUNEO0FBSkEsS0FYb0IsRUFnQnBCO0FBQ0QxYSxNQUFBQSxHQUFHLEVBQUUsT0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSytSLE1BQUwsQ0FBWWxYLEtBQVosSUFBcUIsQ0FBcEMsR0FBd0NnSyxHQUEvQztBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0FoQm9CLEVBMEJwQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLFVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUsrUixNQUFMLENBQVlqWCxRQUFaLElBQXdCLENBQXZDLEdBQTJDK0osR0FBbEQ7QUFDRDtBQUNEOzs7OztBQUxDLEtBMUJvQixFQW9DcEI7QUFDRDdaLE1BQUFBLEdBQUcsRUFBRSxRQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS2lTLE9BQUwsR0FBZSxLQUFLK1IsTUFBTCxDQUFZdFksTUFBWixJQUFzQixDQUFyQyxHQUF5Q29MLEdBQWhEO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQXBDb0IsRUE4Q3BCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsT0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSytSLE1BQUwsQ0FBWWhYLEtBQVosSUFBcUIsQ0FBcEMsR0FBd0M4SixHQUEvQztBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0E5Q29CLEVBd0RwQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLE1BREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUsrUixNQUFMLENBQVkvVyxJQUFaLElBQW9CLENBQW5DLEdBQXVDNkosR0FBOUM7QUFDRDtBQUNEOzs7OztBQUxDLEtBeERvQixFQWtFcEI7QUFDRDdaLE1BQUFBLEdBQUcsRUFBRSxPQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS2lTLE9BQUwsR0FBZSxLQUFLK1IsTUFBTCxDQUFZcFosS0FBWixJQUFxQixDQUFwQyxHQUF3Q2tNLEdBQS9DO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQWxFb0IsRUE0RXBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSytSLE1BQUwsQ0FBWW5aLE9BQVosSUFBdUIsQ0FBdEMsR0FBMENpTSxHQUFqRDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0E1RW9CLEVBc0ZwQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLFNBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUsrUixNQUFMLENBQVk5VyxPQUFaLElBQXVCLENBQXRDLEdBQTBDNEosR0FBakQ7QUFDRDtBQUNEOzs7OztBQUxDLEtBdEZvQixFQWdHcEI7QUFDRDdaLE1BQUFBLEdBQUcsRUFBRSxjQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS2lTLE9BQUwsR0FBZSxLQUFLK1IsTUFBTCxDQUFZckQsWUFBWixJQUE0QixDQUEzQyxHQUErQzdKLEdBQXREO0FBQ0Q7QUFDRDs7Ozs7O0FBTEMsS0FoR29CLEVBMkdwQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLFNBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLb2xCLE9BQUwsS0FBaUIsSUFBeEI7QUFDRDtBQUNEOzs7OztBQUxDLEtBM0dvQixFQXFIcEI7QUFDRG5vQixNQUFBQSxHQUFHLEVBQUUsZUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtvbEIsT0FBTCxHQUFlLEtBQUtBLE9BQUwsQ0FBYTNrQixNQUE1QixHQUFxQyxJQUE1QztBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0FySG9CLEVBK0hwQjtBQUNEeEQsTUFBQUEsR0FBRyxFQUFFLG9CQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS29sQixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFheFIsV0FBNUIsR0FBMEMsSUFBakQ7QUFDRDtBQUpBLEtBL0hvQixDQUFYLENBQVo7O0FBc0lBLFdBQU9zUSxRQUFQO0FBQ0QsR0EzckJELEVBRkE7O0FBOHJCQSxXQUFTbUMsZ0JBQVQsQ0FBMEJ1QixXQUExQixFQUF1QztBQUNyQyxRQUFJbGtCLFFBQVEsQ0FBQ2trQixXQUFELENBQVosRUFBMkI7QUFDekIsYUFBTzFELFFBQVEsQ0FBQ2hLLFVBQVQsQ0FBb0IwTixXQUFwQixDQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUkxRCxRQUFRLENBQUN5QixVQUFULENBQW9CaUMsV0FBcEIsQ0FBSixFQUFzQztBQUMzQyxhQUFPQSxXQUFQO0FBQ0QsS0FGTSxNQUVBLElBQUksUUFBT0EsV0FBUCxNQUF1QixRQUEzQixFQUFxQztBQUMxQyxhQUFPMUQsUUFBUSxDQUFDakosVUFBVCxDQUFvQjJNLFdBQXBCLENBQVA7QUFDRCxLQUZNLE1BRUE7QUFDTCxZQUFNLElBQUl4bUIsb0JBQUosQ0FBeUIsK0JBQStCd21CLFdBQS9CLEdBQTZDLFdBQTdDLFdBQWtFQSxXQUFsRSxDQUF6QixDQUFOO0FBQ0Q7QUFDRjs7QUFFRCxNQUFJQyxTQUFTLEdBQUcsa0JBQWhCLENBNTVIOEIsQ0E0NUhNOztBQUVwQyxXQUFTQyxnQkFBVCxDQUEwQkMsS0FBMUIsRUFBaUNDLEdBQWpDLEVBQXNDO0FBQ3BDLFFBQUksQ0FBQ0QsS0FBRCxJQUFVLENBQUNBLEtBQUssQ0FBQzlWLE9BQXJCLEVBQThCO0FBQzVCLGFBQU9nVyxRQUFRLENBQUM3QyxPQUFULENBQWlCLDBCQUFqQixDQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUksQ0FBQzRDLEdBQUQsSUFBUSxDQUFDQSxHQUFHLENBQUMvVixPQUFqQixFQUEwQjtBQUMvQixhQUFPZ1csUUFBUSxDQUFDN0MsT0FBVCxDQUFpQix3QkFBakIsQ0FBUDtBQUNELEtBRk0sTUFFQSxJQUFJNEMsR0FBRyxHQUFHRCxLQUFWLEVBQWlCO0FBQ3RCLGFBQU9FLFFBQVEsQ0FBQzdDLE9BQVQsQ0FBaUIsa0JBQWpCLEVBQXFDLHVFQUF1RTJDLEtBQUssQ0FBQy9CLEtBQU4sRUFBdkUsR0FBdUYsV0FBdkYsR0FBcUdnQyxHQUFHLENBQUNoQyxLQUFKLEVBQTFJLENBQVA7QUFDRCxLQUZNLE1BRUE7QUFDTCxhQUFPLElBQVA7QUFDRDtBQUNGO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7O0FBY0EsTUFBSWlDLFFBQVE7QUFDWjtBQUNBLGNBQVk7QUFDVjs7O0FBR0EsYUFBU0EsUUFBVCxDQUFrQi9DLE1BQWxCLEVBQTBCO0FBQ3hCOzs7QUFHQSxXQUFLempCLENBQUwsR0FBU3lqQixNQUFNLENBQUM2QyxLQUFoQjtBQUNBOzs7O0FBSUEsV0FBS3BwQixDQUFMLEdBQVN1bUIsTUFBTSxDQUFDOEMsR0FBaEI7QUFDQTs7OztBQUlBLFdBQUs1QyxPQUFMLEdBQWVGLE1BQU0sQ0FBQ0UsT0FBUCxJQUFrQixJQUFqQztBQUNBOzs7O0FBSUEsV0FBSzhDLGVBQUwsR0FBdUIsSUFBdkI7QUFDRDtBQUNEOzs7Ozs7OztBQVFBRCxJQUFBQSxRQUFRLENBQUM3QyxPQUFULEdBQW1CLFNBQVNBLE9BQVQsQ0FBaUIza0IsTUFBakIsRUFBeUJtVCxXQUF6QixFQUFzQztBQUN2RCxVQUFJQSxXQUFXLEtBQUssS0FBSyxDQUF6QixFQUE0QjtBQUMxQkEsUUFBQUEsV0FBVyxHQUFHLElBQWQ7QUFDRDs7QUFFRCxVQUFJLENBQUNuVCxNQUFMLEVBQWE7QUFDWCxjQUFNLElBQUlXLG9CQUFKLENBQXlCLGtEQUF6QixDQUFOO0FBQ0Q7O0FBRUQsVUFBSWdrQixPQUFPLEdBQUcza0IsTUFBTSxZQUFZa1QsT0FBbEIsR0FBNEJsVCxNQUE1QixHQUFxQyxJQUFJa1QsT0FBSixDQUFZbFQsTUFBWixFQUFvQm1ULFdBQXBCLENBQW5EOztBQUVBLFVBQUkyRCxRQUFRLENBQUNELGNBQWIsRUFBNkI7QUFDM0IsY0FBTSxJQUFJM1csb0JBQUosQ0FBeUJ5a0IsT0FBekIsQ0FBTjtBQUNELE9BRkQsTUFFTztBQUNMLGVBQU8sSUFBSTZDLFFBQUosQ0FBYTtBQUNsQjdDLFVBQUFBLE9BQU8sRUFBRUE7QUFEUyxTQUFiLENBQVA7QUFHRDtBQUNGO0FBQ0Q7Ozs7OztBQW5CQTs7QUEyQkE2QyxJQUFBQSxRQUFRLENBQUNFLGFBQVQsR0FBeUIsU0FBU0EsYUFBVCxDQUF1QkosS0FBdkIsRUFBOEJDLEdBQTlCLEVBQW1DO0FBQzFELFVBQUlJLFVBQVUsR0FBR0MsZ0JBQWdCLENBQUNOLEtBQUQsQ0FBakM7QUFBQSxVQUNJTyxRQUFRLEdBQUdELGdCQUFnQixDQUFDTCxHQUFELENBRC9CO0FBRUEsVUFBSU8sYUFBYSxHQUFHVCxnQkFBZ0IsQ0FBQ00sVUFBRCxFQUFhRSxRQUFiLENBQXBDOztBQUVBLFVBQUlDLGFBQWEsSUFBSSxJQUFyQixFQUEyQjtBQUN6QixlQUFPLElBQUlOLFFBQUosQ0FBYTtBQUNsQkYsVUFBQUEsS0FBSyxFQUFFSyxVQURXO0FBRWxCSixVQUFBQSxHQUFHLEVBQUVNO0FBRmEsU0FBYixDQUFQO0FBSUQsT0FMRCxNQUtPO0FBQ0wsZUFBT0MsYUFBUDtBQUNEO0FBQ0Y7QUFDRDs7Ozs7O0FBZEE7O0FBc0JBTixJQUFBQSxRQUFRLENBQUNPLEtBQVQsR0FBaUIsU0FBU0EsS0FBVCxDQUFlVCxLQUFmLEVBQXNCM0IsUUFBdEIsRUFBZ0M7QUFDL0MsVUFBSXZULEdBQUcsR0FBR3dULGdCQUFnQixDQUFDRCxRQUFELENBQTFCO0FBQUEsVUFDSS9aLEVBQUUsR0FBR2djLGdCQUFnQixDQUFDTixLQUFELENBRHpCO0FBRUEsYUFBT0UsUUFBUSxDQUFDRSxhQUFULENBQXVCOWIsRUFBdkIsRUFBMkJBLEVBQUUsQ0FBQzhaLElBQUgsQ0FBUXRULEdBQVIsQ0FBM0IsQ0FBUDtBQUNEO0FBQ0Q7Ozs7OztBQUxBOztBQWFBb1YsSUFBQUEsUUFBUSxDQUFDUSxNQUFULEdBQWtCLFNBQVNBLE1BQVQsQ0FBZ0JULEdBQWhCLEVBQXFCNUIsUUFBckIsRUFBK0I7QUFDL0MsVUFBSXZULEdBQUcsR0FBR3dULGdCQUFnQixDQUFDRCxRQUFELENBQTFCO0FBQUEsVUFDSS9aLEVBQUUsR0FBR2djLGdCQUFnQixDQUFDTCxHQUFELENBRHpCO0FBRUEsYUFBT0MsUUFBUSxDQUFDRSxhQUFULENBQXVCOWIsRUFBRSxDQUFDa2EsS0FBSCxDQUFTMVQsR0FBVCxDQUF2QixFQUFzQ3hHLEVBQXRDLENBQVA7QUFDRDtBQUNEOzs7Ozs7OztBQUxBOztBQWVBNGIsSUFBQUEsUUFBUSxDQUFDMUMsT0FBVCxHQUFtQixTQUFTQSxPQUFULENBQWlCQyxJQUFqQixFQUF1QnhWLElBQXZCLEVBQTZCO0FBQzlDLFVBQUkwWSxNQUFNLEdBQUcsQ0FBQ2xELElBQUksSUFBSSxFQUFULEVBQWFtRCxLQUFiLENBQW1CLEdBQW5CLEVBQXdCLENBQXhCLENBQWI7QUFBQSxVQUNJbG5CLENBQUMsR0FBR2luQixNQUFNLENBQUMsQ0FBRCxDQURkO0FBQUEsVUFFSS9wQixDQUFDLEdBQUcrcEIsTUFBTSxDQUFDLENBQUQsQ0FGZDs7QUFJQSxVQUFJam5CLENBQUMsSUFBSTlDLENBQVQsRUFBWTtBQUNWLFlBQUlvcEIsS0FBSyxHQUFHN08sUUFBUSxDQUFDcU0sT0FBVCxDQUFpQjlqQixDQUFqQixFQUFvQnVPLElBQXBCLENBQVo7QUFBQSxZQUNJZ1ksR0FBRyxHQUFHOU8sUUFBUSxDQUFDcU0sT0FBVCxDQUFpQjVtQixDQUFqQixFQUFvQnFSLElBQXBCLENBRFY7O0FBR0EsWUFBSStYLEtBQUssQ0FBQzlWLE9BQU4sSUFBaUIrVixHQUFHLENBQUMvVixPQUF6QixFQUFrQztBQUNoQyxpQkFBT2dXLFFBQVEsQ0FBQ0UsYUFBVCxDQUF1QkosS0FBdkIsRUFBOEJDLEdBQTlCLENBQVA7QUFDRDs7QUFFRCxZQUFJRCxLQUFLLENBQUM5VixPQUFWLEVBQW1CO0FBQ2pCLGNBQUlZLEdBQUcsR0FBR3FSLFFBQVEsQ0FBQ3FCLE9BQVQsQ0FBaUI1bUIsQ0FBakIsRUFBb0JxUixJQUFwQixDQUFWOztBQUVBLGNBQUk2QyxHQUFHLENBQUNaLE9BQVIsRUFBaUI7QUFDZixtQkFBT2dXLFFBQVEsQ0FBQ08sS0FBVCxDQUFlVCxLQUFmLEVBQXNCbFYsR0FBdEIsQ0FBUDtBQUNEO0FBQ0YsU0FORCxNQU1PLElBQUltVixHQUFHLENBQUMvVixPQUFSLEVBQWlCO0FBQ3RCLGNBQUkyVyxJQUFJLEdBQUcxRSxRQUFRLENBQUNxQixPQUFULENBQWlCOWpCLENBQWpCLEVBQW9CdU8sSUFBcEIsQ0FBWDs7QUFFQSxjQUFJNFksSUFBSSxDQUFDM1csT0FBVCxFQUFrQjtBQUNoQixtQkFBT2dXLFFBQVEsQ0FBQ1EsTUFBVCxDQUFnQlQsR0FBaEIsRUFBcUJZLElBQXJCLENBQVA7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsYUFBT1gsUUFBUSxDQUFDN0MsT0FBVCxDQUFpQixZQUFqQixFQUErQixpQkFBaUJJLElBQWpCLEdBQXdCLCtCQUF2RCxDQUFQO0FBQ0Q7QUFDRDs7Ozs7QUE5QkE7O0FBcUNBeUMsSUFBQUEsUUFBUSxDQUFDWSxVQUFULEdBQXNCLFNBQVNBLFVBQVQsQ0FBb0IvcUIsQ0FBcEIsRUFBdUI7QUFDM0MsYUFBT0EsQ0FBQyxJQUFJQSxDQUFDLENBQUNvcUIsZUFBUCxJQUEwQixLQUFqQztBQUNEO0FBQ0Q7Ozs7QUFIQTs7QUFTQSxRQUFJdFgsTUFBTSxHQUFHcVgsUUFBUSxDQUFDM3FCLFNBQXRCO0FBRUE7Ozs7OztBQUtBc1QsSUFBQUEsTUFBTSxDQUFDbFUsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCeUUsSUFBaEIsRUFBc0I7QUFDcEMsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxjQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLOFEsT0FBTCxHQUFlLEtBQUs2VyxVQUFMLENBQWdCNXBCLEtBQWhCLENBQXNCLElBQXRCLEVBQTRCLENBQUNpQyxJQUFELENBQTVCLEVBQW9DbkIsR0FBcEMsQ0FBd0NtQixJQUF4QyxDQUFmLEdBQStEMlYsR0FBdEU7QUFDRDtBQUNEOzs7Ozs7O0FBUEE7O0FBZ0JBbEcsSUFBQUEsTUFBTSxDQUFDbEUsS0FBUCxHQUFlLFNBQVNBLEtBQVQsQ0FBZXZMLElBQWYsRUFBcUI7QUFDbEMsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxjQUFQO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUs4USxPQUFWLEVBQW1CLE9BQU82RSxHQUFQO0FBQ25CLFVBQUlpUixLQUFLLEdBQUcsS0FBS0EsS0FBTCxDQUFXZ0IsT0FBWCxDQUFtQjVuQixJQUFuQixDQUFaO0FBQUEsVUFDSTZtQixHQUFHLEdBQUcsS0FBS0EsR0FBTCxDQUFTZSxPQUFULENBQWlCNW5CLElBQWpCLENBRFY7QUFFQSxhQUFPeUUsSUFBSSxDQUFDQyxLQUFMLENBQVdtaUIsR0FBRyxDQUFDZ0IsSUFBSixDQUFTakIsS0FBVCxFQUFnQjVtQixJQUFoQixFQUFzQm5CLEdBQXRCLENBQTBCbUIsSUFBMUIsQ0FBWCxJQUE4QyxDQUFyRDtBQUNEO0FBQ0Q7Ozs7O0FBVkE7O0FBaUJBeVAsSUFBQUEsTUFBTSxDQUFDcVksT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWlCOW5CLElBQWpCLEVBQXVCO0FBQ3RDLGFBQU8sS0FBSzhRLE9BQUwsR0FBZSxLQUFLdFQsQ0FBTCxDQUFPNG5CLEtBQVAsQ0FBYSxDQUFiLEVBQWdCMEMsT0FBaEIsQ0FBd0IsS0FBS3huQixDQUE3QixFQUFnQ04sSUFBaEMsQ0FBZixHQUF1RCxLQUE5RDtBQUNEO0FBQ0Q7Ozs7QUFIQTs7QUFTQXlQLElBQUFBLE1BQU0sQ0FBQ3NZLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQyxhQUFPLEtBQUt6bkIsQ0FBTCxDQUFPNlUsT0FBUCxPQUFxQixLQUFLM1gsQ0FBTCxDQUFPMlgsT0FBUCxFQUE1QjtBQUNEO0FBQ0Q7Ozs7O0FBSEE7O0FBVUExRixJQUFBQSxNQUFNLENBQUN1WSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsQ0FBaUJDLFFBQWpCLEVBQTJCO0FBQzFDLFVBQUksQ0FBQyxLQUFLblgsT0FBVixFQUFtQixPQUFPLEtBQVA7QUFDbkIsYUFBTyxLQUFLeFEsQ0FBTCxHQUFTMm5CLFFBQWhCO0FBQ0Q7QUFDRDs7Ozs7QUFKQTs7QUFXQXhZLElBQUFBLE1BQU0sQ0FBQ3lZLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxDQUFrQkQsUUFBbEIsRUFBNEI7QUFDNUMsVUFBSSxDQUFDLEtBQUtuWCxPQUFWLEVBQW1CLE9BQU8sS0FBUDtBQUNuQixhQUFPLEtBQUt0VCxDQUFMLElBQVV5cUIsUUFBakI7QUFDRDtBQUNEOzs7OztBQUpBOztBQVdBeFksSUFBQUEsTUFBTSxDQUFDMFksUUFBUCxHQUFrQixTQUFTQSxRQUFULENBQWtCRixRQUFsQixFQUE0QjtBQUM1QyxVQUFJLENBQUMsS0FBS25YLE9BQVYsRUFBbUIsT0FBTyxLQUFQO0FBQ25CLGFBQU8sS0FBS3hRLENBQUwsSUFBVTJuQixRQUFWLElBQXNCLEtBQUt6cUIsQ0FBTCxHQUFTeXFCLFFBQXRDO0FBQ0Q7QUFDRDs7Ozs7OztBQUpBOztBQWFBeFksSUFBQUEsTUFBTSxDQUFDM1EsR0FBUCxHQUFhLFNBQVNBLEdBQVQsQ0FBYWliLEtBQWIsRUFBb0I7QUFDL0IsVUFBSTVNLElBQUksR0FBRzRNLEtBQUssS0FBSyxLQUFLLENBQWYsR0FBbUIsRUFBbkIsR0FBd0JBLEtBQW5DO0FBQUEsVUFDSTZNLEtBQUssR0FBR3paLElBQUksQ0FBQ3laLEtBRGpCO0FBQUEsVUFFSUMsR0FBRyxHQUFHMVosSUFBSSxDQUFDMFosR0FGZjs7QUFJQSxVQUFJLENBQUMsS0FBSy9WLE9BQVYsRUFBbUIsT0FBTyxJQUFQO0FBQ25CLGFBQU9nVyxRQUFRLENBQUNFLGFBQVQsQ0FBdUJKLEtBQUssSUFBSSxLQUFLdG1CLENBQXJDLEVBQXdDdW1CLEdBQUcsSUFBSSxLQUFLcnBCLENBQXBELENBQVA7QUFDRDtBQUNEOzs7OztBQVJBOztBQWVBaVMsSUFBQUEsTUFBTSxDQUFDMlksT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDLFVBQUk5WCxLQUFLLEdBQUcsSUFBWjs7QUFFQSxVQUFJLENBQUMsS0FBS1EsT0FBVixFQUFtQixPQUFPLEVBQVA7O0FBRW5CLFdBQUssSUFBSWtMLElBQUksR0FBRzdkLFNBQVMsQ0FBQzVDLE1BQXJCLEVBQTZCOHNCLFNBQVMsR0FBRyxJQUFJamxCLEtBQUosQ0FBVTRZLElBQVYsQ0FBekMsRUFBMERFLElBQUksR0FBRyxDQUF0RSxFQUF5RUEsSUFBSSxHQUFHRixJQUFoRixFQUFzRkUsSUFBSSxFQUExRixFQUE4RjtBQUM1Rm1NLFFBQUFBLFNBQVMsQ0FBQ25NLElBQUQsQ0FBVCxHQUFrQi9kLFNBQVMsQ0FBQytkLElBQUQsQ0FBM0I7QUFDRDs7QUFFRCxVQUFJb00sTUFBTSxHQUFHRCxTQUFTLENBQUMvVixHQUFWLENBQWM0VSxnQkFBZCxFQUFnQzNVLE1BQWhDLENBQXVDLFVBQVVsTSxDQUFWLEVBQWE7QUFDL0QsZUFBT2lLLEtBQUssQ0FBQzZYLFFBQU4sQ0FBZTloQixDQUFmLENBQVA7QUFDRCxPQUZZLEVBRVY4RCxJQUZVLEVBQWI7QUFBQSxVQUdJdVIsT0FBTyxHQUFHLEVBSGQ7QUFJQSxVQUFJcGIsQ0FBQyxHQUFHLEtBQUtBLENBQWI7QUFBQSxVQUNJaEYsQ0FBQyxHQUFHLENBRFI7O0FBR0EsYUFBT2dGLENBQUMsR0FBRyxLQUFLOUMsQ0FBaEIsRUFBbUI7QUFDakIsWUFBSW1tQixLQUFLLEdBQUcyRSxNQUFNLENBQUNodEIsQ0FBRCxDQUFOLElBQWEsS0FBS2tDLENBQTlCO0FBQUEsWUFDSW9HLElBQUksR0FBRyxDQUFDK2YsS0FBRCxHQUFTLENBQUMsS0FBS25tQixDQUFmLEdBQW1CLEtBQUtBLENBQXhCLEdBQTRCbW1CLEtBRHZDO0FBRUFqSSxRQUFBQSxPQUFPLENBQUM1ZCxJQUFSLENBQWFncEIsUUFBUSxDQUFDRSxhQUFULENBQXVCMW1CLENBQXZCLEVBQTBCc0QsSUFBMUIsQ0FBYjtBQUNBdEQsUUFBQUEsQ0FBQyxHQUFHc0QsSUFBSjtBQUNBdEksUUFBQUEsQ0FBQyxJQUFJLENBQUw7QUFDRDs7QUFFRCxhQUFPb2dCLE9BQVA7QUFDRDtBQUNEOzs7Ozs7QUExQkE7O0FBa0NBak0sSUFBQUEsTUFBTSxDQUFDOFksT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWlCdEQsUUFBakIsRUFBMkI7QUFDMUMsVUFBSXZULEdBQUcsR0FBR3dULGdCQUFnQixDQUFDRCxRQUFELENBQTFCOztBQUVBLFVBQUksQ0FBQyxLQUFLblUsT0FBTixJQUFpQixDQUFDWSxHQUFHLENBQUNaLE9BQXRCLElBQWlDWSxHQUFHLENBQUNxVCxFQUFKLENBQU8sY0FBUCxNQUEyQixDQUFoRSxFQUFtRTtBQUNqRSxlQUFPLEVBQVA7QUFDRDs7QUFFRCxVQUFJemtCLENBQUMsR0FBRyxLQUFLQSxDQUFiO0FBQUEsVUFDSXFqQixLQURKO0FBQUEsVUFFSS9mLElBRko7QUFHQSxVQUFJOFgsT0FBTyxHQUFHLEVBQWQ7O0FBRUEsYUFBT3BiLENBQUMsR0FBRyxLQUFLOUMsQ0FBaEIsRUFBbUI7QUFDakJtbUIsUUFBQUEsS0FBSyxHQUFHcmpCLENBQUMsQ0FBQzBrQixJQUFGLENBQU90VCxHQUFQLENBQVI7QUFDQTlOLFFBQUFBLElBQUksR0FBRyxDQUFDK2YsS0FBRCxHQUFTLENBQUMsS0FBS25tQixDQUFmLEdBQW1CLEtBQUtBLENBQXhCLEdBQTRCbW1CLEtBQW5DO0FBQ0FqSSxRQUFBQSxPQUFPLENBQUM1ZCxJQUFSLENBQWFncEIsUUFBUSxDQUFDRSxhQUFULENBQXVCMW1CLENBQXZCLEVBQTBCc0QsSUFBMUIsQ0FBYjtBQUNBdEQsUUFBQUEsQ0FBQyxHQUFHc0QsSUFBSjtBQUNEOztBQUVELGFBQU84WCxPQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFyQkE7O0FBNEJBak0sSUFBQUEsTUFBTSxDQUFDK1ksYUFBUCxHQUF1QixTQUFTQSxhQUFULENBQXVCQyxhQUF2QixFQUFzQztBQUMzRCxVQUFJLENBQUMsS0FBSzNYLE9BQVYsRUFBbUIsT0FBTyxFQUFQO0FBQ25CLGFBQU8sS0FBS3lYLE9BQUwsQ0FBYSxLQUFLaHRCLE1BQUwsS0FBZ0JrdEIsYUFBN0IsRUFBNEMzakIsS0FBNUMsQ0FBa0QsQ0FBbEQsRUFBcUQyakIsYUFBckQsQ0FBUDtBQUNEO0FBQ0Q7Ozs7O0FBSkE7O0FBV0FoWixJQUFBQSxNQUFNLENBQUNpWixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBa0I1TSxLQUFsQixFQUF5QjtBQUN6QyxhQUFPLEtBQUt0ZSxDQUFMLEdBQVNzZSxLQUFLLENBQUN4YixDQUFmLElBQW9CLEtBQUtBLENBQUwsR0FBU3diLEtBQUssQ0FBQ3RlLENBQTFDO0FBQ0Q7QUFDRDs7Ozs7QUFIQTs7QUFVQWlTLElBQUFBLE1BQU0sQ0FBQ2taLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQjdNLEtBQXBCLEVBQTJCO0FBQzdDLFVBQUksQ0FBQyxLQUFLaEwsT0FBVixFQUFtQixPQUFPLEtBQVA7QUFDbkIsYUFBTyxDQUFDLEtBQUt0VCxDQUFOLEtBQVksQ0FBQ3NlLEtBQUssQ0FBQ3hiLENBQTFCO0FBQ0Q7QUFDRDs7Ozs7QUFKQTs7QUFXQW1QLElBQUFBLE1BQU0sQ0FBQ21aLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxDQUFrQjlNLEtBQWxCLEVBQXlCO0FBQ3pDLFVBQUksQ0FBQyxLQUFLaEwsT0FBVixFQUFtQixPQUFPLEtBQVA7QUFDbkIsYUFBTyxDQUFDZ0wsS0FBSyxDQUFDdGUsQ0FBUCxLQUFhLENBQUMsS0FBSzhDLENBQTFCO0FBQ0Q7QUFDRDs7Ozs7QUFKQTs7QUFXQW1QLElBQUFBLE1BQU0sQ0FBQ29aLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxDQUFpQi9NLEtBQWpCLEVBQXdCO0FBQ3ZDLFVBQUksQ0FBQyxLQUFLaEwsT0FBVixFQUFtQixPQUFPLEtBQVA7QUFDbkIsYUFBTyxLQUFLeFEsQ0FBTCxJQUFVd2IsS0FBSyxDQUFDeGIsQ0FBaEIsSUFBcUIsS0FBSzlDLENBQUwsSUFBVXNlLEtBQUssQ0FBQ3RlLENBQTVDO0FBQ0Q7QUFDRDs7Ozs7QUFKQTs7QUFXQWlTLElBQUFBLE1BQU0sQ0FBQ2tELE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxDQUFnQm1KLEtBQWhCLEVBQXVCO0FBQ3JDLFVBQUksQ0FBQyxLQUFLaEwsT0FBTixJQUFpQixDQUFDZ0wsS0FBSyxDQUFDaEwsT0FBNUIsRUFBcUM7QUFDbkMsZUFBTyxLQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLeFEsQ0FBTCxDQUFPcVMsTUFBUCxDQUFjbUosS0FBSyxDQUFDeGIsQ0FBcEIsS0FBMEIsS0FBSzlDLENBQUwsQ0FBT21WLE1BQVAsQ0FBY21KLEtBQUssQ0FBQ3RlLENBQXBCLENBQWpDO0FBQ0Q7QUFDRDs7Ozs7OztBQVBBOztBQWdCQWlTLElBQUFBLE1BQU0sQ0FBQ3FaLFlBQVAsR0FBc0IsU0FBU0EsWUFBVCxDQUFzQmhOLEtBQXRCLEVBQTZCO0FBQ2pELFVBQUksQ0FBQyxLQUFLaEwsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSXhRLENBQUMsR0FBRyxLQUFLQSxDQUFMLEdBQVN3YixLQUFLLENBQUN4YixDQUFmLEdBQW1CLEtBQUtBLENBQXhCLEdBQTRCd2IsS0FBSyxDQUFDeGIsQ0FBMUM7QUFBQSxVQUNJOUMsQ0FBQyxHQUFHLEtBQUtBLENBQUwsR0FBU3NlLEtBQUssQ0FBQ3RlLENBQWYsR0FBbUIsS0FBS0EsQ0FBeEIsR0FBNEJzZSxLQUFLLENBQUN0ZSxDQUQxQzs7QUFHQSxVQUFJOEMsQ0FBQyxHQUFHOUMsQ0FBUixFQUFXO0FBQ1QsZUFBTyxJQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBT3NwQixRQUFRLENBQUNFLGFBQVQsQ0FBdUIxbUIsQ0FBdkIsRUFBMEI5QyxDQUExQixDQUFQO0FBQ0Q7QUFDRjtBQUNEOzs7Ozs7QUFYQTs7QUFtQkFpUyxJQUFBQSxNQUFNLENBQUNzWixLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlak4sS0FBZixFQUFzQjtBQUNuQyxVQUFJLENBQUMsS0FBS2hMLE9BQVYsRUFBbUIsT0FBTyxJQUFQO0FBQ25CLFVBQUl4USxDQUFDLEdBQUcsS0FBS0EsQ0FBTCxHQUFTd2IsS0FBSyxDQUFDeGIsQ0FBZixHQUFtQixLQUFLQSxDQUF4QixHQUE0QndiLEtBQUssQ0FBQ3hiLENBQTFDO0FBQUEsVUFDSTlDLENBQUMsR0FBRyxLQUFLQSxDQUFMLEdBQVNzZSxLQUFLLENBQUN0ZSxDQUFmLEdBQW1CLEtBQUtBLENBQXhCLEdBQTRCc2UsS0FBSyxDQUFDdGUsQ0FEMUM7QUFFQSxhQUFPc3BCLFFBQVEsQ0FBQ0UsYUFBVCxDQUF1QjFtQixDQUF2QixFQUEwQjlDLENBQTFCLENBQVA7QUFDRDtBQUNEOzs7Ozs7QUFOQTs7QUFjQXNwQixJQUFBQSxRQUFRLENBQUNrQyxLQUFULEdBQWlCLFNBQVNBLEtBQVQsQ0FBZUMsU0FBZixFQUEwQjtBQUN6QyxVQUFJQyxxQkFBcUIsR0FBR0QsU0FBUyxDQUFDOWUsSUFBVixDQUFlLFVBQVV0TSxDQUFWLEVBQWFzckIsQ0FBYixFQUFnQjtBQUN6RCxlQUFPdHJCLENBQUMsQ0FBQ3lDLENBQUYsR0FBTTZvQixDQUFDLENBQUM3b0IsQ0FBZjtBQUNELE9BRjJCLEVBRXpCb0QsTUFGeUIsQ0FFbEIsVUFBVXdPLEtBQVYsRUFBaUJzTSxJQUFqQixFQUF1QjtBQUMvQixZQUFJNEssS0FBSyxHQUFHbFgsS0FBSyxDQUFDLENBQUQsQ0FBakI7QUFBQSxZQUNJbEQsT0FBTyxHQUFHa0QsS0FBSyxDQUFDLENBQUQsQ0FEbkI7O0FBR0EsWUFBSSxDQUFDbEQsT0FBTCxFQUFjO0FBQ1osaUJBQU8sQ0FBQ29hLEtBQUQsRUFBUTVLLElBQVIsQ0FBUDtBQUNELFNBRkQsTUFFTyxJQUFJeFAsT0FBTyxDQUFDMFosUUFBUixDQUFpQmxLLElBQWpCLEtBQTBCeFAsT0FBTyxDQUFDMlosVUFBUixDQUFtQm5LLElBQW5CLENBQTlCLEVBQXdEO0FBQzdELGlCQUFPLENBQUM0SyxLQUFELEVBQVFwYSxPQUFPLENBQUMrWixLQUFSLENBQWN2SyxJQUFkLENBQVIsQ0FBUDtBQUNELFNBRk0sTUFFQTtBQUNMLGlCQUFPLENBQUM0SyxLQUFLLENBQUNqWCxNQUFOLENBQWEsQ0FBQ25ELE9BQUQsQ0FBYixDQUFELEVBQTBCd1AsSUFBMUIsQ0FBUDtBQUNEO0FBQ0YsT0FiMkIsRUFhekIsQ0FBQyxFQUFELEVBQUssSUFBTCxDQWJ5QixDQUE1QjtBQUFBLFVBY0l2TSxLQUFLLEdBQUdpWCxxQkFBcUIsQ0FBQyxDQUFELENBZGpDO0FBQUEsVUFlSUcsTUFBSyxHQUFHSCxxQkFBcUIsQ0FBQyxDQUFELENBZmpDOztBQWlCQSxVQUFJRyxNQUFKLEVBQVc7QUFDVHBYLFFBQUFBLEtBQUssQ0FBQ25VLElBQU4sQ0FBV3VyQixNQUFYO0FBQ0Q7O0FBRUQsYUFBT3BYLEtBQVA7QUFDRDtBQUNEOzs7OztBQXhCQTs7QUErQkE2VSxJQUFBQSxRQUFRLENBQUN3QyxHQUFULEdBQWUsU0FBU0EsR0FBVCxDQUFhTCxTQUFiLEVBQXdCO0FBQ3JDLFVBQUlNLGdCQUFKOztBQUVBLFVBQUkzQyxLQUFLLEdBQUcsSUFBWjtBQUFBLFVBQ0k0QyxZQUFZLEdBQUcsQ0FEbkI7O0FBR0EsVUFBSTlOLE9BQU8sR0FBRyxFQUFkO0FBQUEsVUFDSStOLElBQUksR0FBR1IsU0FBUyxDQUFDM1csR0FBVixDQUFjLFVBQVVoWCxDQUFWLEVBQWE7QUFDcEMsZUFBTyxDQUFDO0FBQ05vdUIsVUFBQUEsSUFBSSxFQUFFcHVCLENBQUMsQ0FBQ2dGLENBREY7QUFFTnVILFVBQUFBLElBQUksRUFBRTtBQUZBLFNBQUQsRUFHSjtBQUNENmhCLFVBQUFBLElBQUksRUFBRXB1QixDQUFDLENBQUNrQyxDQURQO0FBRURxSyxVQUFBQSxJQUFJLEVBQUU7QUFGTCxTQUhJLENBQVA7QUFPRCxPQVJVLENBRFg7QUFBQSxVQVVJOGhCLFNBQVMsR0FBRyxDQUFDSixnQkFBZ0IsR0FBR25tQixLQUFLLENBQUNqSCxTQUExQixFQUFxQ2dXLE1BQXJDLENBQTRDcFUsS0FBNUMsQ0FBa0R3ckIsZ0JBQWxELEVBQW9FRSxJQUFwRSxDQVZoQjtBQUFBLFVBV0lsbUIsR0FBRyxHQUFHb21CLFNBQVMsQ0FBQ3hmLElBQVYsQ0FBZSxVQUFVdE0sQ0FBVixFQUFhc3JCLENBQWIsRUFBZ0I7QUFDdkMsZUFBT3RyQixDQUFDLENBQUM2ckIsSUFBRixHQUFTUCxDQUFDLENBQUNPLElBQWxCO0FBQ0QsT0FGUyxDQVhWOztBQWVBLFdBQUssSUFBSTVjLFNBQVMsR0FBR3ZKLEdBQWhCLEVBQXFCd0osUUFBUSxHQUFHM0osS0FBSyxDQUFDQyxPQUFOLENBQWN5SixTQUFkLENBQWhDLEVBQTBERSxFQUFFLEdBQUcsQ0FBL0QsRUFBa0VGLFNBQVMsR0FBR0MsUUFBUSxHQUFHRCxTQUFILEdBQWVBLFNBQVMsQ0FBQ0csTUFBTSxDQUFDQyxRQUFSLENBQVQsRUFBMUcsSUFBMEk7QUFDeEksWUFBSTBjLEtBQUo7O0FBRUEsWUFBSTdjLFFBQUosRUFBYztBQUNaLGNBQUlDLEVBQUUsSUFBSUYsU0FBUyxDQUFDdlIsTUFBcEIsRUFBNEI7QUFDNUJxdUIsVUFBQUEsS0FBSyxHQUFHOWMsU0FBUyxDQUFDRSxFQUFFLEVBQUgsQ0FBakI7QUFDRCxTQUhELE1BR087QUFDTEEsVUFBQUEsRUFBRSxHQUFHRixTQUFTLENBQUNsSixJQUFWLEVBQUw7QUFDQSxjQUFJb0osRUFBRSxDQUFDSSxJQUFQLEVBQWE7QUFDYndjLFVBQUFBLEtBQUssR0FBRzVjLEVBQUUsQ0FBQ2hPLEtBQVg7QUFDRDs7QUFFRCxZQUFJMUQsQ0FBQyxHQUFHc3VCLEtBQVI7QUFDQUosUUFBQUEsWUFBWSxJQUFJbHVCLENBQUMsQ0FBQ3VNLElBQUYsS0FBVyxHQUFYLEdBQWlCLENBQWpCLEdBQXFCLENBQUMsQ0FBdEM7O0FBRUEsWUFBSTJoQixZQUFZLEtBQUssQ0FBckIsRUFBd0I7QUFDdEI1QyxVQUFBQSxLQUFLLEdBQUd0ckIsQ0FBQyxDQUFDb3VCLElBQVY7QUFDRCxTQUZELE1BRU87QUFDTCxjQUFJOUMsS0FBSyxJQUFJLENBQUNBLEtBQUQsS0FBVyxDQUFDdHJCLENBQUMsQ0FBQ291QixJQUEzQixFQUFpQztBQUMvQmhPLFlBQUFBLE9BQU8sQ0FBQzVkLElBQVIsQ0FBYWdwQixRQUFRLENBQUNFLGFBQVQsQ0FBdUJKLEtBQXZCLEVBQThCdHJCLENBQUMsQ0FBQ291QixJQUFoQyxDQUFiO0FBQ0Q7O0FBRUQ5QyxVQUFBQSxLQUFLLEdBQUcsSUFBUjtBQUNEO0FBQ0Y7O0FBRUQsYUFBT0UsUUFBUSxDQUFDa0MsS0FBVCxDQUFldE4sT0FBZixDQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFqREE7O0FBd0RBak0sSUFBQUEsTUFBTSxDQUFDb2EsVUFBUCxHQUFvQixTQUFTQSxVQUFULEdBQXNCO0FBQ3hDLFVBQUlsWSxNQUFNLEdBQUcsSUFBYjs7QUFFQSxXQUFLLElBQUkwSyxLQUFLLEdBQUdsZSxTQUFTLENBQUM1QyxNQUF0QixFQUE4QjB0QixTQUFTLEdBQUcsSUFBSTdsQixLQUFKLENBQVVpWixLQUFWLENBQTFDLEVBQTRERSxLQUFLLEdBQUcsQ0FBekUsRUFBNEVBLEtBQUssR0FBR0YsS0FBcEYsRUFBMkZFLEtBQUssRUFBaEcsRUFBb0c7QUFDbEcwTSxRQUFBQSxTQUFTLENBQUMxTSxLQUFELENBQVQsR0FBbUJwZSxTQUFTLENBQUNvZSxLQUFELENBQTVCO0FBQ0Q7O0FBRUQsYUFBT3VLLFFBQVEsQ0FBQ3dDLEdBQVQsQ0FBYSxDQUFDLElBQUQsRUFBT25YLE1BQVAsQ0FBYzhXLFNBQWQsQ0FBYixFQUF1QzNXLEdBQXZDLENBQTJDLFVBQVVoWCxDQUFWLEVBQWE7QUFDN0QsZUFBT3FXLE1BQU0sQ0FBQ21YLFlBQVAsQ0FBb0J4dEIsQ0FBcEIsQ0FBUDtBQUNELE9BRk0sRUFFSmlYLE1BRkksQ0FFRyxVQUFValgsQ0FBVixFQUFhO0FBQ3JCLGVBQU9BLENBQUMsSUFBSSxDQUFDQSxDQUFDLENBQUN5c0IsT0FBRixFQUFiO0FBQ0QsT0FKTSxDQUFQO0FBS0Q7QUFDRDs7OztBQWJBOztBQW1CQXRZLElBQUFBLE1BQU0sQ0FBQ25TLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxHQUFvQjtBQUNwQyxVQUFJLENBQUMsS0FBS3dULE9BQVYsRUFBbUIsT0FBTzRWLFNBQVA7QUFDbkIsYUFBTyxNQUFNLEtBQUtwbUIsQ0FBTCxDQUFPdWtCLEtBQVAsRUFBTixHQUF1QixVQUF2QixHQUFvQyxLQUFLcm5CLENBQUwsQ0FBT3FuQixLQUFQLEVBQXBDLEdBQXFELEdBQTVEO0FBQ0Q7QUFDRDs7Ozs7O0FBSkE7O0FBWUFwVixJQUFBQSxNQUFNLENBQUNvVixLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlaFcsSUFBZixFQUFxQjtBQUNsQyxVQUFJLENBQUMsS0FBS2lDLE9BQVYsRUFBbUIsT0FBTzRWLFNBQVA7QUFDbkIsYUFBTyxLQUFLcG1CLENBQUwsQ0FBT3VrQixLQUFQLENBQWFoVyxJQUFiLElBQXFCLEdBQXJCLEdBQTJCLEtBQUtyUixDQUFMLENBQU9xbkIsS0FBUCxDQUFhaFcsSUFBYixDQUFsQztBQUNEO0FBQ0Q7Ozs7OztBQUpBOztBQVlBWSxJQUFBQSxNQUFNLENBQUNxYSxTQUFQLEdBQW1CLFNBQVNBLFNBQVQsR0FBcUI7QUFDdEMsVUFBSSxDQUFDLEtBQUtoWixPQUFWLEVBQW1CLE9BQU80VixTQUFQO0FBQ25CLGFBQU8sS0FBS3BtQixDQUFMLENBQU93cEIsU0FBUCxLQUFxQixHQUFyQixHQUEyQixLQUFLdHNCLENBQUwsQ0FBT3NzQixTQUFQLEVBQWxDO0FBQ0Q7QUFDRDs7Ozs7OztBQUpBOztBQWFBcmEsSUFBQUEsTUFBTSxDQUFDc2EsU0FBUCxHQUFtQixTQUFTQSxTQUFULENBQW1CbGIsSUFBbkIsRUFBeUI7QUFDMUMsVUFBSSxDQUFDLEtBQUtpQyxPQUFWLEVBQW1CLE9BQU80VixTQUFQO0FBQ25CLGFBQU8sS0FBS3BtQixDQUFMLENBQU95cEIsU0FBUCxDQUFpQmxiLElBQWpCLElBQXlCLEdBQXpCLEdBQStCLEtBQUtyUixDQUFMLENBQU91c0IsU0FBUCxDQUFpQmxiLElBQWpCLENBQXRDO0FBQ0Q7QUFDRDs7Ozs7OztBQUpBOztBQWFBWSxJQUFBQSxNQUFNLENBQUNnVixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBa0J1RixVQUFsQixFQUE4QkMsTUFBOUIsRUFBc0M7QUFDdEQsVUFBSUMsS0FBSyxHQUFHRCxNQUFNLEtBQUssS0FBSyxDQUFoQixHQUFvQixFQUFwQixHQUF5QkEsTUFBckM7QUFBQSxVQUNJRSxlQUFlLEdBQUdELEtBQUssQ0FBQ0UsU0FENUI7QUFBQSxVQUVJQSxTQUFTLEdBQUdELGVBQWUsS0FBSyxLQUFLLENBQXpCLEdBQTZCLEtBQTdCLEdBQXFDQSxlQUZyRDs7QUFJQSxVQUFJLENBQUMsS0FBS3JaLE9BQVYsRUFBbUIsT0FBTzRWLFNBQVA7QUFDbkIsYUFBTyxLQUFLLEtBQUtwbUIsQ0FBTCxDQUFPbWtCLFFBQVAsQ0FBZ0J1RixVQUFoQixDQUFMLEdBQW1DSSxTQUFuQyxHQUErQyxLQUFLNXNCLENBQUwsQ0FBT2luQixRQUFQLENBQWdCdUYsVUFBaEIsQ0FBdEQ7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7QUFSQTs7QUFzQkF2YSxJQUFBQSxNQUFNLENBQUNrWSxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0IzbkIsSUFBcEIsRUFBMEI2TyxJQUExQixFQUFnQztBQUNsRCxVQUFJLENBQUMsS0FBS2lDLE9BQVYsRUFBbUI7QUFDakIsZUFBT2lTLFFBQVEsQ0FBQ2tCLE9BQVQsQ0FBaUIsS0FBS29HLGFBQXRCLENBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUs3c0IsQ0FBTCxDQUFPcXFCLElBQVAsQ0FBWSxLQUFLdm5CLENBQWpCLEVBQW9CTixJQUFwQixFQUEwQjZPLElBQTFCLENBQVA7QUFDRDtBQUNEOzs7Ozs7O0FBUEE7O0FBZ0JBWSxJQUFBQSxNQUFNLENBQUM2YSxZQUFQLEdBQXNCLFNBQVNBLFlBQVQsQ0FBc0JDLEtBQXRCLEVBQTZCO0FBQ2pELGFBQU96RCxRQUFRLENBQUNFLGFBQVQsQ0FBdUJ1RCxLQUFLLENBQUMsS0FBS2pxQixDQUFOLENBQTVCLEVBQXNDaXFCLEtBQUssQ0FBQyxLQUFLL3NCLENBQU4sQ0FBM0MsQ0FBUDtBQUNELEtBRkQ7O0FBSUF6QixJQUFBQSxZQUFZLENBQUMrcUIsUUFBRCxFQUFXLENBQUM7QUFDdEJockIsTUFBQUEsR0FBRyxFQUFFLE9BRGlCO0FBRXRCK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBS3hRLENBQXBCLEdBQXdCLElBQS9CO0FBQ0Q7QUFDRDs7Ozs7QUFMc0IsS0FBRCxFQVVwQjtBQUNEeEUsTUFBQUEsR0FBRyxFQUFFLEtBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUt0VCxDQUFwQixHQUF3QixJQUEvQjtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0FWb0IsRUFvQnBCO0FBQ0QxQixNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUt3ckIsYUFBTCxLQUF1QixJQUE5QjtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0FwQm9CLEVBOEJwQjtBQUNEdnVCLE1BQUFBLEdBQUcsRUFBRSxlQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS29sQixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhM2tCLE1BQTVCLEdBQXFDLElBQTVDO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQTlCb0IsRUF3Q3BCO0FBQ0R4RCxNQUFBQSxHQUFHLEVBQUUsb0JBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLb2xCLE9BQUwsR0FBZSxLQUFLQSxPQUFMLENBQWF4UixXQUE1QixHQUEwQyxJQUFqRDtBQUNEO0FBSkEsS0F4Q29CLENBQVgsQ0FBWjs7QUErQ0EsV0FBT3FVLFFBQVA7QUFDRCxHQXBxQkQsRUFGQTtBQXdxQkE7Ozs7O0FBSUEsTUFBSTBELElBQUk7QUFDUjtBQUNBLGNBQVk7QUFDVixhQUFTQSxJQUFULEdBQWdCLENBQUU7QUFFbEI7Ozs7Ozs7QUFLQUEsSUFBQUEsSUFBSSxDQUFDQyxNQUFMLEdBQWMsU0FBU0EsTUFBVCxDQUFnQjFaLElBQWhCLEVBQXNCO0FBQ2xDLFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUdxRixRQUFRLENBQUNQLFdBQWhCO0FBQ0Q7O0FBRUQsVUFBSTZVLEtBQUssR0FBRzNTLFFBQVEsQ0FBQzRHLEtBQVQsR0FBaUJnTSxPQUFqQixDQUF5QjVaLElBQXpCLEVBQStCalMsR0FBL0IsQ0FBbUM7QUFDN0M0QixRQUFBQSxLQUFLLEVBQUU7QUFEc0MsT0FBbkMsQ0FBWjtBQUdBLGFBQU8sQ0FBQ3FRLElBQUksQ0FBQytILFNBQU4sSUFBbUI0UixLQUFLLENBQUNsaEIsTUFBTixLQUFpQmtoQixLQUFLLENBQUM1ckIsR0FBTixDQUFVO0FBQ25ENEIsUUFBQUEsS0FBSyxFQUFFO0FBRDRDLE9BQVYsRUFFeEM4SSxNQUZIO0FBR0Q7QUFDRDs7Ozs7QUFaQTs7QUFtQkFnaEIsSUFBQUEsSUFBSSxDQUFDSSxlQUFMLEdBQXVCLFNBQVNBLGVBQVQsQ0FBeUI3WixJQUF6QixFQUErQjtBQUNwRCxhQUFPd0QsUUFBUSxDQUFDRyxnQkFBVCxDQUEwQjNELElBQTFCLEtBQW1Dd0QsUUFBUSxDQUFDSyxXQUFULENBQXFCN0QsSUFBckIsQ0FBMUM7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7OztBQUhBOztBQW1CQXlaLElBQUFBLElBQUksQ0FBQzVVLGFBQUwsR0FBcUIsU0FBU2lWLGVBQVQsQ0FBeUJqbUIsS0FBekIsRUFBZ0M7QUFDbkQsYUFBT2dSLGFBQWEsQ0FBQ2hSLEtBQUQsRUFBUXdSLFFBQVEsQ0FBQ1AsV0FBakIsQ0FBcEI7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7O0FBSEE7O0FBcUJBMlUsSUFBQUEsSUFBSSxDQUFDamdCLE1BQUwsR0FBYyxTQUFTQSxNQUFULENBQWdCaFAsTUFBaEIsRUFBd0J3ZSxLQUF4QixFQUErQjtBQUMzQyxVQUFJeGUsTUFBTSxLQUFLLEtBQUssQ0FBcEIsRUFBdUI7QUFDckJBLFFBQUFBLE1BQU0sR0FBRyxNQUFUO0FBQ0Q7O0FBRUQsVUFBSTRSLElBQUksR0FBRzRNLEtBQUssS0FBSyxLQUFLLENBQWYsR0FBbUIsRUFBbkIsR0FBd0JBLEtBQW5DO0FBQUEsVUFDSStRLFdBQVcsR0FBRzNkLElBQUksQ0FBQ2hHLE1BRHZCO0FBQUEsVUFFSUEsTUFBTSxHQUFHMmpCLFdBQVcsS0FBSyxLQUFLLENBQXJCLEdBQXlCLElBQXpCLEdBQWdDQSxXQUY3QztBQUFBLFVBR0lDLG9CQUFvQixHQUFHNWQsSUFBSSxDQUFDcUosZUFIaEM7QUFBQSxVQUlJQSxlQUFlLEdBQUd1VSxvQkFBb0IsS0FBSyxLQUFLLENBQTlCLEdBQWtDLElBQWxDLEdBQXlDQSxvQkFKL0Q7QUFBQSxVQUtJQyxtQkFBbUIsR0FBRzdkLElBQUksQ0FBQ3VELGNBTC9CO0FBQUEsVUFNSUEsY0FBYyxHQUFHc2EsbUJBQW1CLEtBQUssS0FBSyxDQUE3QixHQUFpQyxTQUFqQyxHQUE2Q0EsbUJBTmxFOztBQVFBLGFBQU8xVSxNQUFNLENBQUMvWixNQUFQLENBQWM0SyxNQUFkLEVBQXNCcVAsZUFBdEIsRUFBdUM5RixjQUF2QyxFQUF1RG5HLE1BQXZELENBQThEaFAsTUFBOUQsQ0FBUDtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7OztBQWZBOztBQTZCQWl2QixJQUFBQSxJQUFJLENBQUNTLFlBQUwsR0FBb0IsU0FBU0EsWUFBVCxDQUFzQjF2QixNQUF0QixFQUE4QjB1QixNQUE5QixFQUFzQztBQUN4RCxVQUFJMXVCLE1BQU0sS0FBSyxLQUFLLENBQXBCLEVBQXVCO0FBQ3JCQSxRQUFBQSxNQUFNLEdBQUcsTUFBVDtBQUNEOztBQUVELFVBQUkyVyxLQUFLLEdBQUcrWCxNQUFNLEtBQUssS0FBSyxDQUFoQixHQUFvQixFQUFwQixHQUF5QkEsTUFBckM7QUFBQSxVQUNJaUIsWUFBWSxHQUFHaFosS0FBSyxDQUFDL0ssTUFEekI7QUFBQSxVQUVJQSxNQUFNLEdBQUcrakIsWUFBWSxLQUFLLEtBQUssQ0FBdEIsR0FBMEIsSUFBMUIsR0FBaUNBLFlBRjlDO0FBQUEsVUFHSUMscUJBQXFCLEdBQUdqWixLQUFLLENBQUNzRSxlQUhsQztBQUFBLFVBSUlBLGVBQWUsR0FBRzJVLHFCQUFxQixLQUFLLEtBQUssQ0FBL0IsR0FBbUMsSUFBbkMsR0FBMENBLHFCQUpoRTtBQUFBLFVBS0lDLG9CQUFvQixHQUFHbFosS0FBSyxDQUFDeEIsY0FMakM7QUFBQSxVQU1JQSxjQUFjLEdBQUcwYSxvQkFBb0IsS0FBSyxLQUFLLENBQTlCLEdBQWtDLFNBQWxDLEdBQThDQSxvQkFObkU7O0FBUUEsYUFBTzlVLE1BQU0sQ0FBQy9aLE1BQVAsQ0FBYzRLLE1BQWQsRUFBc0JxUCxlQUF0QixFQUF1QzlGLGNBQXZDLEVBQXVEbkcsTUFBdkQsQ0FBOERoUCxNQUE5RCxFQUFzRSxJQUF0RSxDQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7Ozs7OztBQWZBOztBQThCQWl2QixJQUFBQSxJQUFJLENBQUM3ZixRQUFMLEdBQWdCLFNBQVNBLFFBQVQsQ0FBa0JwUCxNQUFsQixFQUEwQjh2QixNQUExQixFQUFrQztBQUNoRCxVQUFJOXZCLE1BQU0sS0FBSyxLQUFLLENBQXBCLEVBQXVCO0FBQ3JCQSxRQUFBQSxNQUFNLEdBQUcsTUFBVDtBQUNEOztBQUVELFVBQUlxdUIsS0FBSyxHQUFHeUIsTUFBTSxLQUFLLEtBQUssQ0FBaEIsR0FBb0IsRUFBcEIsR0FBeUJBLE1BQXJDO0FBQUEsVUFDSUMsWUFBWSxHQUFHMUIsS0FBSyxDQUFDemlCLE1BRHpCO0FBQUEsVUFFSUEsTUFBTSxHQUFHbWtCLFlBQVksS0FBSyxLQUFLLENBQXRCLEdBQTBCLElBQTFCLEdBQWlDQSxZQUY5QztBQUFBLFVBR0lDLHFCQUFxQixHQUFHM0IsS0FBSyxDQUFDcFQsZUFIbEM7QUFBQSxVQUlJQSxlQUFlLEdBQUcrVSxxQkFBcUIsS0FBSyxLQUFLLENBQS9CLEdBQW1DLElBQW5DLEdBQTBDQSxxQkFKaEU7O0FBTUEsYUFBT2pWLE1BQU0sQ0FBQy9aLE1BQVAsQ0FBYzRLLE1BQWQsRUFBc0JxUCxlQUF0QixFQUF1QyxJQUF2QyxFQUE2QzdMLFFBQTdDLENBQXNEcFAsTUFBdEQsQ0FBUDtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7O0FBYkE7O0FBMEJBaXZCLElBQUFBLElBQUksQ0FBQ2dCLGNBQUwsR0FBc0IsU0FBU0EsY0FBVCxDQUF3Qmp3QixNQUF4QixFQUFnQ2t3QixNQUFoQyxFQUF3QztBQUM1RCxVQUFJbHdCLE1BQU0sS0FBSyxLQUFLLENBQXBCLEVBQXVCO0FBQ3JCQSxRQUFBQSxNQUFNLEdBQUcsTUFBVDtBQUNEOztBQUVELFVBQUkydUIsS0FBSyxHQUFHdUIsTUFBTSxLQUFLLEtBQUssQ0FBaEIsR0FBb0IsRUFBcEIsR0FBeUJBLE1BQXJDO0FBQUEsVUFDSUMsWUFBWSxHQUFHeEIsS0FBSyxDQUFDL2lCLE1BRHpCO0FBQUEsVUFFSUEsTUFBTSxHQUFHdWtCLFlBQVksS0FBSyxLQUFLLENBQXRCLEdBQTBCLElBQTFCLEdBQWlDQSxZQUY5QztBQUFBLFVBR0lDLHFCQUFxQixHQUFHekIsS0FBSyxDQUFDMVQsZUFIbEM7QUFBQSxVQUlJQSxlQUFlLEdBQUdtVixxQkFBcUIsS0FBSyxLQUFLLENBQS9CLEdBQW1DLElBQW5DLEdBQTBDQSxxQkFKaEU7O0FBTUEsYUFBT3JWLE1BQU0sQ0FBQy9aLE1BQVAsQ0FBYzRLLE1BQWQsRUFBc0JxUCxlQUF0QixFQUF1QyxJQUF2QyxFQUE2QzdMLFFBQTdDLENBQXNEcFAsTUFBdEQsRUFBOEQsSUFBOUQsQ0FBUDtBQUNEO0FBQ0Q7Ozs7Ozs7O0FBYkE7O0FBdUJBaXZCLElBQUFBLElBQUksQ0FBQzVmLFNBQUwsR0FBaUIsU0FBU0EsU0FBVCxDQUFtQmdoQixNQUFuQixFQUEyQjtBQUMxQyxVQUFJQyxLQUFLLEdBQUdELE1BQU0sS0FBSyxLQUFLLENBQWhCLEdBQW9CLEVBQXBCLEdBQXlCQSxNQUFyQztBQUFBLFVBQ0lFLFlBQVksR0FBR0QsS0FBSyxDQUFDMWtCLE1BRHpCO0FBQUEsVUFFSUEsTUFBTSxHQUFHMmtCLFlBQVksS0FBSyxLQUFLLENBQXRCLEdBQTBCLElBQTFCLEdBQWlDQSxZQUY5Qzs7QUFJQSxhQUFPeFYsTUFBTSxDQUFDL1osTUFBUCxDQUFjNEssTUFBZCxFQUFzQnlELFNBQXRCLEVBQVA7QUFDRDtBQUNEOzs7Ozs7Ozs7O0FBUEE7O0FBbUJBNGYsSUFBQUEsSUFBSSxDQUFDeGYsSUFBTCxHQUFZLFNBQVNBLElBQVQsQ0FBY3pQLE1BQWQsRUFBc0J3d0IsTUFBdEIsRUFBOEI7QUFDeEMsVUFBSXh3QixNQUFNLEtBQUssS0FBSyxDQUFwQixFQUF1QjtBQUNyQkEsUUFBQUEsTUFBTSxHQUFHLE9BQVQ7QUFDRDs7QUFFRCxVQUFJeXdCLEtBQUssR0FBR0QsTUFBTSxLQUFLLEtBQUssQ0FBaEIsR0FBb0IsRUFBcEIsR0FBeUJBLE1BQXJDO0FBQUEsVUFDSUUsWUFBWSxHQUFHRCxLQUFLLENBQUM3a0IsTUFEekI7QUFBQSxVQUVJQSxNQUFNLEdBQUc4a0IsWUFBWSxLQUFLLEtBQUssQ0FBdEIsR0FBMEIsSUFBMUIsR0FBaUNBLFlBRjlDOztBQUlBLGFBQU8zVixNQUFNLENBQUMvWixNQUFQLENBQWM0SyxNQUFkLEVBQXNCLElBQXRCLEVBQTRCLFNBQTVCLEVBQXVDNkQsSUFBdkMsQ0FBNEN6UCxNQUE1QyxDQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7Ozs7QUFYQTs7QUF3QkFpdkIsSUFBQUEsSUFBSSxDQUFDMEIsUUFBTCxHQUFnQixTQUFTQSxRQUFULEdBQW9CO0FBQ2xDLFVBQUl6a0IsSUFBSSxHQUFHLEtBQVg7QUFBQSxVQUNJMGtCLFVBQVUsR0FBRyxLQURqQjtBQUFBLFVBRUlDLEtBQUssR0FBRyxLQUZaO0FBQUEsVUFHSUMsUUFBUSxHQUFHLEtBSGY7O0FBS0EsVUFBSTFwQixPQUFPLEVBQVgsRUFBZTtBQUNiOEUsUUFBQUEsSUFBSSxHQUFHLElBQVA7QUFDQTBrQixRQUFBQSxVQUFVLEdBQUdycEIsZ0JBQWdCLEVBQTdCO0FBQ0F1cEIsUUFBQUEsUUFBUSxHQUFHcnBCLFdBQVcsRUFBdEI7O0FBRUEsWUFBSTtBQUNGb3BCLFVBQUFBLEtBQUssR0FBRyxJQUFJeHBCLElBQUksQ0FBQ0MsY0FBVCxDQUF3QixJQUF4QixFQUE4QjtBQUNwQ3VFLFlBQUFBLFFBQVEsRUFBRTtBQUQwQixXQUE5QixFQUVMNEksZUFGSyxHQUVhNUksUUFGYixLQUUwQixrQkFGbEM7QUFHRCxTQUpELENBSUUsT0FBTzVKLENBQVAsRUFBVTtBQUNWNHVCLFVBQUFBLEtBQUssR0FBRyxLQUFSO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPO0FBQ0wza0IsUUFBQUEsSUFBSSxFQUFFQSxJQUREO0FBRUwwa0IsUUFBQUEsVUFBVSxFQUFFQSxVQUZQO0FBR0xDLFFBQUFBLEtBQUssRUFBRUEsS0FIRjtBQUlMQyxRQUFBQSxRQUFRLEVBQUVBO0FBSkwsT0FBUDtBQU1ELEtBMUJEOztBQTRCQSxXQUFPN0IsSUFBUDtBQUNELEdBdlBELEVBRkE7O0FBMlBBLFdBQVM4QixPQUFULENBQWlCQyxPQUFqQixFQUEwQkMsS0FBMUIsRUFBaUM7QUFDL0IsUUFBSUMsV0FBVyxHQUFHLFNBQVNBLFdBQVQsQ0FBcUJ2aEIsRUFBckIsRUFBeUI7QUFDekMsYUFBT0EsRUFBRSxDQUFDd2hCLEtBQUgsQ0FBUyxDQUFULEVBQVk7QUFDakJDLFFBQUFBLGFBQWEsRUFBRTtBQURFLE9BQVosRUFFSi9FLE9BRkksQ0FFSSxLQUZKLEVBRVd6UyxPQUZYLEVBQVA7QUFHRCxLQUpEO0FBQUEsUUFLSTJDLEVBQUUsR0FBRzJVLFdBQVcsQ0FBQ0QsS0FBRCxDQUFYLEdBQXFCQyxXQUFXLENBQUNGLE9BQUQsQ0FMekM7O0FBT0EsV0FBTzluQixJQUFJLENBQUNDLEtBQUwsQ0FBV3FlLFFBQVEsQ0FBQ2hLLFVBQVQsQ0FBb0JqQixFQUFwQixFQUF3QmlOLEVBQXhCLENBQTJCLE1BQTNCLENBQVgsQ0FBUDtBQUNEOztBQUVELFdBQVM2SCxjQUFULENBQXdCalEsTUFBeEIsRUFBZ0M2UCxLQUFoQyxFQUF1QzlnQixLQUF2QyxFQUE4QztBQUM1QyxRQUFJbWhCLE9BQU8sR0FBRyxDQUFDLENBQUMsT0FBRCxFQUFVLFVBQVVodkIsQ0FBVixFQUFhc3JCLENBQWIsRUFBZ0I7QUFDdkMsYUFBT0EsQ0FBQyxDQUFDMW9CLElBQUYsR0FBUzVDLENBQUMsQ0FBQzRDLElBQWxCO0FBQ0QsS0FGYyxDQUFELEVBRVYsQ0FBQyxRQUFELEVBQVcsVUFBVTVDLENBQVYsRUFBYXNyQixDQUFiLEVBQWdCO0FBQzdCLGFBQU9BLENBQUMsQ0FBQ3pvQixLQUFGLEdBQVU3QyxDQUFDLENBQUM2QyxLQUFaLEdBQW9CLENBQUN5b0IsQ0FBQyxDQUFDMW9CLElBQUYsR0FBUzVDLENBQUMsQ0FBQzRDLElBQVosSUFBb0IsRUFBL0M7QUFDRCxLQUZHLENBRlUsRUFJVixDQUFDLE9BQUQsRUFBVSxVQUFVNUMsQ0FBVixFQUFhc3JCLENBQWIsRUFBZ0I7QUFDNUIsVUFBSXJkLElBQUksR0FBR3dnQixPQUFPLENBQUN6dUIsQ0FBRCxFQUFJc3JCLENBQUosQ0FBbEI7QUFDQSxhQUFPLENBQUNyZCxJQUFJLEdBQUdBLElBQUksR0FBRyxDQUFmLElBQW9CLENBQTNCO0FBQ0QsS0FIRyxDQUpVLEVBT1YsQ0FBQyxNQUFELEVBQVN3Z0IsT0FBVCxDQVBVLENBQWQ7QUFRQSxRQUFJNVEsT0FBTyxHQUFHLEVBQWQ7QUFDQSxRQUFJb1IsV0FBSixFQUFpQkMsU0FBakI7O0FBRUEsU0FBSyxJQUFJL2YsRUFBRSxHQUFHLENBQVQsRUFBWWdnQixRQUFRLEdBQUdILE9BQTVCLEVBQXFDN2YsRUFBRSxHQUFHZ2dCLFFBQVEsQ0FBQ3p4QixNQUFuRCxFQUEyRHlSLEVBQUUsRUFBN0QsRUFBaUU7QUFDL0QsVUFBSWlnQixXQUFXLEdBQUdELFFBQVEsQ0FBQ2hnQixFQUFELENBQTFCO0FBQUEsVUFDSWhOLElBQUksR0FBR2l0QixXQUFXLENBQUMsQ0FBRCxDQUR0QjtBQUFBLFVBRUlDLE1BQU0sR0FBR0QsV0FBVyxDQUFDLENBQUQsQ0FGeEI7O0FBSUEsVUFBSXZoQixLQUFLLENBQUNwTixPQUFOLENBQWMwQixJQUFkLEtBQXVCLENBQTNCLEVBQThCO0FBQzVCLFlBQUltdEIsWUFBSjs7QUFFQUwsUUFBQUEsV0FBVyxHQUFHOXNCLElBQWQ7QUFDQSxZQUFJb3RCLEtBQUssR0FBR0YsTUFBTSxDQUFDdlEsTUFBRCxFQUFTNlAsS0FBVCxDQUFsQjtBQUNBTyxRQUFBQSxTQUFTLEdBQUdwUSxNQUFNLENBQUNxSSxJQUFQLEVBQWFtSSxZQUFZLEdBQUcsRUFBZixFQUFtQkEsWUFBWSxDQUFDbnRCLElBQUQsQ0FBWixHQUFxQm90QixLQUF4QyxFQUErQ0QsWUFBNUQsRUFBWjs7QUFFQSxZQUFJSixTQUFTLEdBQUdQLEtBQWhCLEVBQXVCO0FBQ3JCLGNBQUlhLGFBQUo7O0FBRUExUSxVQUFBQSxNQUFNLEdBQUdBLE1BQU0sQ0FBQ3FJLElBQVAsRUFBYXFJLGFBQWEsR0FBRyxFQUFoQixFQUFvQkEsYUFBYSxDQUFDcnRCLElBQUQsQ0FBYixHQUFzQm90QixLQUFLLEdBQUcsQ0FBbEQsRUFBcURDLGFBQWxFLEVBQVQ7QUFDQUQsVUFBQUEsS0FBSyxJQUFJLENBQVQ7QUFDRCxTQUxELE1BS087QUFDTHpRLFVBQUFBLE1BQU0sR0FBR29RLFNBQVQ7QUFDRDs7QUFFRHJSLFFBQUFBLE9BQU8sQ0FBQzFiLElBQUQsQ0FBUCxHQUFnQm90QixLQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsV0FBTyxDQUFDelEsTUFBRCxFQUFTakIsT0FBVCxFQUFrQnFSLFNBQWxCLEVBQTZCRCxXQUE3QixDQUFQO0FBQ0Q7O0FBRUQsV0FBU1EsS0FBVCxDQUFnQmYsT0FBaEIsRUFBeUJDLEtBQXpCLEVBQWdDOWdCLEtBQWhDLEVBQXVDbUQsSUFBdkMsRUFBNkM7QUFDM0MsUUFBSTBlLGVBQWUsR0FBR1gsY0FBYyxDQUFDTCxPQUFELEVBQVVDLEtBQVYsRUFBaUI5Z0IsS0FBakIsQ0FBcEM7QUFBQSxRQUNJaVIsTUFBTSxHQUFHNFEsZUFBZSxDQUFDLENBQUQsQ0FENUI7QUFBQSxRQUVJN1IsT0FBTyxHQUFHNlIsZUFBZSxDQUFDLENBQUQsQ0FGN0I7QUFBQSxRQUdJUixTQUFTLEdBQUdRLGVBQWUsQ0FBQyxDQUFELENBSC9CO0FBQUEsUUFJSVQsV0FBVyxHQUFHUyxlQUFlLENBQUMsQ0FBRCxDQUpqQzs7QUFNQSxRQUFJQyxlQUFlLEdBQUdoQixLQUFLLEdBQUc3UCxNQUE5QjtBQUNBLFFBQUk4USxlQUFlLEdBQUcvaEIsS0FBSyxDQUFDNkcsTUFBTixDQUFhLFVBQVVsSixDQUFWLEVBQWE7QUFDOUMsYUFBTyxDQUFDLE9BQUQsRUFBVSxTQUFWLEVBQXFCLFNBQXJCLEVBQWdDLGNBQWhDLEVBQWdEL0ssT0FBaEQsQ0FBd0QrSyxDQUF4RCxLQUE4RCxDQUFyRTtBQUNELEtBRnFCLENBQXRCOztBQUlBLFFBQUlva0IsZUFBZSxDQUFDbHlCLE1BQWhCLEtBQTJCLENBQS9CLEVBQWtDO0FBQ2hDLFVBQUl3eEIsU0FBUyxHQUFHUCxLQUFoQixFQUF1QjtBQUNyQixZQUFJa0IsYUFBSjs7QUFFQVgsUUFBQUEsU0FBUyxHQUFHcFEsTUFBTSxDQUFDcUksSUFBUCxFQUFhMEksYUFBYSxHQUFHLEVBQWhCLEVBQW9CQSxhQUFhLENBQUNaLFdBQUQsQ0FBYixHQUE2QixDQUFqRCxFQUFvRFksYUFBakUsRUFBWjtBQUNEOztBQUVELFVBQUlYLFNBQVMsS0FBS3BRLE1BQWxCLEVBQTBCO0FBQ3hCakIsUUFBQUEsT0FBTyxDQUFDb1IsV0FBRCxDQUFQLEdBQXVCLENBQUNwUixPQUFPLENBQUNvUixXQUFELENBQVAsSUFBd0IsQ0FBekIsSUFBOEJVLGVBQWUsSUFBSVQsU0FBUyxHQUFHcFEsTUFBaEIsQ0FBcEU7QUFDRDtBQUNGOztBQUVELFFBQUlzSSxRQUFRLEdBQUdsQyxRQUFRLENBQUNqSixVQUFULENBQW9CbGUsTUFBTSxDQUFDNEwsTUFBUCxDQUFja1UsT0FBZCxFQUF1QjdNLElBQXZCLENBQXBCLENBQWY7O0FBRUEsUUFBSTRlLGVBQWUsQ0FBQ2x5QixNQUFoQixHQUF5QixDQUE3QixFQUFnQztBQUM5QixVQUFJb3lCLG9CQUFKOztBQUVBLGFBQU8sQ0FBQ0Esb0JBQW9CLEdBQUc1SyxRQUFRLENBQUNoSyxVQUFULENBQW9CeVUsZUFBcEIsRUFBcUMzZSxJQUFyQyxDQUF4QixFQUFvRXdELE9BQXBFLENBQTRFdFUsS0FBNUUsQ0FBa0Y0dkIsb0JBQWxGLEVBQXdHRixlQUF4RyxFQUF5SHpJLElBQXpILENBQThIQyxRQUE5SCxDQUFQO0FBQ0QsS0FKRCxNQUlPO0FBQ0wsYUFBT0EsUUFBUDtBQUNEO0FBQ0Y7O0FBRUQsTUFBSTJJLGdCQUFnQixHQUFHO0FBQ3JCQyxJQUFBQSxJQUFJLEVBQUUsaUJBRGU7QUFFckJDLElBQUFBLE9BQU8sRUFBRSxpQkFGWTtBQUdyQkMsSUFBQUEsSUFBSSxFQUFFLGlCQUhlO0FBSXJCQyxJQUFBQSxJQUFJLEVBQUUsaUJBSmU7QUFLckJDLElBQUFBLElBQUksRUFBRSxpQkFMZTtBQU1yQkMsSUFBQUEsUUFBUSxFQUFFLGlCQU5XO0FBT3JCQyxJQUFBQSxJQUFJLEVBQUUsaUJBUGU7QUFRckJDLElBQUFBLE9BQU8sRUFBRSx1QkFSWTtBQVNyQkMsSUFBQUEsSUFBSSxFQUFFLGlCQVRlO0FBVXJCQyxJQUFBQSxJQUFJLEVBQUUsaUJBVmU7QUFXckJDLElBQUFBLElBQUksRUFBRSxpQkFYZTtBQVlyQkMsSUFBQUEsSUFBSSxFQUFFLGlCQVplO0FBYXJCQyxJQUFBQSxJQUFJLEVBQUUsaUJBYmU7QUFjckJDLElBQUFBLElBQUksRUFBRSxpQkFkZTtBQWVyQkMsSUFBQUEsSUFBSSxFQUFFLGlCQWZlO0FBZ0JyQkMsSUFBQUEsSUFBSSxFQUFFLGlCQWhCZTtBQWlCckJDLElBQUFBLE9BQU8sRUFBRSxpQkFqQlk7QUFrQnJCQyxJQUFBQSxJQUFJLEVBQUUsaUJBbEJlO0FBbUJyQkMsSUFBQUEsSUFBSSxFQUFFLGlCQW5CZTtBQW9CckJDLElBQUFBLElBQUksRUFBRSxpQkFwQmU7QUFxQnJCQyxJQUFBQSxJQUFJLEVBQUU7QUFyQmUsR0FBdkI7QUF1QkEsTUFBSUMscUJBQXFCLEdBQUc7QUFDMUJyQixJQUFBQSxJQUFJLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQURvQjtBQUUxQkMsSUFBQUEsT0FBTyxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FGaUI7QUFHMUJDLElBQUFBLElBQUksRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBSG9CO0FBSTFCQyxJQUFBQSxJQUFJLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQUpvQjtBQUsxQkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FMb0I7QUFNMUJDLElBQUFBLFFBQVEsRUFBRSxDQUFDLEtBQUQsRUFBUSxLQUFSLENBTmdCO0FBTzFCQyxJQUFBQSxJQUFJLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQVBvQjtBQVExQkUsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FSb0I7QUFTMUJDLElBQUFBLElBQUksRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBVG9CO0FBVTFCQyxJQUFBQSxJQUFJLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQVZvQjtBQVcxQkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FYb0I7QUFZMUJDLElBQUFBLElBQUksRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBWm9CO0FBYTFCQyxJQUFBQSxJQUFJLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQWJvQjtBQWMxQkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0Fkb0I7QUFlMUJDLElBQUFBLElBQUksRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBZm9CO0FBZ0IxQkMsSUFBQUEsT0FBTyxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FoQmlCO0FBaUIxQkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FqQm9CO0FBa0IxQkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FsQm9CO0FBbUIxQkMsSUFBQUEsSUFBSSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVA7QUFuQm9CLEdBQTVCLENBMzhKOEIsQ0ErOUozQjs7QUFFSCxNQUFJRyxZQUFZLEdBQUd2QixnQkFBZ0IsQ0FBQ1EsT0FBakIsQ0FBeUIvbEIsT0FBekIsQ0FBaUMsVUFBakMsRUFBNkMsRUFBN0MsRUFBaURtZixLQUFqRCxDQUF1RCxFQUF2RCxDQUFuQjs7QUFDQSxXQUFTNEgsV0FBVCxDQUFxQkMsR0FBckIsRUFBMEI7QUFDeEIsUUFBSXJ3QixLQUFLLEdBQUdpRyxRQUFRLENBQUNvcUIsR0FBRCxFQUFNLEVBQU4sQ0FBcEI7O0FBRUEsUUFBSTFtQixLQUFLLENBQUMzSixLQUFELENBQVQsRUFBa0I7QUFDaEJBLE1BQUFBLEtBQUssR0FBRyxFQUFSOztBQUVBLFdBQUssSUFBSTFELENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcrekIsR0FBRyxDQUFDOXpCLE1BQXhCLEVBQWdDRCxDQUFDLEVBQWpDLEVBQXFDO0FBQ25DLFlBQUlnMEIsSUFBSSxHQUFHRCxHQUFHLENBQUNFLFVBQUosQ0FBZWowQixDQUFmLENBQVg7O0FBRUEsWUFBSSt6QixHQUFHLENBQUMvekIsQ0FBRCxDQUFILENBQU9rMEIsTUFBUCxDQUFjNUIsZ0JBQWdCLENBQUNRLE9BQS9CLE1BQTRDLENBQUMsQ0FBakQsRUFBb0Q7QUFDbERwdkIsVUFBQUEsS0FBSyxJQUFJbXdCLFlBQVksQ0FBQzd3QixPQUFiLENBQXFCK3dCLEdBQUcsQ0FBQy96QixDQUFELENBQXhCLENBQVQ7QUFDRCxTQUZELE1BRU87QUFDTCxlQUFLLElBQUlRLEdBQVQsSUFBZ0JvekIscUJBQWhCLEVBQXVDO0FBQ3JDLGdCQUFJTyxvQkFBb0IsR0FBR1AscUJBQXFCLENBQUNwekIsR0FBRCxDQUFoRDtBQUFBLGdCQUNJNHpCLEdBQUcsR0FBR0Qsb0JBQW9CLENBQUMsQ0FBRCxDQUQ5QjtBQUFBLGdCQUVJRSxHQUFHLEdBQUdGLG9CQUFvQixDQUFDLENBQUQsQ0FGOUI7O0FBSUEsZ0JBQUlILElBQUksSUFBSUksR0FBUixJQUFlSixJQUFJLElBQUlLLEdBQTNCLEVBQWdDO0FBQzlCM3dCLGNBQUFBLEtBQUssSUFBSXN3QixJQUFJLEdBQUdJLEdBQWhCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7O0FBRUQsYUFBT3pxQixRQUFRLENBQUNqRyxLQUFELEVBQVEsRUFBUixDQUFmO0FBQ0QsS0F0QkQsTUFzQk87QUFDTCxhQUFPQSxLQUFQO0FBQ0Q7QUFDRjs7QUFDRCxXQUFTNHdCLFVBQVQsQ0FBb0J6aUIsSUFBcEIsRUFBMEIwaUIsTUFBMUIsRUFBa0M7QUFDaEMsUUFBSXJaLGVBQWUsR0FBR3JKLElBQUksQ0FBQ3FKLGVBQTNCOztBQUVBLFFBQUlxWixNQUFNLEtBQUssS0FBSyxDQUFwQixFQUF1QjtBQUNyQkEsTUFBQUEsTUFBTSxHQUFHLEVBQVQ7QUFDRDs7QUFFRCxXQUFPLElBQUkxYyxNQUFKLENBQVcsS0FBS3lhLGdCQUFnQixDQUFDcFgsZUFBZSxJQUFJLE1BQXBCLENBQXJCLEdBQW1EcVosTUFBOUQsQ0FBUDtBQUNEOztBQUVELE1BQUlDLFdBQVcsR0FBRyxtREFBbEI7O0FBRUEsV0FBU0MsT0FBVCxDQUFpQjVTLEtBQWpCLEVBQXdCNlMsSUFBeEIsRUFBOEI7QUFDNUIsUUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLE1BQUFBLElBQUksR0FBRyxTQUFTQSxJQUFULENBQWMxMEIsQ0FBZCxFQUFpQjtBQUN0QixlQUFPQSxDQUFQO0FBQ0QsT0FGRDtBQUdEOztBQUVELFdBQU87QUFDTDZoQixNQUFBQSxLQUFLLEVBQUVBLEtBREY7QUFFTDhTLE1BQUFBLEtBQUssRUFBRSxTQUFTQSxLQUFULENBQWU5aUIsSUFBZixFQUFxQjtBQUMxQixZQUFJN00sQ0FBQyxHQUFHNk0sSUFBSSxDQUFDLENBQUQsQ0FBWjtBQUNBLGVBQU82aUIsSUFBSSxDQUFDWixXQUFXLENBQUM5dUIsQ0FBRCxDQUFaLENBQVg7QUFDRDtBQUxJLEtBQVA7QUFPRDs7QUFFRCxXQUFTNHZCLFlBQVQsQ0FBc0I1dkIsQ0FBdEIsRUFBeUI7QUFDdkI7QUFDQSxXQUFPQSxDQUFDLENBQUMrSCxPQUFGLENBQVUsSUFBVixFQUFnQixNQUFoQixDQUFQO0FBQ0Q7O0FBRUQsV0FBUzhuQixvQkFBVCxDQUE4Qjd2QixDQUE5QixFQUFpQztBQUMvQixXQUFPQSxDQUFDLENBQUMrSCxPQUFGLENBQVUsSUFBVixFQUFnQixFQUFoQixFQUFvQlAsV0FBcEIsRUFBUDtBQUNEOztBQUVELFdBQVNzb0IsS0FBVCxDQUFlQyxPQUFmLEVBQXdCQyxVQUF4QixFQUFvQztBQUNsQyxRQUFJRCxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDcEIsYUFBTyxJQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBTztBQUNMbFQsUUFBQUEsS0FBSyxFQUFFaEssTUFBTSxDQUFDa2QsT0FBTyxDQUFDL2QsR0FBUixDQUFZNGQsWUFBWixFQUEwQkssSUFBMUIsQ0FBK0IsR0FBL0IsQ0FBRCxDQURSO0FBRUxOLFFBQUFBLEtBQUssRUFBRSxTQUFTQSxLQUFULENBQWUvZCxLQUFmLEVBQXNCO0FBQzNCLGNBQUk1UixDQUFDLEdBQUc0UixLQUFLLENBQUMsQ0FBRCxDQUFiO0FBQ0EsaUJBQU9tZSxPQUFPLENBQUNHLFNBQVIsQ0FBa0IsVUFBVWwxQixDQUFWLEVBQWE7QUFDcEMsbUJBQU82MEIsb0JBQW9CLENBQUM3dkIsQ0FBRCxDQUFwQixLQUE0QjZ2QixvQkFBb0IsQ0FBQzcwQixDQUFELENBQXZEO0FBQ0QsV0FGTSxJQUVGZzFCLFVBRkw7QUFHRDtBQVBJLE9BQVA7QUFTRDtBQUNGOztBQUVELFdBQVM5bUIsTUFBVCxDQUFnQjJULEtBQWhCLEVBQXVCc1QsTUFBdkIsRUFBK0I7QUFDN0IsV0FBTztBQUNMdFQsTUFBQUEsS0FBSyxFQUFFQSxLQURGO0FBRUw4UyxNQUFBQSxLQUFLLEVBQUUsU0FBU0EsS0FBVCxDQUFlckcsS0FBZixFQUFzQjtBQUMzQixZQUFJOEcsQ0FBQyxHQUFHOUcsS0FBSyxDQUFDLENBQUQsQ0FBYjtBQUFBLFlBQ0loaUIsQ0FBQyxHQUFHZ2lCLEtBQUssQ0FBQyxDQUFELENBRGI7QUFFQSxlQUFPdGhCLFlBQVksQ0FBQ29vQixDQUFELEVBQUk5b0IsQ0FBSixDQUFuQjtBQUNELE9BTkk7QUFPTDZvQixNQUFBQSxNQUFNLEVBQUVBO0FBUEgsS0FBUDtBQVNEOztBQUVELFdBQVNFLE1BQVQsQ0FBZ0J4VCxLQUFoQixFQUF1QjtBQUNyQixXQUFPO0FBQ0xBLE1BQUFBLEtBQUssRUFBRUEsS0FERjtBQUVMOFMsTUFBQUEsS0FBSyxFQUFFLFNBQVNBLEtBQVQsQ0FBZS9GLEtBQWYsRUFBc0I7QUFDM0IsWUFBSTVwQixDQUFDLEdBQUc0cEIsS0FBSyxDQUFDLENBQUQsQ0FBYjtBQUNBLGVBQU81cEIsQ0FBUDtBQUNEO0FBTEksS0FBUDtBQU9EOztBQUVELFdBQVNzd0IsV0FBVCxDQUFxQjV4QixLQUFyQixFQUE0QjtBQUMxQjtBQUNBLFdBQU9BLEtBQUssQ0FBQ3FKLE9BQU4sQ0FBYyw2QkFBZCxFQUE2QyxNQUE3QyxDQUFQO0FBQ0Q7O0FBRUQsV0FBU3dvQixZQUFULENBQXNCeGpCLEtBQXRCLEVBQTZCa0MsR0FBN0IsRUFBa0M7QUFDaEMsUUFBSXVoQixHQUFHLEdBQUdsQixVQUFVLENBQUNyZ0IsR0FBRCxDQUFwQjtBQUFBLFFBQ0l3aEIsR0FBRyxHQUFHbkIsVUFBVSxDQUFDcmdCLEdBQUQsRUFBTSxLQUFOLENBRHBCO0FBQUEsUUFFSXloQixLQUFLLEdBQUdwQixVQUFVLENBQUNyZ0IsR0FBRCxFQUFNLEtBQU4sQ0FGdEI7QUFBQSxRQUdJMGhCLElBQUksR0FBR3JCLFVBQVUsQ0FBQ3JnQixHQUFELEVBQU0sS0FBTixDQUhyQjtBQUFBLFFBSUkyaEIsR0FBRyxHQUFHdEIsVUFBVSxDQUFDcmdCLEdBQUQsRUFBTSxLQUFOLENBSnBCO0FBQUEsUUFLSTRoQixRQUFRLEdBQUd2QixVQUFVLENBQUNyZ0IsR0FBRCxFQUFNLE9BQU4sQ0FMekI7QUFBQSxRQU1JNmhCLFVBQVUsR0FBR3hCLFVBQVUsQ0FBQ3JnQixHQUFELEVBQU0sT0FBTixDQU4zQjtBQUFBLFFBT0k4aEIsUUFBUSxHQUFHekIsVUFBVSxDQUFDcmdCLEdBQUQsRUFBTSxPQUFOLENBUHpCO0FBQUEsUUFRSStoQixTQUFTLEdBQUcxQixVQUFVLENBQUNyZ0IsR0FBRCxFQUFNLE9BQU4sQ0FSMUI7QUFBQSxRQVNJZ2lCLFNBQVMsR0FBRzNCLFVBQVUsQ0FBQ3JnQixHQUFELEVBQU0sT0FBTixDQVQxQjtBQUFBLFFBVUlpaUIsU0FBUyxHQUFHNUIsVUFBVSxDQUFDcmdCLEdBQUQsRUFBTSxPQUFOLENBVjFCO0FBQUEsUUFXSWpDLE9BQU8sR0FBRyxTQUFTQSxPQUFULENBQWlCTyxDQUFqQixFQUFvQjtBQUNoQyxhQUFPO0FBQ0xzUCxRQUFBQSxLQUFLLEVBQUVoSyxNQUFNLENBQUN5ZCxXQUFXLENBQUMvaUIsQ0FBQyxDQUFDTixHQUFILENBQVosQ0FEUjtBQUVMMGlCLFFBQUFBLEtBQUssRUFBRSxTQUFTQSxLQUFULENBQWVwRSxLQUFmLEVBQXNCO0FBQzNCLGNBQUl2ckIsQ0FBQyxHQUFHdXJCLEtBQUssQ0FBQyxDQUFELENBQWI7QUFDQSxpQkFBT3ZyQixDQUFQO0FBQ0QsU0FMSTtBQU1MZ04sUUFBQUEsT0FBTyxFQUFFO0FBTkosT0FBUDtBQVFELEtBcEJEO0FBQUEsUUFxQklta0IsT0FBTyxHQUFHLFNBQVNBLE9BQVQsQ0FBaUI1akIsQ0FBakIsRUFBb0I7QUFDaEMsVUFBSVIsS0FBSyxDQUFDQyxPQUFWLEVBQW1CO0FBQ2pCLGVBQU9BLE9BQU8sQ0FBQ08sQ0FBRCxDQUFkO0FBQ0Q7O0FBRUQsY0FBUUEsQ0FBQyxDQUFDTixHQUFWO0FBQ0U7QUFDQSxhQUFLLEdBQUw7QUFDRSxpQkFBTzZpQixLQUFLLENBQUM3Z0IsR0FBRyxDQUFDdkUsSUFBSixDQUFTLE9BQVQsRUFBa0IsS0FBbEIsQ0FBRCxFQUEyQixDQUEzQixDQUFaOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPb2xCLEtBQUssQ0FBQzdnQixHQUFHLENBQUN2RSxJQUFKLENBQVMsTUFBVCxFQUFpQixLQUFqQixDQUFELEVBQTBCLENBQTFCLENBQVo7QUFDRjs7QUFFQSxhQUFLLEdBQUw7QUFDRSxpQkFBTytrQixPQUFPLENBQUNzQixRQUFELENBQWQ7O0FBRUYsYUFBSyxJQUFMO0FBQ0UsaUJBQU90QixPQUFPLENBQUN3QixTQUFELEVBQVl4cUIsY0FBWixDQUFkOztBQUVGLGFBQUssTUFBTDtBQUNFLGlCQUFPZ3BCLE9BQU8sQ0FBQ2tCLElBQUQsQ0FBZDs7QUFFRixhQUFLLE9BQUw7QUFDRSxpQkFBT2xCLE9BQU8sQ0FBQ3lCLFNBQUQsQ0FBZDs7QUFFRixhQUFLLFFBQUw7QUFDRSxpQkFBT3pCLE9BQU8sQ0FBQ21CLEdBQUQsQ0FBZDtBQUNGOztBQUVBLGFBQUssR0FBTDtBQUNFLGlCQUFPbkIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssS0FBTDtBQUNFLGlCQUFPWCxLQUFLLENBQUM3Z0IsR0FBRyxDQUFDaEYsTUFBSixDQUFXLE9BQVgsRUFBb0IsSUFBcEIsRUFBMEIsS0FBMUIsQ0FBRCxFQUFtQyxDQUFuQyxDQUFaOztBQUVGLGFBQUssTUFBTDtBQUNFLGlCQUFPNmxCLEtBQUssQ0FBQzdnQixHQUFHLENBQUNoRixNQUFKLENBQVcsTUFBWCxFQUFtQixJQUFuQixFQUF5QixLQUF6QixDQUFELEVBQWtDLENBQWxDLENBQVo7O0FBRUYsYUFBSyxHQUFMO0FBQ0UsaUJBQU93bEIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssS0FBTDtBQUNFLGlCQUFPWCxLQUFLLENBQUM3Z0IsR0FBRyxDQUFDaEYsTUFBSixDQUFXLE9BQVgsRUFBb0IsS0FBcEIsRUFBMkIsS0FBM0IsQ0FBRCxFQUFvQyxDQUFwQyxDQUFaOztBQUVGLGFBQUssTUFBTDtBQUNFLGlCQUFPNmxCLEtBQUssQ0FBQzdnQixHQUFHLENBQUNoRixNQUFKLENBQVcsTUFBWCxFQUFtQixLQUFuQixFQUEwQixLQUExQixDQUFELEVBQW1DLENBQW5DLENBQVo7QUFDRjs7QUFFQSxhQUFLLEdBQUw7QUFDRSxpQkFBT3dsQixPQUFPLENBQUNvQixRQUFELENBQWQ7O0FBRUYsYUFBSyxJQUFMO0FBQ0UsaUJBQU9wQixPQUFPLENBQUNnQixHQUFELENBQWQ7QUFDRjs7QUFFQSxhQUFLLEdBQUw7QUFDRSxpQkFBT2hCLE9BQU8sQ0FBQ3FCLFVBQUQsQ0FBZDs7QUFFRixhQUFLLEtBQUw7QUFDRSxpQkFBT3JCLE9BQU8sQ0FBQ2lCLEtBQUQsQ0FBZDtBQUNGOztBQUVBLGFBQUssSUFBTDtBQUNFLGlCQUFPakIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPaEIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPaEIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPaEIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPaEIsT0FBTyxDQUFDb0IsUUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPcEIsT0FBTyxDQUFDZ0IsR0FBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPaEIsT0FBTyxDQUFDcUIsVUFBRCxDQUFkOztBQUVGLGFBQUssS0FBTDtBQUNFLGlCQUFPckIsT0FBTyxDQUFDaUIsS0FBRCxDQUFkOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPTCxNQUFNLENBQUNXLFNBQUQsQ0FBYjtBQUNGOztBQUVBLGFBQUssR0FBTDtBQUNFLGlCQUFPbEIsS0FBSyxDQUFDN2dCLEdBQUcsQ0FBQzNFLFNBQUosRUFBRCxFQUFrQixDQUFsQixDQUFaO0FBQ0Y7O0FBRUEsYUFBSyxNQUFMO0FBQ0UsaUJBQU9tbEIsT0FBTyxDQUFDa0IsSUFBRCxDQUFkOztBQUVGLGFBQUssSUFBTDtBQUNFLGlCQUFPbEIsT0FBTyxDQUFDd0IsU0FBRCxFQUFZeHFCLGNBQVosQ0FBZDtBQUNGOztBQUVBLGFBQUssR0FBTDtBQUNFLGlCQUFPZ3BCLE9BQU8sQ0FBQ29CLFFBQUQsQ0FBZDs7QUFFRixhQUFLLElBQUw7QUFDRSxpQkFBT3BCLE9BQU8sQ0FBQ2dCLEdBQUQsQ0FBZDtBQUNGOztBQUVBLGFBQUssR0FBTDtBQUNBLGFBQUssR0FBTDtBQUNFLGlCQUFPaEIsT0FBTyxDQUFDZSxHQUFELENBQWQ7O0FBRUYsYUFBSyxLQUFMO0FBQ0UsaUJBQU9WLEtBQUssQ0FBQzdnQixHQUFHLENBQUM1RSxRQUFKLENBQWEsT0FBYixFQUFzQixLQUF0QixFQUE2QixLQUE3QixDQUFELEVBQXNDLENBQXRDLENBQVo7O0FBRUYsYUFBSyxNQUFMO0FBQ0UsaUJBQU95bEIsS0FBSyxDQUFDN2dCLEdBQUcsQ0FBQzVFLFFBQUosQ0FBYSxNQUFiLEVBQXFCLEtBQXJCLEVBQTRCLEtBQTVCLENBQUQsRUFBcUMsQ0FBckMsQ0FBWjs7QUFFRixhQUFLLEtBQUw7QUFDRSxpQkFBT3lsQixLQUFLLENBQUM3Z0IsR0FBRyxDQUFDNUUsUUFBSixDQUFhLE9BQWIsRUFBc0IsSUFBdEIsRUFBNEIsS0FBNUIsQ0FBRCxFQUFxQyxDQUFyQyxDQUFaOztBQUVGLGFBQUssTUFBTDtBQUNFLGlCQUFPeWxCLEtBQUssQ0FBQzdnQixHQUFHLENBQUM1RSxRQUFKLENBQWEsTUFBYixFQUFxQixJQUFyQixFQUEyQixLQUEzQixDQUFELEVBQW9DLENBQXBDLENBQVo7QUFDRjs7QUFFQSxhQUFLLEdBQUw7QUFDQSxhQUFLLElBQUw7QUFDRSxpQkFBT25CLE1BQU0sQ0FBQyxJQUFJMkosTUFBSixDQUFXLFVBQVVnZSxRQUFRLENBQUMvZCxNQUFuQixHQUE0QixRQUE1QixHQUF1QzJkLEdBQUcsQ0FBQzNkLE1BQTNDLEdBQW9ELEtBQS9ELENBQUQsRUFBd0UsQ0FBeEUsQ0FBYjs7QUFFRixhQUFLLEtBQUw7QUFDRSxpQkFBTzVKLE1BQU0sQ0FBQyxJQUFJMkosTUFBSixDQUFXLFVBQVVnZSxRQUFRLENBQUMvZCxNQUFuQixHQUE0QixJQUE1QixHQUFtQzJkLEdBQUcsQ0FBQzNkLE1BQXZDLEdBQWdELElBQTNELENBQUQsRUFBbUUsQ0FBbkUsQ0FBYjtBQUNGO0FBQ0E7O0FBRUEsYUFBSyxHQUFMO0FBQ0UsaUJBQU91ZCxNQUFNLENBQUMsb0JBQUQsQ0FBYjs7QUFFRjtBQUNFLGlCQUFPcmpCLE9BQU8sQ0FBQ08sQ0FBRCxDQUFkO0FBeEpKO0FBMEpELEtBcExEOztBQXNMQSxRQUFJN04sSUFBSSxHQUFHeXhCLE9BQU8sQ0FBQ3BrQixLQUFELENBQVAsSUFBa0I7QUFDM0JnZCxNQUFBQSxhQUFhLEVBQUV5RjtBQURZLEtBQTdCO0FBR0E5dkIsSUFBQUEsSUFBSSxDQUFDcU4sS0FBTCxHQUFhQSxLQUFiO0FBQ0EsV0FBT3JOLElBQVA7QUFDRDs7QUFFRCxNQUFJMHhCLHVCQUF1QixHQUFHO0FBQzVCanhCLElBQUFBLElBQUksRUFBRTtBQUNKLGlCQUFXLElBRFA7QUFFSitLLE1BQUFBLE9BQU8sRUFBRTtBQUZMLEtBRHNCO0FBSzVCOUssSUFBQUEsS0FBSyxFQUFFO0FBQ0w4SyxNQUFBQSxPQUFPLEVBQUUsR0FESjtBQUVMLGlCQUFXLElBRk47QUFHTCxlQUFPLEtBSEY7QUFJTCxjQUFNO0FBSkQsS0FMcUI7QUFXNUI3SyxJQUFBQSxHQUFHLEVBQUU7QUFDSDZLLE1BQUFBLE9BQU8sRUFBRSxHQUROO0FBRUgsaUJBQVc7QUFGUixLQVh1QjtBQWU1QnpLLElBQUFBLE9BQU8sRUFBRTtBQUNQLGVBQU8sS0FEQTtBQUVQLGNBQU07QUFGQyxLQWZtQjtBQW1CNUI0d0IsSUFBQUEsU0FBUyxFQUFFLEdBbkJpQjtBQW9CNUJDLElBQUFBLFNBQVMsRUFBRSxHQXBCaUI7QUFxQjVCM3dCLElBQUFBLElBQUksRUFBRTtBQUNKdUssTUFBQUEsT0FBTyxFQUFFLEdBREw7QUFFSixpQkFBVztBQUZQLEtBckJzQjtBQXlCNUJ0SyxJQUFBQSxNQUFNLEVBQUU7QUFDTnNLLE1BQUFBLE9BQU8sRUFBRSxHQURIO0FBRU4saUJBQVc7QUFGTCxLQXpCb0I7QUE2QjVCcEssSUFBQUEsTUFBTSxFQUFFO0FBQ05vSyxNQUFBQSxPQUFPLEVBQUUsR0FESDtBQUVOLGlCQUFXO0FBRkw7QUE3Qm9CLEdBQTlCOztBQW1DQSxXQUFTcW1CLFlBQVQsQ0FBc0JDLElBQXRCLEVBQTRCM3FCLE1BQTVCLEVBQW9DbUksVUFBcEMsRUFBZ0Q7QUFDOUMsUUFBSXpILElBQUksR0FBR2lxQixJQUFJLENBQUNqcUIsSUFBaEI7QUFBQSxRQUNJN0ksS0FBSyxHQUFHOHlCLElBQUksQ0FBQzl5QixLQURqQjs7QUFHQSxRQUFJNkksSUFBSSxLQUFLLFNBQWIsRUFBd0I7QUFDdEIsYUFBTztBQUNMeUYsUUFBQUEsT0FBTyxFQUFFLElBREo7QUFFTEMsUUFBQUEsR0FBRyxFQUFFdk87QUFGQSxPQUFQO0FBSUQ7O0FBRUQsUUFBSXFhLEtBQUssR0FBRy9KLFVBQVUsQ0FBQ3pILElBQUQsQ0FBdEI7QUFDQSxRQUFJMEYsR0FBRyxHQUFHbWtCLHVCQUF1QixDQUFDN3BCLElBQUQsQ0FBakM7O0FBRUEsUUFBSSxRQUFPMEYsR0FBUCxNQUFlLFFBQW5CLEVBQTZCO0FBQzNCQSxNQUFBQSxHQUFHLEdBQUdBLEdBQUcsQ0FBQzhMLEtBQUQsQ0FBVDtBQUNEOztBQUVELFFBQUk5TCxHQUFKLEVBQVM7QUFDUCxhQUFPO0FBQ0xELFFBQUFBLE9BQU8sRUFBRSxLQURKO0FBRUxDLFFBQUFBLEdBQUcsRUFBRUE7QUFGQSxPQUFQO0FBSUQ7O0FBRUQsV0FBTzdPLFNBQVA7QUFDRDs7QUFFRCxXQUFTcXpCLFVBQVQsQ0FBb0JybUIsS0FBcEIsRUFBMkI7QUFDekIsUUFBSXNtQixFQUFFLEdBQUd0bUIsS0FBSyxDQUFDNEcsR0FBTixDQUFVLFVBQVVqSixDQUFWLEVBQWE7QUFDOUIsYUFBT0EsQ0FBQyxDQUFDOFQsS0FBVDtBQUNELEtBRlEsRUFFTnpaLE1BRk0sQ0FFQyxVQUFVMEIsQ0FBVixFQUFhb1EsQ0FBYixFQUFnQjtBQUN4QixhQUFPcFEsQ0FBQyxHQUFHLEdBQUosR0FBVW9RLENBQUMsQ0FBQ3BDLE1BQVosR0FBcUIsR0FBNUI7QUFDRCxLQUpRLEVBSU4sRUFKTSxDQUFUO0FBS0EsV0FBTyxDQUFDLE1BQU00ZSxFQUFOLEdBQVcsR0FBWixFQUFpQnRtQixLQUFqQixDQUFQO0FBQ0Q7O0FBRUQsV0FBU2lKLEtBQVQsQ0FBZS9QLEtBQWYsRUFBc0J1WSxLQUF0QixFQUE2QjhVLFFBQTdCLEVBQXVDO0FBQ3JDLFFBQUlDLE9BQU8sR0FBR3R0QixLQUFLLENBQUMrUCxLQUFOLENBQVl3SSxLQUFaLENBQWQ7O0FBRUEsUUFBSStVLE9BQUosRUFBYTtBQUNYLFVBQUlDLEdBQUcsR0FBRyxFQUFWO0FBQ0EsVUFBSUMsVUFBVSxHQUFHLENBQWpCOztBQUVBLFdBQUssSUFBSTkyQixDQUFULElBQWMyMkIsUUFBZCxFQUF3QjtBQUN0QixZQUFJL3RCLGNBQWMsQ0FBQyt0QixRQUFELEVBQVczMkIsQ0FBWCxDQUFsQixFQUFpQztBQUMvQixjQUFJbzFCLENBQUMsR0FBR3VCLFFBQVEsQ0FBQzMyQixDQUFELENBQWhCO0FBQUEsY0FDSW0xQixNQUFNLEdBQUdDLENBQUMsQ0FBQ0QsTUFBRixHQUFXQyxDQUFDLENBQUNELE1BQUYsR0FBVyxDQUF0QixHQUEwQixDQUR2Qzs7QUFHQSxjQUFJLENBQUNDLENBQUMsQ0FBQ3BqQixPQUFILElBQWNvakIsQ0FBQyxDQUFDcmpCLEtBQXBCLEVBQTJCO0FBQ3pCOGtCLFlBQUFBLEdBQUcsQ0FBQ3pCLENBQUMsQ0FBQ3JqQixLQUFGLENBQVFFLEdBQVIsQ0FBWSxDQUFaLENBQUQsQ0FBSCxHQUFzQm1qQixDQUFDLENBQUNULEtBQUYsQ0FBUWlDLE9BQU8sQ0FBQ3B0QixLQUFSLENBQWNzdEIsVUFBZCxFQUEwQkEsVUFBVSxHQUFHM0IsTUFBdkMsQ0FBUixDQUF0QjtBQUNEOztBQUVEMkIsVUFBQUEsVUFBVSxJQUFJM0IsTUFBZDtBQUNEO0FBQ0Y7O0FBRUQsYUFBTyxDQUFDeUIsT0FBRCxFQUFVQyxHQUFWLENBQVA7QUFDRCxLQWxCRCxNQWtCTztBQUNMLGFBQU8sQ0FBQ0QsT0FBRCxFQUFVLEVBQVYsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsV0FBU0csbUJBQVQsQ0FBNkJILE9BQTdCLEVBQXNDO0FBQ3BDLFFBQUlJLE9BQU8sR0FBRyxTQUFTQSxPQUFULENBQWlCamxCLEtBQWpCLEVBQXdCO0FBQ3BDLGNBQVFBLEtBQVI7QUFDRSxhQUFLLEdBQUw7QUFDRSxpQkFBTyxhQUFQOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPLFFBQVA7O0FBRUYsYUFBSyxHQUFMO0FBQ0UsaUJBQU8sUUFBUDs7QUFFRixhQUFLLEdBQUw7QUFDQSxhQUFLLEdBQUw7QUFDRSxpQkFBTyxNQUFQOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPLEtBQVA7O0FBRUYsYUFBSyxHQUFMO0FBQ0UsaUJBQU8sU0FBUDs7QUFFRixhQUFLLEdBQUw7QUFDQSxhQUFLLEdBQUw7QUFDRSxpQkFBTyxPQUFQOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPLE1BQVA7O0FBRUYsYUFBSyxHQUFMO0FBQ0EsYUFBSyxHQUFMO0FBQ0UsaUJBQU8sU0FBUDs7QUFFRixhQUFLLEdBQUw7QUFDRSxpQkFBTyxZQUFQOztBQUVGLGFBQUssR0FBTDtBQUNFLGlCQUFPLFVBQVA7O0FBRUYsYUFBSyxHQUFMO0FBQ0UsaUJBQU8sU0FBUDs7QUFFRjtBQUNFLGlCQUFPLElBQVA7QUF6Q0o7QUEyQ0QsS0E1Q0Q7O0FBOENBLFFBQUkwRCxJQUFKOztBQUVBLFFBQUksQ0FBQ3pPLFdBQVcsQ0FBQzR2QixPQUFPLENBQUNLLENBQVQsQ0FBaEIsRUFBNkI7QUFDM0J4aEIsTUFBQUEsSUFBSSxHQUFHLElBQUlzRSxlQUFKLENBQW9CNmMsT0FBTyxDQUFDSyxDQUE1QixDQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUksQ0FBQ2p3QixXQUFXLENBQUM0dkIsT0FBTyxDQUFDM2IsQ0FBVCxDQUFoQixFQUE2QjtBQUNsQ3hGLE1BQUFBLElBQUksR0FBR3dELFFBQVEsQ0FBQ2hZLE1BQVQsQ0FBZ0IyMUIsT0FBTyxDQUFDM2IsQ0FBeEIsQ0FBUDtBQUNELEtBRk0sTUFFQTtBQUNMeEYsTUFBQUEsSUFBSSxHQUFHLElBQVA7QUFDRDs7QUFFRCxRQUFJLENBQUN6TyxXQUFXLENBQUM0dkIsT0FBTyxDQUFDTSxDQUFULENBQWhCLEVBQTZCO0FBQzNCTixNQUFBQSxPQUFPLENBQUNPLENBQVIsR0FBWSxDQUFDUCxPQUFPLENBQUNNLENBQVIsR0FBWSxDQUFiLElBQWtCLENBQWxCLEdBQXNCLENBQWxDO0FBQ0Q7O0FBRUQsUUFBSSxDQUFDbHdCLFdBQVcsQ0FBQzR2QixPQUFPLENBQUN4QixDQUFULENBQWhCLEVBQTZCO0FBQzNCLFVBQUl3QixPQUFPLENBQUN4QixDQUFSLEdBQVksRUFBWixJQUFrQndCLE9BQU8sQ0FBQ3IwQixDQUFSLEtBQWMsQ0FBcEMsRUFBdUM7QUFDckNxMEIsUUFBQUEsT0FBTyxDQUFDeEIsQ0FBUixJQUFhLEVBQWI7QUFDRCxPQUZELE1BRU8sSUFBSXdCLE9BQU8sQ0FBQ3hCLENBQVIsS0FBYyxFQUFkLElBQW9Cd0IsT0FBTyxDQUFDcjBCLENBQVIsS0FBYyxDQUF0QyxFQUF5QztBQUM5Q3EwQixRQUFBQSxPQUFPLENBQUN4QixDQUFSLEdBQVksQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSXdCLE9BQU8sQ0FBQ1EsQ0FBUixLQUFjLENBQWQsSUFBbUJSLE9BQU8sQ0FBQ1MsQ0FBL0IsRUFBa0M7QUFDaENULE1BQUFBLE9BQU8sQ0FBQ1MsQ0FBUixHQUFZLENBQUNULE9BQU8sQ0FBQ1MsQ0FBckI7QUFDRDs7QUFFRCxRQUFJLENBQUNyd0IsV0FBVyxDQUFDNHZCLE9BQU8sQ0FBQzdvQixDQUFULENBQWhCLEVBQTZCO0FBQzNCNm9CLE1BQUFBLE9BQU8sQ0FBQ1UsQ0FBUixHQUFZMXRCLFdBQVcsQ0FBQ2d0QixPQUFPLENBQUM3b0IsQ0FBVCxDQUF2QjtBQUNEOztBQUVELFFBQUl3YSxJQUFJLEdBQUdqb0IsTUFBTSxDQUFDb0ksSUFBUCxDQUFZa3VCLE9BQVosRUFBcUJ4dUIsTUFBckIsQ0FBNEIsVUFBVThSLENBQVYsRUFBYXZSLENBQWIsRUFBZ0I7QUFDckQsVUFBSW1CLENBQUMsR0FBR2t0QixPQUFPLENBQUNydUIsQ0FBRCxDQUFmOztBQUVBLFVBQUltQixDQUFKLEVBQU87QUFDTG9RLFFBQUFBLENBQUMsQ0FBQ3BRLENBQUQsQ0FBRCxHQUFPOHNCLE9BQU8sQ0FBQ2p1QixDQUFELENBQWQ7QUFDRDs7QUFFRCxhQUFPdVIsQ0FBUDtBQUNELEtBUlUsRUFRUixFQVJRLENBQVg7QUFTQSxXQUFPLENBQUNxTyxJQUFELEVBQU85UyxJQUFQLENBQVA7QUFDRDs7QUFFRCxNQUFJOGhCLGtCQUFrQixHQUFHLElBQXpCOztBQUVBLFdBQVNDLGdCQUFULEdBQTRCO0FBQzFCLFFBQUksQ0FBQ0Qsa0JBQUwsRUFBeUI7QUFDdkJBLE1BQUFBLGtCQUFrQixHQUFHOWEsUUFBUSxDQUFDZ0IsVUFBVCxDQUFvQixhQUFwQixDQUFyQjtBQUNEOztBQUVELFdBQU84WixrQkFBUDtBQUNEOztBQUVELFdBQVNFLHFCQUFULENBQStCMWxCLEtBQS9CLEVBQXNDbEcsTUFBdEMsRUFBOEM7QUFDNUMsUUFBSWtHLEtBQUssQ0FBQ0MsT0FBVixFQUFtQjtBQUNqQixhQUFPRCxLQUFQO0FBQ0Q7O0FBRUQsUUFBSWlDLFVBQVUsR0FBR1YsU0FBUyxDQUFDUyxzQkFBVixDQUFpQ2hDLEtBQUssQ0FBQ0UsR0FBdkMsQ0FBakI7O0FBRUEsUUFBSSxDQUFDK0IsVUFBTCxFQUFpQjtBQUNmLGFBQU9qQyxLQUFQO0FBQ0Q7O0FBRUQsUUFBSTJsQixTQUFTLEdBQUdwa0IsU0FBUyxDQUFDclMsTUFBVixDQUFpQjRLLE1BQWpCLEVBQXlCbUksVUFBekIsQ0FBaEI7QUFDQSxRQUFJMmpCLEtBQUssR0FBR0QsU0FBUyxDQUFDampCLG1CQUFWLENBQThCK2lCLGdCQUFnQixFQUE5QyxDQUFaO0FBQ0EsUUFBSS9nQixNQUFNLEdBQUdraEIsS0FBSyxDQUFDM2dCLEdBQU4sQ0FBVSxVQUFVdlYsQ0FBVixFQUFhO0FBQ2xDLGFBQU84MEIsWUFBWSxDQUFDOTBCLENBQUQsRUFBSW9LLE1BQUosRUFBWW1JLFVBQVosQ0FBbkI7QUFDRCxLQUZZLENBQWI7O0FBSUEsUUFBSXlDLE1BQU0sQ0FBQ21oQixRQUFQLENBQWdCeDBCLFNBQWhCLENBQUosRUFBZ0M7QUFDOUIsYUFBTzJPLEtBQVA7QUFDRDs7QUFFRCxXQUFPMEUsTUFBUDtBQUNEOztBQUVELFdBQVNvaEIsaUJBQVQsQ0FBMkJwaEIsTUFBM0IsRUFBbUM1SyxNQUFuQyxFQUEyQztBQUN6QyxRQUFJb2lCLGdCQUFKOztBQUVBLFdBQU8sQ0FBQ0EsZ0JBQWdCLEdBQUdubUIsS0FBSyxDQUFDakgsU0FBMUIsRUFBcUNnVyxNQUFyQyxDQUE0Q3BVLEtBQTVDLENBQWtEd3JCLGdCQUFsRCxFQUFvRXhYLE1BQU0sQ0FBQ08sR0FBUCxDQUFXLFVBQVV6RSxDQUFWLEVBQWE7QUFDakcsYUFBT2tsQixxQkFBcUIsQ0FBQ2xsQixDQUFELEVBQUkxRyxNQUFKLENBQTVCO0FBQ0QsS0FGMEUsQ0FBcEUsQ0FBUDtBQUdEO0FBQ0Q7Ozs7O0FBS0EsV0FBU2lzQixpQkFBVCxDQUEyQmpzQixNQUEzQixFQUFtQ3ZDLEtBQW5DLEVBQTBDb0QsTUFBMUMsRUFBa0Q7QUFDaEQsUUFBSStKLE1BQU0sR0FBR29oQixpQkFBaUIsQ0FBQ3ZrQixTQUFTLENBQUNFLFdBQVYsQ0FBc0I5RyxNQUF0QixDQUFELEVBQWdDYixNQUFoQyxDQUE5QjtBQUFBLFFBQ0l1RSxLQUFLLEdBQUdxRyxNQUFNLENBQUNPLEdBQVAsQ0FBVyxVQUFVekUsQ0FBVixFQUFhO0FBQ2xDLGFBQU9nakIsWUFBWSxDQUFDaGpCLENBQUQsRUFBSTFHLE1BQUosQ0FBbkI7QUFDRCxLQUZXLENBRFo7QUFBQSxRQUlJa3NCLGlCQUFpQixHQUFHM25CLEtBQUssQ0FBQy9ELElBQU4sQ0FBVyxVQUFVa0csQ0FBVixFQUFhO0FBQzlDLGFBQU9BLENBQUMsQ0FBQ3djLGFBQVQ7QUFDRCxLQUZ1QixDQUp4Qjs7QUFRQSxRQUFJZ0osaUJBQUosRUFBdUI7QUFDckIsYUFBTztBQUNMenVCLFFBQUFBLEtBQUssRUFBRUEsS0FERjtBQUVMbU4sUUFBQUEsTUFBTSxFQUFFQSxNQUZIO0FBR0xzWSxRQUFBQSxhQUFhLEVBQUVnSixpQkFBaUIsQ0FBQ2hKO0FBSDVCLE9BQVA7QUFLRCxLQU5ELE1BTU87QUFDTCxVQUFJaUosV0FBVyxHQUFHdkIsVUFBVSxDQUFDcm1CLEtBQUQsQ0FBNUI7QUFBQSxVQUNJNm5CLFdBQVcsR0FBR0QsV0FBVyxDQUFDLENBQUQsQ0FEN0I7QUFBQSxVQUVJckIsUUFBUSxHQUFHcUIsV0FBVyxDQUFDLENBQUQsQ0FGMUI7QUFBQSxVQUdJblcsS0FBSyxHQUFHaEssTUFBTSxDQUFDb2dCLFdBQUQsRUFBYyxHQUFkLENBSGxCO0FBQUEsVUFJSUMsTUFBTSxHQUFHN2UsS0FBSyxDQUFDL1AsS0FBRCxFQUFRdVksS0FBUixFQUFlOFUsUUFBZixDQUpsQjtBQUFBLFVBS0l3QixVQUFVLEdBQUdELE1BQU0sQ0FBQyxDQUFELENBTHZCO0FBQUEsVUFNSXRCLE9BQU8sR0FBR3NCLE1BQU0sQ0FBQyxDQUFELENBTnBCO0FBQUEsVUFPSXhILEtBQUssR0FBR2tHLE9BQU8sR0FBR0csbUJBQW1CLENBQUNILE9BQUQsQ0FBdEIsR0FBa0MsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQVByRDtBQUFBLFVBUUk3UixNQUFNLEdBQUcyTCxLQUFLLENBQUMsQ0FBRCxDQVJsQjtBQUFBLFVBU0lqYixJQUFJLEdBQUdpYixLQUFLLENBQUMsQ0FBRCxDQVRoQjs7QUFXQSxhQUFPO0FBQ0xwbkIsUUFBQUEsS0FBSyxFQUFFQSxLQURGO0FBRUxtTixRQUFBQSxNQUFNLEVBQUVBLE1BRkg7QUFHTG9MLFFBQUFBLEtBQUssRUFBRUEsS0FIRjtBQUlMc1csUUFBQUEsVUFBVSxFQUFFQSxVQUpQO0FBS0x2QixRQUFBQSxPQUFPLEVBQUVBLE9BTEo7QUFNTDdSLFFBQUFBLE1BQU0sRUFBRUEsTUFOSDtBQU9MdFAsUUFBQUEsSUFBSSxFQUFFQTtBQVBELE9BQVA7QUFTRDtBQUNGOztBQUNELFdBQVMyaUIsZUFBVCxDQUF5QnZzQixNQUF6QixFQUFpQ3ZDLEtBQWpDLEVBQXdDb0QsTUFBeEMsRUFBZ0Q7QUFDOUMsUUFBSTJyQixrQkFBa0IsR0FBR1AsaUJBQWlCLENBQUNqc0IsTUFBRCxFQUFTdkMsS0FBVCxFQUFnQm9ELE1BQWhCLENBQTFDO0FBQUEsUUFDSXFZLE1BQU0sR0FBR3NULGtCQUFrQixDQUFDdFQsTUFEaEM7QUFBQSxRQUVJdFAsSUFBSSxHQUFHNGlCLGtCQUFrQixDQUFDNWlCLElBRjlCO0FBQUEsUUFHSXNaLGFBQWEsR0FBR3NKLGtCQUFrQixDQUFDdEosYUFIdkM7O0FBS0EsV0FBTyxDQUFDaEssTUFBRCxFQUFTdFAsSUFBVCxFQUFlc1osYUFBZixDQUFQO0FBQ0Q7O0FBRUQsTUFBSXVKLGFBQWEsR0FBRyxDQUFDLENBQUQsRUFBSSxFQUFKLEVBQVEsRUFBUixFQUFZLEVBQVosRUFBZ0IsR0FBaEIsRUFBcUIsR0FBckIsRUFBMEIsR0FBMUIsRUFBK0IsR0FBL0IsRUFBb0MsR0FBcEMsRUFBeUMsR0FBekMsRUFBOEMsR0FBOUMsRUFBbUQsR0FBbkQsQ0FBcEI7QUFBQSxNQUNJQyxVQUFVLEdBQUcsQ0FBQyxDQUFELEVBQUksRUFBSixFQUFRLEVBQVIsRUFBWSxFQUFaLEVBQWdCLEdBQWhCLEVBQXFCLEdBQXJCLEVBQTBCLEdBQTFCLEVBQStCLEdBQS9CLEVBQW9DLEdBQXBDLEVBQXlDLEdBQXpDLEVBQThDLEdBQTlDLEVBQW1ELEdBQW5ELENBRGpCOztBQUdBLFdBQVNDLGNBQVQsQ0FBd0I5ekIsSUFBeEIsRUFBOEJoQixLQUE5QixFQUFxQztBQUNuQyxXQUFPLElBQUl3VCxPQUFKLENBQVksbUJBQVosRUFBaUMsbUJBQW1CeFQsS0FBbkIsR0FBMkIsWUFBM0IsV0FBaURBLEtBQWpELElBQXlELFNBQXpELEdBQXFFZ0IsSUFBckUsR0FBNEUsb0JBQTdHLENBQVA7QUFDRDs7QUFFRCxXQUFTK3pCLFNBQVQsQ0FBbUJ0ekIsSUFBbkIsRUFBeUJDLEtBQXpCLEVBQWdDQyxHQUFoQyxFQUFxQztBQUNuQyxRQUFJcXpCLEVBQUUsR0FBRyxJQUFJMzJCLElBQUosQ0FBU0EsSUFBSSxDQUFDaUosR0FBTCxDQUFTN0YsSUFBVCxFQUFlQyxLQUFLLEdBQUcsQ0FBdkIsRUFBMEJDLEdBQTFCLENBQVQsRUFBeUNzekIsU0FBekMsRUFBVDtBQUNBLFdBQU9ELEVBQUUsS0FBSyxDQUFQLEdBQVcsQ0FBWCxHQUFlQSxFQUF0QjtBQUNEOztBQUVELFdBQVNFLGNBQVQsQ0FBd0J6ekIsSUFBeEIsRUFBOEJDLEtBQTlCLEVBQXFDQyxHQUFyQyxFQUEwQztBQUN4QyxXQUFPQSxHQUFHLEdBQUcsQ0FBQ29GLFVBQVUsQ0FBQ3RGLElBQUQsQ0FBVixHQUFtQm96QixVQUFuQixHQUFnQ0QsYUFBakMsRUFBZ0RsekIsS0FBSyxHQUFHLENBQXhELENBQWI7QUFDRDs7QUFFRCxXQUFTeXpCLGdCQUFULENBQTBCMXpCLElBQTFCLEVBQWdDOFEsT0FBaEMsRUFBeUM7QUFDdkMsUUFBSTZpQixLQUFLLEdBQUdydUIsVUFBVSxDQUFDdEYsSUFBRCxDQUFWLEdBQW1Cb3pCLFVBQW5CLEdBQWdDRCxhQUE1QztBQUFBLFFBQ0lTLE1BQU0sR0FBR0QsS0FBSyxDQUFDNUQsU0FBTixDQUFnQixVQUFVbDFCLENBQVYsRUFBYTtBQUN4QyxhQUFPQSxDQUFDLEdBQUdpVyxPQUFYO0FBQ0QsS0FGWSxDQURiO0FBQUEsUUFJSTVRLEdBQUcsR0FBRzRRLE9BQU8sR0FBRzZpQixLQUFLLENBQUNDLE1BQUQsQ0FKekI7QUFLQSxXQUFPO0FBQ0wzekIsTUFBQUEsS0FBSyxFQUFFMnpCLE1BQU0sR0FBRyxDQURYO0FBRUwxekIsTUFBQUEsR0FBRyxFQUFFQTtBQUZBLEtBQVA7QUFJRDtBQUNEOzs7OztBQUtBLFdBQVMyekIsZUFBVCxDQUF5QkMsT0FBekIsRUFBa0M7QUFDaEMsUUFBSTl6QixJQUFJLEdBQUc4ekIsT0FBTyxDQUFDOXpCLElBQW5CO0FBQUEsUUFDSUMsS0FBSyxHQUFHNnpCLE9BQU8sQ0FBQzd6QixLQURwQjtBQUFBLFFBRUlDLEdBQUcsR0FBRzR6QixPQUFPLENBQUM1ekIsR0FGbEI7QUFBQSxRQUdJNFEsT0FBTyxHQUFHMmlCLGNBQWMsQ0FBQ3p6QixJQUFELEVBQU9DLEtBQVAsRUFBY0MsR0FBZCxDQUg1QjtBQUFBLFFBSUlJLE9BQU8sR0FBR2d6QixTQUFTLENBQUN0ekIsSUFBRCxFQUFPQyxLQUFQLEVBQWNDLEdBQWQsQ0FKdkI7QUFLQSxRQUFJMlEsVUFBVSxHQUFHN00sSUFBSSxDQUFDQyxLQUFMLENBQVcsQ0FBQzZNLE9BQU8sR0FBR3hRLE9BQVYsR0FBb0IsRUFBckIsSUFBMkIsQ0FBdEMsQ0FBakI7QUFBQSxRQUNJNEYsUUFESjs7QUFHQSxRQUFJMkssVUFBVSxHQUFHLENBQWpCLEVBQW9CO0FBQ2xCM0ssTUFBQUEsUUFBUSxHQUFHbEcsSUFBSSxHQUFHLENBQWxCO0FBQ0E2USxNQUFBQSxVQUFVLEdBQUc1SyxlQUFlLENBQUNDLFFBQUQsQ0FBNUI7QUFDRCxLQUhELE1BR08sSUFBSTJLLFVBQVUsR0FBRzVLLGVBQWUsQ0FBQ2pHLElBQUQsQ0FBaEMsRUFBd0M7QUFDN0NrRyxNQUFBQSxRQUFRLEdBQUdsRyxJQUFJLEdBQUcsQ0FBbEI7QUFDQTZRLE1BQUFBLFVBQVUsR0FBRyxDQUFiO0FBQ0QsS0FITSxNQUdBO0FBQ0wzSyxNQUFBQSxRQUFRLEdBQUdsRyxJQUFYO0FBQ0Q7O0FBRUQsV0FBTzdFLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYztBQUNuQmIsTUFBQUEsUUFBUSxFQUFFQSxRQURTO0FBRW5CMkssTUFBQUEsVUFBVSxFQUFFQSxVQUZPO0FBR25CdlEsTUFBQUEsT0FBTyxFQUFFQTtBQUhVLEtBQWQsRUFJSmdKLFVBQVUsQ0FBQ3dxQixPQUFELENBSk4sQ0FBUDtBQUtEOztBQUNELFdBQVNDLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQW1DO0FBQ2pDLFFBQUk5dEIsUUFBUSxHQUFHOHRCLFFBQVEsQ0FBQzl0QixRQUF4QjtBQUFBLFFBQ0kySyxVQUFVLEdBQUdtakIsUUFBUSxDQUFDbmpCLFVBRDFCO0FBQUEsUUFFSXZRLE9BQU8sR0FBRzB6QixRQUFRLENBQUMxekIsT0FGdkI7QUFBQSxRQUdJMnpCLGFBQWEsR0FBR1gsU0FBUyxDQUFDcHRCLFFBQUQsRUFBVyxDQUFYLEVBQWMsQ0FBZCxDQUg3QjtBQUFBLFFBSUlndUIsVUFBVSxHQUFHM3VCLFVBQVUsQ0FBQ1csUUFBRCxDQUozQjtBQUtBLFFBQUk0SyxPQUFPLEdBQUdELFVBQVUsR0FBRyxDQUFiLEdBQWlCdlEsT0FBakIsR0FBMkIyekIsYUFBM0IsR0FBMkMsQ0FBekQ7QUFBQSxRQUNJajBCLElBREo7O0FBR0EsUUFBSThRLE9BQU8sR0FBRyxDQUFkLEVBQWlCO0FBQ2Y5USxNQUFBQSxJQUFJLEdBQUdrRyxRQUFRLEdBQUcsQ0FBbEI7QUFDQTRLLE1BQUFBLE9BQU8sSUFBSXZMLFVBQVUsQ0FBQ3ZGLElBQUQsQ0FBckI7QUFDRCxLQUhELE1BR08sSUFBSThRLE9BQU8sR0FBR29qQixVQUFkLEVBQTBCO0FBQy9CbDBCLE1BQUFBLElBQUksR0FBR2tHLFFBQVEsR0FBRyxDQUFsQjtBQUNBNEssTUFBQUEsT0FBTyxJQUFJdkwsVUFBVSxDQUFDVyxRQUFELENBQXJCO0FBQ0QsS0FITSxNQUdBO0FBQ0xsRyxNQUFBQSxJQUFJLEdBQUdrRyxRQUFQO0FBQ0Q7O0FBRUQsUUFBSWl1QixpQkFBaUIsR0FBR1QsZ0JBQWdCLENBQUMxekIsSUFBRCxFQUFPOFEsT0FBUCxDQUF4QztBQUFBLFFBQ0k3USxLQUFLLEdBQUdrMEIsaUJBQWlCLENBQUNsMEIsS0FEOUI7QUFBQSxRQUVJQyxHQUFHLEdBQUdpMEIsaUJBQWlCLENBQUNqMEIsR0FGNUI7O0FBSUEsV0FBTy9FLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYztBQUNuQi9HLE1BQUFBLElBQUksRUFBRUEsSUFEYTtBQUVuQkMsTUFBQUEsS0FBSyxFQUFFQSxLQUZZO0FBR25CQyxNQUFBQSxHQUFHLEVBQUVBO0FBSGMsS0FBZCxFQUlKb0osVUFBVSxDQUFDMHFCLFFBQUQsQ0FKTixDQUFQO0FBS0Q7O0FBQ0QsV0FBU0ksa0JBQVQsQ0FBNEJDLFFBQTVCLEVBQXNDO0FBQ3BDLFFBQUlyMEIsSUFBSSxHQUFHcTBCLFFBQVEsQ0FBQ3IwQixJQUFwQjtBQUFBLFFBQ0lDLEtBQUssR0FBR28wQixRQUFRLENBQUNwMEIsS0FEckI7QUFBQSxRQUVJQyxHQUFHLEdBQUdtMEIsUUFBUSxDQUFDbjBCLEdBRm5CO0FBQUEsUUFHSTRRLE9BQU8sR0FBRzJpQixjQUFjLENBQUN6ekIsSUFBRCxFQUFPQyxLQUFQLEVBQWNDLEdBQWQsQ0FINUI7QUFJQSxXQUFPL0UsTUFBTSxDQUFDNEwsTUFBUCxDQUFjO0FBQ25CL0csTUFBQUEsSUFBSSxFQUFFQSxJQURhO0FBRW5COFEsTUFBQUEsT0FBTyxFQUFFQTtBQUZVLEtBQWQsRUFHSnhILFVBQVUsQ0FBQytxQixRQUFELENBSE4sQ0FBUDtBQUlEOztBQUNELFdBQVNDLGtCQUFULENBQTRCQyxXQUE1QixFQUF5QztBQUN2QyxRQUFJdjBCLElBQUksR0FBR3UwQixXQUFXLENBQUN2MEIsSUFBdkI7QUFBQSxRQUNJOFEsT0FBTyxHQUFHeWpCLFdBQVcsQ0FBQ3pqQixPQUQxQjtBQUFBLFFBRUkwakIsa0JBQWtCLEdBQUdkLGdCQUFnQixDQUFDMXpCLElBQUQsRUFBTzhRLE9BQVAsQ0FGekM7QUFBQSxRQUdJN1EsS0FBSyxHQUFHdTBCLGtCQUFrQixDQUFDdjBCLEtBSC9CO0FBQUEsUUFJSUMsR0FBRyxHQUFHczBCLGtCQUFrQixDQUFDdDBCLEdBSjdCOztBQU1BLFdBQU8vRSxNQUFNLENBQUM0TCxNQUFQLENBQWM7QUFDbkIvRyxNQUFBQSxJQUFJLEVBQUVBLElBRGE7QUFFbkJDLE1BQUFBLEtBQUssRUFBRUEsS0FGWTtBQUduQkMsTUFBQUEsR0FBRyxFQUFFQTtBQUhjLEtBQWQsRUFJSm9KLFVBQVUsQ0FBQ2lyQixXQUFELENBSk4sQ0FBUDtBQUtEOztBQUNELFdBQVNFLGtCQUFULENBQTRCbnhCLEdBQTVCLEVBQWlDO0FBQy9CLFFBQUlveEIsU0FBUyxHQUFHM3lCLFNBQVMsQ0FBQ3VCLEdBQUcsQ0FBQzRDLFFBQUwsQ0FBekI7QUFBQSxRQUNJeXVCLFNBQVMsR0FBR2h4QixjQUFjLENBQUNMLEdBQUcsQ0FBQ3VOLFVBQUwsRUFBaUIsQ0FBakIsRUFBb0I1SyxlQUFlLENBQUMzQyxHQUFHLENBQUM0QyxRQUFMLENBQW5DLENBRDlCO0FBQUEsUUFFSTB1QixZQUFZLEdBQUdqeEIsY0FBYyxDQUFDTCxHQUFHLENBQUNoRCxPQUFMLEVBQWMsQ0FBZCxFQUFpQixDQUFqQixDQUZqQzs7QUFJQSxRQUFJLENBQUNvMEIsU0FBTCxFQUFnQjtBQUNkLGFBQU9yQixjQUFjLENBQUMsVUFBRCxFQUFhL3ZCLEdBQUcsQ0FBQzRDLFFBQWpCLENBQXJCO0FBQ0QsS0FGRCxNQUVPLElBQUksQ0FBQ3l1QixTQUFMLEVBQWdCO0FBQ3JCLGFBQU90QixjQUFjLENBQUMsTUFBRCxFQUFTL3ZCLEdBQUcsQ0FBQ3dnQixJQUFiLENBQXJCO0FBQ0QsS0FGTSxNQUVBLElBQUksQ0FBQzhRLFlBQUwsRUFBbUI7QUFDeEIsYUFBT3ZCLGNBQWMsQ0FBQyxTQUFELEVBQVkvdkIsR0FBRyxDQUFDaEQsT0FBaEIsQ0FBckI7QUFDRCxLQUZNLE1BRUEsT0FBTyxLQUFQO0FBQ1I7O0FBQ0QsV0FBU3UwQixxQkFBVCxDQUErQnZ4QixHQUEvQixFQUFvQztBQUNsQyxRQUFJb3hCLFNBQVMsR0FBRzN5QixTQUFTLENBQUN1QixHQUFHLENBQUN0RCxJQUFMLENBQXpCO0FBQUEsUUFDSTgwQixZQUFZLEdBQUdueEIsY0FBYyxDQUFDTCxHQUFHLENBQUN3TixPQUFMLEVBQWMsQ0FBZCxFQUFpQnZMLFVBQVUsQ0FBQ2pDLEdBQUcsQ0FBQ3RELElBQUwsQ0FBM0IsQ0FEakM7O0FBR0EsUUFBSSxDQUFDMDBCLFNBQUwsRUFBZ0I7QUFDZCxhQUFPckIsY0FBYyxDQUFDLE1BQUQsRUFBUy92QixHQUFHLENBQUN0RCxJQUFiLENBQXJCO0FBQ0QsS0FGRCxNQUVPLElBQUksQ0FBQzgwQixZQUFMLEVBQW1CO0FBQ3hCLGFBQU96QixjQUFjLENBQUMsU0FBRCxFQUFZL3ZCLEdBQUcsQ0FBQ3dOLE9BQWhCLENBQXJCO0FBQ0QsS0FGTSxNQUVBLE9BQU8sS0FBUDtBQUNSOztBQUNELFdBQVNpa0IsdUJBQVQsQ0FBaUN6eEIsR0FBakMsRUFBc0M7QUFDcEMsUUFBSW94QixTQUFTLEdBQUczeUIsU0FBUyxDQUFDdUIsR0FBRyxDQUFDdEQsSUFBTCxDQUF6QjtBQUFBLFFBQ0lnMUIsVUFBVSxHQUFHcnhCLGNBQWMsQ0FBQ0wsR0FBRyxDQUFDckQsS0FBTCxFQUFZLENBQVosRUFBZSxFQUFmLENBRC9CO0FBQUEsUUFFSWcxQixRQUFRLEdBQUd0eEIsY0FBYyxDQUFDTCxHQUFHLENBQUNwRCxHQUFMLEVBQVUsQ0FBVixFQUFhc0YsV0FBVyxDQUFDbEMsR0FBRyxDQUFDdEQsSUFBTCxFQUFXc0QsR0FBRyxDQUFDckQsS0FBZixDQUF4QixDQUY3Qjs7QUFJQSxRQUFJLENBQUN5MEIsU0FBTCxFQUFnQjtBQUNkLGFBQU9yQixjQUFjLENBQUMsTUFBRCxFQUFTL3ZCLEdBQUcsQ0FBQ3RELElBQWIsQ0FBckI7QUFDRCxLQUZELE1BRU8sSUFBSSxDQUFDZzFCLFVBQUwsRUFBaUI7QUFDdEIsYUFBTzNCLGNBQWMsQ0FBQyxPQUFELEVBQVUvdkIsR0FBRyxDQUFDckQsS0FBZCxDQUFyQjtBQUNELEtBRk0sTUFFQSxJQUFJLENBQUNnMUIsUUFBTCxFQUFlO0FBQ3BCLGFBQU81QixjQUFjLENBQUMsS0FBRCxFQUFRL3ZCLEdBQUcsQ0FBQ3BELEdBQVosQ0FBckI7QUFDRCxLQUZNLE1BRUEsT0FBTyxLQUFQO0FBQ1I7O0FBQ0QsV0FBU2cxQixrQkFBVCxDQUE0QjV4QixHQUE1QixFQUFpQztBQUMvQixRQUFJOUMsSUFBSSxHQUFHOEMsR0FBRyxDQUFDOUMsSUFBZjtBQUFBLFFBQ0lDLE1BQU0sR0FBRzZDLEdBQUcsQ0FBQzdDLE1BRGpCO0FBQUEsUUFFSUUsTUFBTSxHQUFHMkMsR0FBRyxDQUFDM0MsTUFGakI7QUFBQSxRQUdJbUYsV0FBVyxHQUFHeEMsR0FBRyxDQUFDd0MsV0FIdEI7QUFJQSxRQUFJcXZCLFNBQVMsR0FBR3h4QixjQUFjLENBQUNuRCxJQUFELEVBQU8sQ0FBUCxFQUFVLEVBQVYsQ0FBZCxJQUErQkEsSUFBSSxLQUFLLEVBQVQsSUFBZUMsTUFBTSxLQUFLLENBQTFCLElBQStCRSxNQUFNLEtBQUssQ0FBMUMsSUFBK0NtRixXQUFXLEtBQUssQ0FBOUc7QUFBQSxRQUNJc3ZCLFdBQVcsR0FBR3p4QixjQUFjLENBQUNsRCxNQUFELEVBQVMsQ0FBVCxFQUFZLEVBQVosQ0FEaEM7QUFBQSxRQUVJNDBCLFdBQVcsR0FBRzF4QixjQUFjLENBQUNoRCxNQUFELEVBQVMsQ0FBVCxFQUFZLEVBQVosQ0FGaEM7QUFBQSxRQUdJMjBCLGdCQUFnQixHQUFHM3hCLGNBQWMsQ0FBQ21DLFdBQUQsRUFBYyxDQUFkLEVBQWlCLEdBQWpCLENBSHJDOztBQUtBLFFBQUksQ0FBQ3F2QixTQUFMLEVBQWdCO0FBQ2QsYUFBTzlCLGNBQWMsQ0FBQyxNQUFELEVBQVM3eUIsSUFBVCxDQUFyQjtBQUNELEtBRkQsTUFFTyxJQUFJLENBQUM0MEIsV0FBTCxFQUFrQjtBQUN2QixhQUFPL0IsY0FBYyxDQUFDLFFBQUQsRUFBVzV5QixNQUFYLENBQXJCO0FBQ0QsS0FGTSxNQUVBLElBQUksQ0FBQzQwQixXQUFMLEVBQWtCO0FBQ3ZCLGFBQU9oQyxjQUFjLENBQUMsUUFBRCxFQUFXMXlCLE1BQVgsQ0FBckI7QUFDRCxLQUZNLE1BRUEsSUFBSSxDQUFDMjBCLGdCQUFMLEVBQXVCO0FBQzVCLGFBQU9qQyxjQUFjLENBQUMsYUFBRCxFQUFnQnZ0QixXQUFoQixDQUFyQjtBQUNELEtBRk0sTUFFQSxPQUFPLEtBQVA7QUFDUjs7QUFFRCxNQUFJeXZCLFNBQVMsR0FBRyxrQkFBaEI7QUFDQSxNQUFJQyxRQUFRLEdBQUcsT0FBZjs7QUFFQSxXQUFTQyxlQUFULENBQXlCbmxCLElBQXpCLEVBQStCO0FBQzdCLFdBQU8sSUFBSXlCLE9BQUosQ0FBWSxrQkFBWixFQUFnQyxnQkFBZ0J6QixJQUFJLENBQUN5RCxJQUFyQixHQUE0QixxQkFBNUQsQ0FBUDtBQUNELEdBaHRMNkIsQ0FndEw1Qjs7O0FBR0YsV0FBUzJoQixzQkFBVCxDQUFnQ2pyQixFQUFoQyxFQUFvQztBQUNsQyxRQUFJQSxFQUFFLENBQUN1cEIsUUFBSCxLQUFnQixJQUFwQixFQUEwQjtBQUN4QnZwQixNQUFBQSxFQUFFLENBQUN1cEIsUUFBSCxHQUFjSCxlQUFlLENBQUNwcEIsRUFBRSxDQUFDaUUsQ0FBSixDQUE3QjtBQUNEOztBQUVELFdBQU9qRSxFQUFFLENBQUN1cEIsUUFBVjtBQUNELEdBenRMNkIsQ0F5dEw1QjtBQUNGOzs7QUFHQSxXQUFTMkIsT0FBVCxDQUFpQkMsSUFBakIsRUFBdUJ0YixJQUF2QixFQUE2QjtBQUMzQixRQUFJL0wsT0FBTyxHQUFHO0FBQ1ovSCxNQUFBQSxFQUFFLEVBQUVvdkIsSUFBSSxDQUFDcHZCLEVBREc7QUFFWjhKLE1BQUFBLElBQUksRUFBRXNsQixJQUFJLENBQUN0bEIsSUFGQztBQUdaNUIsTUFBQUEsQ0FBQyxFQUFFa25CLElBQUksQ0FBQ2xuQixDQUhJO0FBSVp4UyxNQUFBQSxDQUFDLEVBQUUwNUIsSUFBSSxDQUFDMTVCLENBSkk7QUFLWjRTLE1BQUFBLEdBQUcsRUFBRThtQixJQUFJLENBQUM5bUIsR0FMRTtBQU1aMFUsTUFBQUEsT0FBTyxFQUFFb1MsSUFBSSxDQUFDcFM7QUFORixLQUFkO0FBUUEsV0FBTyxJQUFJbE0sUUFBSixDQUFhbmMsTUFBTSxDQUFDNEwsTUFBUCxDQUFjLEVBQWQsRUFBa0J3SCxPQUFsQixFQUEyQitMLElBQTNCLEVBQWlDO0FBQ25EdWIsTUFBQUEsR0FBRyxFQUFFdG5CO0FBRDhDLEtBQWpDLENBQWIsQ0FBUDtBQUdELEdBenVMNkIsQ0F5dUw1QjtBQUNGOzs7QUFHQSxXQUFTdW5CLFNBQVQsQ0FBbUJDLE9BQW5CLEVBQTRCNzVCLENBQTVCLEVBQStCODVCLEVBQS9CLEVBQW1DO0FBQ2pDO0FBQ0EsUUFBSUMsUUFBUSxHQUFHRixPQUFPLEdBQUc3NUIsQ0FBQyxHQUFHLEVBQUosR0FBUyxJQUFsQyxDQUZpQyxDQUVPOztBQUV4QyxRQUFJZzZCLEVBQUUsR0FBR0YsRUFBRSxDQUFDanRCLE1BQUgsQ0FBVWt0QixRQUFWLENBQVQsQ0FKaUMsQ0FJSDs7QUFFOUIsUUFBSS81QixDQUFDLEtBQUtnNkIsRUFBVixFQUFjO0FBQ1osYUFBTyxDQUFDRCxRQUFELEVBQVcvNUIsQ0FBWCxDQUFQO0FBQ0QsS0FSZ0MsQ0FRL0I7OztBQUdGKzVCLElBQUFBLFFBQVEsSUFBSSxDQUFDQyxFQUFFLEdBQUdoNkIsQ0FBTixJQUFXLEVBQVgsR0FBZ0IsSUFBNUIsQ0FYaUMsQ0FXQzs7QUFFbEMsUUFBSWk2QixFQUFFLEdBQUdILEVBQUUsQ0FBQ2p0QixNQUFILENBQVVrdEIsUUFBVixDQUFUOztBQUVBLFFBQUlDLEVBQUUsS0FBS0MsRUFBWCxFQUFlO0FBQ2IsYUFBTyxDQUFDRixRQUFELEVBQVdDLEVBQVgsQ0FBUDtBQUNELEtBakJnQyxDQWlCL0I7OztBQUdGLFdBQU8sQ0FBQ0gsT0FBTyxHQUFHL3hCLElBQUksQ0FBQ2lyQixHQUFMLENBQVNpSCxFQUFULEVBQWFDLEVBQWIsSUFBbUIsRUFBbkIsR0FBd0IsSUFBbkMsRUFBeUNueUIsSUFBSSxDQUFDa3JCLEdBQUwsQ0FBU2dILEVBQVQsRUFBYUMsRUFBYixDQUF6QyxDQUFQO0FBQ0QsR0Fsd0w2QixDQWt3TDVCOzs7QUFHRixXQUFTQyxPQUFULENBQWlCNXZCLEVBQWpCLEVBQXFCdUMsTUFBckIsRUFBNkI7QUFDM0J2QyxJQUFBQSxFQUFFLElBQUl1QyxNQUFNLEdBQUcsRUFBVCxHQUFjLElBQXBCO0FBQ0EsUUFBSW5ELENBQUMsR0FBRyxJQUFJaEosSUFBSixDQUFTNEosRUFBVCxDQUFSO0FBQ0EsV0FBTztBQUNMeEcsTUFBQUEsSUFBSSxFQUFFNEYsQ0FBQyxDQUFDSSxjQUFGLEVBREQ7QUFFTC9GLE1BQUFBLEtBQUssRUFBRTJGLENBQUMsQ0FBQ3l3QixXQUFGLEtBQWtCLENBRnBCO0FBR0xuMkIsTUFBQUEsR0FBRyxFQUFFMEYsQ0FBQyxDQUFDMHdCLFVBQUYsRUFIQTtBQUlMOTFCLE1BQUFBLElBQUksRUFBRW9GLENBQUMsQ0FBQzJ3QixXQUFGLEVBSkQ7QUFLTDkxQixNQUFBQSxNQUFNLEVBQUVtRixDQUFDLENBQUM0d0IsYUFBRixFQUxIO0FBTUw3MUIsTUFBQUEsTUFBTSxFQUFFaUYsQ0FBQyxDQUFDNndCLGFBQUYsRUFOSDtBQU9MM3dCLE1BQUFBLFdBQVcsRUFBRUYsQ0FBQyxDQUFDOHdCLGtCQUFGO0FBUFIsS0FBUDtBQVNELEdBanhMNkIsQ0FpeEw1Qjs7O0FBR0YsV0FBU0MsT0FBVCxDQUFpQnJ6QixHQUFqQixFQUFzQnlGLE1BQXRCLEVBQThCdUgsSUFBOUIsRUFBb0M7QUFDbEMsV0FBT3dsQixTQUFTLENBQUNud0IsWUFBWSxDQUFDckMsR0FBRCxDQUFiLEVBQW9CeUYsTUFBcEIsRUFBNEJ1SCxJQUE1QixDQUFoQjtBQUNELEdBdHhMNkIsQ0FzeEw1Qjs7O0FBR0YsV0FBU3NtQixVQUFULENBQW9CaEIsSUFBcEIsRUFBMEIza0IsR0FBMUIsRUFBK0I7QUFDN0IsUUFBSStWLElBQUo7O0FBRUEsUUFBSXpqQixJQUFJLEdBQUdwSSxNQUFNLENBQUNvSSxJQUFQLENBQVkwTixHQUFHLENBQUNtUixNQUFoQixDQUFYOztBQUVBLFFBQUk3ZSxJQUFJLENBQUMxRixPQUFMLENBQWEsY0FBYixNQUFpQyxDQUFDLENBQXRDLEVBQXlDO0FBQ3ZDMEYsTUFBQUEsSUFBSSxDQUFDbEcsSUFBTCxDQUFVLGNBQVY7QUFDRDs7QUFFRDRULElBQUFBLEdBQUcsR0FBRyxDQUFDK1YsSUFBSSxHQUFHL1YsR0FBUixFQUFhVyxPQUFiLENBQXFCdFUsS0FBckIsQ0FBMkIwcEIsSUFBM0IsRUFBaUN6akIsSUFBakMsQ0FBTjtBQUNBLFFBQUlzekIsSUFBSSxHQUFHakIsSUFBSSxDQUFDMTVCLENBQWhCO0FBQUEsUUFDSThELElBQUksR0FBRzQxQixJQUFJLENBQUNsbkIsQ0FBTCxDQUFPMU8sSUFBUCxHQUFjaVIsR0FBRyxDQUFDL0YsS0FEN0I7QUFBQSxRQUVJakwsS0FBSyxHQUFHMjFCLElBQUksQ0FBQ2xuQixDQUFMLENBQU96TyxLQUFQLEdBQWVnUixHQUFHLENBQUNuSCxNQUFuQixHQUE0Qm1ILEdBQUcsQ0FBQzlGLFFBQUosR0FBZSxDQUZ2RDtBQUFBLFFBR0l1RCxDQUFDLEdBQUd2VCxNQUFNLENBQUM0TCxNQUFQLENBQWMsRUFBZCxFQUFrQjZ1QixJQUFJLENBQUNsbkIsQ0FBdkIsRUFBMEI7QUFDaEMxTyxNQUFBQSxJQUFJLEVBQUVBLElBRDBCO0FBRWhDQyxNQUFBQSxLQUFLLEVBQUVBLEtBRnlCO0FBR2hDQyxNQUFBQSxHQUFHLEVBQUU4RCxJQUFJLENBQUNpckIsR0FBTCxDQUFTMkcsSUFBSSxDQUFDbG5CLENBQUwsQ0FBT3hPLEdBQWhCLEVBQXFCc0YsV0FBVyxDQUFDeEYsSUFBRCxFQUFPQyxLQUFQLENBQWhDLElBQWlEZ1IsR0FBRyxDQUFDNUYsSUFBckQsR0FBNEQ0RixHQUFHLENBQUM3RixLQUFKLEdBQVk7QUFIN0MsS0FBMUIsQ0FIUjtBQUFBLFFBUUkwckIsV0FBVyxHQUFHeFUsUUFBUSxDQUFDakosVUFBVCxDQUFvQjtBQUNwQ3JRLE1BQUFBLEtBQUssRUFBRWlJLEdBQUcsQ0FBQ2pJLEtBRHlCO0FBRXBDQyxNQUFBQSxPQUFPLEVBQUVnSSxHQUFHLENBQUNoSSxPQUZ1QjtBQUdwQ3FDLE1BQUFBLE9BQU8sRUFBRTJGLEdBQUcsQ0FBQzNGLE9BSHVCO0FBSXBDeVQsTUFBQUEsWUFBWSxFQUFFOU4sR0FBRyxDQUFDOE47QUFKa0IsS0FBcEIsRUFLZnVGLEVBTGUsQ0FLWixjQUxZLENBUmxCO0FBQUEsUUFjSXlSLE9BQU8sR0FBR3B3QixZQUFZLENBQUMrSSxDQUFELENBZDFCOztBQWdCQSxRQUFJcW9CLFVBQVUsR0FBR2pCLFNBQVMsQ0FBQ0MsT0FBRCxFQUFVYyxJQUFWLEVBQWdCakIsSUFBSSxDQUFDdGxCLElBQXJCLENBQTFCO0FBQUEsUUFDSTlKLEVBQUUsR0FBR3V3QixVQUFVLENBQUMsQ0FBRCxDQURuQjtBQUFBLFFBRUk3NkIsQ0FBQyxHQUFHNjZCLFVBQVUsQ0FBQyxDQUFELENBRmxCOztBQUlBLFFBQUlELFdBQVcsS0FBSyxDQUFwQixFQUF1QjtBQUNyQnR3QixNQUFBQSxFQUFFLElBQUlzd0IsV0FBTixDQURxQixDQUNGOztBQUVuQjU2QixNQUFBQSxDQUFDLEdBQUcwNUIsSUFBSSxDQUFDdGxCLElBQUwsQ0FBVXZILE1BQVYsQ0FBaUJ2QyxFQUFqQixDQUFKO0FBQ0Q7O0FBRUQsV0FBTztBQUNMQSxNQUFBQSxFQUFFLEVBQUVBLEVBREM7QUFFTHRLLE1BQUFBLENBQUMsRUFBRUE7QUFGRSxLQUFQO0FBSUQsR0FqMEw2QixDQWkwTDVCO0FBQ0Y7OztBQUdBLFdBQVM4NkIsbUJBQVQsQ0FBNkIvdkIsTUFBN0IsRUFBcUNnd0IsVUFBckMsRUFBaUQ3b0IsSUFBakQsRUFBdUQ3RyxNQUF2RCxFQUErRHFjLElBQS9ELEVBQXFFO0FBQ25FLFFBQUlzRyxPQUFPLEdBQUc5YixJQUFJLENBQUM4YixPQUFuQjtBQUFBLFFBQ0k1WixJQUFJLEdBQUdsQyxJQUFJLENBQUNrQyxJQURoQjs7QUFHQSxRQUFJckosTUFBTSxJQUFJOUwsTUFBTSxDQUFDb0ksSUFBUCxDQUFZMEQsTUFBWixFQUFvQm5NLE1BQXBCLEtBQStCLENBQTdDLEVBQWdEO0FBQzlDLFVBQUlvOEIsa0JBQWtCLEdBQUdELFVBQVUsSUFBSTNtQixJQUF2QztBQUFBLFVBQ0lzbEIsSUFBSSxHQUFHdGUsUUFBUSxDQUFDK0IsVUFBVCxDQUFvQmxlLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBY0UsTUFBZCxFQUFzQm1ILElBQXRCLEVBQTRCO0FBQ3pEa0MsUUFBQUEsSUFBSSxFQUFFNG1CLGtCQURtRDtBQUV6RDtBQUNBaE4sUUFBQUEsT0FBTyxFQUFFanNCO0FBSGdELE9BQTVCLENBQXBCLENBRFg7QUFNQSxhQUFPaXNCLE9BQU8sR0FBRzBMLElBQUgsR0FBVUEsSUFBSSxDQUFDMUwsT0FBTCxDQUFhNVosSUFBYixDQUF4QjtBQUNELEtBUkQsTUFRTztBQUNMLGFBQU9nSCxRQUFRLENBQUNrTSxPQUFULENBQWlCLElBQUl6UixPQUFKLENBQVksWUFBWixFQUEwQixpQkFBaUI2UixJQUFqQixHQUF3Qix3QkFBeEIsR0FBbURyYyxNQUE3RSxDQUFqQixDQUFQO0FBQ0Q7QUFDRixHQXAxTDZCLENBbzFMNUI7QUFDRjs7O0FBR0EsV0FBUzR2QixZQUFULENBQXNCMXNCLEVBQXRCLEVBQTBCbEQsTUFBMUIsRUFBa0M7QUFDaEMsV0FBT2tELEVBQUUsQ0FBQzRGLE9BQUgsR0FBYWxDLFNBQVMsQ0FBQ3JTLE1BQVYsQ0FBaUIrWixNQUFNLENBQUMvWixNQUFQLENBQWMsT0FBZCxDQUFqQixFQUF5QztBQUMzRHNVLE1BQUFBLE1BQU0sRUFBRSxJQURtRDtBQUUzRFgsTUFBQUEsV0FBVyxFQUFFO0FBRjhDLEtBQXpDLEVBR2pCRyx3QkFIaUIsQ0FHUW5GLEVBSFIsRUFHWWxELE1BSFosQ0FBYixHQUdtQyxJQUgxQztBQUlELEdBNzFMNkIsQ0E2MUw1QjtBQUNGOzs7QUFHQSxXQUFTNnZCLGdCQUFULENBQTBCM3NCLEVBQTFCLEVBQThCaUMsSUFBOUIsRUFBb0M7QUFDbEMsUUFBSTJxQixvQkFBb0IsR0FBRzNxQixJQUFJLENBQUM0cUIsZUFBaEM7QUFBQSxRQUNJQSxlQUFlLEdBQUdELG9CQUFvQixLQUFLLEtBQUssQ0FBOUIsR0FBa0MsS0FBbEMsR0FBMENBLG9CQURoRTtBQUFBLFFBRUlFLHFCQUFxQixHQUFHN3FCLElBQUksQ0FBQzhxQixvQkFGakM7QUFBQSxRQUdJQSxvQkFBb0IsR0FBR0QscUJBQXFCLEtBQUssS0FBSyxDQUEvQixHQUFtQyxLQUFuQyxHQUEyQ0EscUJBSHRFO0FBQUEsUUFJSUUsYUFBYSxHQUFHL3FCLElBQUksQ0FBQytxQixhQUp6QjtBQUFBLFFBS0lDLGdCQUFnQixHQUFHaHJCLElBQUksQ0FBQ2lyQixXQUw1QjtBQUFBLFFBTUlBLFdBQVcsR0FBR0QsZ0JBQWdCLEtBQUssS0FBSyxDQUExQixHQUE4QixLQUE5QixHQUFzQ0EsZ0JBTnhEO0FBQUEsUUFPSUUsY0FBYyxHQUFHbHJCLElBQUksQ0FBQ21yQixTQVAxQjtBQUFBLFFBUUlBLFNBQVMsR0FBR0QsY0FBYyxLQUFLLEtBQUssQ0FBeEIsR0FBNEIsS0FBNUIsR0FBb0NBLGNBUnBEO0FBU0EsUUFBSXRwQixHQUFHLEdBQUcsT0FBVjs7QUFFQSxRQUFJLENBQUNncEIsZUFBRCxJQUFvQjdzQixFQUFFLENBQUM5SixNQUFILEtBQWMsQ0FBbEMsSUFBdUM4SixFQUFFLENBQUMzRSxXQUFILEtBQW1CLENBQTlELEVBQWlFO0FBQy9Ed0ksTUFBQUEsR0FBRyxJQUFJLEtBQVA7O0FBRUEsVUFBSSxDQUFDa3BCLG9CQUFELElBQXlCL3NCLEVBQUUsQ0FBQzNFLFdBQUgsS0FBbUIsQ0FBaEQsRUFBbUQ7QUFDakR3SSxRQUFBQSxHQUFHLElBQUksTUFBUDtBQUNEO0FBQ0Y7O0FBRUQsUUFBSSxDQUFDcXBCLFdBQVcsSUFBSUYsYUFBaEIsS0FBa0NJLFNBQXRDLEVBQWlEO0FBQy9DdnBCLE1BQUFBLEdBQUcsSUFBSSxHQUFQO0FBQ0Q7O0FBRUQsUUFBSXFwQixXQUFKLEVBQWlCO0FBQ2ZycEIsTUFBQUEsR0FBRyxJQUFJLEdBQVA7QUFDRCxLQUZELE1BRU8sSUFBSW1wQixhQUFKLEVBQW1CO0FBQ3hCbnBCLE1BQUFBLEdBQUcsSUFBSSxJQUFQO0FBQ0Q7O0FBRUQsV0FBTzZvQixZQUFZLENBQUMxc0IsRUFBRCxFQUFLNkQsR0FBTCxDQUFuQjtBQUNELEdBaDRMNkIsQ0FnNEw1Qjs7O0FBR0YsTUFBSXdwQixpQkFBaUIsR0FBRztBQUN0QjczQixJQUFBQSxLQUFLLEVBQUUsQ0FEZTtBQUV0QkMsSUFBQUEsR0FBRyxFQUFFLENBRmlCO0FBR3RCTSxJQUFBQSxJQUFJLEVBQUUsQ0FIZ0I7QUFJdEJDLElBQUFBLE1BQU0sRUFBRSxDQUpjO0FBS3RCRSxJQUFBQSxNQUFNLEVBQUUsQ0FMYztBQU10Qm1GLElBQUFBLFdBQVcsRUFBRTtBQU5TLEdBQXhCO0FBQUEsTUFRSWl5QixxQkFBcUIsR0FBRztBQUMxQmxuQixJQUFBQSxVQUFVLEVBQUUsQ0FEYztBQUUxQnZRLElBQUFBLE9BQU8sRUFBRSxDQUZpQjtBQUcxQkUsSUFBQUEsSUFBSSxFQUFFLENBSG9CO0FBSTFCQyxJQUFBQSxNQUFNLEVBQUUsQ0FKa0I7QUFLMUJFLElBQUFBLE1BQU0sRUFBRSxDQUxrQjtBQU0xQm1GLElBQUFBLFdBQVcsRUFBRTtBQU5hLEdBUjVCO0FBQUEsTUFnQklreUIsd0JBQXdCLEdBQUc7QUFDN0JsbkIsSUFBQUEsT0FBTyxFQUFFLENBRG9CO0FBRTdCdFEsSUFBQUEsSUFBSSxFQUFFLENBRnVCO0FBRzdCQyxJQUFBQSxNQUFNLEVBQUUsQ0FIcUI7QUFJN0JFLElBQUFBLE1BQU0sRUFBRSxDQUpxQjtBQUs3Qm1GLElBQUFBLFdBQVcsRUFBRTtBQUxnQixHQWhCL0IsQ0FuNEw4QixDQXk1TDNCOztBQUVILE1BQUlteUIsY0FBYyxHQUFHLENBQUMsTUFBRCxFQUFTLE9BQVQsRUFBa0IsS0FBbEIsRUFBeUIsTUFBekIsRUFBaUMsUUFBakMsRUFBMkMsUUFBM0MsRUFBcUQsYUFBckQsQ0FBckI7QUFBQSxNQUNJQyxnQkFBZ0IsR0FBRyxDQUFDLFVBQUQsRUFBYSxZQUFiLEVBQTJCLFNBQTNCLEVBQXNDLE1BQXRDLEVBQThDLFFBQTlDLEVBQXdELFFBQXhELEVBQWtFLGFBQWxFLENBRHZCO0FBQUEsTUFFSUMsbUJBQW1CLEdBQUcsQ0FBQyxNQUFELEVBQVMsU0FBVCxFQUFvQixNQUFwQixFQUE0QixRQUE1QixFQUFzQyxRQUF0QyxFQUFnRCxhQUFoRCxDQUYxQixDQTM1TDhCLENBNjVMNEQ7O0FBRTFGLFdBQVN6VSxhQUFULENBQXVCbmtCLElBQXZCLEVBQTZCO0FBQzNCLFFBQUlvSixVQUFVLEdBQUc7QUFDZjNJLE1BQUFBLElBQUksRUFBRSxNQURTO0FBRWZrTCxNQUFBQSxLQUFLLEVBQUUsTUFGUTtBQUdmakwsTUFBQUEsS0FBSyxFQUFFLE9BSFE7QUFJZjZKLE1BQUFBLE1BQU0sRUFBRSxPQUpPO0FBS2Y1SixNQUFBQSxHQUFHLEVBQUUsS0FMVTtBQU1mbUwsTUFBQUEsSUFBSSxFQUFFLEtBTlM7QUFPZjdLLE1BQUFBLElBQUksRUFBRSxNQVBTO0FBUWZ3SSxNQUFBQSxLQUFLLEVBQUUsTUFSUTtBQVNmdkksTUFBQUEsTUFBTSxFQUFFLFFBVE87QUFVZndJLE1BQUFBLE9BQU8sRUFBRSxRQVZNO0FBV2Y4SCxNQUFBQSxPQUFPLEVBQUUsU0FYTTtBQVlmNUYsTUFBQUEsUUFBUSxFQUFFLFNBWks7QUFhZnhLLE1BQUFBLE1BQU0sRUFBRSxRQWJPO0FBY2YySyxNQUFBQSxPQUFPLEVBQUUsUUFkTTtBQWVmeEYsTUFBQUEsV0FBVyxFQUFFLGFBZkU7QUFnQmZpWixNQUFBQSxZQUFZLEVBQUUsYUFoQkM7QUFpQmZ6ZSxNQUFBQSxPQUFPLEVBQUUsU0FqQk07QUFrQmY0SixNQUFBQSxRQUFRLEVBQUUsU0FsQks7QUFtQmZrdUIsTUFBQUEsVUFBVSxFQUFFLFlBbkJHO0FBb0JmQyxNQUFBQSxXQUFXLEVBQUUsWUFwQkU7QUFxQmZDLE1BQUFBLFdBQVcsRUFBRSxZQXJCRTtBQXNCZkMsTUFBQUEsUUFBUSxFQUFFLFVBdEJLO0FBdUJmQyxNQUFBQSxTQUFTLEVBQUUsVUF2Qkk7QUF3QmYxbkIsTUFBQUEsT0FBTyxFQUFFO0FBeEJNLE1BeUJmdlIsSUFBSSxDQUFDOEgsV0FBTCxFQXpCZSxDQUFqQjtBQTBCQSxRQUFJLENBQUNzQixVQUFMLEVBQWlCLE1BQU0sSUFBSXRKLGdCQUFKLENBQXFCRSxJQUFyQixDQUFOO0FBQ2pCLFdBQU9vSixVQUFQO0FBQ0QsR0E1N0w2QixDQTQ3TDVCO0FBQ0Y7QUFDQTs7O0FBR0EsV0FBUzh2QixPQUFULENBQWlCbjFCLEdBQWpCLEVBQXNCZ04sSUFBdEIsRUFBNEI7QUFDMUI7QUFDQSxTQUFLLElBQUkvRCxFQUFFLEdBQUcsQ0FBVCxFQUFZbVksYUFBYSxHQUFHdVQsY0FBakMsRUFBaUQxckIsRUFBRSxHQUFHbVksYUFBYSxDQUFDNXBCLE1BQXBFLEVBQTRFeVIsRUFBRSxFQUE5RSxFQUFrRjtBQUNoRixVQUFJM0QsQ0FBQyxHQUFHOGIsYUFBYSxDQUFDblksRUFBRCxDQUFyQjs7QUFFQSxVQUFJMUssV0FBVyxDQUFDeUIsR0FBRyxDQUFDc0YsQ0FBRCxDQUFKLENBQWYsRUFBeUI7QUFDdkJ0RixRQUFBQSxHQUFHLENBQUNzRixDQUFELENBQUgsR0FBU2t2QixpQkFBaUIsQ0FBQ2x2QixDQUFELENBQTFCO0FBQ0Q7QUFDRjs7QUFFRCxRQUFJNGEsT0FBTyxHQUFHdVIsdUJBQXVCLENBQUN6eEIsR0FBRCxDQUF2QixJQUFnQzR4QixrQkFBa0IsQ0FBQzV4QixHQUFELENBQWhFOztBQUVBLFFBQUlrZ0IsT0FBSixFQUFhO0FBQ1gsYUFBT2xNLFFBQVEsQ0FBQ2tNLE9BQVQsQ0FBaUJBLE9BQWpCLENBQVA7QUFDRDs7QUFFRCxRQUFJa1YsS0FBSyxHQUFHL2lCLFFBQVEsQ0FBQ0wsR0FBVCxFQUFaO0FBQUEsUUFDSXFqQixZQUFZLEdBQUdyb0IsSUFBSSxDQUFDdkgsTUFBTCxDQUFZMnZCLEtBQVosQ0FEbkI7QUFBQSxRQUVJRSxRQUFRLEdBQUdqQyxPQUFPLENBQUNyekIsR0FBRCxFQUFNcTFCLFlBQU4sRUFBb0Jyb0IsSUFBcEIsQ0FGdEI7QUFBQSxRQUdJOUosRUFBRSxHQUFHb3lCLFFBQVEsQ0FBQyxDQUFELENBSGpCO0FBQUEsUUFJSTE4QixDQUFDLEdBQUcwOEIsUUFBUSxDQUFDLENBQUQsQ0FKaEI7O0FBTUEsV0FBTyxJQUFJdGhCLFFBQUosQ0FBYTtBQUNsQjlRLE1BQUFBLEVBQUUsRUFBRUEsRUFEYztBQUVsQjhKLE1BQUFBLElBQUksRUFBRUEsSUFGWTtBQUdsQnBVLE1BQUFBLENBQUMsRUFBRUE7QUFIZSxLQUFiLENBQVA7QUFLRDs7QUFFRCxXQUFTMjhCLFlBQVQsQ0FBc0IxUyxLQUF0QixFQUE2QkMsR0FBN0IsRUFBa0NoWSxJQUFsQyxFQUF3QztBQUN0QyxRQUFJL0ksS0FBSyxHQUFHeEQsV0FBVyxDQUFDdU0sSUFBSSxDQUFDL0ksS0FBTixDQUFYLEdBQTBCLElBQTFCLEdBQWlDK0ksSUFBSSxDQUFDL0ksS0FBbEQ7QUFBQSxRQUNJa0MsTUFBTSxHQUFHLFNBQVNBLE1BQVQsQ0FBZ0JtSCxDQUFoQixFQUFtQm5QLElBQW5CLEVBQXlCO0FBQ3BDbVAsTUFBQUEsQ0FBQyxHQUFHN0osT0FBTyxDQUFDNkosQ0FBRCxFQUFJckosS0FBSyxJQUFJK0ksSUFBSSxDQUFDMHFCLFNBQWQsR0FBMEIsQ0FBMUIsR0FBOEIsQ0FBbEMsRUFBcUMsSUFBckMsQ0FBWDtBQUNBLFVBQUl2RyxTQUFTLEdBQUduTSxHQUFHLENBQUN0WCxHQUFKLENBQVF1TCxLQUFSLENBQWNqTSxJQUFkLEVBQW9CZ04sWUFBcEIsQ0FBaUNoTixJQUFqQyxDQUFoQjtBQUNBLGFBQU9ta0IsU0FBUyxDQUFDaHJCLE1BQVYsQ0FBaUJtSCxDQUFqQixFQUFvQm5QLElBQXBCLENBQVA7QUFDRCxLQUxEO0FBQUEsUUFNSWt0QixNQUFNLEdBQUcsU0FBU0EsTUFBVCxDQUFnQmx0QixJQUFoQixFQUFzQjtBQUNqQyxVQUFJNk8sSUFBSSxDQUFDMHFCLFNBQVQsRUFBb0I7QUFDbEIsWUFBSSxDQUFDMVMsR0FBRyxDQUFDaUIsT0FBSixDQUFZbEIsS0FBWixFQUFtQjVtQixJQUFuQixDQUFMLEVBQStCO0FBQzdCLGlCQUFPNm1CLEdBQUcsQ0FBQ2UsT0FBSixDQUFZNW5CLElBQVosRUFBa0I2bkIsSUFBbEIsQ0FBdUJqQixLQUFLLENBQUNnQixPQUFOLENBQWM1bkIsSUFBZCxDQUF2QixFQUE0Q0EsSUFBNUMsRUFBa0RuQixHQUFsRCxDQUFzRG1CLElBQXRELENBQVA7QUFDRCxTQUZELE1BRU8sT0FBTyxDQUFQO0FBQ1IsT0FKRCxNQUlPO0FBQ0wsZUFBTzZtQixHQUFHLENBQUNnQixJQUFKLENBQVNqQixLQUFULEVBQWdCNW1CLElBQWhCLEVBQXNCbkIsR0FBdEIsQ0FBMEJtQixJQUExQixDQUFQO0FBQ0Q7QUFDRixLQWREOztBQWdCQSxRQUFJNk8sSUFBSSxDQUFDN08sSUFBVCxFQUFlO0FBQ2IsYUFBT2dJLE1BQU0sQ0FBQ2tsQixNQUFNLENBQUNyZSxJQUFJLENBQUM3TyxJQUFOLENBQVAsRUFBb0I2TyxJQUFJLENBQUM3TyxJQUF6QixDQUFiO0FBQ0Q7O0FBRUQsU0FBSyxJQUFJOE0sU0FBUyxHQUFHK0IsSUFBSSxDQUFDbkQsS0FBckIsRUFBNEJxQixRQUFRLEdBQUczSixLQUFLLENBQUNDLE9BQU4sQ0FBY3lKLFNBQWQsQ0FBdkMsRUFBaUV5WSxHQUFHLEdBQUcsQ0FBdkUsRUFBMEV6WSxTQUFTLEdBQUdDLFFBQVEsR0FBR0QsU0FBSCxHQUFlQSxTQUFTLENBQUNHLE1BQU0sQ0FBQ0MsUUFBUixDQUFULEVBQWxILElBQWtKO0FBQ2hKLFVBQUlnRixLQUFKOztBQUVBLFVBQUluRixRQUFKLEVBQWM7QUFDWixZQUFJd1ksR0FBRyxJQUFJelksU0FBUyxDQUFDdlIsTUFBckIsRUFBNkI7QUFDN0IyVyxRQUFBQSxLQUFLLEdBQUdwRixTQUFTLENBQUN5WSxHQUFHLEVBQUosQ0FBakI7QUFDRCxPQUhELE1BR087QUFDTEEsUUFBQUEsR0FBRyxHQUFHelksU0FBUyxDQUFDbEosSUFBVixFQUFOO0FBQ0EsWUFBSTJoQixHQUFHLENBQUNuWSxJQUFSLEVBQWM7QUFDZDhFLFFBQUFBLEtBQUssR0FBR3FULEdBQUcsQ0FBQ3ZtQixLQUFaO0FBQ0Q7O0FBRUQsVUFBSWdCLElBQUksR0FBR2tTLEtBQVg7QUFDQSxVQUFJM0csS0FBSyxHQUFHMmhCLE1BQU0sQ0FBQ2x0QixJQUFELENBQWxCOztBQUVBLFVBQUl5RSxJQUFJLENBQUNrRixHQUFMLENBQVM0QixLQUFULEtBQW1CLENBQXZCLEVBQTBCO0FBQ3hCLGVBQU92RCxNQUFNLENBQUN1RCxLQUFELEVBQVF2TCxJQUFSLENBQWI7QUFDRDtBQUNGOztBQUVELFdBQU9nSSxNQUFNLENBQUMsQ0FBRCxFQUFJNkcsSUFBSSxDQUFDbkQsS0FBTCxDQUFXbUQsSUFBSSxDQUFDbkQsS0FBTCxDQUFXblEsTUFBWCxHQUFvQixDQUEvQixDQUFKLENBQWI7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBc0JBLE1BQUl3YyxRQUFRO0FBQ1o7QUFDQSxjQUFZO0FBQ1Y7OztBQUdBLGFBQVNBLFFBQVQsQ0FBa0JnTSxNQUFsQixFQUEwQjtBQUN4QixVQUFJaFQsSUFBSSxHQUFHZ1QsTUFBTSxDQUFDaFQsSUFBUCxJQUFlcUYsUUFBUSxDQUFDUCxXQUFuQztBQUNBLFVBQUlvTyxPQUFPLEdBQUdGLE1BQU0sQ0FBQ0UsT0FBUCxLQUFtQnZiLE1BQU0sQ0FBQ0MsS0FBUCxDQUFhb2IsTUFBTSxDQUFDOWMsRUFBcEIsSUFBMEIsSUFBSXVMLE9BQUosQ0FBWSxlQUFaLENBQTFCLEdBQXlELElBQTVFLE1BQXNGLENBQUN6QixJQUFJLENBQUNELE9BQU4sR0FBZ0JvbEIsZUFBZSxDQUFDbmxCLElBQUQsQ0FBL0IsR0FBd0MsSUFBOUgsQ0FBZDtBQUNBOzs7O0FBSUEsV0FBSzlKLEVBQUwsR0FBVTNFLFdBQVcsQ0FBQ3loQixNQUFNLENBQUM5YyxFQUFSLENBQVgsR0FBeUJtUCxRQUFRLENBQUNMLEdBQVQsRUFBekIsR0FBMENnTyxNQUFNLENBQUM5YyxFQUEzRDtBQUNBLFVBQUlrSSxDQUFDLEdBQUcsSUFBUjtBQUFBLFVBQ0l4UyxDQUFDLEdBQUcsSUFEUjs7QUFHQSxVQUFJLENBQUNzbkIsT0FBTCxFQUFjO0FBQ1osWUFBSXVWLFNBQVMsR0FBR3pWLE1BQU0sQ0FBQ3VTLEdBQVAsSUFBY3ZTLE1BQU0sQ0FBQ3VTLEdBQVAsQ0FBV3J2QixFQUFYLEtBQWtCLEtBQUtBLEVBQXJDLElBQTJDOGMsTUFBTSxDQUFDdVMsR0FBUCxDQUFXdmxCLElBQVgsQ0FBZ0I0QixNQUFoQixDQUF1QjVCLElBQXZCLENBQTNEOztBQUVBLFlBQUl5b0IsU0FBSixFQUFlO0FBQ2IsY0FBSTVQLEtBQUssR0FBRyxDQUFDN0YsTUFBTSxDQUFDdVMsR0FBUCxDQUFXbm5CLENBQVosRUFBZTRVLE1BQU0sQ0FBQ3VTLEdBQVAsQ0FBVzM1QixDQUExQixDQUFaO0FBQ0F3UyxVQUFBQSxDQUFDLEdBQUd5YSxLQUFLLENBQUMsQ0FBRCxDQUFUO0FBQ0FqdEIsVUFBQUEsQ0FBQyxHQUFHaXRCLEtBQUssQ0FBQyxDQUFELENBQVQ7QUFDRCxTQUpELE1BSU87QUFDTHphLFVBQUFBLENBQUMsR0FBRzBuQixPQUFPLENBQUMsS0FBSzV2QixFQUFOLEVBQVU4SixJQUFJLENBQUN2SCxNQUFMLENBQVksS0FBS3ZDLEVBQWpCLENBQVYsQ0FBWDtBQUNBZ2QsVUFBQUEsT0FBTyxHQUFHdmIsTUFBTSxDQUFDQyxLQUFQLENBQWF3RyxDQUFDLENBQUMxTyxJQUFmLElBQXVCLElBQUkrUixPQUFKLENBQVksZUFBWixDQUF2QixHQUFzRCxJQUFoRTtBQUNBckQsVUFBQUEsQ0FBQyxHQUFHOFUsT0FBTyxHQUFHLElBQUgsR0FBVTlVLENBQXJCO0FBQ0F4UyxVQUFBQSxDQUFDLEdBQUdzbkIsT0FBTyxHQUFHLElBQUgsR0FBVWxULElBQUksQ0FBQ3ZILE1BQUwsQ0FBWSxLQUFLdkMsRUFBakIsQ0FBckI7QUFDRDtBQUNGO0FBQ0Q7Ozs7O0FBS0EsV0FBS3d5QixLQUFMLEdBQWExb0IsSUFBYjtBQUNBOzs7O0FBSUEsV0FBS3hCLEdBQUwsR0FBV3dVLE1BQU0sQ0FBQ3hVLEdBQVAsSUFBYytHLE1BQU0sQ0FBQy9aLE1BQVAsRUFBekI7QUFDQTs7OztBQUlBLFdBQUswbkIsT0FBTCxHQUFlQSxPQUFmO0FBQ0E7Ozs7QUFJQSxXQUFLd1EsUUFBTCxHQUFnQixJQUFoQjtBQUNBOzs7O0FBSUEsV0FBS3RsQixDQUFMLEdBQVNBLENBQVQ7QUFDQTs7OztBQUlBLFdBQUt4UyxDQUFMLEdBQVNBLENBQVQ7QUFDQTs7OztBQUlBLFdBQUsrOEIsZUFBTCxHQUF1QixJQUF2QjtBQUNELEtBakVTLENBaUVSOztBQUVGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkEzaEIsSUFBQUEsUUFBUSxDQUFDNEcsS0FBVCxHQUFpQixTQUFTQSxLQUFULENBQWVsZSxJQUFmLEVBQXFCQyxLQUFyQixFQUE0QkMsR0FBNUIsRUFBaUNNLElBQWpDLEVBQXVDQyxNQUF2QyxFQUErQ0UsTUFBL0MsRUFBdURtRixXQUF2RCxFQUFvRTtBQUNuRixVQUFJakUsV0FBVyxDQUFDN0IsSUFBRCxDQUFmLEVBQXVCO0FBQ3JCLGVBQU8sSUFBSXNYLFFBQUosQ0FBYTtBQUNsQjlRLFVBQUFBLEVBQUUsRUFBRW1QLFFBQVEsQ0FBQ0wsR0FBVDtBQURjLFNBQWIsQ0FBUDtBQUdELE9BSkQsTUFJTztBQUNMLGVBQU9takIsT0FBTyxDQUFDO0FBQ2J6NEIsVUFBQUEsSUFBSSxFQUFFQSxJQURPO0FBRWJDLFVBQUFBLEtBQUssRUFBRUEsS0FGTTtBQUdiQyxVQUFBQSxHQUFHLEVBQUVBLEdBSFE7QUFJYk0sVUFBQUEsSUFBSSxFQUFFQSxJQUpPO0FBS2JDLFVBQUFBLE1BQU0sRUFBRUEsTUFMSztBQU1iRSxVQUFBQSxNQUFNLEVBQUVBLE1BTks7QUFPYm1GLFVBQUFBLFdBQVcsRUFBRUE7QUFQQSxTQUFELEVBUVg2UCxRQUFRLENBQUNQLFdBUkUsQ0FBZDtBQVNEO0FBQ0Y7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWpCQTs7QUFzQ0FrQyxJQUFBQSxRQUFRLENBQUNDLEdBQVQsR0FBZSxTQUFTQSxHQUFULENBQWF2WCxJQUFiLEVBQW1CQyxLQUFuQixFQUEwQkMsR0FBMUIsRUFBK0JNLElBQS9CLEVBQXFDQyxNQUFyQyxFQUE2Q0UsTUFBN0MsRUFBcURtRixXQUFyRCxFQUFrRTtBQUMvRSxVQUFJakUsV0FBVyxDQUFDN0IsSUFBRCxDQUFmLEVBQXVCO0FBQ3JCLGVBQU8sSUFBSXNYLFFBQUosQ0FBYTtBQUNsQjlRLFVBQUFBLEVBQUUsRUFBRW1QLFFBQVEsQ0FBQ0wsR0FBVCxFQURjO0FBRWxCaEYsVUFBQUEsSUFBSSxFQUFFc0UsZUFBZSxDQUFDQztBQUZKLFNBQWIsQ0FBUDtBQUlELE9BTEQsTUFLTztBQUNMLGVBQU80akIsT0FBTyxDQUFDO0FBQ2J6NEIsVUFBQUEsSUFBSSxFQUFFQSxJQURPO0FBRWJDLFVBQUFBLEtBQUssRUFBRUEsS0FGTTtBQUdiQyxVQUFBQSxHQUFHLEVBQUVBLEdBSFE7QUFJYk0sVUFBQUEsSUFBSSxFQUFFQSxJQUpPO0FBS2JDLFVBQUFBLE1BQU0sRUFBRUEsTUFMSztBQU1iRSxVQUFBQSxNQUFNLEVBQUVBLE1BTks7QUFPYm1GLFVBQUFBLFdBQVcsRUFBRUE7QUFQQSxTQUFELEVBUVg4TyxlQUFlLENBQUNDLFdBUkwsQ0FBZDtBQVNEO0FBQ0Y7QUFDRDs7Ozs7OztBQWxCQTs7QUEyQkF5QyxJQUFBQSxRQUFRLENBQUM0aEIsVUFBVCxHQUFzQixTQUFTQSxVQUFULENBQW9CdHlCLElBQXBCLEVBQTBCbVEsT0FBMUIsRUFBbUM7QUFDdkQsVUFBSUEsT0FBTyxLQUFLLEtBQUssQ0FBckIsRUFBd0I7QUFDdEJBLFFBQUFBLE9BQU8sR0FBRyxFQUFWO0FBQ0Q7O0FBRUQsVUFBSXZRLEVBQUUsR0FBR3ZFLE1BQU0sQ0FBQzJFLElBQUQsQ0FBTixHQUFlQSxJQUFJLENBQUM4TixPQUFMLEVBQWYsR0FBZ0NRLEdBQXpDOztBQUVBLFVBQUlqTixNQUFNLENBQUNDLEtBQVAsQ0FBYTFCLEVBQWIsQ0FBSixFQUFzQjtBQUNwQixlQUFPOFEsUUFBUSxDQUFDa00sT0FBVCxDQUFpQixlQUFqQixDQUFQO0FBQ0Q7O0FBRUQsVUFBSTJWLFNBQVMsR0FBR2hrQixhQUFhLENBQUM0QixPQUFPLENBQUN6RyxJQUFULEVBQWVxRixRQUFRLENBQUNQLFdBQXhCLENBQTdCOztBQUVBLFVBQUksQ0FBQytqQixTQUFTLENBQUM5b0IsT0FBZixFQUF3QjtBQUN0QixlQUFPaUgsUUFBUSxDQUFDa00sT0FBVCxDQUFpQmlTLGVBQWUsQ0FBQzBELFNBQUQsQ0FBaEMsQ0FBUDtBQUNEOztBQUVELGFBQU8sSUFBSTdoQixRQUFKLENBQWE7QUFDbEI5USxRQUFBQSxFQUFFLEVBQUVBLEVBRGM7QUFFbEI4SixRQUFBQSxJQUFJLEVBQUU2b0IsU0FGWTtBQUdsQnJxQixRQUFBQSxHQUFHLEVBQUUrRyxNQUFNLENBQUN3RCxVQUFQLENBQWtCdEMsT0FBbEI7QUFIYSxPQUFiLENBQVA7QUFLRDtBQUNEOzs7Ozs7Ozs7O0FBdkJBOztBQW1DQU8sSUFBQUEsUUFBUSxDQUFDZ0IsVUFBVCxHQUFzQixTQUFTQSxVQUFULENBQW9CeUcsWUFBcEIsRUFBa0NoSSxPQUFsQyxFQUEyQztBQUMvRCxVQUFJQSxPQUFPLEtBQUssS0FBSyxDQUFyQixFQUF3QjtBQUN0QkEsUUFBQUEsT0FBTyxHQUFHLEVBQVY7QUFDRDs7QUFFRCxVQUFJLENBQUNqVixRQUFRLENBQUNpZCxZQUFELENBQWIsRUFBNkI7QUFDM0IsY0FBTSxJQUFJdmYsb0JBQUosQ0FBeUIsdUNBQXpCLENBQU47QUFDRCxPQUZELE1BRU8sSUFBSXVmLFlBQVksR0FBRyxDQUFDeVcsUUFBaEIsSUFBNEJ6VyxZQUFZLEdBQUd5VyxRQUEvQyxFQUF5RDtBQUM5RDtBQUNBLGVBQU9sZSxRQUFRLENBQUNrTSxPQUFULENBQWlCLHdCQUFqQixDQUFQO0FBQ0QsT0FITSxNQUdBO0FBQ0wsZUFBTyxJQUFJbE0sUUFBSixDQUFhO0FBQ2xCOVEsVUFBQUEsRUFBRSxFQUFFdVksWUFEYztBQUVsQnpPLFVBQUFBLElBQUksRUFBRTZFLGFBQWEsQ0FBQzRCLE9BQU8sQ0FBQ3pHLElBQVQsRUFBZXFGLFFBQVEsQ0FBQ1AsV0FBeEIsQ0FGRDtBQUdsQnRHLFVBQUFBLEdBQUcsRUFBRStHLE1BQU0sQ0FBQ3dELFVBQVAsQ0FBa0J0QyxPQUFsQjtBQUhhLFNBQWIsQ0FBUDtBQUtEO0FBQ0Y7QUFDRDs7Ozs7Ozs7OztBQWxCQTs7QUE4QkFPLElBQUFBLFFBQVEsQ0FBQzhoQixXQUFULEdBQXVCLFNBQVNBLFdBQVQsQ0FBcUI5dEIsT0FBckIsRUFBOEJ5TCxPQUE5QixFQUF1QztBQUM1RCxVQUFJQSxPQUFPLEtBQUssS0FBSyxDQUFyQixFQUF3QjtBQUN0QkEsUUFBQUEsT0FBTyxHQUFHLEVBQVY7QUFDRDs7QUFFRCxVQUFJLENBQUNqVixRQUFRLENBQUN3SixPQUFELENBQWIsRUFBd0I7QUFDdEIsY0FBTSxJQUFJOUwsb0JBQUosQ0FBeUIsd0NBQXpCLENBQU47QUFDRCxPQUZELE1BRU87QUFDTCxlQUFPLElBQUk4WCxRQUFKLENBQWE7QUFDbEI5USxVQUFBQSxFQUFFLEVBQUU4RSxPQUFPLEdBQUcsSUFESTtBQUVsQmdGLFVBQUFBLElBQUksRUFBRTZFLGFBQWEsQ0FBQzRCLE9BQU8sQ0FBQ3pHLElBQVQsRUFBZXFGLFFBQVEsQ0FBQ1AsV0FBeEIsQ0FGRDtBQUdsQnRHLFVBQUFBLEdBQUcsRUFBRStHLE1BQU0sQ0FBQ3dELFVBQVAsQ0FBa0J0QyxPQUFsQjtBQUhhLFNBQWIsQ0FBUDtBQUtEO0FBQ0Y7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZkE7O0FBNENBTyxJQUFBQSxRQUFRLENBQUMrQixVQUFULEdBQXNCLFNBQVNBLFVBQVQsQ0FBb0IvVixHQUFwQixFQUF5QjtBQUM3QyxVQUFJNjFCLFNBQVMsR0FBR2hrQixhQUFhLENBQUM3UixHQUFHLENBQUNnTixJQUFMLEVBQVdxRixRQUFRLENBQUNQLFdBQXBCLENBQTdCOztBQUVBLFVBQUksQ0FBQytqQixTQUFTLENBQUM5b0IsT0FBZixFQUF3QjtBQUN0QixlQUFPaUgsUUFBUSxDQUFDa00sT0FBVCxDQUFpQmlTLGVBQWUsQ0FBQzBELFNBQUQsQ0FBaEMsQ0FBUDtBQUNEOztBQUVELFVBQUlULEtBQUssR0FBRy9pQixRQUFRLENBQUNMLEdBQVQsRUFBWjtBQUFBLFVBQ0lxakIsWUFBWSxHQUFHUSxTQUFTLENBQUNwd0IsTUFBVixDQUFpQjJ2QixLQUFqQixDQURuQjtBQUFBLFVBRUkvdkIsVUFBVSxHQUFHSCxlQUFlLENBQUNsRixHQUFELEVBQU1vZ0IsYUFBTixFQUFxQixDQUFDLE1BQUQsRUFBUyxRQUFULEVBQW1CLGdCQUFuQixFQUFxQyxpQkFBckMsQ0FBckIsQ0FGaEM7QUFBQSxVQUdJMlYsZUFBZSxHQUFHLENBQUN4M0IsV0FBVyxDQUFDOEcsVUFBVSxDQUFDbUksT0FBWixDQUhsQztBQUFBLFVBSUl3b0Isa0JBQWtCLEdBQUcsQ0FBQ3ozQixXQUFXLENBQUM4RyxVQUFVLENBQUMzSSxJQUFaLENBSnJDO0FBQUEsVUFLSXU1QixnQkFBZ0IsR0FBRyxDQUFDMTNCLFdBQVcsQ0FBQzhHLFVBQVUsQ0FBQzFJLEtBQVosQ0FBWixJQUFrQyxDQUFDNEIsV0FBVyxDQUFDOEcsVUFBVSxDQUFDekksR0FBWixDQUxyRTtBQUFBLFVBTUlzNUIsY0FBYyxHQUFHRixrQkFBa0IsSUFBSUMsZ0JBTjNDO0FBQUEsVUFPSUUsZUFBZSxHQUFHOXdCLFVBQVUsQ0FBQ3pDLFFBQVgsSUFBdUJ5QyxVQUFVLENBQUNrSSxVQVB4RDtBQUFBLFVBUUkvQixHQUFHLEdBQUcrRyxNQUFNLENBQUN3RCxVQUFQLENBQWtCL1YsR0FBbEIsQ0FSVixDQVA2QyxDQWVYO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFVBQUksQ0FBQ2syQixjQUFjLElBQUlILGVBQW5CLEtBQXVDSSxlQUEzQyxFQUE0RDtBQUMxRCxjQUFNLElBQUl0NkIsNkJBQUosQ0FBa0MscUVBQWxDLENBQU47QUFDRDs7QUFFRCxVQUFJbzZCLGdCQUFnQixJQUFJRixlQUF4QixFQUF5QztBQUN2QyxjQUFNLElBQUlsNkIsNkJBQUosQ0FBa0Msd0NBQWxDLENBQU47QUFDRDs7QUFFRCxVQUFJdTZCLFdBQVcsR0FBR0QsZUFBZSxJQUFJOXdCLFVBQVUsQ0FBQ3JJLE9BQVgsSUFBc0IsQ0FBQ2s1QixjQUE1RCxDQTdCNkMsQ0E2QitCOztBQUU1RSxVQUFJdnVCLEtBQUo7QUFBQSxVQUNJMHVCLGFBREo7QUFBQSxVQUVJQyxNQUFNLEdBQUd4RCxPQUFPLENBQUNzQyxLQUFELEVBQVFDLFlBQVIsQ0FGcEI7O0FBSUEsVUFBSWUsV0FBSixFQUFpQjtBQUNmenVCLFFBQUFBLEtBQUssR0FBR2l0QixnQkFBUjtBQUNBeUIsUUFBQUEsYUFBYSxHQUFHNUIscUJBQWhCO0FBQ0E2QixRQUFBQSxNQUFNLEdBQUcvRixlQUFlLENBQUMrRixNQUFELENBQXhCO0FBQ0QsT0FKRCxNQUlPLElBQUlQLGVBQUosRUFBcUI7QUFDMUJwdUIsUUFBQUEsS0FBSyxHQUFHa3RCLG1CQUFSO0FBQ0F3QixRQUFBQSxhQUFhLEdBQUczQix3QkFBaEI7QUFDQTRCLFFBQUFBLE1BQU0sR0FBR3hGLGtCQUFrQixDQUFDd0YsTUFBRCxDQUEzQjtBQUNELE9BSk0sTUFJQTtBQUNMM3VCLFFBQUFBLEtBQUssR0FBR2d0QixjQUFSO0FBQ0EwQixRQUFBQSxhQUFhLEdBQUc3QixpQkFBaEI7QUFDRCxPQTlDNEMsQ0E4QzNDOzs7QUFHRixVQUFJK0IsVUFBVSxHQUFHLEtBQWpCOztBQUVBLFdBQUssSUFBSUMsVUFBVSxHQUFHN3VCLEtBQWpCLEVBQXdCOHVCLFNBQVMsR0FBR3AzQixLQUFLLENBQUNDLE9BQU4sQ0FBY2szQixVQUFkLENBQXBDLEVBQStEeFUsR0FBRyxHQUFHLENBQXJFLEVBQXdFd1UsVUFBVSxHQUFHQyxTQUFTLEdBQUdELFVBQUgsR0FBZ0JBLFVBQVUsQ0FBQ3R0QixNQUFNLENBQUNDLFFBQVIsQ0FBVixFQUFuSCxJQUFvSjtBQUNsSixZQUFJZ2QsS0FBSjs7QUFFQSxZQUFJc1EsU0FBSixFQUFlO0FBQ2IsY0FBSXpVLEdBQUcsSUFBSXdVLFVBQVUsQ0FBQ2gvQixNQUF0QixFQUE4QjtBQUM5QjJ1QixVQUFBQSxLQUFLLEdBQUdxUSxVQUFVLENBQUN4VSxHQUFHLEVBQUosQ0FBbEI7QUFDRCxTQUhELE1BR087QUFDTEEsVUFBQUEsR0FBRyxHQUFHd1UsVUFBVSxDQUFDMzJCLElBQVgsRUFBTjtBQUNBLGNBQUltaUIsR0FBRyxDQUFDM1ksSUFBUixFQUFjO0FBQ2Q4YyxVQUFBQSxLQUFLLEdBQUduRSxHQUFHLENBQUMvbUIsS0FBWjtBQUNEOztBQUVELFlBQUlxSyxDQUFDLEdBQUc2Z0IsS0FBUjtBQUNBLFlBQUk1Z0IsQ0FBQyxHQUFHRixVQUFVLENBQUNDLENBQUQsQ0FBbEI7O0FBRUEsWUFBSSxDQUFDL0csV0FBVyxDQUFDZ0gsQ0FBRCxDQUFoQixFQUFxQjtBQUNuQmd4QixVQUFBQSxVQUFVLEdBQUcsSUFBYjtBQUNELFNBRkQsTUFFTyxJQUFJQSxVQUFKLEVBQWdCO0FBQ3JCbHhCLFVBQUFBLFVBQVUsQ0FBQ0MsQ0FBRCxDQUFWLEdBQWdCK3dCLGFBQWEsQ0FBQy93QixDQUFELENBQTdCO0FBQ0QsU0FGTSxNQUVBO0FBQ0xELFVBQUFBLFVBQVUsQ0FBQ0MsQ0FBRCxDQUFWLEdBQWdCZ3hCLE1BQU0sQ0FBQ2h4QixDQUFELENBQXRCO0FBQ0Q7QUFDRixPQXpFNEMsQ0F5RTNDOzs7QUFHRixVQUFJb3hCLGtCQUFrQixHQUFHTixXQUFXLEdBQUdqRixrQkFBa0IsQ0FBQzlyQixVQUFELENBQXJCLEdBQW9DMHdCLGVBQWUsR0FBR3hFLHFCQUFxQixDQUFDbHNCLFVBQUQsQ0FBeEIsR0FBdUNvc0IsdUJBQXVCLENBQUNwc0IsVUFBRCxDQUFySjtBQUFBLFVBQ0k2YSxPQUFPLEdBQUd3VyxrQkFBa0IsSUFBSTlFLGtCQUFrQixDQUFDdnNCLFVBQUQsQ0FEdEQ7O0FBR0EsVUFBSTZhLE9BQUosRUFBYTtBQUNYLGVBQU9sTSxRQUFRLENBQUNrTSxPQUFULENBQWlCQSxPQUFqQixDQUFQO0FBQ0QsT0FqRjRDLENBaUYzQzs7O0FBR0YsVUFBSXlXLFNBQVMsR0FBR1AsV0FBVyxHQUFHM0YsZUFBZSxDQUFDcHJCLFVBQUQsQ0FBbEIsR0FBaUMwd0IsZUFBZSxHQUFHL0Usa0JBQWtCLENBQUMzckIsVUFBRCxDQUFyQixHQUFvQ0EsVUFBL0c7QUFBQSxVQUNJdXhCLFNBQVMsR0FBR3ZELE9BQU8sQ0FBQ3NELFNBQUQsRUFBWXRCLFlBQVosRUFBMEJRLFNBQTFCLENBRHZCO0FBQUEsVUFFSWdCLE9BQU8sR0FBR0QsU0FBUyxDQUFDLENBQUQsQ0FGdkI7QUFBQSxVQUdJRSxXQUFXLEdBQUdGLFNBQVMsQ0FBQyxDQUFELENBSDNCO0FBQUEsVUFJSXRFLElBQUksR0FBRyxJQUFJdGUsUUFBSixDQUFhO0FBQ3RCOVEsUUFBQUEsRUFBRSxFQUFFMnpCLE9BRGtCO0FBRXRCN3BCLFFBQUFBLElBQUksRUFBRTZvQixTQUZnQjtBQUd0Qmo5QixRQUFBQSxDQUFDLEVBQUVrK0IsV0FIbUI7QUFJdEJ0ckIsUUFBQUEsR0FBRyxFQUFFQTtBQUppQixPQUFiLENBSlgsQ0FwRjZDLENBNkZ6Qzs7O0FBR0osVUFBSW5HLFVBQVUsQ0FBQ3JJLE9BQVgsSUFBc0JrNUIsY0FBdEIsSUFBd0NsMkIsR0FBRyxDQUFDaEQsT0FBSixLQUFnQnMxQixJQUFJLENBQUN0MUIsT0FBakUsRUFBMEU7QUFDeEUsZUFBT2dYLFFBQVEsQ0FBQ2tNLE9BQVQsQ0FBaUIsb0JBQWpCLEVBQXVDLHlDQUF5QzdhLFVBQVUsQ0FBQ3JJLE9BQXBELEdBQThELGlCQUE5RCxHQUFrRnMxQixJQUFJLENBQUN4UixLQUFMLEVBQXpILENBQVA7QUFDRDs7QUFFRCxhQUFPd1IsSUFBUDtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7QUF0R0E7O0FBd0hBdGUsSUFBQUEsUUFBUSxDQUFDcU0sT0FBVCxHQUFtQixTQUFTQSxPQUFULENBQWlCQyxJQUFqQixFQUF1QnhWLElBQXZCLEVBQTZCO0FBQzlDLFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELFVBQUlpc0IsYUFBYSxHQUFHclosWUFBWSxDQUFDNEMsSUFBRCxDQUFoQztBQUFBLFVBQ0lSLElBQUksR0FBR2lYLGFBQWEsQ0FBQyxDQUFELENBRHhCO0FBQUEsVUFFSXBELFVBQVUsR0FBR29ELGFBQWEsQ0FBQyxDQUFELENBRjlCOztBQUlBLGFBQU9yRCxtQkFBbUIsQ0FBQzVULElBQUQsRUFBTzZULFVBQVAsRUFBbUI3b0IsSUFBbkIsRUFBeUIsVUFBekIsRUFBcUN3VixJQUFyQyxDQUExQjtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7O0FBWEE7O0FBMkJBdE0sSUFBQUEsUUFBUSxDQUFDZ2pCLFdBQVQsR0FBdUIsU0FBU0EsV0FBVCxDQUFxQjFXLElBQXJCLEVBQTJCeFYsSUFBM0IsRUFBaUM7QUFDdEQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsVUFBSW1zQixpQkFBaUIsR0FBR3RaLGdCQUFnQixDQUFDMkMsSUFBRCxDQUF4QztBQUFBLFVBQ0lSLElBQUksR0FBR21YLGlCQUFpQixDQUFDLENBQUQsQ0FENUI7QUFBQSxVQUVJdEQsVUFBVSxHQUFHc0QsaUJBQWlCLENBQUMsQ0FBRCxDQUZsQzs7QUFJQSxhQUFPdkQsbUJBQW1CLENBQUM1VCxJQUFELEVBQU82VCxVQUFQLEVBQW1CN29CLElBQW5CLEVBQXlCLFVBQXpCLEVBQXFDd1YsSUFBckMsQ0FBMUI7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7QUFYQTs7QUE0QkF0TSxJQUFBQSxRQUFRLENBQUNrakIsUUFBVCxHQUFvQixTQUFTQSxRQUFULENBQWtCNVcsSUFBbEIsRUFBd0J4VixJQUF4QixFQUE4QjtBQUNoRCxVQUFJQSxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxVQUFJcXNCLGNBQWMsR0FBR3ZaLGFBQWEsQ0FBQzBDLElBQUQsQ0FBbEM7QUFBQSxVQUNJUixJQUFJLEdBQUdxWCxjQUFjLENBQUMsQ0FBRCxDQUR6QjtBQUFBLFVBRUl4RCxVQUFVLEdBQUd3RCxjQUFjLENBQUMsQ0FBRCxDQUYvQjs7QUFJQSxhQUFPekQsbUJBQW1CLENBQUM1VCxJQUFELEVBQU82VCxVQUFQLEVBQW1CN29CLElBQW5CLEVBQXlCLE1BQXpCLEVBQWlDQSxJQUFqQyxDQUExQjtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7O0FBWEE7O0FBMkJBa0osSUFBQUEsUUFBUSxDQUFDb2pCLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQjlXLElBQXBCLEVBQTBCdFYsR0FBMUIsRUFBK0JGLElBQS9CLEVBQXFDO0FBQ3pELFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELFVBQUl2TSxXQUFXLENBQUMraEIsSUFBRCxDQUFYLElBQXFCL2hCLFdBQVcsQ0FBQ3lNLEdBQUQsQ0FBcEMsRUFBMkM7QUFDekMsY0FBTSxJQUFJOU8sb0JBQUosQ0FBeUIsa0RBQXpCLENBQU47QUFDRDs7QUFFRCxVQUFJbTdCLEtBQUssR0FBR3ZzQixJQUFaO0FBQUEsVUFDSXdzQixZQUFZLEdBQUdELEtBQUssQ0FBQ2owQixNQUR6QjtBQUFBLFVBRUlBLE1BQU0sR0FBR2swQixZQUFZLEtBQUssS0FBSyxDQUF0QixHQUEwQixJQUExQixHQUFpQ0EsWUFGOUM7QUFBQSxVQUdJQyxxQkFBcUIsR0FBR0YsS0FBSyxDQUFDNWtCLGVBSGxDO0FBQUEsVUFJSUEsZUFBZSxHQUFHOGtCLHFCQUFxQixLQUFLLEtBQUssQ0FBL0IsR0FBbUMsSUFBbkMsR0FBMENBLHFCQUpoRTtBQUFBLFVBS0lDLFdBQVcsR0FBR2psQixNQUFNLENBQUNrRCxRQUFQLENBQWdCO0FBQ2hDclMsUUFBQUEsTUFBTSxFQUFFQSxNQUR3QjtBQUVoQ3FQLFFBQUFBLGVBQWUsRUFBRUEsZUFGZTtBQUdoQ2lELFFBQUFBLFdBQVcsRUFBRTtBQUhtQixPQUFoQixDQUxsQjtBQUFBLFVBVUkraEIsZ0JBQWdCLEdBQUc5SCxlQUFlLENBQUM2SCxXQUFELEVBQWNsWCxJQUFkLEVBQW9CdFYsR0FBcEIsQ0FWdEM7QUFBQSxVQVdJOFUsSUFBSSxHQUFHMlgsZ0JBQWdCLENBQUMsQ0FBRCxDQVgzQjtBQUFBLFVBWUk5RCxVQUFVLEdBQUc4RCxnQkFBZ0IsQ0FBQyxDQUFELENBWmpDO0FBQUEsVUFhSXZYLE9BQU8sR0FBR3VYLGdCQUFnQixDQUFDLENBQUQsQ0FiOUI7O0FBZUEsVUFBSXZYLE9BQUosRUFBYTtBQUNYLGVBQU9sTSxRQUFRLENBQUNrTSxPQUFULENBQWlCQSxPQUFqQixDQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBT3dULG1CQUFtQixDQUFDNVQsSUFBRCxFQUFPNlQsVUFBUCxFQUFtQjdvQixJQUFuQixFQUF5QixZQUFZRSxHQUFyQyxFQUEwQ3NWLElBQTFDLENBQTFCO0FBQ0Q7QUFDRjtBQUNEOzs7QUE5QkE7O0FBbUNBdE0sSUFBQUEsUUFBUSxDQUFDMGpCLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQnBYLElBQXBCLEVBQTBCdFYsR0FBMUIsRUFBK0JGLElBQS9CLEVBQXFDO0FBQ3pELFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELGFBQU9rSixRQUFRLENBQUNvakIsVUFBVCxDQUFvQjlXLElBQXBCLEVBQTBCdFYsR0FBMUIsRUFBK0JGLElBQS9CLENBQVA7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVBBOztBQTZCQWtKLElBQUFBLFFBQVEsQ0FBQzJqQixPQUFULEdBQW1CLFNBQVNBLE9BQVQsQ0FBaUJyWCxJQUFqQixFQUF1QnhWLElBQXZCLEVBQTZCO0FBQzlDLFVBQUlBLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsRUFBUDtBQUNEOztBQUVELFVBQUk4c0IsU0FBUyxHQUFHMVosUUFBUSxDQUFDb0MsSUFBRCxDQUF4QjtBQUFBLFVBQ0lSLElBQUksR0FBRzhYLFNBQVMsQ0FBQyxDQUFELENBRHBCO0FBQUEsVUFFSWpFLFVBQVUsR0FBR2lFLFNBQVMsQ0FBQyxDQUFELENBRjFCOztBQUlBLGFBQU9sRSxtQkFBbUIsQ0FBQzVULElBQUQsRUFBTzZULFVBQVAsRUFBbUI3b0IsSUFBbkIsRUFBeUIsS0FBekIsRUFBZ0N3VixJQUFoQyxDQUExQjtBQUNEO0FBQ0Q7Ozs7OztBQVhBOztBQW1CQXRNLElBQUFBLFFBQVEsQ0FBQ2tNLE9BQVQsR0FBbUIsU0FBU0EsT0FBVCxDQUFpQjNrQixNQUFqQixFQUF5Qm1ULFdBQXpCLEVBQXNDO0FBQ3ZELFVBQUlBLFdBQVcsS0FBSyxLQUFLLENBQXpCLEVBQTRCO0FBQzFCQSxRQUFBQSxXQUFXLEdBQUcsSUFBZDtBQUNEOztBQUVELFVBQUksQ0FBQ25ULE1BQUwsRUFBYTtBQUNYLGNBQU0sSUFBSVcsb0JBQUosQ0FBeUIsa0RBQXpCLENBQU47QUFDRDs7QUFFRCxVQUFJZ2tCLE9BQU8sR0FBRzNrQixNQUFNLFlBQVlrVCxPQUFsQixHQUE0QmxULE1BQTVCLEdBQXFDLElBQUlrVCxPQUFKLENBQVlsVCxNQUFaLEVBQW9CbVQsV0FBcEIsQ0FBbkQ7O0FBRUEsVUFBSTJELFFBQVEsQ0FBQ0QsY0FBYixFQUE2QjtBQUMzQixjQUFNLElBQUkvVyxvQkFBSixDQUF5QjZrQixPQUF6QixDQUFOO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBTyxJQUFJbE0sUUFBSixDQUFhO0FBQ2xCa00sVUFBQUEsT0FBTyxFQUFFQTtBQURTLFNBQWIsQ0FBUDtBQUdEO0FBQ0Y7QUFDRDs7Ozs7QUFuQkE7O0FBMEJBbE0sSUFBQUEsUUFBUSxDQUFDNmpCLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQmovQixDQUFwQixFQUF1QjtBQUMzQyxhQUFPQSxDQUFDLElBQUlBLENBQUMsQ0FBQys4QixlQUFQLElBQTBCLEtBQWpDO0FBQ0QsS0FGRCxDQUVFOztBQUVGOzs7Ozs7O0FBSkE7O0FBYUEsUUFBSWpxQixNQUFNLEdBQUdzSSxRQUFRLENBQUM1YixTQUF0Qjs7QUFFQXNULElBQUFBLE1BQU0sQ0FBQzVRLEdBQVAsR0FBYSxTQUFTQSxHQUFULENBQWFtQixJQUFiLEVBQW1CO0FBQzlCLGFBQU8sS0FBS0EsSUFBTCxDQUFQO0FBQ0Q7QUFDRDs7Ozs7O0FBSEE7QUFXQTs7Ozs7Ozs7QUFNQXlQLElBQUFBLE1BQU0sQ0FBQ29zQixrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxDQUE0Qmh0QixJQUE1QixFQUFrQztBQUM1RCxVQUFJQSxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxVQUFJaXRCLHFCQUFxQixHQUFHbHRCLFNBQVMsQ0FBQ3JTLE1BQVYsQ0FBaUIsS0FBS2dULEdBQUwsQ0FBU3VMLEtBQVQsQ0FBZWpNLElBQWYsQ0FBakIsRUFBdUNBLElBQXZDLEVBQTZDbUIsZUFBN0MsQ0FBNkQsSUFBN0QsQ0FBNUI7QUFBQSxVQUNJN0ksTUFBTSxHQUFHMjBCLHFCQUFxQixDQUFDMzBCLE1BRG5DO0FBQUEsVUFFSXFQLGVBQWUsR0FBR3NsQixxQkFBcUIsQ0FBQ3RsQixlQUY1QztBQUFBLFVBR0ltQixRQUFRLEdBQUdta0IscUJBQXFCLENBQUNua0IsUUFIckM7O0FBS0EsYUFBTztBQUNMeFEsUUFBQUEsTUFBTSxFQUFFQSxNQURIO0FBRUxxUCxRQUFBQSxlQUFlLEVBQUVBLGVBRlo7QUFHTDlGLFFBQUFBLGNBQWMsRUFBRWlIO0FBSFgsT0FBUDtBQUtELEtBZkQsQ0FlRTs7QUFFRjs7Ozs7Ozs7QUFqQkE7O0FBMkJBbEksSUFBQUEsTUFBTSxDQUFDaWQsS0FBUCxHQUFlLFNBQVNBLEtBQVQsQ0FBZWxqQixNQUFmLEVBQXVCcUYsSUFBdkIsRUFBNkI7QUFDMUMsVUFBSXJGLE1BQU0sS0FBSyxLQUFLLENBQXBCLEVBQXVCO0FBQ3JCQSxRQUFBQSxNQUFNLEdBQUcsQ0FBVDtBQUNEOztBQUVELFVBQUlxRixJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUs4YixPQUFMLENBQWF0VixlQUFlLENBQUNuWCxRQUFoQixDQUF5QnNMLE1BQXpCLENBQWIsRUFBK0NxRixJQUEvQyxDQUFQO0FBQ0Q7QUFDRDs7Ozs7O0FBWEE7O0FBbUJBWSxJQUFBQSxNQUFNLENBQUNzc0IsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDLGFBQU8sS0FBS3BSLE9BQUwsQ0FBYXZVLFFBQVEsQ0FBQ1AsV0FBdEIsQ0FBUDtBQUNEO0FBQ0Q7Ozs7Ozs7OztBQUhBOztBQWNBcEcsSUFBQUEsTUFBTSxDQUFDa2IsT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWlCNVosSUFBakIsRUFBdUJnSixLQUF2QixFQUE4QjtBQUM3QyxVQUFJOFIsS0FBSyxHQUFHOVIsS0FBSyxLQUFLLEtBQUssQ0FBZixHQUFtQixFQUFuQixHQUF3QkEsS0FBcEM7QUFBQSxVQUNJaWlCLG1CQUFtQixHQUFHblEsS0FBSyxDQUFDYyxhQURoQztBQUFBLFVBRUlBLGFBQWEsR0FBR3FQLG1CQUFtQixLQUFLLEtBQUssQ0FBN0IsR0FBaUMsS0FBakMsR0FBeUNBLG1CQUY3RDtBQUFBLFVBR0lDLHFCQUFxQixHQUFHcFEsS0FBSyxDQUFDcVEsZ0JBSGxDO0FBQUEsVUFJSUEsZ0JBQWdCLEdBQUdELHFCQUFxQixLQUFLLEtBQUssQ0FBL0IsR0FBbUMsS0FBbkMsR0FBMkNBLHFCQUpsRTs7QUFNQWxyQixNQUFBQSxJQUFJLEdBQUc2RSxhQUFhLENBQUM3RSxJQUFELEVBQU9xRixRQUFRLENBQUNQLFdBQWhCLENBQXBCOztBQUVBLFVBQUk5RSxJQUFJLENBQUM0QixNQUFMLENBQVksS0FBSzVCLElBQWpCLENBQUosRUFBNEI7QUFDMUIsZUFBTyxJQUFQO0FBQ0QsT0FGRCxNQUVPLElBQUksQ0FBQ0EsSUFBSSxDQUFDRCxPQUFWLEVBQW1CO0FBQ3hCLGVBQU9pSCxRQUFRLENBQUNrTSxPQUFULENBQWlCaVMsZUFBZSxDQUFDbmxCLElBQUQsQ0FBaEMsQ0FBUDtBQUNELE9BRk0sTUFFQTtBQUNMLFlBQUlvckIsS0FBSyxHQUFHLEtBQUtsMUIsRUFBakI7O0FBRUEsWUFBSTBsQixhQUFhLElBQUl1UCxnQkFBckIsRUFBdUM7QUFDckMsY0FBSUUsV0FBVyxHQUFHLEtBQUt6L0IsQ0FBTCxHQUFTb1UsSUFBSSxDQUFDdkgsTUFBTCxDQUFZLEtBQUt2QyxFQUFqQixDQUEzQjtBQUNBLGNBQUlvMUIsS0FBSyxHQUFHLEtBQUsxWCxRQUFMLEVBQVo7O0FBRUEsY0FBSTJYLFNBQVMsR0FBR2xGLE9BQU8sQ0FBQ2lGLEtBQUQsRUFBUUQsV0FBUixFQUFxQnJyQixJQUFyQixDQUF2Qjs7QUFFQW9yQixVQUFBQSxLQUFLLEdBQUdHLFNBQVMsQ0FBQyxDQUFELENBQWpCO0FBQ0Q7O0FBRUQsZUFBT2xHLE9BQU8sQ0FBQyxJQUFELEVBQU87QUFDbkJudkIsVUFBQUEsRUFBRSxFQUFFazFCLEtBRGU7QUFFbkJwckIsVUFBQUEsSUFBSSxFQUFFQTtBQUZhLFNBQVAsQ0FBZDtBQUlEO0FBQ0Y7QUFDRDs7Ozs7O0FBL0JBOztBQXVDQXRCLElBQUFBLE1BQU0sQ0FBQ2lXLFdBQVAsR0FBcUIsU0FBU0EsV0FBVCxDQUFxQnVFLE1BQXJCLEVBQTZCO0FBQ2hELFVBQUkrQixLQUFLLEdBQUcvQixNQUFNLEtBQUssS0FBSyxDQUFoQixHQUFvQixFQUFwQixHQUF5QkEsTUFBckM7QUFBQSxVQUNJOWlCLE1BQU0sR0FBRzZrQixLQUFLLENBQUM3a0IsTUFEbkI7QUFBQSxVQUVJcVAsZUFBZSxHQUFHd1YsS0FBSyxDQUFDeFYsZUFGNUI7QUFBQSxVQUdJOUYsY0FBYyxHQUFHc2IsS0FBSyxDQUFDdGIsY0FIM0I7O0FBS0EsVUFBSW5CLEdBQUcsR0FBRyxLQUFLQSxHQUFMLENBQVN1TCxLQUFULENBQWU7QUFDdkIzVCxRQUFBQSxNQUFNLEVBQUVBLE1BRGU7QUFFdkJxUCxRQUFBQSxlQUFlLEVBQUVBLGVBRk07QUFHdkI5RixRQUFBQSxjQUFjLEVBQUVBO0FBSE8sT0FBZixDQUFWO0FBS0EsYUFBTzBsQixPQUFPLENBQUMsSUFBRCxFQUFPO0FBQ25CN21CLFFBQUFBLEdBQUcsRUFBRUE7QUFEYyxPQUFQLENBQWQ7QUFHRDtBQUNEOzs7Ozs7QUFmQTs7QUF1QkFFLElBQUFBLE1BQU0sQ0FBQzhzQixTQUFQLEdBQW1CLFNBQVNBLFNBQVQsQ0FBbUJwMUIsTUFBbkIsRUFBMkI7QUFDNUMsYUFBTyxLQUFLdWUsV0FBTCxDQUFpQjtBQUN0QnZlLFFBQUFBLE1BQU0sRUFBRUE7QUFEYyxPQUFqQixDQUFQO0FBR0Q7QUFDRDs7Ozs7Ozs7OztBQUxBOztBQWlCQXNJLElBQUFBLE1BQU0sQ0FBQzNRLEdBQVAsR0FBYSxTQUFTQSxHQUFULENBQWErakIsTUFBYixFQUFxQjtBQUNoQyxVQUFJLENBQUMsS0FBSy9SLE9BQVYsRUFBbUIsT0FBTyxJQUFQO0FBQ25CLFVBQUkxSCxVQUFVLEdBQUdILGVBQWUsQ0FBQzRaLE1BQUQsRUFBU3NCLGFBQVQsRUFBd0IsRUFBeEIsQ0FBaEM7QUFBQSxVQUNJcVksZ0JBQWdCLEdBQUcsQ0FBQ2w2QixXQUFXLENBQUM4RyxVQUFVLENBQUN6QyxRQUFaLENBQVosSUFBcUMsQ0FBQ3JFLFdBQVcsQ0FBQzhHLFVBQVUsQ0FBQ2tJLFVBQVosQ0FBakQsSUFBNEUsQ0FBQ2hQLFdBQVcsQ0FBQzhHLFVBQVUsQ0FBQ3JJLE9BQVosQ0FEL0c7QUFFQSxVQUFJMGtCLEtBQUo7O0FBRUEsVUFBSStXLGdCQUFKLEVBQXNCO0FBQ3BCL1csUUFBQUEsS0FBSyxHQUFHK08sZUFBZSxDQUFDNTRCLE1BQU0sQ0FBQzRMLE1BQVAsQ0FBYzhzQixlQUFlLENBQUMsS0FBS25sQixDQUFOLENBQTdCLEVBQXVDL0YsVUFBdkMsQ0FBRCxDQUF2QjtBQUNELE9BRkQsTUFFTyxJQUFJLENBQUM5RyxXQUFXLENBQUM4RyxVQUFVLENBQUNtSSxPQUFaLENBQWhCLEVBQXNDO0FBQzNDa1UsUUFBQUEsS0FBSyxHQUFHc1Asa0JBQWtCLENBQUNuNUIsTUFBTSxDQUFDNEwsTUFBUCxDQUFjcXRCLGtCQUFrQixDQUFDLEtBQUsxbEIsQ0FBTixDQUFoQyxFQUEwQy9GLFVBQTFDLENBQUQsQ0FBMUI7QUFDRCxPQUZNLE1BRUE7QUFDTHFjLFFBQUFBLEtBQUssR0FBRzdwQixNQUFNLENBQUM0TCxNQUFQLENBQWMsS0FBS21kLFFBQUwsRUFBZCxFQUErQnZiLFVBQS9CLENBQVIsQ0FESyxDQUMrQztBQUNwRDs7QUFFQSxZQUFJOUcsV0FBVyxDQUFDOEcsVUFBVSxDQUFDekksR0FBWixDQUFmLEVBQWlDO0FBQy9COGtCLFVBQUFBLEtBQUssQ0FBQzlrQixHQUFOLEdBQVk4RCxJQUFJLENBQUNpckIsR0FBTCxDQUFTenBCLFdBQVcsQ0FBQ3dmLEtBQUssQ0FBQ2hsQixJQUFQLEVBQWFnbEIsS0FBSyxDQUFDL2tCLEtBQW5CLENBQXBCLEVBQStDK2tCLEtBQUssQ0FBQzlrQixHQUFyRCxDQUFaO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJODdCLFNBQVMsR0FBR3JGLE9BQU8sQ0FBQzNSLEtBQUQsRUFBUSxLQUFLOW9CLENBQWIsRUFBZ0IsS0FBS29VLElBQXJCLENBQXZCO0FBQUEsVUFDSTlKLEVBQUUsR0FBR3cxQixTQUFTLENBQUMsQ0FBRCxDQURsQjtBQUFBLFVBRUk5L0IsQ0FBQyxHQUFHOC9CLFNBQVMsQ0FBQyxDQUFELENBRmpCOztBQUlBLGFBQU9yRyxPQUFPLENBQUMsSUFBRCxFQUFPO0FBQ25CbnZCLFFBQUFBLEVBQUUsRUFBRUEsRUFEZTtBQUVuQnRLLFFBQUFBLENBQUMsRUFBRUE7QUFGZ0IsT0FBUCxDQUFkO0FBSUQ7QUFDRDs7Ozs7Ozs7Ozs7OztBQTVCQTs7QUEyQ0E4UyxJQUFBQSxNQUFNLENBQUN1VixJQUFQLEdBQWMsU0FBU0EsSUFBVCxDQUFjQyxRQUFkLEVBQXdCO0FBQ3BDLFVBQUksQ0FBQyxLQUFLblUsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSVksR0FBRyxHQUFHd1QsZ0JBQWdCLENBQUNELFFBQUQsQ0FBMUI7QUFDQSxhQUFPbVIsT0FBTyxDQUFDLElBQUQsRUFBT2lCLFVBQVUsQ0FBQyxJQUFELEVBQU8zbEIsR0FBUCxDQUFqQixDQUFkO0FBQ0Q7QUFDRDs7Ozs7O0FBTEE7O0FBYUFqQyxJQUFBQSxNQUFNLENBQUMyVixLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlSCxRQUFmLEVBQXlCO0FBQ3RDLFVBQUksQ0FBQyxLQUFLblUsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSVksR0FBRyxHQUFHd1QsZ0JBQWdCLENBQUNELFFBQUQsQ0FBaEIsQ0FBMkJJLE1BQTNCLEVBQVY7QUFDQSxhQUFPK1EsT0FBTyxDQUFDLElBQUQsRUFBT2lCLFVBQVUsQ0FBQyxJQUFELEVBQU8zbEIsR0FBUCxDQUFqQixDQUFkO0FBQ0Q7QUFDRDs7Ozs7Ozs7O0FBTEE7O0FBZ0JBakMsSUFBQUEsTUFBTSxDQUFDbVksT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWlCNW5CLElBQWpCLEVBQXVCO0FBQ3RDLFVBQUksQ0FBQyxLQUFLOFEsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsVUFBSW5VLENBQUMsR0FBRyxFQUFSO0FBQUEsVUFDSSsvQixjQUFjLEdBQUczWixRQUFRLENBQUNvQixhQUFULENBQXVCbmtCLElBQXZCLENBRHJCOztBQUdBLGNBQVEwOEIsY0FBUjtBQUNFLGFBQUssT0FBTDtBQUNFLy9CLFVBQUFBLENBQUMsQ0FBQytELEtBQUYsR0FBVSxDQUFWO0FBQ0Y7O0FBRUEsYUFBSyxVQUFMO0FBQ0EsYUFBSyxRQUFMO0FBQ0UvRCxVQUFBQSxDQUFDLENBQUNnRSxHQUFGLEdBQVEsQ0FBUjtBQUNGOztBQUVBLGFBQUssT0FBTDtBQUNBLGFBQUssTUFBTDtBQUNFaEUsVUFBQUEsQ0FBQyxDQUFDc0UsSUFBRixHQUFTLENBQVQ7QUFDRjs7QUFFQSxhQUFLLE9BQUw7QUFDRXRFLFVBQUFBLENBQUMsQ0FBQ3VFLE1BQUYsR0FBVyxDQUFYO0FBQ0Y7O0FBRUEsYUFBSyxTQUFMO0FBQ0V2RSxVQUFBQSxDQUFDLENBQUN5RSxNQUFGLEdBQVcsQ0FBWDtBQUNGOztBQUVBLGFBQUssU0FBTDtBQUNFekUsVUFBQUEsQ0FBQyxDQUFDNEosV0FBRixHQUFnQixDQUFoQjtBQUNBOztBQUVGLGFBQUssY0FBTDtBQUNFO0FBQ0Y7QUE3QkY7O0FBZ0NBLFVBQUltMkIsY0FBYyxLQUFLLE9BQXZCLEVBQWdDO0FBQzlCLy9CLFFBQUFBLENBQUMsQ0FBQ29FLE9BQUYsR0FBWSxDQUFaO0FBQ0Q7O0FBRUQsVUFBSTI3QixjQUFjLEtBQUssVUFBdkIsRUFBbUM7QUFDakMsWUFBSWxLLENBQUMsR0FBRy90QixJQUFJLENBQUN3ZSxJQUFMLENBQVUsS0FBS3ZpQixLQUFMLEdBQWEsQ0FBdkIsQ0FBUjtBQUNBL0QsUUFBQUEsQ0FBQyxDQUFDK0QsS0FBRixHQUFVLENBQUM4eEIsQ0FBQyxHQUFHLENBQUwsSUFBVSxDQUFWLEdBQWMsQ0FBeEI7QUFDRDs7QUFFRCxhQUFPLEtBQUsxekIsR0FBTCxDQUFTbkMsQ0FBVCxDQUFQO0FBQ0Q7QUFDRDs7Ozs7Ozs7O0FBaERBOztBQTJEQThTLElBQUFBLE1BQU0sQ0FBQ2t0QixLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlMzhCLElBQWYsRUFBcUI7QUFDbEMsVUFBSTQ4QixVQUFKOztBQUVBLGFBQU8sS0FBSzlyQixPQUFMLEdBQWUsS0FBS2tVLElBQUwsRUFBVzRYLFVBQVUsR0FBRyxFQUFiLEVBQWlCQSxVQUFVLENBQUM1OEIsSUFBRCxDQUFWLEdBQW1CLENBQXBDLEVBQXVDNDhCLFVBQWxELEdBQStEaFYsT0FBL0QsQ0FBdUU1bkIsSUFBdkUsRUFBNkVvbEIsS0FBN0UsQ0FBbUYsQ0FBbkYsQ0FBZixHQUF1RyxJQUE5RztBQUNELEtBSkQsQ0FJRTs7QUFFRjs7Ozs7Ozs7Ozs7OztBQU5BOztBQXFCQTNWLElBQUFBLE1BQU0sQ0FBQ2dWLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxDQUFrQjFWLEdBQWxCLEVBQXVCRixJQUF2QixFQUE2QjtBQUM3QyxVQUFJQSxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUtpQyxPQUFMLEdBQWVsQyxTQUFTLENBQUNyUyxNQUFWLENBQWlCLEtBQUtnVCxHQUFMLENBQVMwTCxhQUFULENBQXVCcE0sSUFBdkIsQ0FBakIsRUFBK0N3Qix3QkFBL0MsQ0FBd0UsSUFBeEUsRUFBOEV0QixHQUE5RSxDQUFmLEdBQW9HaW5CLFNBQTNHO0FBQ0Q7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBUEE7O0FBMkJBdm1CLElBQUFBLE1BQU0sQ0FBQ290QixjQUFQLEdBQXdCLFNBQVNBLGNBQVQsQ0FBd0JodUIsSUFBeEIsRUFBOEI7QUFDcEQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBR3JPLFVBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUtzUSxPQUFMLEdBQWVsQyxTQUFTLENBQUNyUyxNQUFWLENBQWlCLEtBQUtnVCxHQUFMLENBQVN1TCxLQUFULENBQWVqTSxJQUFmLENBQWpCLEVBQXVDQSxJQUF2QyxFQUE2Q2lCLGNBQTdDLENBQTRELElBQTVELENBQWYsR0FBbUZrbUIsU0FBMUY7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7O0FBUEE7O0FBc0JBdm1CLElBQUFBLE1BQU0sQ0FBQ3F0QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsQ0FBdUJqdUIsSUFBdkIsRUFBNkI7QUFDbEQsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLaUMsT0FBTCxHQUFlbEMsU0FBUyxDQUFDclMsTUFBVixDQUFpQixLQUFLZ1QsR0FBTCxDQUFTdUwsS0FBVCxDQUFlak0sSUFBZixDQUFqQixFQUF1Q0EsSUFBdkMsRUFBNkNrQixtQkFBN0MsQ0FBaUUsSUFBakUsQ0FBZixHQUF3RixFQUEvRjtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7O0FBUEE7O0FBb0JBTixJQUFBQSxNQUFNLENBQUNvVixLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlaFcsSUFBZixFQUFxQjtBQUNsQyxVQUFJQSxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxVQUFJLENBQUMsS0FBS2lDLE9BQVYsRUFBbUI7QUFDakIsZUFBTyxJQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLZ1osU0FBTCxLQUFtQixHQUFuQixHQUF5QixLQUFLQyxTQUFMLENBQWVsYixJQUFmLENBQWhDO0FBQ0Q7QUFDRDs7Ozs7QUFYQTs7QUFrQkFZLElBQUFBLE1BQU0sQ0FBQ3FhLFNBQVAsR0FBbUIsU0FBU0EsU0FBVCxHQUFxQjtBQUN0QyxVQUFJOWhCLE1BQU0sR0FBRyxZQUFiOztBQUVBLFVBQUksS0FBS3ZILElBQUwsR0FBWSxJQUFoQixFQUFzQjtBQUNwQnVILFFBQUFBLE1BQU0sR0FBRyxNQUFNQSxNQUFmO0FBQ0Q7O0FBRUQsYUFBTzR2QixZQUFZLENBQUMsSUFBRCxFQUFPNXZCLE1BQVAsQ0FBbkI7QUFDRDtBQUNEOzs7OztBQVRBOztBQWdCQXlILElBQUFBLE1BQU0sQ0FBQ3N0QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsYUFBT25GLFlBQVksQ0FBQyxJQUFELEVBQU8sY0FBUCxDQUFuQjtBQUNEO0FBQ0Q7Ozs7Ozs7Ozs7QUFIQTs7QUFlQW5vQixJQUFBQSxNQUFNLENBQUNzYSxTQUFQLEdBQW1CLFNBQVNBLFNBQVQsQ0FBbUJzQixNQUFuQixFQUEyQjtBQUM1QyxVQUFJMlIsS0FBSyxHQUFHM1IsTUFBTSxLQUFLLEtBQUssQ0FBaEIsR0FBb0IsRUFBcEIsR0FBeUJBLE1BQXJDO0FBQUEsVUFDSTRSLHFCQUFxQixHQUFHRCxLQUFLLENBQUMvRSxvQkFEbEM7QUFBQSxVQUVJQSxvQkFBb0IsR0FBR2dGLHFCQUFxQixLQUFLLEtBQUssQ0FBL0IsR0FBbUMsS0FBbkMsR0FBMkNBLHFCQUZ0RTtBQUFBLFVBR0lDLHFCQUFxQixHQUFHRixLQUFLLENBQUNqRixlQUhsQztBQUFBLFVBSUlBLGVBQWUsR0FBR21GLHFCQUFxQixLQUFLLEtBQUssQ0FBL0IsR0FBbUMsS0FBbkMsR0FBMkNBLHFCQUpqRTtBQUFBLFVBS0lDLG1CQUFtQixHQUFHSCxLQUFLLENBQUM5RSxhQUxoQztBQUFBLFVBTUlBLGFBQWEsR0FBR2lGLG1CQUFtQixLQUFLLEtBQUssQ0FBN0IsR0FBaUMsSUFBakMsR0FBd0NBLG1CQU41RDs7QUFRQSxhQUFPdEYsZ0JBQWdCLENBQUMsSUFBRCxFQUFPO0FBQzVCRSxRQUFBQSxlQUFlLEVBQUVBLGVBRFc7QUFFNUJFLFFBQUFBLG9CQUFvQixFQUFFQSxvQkFGTTtBQUc1QkMsUUFBQUEsYUFBYSxFQUFFQTtBQUhhLE9BQVAsQ0FBdkI7QUFLRDtBQUNEOzs7Ozs7QUFmQTs7QUF1QkF6b0IsSUFBQUEsTUFBTSxDQUFDMnRCLFNBQVAsR0FBbUIsU0FBU0EsU0FBVCxHQUFxQjtBQUN0QyxhQUFPeEYsWUFBWSxDQUFDLElBQUQsRUFBTywrQkFBUCxDQUFuQjtBQUNEO0FBQ0Q7Ozs7Ozs7O0FBSEE7O0FBYUFub0IsSUFBQUEsTUFBTSxDQUFDNHRCLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxhQUFPekYsWUFBWSxDQUFDLEtBQUtsTCxLQUFMLEVBQUQsRUFBZSxpQ0FBZixDQUFuQjtBQUNEO0FBQ0Q7Ozs7O0FBSEE7O0FBVUFqZCxJQUFBQSxNQUFNLENBQUM2dEIsU0FBUCxHQUFtQixTQUFTQSxTQUFULEdBQXFCO0FBQ3RDLGFBQU8xRixZQUFZLENBQUMsSUFBRCxFQUFPLFlBQVAsQ0FBbkI7QUFDRDtBQUNEOzs7Ozs7Ozs7OztBQUhBOztBQWdCQW5vQixJQUFBQSxNQUFNLENBQUM4dEIsU0FBUCxHQUFtQixTQUFTQSxTQUFULENBQW1COVIsTUFBbkIsRUFBMkI7QUFDNUMsVUFBSStSLEtBQUssR0FBRy9SLE1BQU0sS0FBSyxLQUFLLENBQWhCLEdBQW9CLEVBQXBCLEdBQXlCQSxNQUFyQztBQUFBLFVBQ0lnUyxtQkFBbUIsR0FBR0QsS0FBSyxDQUFDdEYsYUFEaEM7QUFBQSxVQUVJQSxhQUFhLEdBQUd1RixtQkFBbUIsS0FBSyxLQUFLLENBQTdCLEdBQWlDLElBQWpDLEdBQXdDQSxtQkFGNUQ7QUFBQSxVQUdJQyxpQkFBaUIsR0FBR0YsS0FBSyxDQUFDcEYsV0FIOUI7QUFBQSxVQUlJQSxXQUFXLEdBQUdzRixpQkFBaUIsS0FBSyxLQUFLLENBQTNCLEdBQStCLEtBQS9CLEdBQXVDQSxpQkFKekQ7O0FBTUEsYUFBTzdGLGdCQUFnQixDQUFDLElBQUQsRUFBTztBQUM1QkssUUFBQUEsYUFBYSxFQUFFQSxhQURhO0FBRTVCRSxRQUFBQSxXQUFXLEVBQUVBLFdBRmU7QUFHNUJFLFFBQUFBLFNBQVMsRUFBRTtBQUhpQixPQUFQLENBQXZCO0FBS0Q7QUFDRDs7Ozs7Ozs7Ozs7QUFiQTs7QUEwQkE3b0IsSUFBQUEsTUFBTSxDQUFDa3VCLEtBQVAsR0FBZSxTQUFTQSxLQUFULENBQWU5dUIsSUFBZixFQUFxQjtBQUNsQyxVQUFJQSxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxVQUFJLENBQUMsS0FBS2lDLE9BQVYsRUFBbUI7QUFDakIsZUFBTyxJQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLd3NCLFNBQUwsS0FBbUIsR0FBbkIsR0FBeUIsS0FBS0MsU0FBTCxDQUFlMXVCLElBQWYsQ0FBaEM7QUFDRDtBQUNEOzs7O0FBWEE7O0FBaUJBWSxJQUFBQSxNQUFNLENBQUNuUyxRQUFQLEdBQWtCLFNBQVNBLFFBQVQsR0FBb0I7QUFDcEMsYUFBTyxLQUFLd1QsT0FBTCxHQUFlLEtBQUsrVCxLQUFMLEVBQWYsR0FBOEJtUixTQUFyQztBQUNEO0FBQ0Q7Ozs7QUFIQTs7QUFTQXZtQixJQUFBQSxNQUFNLENBQUMwRixPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEMsYUFBTyxLQUFLeW9CLFFBQUwsRUFBUDtBQUNEO0FBQ0Q7Ozs7QUFIQTs7QUFTQW51QixJQUFBQSxNQUFNLENBQUNtdUIsUUFBUCxHQUFrQixTQUFTQSxRQUFULEdBQW9CO0FBQ3BDLGFBQU8sS0FBSzlzQixPQUFMLEdBQWUsS0FBSzdKLEVBQXBCLEdBQXlCME8sR0FBaEM7QUFDRDtBQUNEOzs7O0FBSEE7O0FBU0FsRyxJQUFBQSxNQUFNLENBQUNvdUIsU0FBUCxHQUFtQixTQUFTQSxTQUFULEdBQXFCO0FBQ3RDLGFBQU8sS0FBSy9zQixPQUFMLEdBQWUsS0FBSzdKLEVBQUwsR0FBVSxJQUF6QixHQUFnQzBPLEdBQXZDO0FBQ0Q7QUFDRDs7OztBQUhBOztBQVNBbEcsSUFBQUEsTUFBTSxDQUFDcVYsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLGFBQU8sS0FBS0QsS0FBTCxFQUFQO0FBQ0Q7QUFDRDs7OztBQUhBOztBQVNBcFYsSUFBQUEsTUFBTSxDQUFDcXVCLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxhQUFPLEtBQUs3a0IsUUFBTCxFQUFQO0FBQ0Q7QUFDRDs7Ozs7OztBQUhBOztBQVlBeEosSUFBQUEsTUFBTSxDQUFDa1YsUUFBUCxHQUFrQixTQUFTQSxRQUFULENBQWtCOVYsSUFBbEIsRUFBd0I7QUFDeEMsVUFBSUEsSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUtpQyxPQUFWLEVBQW1CLE9BQU8sRUFBUDtBQUNuQixVQUFJakgsSUFBSSxHQUFHak8sTUFBTSxDQUFDNEwsTUFBUCxDQUFjLEVBQWQsRUFBa0IsS0FBSzJILENBQXZCLENBQVg7O0FBRUEsVUFBSU4sSUFBSSxDQUFDK1YsYUFBVCxFQUF3QjtBQUN0Qi9hLFFBQUFBLElBQUksQ0FBQzZHLGNBQUwsR0FBc0IsS0FBS0EsY0FBM0I7QUFDQTdHLFFBQUFBLElBQUksQ0FBQzJNLGVBQUwsR0FBdUIsS0FBS2pILEdBQUwsQ0FBU2lILGVBQWhDO0FBQ0EzTSxRQUFBQSxJQUFJLENBQUMxQyxNQUFMLEdBQWMsS0FBS29JLEdBQUwsQ0FBU3BJLE1BQXZCO0FBQ0Q7O0FBRUQsYUFBTzBDLElBQVA7QUFDRDtBQUNEOzs7O0FBaEJBOztBQXNCQTRGLElBQUFBLE1BQU0sQ0FBQ3dKLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxHQUFvQjtBQUNwQyxhQUFPLElBQUk1YixJQUFKLENBQVMsS0FBS3lULE9BQUwsR0FBZSxLQUFLN0osRUFBcEIsR0FBeUIwTyxHQUFsQyxDQUFQO0FBQ0QsS0FGRCxDQUVFOztBQUVGOzs7Ozs7Ozs7Ozs7Ozs7QUFKQTs7QUFxQkFsRyxJQUFBQSxNQUFNLENBQUNvWSxJQUFQLEdBQWMsU0FBU0EsSUFBVCxDQUFja1csYUFBZCxFQUE2Qi85QixJQUE3QixFQUFtQzZPLElBQW5DLEVBQXlDO0FBQ3JELFVBQUk3TyxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLGNBQVA7QUFDRDs7QUFFRCxVQUFJNk8sSUFBSSxLQUFLLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkJBLFFBQUFBLElBQUksR0FBRyxFQUFQO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUtpQyxPQUFOLElBQWlCLENBQUNpdEIsYUFBYSxDQUFDanRCLE9BQXBDLEVBQTZDO0FBQzNDLGVBQU9pUyxRQUFRLENBQUNrQixPQUFULENBQWlCLEtBQUtBLE9BQUwsSUFBZ0I4WixhQUFhLENBQUM5WixPQUEvQyxFQUF3RCx3Q0FBeEQsQ0FBUDtBQUNEOztBQUVELFVBQUkrWixPQUFPLEdBQUdwaUMsTUFBTSxDQUFDNEwsTUFBUCxDQUFjO0FBQzFCTCxRQUFBQSxNQUFNLEVBQUUsS0FBS0EsTUFEYTtBQUUxQnFQLFFBQUFBLGVBQWUsRUFBRSxLQUFLQTtBQUZJLE9BQWQsRUFHWDNILElBSFcsQ0FBZDs7QUFLQSxVQUFJbkQsS0FBSyxHQUFHeEksVUFBVSxDQUFDbEQsSUFBRCxDQUFWLENBQWlCc1MsR0FBakIsQ0FBcUJ5USxRQUFRLENBQUNvQixhQUE5QixDQUFaO0FBQUEsVUFDSThaLFlBQVksR0FBR0YsYUFBYSxDQUFDNW9CLE9BQWQsS0FBMEIsS0FBS0EsT0FBTCxFQUQ3QztBQUFBLFVBRUlvWCxPQUFPLEdBQUcwUixZQUFZLEdBQUcsSUFBSCxHQUFVRixhQUZwQztBQUFBLFVBR0l2UixLQUFLLEdBQUd5UixZQUFZLEdBQUdGLGFBQUgsR0FBbUIsSUFIM0M7QUFBQSxVQUlJNzFCLE1BQU0sR0FBR29sQixLQUFLLENBQUNmLE9BQUQsRUFBVUMsS0FBVixFQUFpQjlnQixLQUFqQixFQUF3QnN5QixPQUF4QixDQUpsQjs7QUFNQSxhQUFPQyxZQUFZLEdBQUcvMUIsTUFBTSxDQUFDbWQsTUFBUCxFQUFILEdBQXFCbmQsTUFBeEM7QUFDRDtBQUNEOzs7Ozs7OztBQTFCQTs7QUFvQ0F1SCxJQUFBQSxNQUFNLENBQUN5dUIsT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWlCbCtCLElBQWpCLEVBQXVCNk8sSUFBdkIsRUFBNkI7QUFDNUMsVUFBSTdPLElBQUksS0FBSyxLQUFLLENBQWxCLEVBQXFCO0FBQ25CQSxRQUFBQSxJQUFJLEdBQUcsY0FBUDtBQUNEOztBQUVELFVBQUk2TyxJQUFJLEtBQUssS0FBSyxDQUFsQixFQUFxQjtBQUNuQkEsUUFBQUEsSUFBSSxHQUFHLEVBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUtnWixJQUFMLENBQVU5UCxRQUFRLENBQUM0RyxLQUFULEVBQVYsRUFBNEIzZSxJQUE1QixFQUFrQzZPLElBQWxDLENBQVA7QUFDRDtBQUNEOzs7OztBQVhBOztBQWtCQVksSUFBQUEsTUFBTSxDQUFDMHVCLEtBQVAsR0FBZSxTQUFTQSxLQUFULENBQWVKLGFBQWYsRUFBOEI7QUFDM0MsYUFBTyxLQUFLanRCLE9BQUwsR0FBZWdXLFFBQVEsQ0FBQ0UsYUFBVCxDQUF1QixJQUF2QixFQUE2QitXLGFBQTdCLENBQWYsR0FBNkQsSUFBcEU7QUFDRDtBQUNEOzs7Ozs7O0FBSEE7O0FBWUF0dUIsSUFBQUEsTUFBTSxDQUFDcVksT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWlCaVcsYUFBakIsRUFBZ0MvOUIsSUFBaEMsRUFBc0M7QUFDckQsVUFBSSxDQUFDLEtBQUs4USxPQUFWLEVBQW1CLE9BQU8sS0FBUDs7QUFFbkIsVUFBSTlRLElBQUksS0FBSyxhQUFiLEVBQTRCO0FBQzFCLGVBQU8sS0FBS21WLE9BQUwsT0FBbUI0b0IsYUFBYSxDQUFDNW9CLE9BQWQsRUFBMUI7QUFDRCxPQUZELE1BRU87QUFDTCxZQUFJaXBCLE9BQU8sR0FBR0wsYUFBYSxDQUFDNW9CLE9BQWQsRUFBZDtBQUNBLGVBQU8sS0FBS3lTLE9BQUwsQ0FBYTVuQixJQUFiLEtBQXNCbytCLE9BQXRCLElBQWlDQSxPQUFPLElBQUksS0FBS3pCLEtBQUwsQ0FBVzM4QixJQUFYLENBQW5EO0FBQ0Q7QUFDRjtBQUNEOzs7Ozs7O0FBVkE7O0FBbUJBeVAsSUFBQUEsTUFBTSxDQUFDa0QsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCbUosS0FBaEIsRUFBdUI7QUFDckMsYUFBTyxLQUFLaEwsT0FBTCxJQUFnQmdMLEtBQUssQ0FBQ2hMLE9BQXRCLElBQWlDLEtBQUtxRSxPQUFMLE9BQW1CMkcsS0FBSyxDQUFDM0csT0FBTixFQUFwRCxJQUF1RSxLQUFLcEUsSUFBTCxDQUFVNEIsTUFBVixDQUFpQm1KLEtBQUssQ0FBQy9LLElBQXZCLENBQXZFLElBQXVHLEtBQUt4QixHQUFMLENBQVNvRCxNQUFULENBQWdCbUosS0FBSyxDQUFDdk0sR0FBdEIsQ0FBOUc7QUFDRDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFIQTs7QUF1QkFFLElBQUFBLE1BQU0sQ0FBQzR1QixVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0I3bUIsT0FBcEIsRUFBNkI7QUFDL0MsVUFBSUEsT0FBTyxLQUFLLEtBQUssQ0FBckIsRUFBd0I7QUFDdEJBLFFBQUFBLE9BQU8sR0FBRyxFQUFWO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUsxRyxPQUFWLEVBQW1CLE9BQU8sSUFBUDtBQUNuQixVQUFJakgsSUFBSSxHQUFHMk4sT0FBTyxDQUFDM04sSUFBUixJQUFnQmtPLFFBQVEsQ0FBQytCLFVBQVQsQ0FBb0I7QUFDN0MvSSxRQUFBQSxJQUFJLEVBQUUsS0FBS0E7QUFEa0MsT0FBcEIsQ0FBM0I7QUFBQSxVQUdJdXRCLE9BQU8sR0FBRzltQixPQUFPLENBQUM4bUIsT0FBUixHQUFrQixPQUFPejBCLElBQVAsR0FBYyxDQUFDMk4sT0FBTyxDQUFDOG1CLE9BQXZCLEdBQWlDOW1CLE9BQU8sQ0FBQzhtQixPQUEzRCxHQUFxRSxDQUhuRjtBQUlBLGFBQU9oRixZQUFZLENBQUN6dkIsSUFBRCxFQUFPLEtBQUttYixJQUFMLENBQVVzWixPQUFWLENBQVAsRUFBMkIxaUMsTUFBTSxDQUFDNEwsTUFBUCxDQUFjZ1EsT0FBZCxFQUF1QjtBQUNuRWhNLFFBQUFBLE9BQU8sRUFBRSxRQUQwRDtBQUVuRUUsUUFBQUEsS0FBSyxFQUFFLENBQUMsT0FBRCxFQUFVLFFBQVYsRUFBb0IsTUFBcEIsRUFBNEIsT0FBNUIsRUFBcUMsU0FBckMsRUFBZ0QsU0FBaEQ7QUFGNEQsT0FBdkIsQ0FBM0IsQ0FBbkI7QUFJRDtBQUNEOzs7Ozs7Ozs7Ozs7O0FBZkE7O0FBOEJBK0QsSUFBQUEsTUFBTSxDQUFDOHVCLGtCQUFQLEdBQTRCLFNBQVNBLGtCQUFULENBQTRCL21CLE9BQTVCLEVBQXFDO0FBQy9ELFVBQUlBLE9BQU8sS0FBSyxLQUFLLENBQXJCLEVBQXdCO0FBQ3RCQSxRQUFBQSxPQUFPLEdBQUcsRUFBVjtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLMUcsT0FBVixFQUFtQixPQUFPLElBQVA7QUFDbkIsYUFBT3dvQixZQUFZLENBQUM5aEIsT0FBTyxDQUFDM04sSUFBUixJQUFnQmtPLFFBQVEsQ0FBQytCLFVBQVQsQ0FBb0I7QUFDdEQvSSxRQUFBQSxJQUFJLEVBQUUsS0FBS0E7QUFEMkMsT0FBcEIsQ0FBakIsRUFFZixJQUZlLEVBRVRuVixNQUFNLENBQUM0TCxNQUFQLENBQWNnUSxPQUFkLEVBQXVCO0FBQy9CaE0sUUFBQUEsT0FBTyxFQUFFLE1BRHNCO0FBRS9CRSxRQUFBQSxLQUFLLEVBQUUsQ0FBQyxPQUFELEVBQVUsUUFBVixFQUFvQixNQUFwQixDQUZ3QjtBQUcvQjZ0QixRQUFBQSxTQUFTLEVBQUU7QUFIb0IsT0FBdkIsQ0FGUyxDQUFuQjtBQU9EO0FBQ0Q7Ozs7O0FBZEE7O0FBcUJBeGhCLElBQUFBLFFBQVEsQ0FBQzJYLEdBQVQsR0FBZSxTQUFTQSxHQUFULEdBQWU7QUFDNUIsV0FBSyxJQUFJMVQsSUFBSSxHQUFHN2QsU0FBUyxDQUFDNUMsTUFBckIsRUFBNkI4c0IsU0FBUyxHQUFHLElBQUlqbEIsS0FBSixDQUFVNFksSUFBVixDQUF6QyxFQUEwREUsSUFBSSxHQUFHLENBQXRFLEVBQXlFQSxJQUFJLEdBQUdGLElBQWhGLEVBQXNGRSxJQUFJLEVBQTFGLEVBQThGO0FBQzVGbU0sUUFBQUEsU0FBUyxDQUFDbk0sSUFBRCxDQUFULEdBQWtCL2QsU0FBUyxDQUFDK2QsSUFBRCxDQUEzQjtBQUNEOztBQUVELFVBQUksQ0FBQ21NLFNBQVMsQ0FBQ21XLEtBQVYsQ0FBZ0J6bUIsUUFBUSxDQUFDNmpCLFVBQXpCLENBQUwsRUFBMkM7QUFDekMsY0FBTSxJQUFJMzdCLG9CQUFKLENBQXlCLHlDQUF6QixDQUFOO0FBQ0Q7O0FBRUQsYUFBT3FELE1BQU0sQ0FBQytrQixTQUFELEVBQVksVUFBVS9zQixDQUFWLEVBQWE7QUFDcEMsZUFBT0EsQ0FBQyxDQUFDNlosT0FBRixFQUFQO0FBQ0QsT0FGWSxFQUVWMVEsSUFBSSxDQUFDaXJCLEdBRkssQ0FBYjtBQUdEO0FBQ0Q7Ozs7O0FBYkE7O0FBb0JBM1gsSUFBQUEsUUFBUSxDQUFDNFgsR0FBVCxHQUFlLFNBQVNBLEdBQVQsR0FBZTtBQUM1QixXQUFLLElBQUl0VCxLQUFLLEdBQUdsZSxTQUFTLENBQUM1QyxNQUF0QixFQUE4QjhzQixTQUFTLEdBQUcsSUFBSWpsQixLQUFKLENBQVVpWixLQUFWLENBQTFDLEVBQTRERSxLQUFLLEdBQUcsQ0FBekUsRUFBNEVBLEtBQUssR0FBR0YsS0FBcEYsRUFBMkZFLEtBQUssRUFBaEcsRUFBb0c7QUFDbEc4TCxRQUFBQSxTQUFTLENBQUM5TCxLQUFELENBQVQsR0FBbUJwZSxTQUFTLENBQUNvZSxLQUFELENBQTVCO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDOEwsU0FBUyxDQUFDbVcsS0FBVixDQUFnQnptQixRQUFRLENBQUM2akIsVUFBekIsQ0FBTCxFQUEyQztBQUN6QyxjQUFNLElBQUkzN0Isb0JBQUosQ0FBeUIseUNBQXpCLENBQU47QUFDRDs7QUFFRCxhQUFPcUQsTUFBTSxDQUFDK2tCLFNBQUQsRUFBWSxVQUFVL3NCLENBQVYsRUFBYTtBQUNwQyxlQUFPQSxDQUFDLENBQUM2WixPQUFGLEVBQVA7QUFDRCxPQUZZLEVBRVYxUSxJQUFJLENBQUNrckIsR0FGSyxDQUFiO0FBR0QsS0FaRCxDQVlFOztBQUVGOzs7Ozs7O0FBZEE7O0FBdUJBNVgsSUFBQUEsUUFBUSxDQUFDMG1CLGlCQUFULEdBQTZCLFNBQVNBLGlCQUFULENBQTJCcGEsSUFBM0IsRUFBaUN0VixHQUFqQyxFQUFzQ3lJLE9BQXRDLEVBQStDO0FBQzFFLFVBQUlBLE9BQU8sS0FBSyxLQUFLLENBQXJCLEVBQXdCO0FBQ3RCQSxRQUFBQSxPQUFPLEdBQUcsRUFBVjtBQUNEOztBQUVELFVBQUlFLFFBQVEsR0FBR0YsT0FBZjtBQUFBLFVBQ0lrbkIsZUFBZSxHQUFHaG5CLFFBQVEsQ0FBQ3ZRLE1BRC9CO0FBQUEsVUFFSUEsTUFBTSxHQUFHdTNCLGVBQWUsS0FBSyxLQUFLLENBQXpCLEdBQTZCLElBQTdCLEdBQW9DQSxlQUZqRDtBQUFBLFVBR0lDLHFCQUFxQixHQUFHam5CLFFBQVEsQ0FBQ2xCLGVBSHJDO0FBQUEsVUFJSUEsZUFBZSxHQUFHbW9CLHFCQUFxQixLQUFLLEtBQUssQ0FBL0IsR0FBbUMsSUFBbkMsR0FBMENBLHFCQUpoRTtBQUFBLFVBS0lwRCxXQUFXLEdBQUdqbEIsTUFBTSxDQUFDa0QsUUFBUCxDQUFnQjtBQUNoQ3JTLFFBQUFBLE1BQU0sRUFBRUEsTUFEd0I7QUFFaENxUCxRQUFBQSxlQUFlLEVBQUVBLGVBRmU7QUFHaENpRCxRQUFBQSxXQUFXLEVBQUU7QUFIbUIsT0FBaEIsQ0FMbEI7QUFVQSxhQUFPMlosaUJBQWlCLENBQUNtSSxXQUFELEVBQWNsWCxJQUFkLEVBQW9CdFYsR0FBcEIsQ0FBeEI7QUFDRDtBQUNEOzs7QUFqQkE7O0FBc0JBZ0osSUFBQUEsUUFBUSxDQUFDNm1CLGlCQUFULEdBQTZCLFNBQVNBLGlCQUFULENBQTJCdmEsSUFBM0IsRUFBaUN0VixHQUFqQyxFQUFzQ3lJLE9BQXRDLEVBQStDO0FBQzFFLFVBQUlBLE9BQU8sS0FBSyxLQUFLLENBQXJCLEVBQXdCO0FBQ3RCQSxRQUFBQSxPQUFPLEdBQUcsRUFBVjtBQUNEOztBQUVELGFBQU9PLFFBQVEsQ0FBQzBtQixpQkFBVCxDQUEyQnBhLElBQTNCLEVBQWlDdFYsR0FBakMsRUFBc0N5SSxPQUF0QyxDQUFQO0FBQ0QsS0FORCxDQU1FOztBQUVGOzs7O0FBUkE7O0FBY0F6YixJQUFBQSxZQUFZLENBQUNnYyxRQUFELEVBQVcsQ0FBQztBQUN0QmpjLE1BQUFBLEdBQUcsRUFBRSxTQURpQjtBQUV0QitDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLb2xCLE9BQUwsS0FBaUIsSUFBeEI7QUFDRDtBQUNEOzs7OztBQUxzQixLQUFELEVBVXBCO0FBQ0Rub0IsTUFBQUEsR0FBRyxFQUFFLGVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLb2xCLE9BQUwsR0FBZSxLQUFLQSxPQUFMLENBQWEza0IsTUFBNUIsR0FBcUMsSUFBNUM7QUFDRDtBQUNEOzs7OztBQUxDLEtBVm9CLEVBb0JwQjtBQUNEeEQsTUFBQUEsR0FBRyxFQUFFLG9CQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS29sQixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFheFIsV0FBNUIsR0FBMEMsSUFBakQ7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQXBCb0IsRUErQnBCO0FBQ0QzVyxNQUFBQSxHQUFHLEVBQUUsUUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBS3ZCLEdBQUwsQ0FBU3BJLE1BQXhCLEdBQWlDLElBQXhDO0FBQ0Q7QUFDRDs7Ozs7O0FBTEMsS0EvQm9CLEVBMENwQjtBQUNEckwsTUFBQUEsR0FBRyxFQUFFLGlCQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS2lTLE9BQUwsR0FBZSxLQUFLdkIsR0FBTCxDQUFTaUgsZUFBeEIsR0FBMEMsSUFBakQ7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQTFDb0IsRUFxRHBCO0FBQ0QxYSxNQUFBQSxHQUFHLEVBQUUsZ0JBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUt2QixHQUFMLENBQVNtQixjQUF4QixHQUF5QyxJQUFoRDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0FyRG9CLEVBK0RwQjtBQUNENVUsTUFBQUEsR0FBRyxFQUFFLE1BREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLNDZCLEtBQVo7QUFDRDtBQUNEOzs7OztBQUxDLEtBL0RvQixFQXlFcEI7QUFDRDM5QixNQUFBQSxHQUFHLEVBQUUsVUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBS0MsSUFBTCxDQUFVeUQsSUFBekIsR0FBZ0MsSUFBdkM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQXpFb0IsRUFvRnBCO0FBQ0QxWSxNQUFBQSxHQUFHLEVBQUUsTUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBTzFPLElBQXRCLEdBQTZCa1YsR0FBcEM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQXBGb0IsRUErRnBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWVyTSxJQUFJLENBQUN3ZSxJQUFMLENBQVUsS0FBSzlULENBQUwsQ0FBT3pPLEtBQVAsR0FBZSxDQUF6QixDQUFmLEdBQTZDaVYsR0FBcEQ7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQS9Gb0IsRUEwR3BCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsT0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBT3pPLEtBQXRCLEdBQThCaVYsR0FBckM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQTFHb0IsRUFxSHBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsS0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBT3hPLEdBQXRCLEdBQTRCZ1YsR0FBbkM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQXJIb0IsRUFnSXBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsTUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBT2xPLElBQXRCLEdBQTZCMFUsR0FBcEM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQWhJb0IsRUEySXBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsUUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBT2pPLE1BQXRCLEdBQStCeVUsR0FBdEM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQTNJb0IsRUFzSnBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsUUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBTy9OLE1BQXRCLEdBQStCdVUsR0FBdEM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQXRKb0IsRUFpS3BCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsYUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWUsS0FBSzNCLENBQUwsQ0FBTzVJLFdBQXRCLEdBQW9Db1AsR0FBM0M7QUFDRDtBQUNEOzs7Ozs7O0FBTEMsS0FqS29CLEVBNktwQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLFVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlcWxCLHNCQUFzQixDQUFDLElBQUQsQ0FBdEIsQ0FBNkJ4dkIsUUFBNUMsR0FBdURnUCxHQUE5RDtBQUNEO0FBQ0Q7Ozs7Ozs7QUFMQyxLQTdLb0IsRUF5THBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsWUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWVxbEIsc0JBQXNCLENBQUMsSUFBRCxDQUF0QixDQUE2QjdrQixVQUE1QyxHQUF5RHFFLEdBQWhFO0FBQ0Q7QUFDRDs7Ozs7Ozs7QUFMQyxLQXpMb0IsRUFzTXBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPLEtBQUtpUyxPQUFMLEdBQWVxbEIsc0JBQXNCLENBQUMsSUFBRCxDQUF0QixDQUE2QnAxQixPQUE1QyxHQUFzRDRVLEdBQTdEO0FBQ0Q7QUFDRDs7Ozs7O0FBTEMsS0F0TW9CLEVBaU5wQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLFNBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlK2pCLGtCQUFrQixDQUFDLEtBQUsxbEIsQ0FBTixDQUFsQixDQUEyQm9DLE9BQTFDLEdBQW9Eb0UsR0FBM0Q7QUFDRDtBQUNEOzs7Ozs7O0FBTEMsS0FqTm9CLEVBNk5wQjtBQUNEN1osTUFBQUEsR0FBRyxFQUFFLFlBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlMFosSUFBSSxDQUFDamdCLE1BQUwsQ0FBWSxPQUFaLEVBQXFCO0FBQ3pDcEQsVUFBQUEsTUFBTSxFQUFFLEtBQUtBO0FBRDRCLFNBQXJCLEVBRW5CLEtBQUt6RyxLQUFMLEdBQWEsQ0FGTSxDQUFmLEdBRWMsSUFGckI7QUFHRDtBQUNEOzs7Ozs7O0FBUEMsS0E3Tm9CLEVBMk9wQjtBQUNENUUsTUFBQUEsR0FBRyxFQUFFLFdBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlMFosSUFBSSxDQUFDamdCLE1BQUwsQ0FBWSxNQUFaLEVBQW9CO0FBQ3hDcEQsVUFBQUEsTUFBTSxFQUFFLEtBQUtBO0FBRDJCLFNBQXBCLEVBRW5CLEtBQUt6RyxLQUFMLEdBQWEsQ0FGTSxDQUFmLEdBRWMsSUFGckI7QUFHRDtBQUNEOzs7Ozs7O0FBUEMsS0EzT29CLEVBeVBwQjtBQUNENUUsTUFBQUEsR0FBRyxFQUFFLGNBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlMFosSUFBSSxDQUFDN2YsUUFBTCxDQUFjLE9BQWQsRUFBdUI7QUFDM0N4RCxVQUFBQSxNQUFNLEVBQUUsS0FBS0E7QUFEOEIsU0FBdkIsRUFFbkIsS0FBS3BHLE9BQUwsR0FBZSxDQUZJLENBQWYsR0FFZ0IsSUFGdkI7QUFHRDtBQUNEOzs7Ozs7O0FBUEMsS0F6UG9CLEVBdVFwQjtBQUNEakYsTUFBQUEsR0FBRyxFQUFFLGFBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlMFosSUFBSSxDQUFDN2YsUUFBTCxDQUFjLE1BQWQsRUFBc0I7QUFDMUN4RCxVQUFBQSxNQUFNLEVBQUUsS0FBS0E7QUFENkIsU0FBdEIsRUFFbkIsS0FBS3BHLE9BQUwsR0FBZSxDQUZJLENBQWYsR0FFZ0IsSUFGdkI7QUFHRDtBQUNEOzs7Ozs7O0FBUEMsS0F2UW9CLEVBcVJwQjtBQUNEakYsTUFBQUEsR0FBRyxFQUFFLFFBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLENBQUMsS0FBS25VLENBQXJCLEdBQXlCZ1osR0FBaEM7QUFDRDtBQUNEOzs7Ozs7QUFMQyxLQXJSb0IsRUFnU3BCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsaUJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsWUFBSSxLQUFLaVMsT0FBVCxFQUFrQjtBQUNoQixpQkFBTyxLQUFLQyxJQUFMLENBQVVLLFVBQVYsQ0FBcUIsS0FBS25LLEVBQTFCLEVBQThCO0FBQ25DZSxZQUFBQSxNQUFNLEVBQUUsT0FEMkI7QUFFbkNiLFlBQUFBLE1BQU0sRUFBRSxLQUFLQTtBQUZzQixXQUE5QixDQUFQO0FBSUQsU0FMRCxNQUtPO0FBQ0wsaUJBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRDs7Ozs7O0FBWkMsS0FoU29CLEVBa1RwQjtBQUNEckwsTUFBQUEsR0FBRyxFQUFFLGdCQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLFlBQUksS0FBS2lTLE9BQVQsRUFBa0I7QUFDaEIsaUJBQU8sS0FBS0MsSUFBTCxDQUFVSyxVQUFWLENBQXFCLEtBQUtuSyxFQUExQixFQUE4QjtBQUNuQ2UsWUFBQUEsTUFBTSxFQUFFLE1BRDJCO0FBRW5DYixZQUFBQSxNQUFNLEVBQUUsS0FBS0E7QUFGc0IsV0FBOUIsQ0FBUDtBQUlELFNBTEQsTUFLTztBQUNMLGlCQUFPLElBQVA7QUFDRDtBQUNGO0FBQ0Q7Ozs7O0FBWkMsS0FsVG9CLEVBbVVwQjtBQUNEckwsTUFBQUEsR0FBRyxFQUFFLGVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlLEtBQUtDLElBQUwsQ0FBVStILFNBQXpCLEdBQXFDLElBQTVDO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQW5Vb0IsRUE2VXBCO0FBQ0RoZCxNQUFBQSxHQUFHLEVBQUUsU0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixZQUFJLEtBQUsrUixhQUFULEVBQXdCO0FBQ3RCLGlCQUFPLEtBQVA7QUFDRCxTQUZELE1BRU87QUFDTCxpQkFBTyxLQUFLcEgsTUFBTCxHQUFjLEtBQUsxSyxHQUFMLENBQVM7QUFDNUI0QixZQUFBQSxLQUFLLEVBQUU7QUFEcUIsV0FBVCxFQUVsQjhJLE1BRkksSUFFTSxLQUFLQSxNQUFMLEdBQWMsS0FBSzFLLEdBQUwsQ0FBUztBQUNsQzRCLFlBQUFBLEtBQUssRUFBRTtBQUQyQixXQUFULEVBRXhCOEksTUFKSDtBQUtEO0FBQ0Y7QUFDRDs7Ozs7OztBQWJDLEtBN1VvQixFQWlXcEI7QUFDRDFOLE1BQUFBLEdBQUcsRUFBRSxjQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9rSCxVQUFVLENBQUMsS0FBS3RGLElBQU4sQ0FBakI7QUFDRDtBQUNEOzs7Ozs7O0FBTEMsS0FqV29CLEVBNldwQjtBQUNEM0UsTUFBQUEsR0FBRyxFQUFFLGFBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT29ILFdBQVcsQ0FBQyxLQUFLeEYsSUFBTixFQUFZLEtBQUtDLEtBQWpCLENBQWxCO0FBQ0Q7QUFDRDs7Ozs7OztBQUxDLEtBN1dvQixFQXlYcEI7QUFDRDVFLE1BQUFBLEdBQUcsRUFBRSxZQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8sS0FBS2lTLE9BQUwsR0FBZTlLLFVBQVUsQ0FBQyxLQUFLdkYsSUFBTixDQUF6QixHQUF1Q2tWLEdBQTlDO0FBQ0Q7QUFDRDs7Ozs7Ozs7QUFMQyxLQXpYb0IsRUFzWXBCO0FBQ0Q3WixNQUFBQSxHQUFHLEVBQUUsaUJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTyxLQUFLaVMsT0FBTCxHQUFlcEssZUFBZSxDQUFDLEtBQUtDLFFBQU4sQ0FBOUIsR0FBZ0RnUCxHQUF2RDtBQUNEO0FBSkEsS0F0WW9CLENBQVgsRUEyWVIsQ0FBQztBQUNIN1osTUFBQUEsR0FBRyxFQUFFLFlBREY7QUFFSCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTzJCLFVBQVA7QUFDRDtBQUNEOzs7OztBQUxHLEtBQUQsRUFVRDtBQUNEMUUsTUFBQUEsR0FBRyxFQUFFLFVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTytCLFFBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBVkMsRUFvQkQ7QUFDRDlFLE1BQUFBLEdBQUcsRUFBRSxXQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9nQyxTQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQXBCQyxFQThCRDtBQUNEL0UsTUFBQUEsR0FBRyxFQUFFLFdBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT2lDLFNBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBOUJDLEVBd0NEO0FBQ0RoRixNQUFBQSxHQUFHLEVBQUUsYUFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPbUMsV0FBUDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0F4Q0MsRUFrREQ7QUFDRGxGLE1BQUFBLEdBQUcsRUFBRSxtQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPc0MsaUJBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBbERDLEVBNEREO0FBQ0RyRixNQUFBQSxHQUFHLEVBQUUsd0JBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3dDLHNCQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQTVEQyxFQXNFRDtBQUNEdkYsTUFBQUEsR0FBRyxFQUFFLHVCQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8wQyxxQkFBUDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0F0RUMsRUFnRkQ7QUFDRHpGLE1BQUFBLEdBQUcsRUFBRSxnQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPMkMsY0FBUDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0FoRkMsRUEwRkQ7QUFDRDFGLE1BQUFBLEdBQUcsRUFBRSxzQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPNkMsb0JBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBMUZDLEVBb0dEO0FBQ0Q1RixNQUFBQSxHQUFHLEVBQUUsMkJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTzhDLHlCQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQXBHQyxFQThHRDtBQUNEN0YsTUFBQUEsR0FBRyxFQUFFLDBCQURKO0FBRUQrQyxNQUFBQSxHQUFHLEVBQUUsU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8rQyx3QkFBUDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0E5R0MsRUF3SEQ7QUFDRDlGLE1BQUFBLEdBQUcsRUFBRSxnQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPZ0QsY0FBUDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0F4SEMsRUFrSUQ7QUFDRC9GLE1BQUFBLEdBQUcsRUFBRSw2QkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPaUQsMkJBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBbElDLEVBNElEO0FBQ0RoRyxNQUFBQSxHQUFHLEVBQUUsY0FESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPa0QsWUFBUDtBQUNEO0FBQ0Q7Ozs7O0FBTEMsS0E1SUMsRUFzSkQ7QUFDRGpHLE1BQUFBLEdBQUcsRUFBRSwyQkFESjtBQUVEK0MsTUFBQUEsR0FBRyxFQUFFLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPbUQseUJBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBdEpDLEVBZ0tEO0FBQ0RsRyxNQUFBQSxHQUFHLEVBQUUsMkJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT29ELHlCQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQWhLQyxFQTBLRDtBQUNEbkcsTUFBQUEsR0FBRyxFQUFFLGVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3FELGFBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBMUtDLEVBb0xEO0FBQ0RwRyxNQUFBQSxHQUFHLEVBQUUsNEJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3NELDBCQUFQO0FBQ0Q7QUFDRDs7Ozs7QUFMQyxLQXBMQyxFQThMRDtBQUNEckcsTUFBQUEsR0FBRyxFQUFFLGVBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3VELGFBQVA7QUFDRDtBQUNEOzs7OztBQUxDLEtBOUxDLEVBd01EO0FBQ0R0RyxNQUFBQSxHQUFHLEVBQUUsNEJBREo7QUFFRCtDLE1BQUFBLEdBQUcsRUFBRSxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3dELDBCQUFQO0FBQ0Q7QUFKQSxLQXhNQyxDQTNZUSxDQUFaOztBQTBsQkEsV0FBTzBWLFFBQVA7QUFDRCxHQTVnRUQsRUFGQTs7QUErZ0VBLFdBQVNtUCxnQkFBVCxDQUEwQjJYLFdBQTFCLEVBQXVDO0FBQ3JDLFFBQUk5bUIsUUFBUSxDQUFDNmpCLFVBQVQsQ0FBb0JpRCxXQUFwQixDQUFKLEVBQXNDO0FBQ3BDLGFBQU9BLFdBQVA7QUFDRCxLQUZELE1BRU8sSUFBSUEsV0FBVyxJQUFJQSxXQUFXLENBQUMxcEIsT0FBM0IsSUFBc0M1UyxRQUFRLENBQUNzOEIsV0FBVyxDQUFDMXBCLE9BQVosRUFBRCxDQUFsRCxFQUEyRTtBQUNoRixhQUFPNEMsUUFBUSxDQUFDNGhCLFVBQVQsQ0FBb0JrRixXQUFwQixDQUFQO0FBQ0QsS0FGTSxNQUVBLElBQUlBLFdBQVcsSUFBSSxRQUFPQSxXQUFQLE1BQXVCLFFBQTFDLEVBQW9EO0FBQ3pELGFBQU85bUIsUUFBUSxDQUFDK0IsVUFBVCxDQUFvQitrQixXQUFwQixDQUFQO0FBQ0QsS0FGTSxNQUVBO0FBQ0wsWUFBTSxJQUFJNStCLG9CQUFKLENBQXlCLGdDQUFnQzQrQixXQUFoQyxHQUE4QyxZQUE5QyxXQUFvRUEsV0FBcEUsQ0FBekIsQ0FBTjtBQUNEO0FBQ0Y7O0FBRUQzakMsRUFBQUEsT0FBTyxDQUFDNmMsUUFBUixHQUFtQkEsUUFBbkI7QUFDQTdjLEVBQUFBLE9BQU8sQ0FBQzZuQixRQUFSLEdBQW1CQSxRQUFuQjtBQUNBN25CLEVBQUFBLE9BQU8sQ0FBQ21hLGVBQVIsR0FBMEJBLGVBQTFCO0FBQ0FuYSxFQUFBQSxPQUFPLENBQUNxWixRQUFSLEdBQW1CQSxRQUFuQjtBQUNBclosRUFBQUEsT0FBTyxDQUFDc3ZCLElBQVIsR0FBZUEsSUFBZjtBQUNBdHZCLEVBQUFBLE9BQU8sQ0FBQzRyQixRQUFSLEdBQW1CQSxRQUFuQjtBQUNBNXJCLEVBQUFBLE9BQU8sQ0FBQ3dhLFdBQVIsR0FBc0JBLFdBQXRCO0FBQ0F4YSxFQUFBQSxPQUFPLENBQUM0WCxTQUFSLEdBQW9CQSxTQUFwQjtBQUNBNVgsRUFBQUEsT0FBTyxDQUFDa2IsUUFBUixHQUFtQkEsUUFBbkI7QUFDQWxiLEVBQUFBLE9BQU8sQ0FBQ3dYLElBQVIsR0FBZUEsSUFBZjtBQUVBLFNBQU94WCxPQUFQO0FBRUQsQ0F2a1FZLENBdWtRWCxFQXZrUVcsQ0FBYiIsInNvdXJjZXNDb250ZW50IjpbInZhciBsdXhvbiA9IChmdW5jdGlvbiAoZXhwb3J0cykge1xuICAndXNlIHN0cmljdCc7XG5cbiAgZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XG4gICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgICBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICB9XG5cbiAgZnVuY3Rpb24gX2luaGVyaXRzTG9vc2Uoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHtcbiAgICBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MucHJvdG90eXBlKTtcbiAgICBzdWJDbGFzcy5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBzdWJDbGFzcztcbiAgICBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzO1xuICB9XG5cbiAgZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHtcbiAgICBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2Yobykge1xuICAgICAgcmV0dXJuIG8uX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihvKTtcbiAgICB9O1xuICAgIHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7XG4gIH1cblxuICBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkge1xuICAgIF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkge1xuICAgICAgby5fX3Byb3RvX18gPSBwO1xuICAgICAgcmV0dXJuIG87XG4gICAgfTtcblxuICAgIHJldHVybiBfc2V0UHJvdG90eXBlT2YobywgcCk7XG4gIH1cblxuICBmdW5jdGlvbiBpc05hdGl2ZVJlZmxlY3RDb25zdHJ1Y3QoKSB7XG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcInVuZGVmaW5lZFwiIHx8ICFSZWZsZWN0LmNvbnN0cnVjdCkgcmV0dXJuIGZhbHNlO1xuICAgIGlmIChSZWZsZWN0LmNvbnN0cnVjdC5zaGFtKSByZXR1cm4gZmFsc2U7XG4gICAgaWYgKHR5cGVvZiBQcm94eSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gdHJ1ZTtcblxuICAgIHRyeSB7XG4gICAgICBEYXRlLnByb3RvdHlwZS50b1N0cmluZy5jYWxsKFJlZmxlY3QuY29uc3RydWN0KERhdGUsIFtdLCBmdW5jdGlvbiAoKSB7fSkpO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIF9jb25zdHJ1Y3QoUGFyZW50LCBhcmdzLCBDbGFzcykge1xuICAgIGlmIChpc05hdGl2ZVJlZmxlY3RDb25zdHJ1Y3QoKSkge1xuICAgICAgX2NvbnN0cnVjdCA9IFJlZmxlY3QuY29uc3RydWN0O1xuICAgIH0gZWxzZSB7XG4gICAgICBfY29uc3RydWN0ID0gZnVuY3Rpb24gX2NvbnN0cnVjdChQYXJlbnQsIGFyZ3MsIENsYXNzKSB7XG4gICAgICAgIHZhciBhID0gW251bGxdO1xuICAgICAgICBhLnB1c2guYXBwbHkoYSwgYXJncyk7XG4gICAgICAgIHZhciBDb25zdHJ1Y3RvciA9IEZ1bmN0aW9uLmJpbmQuYXBwbHkoUGFyZW50LCBhKTtcbiAgICAgICAgdmFyIGluc3RhbmNlID0gbmV3IENvbnN0cnVjdG9yKCk7XG4gICAgICAgIGlmIChDbGFzcykgX3NldFByb3RvdHlwZU9mKGluc3RhbmNlLCBDbGFzcy5wcm90b3R5cGUpO1xuICAgICAgICByZXR1cm4gaW5zdGFuY2U7XG4gICAgICB9O1xuICAgIH1cblxuICAgIHJldHVybiBfY29uc3RydWN0LmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG4gIH1cblxuICBmdW5jdGlvbiBfaXNOYXRpdmVGdW5jdGlvbihmbikge1xuICAgIHJldHVybiBGdW5jdGlvbi50b1N0cmluZy5jYWxsKGZuKS5pbmRleE9mKFwiW25hdGl2ZSBjb2RlXVwiKSAhPT0gLTE7XG4gIH1cblxuICBmdW5jdGlvbiBfd3JhcE5hdGl2ZVN1cGVyKENsYXNzKSB7XG4gICAgdmFyIF9jYWNoZSA9IHR5cGVvZiBNYXAgPT09IFwiZnVuY3Rpb25cIiA/IG5ldyBNYXAoKSA6IHVuZGVmaW5lZDtcblxuICAgIF93cmFwTmF0aXZlU3VwZXIgPSBmdW5jdGlvbiBfd3JhcE5hdGl2ZVN1cGVyKENsYXNzKSB7XG4gICAgICBpZiAoQ2xhc3MgPT09IG51bGwgfHwgIV9pc05hdGl2ZUZ1bmN0aW9uKENsYXNzKSkgcmV0dXJuIENsYXNzO1xuXG4gICAgICBpZiAodHlwZW9mIENsYXNzICE9PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uXCIpO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIF9jYWNoZSAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICBpZiAoX2NhY2hlLmhhcyhDbGFzcykpIHJldHVybiBfY2FjaGUuZ2V0KENsYXNzKTtcblxuICAgICAgICBfY2FjaGUuc2V0KENsYXNzLCBXcmFwcGVyKTtcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gV3JhcHBlcigpIHtcbiAgICAgICAgcmV0dXJuIF9jb25zdHJ1Y3QoQ2xhc3MsIGFyZ3VtZW50cywgX2dldFByb3RvdHlwZU9mKHRoaXMpLmNvbnN0cnVjdG9yKTtcbiAgICAgIH1cblxuICAgICAgV3JhcHBlci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKENsYXNzLnByb3RvdHlwZSwge1xuICAgICAgICBjb25zdHJ1Y3Rvcjoge1xuICAgICAgICAgIHZhbHVlOiBXcmFwcGVyLFxuICAgICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgICAgIHdyaXRhYmxlOiB0cnVlLFxuICAgICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBfc2V0UHJvdG90eXBlT2YoV3JhcHBlciwgQ2xhc3MpO1xuICAgIH07XG5cbiAgICByZXR1cm4gX3dyYXBOYXRpdmVTdXBlcihDbGFzcyk7XG4gIH1cblxuICAvLyB0aGVzZSBhcmVuJ3QgcmVhbGx5IHByaXZhdGUsIGJ1dCBub3IgYXJlIHRoZXkgcmVhbGx5IHVzZWZ1bCB0byBkb2N1bWVudFxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgdmFyIEx1eG9uRXJyb3IgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uIChfRXJyb3IpIHtcbiAgICBfaW5oZXJpdHNMb29zZShMdXhvbkVycm9yLCBfRXJyb3IpO1xuXG4gICAgZnVuY3Rpb24gTHV4b25FcnJvcigpIHtcbiAgICAgIHJldHVybiBfRXJyb3IuYXBwbHkodGhpcywgYXJndW1lbnRzKSB8fCB0aGlzO1xuICAgIH1cblxuICAgIHJldHVybiBMdXhvbkVycm9yO1xuICB9KF93cmFwTmF0aXZlU3VwZXIoRXJyb3IpKTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG5cbiAgdmFyIEludmFsaWREYXRlVGltZUVycm9yID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoX0x1eG9uRXJyb3IpIHtcbiAgICBfaW5oZXJpdHNMb29zZShJbnZhbGlkRGF0ZVRpbWVFcnJvciwgX0x1eG9uRXJyb3IpO1xuXG4gICAgZnVuY3Rpb24gSW52YWxpZERhdGVUaW1lRXJyb3IocmVhc29uKSB7XG4gICAgICByZXR1cm4gX0x1eG9uRXJyb3IuY2FsbCh0aGlzLCBcIkludmFsaWQgRGF0ZVRpbWU6IFwiICsgcmVhc29uLnRvTWVzc2FnZSgpKSB8fCB0aGlzO1xuICAgIH1cblxuICAgIHJldHVybiBJbnZhbGlkRGF0ZVRpbWVFcnJvcjtcbiAgfShMdXhvbkVycm9yKTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHZhciBJbnZhbGlkSW50ZXJ2YWxFcnJvciA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKF9MdXhvbkVycm9yMikge1xuICAgIF9pbmhlcml0c0xvb3NlKEludmFsaWRJbnRlcnZhbEVycm9yLCBfTHV4b25FcnJvcjIpO1xuXG4gICAgZnVuY3Rpb24gSW52YWxpZEludGVydmFsRXJyb3IocmVhc29uKSB7XG4gICAgICByZXR1cm4gX0x1eG9uRXJyb3IyLmNhbGwodGhpcywgXCJJbnZhbGlkIEludGVydmFsOiBcIiArIHJlYXNvbi50b01lc3NhZ2UoKSkgfHwgdGhpcztcbiAgICB9XG5cbiAgICByZXR1cm4gSW52YWxpZEludGVydmFsRXJyb3I7XG4gIH0oTHV4b25FcnJvcik7XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB2YXIgSW52YWxpZER1cmF0aW9uRXJyb3IgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uIChfTHV4b25FcnJvcjMpIHtcbiAgICBfaW5oZXJpdHNMb29zZShJbnZhbGlkRHVyYXRpb25FcnJvciwgX0x1eG9uRXJyb3IzKTtcblxuICAgIGZ1bmN0aW9uIEludmFsaWREdXJhdGlvbkVycm9yKHJlYXNvbikge1xuICAgICAgcmV0dXJuIF9MdXhvbkVycm9yMy5jYWxsKHRoaXMsIFwiSW52YWxpZCBEdXJhdGlvbjogXCIgKyByZWFzb24udG9NZXNzYWdlKCkpIHx8IHRoaXM7XG4gICAgfVxuXG4gICAgcmV0dXJuIEludmFsaWREdXJhdGlvbkVycm9yO1xuICB9KEx1eG9uRXJyb3IpO1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdmFyIENvbmZsaWN0aW5nU3BlY2lmaWNhdGlvbkVycm9yID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoX0x1eG9uRXJyb3I0KSB7XG4gICAgX2luaGVyaXRzTG9vc2UoQ29uZmxpY3RpbmdTcGVjaWZpY2F0aW9uRXJyb3IsIF9MdXhvbkVycm9yNCk7XG5cbiAgICBmdW5jdGlvbiBDb25mbGljdGluZ1NwZWNpZmljYXRpb25FcnJvcigpIHtcbiAgICAgIHJldHVybiBfTHV4b25FcnJvcjQuYXBwbHkodGhpcywgYXJndW1lbnRzKSB8fCB0aGlzO1xuICAgIH1cblxuICAgIHJldHVybiBDb25mbGljdGluZ1NwZWNpZmljYXRpb25FcnJvcjtcbiAgfShMdXhvbkVycm9yKTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHZhciBJbnZhbGlkVW5pdEVycm9yID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoX0x1eG9uRXJyb3I1KSB7XG4gICAgX2luaGVyaXRzTG9vc2UoSW52YWxpZFVuaXRFcnJvciwgX0x1eG9uRXJyb3I1KTtcblxuICAgIGZ1bmN0aW9uIEludmFsaWRVbml0RXJyb3IodW5pdCkge1xuICAgICAgcmV0dXJuIF9MdXhvbkVycm9yNS5jYWxsKHRoaXMsIFwiSW52YWxpZCB1bml0IFwiICsgdW5pdCkgfHwgdGhpcztcbiAgICB9XG5cbiAgICByZXR1cm4gSW52YWxpZFVuaXRFcnJvcjtcbiAgfShMdXhvbkVycm9yKTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHZhciBJbnZhbGlkQXJndW1lbnRFcnJvciA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKF9MdXhvbkVycm9yNikge1xuICAgIF9pbmhlcml0c0xvb3NlKEludmFsaWRBcmd1bWVudEVycm9yLCBfTHV4b25FcnJvcjYpO1xuXG4gICAgZnVuY3Rpb24gSW52YWxpZEFyZ3VtZW50RXJyb3IoKSB7XG4gICAgICByZXR1cm4gX0x1eG9uRXJyb3I2LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykgfHwgdGhpcztcbiAgICB9XG5cbiAgICByZXR1cm4gSW52YWxpZEFyZ3VtZW50RXJyb3I7XG4gIH0oTHV4b25FcnJvcik7XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB2YXIgWm9uZUlzQWJzdHJhY3RFcnJvciA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKF9MdXhvbkVycm9yNykge1xuICAgIF9pbmhlcml0c0xvb3NlKFpvbmVJc0Fic3RyYWN0RXJyb3IsIF9MdXhvbkVycm9yNyk7XG5cbiAgICBmdW5jdGlvbiBab25lSXNBYnN0cmFjdEVycm9yKCkge1xuICAgICAgcmV0dXJuIF9MdXhvbkVycm9yNy5jYWxsKHRoaXMsIFwiWm9uZSBpcyBhbiBhYnN0cmFjdCBjbGFzc1wiKSB8fCB0aGlzO1xuICAgIH1cblxuICAgIHJldHVybiBab25lSXNBYnN0cmFjdEVycm9yO1xuICB9KEx1eG9uRXJyb3IpO1xuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgdmFyIG4gPSBcIm51bWVyaWNcIixcbiAgICAgIHMgPSBcInNob3J0XCIsXG4gICAgICBsID0gXCJsb25nXCI7XG4gIHZhciBEQVRFX1NIT1JUID0ge1xuICAgIHllYXI6IG4sXG4gICAgbW9udGg6IG4sXG4gICAgZGF5OiBuXG4gIH07XG4gIHZhciBEQVRFX01FRCA9IHtcbiAgICB5ZWFyOiBuLFxuICAgIG1vbnRoOiBzLFxuICAgIGRheTogblxuICB9O1xuICB2YXIgREFURV9GVUxMID0ge1xuICAgIHllYXI6IG4sXG4gICAgbW9udGg6IGwsXG4gICAgZGF5OiBuXG4gIH07XG4gIHZhciBEQVRFX0hVR0UgPSB7XG4gICAgeWVhcjogbixcbiAgICBtb250aDogbCxcbiAgICBkYXk6IG4sXG4gICAgd2Vla2RheTogbFxuICB9O1xuICB2YXIgVElNRV9TSU1QTEUgPSB7XG4gICAgaG91cjogbixcbiAgICBtaW51dGU6IG5cbiAgfTtcbiAgdmFyIFRJTUVfV0lUSF9TRUNPTkRTID0ge1xuICAgIGhvdXI6IG4sXG4gICAgbWludXRlOiBuLFxuICAgIHNlY29uZDogblxuICB9O1xuICB2YXIgVElNRV9XSVRIX1NIT1JUX09GRlNFVCA9IHtcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICBzZWNvbmQ6IG4sXG4gICAgdGltZVpvbmVOYW1lOiBzXG4gIH07XG4gIHZhciBUSU1FX1dJVEhfTE9OR19PRkZTRVQgPSB7XG4gICAgaG91cjogbixcbiAgICBtaW51dGU6IG4sXG4gICAgc2Vjb25kOiBuLFxuICAgIHRpbWVab25lTmFtZTogbFxuICB9O1xuICB2YXIgVElNRV8yNF9TSU1QTEUgPSB7XG4gICAgaG91cjogbixcbiAgICBtaW51dGU6IG4sXG4gICAgaG91cjEyOiBmYWxzZVxuICB9O1xuICAvKipcbiAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfTsgZm9ybWF0IGxpa2UgJzA5OjMwOjIzJywgYWx3YXlzIDI0LWhvdXIuXG4gICAqL1xuXG4gIHZhciBUSU1FXzI0X1dJVEhfU0VDT05EUyA9IHtcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICBzZWNvbmQ6IG4sXG4gICAgaG91cjEyOiBmYWxzZVxuICB9O1xuICAvKipcbiAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfTsgZm9ybWF0IGxpa2UgJzA5OjMwOjIzIEVEVCcsIGFsd2F5cyAyNC1ob3VyLlxuICAgKi9cblxuICB2YXIgVElNRV8yNF9XSVRIX1NIT1JUX09GRlNFVCA9IHtcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICBzZWNvbmQ6IG4sXG4gICAgaG91cjEyOiBmYWxzZSxcbiAgICB0aW1lWm9uZU5hbWU6IHNcbiAgfTtcbiAgLyoqXG4gICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ307IGZvcm1hdCBsaWtlICcwOTozMDoyMyBFYXN0ZXJuIERheWxpZ2h0IFRpbWUnLCBhbHdheXMgMjQtaG91ci5cbiAgICovXG5cbiAgdmFyIFRJTUVfMjRfV0lUSF9MT05HX09GRlNFVCA9IHtcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICBzZWNvbmQ6IG4sXG4gICAgaG91cjEyOiBmYWxzZSxcbiAgICB0aW1lWm9uZU5hbWU6IGxcbiAgfTtcbiAgLyoqXG4gICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ307IGZvcm1hdCBsaWtlICcxMC8xNC8xOTgzLCA5OjMwIEFNJy4gT25seSAxMi1ob3VyIGlmIHRoZSBsb2NhbGUgaXMuXG4gICAqL1xuXG4gIHZhciBEQVRFVElNRV9TSE9SVCA9IHtcbiAgICB5ZWFyOiBuLFxuICAgIG1vbnRoOiBuLFxuICAgIGRheTogbixcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogblxuICB9O1xuICAvKipcbiAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfTsgZm9ybWF0IGxpa2UgJzEwLzE0LzE5ODMsIDk6MzA6MzMgQU0nLiBPbmx5IDEyLWhvdXIgaWYgdGhlIGxvY2FsZSBpcy5cbiAgICovXG5cbiAgdmFyIERBVEVUSU1FX1NIT1JUX1dJVEhfU0VDT05EUyA9IHtcbiAgICB5ZWFyOiBuLFxuICAgIG1vbnRoOiBuLFxuICAgIGRheTogbixcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICBzZWNvbmQ6IG5cbiAgfTtcbiAgdmFyIERBVEVUSU1FX01FRCA9IHtcbiAgICB5ZWFyOiBuLFxuICAgIG1vbnRoOiBzLFxuICAgIGRheTogbixcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogblxuICB9O1xuICB2YXIgREFURVRJTUVfTUVEX1dJVEhfU0VDT05EUyA9IHtcbiAgICB5ZWFyOiBuLFxuICAgIG1vbnRoOiBzLFxuICAgIGRheTogbixcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICBzZWNvbmQ6IG5cbiAgfTtcbiAgdmFyIERBVEVUSU1FX01FRF9XSVRIX1dFRUtEQVkgPSB7XG4gICAgeWVhcjogbixcbiAgICBtb250aDogcyxcbiAgICBkYXk6IG4sXG4gICAgd2Vla2RheTogcyxcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogblxuICB9O1xuICB2YXIgREFURVRJTUVfRlVMTCA9IHtcbiAgICB5ZWFyOiBuLFxuICAgIG1vbnRoOiBsLFxuICAgIGRheTogbixcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICB0aW1lWm9uZU5hbWU6IHNcbiAgfTtcbiAgdmFyIERBVEVUSU1FX0ZVTExfV0lUSF9TRUNPTkRTID0ge1xuICAgIHllYXI6IG4sXG4gICAgbW9udGg6IGwsXG4gICAgZGF5OiBuLFxuICAgIGhvdXI6IG4sXG4gICAgbWludXRlOiBuLFxuICAgIHNlY29uZDogbixcbiAgICB0aW1lWm9uZU5hbWU6IHNcbiAgfTtcbiAgdmFyIERBVEVUSU1FX0hVR0UgPSB7XG4gICAgeWVhcjogbixcbiAgICBtb250aDogbCxcbiAgICBkYXk6IG4sXG4gICAgd2Vla2RheTogbCxcbiAgICBob3VyOiBuLFxuICAgIG1pbnV0ZTogbixcbiAgICB0aW1lWm9uZU5hbWU6IGxcbiAgfTtcbiAgdmFyIERBVEVUSU1FX0hVR0VfV0lUSF9TRUNPTkRTID0ge1xuICAgIHllYXI6IG4sXG4gICAgbW9udGg6IGwsXG4gICAgZGF5OiBuLFxuICAgIHdlZWtkYXk6IGwsXG4gICAgaG91cjogbixcbiAgICBtaW51dGU6IG4sXG4gICAgc2Vjb25kOiBuLFxuICAgIHRpbWVab25lTmFtZTogbFxuICB9O1xuXG4gIC8qXG4gICAgVGhpcyBpcyBqdXN0IGEganVuayBkcmF3ZXIsIGNvbnRhaW5pbmcgYW55dGhpbmcgdXNlZCBhY3Jvc3MgbXVsdGlwbGUgY2xhc3Nlcy5cbiAgICBCZWNhdXNlIEx1eG9uIGlzIHNtYWxsKGlzaCksIHRoaXMgc2hvdWxkIHN0YXkgc21hbGwgYW5kIHdlIHdvbid0IHdvcnJ5IGFib3V0IHNwbGl0dGluZ1xuICAgIGl0IHVwIGludG8sIHNheSwgcGFyc2luZ1V0aWwuanMgYW5kIGJhc2ljVXRpbC5qcyBhbmQgc28gb24uIEJ1dCB0aGV5IGFyZSBkaXZpZGVkIHVwIGJ5IGZlYXR1cmUgYXJlYS5cbiAgKi9cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICAvLyBUWVBFU1xuXG4gIGZ1bmN0aW9uIGlzVW5kZWZpbmVkKG8pIHtcbiAgICByZXR1cm4gdHlwZW9mIG8gPT09IFwidW5kZWZpbmVkXCI7XG4gIH1cbiAgZnVuY3Rpb24gaXNOdW1iZXIobykge1xuICAgIHJldHVybiB0eXBlb2YgbyA9PT0gXCJudW1iZXJcIjtcbiAgfVxuICBmdW5jdGlvbiBpc0ludGVnZXIobykge1xuICAgIHJldHVybiB0eXBlb2YgbyA9PT0gXCJudW1iZXJcIiAmJiBvICUgMSA9PT0gMDtcbiAgfVxuICBmdW5jdGlvbiBpc1N0cmluZyhvKSB7XG4gICAgcmV0dXJuIHR5cGVvZiBvID09PSBcInN0cmluZ1wiO1xuICB9XG4gIGZ1bmN0aW9uIGlzRGF0ZShvKSB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvKSA9PT0gXCJbb2JqZWN0IERhdGVdXCI7XG4gIH0gLy8gQ0FQQUJJTElUSUVTXG5cbiAgZnVuY3Rpb24gaGFzSW50bCgpIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIHR5cGVvZiBJbnRsICE9PSBcInVuZGVmaW5lZFwiICYmIEludGwuRGF0ZVRpbWVGb3JtYXQ7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiBoYXNGb3JtYXRUb1BhcnRzKCkge1xuICAgIHJldHVybiAhaXNVbmRlZmluZWQoSW50bC5EYXRlVGltZUZvcm1hdC5wcm90b3R5cGUuZm9ybWF0VG9QYXJ0cyk7XG4gIH1cbiAgZnVuY3Rpb24gaGFzUmVsYXRpdmUoKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiB0eXBlb2YgSW50bCAhPT0gXCJ1bmRlZmluZWRcIiAmJiAhIUludGwuUmVsYXRpdmVUaW1lRm9ybWF0O1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gIH0gLy8gT0JKRUNUUyBBTkQgQVJSQVlTXG5cbiAgZnVuY3Rpb24gbWF5YmVBcnJheSh0aGluZykge1xuICAgIHJldHVybiBBcnJheS5pc0FycmF5KHRoaW5nKSA/IHRoaW5nIDogW3RoaW5nXTtcbiAgfVxuICBmdW5jdGlvbiBiZXN0QnkoYXJyLCBieSwgY29tcGFyZSkge1xuICAgIGlmIChhcnIubGVuZ3RoID09PSAwKSB7XG4gICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIHJldHVybiBhcnIucmVkdWNlKGZ1bmN0aW9uIChiZXN0LCBuZXh0KSB7XG4gICAgICB2YXIgcGFpciA9IFtieShuZXh0KSwgbmV4dF07XG5cbiAgICAgIGlmICghYmVzdCkge1xuICAgICAgICByZXR1cm4gcGFpcjtcbiAgICAgIH0gZWxzZSBpZiAoY29tcGFyZShiZXN0WzBdLCBwYWlyWzBdKSA9PT0gYmVzdFswXSkge1xuICAgICAgICByZXR1cm4gYmVzdDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBwYWlyO1xuICAgICAgfVxuICAgIH0sIG51bGwpWzFdO1xuICB9XG4gIGZ1bmN0aW9uIHBpY2sob2JqLCBrZXlzKSB7XG4gICAgcmV0dXJuIGtleXMucmVkdWNlKGZ1bmN0aW9uIChhLCBrKSB7XG4gICAgICBhW2tdID0gb2JqW2tdO1xuICAgICAgcmV0dXJuIGE7XG4gICAgfSwge30pO1xuICB9XG4gIGZ1bmN0aW9uIGhhc093blByb3BlcnR5KG9iaiwgcHJvcCkge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKTtcbiAgfSAvLyBOVU1CRVJTIEFORCBTVFJJTkdTXG5cbiAgZnVuY3Rpb24gaW50ZWdlckJldHdlZW4odGhpbmcsIGJvdHRvbSwgdG9wKSB7XG4gICAgcmV0dXJuIGlzSW50ZWdlcih0aGluZykgJiYgdGhpbmcgPj0gYm90dG9tICYmIHRoaW5nIDw9IHRvcDtcbiAgfSAvLyB4ICUgbiBidXQgdGFrZXMgdGhlIHNpZ24gb2YgbiBpbnN0ZWFkIG9mIHhcblxuICBmdW5jdGlvbiBmbG9vck1vZCh4LCBuKSB7XG4gICAgcmV0dXJuIHggLSBuICogTWF0aC5mbG9vcih4IC8gbik7XG4gIH1cbiAgZnVuY3Rpb24gcGFkU3RhcnQoaW5wdXQsIG4pIHtcbiAgICBpZiAobiA9PT0gdm9pZCAwKSB7XG4gICAgICBuID0gMjtcbiAgICB9XG5cbiAgICBpZiAoaW5wdXQudG9TdHJpbmcoKS5sZW5ndGggPCBuKSB7XG4gICAgICByZXR1cm4gKFwiMFwiLnJlcGVhdChuKSArIGlucHV0KS5zbGljZSgtbik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBpbnB1dC50b1N0cmluZygpO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiBwYXJzZUludGVnZXIoc3RyaW5nKSB7XG4gICAgaWYgKGlzVW5kZWZpbmVkKHN0cmluZykgfHwgc3RyaW5nID09PSBudWxsIHx8IHN0cmluZyA9PT0gXCJcIikge1xuICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHBhcnNlSW50KHN0cmluZywgMTApO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiBwYXJzZU1pbGxpcyhmcmFjdGlvbikge1xuICAgIC8vIFJldHVybiB1bmRlZmluZWQgKGluc3RlYWQgb2YgMCkgaW4gdGhlc2UgY2FzZXMsIHdoZXJlIGZyYWN0aW9uIGlzIG5vdCBzZXRcbiAgICBpZiAoaXNVbmRlZmluZWQoZnJhY3Rpb24pIHx8IGZyYWN0aW9uID09PSBudWxsIHx8IGZyYWN0aW9uID09PSBcIlwiKSB7XG4gICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgZiA9IHBhcnNlRmxvYXQoXCIwLlwiICsgZnJhY3Rpb24pICogMTAwMDtcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKGYpO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiByb3VuZFRvKG51bWJlciwgZGlnaXRzLCB0b3dhcmRaZXJvKSB7XG4gICAgaWYgKHRvd2FyZFplcm8gPT09IHZvaWQgMCkge1xuICAgICAgdG93YXJkWmVybyA9IGZhbHNlO1xuICAgIH1cblxuICAgIHZhciBmYWN0b3IgPSBNYXRoLnBvdygxMCwgZGlnaXRzKSxcbiAgICAgICAgcm91bmRlciA9IHRvd2FyZFplcm8gPyBNYXRoLnRydW5jIDogTWF0aC5yb3VuZDtcbiAgICByZXR1cm4gcm91bmRlcihudW1iZXIgKiBmYWN0b3IpIC8gZmFjdG9yO1xuICB9IC8vIERBVEUgQkFTSUNTXG5cbiAgZnVuY3Rpb24gaXNMZWFwWWVhcih5ZWFyKSB7XG4gICAgcmV0dXJuIHllYXIgJSA0ID09PSAwICYmICh5ZWFyICUgMTAwICE9PSAwIHx8IHllYXIgJSA0MDAgPT09IDApO1xuICB9XG4gIGZ1bmN0aW9uIGRheXNJblllYXIoeWVhcikge1xuICAgIHJldHVybiBpc0xlYXBZZWFyKHllYXIpID8gMzY2IDogMzY1O1xuICB9XG4gIGZ1bmN0aW9uIGRheXNJbk1vbnRoKHllYXIsIG1vbnRoKSB7XG4gICAgdmFyIG1vZE1vbnRoID0gZmxvb3JNb2QobW9udGggLSAxLCAxMikgKyAxLFxuICAgICAgICBtb2RZZWFyID0geWVhciArIChtb250aCAtIG1vZE1vbnRoKSAvIDEyO1xuXG4gICAgaWYgKG1vZE1vbnRoID09PSAyKSB7XG4gICAgICByZXR1cm4gaXNMZWFwWWVhcihtb2RZZWFyKSA/IDI5IDogMjg7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBbMzEsIG51bGwsIDMxLCAzMCwgMzEsIDMwLCAzMSwgMzEsIDMwLCAzMSwgMzAsIDMxXVttb2RNb250aCAtIDFdO1xuICAgIH1cbiAgfSAvLyBjb3ZlcnQgYSBjYWxlbmRhciBvYmplY3QgdG8gYSBsb2NhbCB0aW1lc3RhbXAgKGVwb2NoLCBidXQgd2l0aCB0aGUgb2Zmc2V0IGJha2VkIGluKVxuXG4gIGZ1bmN0aW9uIG9ialRvTG9jYWxUUyhvYmopIHtcbiAgICB2YXIgZCA9IERhdGUuVVRDKG9iai55ZWFyLCBvYmoubW9udGggLSAxLCBvYmouZGF5LCBvYmouaG91ciwgb2JqLm1pbnV0ZSwgb2JqLnNlY29uZCwgb2JqLm1pbGxpc2Vjb25kKTsgLy8gZm9yIGxlZ2FjeSByZWFzb25zLCB5ZWFycyBiZXR3ZWVuIDAgYW5kIDk5IGFyZSBpbnRlcnByZXRlZCBhcyAxOVhYOyByZXZlcnQgdGhhdFxuXG4gICAgaWYgKG9iai55ZWFyIDwgMTAwICYmIG9iai55ZWFyID49IDApIHtcbiAgICAgIGQgPSBuZXcgRGF0ZShkKTtcbiAgICAgIGQuc2V0VVRDRnVsbFllYXIoZC5nZXRVVENGdWxsWWVhcigpIC0gMTkwMCk7XG4gICAgfVxuXG4gICAgcmV0dXJuICtkO1xuICB9XG4gIGZ1bmN0aW9uIHdlZWtzSW5XZWVrWWVhcih3ZWVrWWVhcikge1xuICAgIHZhciBwMSA9ICh3ZWVrWWVhciArIE1hdGguZmxvb3Iod2Vla1llYXIgLyA0KSAtIE1hdGguZmxvb3Iod2Vla1llYXIgLyAxMDApICsgTWF0aC5mbG9vcih3ZWVrWWVhciAvIDQwMCkpICUgNyxcbiAgICAgICAgbGFzdCA9IHdlZWtZZWFyIC0gMSxcbiAgICAgICAgcDIgPSAobGFzdCArIE1hdGguZmxvb3IobGFzdCAvIDQpIC0gTWF0aC5mbG9vcihsYXN0IC8gMTAwKSArIE1hdGguZmxvb3IobGFzdCAvIDQwMCkpICUgNztcbiAgICByZXR1cm4gcDEgPT09IDQgfHwgcDIgPT09IDMgPyA1MyA6IDUyO1xuICB9XG4gIGZ1bmN0aW9uIHVudHJ1bmNhdGVZZWFyKHllYXIpIHtcbiAgICBpZiAoeWVhciA+IDk5KSB7XG4gICAgICByZXR1cm4geWVhcjtcbiAgICB9IGVsc2UgcmV0dXJuIHllYXIgPiA2MCA/IDE5MDAgKyB5ZWFyIDogMjAwMCArIHllYXI7XG4gIH0gLy8gUEFSU0lOR1xuXG4gIGZ1bmN0aW9uIHBhcnNlWm9uZUluZm8odHMsIG9mZnNldEZvcm1hdCwgbG9jYWxlLCB0aW1lWm9uZSkge1xuICAgIGlmICh0aW1lWm9uZSA9PT0gdm9pZCAwKSB7XG4gICAgICB0aW1lWm9uZSA9IG51bGw7XG4gICAgfVxuXG4gICAgdmFyIGRhdGUgPSBuZXcgRGF0ZSh0cyksXG4gICAgICAgIGludGxPcHRzID0ge1xuICAgICAgaG91cjEyOiBmYWxzZSxcbiAgICAgIHllYXI6IFwibnVtZXJpY1wiLFxuICAgICAgbW9udGg6IFwiMi1kaWdpdFwiLFxuICAgICAgZGF5OiBcIjItZGlnaXRcIixcbiAgICAgIGhvdXI6IFwiMi1kaWdpdFwiLFxuICAgICAgbWludXRlOiBcIjItZGlnaXRcIlxuICAgIH07XG5cbiAgICBpZiAodGltZVpvbmUpIHtcbiAgICAgIGludGxPcHRzLnRpbWVab25lID0gdGltZVpvbmU7XG4gICAgfVxuXG4gICAgdmFyIG1vZGlmaWVkID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICB0aW1lWm9uZU5hbWU6IG9mZnNldEZvcm1hdFxuICAgIH0sIGludGxPcHRzKSxcbiAgICAgICAgaW50bCA9IGhhc0ludGwoKTtcblxuICAgIGlmIChpbnRsICYmIGhhc0Zvcm1hdFRvUGFydHMoKSkge1xuICAgICAgdmFyIHBhcnNlZCA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KGxvY2FsZSwgbW9kaWZpZWQpLmZvcm1hdFRvUGFydHMoZGF0ZSkuZmluZChmdW5jdGlvbiAobSkge1xuICAgICAgICByZXR1cm4gbS50eXBlLnRvTG93ZXJDYXNlKCkgPT09IFwidGltZXpvbmVuYW1lXCI7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBwYXJzZWQgPyBwYXJzZWQudmFsdWUgOiBudWxsO1xuICAgIH0gZWxzZSBpZiAoaW50bCkge1xuICAgICAgLy8gdGhpcyBwcm9iYWJseSBkb2Vzbid0IHdvcmsgZm9yIGFsbCBsb2NhbGVzXG4gICAgICB2YXIgd2l0aG91dCA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KGxvY2FsZSwgaW50bE9wdHMpLmZvcm1hdChkYXRlKSxcbiAgICAgICAgICBpbmNsdWRlZCA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KGxvY2FsZSwgbW9kaWZpZWQpLmZvcm1hdChkYXRlKSxcbiAgICAgICAgICBkaWZmZWQgPSBpbmNsdWRlZC5zdWJzdHJpbmcod2l0aG91dC5sZW5ndGgpLFxuICAgICAgICAgIHRyaW1tZWQgPSBkaWZmZWQucmVwbGFjZSgvXlssIFxcdTIwMGVdKy8sIFwiXCIpO1xuICAgICAgcmV0dXJuIHRyaW1tZWQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfSAvLyBzaWduZWRPZmZzZXQoJy01JywgJzMwJykgLT4gLTMzMFxuXG4gIGZ1bmN0aW9uIHNpZ25lZE9mZnNldChvZmZIb3VyU3RyLCBvZmZNaW51dGVTdHIpIHtcbiAgICB2YXIgb2ZmSG91ciA9IHBhcnNlSW50KG9mZkhvdXJTdHIsIDEwKTsgLy8gZG9uJ3QgfHwgdGhpcyBiZWNhdXNlIHdlIHdhbnQgdG8gcHJlc2VydmUgLTBcblxuICAgIGlmIChOdW1iZXIuaXNOYU4ob2ZmSG91cikpIHtcbiAgICAgIG9mZkhvdXIgPSAwO1xuICAgIH1cblxuICAgIHZhciBvZmZNaW4gPSBwYXJzZUludChvZmZNaW51dGVTdHIsIDEwKSB8fCAwLFxuICAgICAgICBvZmZNaW5TaWduZWQgPSBvZmZIb3VyIDwgMCB8fCBPYmplY3QuaXMob2ZmSG91ciwgLTApID8gLW9mZk1pbiA6IG9mZk1pbjtcbiAgICByZXR1cm4gb2ZmSG91ciAqIDYwICsgb2ZmTWluU2lnbmVkO1xuICB9IC8vIENPRVJDSU9OXG5cbiAgZnVuY3Rpb24gYXNOdW1iZXIodmFsdWUpIHtcbiAgICB2YXIgbnVtZXJpY1ZhbHVlID0gTnVtYmVyKHZhbHVlKTtcbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSBcImJvb2xlYW5cIiB8fCB2YWx1ZSA9PT0gXCJcIiB8fCBOdW1iZXIuaXNOYU4obnVtZXJpY1ZhbHVlKSkgdGhyb3cgbmV3IEludmFsaWRBcmd1bWVudEVycm9yKFwiSW52YWxpZCB1bml0IHZhbHVlIFwiICsgdmFsdWUpO1xuICAgIHJldHVybiBudW1lcmljVmFsdWU7XG4gIH1cbiAgZnVuY3Rpb24gbm9ybWFsaXplT2JqZWN0KG9iaiwgbm9ybWFsaXplciwgbm9uVW5pdEtleXMpIHtcbiAgICB2YXIgbm9ybWFsaXplZCA9IHt9O1xuXG4gICAgZm9yICh2YXIgdSBpbiBvYmopIHtcbiAgICAgIGlmIChoYXNPd25Qcm9wZXJ0eShvYmosIHUpKSB7XG4gICAgICAgIGlmIChub25Vbml0S2V5cy5pbmRleE9mKHUpID49IDApIGNvbnRpbnVlO1xuICAgICAgICB2YXIgdiA9IG9ialt1XTtcbiAgICAgICAgaWYgKHYgPT09IHVuZGVmaW5lZCB8fCB2ID09PSBudWxsKSBjb250aW51ZTtcbiAgICAgICAgbm9ybWFsaXplZFtub3JtYWxpemVyKHUpXSA9IGFzTnVtYmVyKHYpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBub3JtYWxpemVkO1xuICB9XG4gIGZ1bmN0aW9uIGZvcm1hdE9mZnNldChvZmZzZXQsIGZvcm1hdCkge1xuICAgIHZhciBob3VycyA9IE1hdGgudHJ1bmMob2Zmc2V0IC8gNjApLFxuICAgICAgICBtaW51dGVzID0gTWF0aC5hYnMob2Zmc2V0ICUgNjApLFxuICAgICAgICBzaWduID0gaG91cnMgPj0gMCAmJiAhT2JqZWN0LmlzKGhvdXJzLCAtMCkgPyBcIitcIiA6IFwiLVwiLFxuICAgICAgICBiYXNlID0gXCJcIiArIHNpZ24gKyBNYXRoLmFicyhob3Vycyk7XG5cbiAgICBzd2l0Y2ggKGZvcm1hdCkge1xuICAgICAgY2FzZSBcInNob3J0XCI6XG4gICAgICAgIHJldHVybiBcIlwiICsgc2lnbiArIHBhZFN0YXJ0KE1hdGguYWJzKGhvdXJzKSwgMikgKyBcIjpcIiArIHBhZFN0YXJ0KG1pbnV0ZXMsIDIpO1xuXG4gICAgICBjYXNlIFwibmFycm93XCI6XG4gICAgICAgIHJldHVybiBtaW51dGVzID4gMCA/IGJhc2UgKyBcIjpcIiArIG1pbnV0ZXMgOiBiYXNlO1xuXG4gICAgICBjYXNlIFwidGVjaGllXCI6XG4gICAgICAgIHJldHVybiBcIlwiICsgc2lnbiArIHBhZFN0YXJ0KE1hdGguYWJzKGhvdXJzKSwgMikgKyBwYWRTdGFydChtaW51dGVzLCAyKTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoXCJWYWx1ZSBmb3JtYXQgXCIgKyBmb3JtYXQgKyBcIiBpcyBvdXQgb2YgcmFuZ2UgZm9yIHByb3BlcnR5IGZvcm1hdFwiKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gdGltZU9iamVjdChvYmopIHtcbiAgICByZXR1cm4gcGljayhvYmosIFtcImhvdXJcIiwgXCJtaW51dGVcIiwgXCJzZWNvbmRcIiwgXCJtaWxsaXNlY29uZFwiXSk7XG4gIH1cbiAgdmFyIGlhbmFSZWdleCA9IC9bQS1aYS16XystXXsxLDI1Nn0oOj9cXC9bQS1aYS16XystXXsxLDI1Nn0oXFwvW0EtWmEtel8rLV17MSwyNTZ9KT8pPy87XG5cbiAgZnVuY3Rpb24gc3RyaW5naWZ5KG9iaikge1xuICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShvYmosIE9iamVjdC5rZXlzKG9iaikuc29ydCgpKTtcbiAgfVxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG5cblxuICB2YXIgbW9udGhzTG9uZyA9IFtcIkphbnVhcnlcIiwgXCJGZWJydWFyeVwiLCBcIk1hcmNoXCIsIFwiQXByaWxcIiwgXCJNYXlcIiwgXCJKdW5lXCIsIFwiSnVseVwiLCBcIkF1Z3VzdFwiLCBcIlNlcHRlbWJlclwiLCBcIk9jdG9iZXJcIiwgXCJOb3ZlbWJlclwiLCBcIkRlY2VtYmVyXCJdO1xuICB2YXIgbW9udGhzU2hvcnQgPSBbXCJKYW5cIiwgXCJGZWJcIiwgXCJNYXJcIiwgXCJBcHJcIiwgXCJNYXlcIiwgXCJKdW5cIiwgXCJKdWxcIiwgXCJBdWdcIiwgXCJTZXBcIiwgXCJPY3RcIiwgXCJOb3ZcIiwgXCJEZWNcIl07XG4gIHZhciBtb250aHNOYXJyb3cgPSBbXCJKXCIsIFwiRlwiLCBcIk1cIiwgXCJBXCIsIFwiTVwiLCBcIkpcIiwgXCJKXCIsIFwiQVwiLCBcIlNcIiwgXCJPXCIsIFwiTlwiLCBcIkRcIl07XG4gIGZ1bmN0aW9uIG1vbnRocyhsZW5ndGgpIHtcbiAgICBzd2l0Y2ggKGxlbmd0aCkge1xuICAgICAgY2FzZSBcIm5hcnJvd1wiOlxuICAgICAgICByZXR1cm4gbW9udGhzTmFycm93O1xuXG4gICAgICBjYXNlIFwic2hvcnRcIjpcbiAgICAgICAgcmV0dXJuIG1vbnRoc1Nob3J0O1xuXG4gICAgICBjYXNlIFwibG9uZ1wiOlxuICAgICAgICByZXR1cm4gbW9udGhzTG9uZztcblxuICAgICAgY2FzZSBcIm51bWVyaWNcIjpcbiAgICAgICAgcmV0dXJuIFtcIjFcIiwgXCIyXCIsIFwiM1wiLCBcIjRcIiwgXCI1XCIsIFwiNlwiLCBcIjdcIiwgXCI4XCIsIFwiOVwiLCBcIjEwXCIsIFwiMTFcIiwgXCIxMlwiXTtcblxuICAgICAgY2FzZSBcIjItZGlnaXRcIjpcbiAgICAgICAgcmV0dXJuIFtcIjAxXCIsIFwiMDJcIiwgXCIwM1wiLCBcIjA0XCIsIFwiMDVcIiwgXCIwNlwiLCBcIjA3XCIsIFwiMDhcIiwgXCIwOVwiLCBcIjEwXCIsIFwiMTFcIiwgXCIxMlwiXTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG4gIHZhciB3ZWVrZGF5c0xvbmcgPSBbXCJNb25kYXlcIiwgXCJUdWVzZGF5XCIsIFwiV2VkbmVzZGF5XCIsIFwiVGh1cnNkYXlcIiwgXCJGcmlkYXlcIiwgXCJTYXR1cmRheVwiLCBcIlN1bmRheVwiXTtcbiAgdmFyIHdlZWtkYXlzU2hvcnQgPSBbXCJNb25cIiwgXCJUdWVcIiwgXCJXZWRcIiwgXCJUaHVcIiwgXCJGcmlcIiwgXCJTYXRcIiwgXCJTdW5cIl07XG4gIHZhciB3ZWVrZGF5c05hcnJvdyA9IFtcIk1cIiwgXCJUXCIsIFwiV1wiLCBcIlRcIiwgXCJGXCIsIFwiU1wiLCBcIlNcIl07XG4gIGZ1bmN0aW9uIHdlZWtkYXlzKGxlbmd0aCkge1xuICAgIHN3aXRjaCAobGVuZ3RoKSB7XG4gICAgICBjYXNlIFwibmFycm93XCI6XG4gICAgICAgIHJldHVybiB3ZWVrZGF5c05hcnJvdztcblxuICAgICAgY2FzZSBcInNob3J0XCI6XG4gICAgICAgIHJldHVybiB3ZWVrZGF5c1Nob3J0O1xuXG4gICAgICBjYXNlIFwibG9uZ1wiOlxuICAgICAgICByZXR1cm4gd2Vla2RheXNMb25nO1xuXG4gICAgICBjYXNlIFwibnVtZXJpY1wiOlxuICAgICAgICByZXR1cm4gW1wiMVwiLCBcIjJcIiwgXCIzXCIsIFwiNFwiLCBcIjVcIiwgXCI2XCIsIFwiN1wiXTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG4gIHZhciBtZXJpZGllbXMgPSBbXCJBTVwiLCBcIlBNXCJdO1xuICB2YXIgZXJhc0xvbmcgPSBbXCJCZWZvcmUgQ2hyaXN0XCIsIFwiQW5ubyBEb21pbmlcIl07XG4gIHZhciBlcmFzU2hvcnQgPSBbXCJCQ1wiLCBcIkFEXCJdO1xuICB2YXIgZXJhc05hcnJvdyA9IFtcIkJcIiwgXCJBXCJdO1xuICBmdW5jdGlvbiBlcmFzKGxlbmd0aCkge1xuICAgIHN3aXRjaCAobGVuZ3RoKSB7XG4gICAgICBjYXNlIFwibmFycm93XCI6XG4gICAgICAgIHJldHVybiBlcmFzTmFycm93O1xuXG4gICAgICBjYXNlIFwic2hvcnRcIjpcbiAgICAgICAgcmV0dXJuIGVyYXNTaG9ydDtcblxuICAgICAgY2FzZSBcImxvbmdcIjpcbiAgICAgICAgcmV0dXJuIGVyYXNMb25nO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gbWVyaWRpZW1Gb3JEYXRlVGltZShkdCkge1xuICAgIHJldHVybiBtZXJpZGllbXNbZHQuaG91ciA8IDEyID8gMCA6IDFdO1xuICB9XG4gIGZ1bmN0aW9uIHdlZWtkYXlGb3JEYXRlVGltZShkdCwgbGVuZ3RoKSB7XG4gICAgcmV0dXJuIHdlZWtkYXlzKGxlbmd0aClbZHQud2Vla2RheSAtIDFdO1xuICB9XG4gIGZ1bmN0aW9uIG1vbnRoRm9yRGF0ZVRpbWUoZHQsIGxlbmd0aCkge1xuICAgIHJldHVybiBtb250aHMobGVuZ3RoKVtkdC5tb250aCAtIDFdO1xuICB9XG4gIGZ1bmN0aW9uIGVyYUZvckRhdGVUaW1lKGR0LCBsZW5ndGgpIHtcbiAgICByZXR1cm4gZXJhcyhsZW5ndGgpW2R0LnllYXIgPCAwID8gMCA6IDFdO1xuICB9XG4gIGZ1bmN0aW9uIGZvcm1hdFJlbGF0aXZlVGltZSh1bml0LCBjb3VudCwgbnVtZXJpYywgbmFycm93KSB7XG4gICAgaWYgKG51bWVyaWMgPT09IHZvaWQgMCkge1xuICAgICAgbnVtZXJpYyA9IFwiYWx3YXlzXCI7XG4gICAgfVxuXG4gICAgaWYgKG5hcnJvdyA9PT0gdm9pZCAwKSB7XG4gICAgICBuYXJyb3cgPSBmYWxzZTtcbiAgICB9XG5cbiAgICB2YXIgdW5pdHMgPSB7XG4gICAgICB5ZWFyczogW1wieWVhclwiLCBcInlyLlwiXSxcbiAgICAgIHF1YXJ0ZXJzOiBbXCJxdWFydGVyXCIsIFwicXRyLlwiXSxcbiAgICAgIG1vbnRoczogW1wibW9udGhcIiwgXCJtby5cIl0sXG4gICAgICB3ZWVrczogW1wid2Vla1wiLCBcIndrLlwiXSxcbiAgICAgIGRheXM6IFtcImRheVwiLCBcImRheVwiLCBcImRheXNcIl0sXG4gICAgICBob3VyczogW1wiaG91clwiLCBcImhyLlwiXSxcbiAgICAgIG1pbnV0ZXM6IFtcIm1pbnV0ZVwiLCBcIm1pbi5cIl0sXG4gICAgICBzZWNvbmRzOiBbXCJzZWNvbmRcIiwgXCJzZWMuXCJdXG4gICAgfTtcbiAgICB2YXIgbGFzdGFibGUgPSBbXCJob3Vyc1wiLCBcIm1pbnV0ZXNcIiwgXCJzZWNvbmRzXCJdLmluZGV4T2YodW5pdCkgPT09IC0xO1xuXG4gICAgaWYgKG51bWVyaWMgPT09IFwiYXV0b1wiICYmIGxhc3RhYmxlKSB7XG4gICAgICB2YXIgaXNEYXkgPSB1bml0ID09PSBcImRheXNcIjtcblxuICAgICAgc3dpdGNoIChjb3VudCkge1xuICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgcmV0dXJuIGlzRGF5ID8gXCJ0b21vcnJvd1wiIDogXCJuZXh0IFwiICsgdW5pdHNbdW5pdF1bMF07XG5cbiAgICAgICAgY2FzZSAtMTpcbiAgICAgICAgICByZXR1cm4gaXNEYXkgPyBcInllc3RlcmRheVwiIDogXCJsYXN0IFwiICsgdW5pdHNbdW5pdF1bMF07XG5cbiAgICAgICAgY2FzZSAwOlxuICAgICAgICAgIHJldHVybiBpc0RheSA/IFwidG9kYXlcIiA6IFwidGhpcyBcIiArIHVuaXRzW3VuaXRdWzBdO1xuXG4gICAgICAgIGRlZmF1bHQ6IC8vIGZhbGwgdGhyb3VnaFxuXG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIGlzSW5QYXN0ID0gT2JqZWN0LmlzKGNvdW50LCAtMCkgfHwgY291bnQgPCAwLFxuICAgICAgICBmbXRWYWx1ZSA9IE1hdGguYWJzKGNvdW50KSxcbiAgICAgICAgc2luZ3VsYXIgPSBmbXRWYWx1ZSA9PT0gMSxcbiAgICAgICAgbGlsVW5pdHMgPSB1bml0c1t1bml0XSxcbiAgICAgICAgZm10VW5pdCA9IG5hcnJvdyA/IHNpbmd1bGFyID8gbGlsVW5pdHNbMV0gOiBsaWxVbml0c1syXSB8fCBsaWxVbml0c1sxXSA6IHNpbmd1bGFyID8gdW5pdHNbdW5pdF1bMF0gOiB1bml0O1xuICAgIHJldHVybiBpc0luUGFzdCA/IGZtdFZhbHVlICsgXCIgXCIgKyBmbXRVbml0ICsgXCIgYWdvXCIgOiBcImluIFwiICsgZm10VmFsdWUgKyBcIiBcIiArIGZtdFVuaXQ7XG4gIH1cbiAgZnVuY3Rpb24gZm9ybWF0U3RyaW5nKGtub3duRm9ybWF0KSB7XG4gICAgLy8gdGhlc2UgYWxsIGhhdmUgdGhlIG9mZnNldHMgcmVtb3ZlZCBiZWNhdXNlIHdlIGRvbid0IGhhdmUgYWNjZXNzIHRvIHRoZW1cbiAgICAvLyB3aXRob3V0IGFsbCB0aGUgaW50bCBzdHVmZiB0aGlzIGlzIGJhY2tmaWxsaW5nXG4gICAgdmFyIGZpbHRlcmVkID0gcGljayhrbm93bkZvcm1hdCwgW1wid2Vla2RheVwiLCBcImVyYVwiLCBcInllYXJcIiwgXCJtb250aFwiLCBcImRheVwiLCBcImhvdXJcIiwgXCJtaW51dGVcIiwgXCJzZWNvbmRcIiwgXCJ0aW1lWm9uZU5hbWVcIiwgXCJob3VyMTJcIl0pLFxuICAgICAgICBrZXkgPSBzdHJpbmdpZnkoZmlsdGVyZWQpLFxuICAgICAgICBkYXRlVGltZUh1Z2UgPSBcIkVFRUUsIExMTEwgZCwgeXl5eSwgaDptbSBhXCI7XG5cbiAgICBzd2l0Y2ggKGtleSkge1xuICAgICAgY2FzZSBzdHJpbmdpZnkoREFURV9TSE9SVCk6XG4gICAgICAgIHJldHVybiBcIk0vZC95eXl5XCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KERBVEVfTUVEKTpcbiAgICAgICAgcmV0dXJuIFwiTExMIGQsIHl5eXlcIjtcblxuICAgICAgY2FzZSBzdHJpbmdpZnkoREFURV9GVUxMKTpcbiAgICAgICAgcmV0dXJuIFwiTExMTCBkLCB5eXl5XCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KERBVEVfSFVHRSk6XG4gICAgICAgIHJldHVybiBcIkVFRUUsIExMTEwgZCwgeXl5eVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShUSU1FX1NJTVBMRSk6XG4gICAgICAgIHJldHVybiBcImg6bW0gYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShUSU1FX1dJVEhfU0VDT05EUyk6XG4gICAgICAgIHJldHVybiBcImg6bW06c3MgYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShUSU1FX1dJVEhfU0hPUlRfT0ZGU0VUKTpcbiAgICAgICAgcmV0dXJuIFwiaDptbSBhXCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KFRJTUVfV0lUSF9MT05HX09GRlNFVCk6XG4gICAgICAgIHJldHVybiBcImg6bW0gYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShUSU1FXzI0X1NJTVBMRSk6XG4gICAgICAgIHJldHVybiBcIkhIOm1tXCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KFRJTUVfMjRfV0lUSF9TRUNPTkRTKTpcbiAgICAgICAgcmV0dXJuIFwiSEg6bW06c3NcIjtcblxuICAgICAgY2FzZSBzdHJpbmdpZnkoVElNRV8yNF9XSVRIX1NIT1JUX09GRlNFVCk6XG4gICAgICAgIHJldHVybiBcIkhIOm1tXCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KFRJTUVfMjRfV0lUSF9MT05HX09GRlNFVCk6XG4gICAgICAgIHJldHVybiBcIkhIOm1tXCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KERBVEVUSU1FX1NIT1JUKTpcbiAgICAgICAgcmV0dXJuIFwiTS9kL3l5eXksIGg6bW0gYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShEQVRFVElNRV9NRUQpOlxuICAgICAgICByZXR1cm4gXCJMTEwgZCwgeXl5eSwgaDptbSBhXCI7XG5cbiAgICAgIGNhc2Ugc3RyaW5naWZ5KERBVEVUSU1FX0ZVTEwpOlxuICAgICAgICByZXR1cm4gXCJMTExMIGQsIHl5eXksIGg6bW0gYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShEQVRFVElNRV9IVUdFKTpcbiAgICAgICAgcmV0dXJuIGRhdGVUaW1lSHVnZTtcblxuICAgICAgY2FzZSBzdHJpbmdpZnkoREFURVRJTUVfU0hPUlRfV0lUSF9TRUNPTkRTKTpcbiAgICAgICAgcmV0dXJuIFwiTS9kL3l5eXksIGg6bW06c3MgYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShEQVRFVElNRV9NRURfV0lUSF9TRUNPTkRTKTpcbiAgICAgICAgcmV0dXJuIFwiTExMIGQsIHl5eXksIGg6bW06c3MgYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShEQVRFVElNRV9NRURfV0lUSF9XRUVLREFZKTpcbiAgICAgICAgcmV0dXJuIFwiRUVFLCBkIExMTCB5eXl5LCBoOm1tIGFcIjtcblxuICAgICAgY2FzZSBzdHJpbmdpZnkoREFURVRJTUVfRlVMTF9XSVRIX1NFQ09ORFMpOlxuICAgICAgICByZXR1cm4gXCJMTExMIGQsIHl5eXksIGg6bW06c3MgYVwiO1xuXG4gICAgICBjYXNlIHN0cmluZ2lmeShEQVRFVElNRV9IVUdFX1dJVEhfU0VDT05EUyk6XG4gICAgICAgIHJldHVybiBcIkVFRUUsIExMTEwgZCwgeXl5eSwgaDptbTpzcyBhXCI7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBkYXRlVGltZUh1Z2U7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gc3RyaW5naWZ5VG9rZW5zKHNwbGl0cywgdG9rZW5Ub1N0cmluZykge1xuICAgIHZhciBzID0gXCJcIjtcblxuICAgIGZvciAodmFyIF9pdGVyYXRvciA9IHNwbGl0cywgX2lzQXJyYXkgPSBBcnJheS5pc0FycmF5KF9pdGVyYXRvciksIF9pID0gMCwgX2l0ZXJhdG9yID0gX2lzQXJyYXkgPyBfaXRlcmF0b3IgOiBfaXRlcmF0b3JbU3ltYm9sLml0ZXJhdG9yXSgpOzspIHtcbiAgICAgIHZhciBfcmVmO1xuXG4gICAgICBpZiAoX2lzQXJyYXkpIHtcbiAgICAgICAgaWYgKF9pID49IF9pdGVyYXRvci5sZW5ndGgpIGJyZWFrO1xuICAgICAgICBfcmVmID0gX2l0ZXJhdG9yW19pKytdO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgX2kgPSBfaXRlcmF0b3IubmV4dCgpO1xuICAgICAgICBpZiAoX2kuZG9uZSkgYnJlYWs7XG4gICAgICAgIF9yZWYgPSBfaS52YWx1ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHRva2VuID0gX3JlZjtcblxuICAgICAgaWYgKHRva2VuLmxpdGVyYWwpIHtcbiAgICAgICAgcyArPSB0b2tlbi52YWw7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzICs9IHRva2VuVG9TdHJpbmcodG9rZW4udmFsKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gcztcbiAgfVxuXG4gIHZhciBfbWFjcm9Ub2tlblRvRm9ybWF0T3B0cyA9IHtcbiAgICBEOiBEQVRFX1NIT1JULFxuICAgIEREOiBEQVRFX01FRCxcbiAgICBEREQ6IERBVEVfRlVMTCxcbiAgICBEREREOiBEQVRFX0hVR0UsXG4gICAgdDogVElNRV9TSU1QTEUsXG4gICAgdHQ6IFRJTUVfV0lUSF9TRUNPTkRTLFxuICAgIHR0dDogVElNRV9XSVRIX1NIT1JUX09GRlNFVCxcbiAgICB0dHR0OiBUSU1FX1dJVEhfTE9OR19PRkZTRVQsXG4gICAgVDogVElNRV8yNF9TSU1QTEUsXG4gICAgVFQ6IFRJTUVfMjRfV0lUSF9TRUNPTkRTLFxuICAgIFRUVDogVElNRV8yNF9XSVRIX1NIT1JUX09GRlNFVCxcbiAgICBUVFRUOiBUSU1FXzI0X1dJVEhfTE9OR19PRkZTRVQsXG4gICAgZjogREFURVRJTUVfU0hPUlQsXG4gICAgZmY6IERBVEVUSU1FX01FRCxcbiAgICBmZmY6IERBVEVUSU1FX0ZVTEwsXG4gICAgZmZmZjogREFURVRJTUVfSFVHRSxcbiAgICBGOiBEQVRFVElNRV9TSE9SVF9XSVRIX1NFQ09ORFMsXG4gICAgRkY6IERBVEVUSU1FX01FRF9XSVRIX1NFQ09ORFMsXG4gICAgRkZGOiBEQVRFVElNRV9GVUxMX1dJVEhfU0VDT05EUyxcbiAgICBGRkZGOiBEQVRFVElNRV9IVUdFX1dJVEhfU0VDT05EU1xuICB9O1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdmFyIEZvcm1hdHRlciA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKCkge1xuICAgIEZvcm1hdHRlci5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUobG9jYWxlLCBvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ldyBGb3JtYXR0ZXIobG9jYWxlLCBvcHRzKTtcbiAgICB9O1xuXG4gICAgRm9ybWF0dGVyLnBhcnNlRm9ybWF0ID0gZnVuY3Rpb24gcGFyc2VGb3JtYXQoZm10KSB7XG4gICAgICB2YXIgY3VycmVudCA9IG51bGwsXG4gICAgICAgICAgY3VycmVudEZ1bGwgPSBcIlwiLFxuICAgICAgICAgIGJyYWNrZXRlZCA9IGZhbHNlO1xuICAgICAgdmFyIHNwbGl0cyA9IFtdO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZtdC5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgYyA9IGZtdC5jaGFyQXQoaSk7XG5cbiAgICAgICAgaWYgKGMgPT09IFwiJ1wiKSB7XG4gICAgICAgICAgaWYgKGN1cnJlbnRGdWxsLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHNwbGl0cy5wdXNoKHtcbiAgICAgICAgICAgICAgbGl0ZXJhbDogYnJhY2tldGVkLFxuICAgICAgICAgICAgICB2YWw6IGN1cnJlbnRGdWxsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjdXJyZW50ID0gbnVsbDtcbiAgICAgICAgICBjdXJyZW50RnVsbCA9IFwiXCI7XG4gICAgICAgICAgYnJhY2tldGVkID0gIWJyYWNrZXRlZDtcbiAgICAgICAgfSBlbHNlIGlmIChicmFja2V0ZWQpIHtcbiAgICAgICAgICBjdXJyZW50RnVsbCArPSBjO1xuICAgICAgICB9IGVsc2UgaWYgKGMgPT09IGN1cnJlbnQpIHtcbiAgICAgICAgICBjdXJyZW50RnVsbCArPSBjO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmIChjdXJyZW50RnVsbC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBzcGxpdHMucHVzaCh7XG4gICAgICAgICAgICAgIGxpdGVyYWw6IGZhbHNlLFxuICAgICAgICAgICAgICB2YWw6IGN1cnJlbnRGdWxsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjdXJyZW50RnVsbCA9IGM7XG4gICAgICAgICAgY3VycmVudCA9IGM7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGN1cnJlbnRGdWxsLmxlbmd0aCA+IDApIHtcbiAgICAgICAgc3BsaXRzLnB1c2goe1xuICAgICAgICAgIGxpdGVyYWw6IGJyYWNrZXRlZCxcbiAgICAgICAgICB2YWw6IGN1cnJlbnRGdWxsXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gc3BsaXRzO1xuICAgIH07XG5cbiAgICBGb3JtYXR0ZXIubWFjcm9Ub2tlblRvRm9ybWF0T3B0cyA9IGZ1bmN0aW9uIG1hY3JvVG9rZW5Ub0Zvcm1hdE9wdHModG9rZW4pIHtcbiAgICAgIHJldHVybiBfbWFjcm9Ub2tlblRvRm9ybWF0T3B0c1t0b2tlbl07XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIEZvcm1hdHRlcihsb2NhbGUsIGZvcm1hdE9wdHMpIHtcbiAgICAgIHRoaXMub3B0cyA9IGZvcm1hdE9wdHM7XG4gICAgICB0aGlzLmxvYyA9IGxvY2FsZTtcbiAgICAgIHRoaXMuc3lzdGVtTG9jID0gbnVsbDtcbiAgICB9XG5cbiAgICB2YXIgX3Byb3RvID0gRm9ybWF0dGVyLnByb3RvdHlwZTtcblxuICAgIF9wcm90by5mb3JtYXRXaXRoU3lzdGVtRGVmYXVsdCA9IGZ1bmN0aW9uIGZvcm1hdFdpdGhTeXN0ZW1EZWZhdWx0KGR0LCBvcHRzKSB7XG4gICAgICBpZiAodGhpcy5zeXN0ZW1Mb2MgPT09IG51bGwpIHtcbiAgICAgICAgdGhpcy5zeXN0ZW1Mb2MgPSB0aGlzLmxvYy5yZWRlZmF1bHRUb1N5c3RlbSgpO1xuICAgICAgfVxuXG4gICAgICB2YXIgZGYgPSB0aGlzLnN5c3RlbUxvYy5kdEZvcm1hdHRlcihkdCwgT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5vcHRzLCBvcHRzKSk7XG4gICAgICByZXR1cm4gZGYuZm9ybWF0KCk7XG4gICAgfTtcblxuICAgIF9wcm90by5mb3JtYXREYXRlVGltZSA9IGZ1bmN0aW9uIGZvcm1hdERhdGVUaW1lKGR0LCBvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgdmFyIGRmID0gdGhpcy5sb2MuZHRGb3JtYXR0ZXIoZHQsIE9iamVjdC5hc3NpZ24oe30sIHRoaXMub3B0cywgb3B0cykpO1xuICAgICAgcmV0dXJuIGRmLmZvcm1hdCgpO1xuICAgIH07XG5cbiAgICBfcHJvdG8uZm9ybWF0RGF0ZVRpbWVQYXJ0cyA9IGZ1bmN0aW9uIGZvcm1hdERhdGVUaW1lUGFydHMoZHQsIG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICB2YXIgZGYgPSB0aGlzLmxvYy5kdEZvcm1hdHRlcihkdCwgT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5vcHRzLCBvcHRzKSk7XG4gICAgICByZXR1cm4gZGYuZm9ybWF0VG9QYXJ0cygpO1xuICAgIH07XG5cbiAgICBfcHJvdG8ucmVzb2x2ZWRPcHRpb25zID0gZnVuY3Rpb24gcmVzb2x2ZWRPcHRpb25zKGR0LCBvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgdmFyIGRmID0gdGhpcy5sb2MuZHRGb3JtYXR0ZXIoZHQsIE9iamVjdC5hc3NpZ24oe30sIHRoaXMub3B0cywgb3B0cykpO1xuICAgICAgcmV0dXJuIGRmLnJlc29sdmVkT3B0aW9ucygpO1xuICAgIH07XG5cbiAgICBfcHJvdG8ubnVtID0gZnVuY3Rpb24gbnVtKG4sIHApIHtcbiAgICAgIGlmIChwID09PSB2b2lkIDApIHtcbiAgICAgICAgcCA9IDA7XG4gICAgICB9XG5cbiAgICAgIC8vIHdlIGdldCBzb21lIHBlcmYgb3V0IG9mIGRvaW5nIHRoaXMgaGVyZSwgYW5ub3lpbmdseVxuICAgICAgaWYgKHRoaXMub3B0cy5mb3JjZVNpbXBsZSkge1xuICAgICAgICByZXR1cm4gcGFkU3RhcnQobiwgcCk7XG4gICAgICB9XG5cbiAgICAgIHZhciBvcHRzID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5vcHRzKTtcblxuICAgICAgaWYgKHAgPiAwKSB7XG4gICAgICAgIG9wdHMucGFkVG8gPSBwO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5sb2MubnVtYmVyRm9ybWF0dGVyKG9wdHMpLmZvcm1hdChuKTtcbiAgICB9O1xuXG4gICAgX3Byb3RvLmZvcm1hdERhdGVUaW1lRnJvbVN0cmluZyA9IGZ1bmN0aW9uIGZvcm1hdERhdGVUaW1lRnJvbVN0cmluZyhkdCwgZm10KSB7XG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgICB2YXIga25vd25FbmdsaXNoID0gdGhpcy5sb2MubGlzdGluZ01vZGUoKSA9PT0gXCJlblwiLFxuICAgICAgICAgIHVzZURhdGVUaW1lRm9ybWF0dGVyID0gdGhpcy5sb2Mub3V0cHV0Q2FsZW5kYXIgJiYgdGhpcy5sb2Mub3V0cHV0Q2FsZW5kYXIgIT09IFwiZ3JlZ29yeVwiICYmIGhhc0Zvcm1hdFRvUGFydHMoKSxcbiAgICAgICAgICBzdHJpbmcgPSBmdW5jdGlvbiBzdHJpbmcob3B0cywgZXh0cmFjdCkge1xuICAgICAgICByZXR1cm4gX3RoaXMubG9jLmV4dHJhY3QoZHQsIG9wdHMsIGV4dHJhY3QpO1xuICAgICAgfSxcbiAgICAgICAgICBmb3JtYXRPZmZzZXQgPSBmdW5jdGlvbiBmb3JtYXRPZmZzZXQob3B0cykge1xuICAgICAgICBpZiAoZHQuaXNPZmZzZXRGaXhlZCAmJiBkdC5vZmZzZXQgPT09IDAgJiYgb3B0cy5hbGxvd1opIHtcbiAgICAgICAgICByZXR1cm4gXCJaXCI7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZHQuaXNWYWxpZCA/IGR0LnpvbmUuZm9ybWF0T2Zmc2V0KGR0LnRzLCBvcHRzLmZvcm1hdCkgOiBcIlwiO1xuICAgICAgfSxcbiAgICAgICAgICBtZXJpZGllbSA9IGZ1bmN0aW9uIG1lcmlkaWVtKCkge1xuICAgICAgICByZXR1cm4ga25vd25FbmdsaXNoID8gbWVyaWRpZW1Gb3JEYXRlVGltZShkdCkgOiBzdHJpbmcoe1xuICAgICAgICAgIGhvdXI6IFwibnVtZXJpY1wiLFxuICAgICAgICAgIGhvdXIxMjogdHJ1ZVxuICAgICAgICB9LCBcImRheXBlcmlvZFwiKTtcbiAgICAgIH0sXG4gICAgICAgICAgbW9udGggPSBmdW5jdGlvbiBtb250aChsZW5ndGgsIHN0YW5kYWxvbmUpIHtcbiAgICAgICAgcmV0dXJuIGtub3duRW5nbGlzaCA/IG1vbnRoRm9yRGF0ZVRpbWUoZHQsIGxlbmd0aCkgOiBzdHJpbmcoc3RhbmRhbG9uZSA/IHtcbiAgICAgICAgICBtb250aDogbGVuZ3RoXG4gICAgICAgIH0gOiB7XG4gICAgICAgICAgbW9udGg6IGxlbmd0aCxcbiAgICAgICAgICBkYXk6IFwibnVtZXJpY1wiXG4gICAgICAgIH0sIFwibW9udGhcIik7XG4gICAgICB9LFxuICAgICAgICAgIHdlZWtkYXkgPSBmdW5jdGlvbiB3ZWVrZGF5KGxlbmd0aCwgc3RhbmRhbG9uZSkge1xuICAgICAgICByZXR1cm4ga25vd25FbmdsaXNoID8gd2Vla2RheUZvckRhdGVUaW1lKGR0LCBsZW5ndGgpIDogc3RyaW5nKHN0YW5kYWxvbmUgPyB7XG4gICAgICAgICAgd2Vla2RheTogbGVuZ3RoXG4gICAgICAgIH0gOiB7XG4gICAgICAgICAgd2Vla2RheTogbGVuZ3RoLFxuICAgICAgICAgIG1vbnRoOiBcImxvbmdcIixcbiAgICAgICAgICBkYXk6IFwibnVtZXJpY1wiXG4gICAgICAgIH0sIFwid2Vla2RheVwiKTtcbiAgICAgIH0sXG4gICAgICAgICAgbWF5YmVNYWNybyA9IGZ1bmN0aW9uIG1heWJlTWFjcm8odG9rZW4pIHtcbiAgICAgICAgdmFyIGZvcm1hdE9wdHMgPSBGb3JtYXR0ZXIubWFjcm9Ub2tlblRvRm9ybWF0T3B0cyh0b2tlbik7XG5cbiAgICAgICAgaWYgKGZvcm1hdE9wdHMpIHtcbiAgICAgICAgICByZXR1cm4gX3RoaXMuZm9ybWF0V2l0aFN5c3RlbURlZmF1bHQoZHQsIGZvcm1hdE9wdHMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiB0b2tlbjtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgICAgICBlcmEgPSBmdW5jdGlvbiBlcmEobGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiBrbm93bkVuZ2xpc2ggPyBlcmFGb3JEYXRlVGltZShkdCwgbGVuZ3RoKSA6IHN0cmluZyh7XG4gICAgICAgICAgZXJhOiBsZW5ndGhcbiAgICAgICAgfSwgXCJlcmFcIik7XG4gICAgICB9LFxuICAgICAgICAgIHRva2VuVG9TdHJpbmcgPSBmdW5jdGlvbiB0b2tlblRvU3RyaW5nKHRva2VuKSB7XG4gICAgICAgIC8vIFdoZXJlIHBvc3NpYmxlOiBodHRwOi8vY2xkci51bmljb2RlLm9yZy90cmFuc2xhdGlvbi9kYXRlLXRpbWUjVE9DLVN0YW5kLUFsb25lLXZzLi1Gb3JtYXQtU3R5bGVzXG4gICAgICAgIHN3aXRjaCAodG9rZW4pIHtcbiAgICAgICAgICAvLyBtc1xuICAgICAgICAgIGNhc2UgXCJTXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0Lm1pbGxpc2Vjb25kKTtcblxuICAgICAgICAgIGNhc2UgXCJ1XCI6IC8vIGZhbGxzIHRocm91Z2hcblxuICAgICAgICAgIGNhc2UgXCJTU1NcIjpcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5udW0oZHQubWlsbGlzZWNvbmQsIDMpO1xuICAgICAgICAgIC8vIHNlY29uZHNcblxuICAgICAgICAgIGNhc2UgXCJzXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0LnNlY29uZCk7XG5cbiAgICAgICAgICBjYXNlIFwic3NcIjpcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5udW0oZHQuc2Vjb25kLCAyKTtcbiAgICAgICAgICAvLyBtaW51dGVzXG5cbiAgICAgICAgICBjYXNlIFwibVwiOlxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC5taW51dGUpO1xuXG4gICAgICAgICAgY2FzZSBcIm1tXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0Lm1pbnV0ZSwgMik7XG4gICAgICAgICAgLy8gaG91cnNcblxuICAgICAgICAgIGNhc2UgXCJoXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0LmhvdXIgJSAxMiA9PT0gMCA/IDEyIDogZHQuaG91ciAlIDEyKTtcblxuICAgICAgICAgIGNhc2UgXCJoaFwiOlxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC5ob3VyICUgMTIgPT09IDAgPyAxMiA6IGR0LmhvdXIgJSAxMiwgMik7XG5cbiAgICAgICAgICBjYXNlIFwiSFwiOlxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC5ob3VyKTtcblxuICAgICAgICAgIGNhc2UgXCJISFwiOlxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC5ob3VyLCAyKTtcbiAgICAgICAgICAvLyBvZmZzZXRcblxuICAgICAgICAgIGNhc2UgXCJaXCI6XG4gICAgICAgICAgICAvLyBsaWtlICs2XG4gICAgICAgICAgICByZXR1cm4gZm9ybWF0T2Zmc2V0KHtcbiAgICAgICAgICAgICAgZm9ybWF0OiBcIm5hcnJvd1wiLFxuICAgICAgICAgICAgICBhbGxvd1o6IF90aGlzLm9wdHMuYWxsb3daXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgIGNhc2UgXCJaWlwiOlxuICAgICAgICAgICAgLy8gbGlrZSArMDY6MDBcbiAgICAgICAgICAgIHJldHVybiBmb3JtYXRPZmZzZXQoe1xuICAgICAgICAgICAgICBmb3JtYXQ6IFwic2hvcnRcIixcbiAgICAgICAgICAgICAgYWxsb3daOiBfdGhpcy5vcHRzLmFsbG93WlxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICBjYXNlIFwiWlpaXCI6XG4gICAgICAgICAgICAvLyBsaWtlICswNjAwXG4gICAgICAgICAgICByZXR1cm4gZm9ybWF0T2Zmc2V0KHtcbiAgICAgICAgICAgICAgZm9ybWF0OiBcInRlY2hpZVwiLFxuICAgICAgICAgICAgICBhbGxvd1o6IGZhbHNlXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgIGNhc2UgXCJaWlpaXCI6XG4gICAgICAgICAgICAvLyBsaWtlIEVTVFxuICAgICAgICAgICAgcmV0dXJuIGR0LnpvbmUub2Zmc2V0TmFtZShkdC50cywge1xuICAgICAgICAgICAgICBmb3JtYXQ6IFwic2hvcnRcIixcbiAgICAgICAgICAgICAgbG9jYWxlOiBfdGhpcy5sb2MubG9jYWxlXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgIGNhc2UgXCJaWlpaWlwiOlxuICAgICAgICAgICAgLy8gbGlrZSBFYXN0ZXJuIFN0YW5kYXJkIFRpbWVcbiAgICAgICAgICAgIHJldHVybiBkdC56b25lLm9mZnNldE5hbWUoZHQudHMsIHtcbiAgICAgICAgICAgICAgZm9ybWF0OiBcImxvbmdcIixcbiAgICAgICAgICAgICAgbG9jYWxlOiBfdGhpcy5sb2MubG9jYWxlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyB6b25lXG5cbiAgICAgICAgICBjYXNlIFwielwiOlxuICAgICAgICAgICAgLy8gbGlrZSBBbWVyaWNhL05ld19Zb3JrXG4gICAgICAgICAgICByZXR1cm4gZHQuem9uZU5hbWU7XG4gICAgICAgICAgLy8gbWVyaWRpZW1zXG5cbiAgICAgICAgICBjYXNlIFwiYVwiOlxuICAgICAgICAgICAgcmV0dXJuIG1lcmlkaWVtKCk7XG4gICAgICAgICAgLy8gZGF0ZXNcblxuICAgICAgICAgIGNhc2UgXCJkXCI6XG4gICAgICAgICAgICByZXR1cm4gdXNlRGF0ZVRpbWVGb3JtYXR0ZXIgPyBzdHJpbmcoe1xuICAgICAgICAgICAgICBkYXk6IFwibnVtZXJpY1wiXG4gICAgICAgICAgICB9LCBcImRheVwiKSA6IF90aGlzLm51bShkdC5kYXkpO1xuXG4gICAgICAgICAgY2FzZSBcImRkXCI6XG4gICAgICAgICAgICByZXR1cm4gdXNlRGF0ZVRpbWVGb3JtYXR0ZXIgPyBzdHJpbmcoe1xuICAgICAgICAgICAgICBkYXk6IFwiMi1kaWdpdFwiXG4gICAgICAgICAgICB9LCBcImRheVwiKSA6IF90aGlzLm51bShkdC5kYXksIDIpO1xuICAgICAgICAgIC8vIHdlZWtkYXlzIC0gc3RhbmRhbG9uZVxuXG4gICAgICAgICAgY2FzZSBcImNcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgMVxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC53ZWVrZGF5KTtcblxuICAgICAgICAgIGNhc2UgXCJjY2NcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgJ1R1ZXMnXG4gICAgICAgICAgICByZXR1cm4gd2Vla2RheShcInNob3J0XCIsIHRydWUpO1xuXG4gICAgICAgICAgY2FzZSBcImNjY2NcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgJ1R1ZXNkYXknXG4gICAgICAgICAgICByZXR1cm4gd2Vla2RheShcImxvbmdcIiwgdHJ1ZSk7XG5cbiAgICAgICAgICBjYXNlIFwiY2NjY2NcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgJ1QnXG4gICAgICAgICAgICByZXR1cm4gd2Vla2RheShcIm5hcnJvd1wiLCB0cnVlKTtcbiAgICAgICAgICAvLyB3ZWVrZGF5cyAtIGZvcm1hdFxuXG4gICAgICAgICAgY2FzZSBcIkVcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgMVxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC53ZWVrZGF5KTtcblxuICAgICAgICAgIGNhc2UgXCJFRUVcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgJ1R1ZXMnXG4gICAgICAgICAgICByZXR1cm4gd2Vla2RheShcInNob3J0XCIsIGZhbHNlKTtcblxuICAgICAgICAgIGNhc2UgXCJFRUVFXCI6XG4gICAgICAgICAgICAvLyBsaWtlICdUdWVzZGF5J1xuICAgICAgICAgICAgcmV0dXJuIHdlZWtkYXkoXCJsb25nXCIsIGZhbHNlKTtcblxuICAgICAgICAgIGNhc2UgXCJFRUVFRVwiOlxuICAgICAgICAgICAgLy8gbGlrZSAnVCdcbiAgICAgICAgICAgIHJldHVybiB3ZWVrZGF5KFwibmFycm93XCIsIGZhbHNlKTtcbiAgICAgICAgICAvLyBtb250aHMgLSBzdGFuZGFsb25lXG5cbiAgICAgICAgICBjYXNlIFwiTFwiOlxuICAgICAgICAgICAgLy8gbGlrZSAxXG4gICAgICAgICAgICByZXR1cm4gdXNlRGF0ZVRpbWVGb3JtYXR0ZXIgPyBzdHJpbmcoe1xuICAgICAgICAgICAgICBtb250aDogXCJudW1lcmljXCIsXG4gICAgICAgICAgICAgIGRheTogXCJudW1lcmljXCJcbiAgICAgICAgICAgIH0sIFwibW9udGhcIikgOiBfdGhpcy5udW0oZHQubW9udGgpO1xuXG4gICAgICAgICAgY2FzZSBcIkxMXCI6XG4gICAgICAgICAgICAvLyBsaWtlIDAxLCBkb2Vzbid0IHNlZW0gdG8gd29ya1xuICAgICAgICAgICAgcmV0dXJuIHVzZURhdGVUaW1lRm9ybWF0dGVyID8gc3RyaW5nKHtcbiAgICAgICAgICAgICAgbW9udGg6IFwiMi1kaWdpdFwiLFxuICAgICAgICAgICAgICBkYXk6IFwibnVtZXJpY1wiXG4gICAgICAgICAgICB9LCBcIm1vbnRoXCIpIDogX3RoaXMubnVtKGR0Lm1vbnRoLCAyKTtcblxuICAgICAgICAgIGNhc2UgXCJMTExcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgSmFuXG4gICAgICAgICAgICByZXR1cm4gbW9udGgoXCJzaG9ydFwiLCB0cnVlKTtcblxuICAgICAgICAgIGNhc2UgXCJMTExMXCI6XG4gICAgICAgICAgICAvLyBsaWtlIEphbnVhcnlcbiAgICAgICAgICAgIHJldHVybiBtb250aChcImxvbmdcIiwgdHJ1ZSk7XG5cbiAgICAgICAgICBjYXNlIFwiTExMTExcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgSlxuICAgICAgICAgICAgcmV0dXJuIG1vbnRoKFwibmFycm93XCIsIHRydWUpO1xuICAgICAgICAgIC8vIG1vbnRocyAtIGZvcm1hdFxuXG4gICAgICAgICAgY2FzZSBcIk1cIjpcbiAgICAgICAgICAgIC8vIGxpa2UgMVxuICAgICAgICAgICAgcmV0dXJuIHVzZURhdGVUaW1lRm9ybWF0dGVyID8gc3RyaW5nKHtcbiAgICAgICAgICAgICAgbW9udGg6IFwibnVtZXJpY1wiXG4gICAgICAgICAgICB9LCBcIm1vbnRoXCIpIDogX3RoaXMubnVtKGR0Lm1vbnRoKTtcblxuICAgICAgICAgIGNhc2UgXCJNTVwiOlxuICAgICAgICAgICAgLy8gbGlrZSAwMVxuICAgICAgICAgICAgcmV0dXJuIHVzZURhdGVUaW1lRm9ybWF0dGVyID8gc3RyaW5nKHtcbiAgICAgICAgICAgICAgbW9udGg6IFwiMi1kaWdpdFwiXG4gICAgICAgICAgICB9LCBcIm1vbnRoXCIpIDogX3RoaXMubnVtKGR0Lm1vbnRoLCAyKTtcblxuICAgICAgICAgIGNhc2UgXCJNTU1cIjpcbiAgICAgICAgICAgIC8vIGxpa2UgSmFuXG4gICAgICAgICAgICByZXR1cm4gbW9udGgoXCJzaG9ydFwiLCBmYWxzZSk7XG5cbiAgICAgICAgICBjYXNlIFwiTU1NTVwiOlxuICAgICAgICAgICAgLy8gbGlrZSBKYW51YXJ5XG4gICAgICAgICAgICByZXR1cm4gbW9udGgoXCJsb25nXCIsIGZhbHNlKTtcblxuICAgICAgICAgIGNhc2UgXCJNTU1NTVwiOlxuICAgICAgICAgICAgLy8gbGlrZSBKXG4gICAgICAgICAgICByZXR1cm4gbW9udGgoXCJuYXJyb3dcIiwgZmFsc2UpO1xuICAgICAgICAgIC8vIHllYXJzXG5cbiAgICAgICAgICBjYXNlIFwieVwiOlxuICAgICAgICAgICAgLy8gbGlrZSAyMDE0XG4gICAgICAgICAgICByZXR1cm4gdXNlRGF0ZVRpbWVGb3JtYXR0ZXIgPyBzdHJpbmcoe1xuICAgICAgICAgICAgICB5ZWFyOiBcIm51bWVyaWNcIlxuICAgICAgICAgICAgfSwgXCJ5ZWFyXCIpIDogX3RoaXMubnVtKGR0LnllYXIpO1xuXG4gICAgICAgICAgY2FzZSBcInl5XCI6XG4gICAgICAgICAgICAvLyBsaWtlIDE0XG4gICAgICAgICAgICByZXR1cm4gdXNlRGF0ZVRpbWVGb3JtYXR0ZXIgPyBzdHJpbmcoe1xuICAgICAgICAgICAgICB5ZWFyOiBcIjItZGlnaXRcIlxuICAgICAgICAgICAgfSwgXCJ5ZWFyXCIpIDogX3RoaXMubnVtKGR0LnllYXIudG9TdHJpbmcoKS5zbGljZSgtMiksIDIpO1xuXG4gICAgICAgICAgY2FzZSBcInl5eXlcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgMDAxMlxuICAgICAgICAgICAgcmV0dXJuIHVzZURhdGVUaW1lRm9ybWF0dGVyID8gc3RyaW5nKHtcbiAgICAgICAgICAgICAgeWVhcjogXCJudW1lcmljXCJcbiAgICAgICAgICAgIH0sIFwieWVhclwiKSA6IF90aGlzLm51bShkdC55ZWFyLCA0KTtcblxuICAgICAgICAgIGNhc2UgXCJ5eXl5eXlcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgMDAwMDEyXG4gICAgICAgICAgICByZXR1cm4gdXNlRGF0ZVRpbWVGb3JtYXR0ZXIgPyBzdHJpbmcoe1xuICAgICAgICAgICAgICB5ZWFyOiBcIm51bWVyaWNcIlxuICAgICAgICAgICAgfSwgXCJ5ZWFyXCIpIDogX3RoaXMubnVtKGR0LnllYXIsIDYpO1xuICAgICAgICAgIC8vIGVyYXNcblxuICAgICAgICAgIGNhc2UgXCJHXCI6XG4gICAgICAgICAgICAvLyBsaWtlIEFEXG4gICAgICAgICAgICByZXR1cm4gZXJhKFwic2hvcnRcIik7XG5cbiAgICAgICAgICBjYXNlIFwiR0dcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgQW5ubyBEb21pbmlcbiAgICAgICAgICAgIHJldHVybiBlcmEoXCJsb25nXCIpO1xuXG4gICAgICAgICAgY2FzZSBcIkdHR0dHXCI6XG4gICAgICAgICAgICByZXR1cm4gZXJhKFwibmFycm93XCIpO1xuXG4gICAgICAgICAgY2FzZSBcImtrXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0LndlZWtZZWFyLnRvU3RyaW5nKCkuc2xpY2UoLTIpLCAyKTtcblxuICAgICAgICAgIGNhc2UgXCJra2trXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0LndlZWtZZWFyLCA0KTtcblxuICAgICAgICAgIGNhc2UgXCJXXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0LndlZWtOdW1iZXIpO1xuXG4gICAgICAgICAgY2FzZSBcIldXXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0LndlZWtOdW1iZXIsIDIpO1xuXG4gICAgICAgICAgY2FzZSBcIm9cIjpcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5udW0oZHQub3JkaW5hbCk7XG5cbiAgICAgICAgICBjYXNlIFwib29vXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKGR0Lm9yZGluYWwsIDMpO1xuXG4gICAgICAgICAgY2FzZSBcInFcIjpcbiAgICAgICAgICAgIC8vIGxpa2UgMVxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC5xdWFydGVyKTtcblxuICAgICAgICAgIGNhc2UgXCJxcVwiOlxuICAgICAgICAgICAgLy8gbGlrZSAwMVxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC5xdWFydGVyLCAyKTtcblxuICAgICAgICAgIGNhc2UgXCJYXCI6XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMubnVtKE1hdGguZmxvb3IoZHQudHMgLyAxMDAwKSk7XG5cbiAgICAgICAgICBjYXNlIFwieFwiOlxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm51bShkdC50cyk7XG5cbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgcmV0dXJuIG1heWJlTWFjcm8odG9rZW4pO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gc3RyaW5naWZ5VG9rZW5zKEZvcm1hdHRlci5wYXJzZUZvcm1hdChmbXQpLCB0b2tlblRvU3RyaW5nKTtcbiAgICB9O1xuXG4gICAgX3Byb3RvLmZvcm1hdER1cmF0aW9uRnJvbVN0cmluZyA9IGZ1bmN0aW9uIGZvcm1hdER1cmF0aW9uRnJvbVN0cmluZyhkdXIsIGZtdCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciB0b2tlblRvRmllbGQgPSBmdW5jdGlvbiB0b2tlblRvRmllbGQodG9rZW4pIHtcbiAgICAgICAgc3dpdGNoICh0b2tlblswXSkge1xuICAgICAgICAgIGNhc2UgXCJTXCI6XG4gICAgICAgICAgICByZXR1cm4gXCJtaWxsaXNlY29uZFwiO1xuXG4gICAgICAgICAgY2FzZSBcInNcIjpcbiAgICAgICAgICAgIHJldHVybiBcInNlY29uZFwiO1xuXG4gICAgICAgICAgY2FzZSBcIm1cIjpcbiAgICAgICAgICAgIHJldHVybiBcIm1pbnV0ZVwiO1xuXG4gICAgICAgICAgY2FzZSBcImhcIjpcbiAgICAgICAgICAgIHJldHVybiBcImhvdXJcIjtcblxuICAgICAgICAgIGNhc2UgXCJkXCI6XG4gICAgICAgICAgICByZXR1cm4gXCJkYXlcIjtcblxuICAgICAgICAgIGNhc2UgXCJNXCI6XG4gICAgICAgICAgICByZXR1cm4gXCJtb250aFwiO1xuXG4gICAgICAgICAgY2FzZSBcInlcIjpcbiAgICAgICAgICAgIHJldHVybiBcInllYXJcIjtcblxuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgICAgICB0b2tlblRvU3RyaW5nID0gZnVuY3Rpb24gdG9rZW5Ub1N0cmluZyhsaWxkdXIpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICh0b2tlbikge1xuICAgICAgICAgIHZhciBtYXBwZWQgPSB0b2tlblRvRmllbGQodG9rZW4pO1xuXG4gICAgICAgICAgaWYgKG1hcHBlZCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzMi5udW0obGlsZHVyLmdldChtYXBwZWQpLCB0b2tlbi5sZW5ndGgpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgfSxcbiAgICAgICAgICB0b2tlbnMgPSBGb3JtYXR0ZXIucGFyc2VGb3JtYXQoZm10KSxcbiAgICAgICAgICByZWFsVG9rZW5zID0gdG9rZW5zLnJlZHVjZShmdW5jdGlvbiAoZm91bmQsIF9yZWYyKSB7XG4gICAgICAgIHZhciBsaXRlcmFsID0gX3JlZjIubGl0ZXJhbCxcbiAgICAgICAgICAgIHZhbCA9IF9yZWYyLnZhbDtcbiAgICAgICAgcmV0dXJuIGxpdGVyYWwgPyBmb3VuZCA6IGZvdW5kLmNvbmNhdCh2YWwpO1xuICAgICAgfSwgW10pLFxuICAgICAgICAgIGNvbGxhcHNlZCA9IGR1ci5zaGlmdFRvLmFwcGx5KGR1ciwgcmVhbFRva2Vucy5tYXAodG9rZW5Ub0ZpZWxkKS5maWx0ZXIoZnVuY3Rpb24gKHQpIHtcbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgICB9KSk7XG5cbiAgICAgIHJldHVybiBzdHJpbmdpZnlUb2tlbnModG9rZW5zLCB0b2tlblRvU3RyaW5nKGNvbGxhcHNlZCkpO1xuICAgIH07XG5cbiAgICByZXR1cm4gRm9ybWF0dGVyO1xuICB9KCk7XG5cbiAgdmFyIEludmFsaWQgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBJbnZhbGlkKHJlYXNvbiwgZXhwbGFuYXRpb24pIHtcbiAgICAgIHRoaXMucmVhc29uID0gcmVhc29uO1xuICAgICAgdGhpcy5leHBsYW5hdGlvbiA9IGV4cGxhbmF0aW9uO1xuICAgIH1cblxuICAgIHZhciBfcHJvdG8gPSBJbnZhbGlkLnByb3RvdHlwZTtcblxuICAgIF9wcm90by50b01lc3NhZ2UgPSBmdW5jdGlvbiB0b01lc3NhZ2UoKSB7XG4gICAgICBpZiAodGhpcy5leHBsYW5hdGlvbikge1xuICAgICAgICByZXR1cm4gdGhpcy5yZWFzb24gKyBcIjogXCIgKyB0aGlzLmV4cGxhbmF0aW9uO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVhc29uO1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gSW52YWxpZDtcbiAgfSgpO1xuXG4gIC8qKlxuICAgKiBAaW50ZXJmYWNlXG4gICAqL1xuXG4gIHZhciBab25lID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gWm9uZSgpIHt9XG5cbiAgICB2YXIgX3Byb3RvID0gWm9uZS5wcm90b3R5cGU7XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBvZmZzZXQncyBjb21tb24gbmFtZSAoc3VjaCBhcyBFU1QpIGF0IHRoZSBzcGVjaWZpZWQgdGltZXN0YW1wXG4gICAgICogQGFic3RyYWN0XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHRzIC0gRXBvY2ggbWlsbGlzZWNvbmRzIGZvciB3aGljaCB0byBnZXQgdGhlIG5hbWVcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIE9wdGlvbnMgdG8gYWZmZWN0IHRoZSBmb3JtYXRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5mb3JtYXQgLSBXaGF0IHN0eWxlIG9mIG9mZnNldCB0byByZXR1cm4uIEFjY2VwdHMgJ2xvbmcnIG9yICdzaG9ydCcuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdHMubG9jYWxlIC0gV2hhdCBsb2NhbGUgdG8gcmV0dXJuIHRoZSBvZmZzZXQgbmFtZSBpbi5cbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgX3Byb3RvLm9mZnNldE5hbWUgPSBmdW5jdGlvbiBvZmZzZXROYW1lKHRzLCBvcHRzKSB7XG4gICAgICB0aHJvdyBuZXcgWm9uZUlzQWJzdHJhY3RFcnJvcigpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBvZmZzZXQncyB2YWx1ZSBhcyBhIHN0cmluZ1xuICAgICAqIEBhYnN0cmFjdFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB0cyAtIEVwb2NoIG1pbGxpc2Vjb25kcyBmb3Igd2hpY2ggdG8gZ2V0IHRoZSBvZmZzZXRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZm9ybWF0IC0gV2hhdCBzdHlsZSBvZiBvZmZzZXQgdG8gcmV0dXJuLlxuICAgICAqICAgICAgICAgICAgICAgICAgICAgICAgICBBY2NlcHRzICduYXJyb3cnLCAnc2hvcnQnLCBvciAndGVjaGllJy4gUmV0dXJuaW5nICcrNicsICcrMDY6MDAnLCBvciAnKzA2MDAnIHJlc3BlY3RpdmVseVxuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZm9ybWF0T2Zmc2V0ID0gZnVuY3Rpb24gZm9ybWF0T2Zmc2V0KHRzLCBmb3JtYXQpIHtcbiAgICAgIHRocm93IG5ldyBab25lSXNBYnN0cmFjdEVycm9yKCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiB0aGUgb2Zmc2V0IGluIG1pbnV0ZXMgZm9yIHRoaXMgem9uZSBhdCB0aGUgc3BlY2lmaWVkIHRpbWVzdGFtcC5cbiAgICAgKiBAYWJzdHJhY3RcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gdHMgLSBFcG9jaCBtaWxsaXNlY29uZHMgZm9yIHdoaWNoIHRvIGNvbXB1dGUgdGhlIG9mZnNldFxuICAgICAqIEByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8ub2Zmc2V0ID0gZnVuY3Rpb24gb2Zmc2V0KHRzKSB7XG4gICAgICB0aHJvdyBuZXcgWm9uZUlzQWJzdHJhY3RFcnJvcigpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gd2hldGhlciB0aGlzIFpvbmUgaXMgZXF1YWwgdG8gYW5vdGhlciB6b25lXG4gICAgICogQGFic3RyYWN0XG4gICAgICogQHBhcmFtIHtab25lfSBvdGhlclpvbmUgLSB0aGUgem9uZSB0byBjb21wYXJlXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZXF1YWxzID0gZnVuY3Rpb24gZXF1YWxzKG90aGVyWm9uZSkge1xuICAgICAgdGhyb3cgbmV3IFpvbmVJc0Fic3RyYWN0RXJyb3IoKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHdoZXRoZXIgdGhpcyBab25lIGlzIHZhbGlkLlxuICAgICAqIEBhYnN0cmFjdFxuICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9jcmVhdGVDbGFzcyhab25lLCBbe1xuICAgICAga2V5OiBcInR5cGVcIixcblxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgdHlwZSBvZiB6b25lXG4gICAgICAgKiBAYWJzdHJhY3RcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICB0aHJvdyBuZXcgWm9uZUlzQWJzdHJhY3RFcnJvcigpO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgbmFtZSBvZiB0aGlzIHpvbmUuXG4gICAgICAgKiBAYWJzdHJhY3RcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJuYW1lXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgdGhyb3cgbmV3IFpvbmVJc0Fic3RyYWN0RXJyb3IoKTtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyB3aGV0aGVyIHRoZSBvZmZzZXQgaXMga25vd24gdG8gYmUgZml4ZWQgZm9yIHRoZSB3aG9sZSB5ZWFyLlxuICAgICAgICogQGFic3RyYWN0XG4gICAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcInVuaXZlcnNhbFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHRocm93IG5ldyBab25lSXNBYnN0cmFjdEVycm9yKCk7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiBcImlzVmFsaWRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICB0aHJvdyBuZXcgWm9uZUlzQWJzdHJhY3RFcnJvcigpO1xuICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBab25lO1xuICB9KCk7XG5cbiAgdmFyIHNpbmdsZXRvbiA9IG51bGw7XG4gIC8qKlxuICAgKiBSZXByZXNlbnRzIHRoZSBsb2NhbCB6b25lIGZvciB0aGlzIEphdmFzY3JpcHQgZW52aXJvbm1lbnQuXG4gICAqIEBpbXBsZW1lbnRzIHtab25lfVxuICAgKi9cblxuICB2YXIgTG9jYWxab25lID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoX1pvbmUpIHtcbiAgICBfaW5oZXJpdHNMb29zZShMb2NhbFpvbmUsIF9ab25lKTtcblxuICAgIGZ1bmN0aW9uIExvY2FsWm9uZSgpIHtcbiAgICAgIHJldHVybiBfWm9uZS5hcHBseSh0aGlzLCBhcmd1bWVudHMpIHx8IHRoaXM7XG4gICAgfVxuXG4gICAgdmFyIF9wcm90byA9IExvY2FsWm9uZS5wcm90b3R5cGU7XG5cbiAgICAvKiogQG92ZXJyaWRlICoqL1xuICAgIF9wcm90by5vZmZzZXROYW1lID0gZnVuY3Rpb24gb2Zmc2V0TmFtZSh0cywgX3JlZikge1xuICAgICAgdmFyIGZvcm1hdCA9IF9yZWYuZm9ybWF0LFxuICAgICAgICAgIGxvY2FsZSA9IF9yZWYubG9jYWxlO1xuICAgICAgcmV0dXJuIHBhcnNlWm9uZUluZm8odHMsIGZvcm1hdCwgbG9jYWxlKTtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZm9ybWF0T2Zmc2V0ID0gZnVuY3Rpb24gZm9ybWF0T2Zmc2V0JDEodHMsIGZvcm1hdCkge1xuICAgICAgcmV0dXJuIGZvcm1hdE9mZnNldCh0aGlzLm9mZnNldCh0cyksIGZvcm1hdCk7XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX3Byb3RvLm9mZnNldCA9IGZ1bmN0aW9uIG9mZnNldCh0cykge1xuICAgICAgcmV0dXJuIC1uZXcgRGF0ZSh0cykuZ2V0VGltZXpvbmVPZmZzZXQoKTtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZXF1YWxzID0gZnVuY3Rpb24gZXF1YWxzKG90aGVyWm9uZSkge1xuICAgICAgcmV0dXJuIG90aGVyWm9uZS50eXBlID09PSBcImxvY2FsXCI7XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX2NyZWF0ZUNsYXNzKExvY2FsWm9uZSwgW3tcbiAgICAgIGtleTogXCJ0eXBlXCIsXG5cbiAgICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIFwibG9jYWxcIjtcbiAgICAgIH1cbiAgICAgIC8qKiBAb3ZlcnJpZGUgKiovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwibmFtZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIGlmIChoYXNJbnRsKCkpIHtcbiAgICAgICAgICByZXR1cm4gbmV3IEludGwuRGF0ZVRpbWVGb3JtYXQoKS5yZXNvbHZlZE9wdGlvbnMoKS50aW1lWm9uZTtcbiAgICAgICAgfSBlbHNlIHJldHVybiBcImxvY2FsXCI7XG4gICAgICB9XG4gICAgICAvKiogQG92ZXJyaWRlICoqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcInVuaXZlcnNhbFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaXNWYWxpZFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH1dLCBbe1xuICAgICAga2V5OiBcImluc3RhbmNlXCIsXG5cbiAgICAgIC8qKlxuICAgICAgICogR2V0IGEgc2luZ2xldG9uIGluc3RhbmNlIG9mIHRoZSBsb2NhbCB6b25lXG4gICAgICAgKiBAcmV0dXJuIHtMb2NhbFpvbmV9XG4gICAgICAgKi9cbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICBpZiAoc2luZ2xldG9uID09PSBudWxsKSB7XG4gICAgICAgICAgc2luZ2xldG9uID0gbmV3IExvY2FsWm9uZSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHNpbmdsZXRvbjtcbiAgICAgIH1cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gTG9jYWxab25lO1xuICB9KFpvbmUpO1xuXG4gIHZhciBtYXRjaGluZ1JlZ2V4ID0gUmVnRXhwKFwiXlwiICsgaWFuYVJlZ2V4LnNvdXJjZSArIFwiJFwiKTtcbiAgdmFyIGR0ZkNhY2hlID0ge307XG5cbiAgZnVuY3Rpb24gbWFrZURURih6b25lKSB7XG4gICAgaWYgKCFkdGZDYWNoZVt6b25lXSkge1xuICAgICAgZHRmQ2FjaGVbem9uZV0gPSBuZXcgSW50bC5EYXRlVGltZUZvcm1hdChcImVuLVVTXCIsIHtcbiAgICAgICAgaG91cjEyOiBmYWxzZSxcbiAgICAgICAgdGltZVpvbmU6IHpvbmUsXG4gICAgICAgIHllYXI6IFwibnVtZXJpY1wiLFxuICAgICAgICBtb250aDogXCIyLWRpZ2l0XCIsXG4gICAgICAgIGRheTogXCIyLWRpZ2l0XCIsXG4gICAgICAgIGhvdXI6IFwiMi1kaWdpdFwiLFxuICAgICAgICBtaW51dGU6IFwiMi1kaWdpdFwiLFxuICAgICAgICBzZWNvbmQ6IFwiMi1kaWdpdFwiXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4gZHRmQ2FjaGVbem9uZV07XG4gIH1cblxuICB2YXIgdHlwZVRvUG9zID0ge1xuICAgIHllYXI6IDAsXG4gICAgbW9udGg6IDEsXG4gICAgZGF5OiAyLFxuICAgIGhvdXI6IDMsXG4gICAgbWludXRlOiA0LFxuICAgIHNlY29uZDogNVxuICB9O1xuXG4gIGZ1bmN0aW9uIGhhY2t5T2Zmc2V0KGR0ZiwgZGF0ZSkge1xuICAgIHZhciBmb3JtYXR0ZWQgPSBkdGYuZm9ybWF0KGRhdGUpLnJlcGxhY2UoL1xcdTIwMEUvZywgXCJcIiksXG4gICAgICAgIHBhcnNlZCA9IC8oXFxkKylcXC8oXFxkKylcXC8oXFxkKyksPyAoXFxkKyk6KFxcZCspOihcXGQrKS8uZXhlYyhmb3JtYXR0ZWQpLFxuICAgICAgICBmTW9udGggPSBwYXJzZWRbMV0sXG4gICAgICAgIGZEYXkgPSBwYXJzZWRbMl0sXG4gICAgICAgIGZZZWFyID0gcGFyc2VkWzNdLFxuICAgICAgICBmSG91ciA9IHBhcnNlZFs0XSxcbiAgICAgICAgZk1pbnV0ZSA9IHBhcnNlZFs1XSxcbiAgICAgICAgZlNlY29uZCA9IHBhcnNlZFs2XTtcbiAgICByZXR1cm4gW2ZZZWFyLCBmTW9udGgsIGZEYXksIGZIb3VyLCBmTWludXRlLCBmU2Vjb25kXTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHBhcnRzT2Zmc2V0KGR0ZiwgZGF0ZSkge1xuICAgIHZhciBmb3JtYXR0ZWQgPSBkdGYuZm9ybWF0VG9QYXJ0cyhkYXRlKSxcbiAgICAgICAgZmlsbGVkID0gW107XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZvcm1hdHRlZC5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIF9mb3JtYXR0ZWQkaSA9IGZvcm1hdHRlZFtpXSxcbiAgICAgICAgICB0eXBlID0gX2Zvcm1hdHRlZCRpLnR5cGUsXG4gICAgICAgICAgdmFsdWUgPSBfZm9ybWF0dGVkJGkudmFsdWUsXG4gICAgICAgICAgcG9zID0gdHlwZVRvUG9zW3R5cGVdO1xuXG4gICAgICBpZiAoIWlzVW5kZWZpbmVkKHBvcykpIHtcbiAgICAgICAgZmlsbGVkW3Bvc10gPSBwYXJzZUludCh2YWx1ZSwgMTApO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmaWxsZWQ7XG4gIH1cblxuICB2YXIgaWFuYVpvbmVDYWNoZSA9IHt9O1xuICAvKipcbiAgICogQSB6b25lIGlkZW50aWZpZWQgYnkgYW4gSUFOQSBpZGVudGlmaWVyLCBsaWtlIEFtZXJpY2EvTmV3X1lvcmtcbiAgICogQGltcGxlbWVudHMge1pvbmV9XG4gICAqL1xuXG4gIHZhciBJQU5BWm9uZSA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKF9ab25lKSB7XG4gICAgX2luaGVyaXRzTG9vc2UoSUFOQVpvbmUsIF9ab25lKTtcblxuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lIC0gWm9uZSBuYW1lXG4gICAgICogQHJldHVybiB7SUFOQVpvbmV9XG4gICAgICovXG4gICAgSUFOQVpvbmUuY3JlYXRlID0gZnVuY3Rpb24gY3JlYXRlKG5hbWUpIHtcbiAgICAgIGlmICghaWFuYVpvbmVDYWNoZVtuYW1lXSkge1xuICAgICAgICBpYW5hWm9uZUNhY2hlW25hbWVdID0gbmV3IElBTkFab25lKG5hbWUpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gaWFuYVpvbmVDYWNoZVtuYW1lXTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmVzZXQgbG9jYWwgY2FjaGVzLiBTaG91bGQgb25seSBiZSBuZWNlc3NhcnkgaW4gdGVzdGluZyBzY2VuYXJpb3MuXG4gICAgICogQHJldHVybiB7dm9pZH1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBJQU5BWm9uZS5yZXNldENhY2hlID0gZnVuY3Rpb24gcmVzZXRDYWNoZSgpIHtcbiAgICAgIGlhbmFab25lQ2FjaGUgPSB7fTtcbiAgICAgIGR0ZkNhY2hlID0ge307XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgd2hldGhlciB0aGUgcHJvdmlkZWQgc3RyaW5nIGlzIGEgdmFsaWQgc3BlY2lmaWVyLiBUaGlzIG9ubHkgY2hlY2tzIHRoZSBzdHJpbmcncyBmb3JtYXQsIG5vdCB0aGF0IHRoZSBzcGVjaWZpZXIgaWRlbnRpZmllcyBhIGtub3duIHpvbmU7IHNlZSBpc1ZhbGlkWm9uZSBmb3IgdGhhdC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcyAtIFRoZSBzdHJpbmcgdG8gY2hlY2sgdmFsaWRpdHkgb25cbiAgICAgKiBAZXhhbXBsZSBJQU5BWm9uZS5pc1ZhbGlkU3BlY2lmaWVyKFwiQW1lcmljYS9OZXdfWW9ya1wiKSAvLz0+IHRydWVcbiAgICAgKiBAZXhhbXBsZSBJQU5BWm9uZS5pc1ZhbGlkU3BlY2lmaWVyKFwiRmFudGFzaWEvQ2FzdGxlXCIpIC8vPT4gdHJ1ZVxuICAgICAqIEBleGFtcGxlIElBTkFab25lLmlzVmFsaWRTcGVjaWZpZXIoXCJTcG9ydH5+YmxvcnBcIikgLy89PiBmYWxzZVxuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgO1xuXG4gICAgSUFOQVpvbmUuaXNWYWxpZFNwZWNpZmllciA9IGZ1bmN0aW9uIGlzVmFsaWRTcGVjaWZpZXIocykge1xuICAgICAgcmV0dXJuICEhKHMgJiYgcy5tYXRjaChtYXRjaGluZ1JlZ2V4KSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgd2hldGhlciB0aGUgcHJvdmlkZWQgc3RyaW5nIGlkZW50aWZpZXMgYSByZWFsIHpvbmVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gem9uZSAtIFRoZSBzdHJpbmcgdG8gY2hlY2tcbiAgICAgKiBAZXhhbXBsZSBJQU5BWm9uZS5pc1ZhbGlkWm9uZShcIkFtZXJpY2EvTmV3X1lvcmtcIikgLy89PiB0cnVlXG4gICAgICogQGV4YW1wbGUgSUFOQVpvbmUuaXNWYWxpZFpvbmUoXCJGYW50YXNpYS9DYXN0bGVcIikgLy89PiBmYWxzZVxuICAgICAqIEBleGFtcGxlIElBTkFab25lLmlzVmFsaWRab25lKFwiU3BvcnR+fmJsb3JwXCIpIC8vPT4gZmFsc2VcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIElBTkFab25lLmlzVmFsaWRab25lID0gZnVuY3Rpb24gaXNWYWxpZFpvbmUoem9uZSkge1xuICAgICAgdHJ5IHtcbiAgICAgICAgbmV3IEludGwuRGF0ZVRpbWVGb3JtYXQoXCJlbi1VU1wiLCB7XG4gICAgICAgICAgdGltZVpvbmU6IHpvbmVcbiAgICAgICAgfSkuZm9ybWF0KCk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfSAvLyBFdGMvR01UKzggLT4gLTQ4MFxuXG4gICAgLyoqIEBpZ25vcmUgKi9cbiAgICA7XG5cbiAgICBJQU5BWm9uZS5wYXJzZUdNVE9mZnNldCA9IGZ1bmN0aW9uIHBhcnNlR01UT2Zmc2V0KHNwZWNpZmllcikge1xuICAgICAgaWYgKHNwZWNpZmllcikge1xuICAgICAgICB2YXIgbWF0Y2ggPSBzcGVjaWZpZXIubWF0Y2goL15FdGNcXC9HTVQoWystXVxcZHsxLDJ9KSQvaSk7XG5cbiAgICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgICAgcmV0dXJuIC02MCAqIHBhcnNlSW50KG1hdGNoWzFdKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gSUFOQVpvbmUobmFtZSkge1xuICAgICAgdmFyIF90aGlzO1xuXG4gICAgICBfdGhpcyA9IF9ab25lLmNhbGwodGhpcykgfHwgdGhpcztcbiAgICAgIC8qKiBAcHJpdmF0ZSAqKi9cblxuICAgICAgX3RoaXMuem9uZU5hbWUgPSBuYW1lO1xuICAgICAgLyoqIEBwcml2YXRlICoqL1xuXG4gICAgICBfdGhpcy52YWxpZCA9IElBTkFab25lLmlzVmFsaWRab25lKG5hbWUpO1xuICAgICAgcmV0dXJuIF90aGlzO1xuICAgIH1cbiAgICAvKiogQG92ZXJyaWRlICoqL1xuXG5cbiAgICB2YXIgX3Byb3RvID0gSUFOQVpvbmUucHJvdG90eXBlO1xuXG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICBfcHJvdG8ub2Zmc2V0TmFtZSA9IGZ1bmN0aW9uIG9mZnNldE5hbWUodHMsIF9yZWYpIHtcbiAgICAgIHZhciBmb3JtYXQgPSBfcmVmLmZvcm1hdCxcbiAgICAgICAgICBsb2NhbGUgPSBfcmVmLmxvY2FsZTtcbiAgICAgIHJldHVybiBwYXJzZVpvbmVJbmZvKHRzLCBmb3JtYXQsIGxvY2FsZSwgdGhpcy5uYW1lKTtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZm9ybWF0T2Zmc2V0ID0gZnVuY3Rpb24gZm9ybWF0T2Zmc2V0JDEodHMsIGZvcm1hdCkge1xuICAgICAgcmV0dXJuIGZvcm1hdE9mZnNldCh0aGlzLm9mZnNldCh0cyksIGZvcm1hdCk7XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX3Byb3RvLm9mZnNldCA9IGZ1bmN0aW9uIG9mZnNldCh0cykge1xuICAgICAgdmFyIGRhdGUgPSBuZXcgRGF0ZSh0cyksXG4gICAgICAgICAgZHRmID0gbWFrZURURih0aGlzLm5hbWUpLFxuICAgICAgICAgIF9yZWYyID0gZHRmLmZvcm1hdFRvUGFydHMgPyBwYXJ0c09mZnNldChkdGYsIGRhdGUpIDogaGFja3lPZmZzZXQoZHRmLCBkYXRlKSxcbiAgICAgICAgICB5ZWFyID0gX3JlZjJbMF0sXG4gICAgICAgICAgbW9udGggPSBfcmVmMlsxXSxcbiAgICAgICAgICBkYXkgPSBfcmVmMlsyXSxcbiAgICAgICAgICBob3VyID0gX3JlZjJbM10sXG4gICAgICAgICAgbWludXRlID0gX3JlZjJbNF0sXG4gICAgICAgICAgc2Vjb25kID0gX3JlZjJbNV0sXG4gICAgICAgICAgYWRqdXN0ZWRIb3VyID0gaG91ciA9PT0gMjQgPyAwIDogaG91cjtcblxuICAgICAgdmFyIGFzVVRDID0gb2JqVG9Mb2NhbFRTKHtcbiAgICAgICAgeWVhcjogeWVhcixcbiAgICAgICAgbW9udGg6IG1vbnRoLFxuICAgICAgICBkYXk6IGRheSxcbiAgICAgICAgaG91cjogYWRqdXN0ZWRIb3VyLFxuICAgICAgICBtaW51dGU6IG1pbnV0ZSxcbiAgICAgICAgc2Vjb25kOiBzZWNvbmQsXG4gICAgICAgIG1pbGxpc2Vjb25kOiAwXG4gICAgICB9KTtcbiAgICAgIHZhciBhc1RTID0gZGF0ZS52YWx1ZU9mKCk7XG4gICAgICBhc1RTIC09IGFzVFMgJSAxMDAwO1xuICAgICAgcmV0dXJuIChhc1VUQyAtIGFzVFMpIC8gKDYwICogMTAwMCk7XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX3Byb3RvLmVxdWFscyA9IGZ1bmN0aW9uIGVxdWFscyhvdGhlclpvbmUpIHtcbiAgICAgIHJldHVybiBvdGhlclpvbmUudHlwZSA9PT0gXCJpYW5hXCIgJiYgb3RoZXJab25lLm5hbWUgPT09IHRoaXMubmFtZTtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfY3JlYXRlQ2xhc3MoSUFOQVpvbmUsIFt7XG4gICAgICBrZXk6IFwidHlwZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBcImlhbmFcIjtcbiAgICAgIH1cbiAgICAgIC8qKiBAb3ZlcnJpZGUgKiovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwibmFtZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnpvbmVOYW1lO1xuICAgICAgfVxuICAgICAgLyoqIEBvdmVycmlkZSAqKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ1bml2ZXJzYWxcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiBcImlzVmFsaWRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy52YWxpZDtcbiAgICAgIH1cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gSUFOQVpvbmU7XG4gIH0oWm9uZSk7XG5cbiAgdmFyIHNpbmdsZXRvbiQxID0gbnVsbDtcbiAgLyoqXG4gICAqIEEgem9uZSB3aXRoIGEgZml4ZWQgb2Zmc2V0IChtZWFuaW5nIG5vIERTVClcbiAgICogQGltcGxlbWVudHMge1pvbmV9XG4gICAqL1xuXG4gIHZhciBGaXhlZE9mZnNldFpvbmUgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uIChfWm9uZSkge1xuICAgIF9pbmhlcml0c0xvb3NlKEZpeGVkT2Zmc2V0Wm9uZSwgX1pvbmUpO1xuXG4gICAgLyoqXG4gICAgICogR2V0IGFuIGluc3RhbmNlIHdpdGggYSBzcGVjaWZpZWQgb2Zmc2V0XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9mZnNldCAtIFRoZSBvZmZzZXQgaW4gbWludXRlc1xuICAgICAqIEByZXR1cm4ge0ZpeGVkT2Zmc2V0Wm9uZX1cbiAgICAgKi9cbiAgICBGaXhlZE9mZnNldFpvbmUuaW5zdGFuY2UgPSBmdW5jdGlvbiBpbnN0YW5jZShvZmZzZXQpIHtcbiAgICAgIHJldHVybiBvZmZzZXQgPT09IDAgPyBGaXhlZE9mZnNldFpvbmUudXRjSW5zdGFuY2UgOiBuZXcgRml4ZWRPZmZzZXRab25lKG9mZnNldCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEdldCBhbiBpbnN0YW5jZSBvZiBGaXhlZE9mZnNldFpvbmUgZnJvbSBhIFVUQyBvZmZzZXQgc3RyaW5nLCBsaWtlIFwiVVRDKzZcIlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzIC0gVGhlIG9mZnNldCBzdHJpbmcgdG8gcGFyc2VcbiAgICAgKiBAZXhhbXBsZSBGaXhlZE9mZnNldFpvbmUucGFyc2VTcGVjaWZpZXIoXCJVVEMrNlwiKVxuICAgICAqIEBleGFtcGxlIEZpeGVkT2Zmc2V0Wm9uZS5wYXJzZVNwZWNpZmllcihcIlVUQyswNlwiKVxuICAgICAqIEBleGFtcGxlIEZpeGVkT2Zmc2V0Wm9uZS5wYXJzZVNwZWNpZmllcihcIlVUQy02OjAwXCIpXG4gICAgICogQHJldHVybiB7Rml4ZWRPZmZzZXRab25lfVxuICAgICAqL1xuICAgIDtcblxuICAgIEZpeGVkT2Zmc2V0Wm9uZS5wYXJzZVNwZWNpZmllciA9IGZ1bmN0aW9uIHBhcnNlU3BlY2lmaWVyKHMpIHtcbiAgICAgIGlmIChzKSB7XG4gICAgICAgIHZhciByID0gcy5tYXRjaCgvXnV0Yyg/OihbKy1dXFxkezEsMn0pKD86OihcXGR7Mn0pKT8pPyQvaSk7XG5cbiAgICAgICAgaWYgKHIpIHtcbiAgICAgICAgICByZXR1cm4gbmV3IEZpeGVkT2Zmc2V0Wm9uZShzaWduZWRPZmZzZXQoclsxXSwgclsyXSkpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICBfY3JlYXRlQ2xhc3MoRml4ZWRPZmZzZXRab25lLCBudWxsLCBbe1xuICAgICAga2V5OiBcInV0Y0luc3RhbmNlXCIsXG5cbiAgICAgIC8qKlxuICAgICAgICogR2V0IGEgc2luZ2xldG9uIGluc3RhbmNlIG9mIFVUQ1xuICAgICAgICogQHJldHVybiB7Rml4ZWRPZmZzZXRab25lfVxuICAgICAgICovXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgaWYgKHNpbmdsZXRvbiQxID09PSBudWxsKSB7XG4gICAgICAgICAgc2luZ2xldG9uJDEgPSBuZXcgRml4ZWRPZmZzZXRab25lKDApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHNpbmdsZXRvbiQxO1xuICAgICAgfVxuICAgIH1dKTtcblxuICAgIGZ1bmN0aW9uIEZpeGVkT2Zmc2V0Wm9uZShvZmZzZXQpIHtcbiAgICAgIHZhciBfdGhpcztcblxuICAgICAgX3RoaXMgPSBfWm9uZS5jYWxsKHRoaXMpIHx8IHRoaXM7XG4gICAgICAvKiogQHByaXZhdGUgKiovXG5cbiAgICAgIF90aGlzLmZpeGVkID0gb2Zmc2V0O1xuICAgICAgcmV0dXJuIF90aGlzO1xuICAgIH1cbiAgICAvKiogQG92ZXJyaWRlICoqL1xuXG5cbiAgICB2YXIgX3Byb3RvID0gRml4ZWRPZmZzZXRab25lLnByb3RvdHlwZTtcblxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgX3Byb3RvLm9mZnNldE5hbWUgPSBmdW5jdGlvbiBvZmZzZXROYW1lKCkge1xuICAgICAgcmV0dXJuIHRoaXMubmFtZTtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZm9ybWF0T2Zmc2V0ID0gZnVuY3Rpb24gZm9ybWF0T2Zmc2V0JDEodHMsIGZvcm1hdCkge1xuICAgICAgcmV0dXJuIGZvcm1hdE9mZnNldCh0aGlzLmZpeGVkLCBmb3JtYXQpO1xuICAgIH1cbiAgICAvKiogQG92ZXJyaWRlICoqL1xuICAgIDtcblxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgX3Byb3RvLm9mZnNldCA9IGZ1bmN0aW9uIG9mZnNldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmZpeGVkO1xuICAgIH1cbiAgICAvKiogQG92ZXJyaWRlICoqL1xuICAgIDtcblxuICAgIF9wcm90by5lcXVhbHMgPSBmdW5jdGlvbiBlcXVhbHMob3RoZXJab25lKSB7XG4gICAgICByZXR1cm4gb3RoZXJab25lLnR5cGUgPT09IFwiZml4ZWRcIiAmJiBvdGhlclpvbmUuZml4ZWQgPT09IHRoaXMuZml4ZWQ7XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX2NyZWF0ZUNsYXNzKEZpeGVkT2Zmc2V0Wm9uZSwgW3tcbiAgICAgIGtleTogXCJ0eXBlXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIFwiZml4ZWRcIjtcbiAgICAgIH1cbiAgICAgIC8qKiBAb3ZlcnJpZGUgKiovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwibmFtZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmZpeGVkID09PSAwID8gXCJVVENcIiA6IFwiVVRDXCIgKyBmb3JtYXRPZmZzZXQodGhpcy5maXhlZCwgXCJuYXJyb3dcIik7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiBcInVuaXZlcnNhbFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH0sIHtcbiAgICAgIGtleTogXCJpc1ZhbGlkXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfV0pO1xuXG4gICAgcmV0dXJuIEZpeGVkT2Zmc2V0Wm9uZTtcbiAgfShab25lKTtcblxuICAvKipcbiAgICogQSB6b25lIHRoYXQgZmFpbGVkIHRvIHBhcnNlLiBZb3Ugc2hvdWxkIG5ldmVyIG5lZWQgdG8gaW5zdGFudGlhdGUgdGhpcy5cbiAgICogQGltcGxlbWVudHMge1pvbmV9XG4gICAqL1xuXG4gIHZhciBJbnZhbGlkWm9uZSA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKF9ab25lKSB7XG4gICAgX2luaGVyaXRzTG9vc2UoSW52YWxpZFpvbmUsIF9ab25lKTtcblxuICAgIGZ1bmN0aW9uIEludmFsaWRab25lKHpvbmVOYW1lKSB7XG4gICAgICB2YXIgX3RoaXM7XG5cbiAgICAgIF90aGlzID0gX1pvbmUuY2FsbCh0aGlzKSB8fCB0aGlzO1xuICAgICAgLyoqICBAcHJpdmF0ZSAqL1xuXG4gICAgICBfdGhpcy56b25lTmFtZSA9IHpvbmVOYW1lO1xuICAgICAgcmV0dXJuIF90aGlzO1xuICAgIH1cbiAgICAvKiogQG92ZXJyaWRlICoqL1xuXG5cbiAgICB2YXIgX3Byb3RvID0gSW52YWxpZFpvbmUucHJvdG90eXBlO1xuXG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICBfcHJvdG8ub2Zmc2V0TmFtZSA9IGZ1bmN0aW9uIG9mZnNldE5hbWUoKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZm9ybWF0T2Zmc2V0ID0gZnVuY3Rpb24gZm9ybWF0T2Zmc2V0KCkge1xuICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX3Byb3RvLm9mZnNldCA9IGZ1bmN0aW9uIG9mZnNldCgpIHtcbiAgICAgIHJldHVybiBOYU47XG4gICAgfVxuICAgIC8qKiBAb3ZlcnJpZGUgKiovXG4gICAgO1xuXG4gICAgX3Byb3RvLmVxdWFscyA9IGZ1bmN0aW9uIGVxdWFscygpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgLyoqIEBvdmVycmlkZSAqKi9cbiAgICA7XG5cbiAgICBfY3JlYXRlQ2xhc3MoSW52YWxpZFpvbmUsIFt7XG4gICAgICBrZXk6IFwidHlwZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBcImludmFsaWRcIjtcbiAgICAgIH1cbiAgICAgIC8qKiBAb3ZlcnJpZGUgKiovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwibmFtZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnpvbmVOYW1lO1xuICAgICAgfVxuICAgICAgLyoqIEBvdmVycmlkZSAqKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ1bml2ZXJzYWxcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiBcImlzVmFsaWRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfV0pO1xuXG4gICAgcmV0dXJuIEludmFsaWRab25lO1xuICB9KFpvbmUpO1xuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgZnVuY3Rpb24gbm9ybWFsaXplWm9uZShpbnB1dCwgZGVmYXVsdFpvbmUpIHtcbiAgICB2YXIgb2Zmc2V0O1xuXG4gICAgaWYgKGlzVW5kZWZpbmVkKGlucHV0KSB8fCBpbnB1dCA9PT0gbnVsbCkge1xuICAgICAgcmV0dXJuIGRlZmF1bHRab25lO1xuICAgIH0gZWxzZSBpZiAoaW5wdXQgaW5zdGFuY2VvZiBab25lKSB7XG4gICAgICByZXR1cm4gaW5wdXQ7XG4gICAgfSBlbHNlIGlmIChpc1N0cmluZyhpbnB1dCkpIHtcbiAgICAgIHZhciBsb3dlcmVkID0gaW5wdXQudG9Mb3dlckNhc2UoKTtcbiAgICAgIGlmIChsb3dlcmVkID09PSBcImxvY2FsXCIpIHJldHVybiBkZWZhdWx0Wm9uZTtlbHNlIGlmIChsb3dlcmVkID09PSBcInV0Y1wiIHx8IGxvd2VyZWQgPT09IFwiZ210XCIpIHJldHVybiBGaXhlZE9mZnNldFpvbmUudXRjSW5zdGFuY2U7ZWxzZSBpZiAoKG9mZnNldCA9IElBTkFab25lLnBhcnNlR01UT2Zmc2V0KGlucHV0KSkgIT0gbnVsbCkge1xuICAgICAgICAvLyBoYW5kbGUgRXRjL0dNVC00LCB3aGljaCBWOCBjaG9rZXMgb25cbiAgICAgICAgcmV0dXJuIEZpeGVkT2Zmc2V0Wm9uZS5pbnN0YW5jZShvZmZzZXQpO1xuICAgICAgfSBlbHNlIGlmIChJQU5BWm9uZS5pc1ZhbGlkU3BlY2lmaWVyKGxvd2VyZWQpKSByZXR1cm4gSUFOQVpvbmUuY3JlYXRlKGlucHV0KTtlbHNlIHJldHVybiBGaXhlZE9mZnNldFpvbmUucGFyc2VTcGVjaWZpZXIobG93ZXJlZCkgfHwgbmV3IEludmFsaWRab25lKGlucHV0KTtcbiAgICB9IGVsc2UgaWYgKGlzTnVtYmVyKGlucHV0KSkge1xuICAgICAgcmV0dXJuIEZpeGVkT2Zmc2V0Wm9uZS5pbnN0YW5jZShpbnB1dCk7XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgaW5wdXQgPT09IFwib2JqZWN0XCIgJiYgaW5wdXQub2Zmc2V0ICYmIHR5cGVvZiBpbnB1dC5vZmZzZXQgPT09IFwibnVtYmVyXCIpIHtcbiAgICAgIC8vIFRoaXMgaXMgZHVtYiwgYnV0IHRoZSBpbnN0YW5jZW9mIGNoZWNrIGFib3ZlIGRvZXNuJ3Qgc2VlbSB0byByZWFsbHkgd29ya1xuICAgICAgLy8gc28gd2UncmUgZHVjayBjaGVja2luZyBpdFxuICAgICAgcmV0dXJuIGlucHV0O1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbmV3IEludmFsaWRab25lKGlucHV0KTtcbiAgICB9XG4gIH1cblxuICB2YXIgbm93ID0gZnVuY3Rpb24gbm93KCkge1xuICAgIHJldHVybiBEYXRlLm5vdygpO1xuICB9LFxuICAgICAgZGVmYXVsdFpvbmUgPSBudWxsLFxuICAgICAgLy8gbm90IHNldHRpbmcgdGhpcyBkaXJlY3RseSB0byBMb2NhbFpvbmUuaW5zdGFuY2UgYmMgbG9hZGluZyBvcmRlciBpc3N1ZXNcbiAgZGVmYXVsdExvY2FsZSA9IG51bGwsXG4gICAgICBkZWZhdWx0TnVtYmVyaW5nU3lzdGVtID0gbnVsbCxcbiAgICAgIGRlZmF1bHRPdXRwdXRDYWxlbmRhciA9IG51bGwsXG4gICAgICB0aHJvd09uSW52YWxpZCA9IGZhbHNlO1xuICAvKipcbiAgICogU2V0dGluZ3MgY29udGFpbnMgc3RhdGljIGdldHRlcnMgYW5kIHNldHRlcnMgdGhhdCBjb250cm9sIEx1eG9uJ3Mgb3ZlcmFsbCBiZWhhdmlvci4gTHV4b24gaXMgYSBzaW1wbGUgbGlicmFyeSB3aXRoIGZldyBvcHRpb25zLCBidXQgdGhlIG9uZXMgaXQgZG9lcyBoYXZlIGxpdmUgaGVyZS5cbiAgICovXG5cblxuICB2YXIgU2V0dGluZ3MgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBTZXR0aW5ncygpIHt9XG5cbiAgICAvKipcbiAgICAgKiBSZXNldCBMdXhvbidzIGdsb2JhbCBjYWNoZXMuIFNob3VsZCBvbmx5IGJlIG5lY2Vzc2FyeSBpbiB0ZXN0aW5nIHNjZW5hcmlvcy5cbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgICAqL1xuICAgIFNldHRpbmdzLnJlc2V0Q2FjaGVzID0gZnVuY3Rpb24gcmVzZXRDYWNoZXMoKSB7XG4gICAgICBMb2NhbGUucmVzZXRDYWNoZSgpO1xuICAgICAgSUFOQVpvbmUucmVzZXRDYWNoZSgpO1xuICAgIH07XG5cbiAgICBfY3JlYXRlQ2xhc3MoU2V0dGluZ3MsIG51bGwsIFt7XG4gICAgICBrZXk6IFwibm93XCIsXG5cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBjYWxsYmFjayBmb3IgcmV0dXJuaW5nIHRoZSBjdXJyZW50IHRpbWVzdGFtcC5cbiAgICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cbiAgICAgICAqL1xuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBub3c7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFNldCB0aGUgY2FsbGJhY2sgZm9yIHJldHVybmluZyB0aGUgY3VycmVudCB0aW1lc3RhbXAuXG4gICAgICAgKiBUaGUgZnVuY3Rpb24gc2hvdWxkIHJldHVybiBhIG51bWJlciwgd2hpY2ggd2lsbCBiZSBpbnRlcnByZXRlZCBhcyBhbiBFcG9jaCBtaWxsaXNlY29uZCBjb3VudFxuICAgICAgICogQHR5cGUge2Z1bmN0aW9ufVxuICAgICAgICogQGV4YW1wbGUgU2V0dGluZ3Mubm93ID0gKCkgPT4gRGF0ZS5ub3coKSArIDMwMDAgLy8gcHJldGVuZCBpdCBpcyAzIHNlY29uZHMgaW4gdGhlIGZ1dHVyZVxuICAgICAgICogQGV4YW1wbGUgU2V0dGluZ3Mubm93ID0gKCkgPT4gMCAvLyBhbHdheXMgcHJldGVuZCBpdCdzIEphbiAxLCAxOTcwIGF0IG1pZG5pZ2h0IGluIFVUQyB0aW1lXG4gICAgICAgKi9cbiAgICAgICxcbiAgICAgIHNldDogZnVuY3Rpb24gc2V0KG4pIHtcbiAgICAgICAgbm93ID0gbjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBkZWZhdWx0IHRpbWUgem9uZSB0byBjcmVhdGUgRGF0ZVRpbWVzIGluLlxuICAgICAgICogQHR5cGUge3N0cmluZ31cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcImRlZmF1bHRab25lTmFtZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBTZXR0aW5ncy5kZWZhdWx0Wm9uZS5uYW1lO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBTZXQgdGhlIGRlZmF1bHQgdGltZSB6b25lIHRvIGNyZWF0ZSBEYXRlVGltZXMgaW4uIERvZXMgbm90IGFmZmVjdCBleGlzdGluZyBpbnN0YW5jZXMuXG4gICAgICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgICAgICovXG4gICAgICAsXG4gICAgICBzZXQ6IGZ1bmN0aW9uIHNldCh6KSB7XG4gICAgICAgIGlmICgheikge1xuICAgICAgICAgIGRlZmF1bHRab25lID0gbnVsbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBkZWZhdWx0Wm9uZSA9IG5vcm1hbGl6ZVpvbmUoeik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBkZWZhdWx0IHRpbWUgem9uZSBvYmplY3QgdG8gY3JlYXRlIERhdGVUaW1lcyBpbi4gRG9lcyBub3QgYWZmZWN0IGV4aXN0aW5nIGluc3RhbmNlcy5cbiAgICAgICAqIEB0eXBlIHtab25lfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiZGVmYXVsdFpvbmVcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gZGVmYXVsdFpvbmUgfHwgTG9jYWxab25lLmluc3RhbmNlO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIGRlZmF1bHQgbG9jYWxlIHRvIGNyZWF0ZSBEYXRlVGltZXMgd2l0aC4gRG9lcyBub3QgYWZmZWN0IGV4aXN0aW5nIGluc3RhbmNlcy5cbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJkZWZhdWx0TG9jYWxlXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIGRlZmF1bHRMb2NhbGU7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFNldCB0aGUgZGVmYXVsdCBsb2NhbGUgdG8gY3JlYXRlIERhdGVUaW1lcyB3aXRoLiBEb2VzIG5vdCBhZmZlY3QgZXhpc3RpbmcgaW5zdGFuY2VzLlxuICAgICAgICogQHR5cGUge3N0cmluZ31cbiAgICAgICAqL1xuICAgICAgLFxuICAgICAgc2V0OiBmdW5jdGlvbiBzZXQobG9jYWxlKSB7XG4gICAgICAgIGRlZmF1bHRMb2NhbGUgPSBsb2NhbGU7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgZGVmYXVsdCBudW1iZXJpbmcgc3lzdGVtIHRvIGNyZWF0ZSBEYXRlVGltZXMgd2l0aC4gRG9lcyBub3QgYWZmZWN0IGV4aXN0aW5nIGluc3RhbmNlcy5cbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJkZWZhdWx0TnVtYmVyaW5nU3lzdGVtXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIGRlZmF1bHROdW1iZXJpbmdTeXN0ZW07XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFNldCB0aGUgZGVmYXVsdCBudW1iZXJpbmcgc3lzdGVtIHRvIGNyZWF0ZSBEYXRlVGltZXMgd2l0aC4gRG9lcyBub3QgYWZmZWN0IGV4aXN0aW5nIGluc3RhbmNlcy5cbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cbiAgICAgICxcbiAgICAgIHNldDogZnVuY3Rpb24gc2V0KG51bWJlcmluZ1N5c3RlbSkge1xuICAgICAgICBkZWZhdWx0TnVtYmVyaW5nU3lzdGVtID0gbnVtYmVyaW5nU3lzdGVtO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIGRlZmF1bHQgb3V0cHV0IGNhbGVuZGFyIHRvIGNyZWF0ZSBEYXRlVGltZXMgd2l0aC4gRG9lcyBub3QgYWZmZWN0IGV4aXN0aW5nIGluc3RhbmNlcy5cbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJkZWZhdWx0T3V0cHV0Q2FsZW5kYXJcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gZGVmYXVsdE91dHB1dENhbGVuZGFyO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBTZXQgdGhlIGRlZmF1bHQgb3V0cHV0IGNhbGVuZGFyIHRvIGNyZWF0ZSBEYXRlVGltZXMgd2l0aC4gRG9lcyBub3QgYWZmZWN0IGV4aXN0aW5nIGluc3RhbmNlcy5cbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cbiAgICAgICxcbiAgICAgIHNldDogZnVuY3Rpb24gc2V0KG91dHB1dENhbGVuZGFyKSB7XG4gICAgICAgIGRlZmF1bHRPdXRwdXRDYWxlbmRhciA9IG91dHB1dENhbGVuZGFyO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgd2hldGhlciBMdXhvbiB3aWxsIHRocm93IHdoZW4gaXQgZW5jb3VudGVycyBpbnZhbGlkIERhdGVUaW1lcywgRHVyYXRpb25zLCBvciBJbnRlcnZhbHNcbiAgICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwidGhyb3dPbkludmFsaWRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhyb3dPbkludmFsaWQ7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFNldCB3aGV0aGVyIEx1eG9uIHdpbGwgdGhyb3cgd2hlbiBpdCBlbmNvdW50ZXJzIGludmFsaWQgRGF0ZVRpbWVzLCBEdXJhdGlvbnMsIG9yIEludGVydmFsc1xuICAgICAgICogQHR5cGUge2Jvb2xlYW59XG4gICAgICAgKi9cbiAgICAgICxcbiAgICAgIHNldDogZnVuY3Rpb24gc2V0KHQpIHtcbiAgICAgICAgdGhyb3dPbkludmFsaWQgPSB0O1xuICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBTZXR0aW5ncztcbiAgfSgpO1xuXG4gIHZhciBpbnRsRFRDYWNoZSA9IHt9O1xuXG4gIGZ1bmN0aW9uIGdldENhY2hlZERURihsb2NTdHJpbmcsIG9wdHMpIHtcbiAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICBvcHRzID0ge307XG4gICAgfVxuXG4gICAgdmFyIGtleSA9IEpTT04uc3RyaW5naWZ5KFtsb2NTdHJpbmcsIG9wdHNdKTtcbiAgICB2YXIgZHRmID0gaW50bERUQ2FjaGVba2V5XTtcblxuICAgIGlmICghZHRmKSB7XG4gICAgICBkdGYgPSBuZXcgSW50bC5EYXRlVGltZUZvcm1hdChsb2NTdHJpbmcsIG9wdHMpO1xuICAgICAgaW50bERUQ2FjaGVba2V5XSA9IGR0ZjtcbiAgICB9XG5cbiAgICByZXR1cm4gZHRmO1xuICB9XG5cbiAgdmFyIGludGxOdW1DYWNoZSA9IHt9O1xuXG4gIGZ1bmN0aW9uIGdldENhY2hlZElORihsb2NTdHJpbmcsIG9wdHMpIHtcbiAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICBvcHRzID0ge307XG4gICAgfVxuXG4gICAgdmFyIGtleSA9IEpTT04uc3RyaW5naWZ5KFtsb2NTdHJpbmcsIG9wdHNdKTtcbiAgICB2YXIgaW5mID0gaW50bE51bUNhY2hlW2tleV07XG5cbiAgICBpZiAoIWluZikge1xuICAgICAgaW5mID0gbmV3IEludGwuTnVtYmVyRm9ybWF0KGxvY1N0cmluZywgb3B0cyk7XG4gICAgICBpbnRsTnVtQ2FjaGVba2V5XSA9IGluZjtcbiAgICB9XG5cbiAgICByZXR1cm4gaW5mO1xuICB9XG5cbiAgdmFyIGludGxSZWxDYWNoZSA9IHt9O1xuXG4gIGZ1bmN0aW9uIGdldENhY2hlZFJURihsb2NTdHJpbmcsIG9wdHMpIHtcbiAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICBvcHRzID0ge307XG4gICAgfVxuXG4gICAgdmFyIGtleSA9IEpTT04uc3RyaW5naWZ5KFtsb2NTdHJpbmcsIG9wdHNdKTtcbiAgICB2YXIgaW5mID0gaW50bFJlbENhY2hlW2tleV07XG5cbiAgICBpZiAoIWluZikge1xuICAgICAgaW5mID0gbmV3IEludGwuUmVsYXRpdmVUaW1lRm9ybWF0KGxvY1N0cmluZywgb3B0cyk7XG4gICAgICBpbnRsUmVsQ2FjaGVba2V5XSA9IGluZjtcbiAgICB9XG5cbiAgICByZXR1cm4gaW5mO1xuICB9XG5cbiAgdmFyIHN5c0xvY2FsZUNhY2hlID0gbnVsbDtcblxuICBmdW5jdGlvbiBzeXN0ZW1Mb2NhbGUoKSB7XG4gICAgaWYgKHN5c0xvY2FsZUNhY2hlKSB7XG4gICAgICByZXR1cm4gc3lzTG9jYWxlQ2FjaGU7XG4gICAgfSBlbHNlIGlmIChoYXNJbnRsKCkpIHtcbiAgICAgIHZhciBjb21wdXRlZFN5cyA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KCkucmVzb2x2ZWRPcHRpb25zKCkubG9jYWxlOyAvLyBub2RlIHNvbWV0aW1lcyBkZWZhdWx0cyB0byBcInVuZFwiLiBPdmVycmlkZSB0aGF0IGJlY2F1c2UgdGhhdCBpcyBkdW1iXG5cbiAgICAgIHN5c0xvY2FsZUNhY2hlID0gIWNvbXB1dGVkU3lzIHx8IGNvbXB1dGVkU3lzID09PSBcInVuZFwiID8gXCJlbi1VU1wiIDogY29tcHV0ZWRTeXM7XG4gICAgICByZXR1cm4gc3lzTG9jYWxlQ2FjaGU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN5c0xvY2FsZUNhY2hlID0gXCJlbi1VU1wiO1xuICAgICAgcmV0dXJuIHN5c0xvY2FsZUNhY2hlO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHBhcnNlTG9jYWxlU3RyaW5nKGxvY2FsZVN0cikge1xuICAgIC8vIEkgcmVhbGx5IHdhbnQgdG8gYXZvaWQgd3JpdGluZyBhIEJDUCA0NyBwYXJzZXJcbiAgICAvLyBzZWUsIGUuZy4gaHR0cHM6Ly9naXRodWIuY29tL3dvb29ybS9iY3AtNDdcbiAgICAvLyBJbnN0ZWFkLCB3ZSdsbCBkbyB0aGlzOlxuICAgIC8vIGEpIGlmIHRoZSBzdHJpbmcgaGFzIG5vIC11IGV4dGVuc2lvbnMsIGp1c3QgbGVhdmUgaXQgYWxvbmVcbiAgICAvLyBiKSBpZiBpdCBkb2VzLCB1c2UgSW50bCB0byByZXNvbHZlIGV2ZXJ5dGhpbmdcbiAgICAvLyBjKSBpZiBJbnRsIGZhaWxzLCB0cnkgYWdhaW4gd2l0aG91dCB0aGUgLXVcbiAgICB2YXIgdUluZGV4ID0gbG9jYWxlU3RyLmluZGV4T2YoXCItdS1cIik7XG5cbiAgICBpZiAodUluZGV4ID09PSAtMSkge1xuICAgICAgcmV0dXJuIFtsb2NhbGVTdHJdO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgb3B0aW9ucztcbiAgICAgIHZhciBzbWFsbGVyID0gbG9jYWxlU3RyLnN1YnN0cmluZygwLCB1SW5kZXgpO1xuXG4gICAgICB0cnkge1xuICAgICAgICBvcHRpb25zID0gZ2V0Q2FjaGVkRFRGKGxvY2FsZVN0cikucmVzb2x2ZWRPcHRpb25zKCk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIG9wdGlvbnMgPSBnZXRDYWNoZWREVEYoc21hbGxlcikucmVzb2x2ZWRPcHRpb25zKCk7XG4gICAgICB9XG5cbiAgICAgIHZhciBfb3B0aW9ucyA9IG9wdGlvbnMsXG4gICAgICAgICAgbnVtYmVyaW5nU3lzdGVtID0gX29wdGlvbnMubnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIGNhbGVuZGFyID0gX29wdGlvbnMuY2FsZW5kYXI7IC8vIHJldHVybiB0aGUgc21hbGxlciBvbmUgc28gdGhhdCB3ZSBjYW4gYXBwZW5kIHRoZSBjYWxlbmRhciBhbmQgbnVtYmVyaW5nIG92ZXJyaWRlcyB0byBpdFxuXG4gICAgICByZXR1cm4gW3NtYWxsZXIsIG51bWJlcmluZ1N5c3RlbSwgY2FsZW5kYXJdO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGludGxDb25maWdTdHJpbmcobG9jYWxlU3RyLCBudW1iZXJpbmdTeXN0ZW0sIG91dHB1dENhbGVuZGFyKSB7XG4gICAgaWYgKGhhc0ludGwoKSkge1xuICAgICAgaWYgKG91dHB1dENhbGVuZGFyIHx8IG51bWJlcmluZ1N5c3RlbSkge1xuICAgICAgICBsb2NhbGVTdHIgKz0gXCItdVwiO1xuXG4gICAgICAgIGlmIChvdXRwdXRDYWxlbmRhcikge1xuICAgICAgICAgIGxvY2FsZVN0ciArPSBcIi1jYS1cIiArIG91dHB1dENhbGVuZGFyO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG51bWJlcmluZ1N5c3RlbSkge1xuICAgICAgICAgIGxvY2FsZVN0ciArPSBcIi1udS1cIiArIG51bWJlcmluZ1N5c3RlbTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBsb2NhbGVTdHI7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gbG9jYWxlU3RyO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gW107XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gbWFwTW9udGhzKGYpIHtcbiAgICB2YXIgbXMgPSBbXTtcblxuICAgIGZvciAodmFyIGkgPSAxOyBpIDw9IDEyOyBpKyspIHtcbiAgICAgIHZhciBkdCA9IERhdGVUaW1lLnV0YygyMDE2LCBpLCAxKTtcbiAgICAgIG1zLnB1c2goZihkdCkpO1xuICAgIH1cblxuICAgIHJldHVybiBtcztcbiAgfVxuXG4gIGZ1bmN0aW9uIG1hcFdlZWtkYXlzKGYpIHtcbiAgICB2YXIgbXMgPSBbXTtcblxuICAgIGZvciAodmFyIGkgPSAxOyBpIDw9IDc7IGkrKykge1xuICAgICAgdmFyIGR0ID0gRGF0ZVRpbWUudXRjKDIwMTYsIDExLCAxMyArIGkpO1xuICAgICAgbXMucHVzaChmKGR0KSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1zO1xuICB9XG5cbiAgZnVuY3Rpb24gbGlzdFN0dWZmKGxvYywgbGVuZ3RoLCBkZWZhdWx0T0ssIGVuZ2xpc2hGbiwgaW50bEZuKSB7XG4gICAgdmFyIG1vZGUgPSBsb2MubGlzdGluZ01vZGUoZGVmYXVsdE9LKTtcblxuICAgIGlmIChtb2RlID09PSBcImVycm9yXCIpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH0gZWxzZSBpZiAobW9kZSA9PT0gXCJlblwiKSB7XG4gICAgICByZXR1cm4gZW5nbGlzaEZuKGxlbmd0aCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBpbnRsRm4obGVuZ3RoKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBzdXBwb3J0c0Zhc3ROdW1iZXJzKGxvYykge1xuICAgIGlmIChsb2MubnVtYmVyaW5nU3lzdGVtICYmIGxvYy5udW1iZXJpbmdTeXN0ZW0gIT09IFwibGF0blwiKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBsb2MubnVtYmVyaW5nU3lzdGVtID09PSBcImxhdG5cIiB8fCAhbG9jLmxvY2FsZSB8fCBsb2MubG9jYWxlLnN0YXJ0c1dpdGgoXCJlblwiKSB8fCBoYXNJbnRsKCkgJiYgbmV3IEludGwuRGF0ZVRpbWVGb3JtYXQobG9jLmludGwpLnJlc29sdmVkT3B0aW9ucygpLm51bWJlcmluZ1N5c3RlbSA9PT0gXCJsYXRuXCI7XG4gICAgfVxuICB9XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIHZhciBQb2x5TnVtYmVyRm9ybWF0dGVyID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gUG9seU51bWJlckZvcm1hdHRlcihpbnRsLCBmb3JjZVNpbXBsZSwgb3B0cykge1xuICAgICAgdGhpcy5wYWRUbyA9IG9wdHMucGFkVG8gfHwgMDtcbiAgICAgIHRoaXMuZmxvb3IgPSBvcHRzLmZsb29yIHx8IGZhbHNlO1xuXG4gICAgICBpZiAoIWZvcmNlU2ltcGxlICYmIGhhc0ludGwoKSkge1xuICAgICAgICB2YXIgaW50bE9wdHMgPSB7XG4gICAgICAgICAgdXNlR3JvdXBpbmc6IGZhbHNlXG4gICAgICAgIH07XG4gICAgICAgIGlmIChvcHRzLnBhZFRvID4gMCkgaW50bE9wdHMubWluaW11bUludGVnZXJEaWdpdHMgPSBvcHRzLnBhZFRvO1xuICAgICAgICB0aGlzLmluZiA9IGdldENhY2hlZElORihpbnRsLCBpbnRsT3B0cyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIF9wcm90byA9IFBvbHlOdW1iZXJGb3JtYXR0ZXIucHJvdG90eXBlO1xuXG4gICAgX3Byb3RvLmZvcm1hdCA9IGZ1bmN0aW9uIGZvcm1hdChpKSB7XG4gICAgICBpZiAodGhpcy5pbmYpIHtcbiAgICAgICAgdmFyIGZpeGVkID0gdGhpcy5mbG9vciA/IE1hdGguZmxvb3IoaSkgOiBpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbmYuZm9ybWF0KGZpeGVkKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIHRvIG1hdGNoIHRoZSBicm93c2VyJ3MgbnVtYmVyZm9ybWF0dGVyIGRlZmF1bHRzXG4gICAgICAgIHZhciBfZml4ZWQgPSB0aGlzLmZsb29yID8gTWF0aC5mbG9vcihpKSA6IHJvdW5kVG8oaSwgMyk7XG5cbiAgICAgICAgcmV0dXJuIHBhZFN0YXJ0KF9maXhlZCwgdGhpcy5wYWRUbyk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIHJldHVybiBQb2x5TnVtYmVyRm9ybWF0dGVyO1xuICB9KCk7XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIHZhciBQb2x5RGF0ZUZvcm1hdHRlciA9XG4gIC8qI19fUFVSRV9fKi9cbiAgZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIFBvbHlEYXRlRm9ybWF0dGVyKGR0LCBpbnRsLCBvcHRzKSB7XG4gICAgICB0aGlzLm9wdHMgPSBvcHRzO1xuICAgICAgdGhpcy5oYXNJbnRsID0gaGFzSW50bCgpO1xuICAgICAgdmFyIHo7XG5cbiAgICAgIGlmIChkdC56b25lLnVuaXZlcnNhbCAmJiB0aGlzLmhhc0ludGwpIHtcbiAgICAgICAgLy8gQ2hyb21pdW0gZG9lc24ndCBzdXBwb3J0IGZpeGVkLW9mZnNldCB6b25lcyBsaWtlIEV0Yy9HTVQrOCBpbiBpdHMgZm9ybWF0dGVyLFxuICAgICAgICAvLyBTZWUgaHR0cHM6Ly9idWdzLmNocm9taXVtLm9yZy9wL2Nocm9taXVtL2lzc3Vlcy9kZXRhaWw/aWQ9MzY0Mzc0LlxuICAgICAgICAvLyBTbyB3ZSBoYXZlIHRvIG1ha2UgZG8uIFR3byBjYXNlczpcbiAgICAgICAgLy8gMS4gVGhlIGZvcm1hdCBvcHRpb25zIHRlbGwgdXMgdG8gc2hvdyB0aGUgem9uZS4gV2UgY2FuJ3QgZG8gdGhhdCwgc28gdGhlIGJlc3RcbiAgICAgICAgLy8gd2UgY2FuIGRvIGlzIGZvcm1hdCB0aGUgZGF0ZSBpbiBVVEMuXG4gICAgICAgIC8vIDIuIFRoZSBmb3JtYXQgb3B0aW9ucyBkb24ndCB0ZWxsIHVzIHRvIHNob3cgdGhlIHpvbmUuIFRoZW4gd2UgY2FuIGFkanVzdCB0aGVtXG4gICAgICAgIC8vIHRoZSB0aW1lIGFuZCB0ZWxsIHRoZSBmb3JtYXR0ZXIgdG8gc2hvdyBpdCB0byB1cyBpbiBVVEMsIHNvIHRoYXQgdGhlIHRpbWUgaXMgcmlnaHRcbiAgICAgICAgLy8gYW5kIHRoZSBiYWQgem9uZSBkb2Vzbid0IHNob3cgdXAuXG4gICAgICAgIC8vIFdlIGNhbiBjbGVhbiBhbGwgdGhpcyB1cCB3aGVuIENocm9tZSBmaXhlcyB0aGlzLlxuICAgICAgICB6ID0gXCJVVENcIjtcblxuICAgICAgICBpZiAob3B0cy50aW1lWm9uZU5hbWUpIHtcbiAgICAgICAgICB0aGlzLmR0ID0gZHQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5kdCA9IGR0Lm9mZnNldCA9PT0gMCA/IGR0IDogRGF0ZVRpbWUuZnJvbU1pbGxpcyhkdC50cyArIGR0Lm9mZnNldCAqIDYwICogMTAwMCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoZHQuem9uZS50eXBlID09PSBcImxvY2FsXCIpIHtcbiAgICAgICAgdGhpcy5kdCA9IGR0O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5kdCA9IGR0O1xuICAgICAgICB6ID0gZHQuem9uZS5uYW1lO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5oYXNJbnRsKSB7XG4gICAgICAgIHZhciBpbnRsT3B0cyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMub3B0cyk7XG5cbiAgICAgICAgaWYgKHopIHtcbiAgICAgICAgICBpbnRsT3B0cy50aW1lWm9uZSA9IHo7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmR0ZiA9IGdldENhY2hlZERURihpbnRsLCBpbnRsT3B0cyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIF9wcm90bzIgPSBQb2x5RGF0ZUZvcm1hdHRlci5wcm90b3R5cGU7XG5cbiAgICBfcHJvdG8yLmZvcm1hdCA9IGZ1bmN0aW9uIGZvcm1hdCgpIHtcbiAgICAgIGlmICh0aGlzLmhhc0ludGwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZHRmLmZvcm1hdCh0aGlzLmR0LnRvSlNEYXRlKCkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIHRva2VuRm9ybWF0ID0gZm9ybWF0U3RyaW5nKHRoaXMub3B0cyksXG4gICAgICAgICAgICBsb2MgPSBMb2NhbGUuY3JlYXRlKFwiZW4tVVNcIik7XG4gICAgICAgIHJldHVybiBGb3JtYXR0ZXIuY3JlYXRlKGxvYykuZm9ybWF0RGF0ZVRpbWVGcm9tU3RyaW5nKHRoaXMuZHQsIHRva2VuRm9ybWF0KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3Byb3RvMi5mb3JtYXRUb1BhcnRzID0gZnVuY3Rpb24gZm9ybWF0VG9QYXJ0cygpIHtcbiAgICAgIGlmICh0aGlzLmhhc0ludGwgJiYgaGFzRm9ybWF0VG9QYXJ0cygpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmR0Zi5mb3JtYXRUb1BhcnRzKHRoaXMuZHQudG9KU0RhdGUoKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBUaGlzIGlzIGtpbmQgb2YgYSBjb3Agb3V0LiBXZSBhY3R1YWxseSBjb3VsZCBkbyB0aGlzIGZvciBFbmdsaXNoLiBIb3dldmVyLCB3ZSBjb3VsZG4ndCBkbyBpdCBmb3IgaW50bCBzdHJpbmdzXG4gICAgICAgIC8vIGFuZCBJTU8gaXQncyB0b28gd2VpcmQgdG8gaGF2ZSBhbiB1bmNhbm55IHZhbGxleSBsaWtlIHRoYXRcbiAgICAgICAgcmV0dXJuIFtdO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfcHJvdG8yLnJlc29sdmVkT3B0aW9ucyA9IGZ1bmN0aW9uIHJlc29sdmVkT3B0aW9ucygpIHtcbiAgICAgIGlmICh0aGlzLmhhc0ludGwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZHRmLnJlc29sdmVkT3B0aW9ucygpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBsb2NhbGU6IFwiZW4tVVNcIixcbiAgICAgICAgICBudW1iZXJpbmdTeXN0ZW06IFwibGF0blwiLFxuICAgICAgICAgIG91dHB1dENhbGVuZGFyOiBcImdyZWdvcnlcIlxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gUG9seURhdGVGb3JtYXR0ZXI7XG4gIH0oKTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG5cbiAgdmFyIFBvbHlSZWxGb3JtYXR0ZXIgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBQb2x5UmVsRm9ybWF0dGVyKGludGwsIGlzRW5nbGlzaCwgb3B0cykge1xuICAgICAgdGhpcy5vcHRzID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICAgIHN0eWxlOiBcImxvbmdcIlxuICAgICAgfSwgb3B0cyk7XG5cbiAgICAgIGlmICghaXNFbmdsaXNoICYmIGhhc1JlbGF0aXZlKCkpIHtcbiAgICAgICAgdGhpcy5ydGYgPSBnZXRDYWNoZWRSVEYoaW50bCwgb3B0cyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIF9wcm90bzMgPSBQb2x5UmVsRm9ybWF0dGVyLnByb3RvdHlwZTtcblxuICAgIF9wcm90bzMuZm9ybWF0ID0gZnVuY3Rpb24gZm9ybWF0KGNvdW50LCB1bml0KSB7XG4gICAgICBpZiAodGhpcy5ydGYpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucnRmLmZvcm1hdChjb3VudCwgdW5pdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZm9ybWF0UmVsYXRpdmVUaW1lKHVuaXQsIGNvdW50LCB0aGlzLm9wdHMubnVtZXJpYywgdGhpcy5vcHRzLnN0eWxlICE9PSBcImxvbmdcIik7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF9wcm90bzMuZm9ybWF0VG9QYXJ0cyA9IGZ1bmN0aW9uIGZvcm1hdFRvUGFydHMoY291bnQsIHVuaXQpIHtcbiAgICAgIGlmICh0aGlzLnJ0Zikge1xuICAgICAgICByZXR1cm4gdGhpcy5ydGYuZm9ybWF0VG9QYXJ0cyhjb3VudCwgdW5pdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gW107XG4gICAgICB9XG4gICAgfTtcblxuICAgIHJldHVybiBQb2x5UmVsRm9ybWF0dGVyO1xuICB9KCk7XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIHZhciBMb2NhbGUgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICBMb2NhbGUuZnJvbU9wdHMgPSBmdW5jdGlvbiBmcm9tT3B0cyhvcHRzKSB7XG4gICAgICByZXR1cm4gTG9jYWxlLmNyZWF0ZShvcHRzLmxvY2FsZSwgb3B0cy5udW1iZXJpbmdTeXN0ZW0sIG9wdHMub3V0cHV0Q2FsZW5kYXIsIG9wdHMuZGVmYXVsdFRvRU4pO1xuICAgIH07XG5cbiAgICBMb2NhbGUuY3JlYXRlID0gZnVuY3Rpb24gY3JlYXRlKGxvY2FsZSwgbnVtYmVyaW5nU3lzdGVtLCBvdXRwdXRDYWxlbmRhciwgZGVmYXVsdFRvRU4pIHtcbiAgICAgIGlmIChkZWZhdWx0VG9FTiA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGRlZmF1bHRUb0VOID0gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHZhciBzcGVjaWZpZWRMb2NhbGUgPSBsb2NhbGUgfHwgU2V0dGluZ3MuZGVmYXVsdExvY2FsZSxcbiAgICAgICAgICAvLyB0aGUgc3lzdGVtIGxvY2FsZSBpcyB1c2VmdWwgZm9yIGh1bWFuIHJlYWRhYmxlIHN0cmluZ3MgYnV0IGFubm95aW5nIGZvciBwYXJzaW5nL2Zvcm1hdHRpbmcga25vd24gZm9ybWF0c1xuICAgICAgbG9jYWxlUiA9IHNwZWNpZmllZExvY2FsZSB8fCAoZGVmYXVsdFRvRU4gPyBcImVuLVVTXCIgOiBzeXN0ZW1Mb2NhbGUoKSksXG4gICAgICAgICAgbnVtYmVyaW5nU3lzdGVtUiA9IG51bWJlcmluZ1N5c3RlbSB8fCBTZXR0aW5ncy5kZWZhdWx0TnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIG91dHB1dENhbGVuZGFyUiA9IG91dHB1dENhbGVuZGFyIHx8IFNldHRpbmdzLmRlZmF1bHRPdXRwdXRDYWxlbmRhcjtcbiAgICAgIHJldHVybiBuZXcgTG9jYWxlKGxvY2FsZVIsIG51bWJlcmluZ1N5c3RlbVIsIG91dHB1dENhbGVuZGFyUiwgc3BlY2lmaWVkTG9jYWxlKTtcbiAgICB9O1xuXG4gICAgTG9jYWxlLnJlc2V0Q2FjaGUgPSBmdW5jdGlvbiByZXNldENhY2hlKCkge1xuICAgICAgc3lzTG9jYWxlQ2FjaGUgPSBudWxsO1xuICAgICAgaW50bERUQ2FjaGUgPSB7fTtcbiAgICAgIGludGxOdW1DYWNoZSA9IHt9O1xuICAgICAgaW50bFJlbENhY2hlID0ge307XG4gICAgfTtcblxuICAgIExvY2FsZS5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChfdGVtcCkge1xuICAgICAgdmFyIF9yZWYgPSBfdGVtcCA9PT0gdm9pZCAwID8ge30gOiBfdGVtcCxcbiAgICAgICAgICBsb2NhbGUgPSBfcmVmLmxvY2FsZSxcbiAgICAgICAgICBudW1iZXJpbmdTeXN0ZW0gPSBfcmVmLm51bWJlcmluZ1N5c3RlbSxcbiAgICAgICAgICBvdXRwdXRDYWxlbmRhciA9IF9yZWYub3V0cHV0Q2FsZW5kYXI7XG5cbiAgICAgIHJldHVybiBMb2NhbGUuY3JlYXRlKGxvY2FsZSwgbnVtYmVyaW5nU3lzdGVtLCBvdXRwdXRDYWxlbmRhcik7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIExvY2FsZShsb2NhbGUsIG51bWJlcmluZywgb3V0cHV0Q2FsZW5kYXIsIHNwZWNpZmllZExvY2FsZSkge1xuICAgICAgdmFyIF9wYXJzZUxvY2FsZVN0cmluZyA9IHBhcnNlTG9jYWxlU3RyaW5nKGxvY2FsZSksXG4gICAgICAgICAgcGFyc2VkTG9jYWxlID0gX3BhcnNlTG9jYWxlU3RyaW5nWzBdLFxuICAgICAgICAgIHBhcnNlZE51bWJlcmluZ1N5c3RlbSA9IF9wYXJzZUxvY2FsZVN0cmluZ1sxXSxcbiAgICAgICAgICBwYXJzZWRPdXRwdXRDYWxlbmRhciA9IF9wYXJzZUxvY2FsZVN0cmluZ1syXTtcblxuICAgICAgdGhpcy5sb2NhbGUgPSBwYXJzZWRMb2NhbGU7XG4gICAgICB0aGlzLm51bWJlcmluZ1N5c3RlbSA9IG51bWJlcmluZyB8fCBwYXJzZWROdW1iZXJpbmdTeXN0ZW0gfHwgbnVsbDtcbiAgICAgIHRoaXMub3V0cHV0Q2FsZW5kYXIgPSBvdXRwdXRDYWxlbmRhciB8fCBwYXJzZWRPdXRwdXRDYWxlbmRhciB8fCBudWxsO1xuICAgICAgdGhpcy5pbnRsID0gaW50bENvbmZpZ1N0cmluZyh0aGlzLmxvY2FsZSwgdGhpcy5udW1iZXJpbmdTeXN0ZW0sIHRoaXMub3V0cHV0Q2FsZW5kYXIpO1xuICAgICAgdGhpcy53ZWVrZGF5c0NhY2hlID0ge1xuICAgICAgICBmb3JtYXQ6IHt9LFxuICAgICAgICBzdGFuZGFsb25lOiB7fVxuICAgICAgfTtcbiAgICAgIHRoaXMubW9udGhzQ2FjaGUgPSB7XG4gICAgICAgIGZvcm1hdDoge30sXG4gICAgICAgIHN0YW5kYWxvbmU6IHt9XG4gICAgICB9O1xuICAgICAgdGhpcy5tZXJpZGllbUNhY2hlID0gbnVsbDtcbiAgICAgIHRoaXMuZXJhQ2FjaGUgPSB7fTtcbiAgICAgIHRoaXMuc3BlY2lmaWVkTG9jYWxlID0gc3BlY2lmaWVkTG9jYWxlO1xuICAgICAgdGhpcy5mYXN0TnVtYmVyc0NhY2hlZCA9IG51bGw7XG4gICAgfVxuXG4gICAgdmFyIF9wcm90bzQgPSBMb2NhbGUucHJvdG90eXBlO1xuXG4gICAgX3Byb3RvNC5saXN0aW5nTW9kZSA9IGZ1bmN0aW9uIGxpc3RpbmdNb2RlKGRlZmF1bHRPSykge1xuICAgICAgaWYgKGRlZmF1bHRPSyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGRlZmF1bHRPSyA9IHRydWU7XG4gICAgICB9XG5cbiAgICAgIHZhciBpbnRsID0gaGFzSW50bCgpLFxuICAgICAgICAgIGhhc0ZUUCA9IGludGwgJiYgaGFzRm9ybWF0VG9QYXJ0cygpLFxuICAgICAgICAgIGlzQWN0dWFsbHlFbiA9IHRoaXMuaXNFbmdsaXNoKCksXG4gICAgICAgICAgaGFzTm9XZWlyZG5lc3MgPSAodGhpcy5udW1iZXJpbmdTeXN0ZW0gPT09IG51bGwgfHwgdGhpcy5udW1iZXJpbmdTeXN0ZW0gPT09IFwibGF0blwiKSAmJiAodGhpcy5vdXRwdXRDYWxlbmRhciA9PT0gbnVsbCB8fCB0aGlzLm91dHB1dENhbGVuZGFyID09PSBcImdyZWdvcnlcIik7XG5cbiAgICAgIGlmICghaGFzRlRQICYmICEoaXNBY3R1YWxseUVuICYmIGhhc05vV2VpcmRuZXNzKSAmJiAhZGVmYXVsdE9LKSB7XG4gICAgICAgIHJldHVybiBcImVycm9yXCI7XG4gICAgICB9IGVsc2UgaWYgKCFoYXNGVFAgfHwgaXNBY3R1YWxseUVuICYmIGhhc05vV2VpcmRuZXNzKSB7XG4gICAgICAgIHJldHVybiBcImVuXCI7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gXCJpbnRsXCI7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF9wcm90bzQuY2xvbmUgPSBmdW5jdGlvbiBjbG9uZShhbHRzKSB7XG4gICAgICBpZiAoIWFsdHMgfHwgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoYWx0cykubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIExvY2FsZS5jcmVhdGUoYWx0cy5sb2NhbGUgfHwgdGhpcy5zcGVjaWZpZWRMb2NhbGUsIGFsdHMubnVtYmVyaW5nU3lzdGVtIHx8IHRoaXMubnVtYmVyaW5nU3lzdGVtLCBhbHRzLm91dHB1dENhbGVuZGFyIHx8IHRoaXMub3V0cHV0Q2FsZW5kYXIsIGFsdHMuZGVmYXVsdFRvRU4gfHwgZmFsc2UpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfcHJvdG80LnJlZGVmYXVsdFRvRU4gPSBmdW5jdGlvbiByZWRlZmF1bHRUb0VOKGFsdHMpIHtcbiAgICAgIGlmIChhbHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgYWx0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5jbG9uZShPYmplY3QuYXNzaWduKHt9LCBhbHRzLCB7XG4gICAgICAgIGRlZmF1bHRUb0VOOiB0cnVlXG4gICAgICB9KSk7XG4gICAgfTtcblxuICAgIF9wcm90bzQucmVkZWZhdWx0VG9TeXN0ZW0gPSBmdW5jdGlvbiByZWRlZmF1bHRUb1N5c3RlbShhbHRzKSB7XG4gICAgICBpZiAoYWx0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGFsdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuY2xvbmUoT2JqZWN0LmFzc2lnbih7fSwgYWx0cywge1xuICAgICAgICBkZWZhdWx0VG9FTjogZmFsc2VcbiAgICAgIH0pKTtcbiAgICB9O1xuXG4gICAgX3Byb3RvNC5tb250aHMgPSBmdW5jdGlvbiBtb250aHMkMShsZW5ndGgsIGZvcm1hdCwgZGVmYXVsdE9LKSB7XG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgICBpZiAoZm9ybWF0ID09PSB2b2lkIDApIHtcbiAgICAgICAgZm9ybWF0ID0gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGlmIChkZWZhdWx0T0sgPT09IHZvaWQgMCkge1xuICAgICAgICBkZWZhdWx0T0sgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbGlzdFN0dWZmKHRoaXMsIGxlbmd0aCwgZGVmYXVsdE9LLCBtb250aHMsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGludGwgPSBmb3JtYXQgPyB7XG4gICAgICAgICAgbW9udGg6IGxlbmd0aCxcbiAgICAgICAgICBkYXk6IFwibnVtZXJpY1wiXG4gICAgICAgIH0gOiB7XG4gICAgICAgICAgbW9udGg6IGxlbmd0aFxuICAgICAgICB9LFxuICAgICAgICAgICAgZm9ybWF0U3RyID0gZm9ybWF0ID8gXCJmb3JtYXRcIiA6IFwic3RhbmRhbG9uZVwiO1xuXG4gICAgICAgIGlmICghX3RoaXMubW9udGhzQ2FjaGVbZm9ybWF0U3RyXVtsZW5ndGhdKSB7XG4gICAgICAgICAgX3RoaXMubW9udGhzQ2FjaGVbZm9ybWF0U3RyXVtsZW5ndGhdID0gbWFwTW9udGhzKGZ1bmN0aW9uIChkdCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzLmV4dHJhY3QoZHQsIGludGwsIFwibW9udGhcIik7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gX3RoaXMubW9udGhzQ2FjaGVbZm9ybWF0U3RyXVtsZW5ndGhdO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF9wcm90bzQud2Vla2RheXMgPSBmdW5jdGlvbiB3ZWVrZGF5cyQxKGxlbmd0aCwgZm9ybWF0LCBkZWZhdWx0T0spIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICBpZiAoZm9ybWF0ID09PSB2b2lkIDApIHtcbiAgICAgICAgZm9ybWF0ID0gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGlmIChkZWZhdWx0T0sgPT09IHZvaWQgMCkge1xuICAgICAgICBkZWZhdWx0T0sgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbGlzdFN0dWZmKHRoaXMsIGxlbmd0aCwgZGVmYXVsdE9LLCB3ZWVrZGF5cywgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgaW50bCA9IGZvcm1hdCA/IHtcbiAgICAgICAgICB3ZWVrZGF5OiBsZW5ndGgsXG4gICAgICAgICAgeWVhcjogXCJudW1lcmljXCIsXG4gICAgICAgICAgbW9udGg6IFwibG9uZ1wiLFxuICAgICAgICAgIGRheTogXCJudW1lcmljXCJcbiAgICAgICAgfSA6IHtcbiAgICAgICAgICB3ZWVrZGF5OiBsZW5ndGhcbiAgICAgICAgfSxcbiAgICAgICAgICAgIGZvcm1hdFN0ciA9IGZvcm1hdCA/IFwiZm9ybWF0XCIgOiBcInN0YW5kYWxvbmVcIjtcblxuICAgICAgICBpZiAoIV90aGlzMi53ZWVrZGF5c0NhY2hlW2Zvcm1hdFN0cl1bbGVuZ3RoXSkge1xuICAgICAgICAgIF90aGlzMi53ZWVrZGF5c0NhY2hlW2Zvcm1hdFN0cl1bbGVuZ3RoXSA9IG1hcFdlZWtkYXlzKGZ1bmN0aW9uIChkdCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzMi5leHRyYWN0KGR0LCBpbnRsLCBcIndlZWtkYXlcIik7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gX3RoaXMyLndlZWtkYXlzQ2FjaGVbZm9ybWF0U3RyXVtsZW5ndGhdO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF9wcm90bzQubWVyaWRpZW1zID0gZnVuY3Rpb24gbWVyaWRpZW1zJDEoZGVmYXVsdE9LKSB7XG4gICAgICB2YXIgX3RoaXMzID0gdGhpcztcblxuICAgICAgaWYgKGRlZmF1bHRPSyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGRlZmF1bHRPSyA9IHRydWU7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBsaXN0U3R1ZmYodGhpcywgdW5kZWZpbmVkLCBkZWZhdWx0T0ssIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIG1lcmlkaWVtcztcbiAgICAgIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gSW4gdGhlb3J5IHRoZXJlIGNvdWxkIGJlIGFyaWJpdHJhcnkgZGF5IHBlcmlvZHMuIFdlJ3JlIGdvbm5hIGFzc3VtZSB0aGVyZSBhcmUgZXhhY3RseSB0d29cbiAgICAgICAgLy8gZm9yIEFNIGFuZCBQTS4gVGhpcyBpcyBwcm9iYWJseSB3cm9uZywgYnV0IGl0J3MgbWFrZXMgcGFyc2luZyB3YXkgZWFzaWVyLlxuICAgICAgICBpZiAoIV90aGlzMy5tZXJpZGllbUNhY2hlKSB7XG4gICAgICAgICAgdmFyIGludGwgPSB7XG4gICAgICAgICAgICBob3VyOiBcIm51bWVyaWNcIixcbiAgICAgICAgICAgIGhvdXIxMjogdHJ1ZVxuICAgICAgICAgIH07XG4gICAgICAgICAgX3RoaXMzLm1lcmlkaWVtQ2FjaGUgPSBbRGF0ZVRpbWUudXRjKDIwMTYsIDExLCAxMywgOSksIERhdGVUaW1lLnV0YygyMDE2LCAxMSwgMTMsIDE5KV0ubWFwKGZ1bmN0aW9uIChkdCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzMy5leHRyYWN0KGR0LCBpbnRsLCBcImRheXBlcmlvZFwiKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBfdGhpczMubWVyaWRpZW1DYWNoZTtcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfcHJvdG80LmVyYXMgPSBmdW5jdGlvbiBlcmFzJDEobGVuZ3RoLCBkZWZhdWx0T0spIHtcbiAgICAgIHZhciBfdGhpczQgPSB0aGlzO1xuXG4gICAgICBpZiAoZGVmYXVsdE9LID09PSB2b2lkIDApIHtcbiAgICAgICAgZGVmYXVsdE9LID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGxpc3RTdHVmZih0aGlzLCBsZW5ndGgsIGRlZmF1bHRPSywgZXJhcywgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgaW50bCA9IHtcbiAgICAgICAgICBlcmE6IGxlbmd0aFxuICAgICAgICB9OyAvLyBUaGlzIGlzIHV0dGVyIGJ1bGxzaGl0LiBEaWZmZXJlbnQgY2FsZW5kYXJzIGFyZSBnb2luZyB0byBkZWZpbmUgZXJhcyB0b3RhbGx5IGRpZmZlcmVudGx5LiBXaGF0IEkgbmVlZCBpcyB0aGUgbWluaW11bSBzZXQgb2YgZGF0ZXNcbiAgICAgICAgLy8gdG8gZGVmaW5pdGVseSBlbnVtZXJhdGUgdGhlbS5cblxuICAgICAgICBpZiAoIV90aGlzNC5lcmFDYWNoZVtsZW5ndGhdKSB7XG4gICAgICAgICAgX3RoaXM0LmVyYUNhY2hlW2xlbmd0aF0gPSBbRGF0ZVRpbWUudXRjKC00MCwgMSwgMSksIERhdGVUaW1lLnV0YygyMDE3LCAxLCAxKV0ubWFwKGZ1bmN0aW9uIChkdCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzNC5leHRyYWN0KGR0LCBpbnRsLCBcImVyYVwiKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBfdGhpczQuZXJhQ2FjaGVbbGVuZ3RoXTtcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfcHJvdG80LmV4dHJhY3QgPSBmdW5jdGlvbiBleHRyYWN0KGR0LCBpbnRsT3B0cywgZmllbGQpIHtcbiAgICAgIHZhciBkZiA9IHRoaXMuZHRGb3JtYXR0ZXIoZHQsIGludGxPcHRzKSxcbiAgICAgICAgICByZXN1bHRzID0gZGYuZm9ybWF0VG9QYXJ0cygpLFxuICAgICAgICAgIG1hdGNoaW5nID0gcmVzdWx0cy5maW5kKGZ1bmN0aW9uIChtKSB7XG4gICAgICAgIHJldHVybiBtLnR5cGUudG9Mb3dlckNhc2UoKSA9PT0gZmllbGQ7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBtYXRjaGluZyA/IG1hdGNoaW5nLnZhbHVlIDogbnVsbDtcbiAgICB9O1xuXG4gICAgX3Byb3RvNC5udW1iZXJGb3JtYXR0ZXIgPSBmdW5jdGlvbiBudW1iZXJGb3JtYXR0ZXIob3B0cykge1xuICAgICAgaWYgKG9wdHMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIC8vIHRoaXMgZm9yY2VzaW1wbGUgb3B0aW9uIGlzIG5ldmVyIHVzZWQgKHRoZSBvbmx5IGNhbGxlciBzaG9ydC1jaXJjdWl0cyBvbiBpdCwgYnV0IGl0IHNlZW1zIHNhZmVyIHRvIGxlYXZlKVxuICAgICAgLy8gKGluIGNvbnRyYXN0LCB0aGUgcmVzdCBvZiB0aGUgY29uZGl0aW9uIGlzIHVzZWQgaGVhdmlseSlcbiAgICAgIHJldHVybiBuZXcgUG9seU51bWJlckZvcm1hdHRlcih0aGlzLmludGwsIG9wdHMuZm9yY2VTaW1wbGUgfHwgdGhpcy5mYXN0TnVtYmVycywgb3B0cyk7XG4gICAgfTtcblxuICAgIF9wcm90bzQuZHRGb3JtYXR0ZXIgPSBmdW5jdGlvbiBkdEZvcm1hdHRlcihkdCwgaW50bE9wdHMpIHtcbiAgICAgIGlmIChpbnRsT3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGludGxPcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBuZXcgUG9seURhdGVGb3JtYXR0ZXIoZHQsIHRoaXMuaW50bCwgaW50bE9wdHMpO1xuICAgIH07XG5cbiAgICBfcHJvdG80LnJlbEZvcm1hdHRlciA9IGZ1bmN0aW9uIHJlbEZvcm1hdHRlcihvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ldyBQb2x5UmVsRm9ybWF0dGVyKHRoaXMuaW50bCwgdGhpcy5pc0VuZ2xpc2goKSwgb3B0cyk7XG4gICAgfTtcblxuICAgIF9wcm90bzQuaXNFbmdsaXNoID0gZnVuY3Rpb24gaXNFbmdsaXNoKCkge1xuICAgICAgcmV0dXJuIHRoaXMubG9jYWxlID09PSBcImVuXCIgfHwgdGhpcy5sb2NhbGUudG9Mb3dlckNhc2UoKSA9PT0gXCJlbi11c1wiIHx8IGhhc0ludGwoKSAmJiBuZXcgSW50bC5EYXRlVGltZUZvcm1hdCh0aGlzLmludGwpLnJlc29sdmVkT3B0aW9ucygpLmxvY2FsZS5zdGFydHNXaXRoKFwiZW4tdXNcIik7XG4gICAgfTtcblxuICAgIF9wcm90bzQuZXF1YWxzID0gZnVuY3Rpb24gZXF1YWxzKG90aGVyKSB7XG4gICAgICByZXR1cm4gdGhpcy5sb2NhbGUgPT09IG90aGVyLmxvY2FsZSAmJiB0aGlzLm51bWJlcmluZ1N5c3RlbSA9PT0gb3RoZXIubnVtYmVyaW5nU3lzdGVtICYmIHRoaXMub3V0cHV0Q2FsZW5kYXIgPT09IG90aGVyLm91dHB1dENhbGVuZGFyO1xuICAgIH07XG5cbiAgICBfY3JlYXRlQ2xhc3MoTG9jYWxlLCBbe1xuICAgICAga2V5OiBcImZhc3ROdW1iZXJzXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgaWYgKHRoaXMuZmFzdE51bWJlcnNDYWNoZWQgPT0gbnVsbCkge1xuICAgICAgICAgIHRoaXMuZmFzdE51bWJlcnNDYWNoZWQgPSBzdXBwb3J0c0Zhc3ROdW1iZXJzKHRoaXMpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuZmFzdE51bWJlcnNDYWNoZWQ7XG4gICAgICB9XG4gICAgfV0pO1xuXG4gICAgcmV0dXJuIExvY2FsZTtcbiAgfSgpO1xuXG4gIC8qXG4gICAqIFRoaXMgZmlsZSBoYW5kbGVzIHBhcnNpbmcgZm9yIHdlbGwtc3BlY2lmaWVkIGZvcm1hdHMuIEhlcmUncyBob3cgaXQgd29ya3M6XG4gICAqIFR3byB0aGluZ3MgZ28gaW50byBwYXJzaW5nOiBhIHJlZ2V4IHRvIG1hdGNoIHdpdGggYW5kIGFuIGV4dHJhY3RvciB0byB0YWtlIGFwYXJ0IHRoZSBncm91cHMgaW4gdGhlIG1hdGNoLlxuICAgKiBBbiBleHRyYWN0b3IgaXMganVzdCBhIGZ1bmN0aW9uIHRoYXQgdGFrZXMgYSByZWdleCBtYXRjaCBhcnJheSBhbmQgcmV0dXJucyBhIHsgeWVhcjogLi4uLCBtb250aDogLi4uIH0gb2JqZWN0XG4gICAqIHBhcnNlKCkgZG9lcyB0aGUgd29yayBvZiBleGVjdXRpbmcgdGhlIHJlZ2V4IGFuZCBhcHBseWluZyB0aGUgZXh0cmFjdG9yLiBJdCB0YWtlcyBtdWx0aXBsZSByZWdleC9leHRyYWN0b3IgcGFpcnMgdG8gdHJ5IGluIHNlcXVlbmNlLlxuICAgKiBFeHRyYWN0b3JzIGNhbiB0YWtlIGEgXCJjdXJzb3JcIiByZXByZXNlbnRpbmcgdGhlIG9mZnNldCBpbiB0aGUgbWF0Y2ggdG8gbG9vayBhdC4gVGhpcyBtYWtlcyBpdCBlYXN5IHRvIGNvbWJpbmUgZXh0cmFjdG9ycy5cbiAgICogY29tYmluZUV4dHJhY3RvcnMoKSBkb2VzIHRoZSB3b3JrIG9mIGNvbWJpbmluZyB0aGVtLCBrZWVwaW5nIHRyYWNrIG9mIHRoZSBjdXJzb3IgdGhyb3VnaCBtdWx0aXBsZSBleHRyYWN0aW9ucy5cbiAgICogU29tZSBleHRyYWN0aW9ucyBhcmUgc3VwZXIgZHVtYiBhbmQgc2ltcGxlUGFyc2UgYW5kIGZyb21TdHJpbmdzIGhlbHAgRFJZIHRoZW0uXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGNvbWJpbmVSZWdleGVzKCkge1xuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCByZWdleGVzID0gbmV3IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgcmVnZXhlc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICB2YXIgZnVsbCA9IHJlZ2V4ZXMucmVkdWNlKGZ1bmN0aW9uIChmLCByKSB7XG4gICAgICByZXR1cm4gZiArIHIuc291cmNlO1xuICAgIH0sIFwiXCIpO1xuICAgIHJldHVybiBSZWdFeHAoXCJeXCIgKyBmdWxsICsgXCIkXCIpO1xuICB9XG5cbiAgZnVuY3Rpb24gY29tYmluZUV4dHJhY3RvcnMoKSB7XG4gICAgZm9yICh2YXIgX2xlbjIgPSBhcmd1bWVudHMubGVuZ3RoLCBleHRyYWN0b3JzID0gbmV3IEFycmF5KF9sZW4yKSwgX2tleTIgPSAwOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICBleHRyYWN0b3JzW19rZXkyXSA9IGFyZ3VtZW50c1tfa2V5Ml07XG4gICAgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChtKSB7XG4gICAgICByZXR1cm4gZXh0cmFjdG9ycy5yZWR1Y2UoZnVuY3Rpb24gKF9yZWYsIGV4KSB7XG4gICAgICAgIHZhciBtZXJnZWRWYWxzID0gX3JlZlswXSxcbiAgICAgICAgICAgIG1lcmdlZFpvbmUgPSBfcmVmWzFdLFxuICAgICAgICAgICAgY3Vyc29yID0gX3JlZlsyXTtcblxuICAgICAgICB2YXIgX2V4ID0gZXgobSwgY3Vyc29yKSxcbiAgICAgICAgICAgIHZhbCA9IF9leFswXSxcbiAgICAgICAgICAgIHpvbmUgPSBfZXhbMV0sXG4gICAgICAgICAgICBuZXh0ID0gX2V4WzJdO1xuXG4gICAgICAgIHJldHVybiBbT2JqZWN0LmFzc2lnbihtZXJnZWRWYWxzLCB2YWwpLCBtZXJnZWRab25lIHx8IHpvbmUsIG5leHRdO1xuICAgICAgfSwgW3t9LCBudWxsLCAxXSkuc2xpY2UoMCwgMik7XG4gICAgfTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHBhcnNlKHMpIHtcbiAgICBpZiAocyA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gW251bGwsIG51bGxdO1xuICAgIH1cblxuICAgIGZvciAodmFyIF9sZW4zID0gYXJndW1lbnRzLmxlbmd0aCwgcGF0dGVybnMgPSBuZXcgQXJyYXkoX2xlbjMgPiAxID8gX2xlbjMgLSAxIDogMCksIF9rZXkzID0gMTsgX2tleTMgPCBfbGVuMzsgX2tleTMrKykge1xuICAgICAgcGF0dGVybnNbX2tleTMgLSAxXSA9IGFyZ3VtZW50c1tfa2V5M107XG4gICAgfVxuXG4gICAgZm9yICh2YXIgX2kgPSAwLCBfcGF0dGVybnMgPSBwYXR0ZXJuczsgX2kgPCBfcGF0dGVybnMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgX3BhdHRlcm5zJF9pID0gX3BhdHRlcm5zW19pXSxcbiAgICAgICAgICByZWdleCA9IF9wYXR0ZXJucyRfaVswXSxcbiAgICAgICAgICBleHRyYWN0b3IgPSBfcGF0dGVybnMkX2lbMV07XG4gICAgICB2YXIgbSA9IHJlZ2V4LmV4ZWMocyk7XG5cbiAgICAgIGlmIChtKSB7XG4gICAgICAgIHJldHVybiBleHRyYWN0b3IobSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIFtudWxsLCBudWxsXTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHNpbXBsZVBhcnNlKCkge1xuICAgIGZvciAodmFyIF9sZW40ID0gYXJndW1lbnRzLmxlbmd0aCwga2V5cyA9IG5ldyBBcnJheShfbGVuNCksIF9rZXk0ID0gMDsgX2tleTQgPCBfbGVuNDsgX2tleTQrKykge1xuICAgICAga2V5c1tfa2V5NF0gPSBhcmd1bWVudHNbX2tleTRdO1xuICAgIH1cblxuICAgIHJldHVybiBmdW5jdGlvbiAobWF0Y2gsIGN1cnNvcikge1xuICAgICAgdmFyIHJldCA9IHt9O1xuICAgICAgdmFyIGk7XG5cbiAgICAgIGZvciAoaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHJldFtrZXlzW2ldXSA9IHBhcnNlSW50ZWdlcihtYXRjaFtjdXJzb3IgKyBpXSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBbcmV0LCBudWxsLCBjdXJzb3IgKyBpXTtcbiAgICB9O1xuICB9IC8vIElTTyBhbmQgU1FMIHBhcnNpbmdcblxuXG4gIHZhciBvZmZzZXRSZWdleCA9IC8oPzooWil8KFsrLV1cXGRcXGQpKD86Oj8oXFxkXFxkKSk/KS8sXG4gICAgICBpc29UaW1lQmFzZVJlZ2V4ID0gLyhcXGRcXGQpKD86Oj8oXFxkXFxkKSg/Ojo/KFxcZFxcZCkoPzpbLixdKFxcZHsxLDl9KSk/KT8pPy8sXG4gICAgICBpc29UaW1lUmVnZXggPSBSZWdFeHAoXCJcIiArIGlzb1RpbWVCYXNlUmVnZXguc291cmNlICsgb2Zmc2V0UmVnZXguc291cmNlICsgXCI/XCIpLFxuICAgICAgaXNvVGltZUV4dGVuc2lvblJlZ2V4ID0gUmVnRXhwKFwiKD86VFwiICsgaXNvVGltZVJlZ2V4LnNvdXJjZSArIFwiKT9cIiksXG4gICAgICBpc29ZbWRSZWdleCA9IC8oWystXVxcZHs2fXxcXGR7NH0pKD86LT8oXFxkXFxkKSg/Oi0/KFxcZFxcZCkpPyk/LyxcbiAgICAgIGlzb1dlZWtSZWdleCA9IC8oXFxkezR9KS0/VyhcXGRcXGQpKD86LT8oXFxkKSk/LyxcbiAgICAgIGlzb09yZGluYWxSZWdleCA9IC8oXFxkezR9KS0/KFxcZHszfSkvLFxuICAgICAgZXh0cmFjdElTT1dlZWtEYXRhID0gc2ltcGxlUGFyc2UoXCJ3ZWVrWWVhclwiLCBcIndlZWtOdW1iZXJcIiwgXCJ3ZWVrRGF5XCIpLFxuICAgICAgZXh0cmFjdElTT09yZGluYWxEYXRhID0gc2ltcGxlUGFyc2UoXCJ5ZWFyXCIsIFwib3JkaW5hbFwiKSxcbiAgICAgIHNxbFltZFJlZ2V4ID0gLyhcXGR7NH0pLShcXGRcXGQpLShcXGRcXGQpLyxcbiAgICAgIC8vIGR1bWJlZC1kb3duIHZlcnNpb24gb2YgdGhlIElTTyBvbmVcbiAgc3FsVGltZVJlZ2V4ID0gUmVnRXhwKGlzb1RpbWVCYXNlUmVnZXguc291cmNlICsgXCIgPyg/OlwiICsgb2Zmc2V0UmVnZXguc291cmNlICsgXCJ8KFwiICsgaWFuYVJlZ2V4LnNvdXJjZSArIFwiKSk/XCIpLFxuICAgICAgc3FsVGltZUV4dGVuc2lvblJlZ2V4ID0gUmVnRXhwKFwiKD86IFwiICsgc3FsVGltZVJlZ2V4LnNvdXJjZSArIFwiKT9cIik7XG5cbiAgZnVuY3Rpb24gaW50KG1hdGNoLCBwb3MsIGZhbGxiYWNrKSB7XG4gICAgdmFyIG0gPSBtYXRjaFtwb3NdO1xuICAgIHJldHVybiBpc1VuZGVmaW5lZChtKSA/IGZhbGxiYWNrIDogcGFyc2VJbnRlZ2VyKG0pO1xuICB9XG5cbiAgZnVuY3Rpb24gZXh0cmFjdElTT1ltZChtYXRjaCwgY3Vyc29yKSB7XG4gICAgdmFyIGl0ZW0gPSB7XG4gICAgICB5ZWFyOiBpbnQobWF0Y2gsIGN1cnNvciksXG4gICAgICBtb250aDogaW50KG1hdGNoLCBjdXJzb3IgKyAxLCAxKSxcbiAgICAgIGRheTogaW50KG1hdGNoLCBjdXJzb3IgKyAyLCAxKVxuICAgIH07XG4gICAgcmV0dXJuIFtpdGVtLCBudWxsLCBjdXJzb3IgKyAzXTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGV4dHJhY3RJU09UaW1lKG1hdGNoLCBjdXJzb3IpIHtcbiAgICB2YXIgaXRlbSA9IHtcbiAgICAgIGhvdXI6IGludChtYXRjaCwgY3Vyc29yLCAwKSxcbiAgICAgIG1pbnV0ZTogaW50KG1hdGNoLCBjdXJzb3IgKyAxLCAwKSxcbiAgICAgIHNlY29uZDogaW50KG1hdGNoLCBjdXJzb3IgKyAyLCAwKSxcbiAgICAgIG1pbGxpc2Vjb25kOiBwYXJzZU1pbGxpcyhtYXRjaFtjdXJzb3IgKyAzXSlcbiAgICB9O1xuICAgIHJldHVybiBbaXRlbSwgbnVsbCwgY3Vyc29yICsgNF07XG4gIH1cblxuICBmdW5jdGlvbiBleHRyYWN0SVNPT2Zmc2V0KG1hdGNoLCBjdXJzb3IpIHtcbiAgICB2YXIgbG9jYWwgPSAhbWF0Y2hbY3Vyc29yXSAmJiAhbWF0Y2hbY3Vyc29yICsgMV0sXG4gICAgICAgIGZ1bGxPZmZzZXQgPSBzaWduZWRPZmZzZXQobWF0Y2hbY3Vyc29yICsgMV0sIG1hdGNoW2N1cnNvciArIDJdKSxcbiAgICAgICAgem9uZSA9IGxvY2FsID8gbnVsbCA6IEZpeGVkT2Zmc2V0Wm9uZS5pbnN0YW5jZShmdWxsT2Zmc2V0KTtcbiAgICByZXR1cm4gW3t9LCB6b25lLCBjdXJzb3IgKyAzXTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGV4dHJhY3RJQU5BWm9uZShtYXRjaCwgY3Vyc29yKSB7XG4gICAgdmFyIHpvbmUgPSBtYXRjaFtjdXJzb3JdID8gSUFOQVpvbmUuY3JlYXRlKG1hdGNoW2N1cnNvcl0pIDogbnVsbDtcbiAgICByZXR1cm4gW3t9LCB6b25lLCBjdXJzb3IgKyAxXTtcbiAgfSAvLyBJU08gZHVyYXRpb24gcGFyc2luZ1xuXG5cbiAgdmFyIGlzb0R1cmF0aW9uID0gL15QKD86KD86KC0/XFxkezEsOX0pWSk/KD86KC0/XFxkezEsOX0pTSk/KD86KC0/XFxkezEsOX0pVyk/KD86KC0/XFxkezEsOX0pRCk/KD86VCg/OigtP1xcZHsxLDl9KUgpPyg/OigtP1xcZHsxLDl9KU0pPyg/OigtP1xcZHsxLDl9KSg/OlsuLF0oLT9cXGR7MSw5fSkpP1MpPyk/KSQvO1xuXG4gIGZ1bmN0aW9uIGV4dHJhY3RJU09EdXJhdGlvbihtYXRjaCkge1xuICAgIHZhciB5ZWFyU3RyID0gbWF0Y2hbMV0sXG4gICAgICAgIG1vbnRoU3RyID0gbWF0Y2hbMl0sXG4gICAgICAgIHdlZWtTdHIgPSBtYXRjaFszXSxcbiAgICAgICAgZGF5U3RyID0gbWF0Y2hbNF0sXG4gICAgICAgIGhvdXJTdHIgPSBtYXRjaFs1XSxcbiAgICAgICAgbWludXRlU3RyID0gbWF0Y2hbNl0sXG4gICAgICAgIHNlY29uZFN0ciA9IG1hdGNoWzddLFxuICAgICAgICBtaWxsaXNlY29uZHNTdHIgPSBtYXRjaFs4XTtcbiAgICByZXR1cm4gW3tcbiAgICAgIHllYXJzOiBwYXJzZUludGVnZXIoeWVhclN0ciksXG4gICAgICBtb250aHM6IHBhcnNlSW50ZWdlcihtb250aFN0ciksXG4gICAgICB3ZWVrczogcGFyc2VJbnRlZ2VyKHdlZWtTdHIpLFxuICAgICAgZGF5czogcGFyc2VJbnRlZ2VyKGRheVN0ciksXG4gICAgICBob3VyczogcGFyc2VJbnRlZ2VyKGhvdXJTdHIpLFxuICAgICAgbWludXRlczogcGFyc2VJbnRlZ2VyKG1pbnV0ZVN0ciksXG4gICAgICBzZWNvbmRzOiBwYXJzZUludGVnZXIoc2Vjb25kU3RyKSxcbiAgICAgIG1pbGxpc2Vjb25kczogcGFyc2VNaWxsaXMobWlsbGlzZWNvbmRzU3RyKVxuICAgIH1dO1xuICB9IC8vIFRoZXNlIGFyZSBhIGxpdHRsZSBicmFpbmRlYWQuIEVEVCAqc2hvdWxkKiB0ZWxsIHVzIHRoYXQgd2UncmUgaW4sIHNheSwgQW1lcmljYS9OZXdfWW9ya1xuICAvLyBhbmQgbm90IGp1c3QgdGhhdCB3ZSdyZSBpbiAtMjQwICpyaWdodCBub3cqLiBCdXQgc2luY2UgSSBkb24ndCB0aGluayB0aGVzZSBhcmUgdXNlZCB0aGF0IG9mdGVuXG4gIC8vIEknbSBqdXN0IGdvaW5nIHRvIGlnbm9yZSB0aGF0XG5cblxuICB2YXIgb2JzT2Zmc2V0cyA9IHtcbiAgICBHTVQ6IDAsXG4gICAgRURUOiAtNCAqIDYwLFxuICAgIEVTVDogLTUgKiA2MCxcbiAgICBDRFQ6IC01ICogNjAsXG4gICAgQ1NUOiAtNiAqIDYwLFxuICAgIE1EVDogLTYgKiA2MCxcbiAgICBNU1Q6IC03ICogNjAsXG4gICAgUERUOiAtNyAqIDYwLFxuICAgIFBTVDogLTggKiA2MFxuICB9O1xuXG4gIGZ1bmN0aW9uIGZyb21TdHJpbmdzKHdlZWtkYXlTdHIsIHllYXJTdHIsIG1vbnRoU3RyLCBkYXlTdHIsIGhvdXJTdHIsIG1pbnV0ZVN0ciwgc2Vjb25kU3RyKSB7XG4gICAgdmFyIHJlc3VsdCA9IHtcbiAgICAgIHllYXI6IHllYXJTdHIubGVuZ3RoID09PSAyID8gdW50cnVuY2F0ZVllYXIocGFyc2VJbnRlZ2VyKHllYXJTdHIpKSA6IHBhcnNlSW50ZWdlcih5ZWFyU3RyKSxcbiAgICAgIG1vbnRoOiBtb250aHNTaG9ydC5pbmRleE9mKG1vbnRoU3RyKSArIDEsXG4gICAgICBkYXk6IHBhcnNlSW50ZWdlcihkYXlTdHIpLFxuICAgICAgaG91cjogcGFyc2VJbnRlZ2VyKGhvdXJTdHIpLFxuICAgICAgbWludXRlOiBwYXJzZUludGVnZXIobWludXRlU3RyKVxuICAgIH07XG4gICAgaWYgKHNlY29uZFN0cikgcmVzdWx0LnNlY29uZCA9IHBhcnNlSW50ZWdlcihzZWNvbmRTdHIpO1xuXG4gICAgaWYgKHdlZWtkYXlTdHIpIHtcbiAgICAgIHJlc3VsdC53ZWVrZGF5ID0gd2Vla2RheVN0ci5sZW5ndGggPiAzID8gd2Vla2RheXNMb25nLmluZGV4T2Yod2Vla2RheVN0cikgKyAxIDogd2Vla2RheXNTaG9ydC5pbmRleE9mKHdlZWtkYXlTdHIpICsgMTtcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9IC8vIFJGQyAyODIyLzUzMjJcblxuXG4gIHZhciByZmMyODIyID0gL14oPzooTW9ufFR1ZXxXZWR8VGh1fEZyaXxTYXR8U3VuKSxcXHMpPyhcXGR7MSwyfSlcXHMoSmFufEZlYnxNYXJ8QXByfE1heXxKdW58SnVsfEF1Z3xTZXB8T2N0fE5vdnxEZWMpXFxzKFxcZHsyLDR9KVxccyhcXGRcXGQpOihcXGRcXGQpKD86OihcXGRcXGQpKT9cXHMoPzooVVR8R01UfFtFQ01QXVtTRF1UKXwoW1p6XSl8KD86KFsrLV1cXGRcXGQpKFxcZFxcZCkpKSQvO1xuXG4gIGZ1bmN0aW9uIGV4dHJhY3RSRkMyODIyKG1hdGNoKSB7XG4gICAgdmFyIHdlZWtkYXlTdHIgPSBtYXRjaFsxXSxcbiAgICAgICAgZGF5U3RyID0gbWF0Y2hbMl0sXG4gICAgICAgIG1vbnRoU3RyID0gbWF0Y2hbM10sXG4gICAgICAgIHllYXJTdHIgPSBtYXRjaFs0XSxcbiAgICAgICAgaG91clN0ciA9IG1hdGNoWzVdLFxuICAgICAgICBtaW51dGVTdHIgPSBtYXRjaFs2XSxcbiAgICAgICAgc2Vjb25kU3RyID0gbWF0Y2hbN10sXG4gICAgICAgIG9ic09mZnNldCA9IG1hdGNoWzhdLFxuICAgICAgICBtaWxPZmZzZXQgPSBtYXRjaFs5XSxcbiAgICAgICAgb2ZmSG91clN0ciA9IG1hdGNoWzEwXSxcbiAgICAgICAgb2ZmTWludXRlU3RyID0gbWF0Y2hbMTFdLFxuICAgICAgICByZXN1bHQgPSBmcm9tU3RyaW5ncyh3ZWVrZGF5U3RyLCB5ZWFyU3RyLCBtb250aFN0ciwgZGF5U3RyLCBob3VyU3RyLCBtaW51dGVTdHIsIHNlY29uZFN0cik7XG4gICAgdmFyIG9mZnNldDtcblxuICAgIGlmIChvYnNPZmZzZXQpIHtcbiAgICAgIG9mZnNldCA9IG9ic09mZnNldHNbb2JzT2Zmc2V0XTtcbiAgICB9IGVsc2UgaWYgKG1pbE9mZnNldCkge1xuICAgICAgb2Zmc2V0ID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgb2Zmc2V0ID0gc2lnbmVkT2Zmc2V0KG9mZkhvdXJTdHIsIG9mZk1pbnV0ZVN0cik7XG4gICAgfVxuXG4gICAgcmV0dXJuIFtyZXN1bHQsIG5ldyBGaXhlZE9mZnNldFpvbmUob2Zmc2V0KV07XG4gIH1cblxuICBmdW5jdGlvbiBwcmVwcm9jZXNzUkZDMjgyMihzKSB7XG4gICAgLy8gUmVtb3ZlIGNvbW1lbnRzIGFuZCBmb2xkaW5nIHdoaXRlc3BhY2UgYW5kIHJlcGxhY2UgbXVsdGlwbGUtc3BhY2VzIHdpdGggYSBzaW5nbGUgc3BhY2VcbiAgICByZXR1cm4gcy5yZXBsYWNlKC9cXChbXildKlxcKXxbXFxuXFx0XS9nLCBcIiBcIikucmVwbGFjZSgvKFxcc1xccyspL2csIFwiIFwiKS50cmltKCk7XG4gIH0gLy8gaHR0cCBkYXRlXG5cblxuICB2YXIgcmZjMTEyMyA9IC9eKE1vbnxUdWV8V2VkfFRodXxGcml8U2F0fFN1biksIChcXGRcXGQpIChKYW58RmVifE1hcnxBcHJ8TWF5fEp1bnxKdWx8QXVnfFNlcHxPY3R8Tm92fERlYykgKFxcZHs0fSkgKFxcZFxcZCk6KFxcZFxcZCk6KFxcZFxcZCkgR01UJC8sXG4gICAgICByZmM4NTAgPSAvXihNb25kYXl8VHVlc2RheXxXZWRzZGF5fFRodXJzZGF5fEZyaWRheXxTYXR1cmRheXxTdW5kYXkpLCAoXFxkXFxkKS0oSmFufEZlYnxNYXJ8QXByfE1heXxKdW58SnVsfEF1Z3xTZXB8T2N0fE5vdnxEZWMpLShcXGRcXGQpIChcXGRcXGQpOihcXGRcXGQpOihcXGRcXGQpIEdNVCQvLFxuICAgICAgYXNjaWkgPSAvXihNb258VHVlfFdlZHxUaHV8RnJpfFNhdHxTdW4pIChKYW58RmVifE1hcnxBcHJ8TWF5fEp1bnxKdWx8QXVnfFNlcHxPY3R8Tm92fERlYykgKCBcXGR8XFxkXFxkKSAoXFxkXFxkKTooXFxkXFxkKTooXFxkXFxkKSAoXFxkezR9KSQvO1xuXG4gIGZ1bmN0aW9uIGV4dHJhY3RSRkMxMTIzT3I4NTAobWF0Y2gpIHtcbiAgICB2YXIgd2Vla2RheVN0ciA9IG1hdGNoWzFdLFxuICAgICAgICBkYXlTdHIgPSBtYXRjaFsyXSxcbiAgICAgICAgbW9udGhTdHIgPSBtYXRjaFszXSxcbiAgICAgICAgeWVhclN0ciA9IG1hdGNoWzRdLFxuICAgICAgICBob3VyU3RyID0gbWF0Y2hbNV0sXG4gICAgICAgIG1pbnV0ZVN0ciA9IG1hdGNoWzZdLFxuICAgICAgICBzZWNvbmRTdHIgPSBtYXRjaFs3XSxcbiAgICAgICAgcmVzdWx0ID0gZnJvbVN0cmluZ3Mod2Vla2RheVN0ciwgeWVhclN0ciwgbW9udGhTdHIsIGRheVN0ciwgaG91clN0ciwgbWludXRlU3RyLCBzZWNvbmRTdHIpO1xuICAgIHJldHVybiBbcmVzdWx0LCBGaXhlZE9mZnNldFpvbmUudXRjSW5zdGFuY2VdO1xuICB9XG5cbiAgZnVuY3Rpb24gZXh0cmFjdEFTQ0lJKG1hdGNoKSB7XG4gICAgdmFyIHdlZWtkYXlTdHIgPSBtYXRjaFsxXSxcbiAgICAgICAgbW9udGhTdHIgPSBtYXRjaFsyXSxcbiAgICAgICAgZGF5U3RyID0gbWF0Y2hbM10sXG4gICAgICAgIGhvdXJTdHIgPSBtYXRjaFs0XSxcbiAgICAgICAgbWludXRlU3RyID0gbWF0Y2hbNV0sXG4gICAgICAgIHNlY29uZFN0ciA9IG1hdGNoWzZdLFxuICAgICAgICB5ZWFyU3RyID0gbWF0Y2hbN10sXG4gICAgICAgIHJlc3VsdCA9IGZyb21TdHJpbmdzKHdlZWtkYXlTdHIsIHllYXJTdHIsIG1vbnRoU3RyLCBkYXlTdHIsIGhvdXJTdHIsIG1pbnV0ZVN0ciwgc2Vjb25kU3RyKTtcbiAgICByZXR1cm4gW3Jlc3VsdCwgRml4ZWRPZmZzZXRab25lLnV0Y0luc3RhbmNlXTtcbiAgfVxuXG4gIHZhciBpc29ZbWRXaXRoVGltZUV4dGVuc2lvblJlZ2V4ID0gY29tYmluZVJlZ2V4ZXMoaXNvWW1kUmVnZXgsIGlzb1RpbWVFeHRlbnNpb25SZWdleCk7XG4gIHZhciBpc29XZWVrV2l0aFRpbWVFeHRlbnNpb25SZWdleCA9IGNvbWJpbmVSZWdleGVzKGlzb1dlZWtSZWdleCwgaXNvVGltZUV4dGVuc2lvblJlZ2V4KTtcbiAgdmFyIGlzb09yZGluYWxXaXRoVGltZUV4dGVuc2lvblJlZ2V4ID0gY29tYmluZVJlZ2V4ZXMoaXNvT3JkaW5hbFJlZ2V4LCBpc29UaW1lRXh0ZW5zaW9uUmVnZXgpO1xuICB2YXIgaXNvVGltZUNvbWJpbmVkUmVnZXggPSBjb21iaW5lUmVnZXhlcyhpc29UaW1lUmVnZXgpO1xuICB2YXIgZXh0cmFjdElTT1ltZFRpbWVBbmRPZmZzZXQgPSBjb21iaW5lRXh0cmFjdG9ycyhleHRyYWN0SVNPWW1kLCBleHRyYWN0SVNPVGltZSwgZXh0cmFjdElTT09mZnNldCk7XG4gIHZhciBleHRyYWN0SVNPV2Vla1RpbWVBbmRPZmZzZXQgPSBjb21iaW5lRXh0cmFjdG9ycyhleHRyYWN0SVNPV2Vla0RhdGEsIGV4dHJhY3RJU09UaW1lLCBleHRyYWN0SVNPT2Zmc2V0KTtcbiAgdmFyIGV4dHJhY3RJU09PcmRpbmFsRGF0YUFuZFRpbWUgPSBjb21iaW5lRXh0cmFjdG9ycyhleHRyYWN0SVNPT3JkaW5hbERhdGEsIGV4dHJhY3RJU09UaW1lKTtcbiAgdmFyIGV4dHJhY3RJU09UaW1lQW5kT2Zmc2V0ID0gY29tYmluZUV4dHJhY3RvcnMoZXh0cmFjdElTT1RpbWUsIGV4dHJhY3RJU09PZmZzZXQpO1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgZnVuY3Rpb24gcGFyc2VJU09EYXRlKHMpIHtcbiAgICByZXR1cm4gcGFyc2UocywgW2lzb1ltZFdpdGhUaW1lRXh0ZW5zaW9uUmVnZXgsIGV4dHJhY3RJU09ZbWRUaW1lQW5kT2Zmc2V0XSwgW2lzb1dlZWtXaXRoVGltZUV4dGVuc2lvblJlZ2V4LCBleHRyYWN0SVNPV2Vla1RpbWVBbmRPZmZzZXRdLCBbaXNvT3JkaW5hbFdpdGhUaW1lRXh0ZW5zaW9uUmVnZXgsIGV4dHJhY3RJU09PcmRpbmFsRGF0YUFuZFRpbWVdLCBbaXNvVGltZUNvbWJpbmVkUmVnZXgsIGV4dHJhY3RJU09UaW1lQW5kT2Zmc2V0XSk7XG4gIH1cbiAgZnVuY3Rpb24gcGFyc2VSRkMyODIyRGF0ZShzKSB7XG4gICAgcmV0dXJuIHBhcnNlKHByZXByb2Nlc3NSRkMyODIyKHMpLCBbcmZjMjgyMiwgZXh0cmFjdFJGQzI4MjJdKTtcbiAgfVxuICBmdW5jdGlvbiBwYXJzZUhUVFBEYXRlKHMpIHtcbiAgICByZXR1cm4gcGFyc2UocywgW3JmYzExMjMsIGV4dHJhY3RSRkMxMTIzT3I4NTBdLCBbcmZjODUwLCBleHRyYWN0UkZDMTEyM09yODUwXSwgW2FzY2lpLCBleHRyYWN0QVNDSUldKTtcbiAgfVxuICBmdW5jdGlvbiBwYXJzZUlTT0R1cmF0aW9uKHMpIHtcbiAgICByZXR1cm4gcGFyc2UocywgW2lzb0R1cmF0aW9uLCBleHRyYWN0SVNPRHVyYXRpb25dKTtcbiAgfVxuICB2YXIgc3FsWW1kV2l0aFRpbWVFeHRlbnNpb25SZWdleCA9IGNvbWJpbmVSZWdleGVzKHNxbFltZFJlZ2V4LCBzcWxUaW1lRXh0ZW5zaW9uUmVnZXgpO1xuICB2YXIgc3FsVGltZUNvbWJpbmVkUmVnZXggPSBjb21iaW5lUmVnZXhlcyhzcWxUaW1lUmVnZXgpO1xuICB2YXIgZXh0cmFjdElTT1ltZFRpbWVPZmZzZXRBbmRJQU5BWm9uZSA9IGNvbWJpbmVFeHRyYWN0b3JzKGV4dHJhY3RJU09ZbWQsIGV4dHJhY3RJU09UaW1lLCBleHRyYWN0SVNPT2Zmc2V0LCBleHRyYWN0SUFOQVpvbmUpO1xuICB2YXIgZXh0cmFjdElTT1RpbWVPZmZzZXRBbmRJQU5BWm9uZSA9IGNvbWJpbmVFeHRyYWN0b3JzKGV4dHJhY3RJU09UaW1lLCBleHRyYWN0SVNPT2Zmc2V0LCBleHRyYWN0SUFOQVpvbmUpO1xuICBmdW5jdGlvbiBwYXJzZVNRTChzKSB7XG4gICAgcmV0dXJuIHBhcnNlKHMsIFtzcWxZbWRXaXRoVGltZUV4dGVuc2lvblJlZ2V4LCBleHRyYWN0SVNPWW1kVGltZU9mZnNldEFuZElBTkFab25lXSwgW3NxbFRpbWVDb21iaW5lZFJlZ2V4LCBleHRyYWN0SVNPVGltZU9mZnNldEFuZElBTkFab25lXSk7XG4gIH1cblxuICB2YXIgSU5WQUxJRCA9IFwiSW52YWxpZCBEdXJhdGlvblwiOyAvLyB1bml0IGNvbnZlcnNpb24gY29uc3RhbnRzXG5cbiAgdmFyIGxvd09yZGVyTWF0cml4ID0ge1xuICAgIHdlZWtzOiB7XG4gICAgICBkYXlzOiA3LFxuICAgICAgaG91cnM6IDcgKiAyNCxcbiAgICAgIG1pbnV0ZXM6IDcgKiAyNCAqIDYwLFxuICAgICAgc2Vjb25kczogNyAqIDI0ICogNjAgKiA2MCxcbiAgICAgIG1pbGxpc2Vjb25kczogNyAqIDI0ICogNjAgKiA2MCAqIDEwMDBcbiAgICB9LFxuICAgIGRheXM6IHtcbiAgICAgIGhvdXJzOiAyNCxcbiAgICAgIG1pbnV0ZXM6IDI0ICogNjAsXG4gICAgICBzZWNvbmRzOiAyNCAqIDYwICogNjAsXG4gICAgICBtaWxsaXNlY29uZHM6IDI0ICogNjAgKiA2MCAqIDEwMDBcbiAgICB9LFxuICAgIGhvdXJzOiB7XG4gICAgICBtaW51dGVzOiA2MCxcbiAgICAgIHNlY29uZHM6IDYwICogNjAsXG4gICAgICBtaWxsaXNlY29uZHM6IDYwICogNjAgKiAxMDAwXG4gICAgfSxcbiAgICBtaW51dGVzOiB7XG4gICAgICBzZWNvbmRzOiA2MCxcbiAgICAgIG1pbGxpc2Vjb25kczogNjAgKiAxMDAwXG4gICAgfSxcbiAgICBzZWNvbmRzOiB7XG4gICAgICBtaWxsaXNlY29uZHM6IDEwMDBcbiAgICB9XG4gIH0sXG4gICAgICBjYXN1YWxNYXRyaXggPSBPYmplY3QuYXNzaWduKHtcbiAgICB5ZWFyczoge1xuICAgICAgbW9udGhzOiAxMixcbiAgICAgIHdlZWtzOiA1MixcbiAgICAgIGRheXM6IDM2NSxcbiAgICAgIGhvdXJzOiAzNjUgKiAyNCxcbiAgICAgIG1pbnV0ZXM6IDM2NSAqIDI0ICogNjAsXG4gICAgICBzZWNvbmRzOiAzNjUgKiAyNCAqIDYwICogNjAsXG4gICAgICBtaWxsaXNlY29uZHM6IDM2NSAqIDI0ICogNjAgKiA2MCAqIDEwMDBcbiAgICB9LFxuICAgIHF1YXJ0ZXJzOiB7XG4gICAgICBtb250aHM6IDMsXG4gICAgICB3ZWVrczogMTMsXG4gICAgICBkYXlzOiA5MSxcbiAgICAgIGhvdXJzOiA5MSAqIDI0LFxuICAgICAgbWludXRlczogOTEgKiAyNCAqIDYwLFxuICAgICAgbWlsbGlzZWNvbmRzOiA5MSAqIDI0ICogNjAgKiA2MCAqIDEwMDBcbiAgICB9LFxuICAgIG1vbnRoczoge1xuICAgICAgd2Vla3M6IDQsXG4gICAgICBkYXlzOiAzMCxcbiAgICAgIGhvdXJzOiAzMCAqIDI0LFxuICAgICAgbWludXRlczogMzAgKiAyNCAqIDYwLFxuICAgICAgc2Vjb25kczogMzAgKiAyNCAqIDYwICogNjAsXG4gICAgICBtaWxsaXNlY29uZHM6IDMwICogMjQgKiA2MCAqIDYwICogMTAwMFxuICAgIH1cbiAgfSwgbG93T3JkZXJNYXRyaXgpLFxuICAgICAgZGF5c0luWWVhckFjY3VyYXRlID0gMTQ2MDk3LjAgLyA0MDAsXG4gICAgICBkYXlzSW5Nb250aEFjY3VyYXRlID0gMTQ2MDk3LjAgLyA0ODAwLFxuICAgICAgYWNjdXJhdGVNYXRyaXggPSBPYmplY3QuYXNzaWduKHtcbiAgICB5ZWFyczoge1xuICAgICAgbW9udGhzOiAxMixcbiAgICAgIHdlZWtzOiBkYXlzSW5ZZWFyQWNjdXJhdGUgLyA3LFxuICAgICAgZGF5czogZGF5c0luWWVhckFjY3VyYXRlLFxuICAgICAgaG91cnM6IGRheXNJblllYXJBY2N1cmF0ZSAqIDI0LFxuICAgICAgbWludXRlczogZGF5c0luWWVhckFjY3VyYXRlICogMjQgKiA2MCxcbiAgICAgIHNlY29uZHM6IGRheXNJblllYXJBY2N1cmF0ZSAqIDI0ICogNjAgKiA2MCxcbiAgICAgIG1pbGxpc2Vjb25kczogZGF5c0luWWVhckFjY3VyYXRlICogMjQgKiA2MCAqIDYwICogMTAwMFxuICAgIH0sXG4gICAgcXVhcnRlcnM6IHtcbiAgICAgIG1vbnRoczogMyxcbiAgICAgIHdlZWtzOiBkYXlzSW5ZZWFyQWNjdXJhdGUgLyAyOCxcbiAgICAgIGRheXM6IGRheXNJblllYXJBY2N1cmF0ZSAvIDQsXG4gICAgICBob3VyczogZGF5c0luWWVhckFjY3VyYXRlICogMjQgLyA0LFxuICAgICAgbWludXRlczogZGF5c0luWWVhckFjY3VyYXRlICogMjQgKiA2MCAvIDQsXG4gICAgICBzZWNvbmRzOiBkYXlzSW5ZZWFyQWNjdXJhdGUgKiAyNCAqIDYwICogNjAgLyA0LFxuICAgICAgbWlsbGlzZWNvbmRzOiBkYXlzSW5ZZWFyQWNjdXJhdGUgKiAyNCAqIDYwICogNjAgKiAxMDAwIC8gNFxuICAgIH0sXG4gICAgbW9udGhzOiB7XG4gICAgICB3ZWVrczogZGF5c0luTW9udGhBY2N1cmF0ZSAvIDcsXG4gICAgICBkYXlzOiBkYXlzSW5Nb250aEFjY3VyYXRlLFxuICAgICAgaG91cnM6IGRheXNJbk1vbnRoQWNjdXJhdGUgKiAyNCxcbiAgICAgIG1pbnV0ZXM6IGRheXNJbk1vbnRoQWNjdXJhdGUgKiAyNCAqIDYwLFxuICAgICAgc2Vjb25kczogZGF5c0luTW9udGhBY2N1cmF0ZSAqIDI0ICogNjAgKiA2MCxcbiAgICAgIG1pbGxpc2Vjb25kczogZGF5c0luTW9udGhBY2N1cmF0ZSAqIDI0ICogNjAgKiA2MCAqIDEwMDBcbiAgICB9XG4gIH0sIGxvd09yZGVyTWF0cml4KTsgLy8gdW5pdHMgb3JkZXJlZCBieSBzaXplXG5cbiAgdmFyIG9yZGVyZWRVbml0cyA9IFtcInllYXJzXCIsIFwicXVhcnRlcnNcIiwgXCJtb250aHNcIiwgXCJ3ZWVrc1wiLCBcImRheXNcIiwgXCJob3Vyc1wiLCBcIm1pbnV0ZXNcIiwgXCJzZWNvbmRzXCIsIFwibWlsbGlzZWNvbmRzXCJdO1xuICB2YXIgcmV2ZXJzZVVuaXRzID0gb3JkZXJlZFVuaXRzLnNsaWNlKDApLnJldmVyc2UoKTsgLy8gY2xvbmUgcmVhbGx5IG1lYW5zIFwiY3JlYXRlIGFub3RoZXIgaW5zdGFuY2UganVzdCBsaWtlIHRoaXMgb25lLCBidXQgd2l0aCB0aGVzZSBjaGFuZ2VzXCJcblxuICBmdW5jdGlvbiBjbG9uZShkdXIsIGFsdHMsIGNsZWFyKSB7XG4gICAgaWYgKGNsZWFyID09PSB2b2lkIDApIHtcbiAgICAgIGNsZWFyID0gZmFsc2U7XG4gICAgfVxuXG4gICAgLy8gZGVlcCBtZXJnZSBmb3IgdmFsc1xuICAgIHZhciBjb25mID0ge1xuICAgICAgdmFsdWVzOiBjbGVhciA/IGFsdHMudmFsdWVzIDogT2JqZWN0LmFzc2lnbih7fSwgZHVyLnZhbHVlcywgYWx0cy52YWx1ZXMgfHwge30pLFxuICAgICAgbG9jOiBkdXIubG9jLmNsb25lKGFsdHMubG9jKSxcbiAgICAgIGNvbnZlcnNpb25BY2N1cmFjeTogYWx0cy5jb252ZXJzaW9uQWNjdXJhY3kgfHwgZHVyLmNvbnZlcnNpb25BY2N1cmFjeVxuICAgIH07XG4gICAgcmV0dXJuIG5ldyBEdXJhdGlvbihjb25mKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGFudGlUcnVuYyhuKSB7XG4gICAgcmV0dXJuIG4gPCAwID8gTWF0aC5mbG9vcihuKSA6IE1hdGguY2VpbChuKTtcbiAgfSAvLyBOQjogbXV0YXRlcyBwYXJhbWV0ZXJzXG5cblxuICBmdW5jdGlvbiBjb252ZXJ0KG1hdHJpeCwgZnJvbU1hcCwgZnJvbVVuaXQsIHRvTWFwLCB0b1VuaXQpIHtcbiAgICB2YXIgY29udiA9IG1hdHJpeFt0b1VuaXRdW2Zyb21Vbml0XSxcbiAgICAgICAgcmF3ID0gZnJvbU1hcFtmcm9tVW5pdF0gLyBjb252LFxuICAgICAgICBzYW1lU2lnbiA9IE1hdGguc2lnbihyYXcpID09PSBNYXRoLnNpZ24odG9NYXBbdG9Vbml0XSksXG4gICAgICAgIC8vIG9rLCBzbyB0aGlzIGlzIHdpbGQsIGJ1dCBzZWUgdGhlIG1hdHJpeCBpbiB0aGUgdGVzdHNcbiAgICBhZGRlZCA9ICFzYW1lU2lnbiAmJiB0b01hcFt0b1VuaXRdICE9PSAwICYmIE1hdGguYWJzKHJhdykgPD0gMSA/IGFudGlUcnVuYyhyYXcpIDogTWF0aC50cnVuYyhyYXcpO1xuICAgIHRvTWFwW3RvVW5pdF0gKz0gYWRkZWQ7XG4gICAgZnJvbU1hcFtmcm9tVW5pdF0gLT0gYWRkZWQgKiBjb252O1xuICB9IC8vIE5COiBtdXRhdGVzIHBhcmFtZXRlcnNcblxuXG4gIGZ1bmN0aW9uIG5vcm1hbGl6ZVZhbHVlcyhtYXRyaXgsIHZhbHMpIHtcbiAgICByZXZlcnNlVW5pdHMucmVkdWNlKGZ1bmN0aW9uIChwcmV2aW91cywgY3VycmVudCkge1xuICAgICAgaWYgKCFpc1VuZGVmaW5lZCh2YWxzW2N1cnJlbnRdKSkge1xuICAgICAgICBpZiAocHJldmlvdXMpIHtcbiAgICAgICAgICBjb252ZXJ0KG1hdHJpeCwgdmFscywgcHJldmlvdXMsIHZhbHMsIGN1cnJlbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGN1cnJlbnQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gcHJldmlvdXM7XG4gICAgICB9XG4gICAgfSwgbnVsbCk7XG4gIH1cbiAgLyoqXG4gICAqIEEgRHVyYXRpb24gb2JqZWN0IHJlcHJlc2VudHMgYSBwZXJpb2Qgb2YgdGltZSwgbGlrZSBcIjIgbW9udGhzXCIgb3IgXCIxIGRheSwgMSBob3VyXCIuIENvbmNlcHR1YWxseSwgaXQncyBqdXN0IGEgbWFwIG9mIHVuaXRzIHRvIHRoZWlyIHF1YW50aXRpZXMsIGFjY29tcGFuaWVkIGJ5IHNvbWUgYWRkaXRpb25hbCBjb25maWd1cmF0aW9uIGFuZCBtZXRob2RzIGZvciBjcmVhdGluZywgcGFyc2luZywgaW50ZXJyb2dhdGluZywgdHJhbnNmb3JtaW5nLCBhbmQgZm9ybWF0dGluZyB0aGVtLiBUaGV5IGNhbiBiZSB1c2VkIG9uIHRoZWlyIG93biBvciBpbiBjb25qdW5jdGlvbiB3aXRoIG90aGVyIEx1eG9uIHR5cGVzOyBmb3IgZXhhbXBsZSwgeW91IGNhbiB1c2Uge0BsaW5rIERhdGVUaW1lLnBsdXN9IHRvIGFkZCBhIER1cmF0aW9uIG9iamVjdCB0byBhIERhdGVUaW1lLCBwcm9kdWNpbmcgYW5vdGhlciBEYXRlVGltZS5cbiAgICpcbiAgICogSGVyZSBpcyBhIGJyaWVmIG92ZXJ2aWV3IG9mIGNvbW1vbmx5IHVzZWQgbWV0aG9kcyBhbmQgZ2V0dGVycyBpbiBEdXJhdGlvbjpcbiAgICpcbiAgICogKiAqKkNyZWF0aW9uKiogVG8gY3JlYXRlIGEgRHVyYXRpb24sIHVzZSB7QGxpbmsgRHVyYXRpb24uZnJvbU1pbGxpc30sIHtAbGluayBEdXJhdGlvbi5mcm9tT2JqZWN0fSwgb3Ige0BsaW5rIER1cmF0aW9uLmZyb21JU099LlxuICAgKiAqICoqVW5pdCB2YWx1ZXMqKiBTZWUgdGhlIHtAbGluayBEdXJhdGlvbi55ZWFyc30sIHtAbGluayBEdXJhdGlvbi5tb250aHN9LCB7QGxpbmsgRHVyYXRpb24ud2Vla3N9LCB7QGxpbmsgRHVyYXRpb24uZGF5c30sIHtAbGluayBEdXJhdGlvbi5ob3Vyc30sIHtAbGluayBEdXJhdGlvbi5taW51dGVzfSwge0BsaW5rIER1cmF0aW9uLnNlY29uZHN9LCB7QGxpbmsgRHVyYXRpb24ubWlsbGlzZWNvbmRzfSBhY2Nlc3NvcnMuXG4gICAqICogKipDb25maWd1cmF0aW9uKiogU2VlICB7QGxpbmsgRHVyYXRpb24ubG9jYWxlfSBhbmQge0BsaW5rIER1cmF0aW9uLm51bWJlcmluZ1N5c3RlbX0gYWNjZXNzb3JzLlxuICAgKiAqICoqVHJhbnNmb3JtYXRpb24qKiBUbyBjcmVhdGUgbmV3IER1cmF0aW9ucyBvdXQgb2Ygb2xkIG9uZXMgdXNlIHtAbGluayBEdXJhdGlvbi5wbHVzfSwge0BsaW5rIER1cmF0aW9uLm1pbnVzfSwge0BsaW5rIER1cmF0aW9uLm5vcm1hbGl6ZX0sIHtAbGluayBEdXJhdGlvbi5zZXR9LCB7QGxpbmsgRHVyYXRpb24ucmVjb25maWd1cmV9LCB7QGxpbmsgRHVyYXRpb24uc2hpZnRUb30sIGFuZCB7QGxpbmsgRHVyYXRpb24ubmVnYXRlfS5cbiAgICogKiAqKk91dHB1dCoqIFRvIGNvbnZlcnQgdGhlIER1cmF0aW9uIGludG8gb3RoZXIgcmVwcmVzZW50YXRpb25zLCBzZWUge0BsaW5rIER1cmF0aW9uLmFzfSwge0BsaW5rIER1cmF0aW9uLnRvSVNPfSwge0BsaW5rIER1cmF0aW9uLnRvRm9ybWF0fSwgYW5kIHtAbGluayBEdXJhdGlvbi50b0pTT059XG4gICAqXG4gICAqIFRoZXJlJ3MgYXJlIG1vcmUgbWV0aG9kcyBkb2N1bWVudGVkIGJlbG93LiBJbiBhZGRpdGlvbiwgZm9yIG1vcmUgaW5mb3JtYXRpb24gb24gc3VidGxlciB0b3BpY3MgbGlrZSBpbnRlcm5hdGlvbmFsaXphdGlvbiBhbmQgdmFsaWRpdHksIHNlZSB0aGUgZXh0ZXJuYWwgZG9jdW1lbnRhdGlvbi5cbiAgICovXG5cblxuICB2YXIgRHVyYXRpb24gPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIGZ1bmN0aW9uIER1cmF0aW9uKGNvbmZpZykge1xuICAgICAgdmFyIGFjY3VyYXRlID0gY29uZmlnLmNvbnZlcnNpb25BY2N1cmFjeSA9PT0gXCJsb25ndGVybVwiIHx8IGZhbHNlO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLnZhbHVlcyA9IGNvbmZpZy52YWx1ZXM7XG4gICAgICAvKipcbiAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxuICAgICAgICovXG5cbiAgICAgIHRoaXMubG9jID0gY29uZmlnLmxvYyB8fCBMb2NhbGUuY3JlYXRlKCk7XG4gICAgICAvKipcbiAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxuICAgICAgICovXG5cbiAgICAgIHRoaXMuY29udmVyc2lvbkFjY3VyYWN5ID0gYWNjdXJhdGUgPyBcImxvbmd0ZXJtXCIgOiBcImNhc3VhbFwiO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLmludmFsaWQgPSBjb25maWcuaW52YWxpZCB8fCBudWxsO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLm1hdHJpeCA9IGFjY3VyYXRlID8gYWNjdXJhdGVNYXRyaXggOiBjYXN1YWxNYXRyaXg7XG4gICAgICAvKipcbiAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxuICAgICAgICovXG5cbiAgICAgIHRoaXMuaXNMdXhvbkR1cmF0aW9uID0gdHJ1ZTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIER1cmF0aW9uIGZyb20gYSBudW1iZXIgb2YgbWlsbGlzZWNvbmRzLlxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBjb3VudCBvZiBtaWxsaXNlY29uZHNcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnMgZm9yIHBhcnNpbmdcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW29wdHMubG9jYWxlPSdlbi1VUyddIC0gdGhlIGxvY2FsZSB0byB1c2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5udW1iZXJpbmdTeXN0ZW0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbSB0byB1c2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW29wdHMuY29udmVyc2lvbkFjY3VyYWN5PSdjYXN1YWwnXSAtIHRoZSBjb252ZXJzaW9uIHN5c3RlbSB0byB1c2VcbiAgICAgKiBAcmV0dXJuIHtEdXJhdGlvbn1cbiAgICAgKi9cblxuXG4gICAgRHVyYXRpb24uZnJvbU1pbGxpcyA9IGZ1bmN0aW9uIGZyb21NaWxsaXMoY291bnQsIG9wdHMpIHtcbiAgICAgIHJldHVybiBEdXJhdGlvbi5mcm9tT2JqZWN0KE9iamVjdC5hc3NpZ24oe1xuICAgICAgICBtaWxsaXNlY29uZHM6IGNvdW50XG4gICAgICB9LCBvcHRzKSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhIER1cmF0aW9uIGZyb20gYSBKYXZhc2NyaXB0IG9iamVjdCB3aXRoIGtleXMgbGlrZSAneWVhcnMnIGFuZCAnaG91cnMuXG4gICAgICogSWYgdGhpcyBvYmplY3QgaXMgZW1wdHkgdGhlbiBhIHplcm8gbWlsbGlzZWNvbmRzIGR1cmF0aW9uIGlzIHJldHVybmVkLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvYmogLSB0aGUgb2JqZWN0IHRvIGNyZWF0ZSB0aGUgRGF0ZVRpbWUgZnJvbVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoueWVhcnNcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gb2JqLnF1YXJ0ZXJzXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9iai5tb250aHNcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gb2JqLndlZWtzXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9iai5kYXlzXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9iai5ob3Vyc1xuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoubWludXRlc1xuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmouc2Vjb25kc1xuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoubWlsbGlzZWNvbmRzXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvYmoubG9jYWxlPSdlbi1VUyddIC0gdGhlIGxvY2FsZSB0byB1c2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb2JqLm51bWJlcmluZ1N5c3RlbSAtIHRoZSBudW1iZXJpbmcgc3lzdGVtIHRvIHVzZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb2JqLmNvbnZlcnNpb25BY2N1cmFjeT0nY2FzdWFsJ10gLSB0aGUgY29udmVyc2lvbiBzeXN0ZW0gdG8gdXNlXG4gICAgICogQHJldHVybiB7RHVyYXRpb259XG4gICAgICovXG4gICAgO1xuXG4gICAgRHVyYXRpb24uZnJvbU9iamVjdCA9IGZ1bmN0aW9uIGZyb21PYmplY3Qob2JqKSB7XG4gICAgICBpZiAob2JqID09IG51bGwgfHwgdHlwZW9mIG9iaiAhPT0gXCJvYmplY3RcIikge1xuICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEFyZ3VtZW50RXJyb3IoXCJEdXJhdGlvbi5mcm9tT2JqZWN0OiBhcmd1bWVudCBleHBlY3RlZCB0byBiZSBhbiBvYmplY3QsIGdvdCBcIiArIChvYmogPT09IG51bGwgPyBcIm51bGxcIiA6IHR5cGVvZiBvYmopKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ldyBEdXJhdGlvbih7XG4gICAgICAgIHZhbHVlczogbm9ybWFsaXplT2JqZWN0KG9iaiwgRHVyYXRpb24ubm9ybWFsaXplVW5pdCwgW1wibG9jYWxlXCIsIFwibnVtYmVyaW5nU3lzdGVtXCIsIFwiY29udmVyc2lvbkFjY3VyYWN5XCIsIFwiem9uZVwiIC8vIGEgYml0IG9mIGRlYnQ7IGl0J3Mgc3VwZXIgaW5jb252ZW5pZW50IGludGVybmFsbHkgbm90IHRvIGJlIGFibGUgdG8gYmxpbmRseSBwYXNzIHRoaXNcbiAgICAgICAgXSksXG4gICAgICAgIGxvYzogTG9jYWxlLmZyb21PYmplY3Qob2JqKSxcbiAgICAgICAgY29udmVyc2lvbkFjY3VyYWN5OiBvYmouY29udmVyc2lvbkFjY3VyYWN5XG4gICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgRHVyYXRpb24gZnJvbSBhbiBJU08gODYwMSBkdXJhdGlvbiBzdHJpbmcuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRleHQgLSB0ZXh0IHRvIHBhcnNlXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRpb25zIGZvciBwYXJzaW5nXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmxvY2FsZT0nZW4tVVMnXSAtIHRoZSBsb2NhbGUgdG8gdXNlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdHMubnVtYmVyaW5nU3lzdGVtIC0gdGhlIG51bWJlcmluZyBzeXN0ZW0gdG8gdXNlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmNvbnZlcnNpb25BY2N1cmFjeT0nY2FzdWFsJ10gLSB0aGUgY29udmVyc2lvbiBzeXN0ZW0gdG8gdXNlXG4gICAgICogQHNlZSBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9JU09fODYwMSNEdXJhdGlvbnNcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tSVNPKCdQM1k2TTFXNERUMTJIMzBNNVMnKS50b09iamVjdCgpIC8vPT4geyB5ZWFyczogMywgbW9udGhzOiA2LCB3ZWVrczogMSwgZGF5czogNCwgaG91cnM6IDEyLCBtaW51dGVzOiAzMCwgc2Vjb25kczogNSB9XG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbUlTTygnUFQyM0gnKS50b09iamVjdCgpIC8vPT4geyBob3VyczogMjMgfVxuICAgICAqIEBleGFtcGxlIER1cmF0aW9uLmZyb21JU08oJ1A1WTNNJykudG9PYmplY3QoKSAvLz0+IHsgeWVhcnM6IDUsIG1vbnRoczogMyB9XG4gICAgICogQHJldHVybiB7RHVyYXRpb259XG4gICAgICovXG4gICAgO1xuXG4gICAgRHVyYXRpb24uZnJvbUlTTyA9IGZ1bmN0aW9uIGZyb21JU08odGV4dCwgb3B0cykge1xuICAgICAgdmFyIF9wYXJzZUlTT0R1cmF0aW9uID0gcGFyc2VJU09EdXJhdGlvbih0ZXh0KSxcbiAgICAgICAgICBwYXJzZWQgPSBfcGFyc2VJU09EdXJhdGlvblswXTtcblxuICAgICAgaWYgKHBhcnNlZCkge1xuICAgICAgICB2YXIgb2JqID0gT2JqZWN0LmFzc2lnbihwYXJzZWQsIG9wdHMpO1xuICAgICAgICByZXR1cm4gRHVyYXRpb24uZnJvbU9iamVjdChvYmopO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIER1cmF0aW9uLmludmFsaWQoXCJ1bnBhcnNhYmxlXCIsIFwidGhlIGlucHV0IFxcXCJcIiArIHRleHQgKyBcIlxcXCIgY2FuJ3QgYmUgcGFyc2VkIGFzIElTTyA4NjAxXCIpO1xuICAgICAgfVxuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYW4gaW52YWxpZCBEdXJhdGlvbi5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmVhc29uIC0gc2ltcGxlIHN0cmluZyBvZiB3aHkgdGhpcyBkYXRldGltZSBpcyBpbnZhbGlkLiBTaG91bGQgbm90IGNvbnRhaW4gcGFyYW1ldGVycyBvciBhbnl0aGluZyBlbHNlIGRhdGEtZGVwZW5kZW50XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtleHBsYW5hdGlvbj1udWxsXSAtIGxvbmdlciBleHBsYW5hdGlvbiwgbWF5IGluY2x1ZGUgcGFyYW1ldGVycyBhbmQgb3RoZXIgdXNlZnVsIGRlYnVnZ2luZyBpbmZvcm1hdGlvblxuICAgICAqIEByZXR1cm4ge0R1cmF0aW9ufVxuICAgICAqL1xuICAgIDtcblxuICAgIER1cmF0aW9uLmludmFsaWQgPSBmdW5jdGlvbiBpbnZhbGlkKHJlYXNvbiwgZXhwbGFuYXRpb24pIHtcbiAgICAgIGlmIChleHBsYW5hdGlvbiA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGV4cGxhbmF0aW9uID0gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKCFyZWFzb24pIHtcbiAgICAgICAgdGhyb3cgbmV3IEludmFsaWRBcmd1bWVudEVycm9yKFwibmVlZCB0byBzcGVjaWZ5IGEgcmVhc29uIHRoZSBEdXJhdGlvbiBpcyBpbnZhbGlkXCIpO1xuICAgICAgfVxuXG4gICAgICB2YXIgaW52YWxpZCA9IHJlYXNvbiBpbnN0YW5jZW9mIEludmFsaWQgPyByZWFzb24gOiBuZXcgSW52YWxpZChyZWFzb24sIGV4cGxhbmF0aW9uKTtcblxuICAgICAgaWYgKFNldHRpbmdzLnRocm93T25JbnZhbGlkKSB7XG4gICAgICAgIHRocm93IG5ldyBJbnZhbGlkRHVyYXRpb25FcnJvcihpbnZhbGlkKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBuZXcgRHVyYXRpb24oe1xuICAgICAgICAgIGludmFsaWQ6IGludmFsaWRcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgO1xuXG4gICAgRHVyYXRpb24ubm9ybWFsaXplVW5pdCA9IGZ1bmN0aW9uIG5vcm1hbGl6ZVVuaXQodW5pdCkge1xuICAgICAgdmFyIG5vcm1hbGl6ZWQgPSB7XG4gICAgICAgIHllYXI6IFwieWVhcnNcIixcbiAgICAgICAgeWVhcnM6IFwieWVhcnNcIixcbiAgICAgICAgcXVhcnRlcjogXCJxdWFydGVyc1wiLFxuICAgICAgICBxdWFydGVyczogXCJxdWFydGVyc1wiLFxuICAgICAgICBtb250aDogXCJtb250aHNcIixcbiAgICAgICAgbW9udGhzOiBcIm1vbnRoc1wiLFxuICAgICAgICB3ZWVrOiBcIndlZWtzXCIsXG4gICAgICAgIHdlZWtzOiBcIndlZWtzXCIsXG4gICAgICAgIGRheTogXCJkYXlzXCIsXG4gICAgICAgIGRheXM6IFwiZGF5c1wiLFxuICAgICAgICBob3VyOiBcImhvdXJzXCIsXG4gICAgICAgIGhvdXJzOiBcImhvdXJzXCIsXG4gICAgICAgIG1pbnV0ZTogXCJtaW51dGVzXCIsXG4gICAgICAgIG1pbnV0ZXM6IFwibWludXRlc1wiLFxuICAgICAgICBzZWNvbmQ6IFwic2Vjb25kc1wiLFxuICAgICAgICBzZWNvbmRzOiBcInNlY29uZHNcIixcbiAgICAgICAgbWlsbGlzZWNvbmQ6IFwibWlsbGlzZWNvbmRzXCIsXG4gICAgICAgIG1pbGxpc2Vjb25kczogXCJtaWxsaXNlY29uZHNcIlxuICAgICAgfVt1bml0ID8gdW5pdC50b0xvd2VyQ2FzZSgpIDogdW5pdF07XG4gICAgICBpZiAoIW5vcm1hbGl6ZWQpIHRocm93IG5ldyBJbnZhbGlkVW5pdEVycm9yKHVuaXQpO1xuICAgICAgcmV0dXJuIG5vcm1hbGl6ZWQ7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENoZWNrIGlmIGFuIG9iamVjdCBpcyBhIER1cmF0aW9uLiBXb3JrcyBhY3Jvc3MgY29udGV4dCBib3VuZGFyaWVzXG4gICAgICogQHBhcmFtIHtvYmplY3R9IG9cbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIER1cmF0aW9uLmlzRHVyYXRpb24gPSBmdW5jdGlvbiBpc0R1cmF0aW9uKG8pIHtcbiAgICAgIHJldHVybiBvICYmIG8uaXNMdXhvbkR1cmF0aW9uIHx8IGZhbHNlO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBHZXQgIHRoZSBsb2NhbGUgb2YgYSBEdXJhdGlvbiwgc3VjaCAnZW4tR0InXG4gICAgICogQHR5cGUge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICB2YXIgX3Byb3RvID0gRHVyYXRpb24ucHJvdG90eXBlO1xuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIER1cmF0aW9uIGZvcm1hdHRlZCBhY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmllZCBmb3JtYXQgc3RyaW5nLiBZb3UgbWF5IHVzZSB0aGVzZSB0b2tlbnM6XG4gICAgICogKiBgU2AgZm9yIG1pbGxpc2Vjb25kc1xuICAgICAqICogYHNgIGZvciBzZWNvbmRzXG4gICAgICogKiBgbWAgZm9yIG1pbnV0ZXNcbiAgICAgKiAqIGBoYCBmb3IgaG91cnNcbiAgICAgKiAqIGBkYCBmb3IgZGF5c1xuICAgICAqICogYE1gIGZvciBtb250aHNcbiAgICAgKiAqIGB5YCBmb3IgeWVhcnNcbiAgICAgKiBOb3RlczpcbiAgICAgKiAqIEFkZCBwYWRkaW5nIGJ5IHJlcGVhdGluZyB0aGUgdG9rZW4sIGUuZy4gXCJ5eVwiIHBhZHMgdGhlIHllYXJzIHRvIHR3byBkaWdpdHMsIFwiaGhoaFwiIHBhZHMgdGhlIGhvdXJzIG91dCB0byBmb3VyIGRpZ2l0c1xuICAgICAqICogVGhlIGR1cmF0aW9uIHdpbGwgYmUgY29udmVydGVkIHRvIHRoZSBzZXQgb2YgdW5pdHMgaW4gdGhlIGZvcm1hdCBzdHJpbmcgdXNpbmcge0BsaW5rIER1cmF0aW9uLnNoaWZ0VG99IGFuZCB0aGUgRHVyYXRpb25zJ3MgY29udmVyc2lvbiBhY2N1cmFjeSBzZXR0aW5nLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBmbXQgLSB0aGUgZm9ybWF0IHN0cmluZ1xuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9uc1xuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuZmxvb3I9dHJ1ZV0gLSBmbG9vciBudW1lcmljYWwgdmFsdWVzXG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IHllYXJzOiAxLCBkYXlzOiA2LCBzZWNvbmRzOiAyIH0pLnRvRm9ybWF0KFwieSBkIHNcIikgLy89PiBcIjEgNiAyXCJcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHsgeWVhcnM6IDEsIGRheXM6IDYsIHNlY29uZHM6IDIgfSkudG9Gb3JtYXQoXCJ5eSBkZCBzc3NcIikgLy89PiBcIjAxIDA2IDAwMlwiXG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IHllYXJzOiAxLCBkYXlzOiA2LCBzZWNvbmRzOiAyIH0pLnRvRm9ybWF0KFwiTSBTXCIpIC8vPT4gXCIxMiA1MTg0MDIwMDBcIlxuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICBfcHJvdG8udG9Gb3JtYXQgPSBmdW5jdGlvbiB0b0Zvcm1hdChmbXQsIG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICAvLyByZXZlcnNlLWNvbXBhdCBzaW5jZSAxLjI7IHdlIGFsd2F5cyByb3VuZCBkb3duIG5vdywgbmV2ZXIgdXAsIGFuZCB3ZSBkbyBpdCBieSBkZWZhdWx0XG4gICAgICB2YXIgZm10T3B0cyA9IE9iamVjdC5hc3NpZ24oe30sIG9wdHMsIHtcbiAgICAgICAgZmxvb3I6IG9wdHMucm91bmQgIT09IGZhbHNlICYmIG9wdHMuZmxvb3IgIT09IGZhbHNlXG4gICAgICB9KTtcbiAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyBGb3JtYXR0ZXIuY3JlYXRlKHRoaXMubG9jLCBmbXRPcHRzKS5mb3JtYXREdXJhdGlvbkZyb21TdHJpbmcodGhpcywgZm10KSA6IElOVkFMSUQ7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYSBKYXZhc2NyaXB0IG9iamVjdCB3aXRoIHRoaXMgRHVyYXRpb24ncyB2YWx1ZXMuXG4gICAgICogQHBhcmFtIG9wdHMgLSBvcHRpb25zIGZvciBnZW5lcmF0aW5nIHRoZSBvYmplY3RcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRzLmluY2x1ZGVDb25maWc9ZmFsc2VdIC0gaW5jbHVkZSBjb25maWd1cmF0aW9uIGF0dHJpYnV0ZXMgaW4gdGhlIG91dHB1dFxuICAgICAqIEBleGFtcGxlIER1cmF0aW9uLmZyb21PYmplY3QoeyB5ZWFyczogMSwgZGF5czogNiwgc2Vjb25kczogMiB9KS50b09iamVjdCgpIC8vPT4geyB5ZWFyczogMSwgZGF5czogNiwgc2Vjb25kczogMiB9XG4gICAgICogQHJldHVybiB7T2JqZWN0fVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHt9O1xuICAgICAgdmFyIGJhc2UgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnZhbHVlcyk7XG5cbiAgICAgIGlmIChvcHRzLmluY2x1ZGVDb25maWcpIHtcbiAgICAgICAgYmFzZS5jb252ZXJzaW9uQWNjdXJhY3kgPSB0aGlzLmNvbnZlcnNpb25BY2N1cmFjeTtcbiAgICAgICAgYmFzZS5udW1iZXJpbmdTeXN0ZW0gPSB0aGlzLmxvYy5udW1iZXJpbmdTeXN0ZW07XG4gICAgICAgIGJhc2UubG9jYWxlID0gdGhpcy5sb2MubG9jYWxlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gYmFzZTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhbiBJU08gODYwMS1jb21wbGlhbnQgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRoaXMgRHVyYXRpb24uXG4gICAgICogQHNlZSBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9JU09fODYwMSNEdXJhdGlvbnNcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHsgeWVhcnM6IDMsIHNlY29uZHM6IDQ1IH0pLnRvSVNPKCkgLy89PiAnUDNZVDQ1UydcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHsgbW9udGhzOiA0LCBzZWNvbmRzOiA0NSB9KS50b0lTTygpIC8vPT4gJ1A0TVQ0NVMnXG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IG1vbnRoczogNSB9KS50b0lTTygpIC8vPT4gJ1A1TSdcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHsgbWludXRlczogNSB9KS50b0lTTygpIC8vPT4gJ1BUNU0nXG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IG1pbGxpc2Vjb25kczogNiB9KS50b0lTTygpIC8vPT4gJ1BUMC4wMDZTJ1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9JU08gPSBmdW5jdGlvbiB0b0lTTygpIHtcbiAgICAgIC8vIHdlIGNvdWxkIHVzZSB0aGUgZm9ybWF0dGVyLCBidXQgdGhpcyBpcyBhbiBlYXNpZXIgd2F5IHRvIGdldCB0aGUgbWluaW11bSBzdHJpbmdcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSByZXR1cm4gbnVsbDtcbiAgICAgIHZhciBzID0gXCJQXCI7XG4gICAgICBpZiAodGhpcy55ZWFycyAhPT0gMCkgcyArPSB0aGlzLnllYXJzICsgXCJZXCI7XG4gICAgICBpZiAodGhpcy5tb250aHMgIT09IDAgfHwgdGhpcy5xdWFydGVycyAhPT0gMCkgcyArPSB0aGlzLm1vbnRocyArIHRoaXMucXVhcnRlcnMgKiAzICsgXCJNXCI7XG4gICAgICBpZiAodGhpcy53ZWVrcyAhPT0gMCkgcyArPSB0aGlzLndlZWtzICsgXCJXXCI7XG4gICAgICBpZiAodGhpcy5kYXlzICE9PSAwKSBzICs9IHRoaXMuZGF5cyArIFwiRFwiO1xuICAgICAgaWYgKHRoaXMuaG91cnMgIT09IDAgfHwgdGhpcy5taW51dGVzICE9PSAwIHx8IHRoaXMuc2Vjb25kcyAhPT0gMCB8fCB0aGlzLm1pbGxpc2Vjb25kcyAhPT0gMCkgcyArPSBcIlRcIjtcbiAgICAgIGlmICh0aGlzLmhvdXJzICE9PSAwKSBzICs9IHRoaXMuaG91cnMgKyBcIkhcIjtcbiAgICAgIGlmICh0aGlzLm1pbnV0ZXMgIT09IDApIHMgKz0gdGhpcy5taW51dGVzICsgXCJNXCI7XG4gICAgICBpZiAodGhpcy5zZWNvbmRzICE9PSAwIHx8IHRoaXMubWlsbGlzZWNvbmRzICE9PSAwKSAvLyB0aGlzIHdpbGwgaGFuZGxlIFwiZmxvYXRpbmcgcG9pbnQgbWFkbmVzc1wiIGJ5IHJlbW92aW5nIGV4dHJhIGRlY2ltYWwgcGxhY2VzXG4gICAgICAgIC8vIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzU4ODAwNC9pcy1mbG9hdGluZy1wb2ludC1tYXRoLWJyb2tlblxuICAgICAgICBzICs9IHJvdW5kVG8odGhpcy5zZWNvbmRzICsgdGhpcy5taWxsaXNlY29uZHMgLyAxMDAwLCAzKSArIFwiU1wiO1xuICAgICAgaWYgKHMgPT09IFwiUFwiKSBzICs9IFwiVDBTXCI7XG4gICAgICByZXR1cm4gcztcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhbiBJU08gODYwMSByZXByZXNlbnRhdGlvbiBvZiB0aGlzIER1cmF0aW9uIGFwcHJvcHJpYXRlIGZvciB1c2UgaW4gSlNPTi5cbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvSlNPTiA9IGZ1bmN0aW9uIHRvSlNPTigpIHtcbiAgICAgIHJldHVybiB0aGlzLnRvSVNPKCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gSVNPIDg2MDEgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBEdXJhdGlvbiBhcHByb3ByaWF0ZSBmb3IgdXNlIGluIGRlYnVnZ2luZy5cbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy50b0lTTygpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIG1pbGxpc2Vjb25kcyB2YWx1ZSBvZiB0aGlzIER1cmF0aW9uLlxuICAgICAqIEByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udmFsdWVPZiA9IGZ1bmN0aW9uIHZhbHVlT2YoKSB7XG4gICAgICByZXR1cm4gdGhpcy5hcyhcIm1pbGxpc2Vjb25kc1wiKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogTWFrZSB0aGlzIER1cmF0aW9uIGxvbmdlciBieSB0aGUgc3BlY2lmaWVkIGFtb3VudC4gUmV0dXJuIGEgbmV3bHktY29uc3RydWN0ZWQgRHVyYXRpb24uXG4gICAgICogQHBhcmFtIHtEdXJhdGlvbnxPYmplY3R8bnVtYmVyfSBkdXJhdGlvbiAtIFRoZSBhbW91bnQgdG8gYWRkLiBFaXRoZXIgYSBMdXhvbiBEdXJhdGlvbiwgYSBudW1iZXIgb2YgbWlsbGlzZWNvbmRzLCB0aGUgb2JqZWN0IGFyZ3VtZW50IHRvIER1cmF0aW9uLmZyb21PYmplY3QoKVxuICAgICAqIEByZXR1cm4ge0R1cmF0aW9ufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5wbHVzID0gZnVuY3Rpb24gcGx1cyhkdXJhdGlvbikge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuICAgICAgdmFyIGR1ciA9IGZyaWVuZGx5RHVyYXRpb24oZHVyYXRpb24pLFxuICAgICAgICAgIHJlc3VsdCA9IHt9O1xuXG4gICAgICBmb3IgKHZhciBfaSA9IDAsIF9vcmRlcmVkVW5pdHMgPSBvcmRlcmVkVW5pdHM7IF9pIDwgX29yZGVyZWRVbml0cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgdmFyIGsgPSBfb3JkZXJlZFVuaXRzW19pXTtcblxuICAgICAgICBpZiAoaGFzT3duUHJvcGVydHkoZHVyLnZhbHVlcywgaykgfHwgaGFzT3duUHJvcGVydHkodGhpcy52YWx1ZXMsIGspKSB7XG4gICAgICAgICAgcmVzdWx0W2tdID0gZHVyLmdldChrKSArIHRoaXMuZ2V0KGspO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjbG9uZSh0aGlzLCB7XG4gICAgICAgIHZhbHVlczogcmVzdWx0XG4gICAgICB9LCB0cnVlKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogTWFrZSB0aGlzIER1cmF0aW9uIHNob3J0ZXIgYnkgdGhlIHNwZWNpZmllZCBhbW91bnQuIFJldHVybiBhIG5ld2x5LWNvbnN0cnVjdGVkIER1cmF0aW9uLlxuICAgICAqIEBwYXJhbSB7RHVyYXRpb258T2JqZWN0fG51bWJlcn0gZHVyYXRpb24gLSBUaGUgYW1vdW50IHRvIHN1YnRyYWN0LiBFaXRoZXIgYSBMdXhvbiBEdXJhdGlvbiwgYSBudW1iZXIgb2YgbWlsbGlzZWNvbmRzLCB0aGUgb2JqZWN0IGFyZ3VtZW50IHRvIER1cmF0aW9uLmZyb21PYmplY3QoKVxuICAgICAqIEByZXR1cm4ge0R1cmF0aW9ufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5taW51cyA9IGZ1bmN0aW9uIG1pbnVzKGR1cmF0aW9uKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHRoaXM7XG4gICAgICB2YXIgZHVyID0gZnJpZW5kbHlEdXJhdGlvbihkdXJhdGlvbik7XG4gICAgICByZXR1cm4gdGhpcy5wbHVzKGR1ci5uZWdhdGUoKSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFNjYWxlIHRoaXMgRHVyYXRpb24gYnkgdGhlIHNwZWNpZmllZCBhbW91bnQuIFJldHVybiBhIG5ld2x5LWNvbnN0cnVjdGVkIER1cmF0aW9uLlxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IGZuIC0gVGhlIGZ1bmN0aW9uIHRvIGFwcGx5IHRvIGVhY2ggdW5pdC4gQXJpdHkgaXMgMSBvciAyOiB0aGUgdmFsdWUgb2YgdGhlIHVuaXQgYW5kLCBvcHRpb25hbGx5LCB0aGUgdW5pdCBuYW1lLiBNdXN0IHJldHVybiBhIG51bWJlci5cbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHsgaG91cnM6IDEsIG1pbnV0ZXM6IDMwIH0pLm1hcFVuaXQoeCA9PiB4ICogMikgLy89PiB7IGhvdXJzOiAyLCBtaW51dGVzOiA2MCB9XG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IGhvdXJzOiAxLCBtaW51dGVzOiAzMCB9KS5tYXBVbml0KCh4LCB1KSA9PiB1ID09PSBcImhvdXJcIiA/IHggKiAyIDogeCkgLy89PiB7IGhvdXJzOiAyLCBtaW51dGVzOiAzMCB9XG4gICAgICogQHJldHVybiB7RHVyYXRpb259XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLm1hcFVuaXRzID0gZnVuY3Rpb24gbWFwVW5pdHMoZm4pIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSByZXR1cm4gdGhpcztcbiAgICAgIHZhciByZXN1bHQgPSB7fTtcblxuICAgICAgZm9yICh2YXIgX2kyID0gMCwgX09iamVjdCRrZXlzID0gT2JqZWN0LmtleXModGhpcy52YWx1ZXMpOyBfaTIgPCBfT2JqZWN0JGtleXMubGVuZ3RoOyBfaTIrKykge1xuICAgICAgICB2YXIgayA9IF9PYmplY3Qka2V5c1tfaTJdO1xuICAgICAgICByZXN1bHRba10gPSBhc051bWJlcihmbih0aGlzLnZhbHVlc1trXSwgaykpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gY2xvbmUodGhpcywge1xuICAgICAgICB2YWx1ZXM6IHJlc3VsdFxuICAgICAgfSwgdHJ1ZSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgdmFsdWUgb2YgdW5pdC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdW5pdCAtIGEgdW5pdCBzdWNoIGFzICdtaW51dGUnIG9yICdkYXknXG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7eWVhcnM6IDIsIGRheXM6IDN9KS55ZWFycyAvLz0+IDJcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHt5ZWFyczogMiwgZGF5czogM30pLm1vbnRocyAvLz0+IDBcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHt5ZWFyczogMiwgZGF5czogM30pLmRheXMgLy89PiAzXG4gICAgICogQHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5nZXQgPSBmdW5jdGlvbiBnZXQodW5pdCkge1xuICAgICAgcmV0dXJuIHRoaXNbRHVyYXRpb24ubm9ybWFsaXplVW5pdCh1bml0KV07XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFwiU2V0XCIgdGhlIHZhbHVlcyBvZiBzcGVjaWZpZWQgdW5pdHMuIFJldHVybiBhIG5ld2x5LWNvbnN0cnVjdGVkIER1cmF0aW9uLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSB2YWx1ZXMgLSBhIG1hcHBpbmcgb2YgdW5pdHMgdG8gbnVtYmVyc1xuICAgICAqIEBleGFtcGxlIGR1ci5zZXQoeyB5ZWFyczogMjAxNyB9KVxuICAgICAqIEBleGFtcGxlIGR1ci5zZXQoeyBob3VyczogOCwgbWludXRlczogMzAgfSlcbiAgICAgKiBAcmV0dXJuIHtEdXJhdGlvbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uc2V0ID0gZnVuY3Rpb24gc2V0KHZhbHVlcykge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuICAgICAgdmFyIG1peGVkID0gT2JqZWN0LmFzc2lnbih0aGlzLnZhbHVlcywgbm9ybWFsaXplT2JqZWN0KHZhbHVlcywgRHVyYXRpb24ubm9ybWFsaXplVW5pdCwgW10pKTtcbiAgICAgIHJldHVybiBjbG9uZSh0aGlzLCB7XG4gICAgICAgIHZhbHVlczogbWl4ZWRcbiAgICAgIH0pO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBcIlNldFwiIHRoZSBsb2NhbGUgYW5kL29yIG51bWJlcmluZ1N5c3RlbS4gIFJldHVybnMgYSBuZXdseS1jb25zdHJ1Y3RlZCBEdXJhdGlvbi5cbiAgICAgKiBAZXhhbXBsZSBkdXIucmVjb25maWd1cmUoeyBsb2NhbGU6ICdlbi1HQicgfSlcbiAgICAgKiBAcmV0dXJuIHtEdXJhdGlvbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8ucmVjb25maWd1cmUgPSBmdW5jdGlvbiByZWNvbmZpZ3VyZShfdGVtcCkge1xuICAgICAgdmFyIF9yZWYgPSBfdGVtcCA9PT0gdm9pZCAwID8ge30gOiBfdGVtcCxcbiAgICAgICAgICBsb2NhbGUgPSBfcmVmLmxvY2FsZSxcbiAgICAgICAgICBudW1iZXJpbmdTeXN0ZW0gPSBfcmVmLm51bWJlcmluZ1N5c3RlbSxcbiAgICAgICAgICBjb252ZXJzaW9uQWNjdXJhY3kgPSBfcmVmLmNvbnZlcnNpb25BY2N1cmFjeTtcblxuICAgICAgdmFyIGxvYyA9IHRoaXMubG9jLmNsb25lKHtcbiAgICAgICAgbG9jYWxlOiBsb2NhbGUsXG4gICAgICAgIG51bWJlcmluZ1N5c3RlbTogbnVtYmVyaW5nU3lzdGVtXG4gICAgICB9KSxcbiAgICAgICAgICBvcHRzID0ge1xuICAgICAgICBsb2M6IGxvY1xuICAgICAgfTtcblxuICAgICAgaWYgKGNvbnZlcnNpb25BY2N1cmFjeSkge1xuICAgICAgICBvcHRzLmNvbnZlcnNpb25BY2N1cmFjeSA9IGNvbnZlcnNpb25BY2N1cmFjeTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGNsb25lKHRoaXMsIG9wdHMpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIGxlbmd0aCBvZiB0aGUgZHVyYXRpb24gaW4gdGhlIHNwZWNpZmllZCB1bml0LlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1bml0IC0gYSB1bml0IHN1Y2ggYXMgJ21pbnV0ZXMnIG9yICdkYXlzJ1xuICAgICAqIEBleGFtcGxlIER1cmF0aW9uLmZyb21PYmplY3Qoe3llYXJzOiAxfSkuYXMoJ2RheXMnKSAvLz0+IDM2NVxuICAgICAqIEBleGFtcGxlIER1cmF0aW9uLmZyb21PYmplY3Qoe3llYXJzOiAxfSkuYXMoJ21vbnRocycpIC8vPT4gMTJcbiAgICAgKiBAZXhhbXBsZSBEdXJhdGlvbi5mcm9tT2JqZWN0KHtob3VyczogNjB9KS5hcygnZGF5cycpIC8vPT4gMi41XG4gICAgICogQHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5hcyA9IGZ1bmN0aW9uIGFzKHVuaXQpIHtcbiAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLnNoaWZ0VG8odW5pdCkuZ2V0KHVuaXQpIDogTmFOO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZWR1Y2UgdGhpcyBEdXJhdGlvbiB0byBpdHMgY2Fub25pY2FsIHJlcHJlc2VudGF0aW9uIGluIGl0cyBjdXJyZW50IHVuaXRzLlxuICAgICAqIEBleGFtcGxlIER1cmF0aW9uLmZyb21PYmplY3QoeyB5ZWFyczogMiwgZGF5czogNTAwMCB9KS5ub3JtYWxpemUoKS50b09iamVjdCgpIC8vPT4geyB5ZWFyczogMTUsIGRheXM6IDI1NSB9XG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IGhvdXJzOiAxMiwgbWludXRlczogLTQ1IH0pLm5vcm1hbGl6ZSgpLnRvT2JqZWN0KCkgLy89PiB7IGhvdXJzOiAxMSwgbWludXRlczogMTUgfVxuICAgICAqIEByZXR1cm4ge0R1cmF0aW9ufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5ub3JtYWxpemUgPSBmdW5jdGlvbiBub3JtYWxpemUoKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHRoaXM7XG4gICAgICB2YXIgdmFscyA9IHRoaXMudG9PYmplY3QoKTtcbiAgICAgIG5vcm1hbGl6ZVZhbHVlcyh0aGlzLm1hdHJpeCwgdmFscyk7XG4gICAgICByZXR1cm4gY2xvbmUodGhpcywge1xuICAgICAgICB2YWx1ZXM6IHZhbHNcbiAgICAgIH0sIHRydWUpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0IHRoaXMgRHVyYXRpb24gaW50byBpdHMgcmVwcmVzZW50YXRpb24gaW4gYSBkaWZmZXJlbnQgc2V0IG9mIHVuaXRzLlxuICAgICAqIEBleGFtcGxlIER1cmF0aW9uLmZyb21PYmplY3QoeyBob3VyczogMSwgc2Vjb25kczogMzAgfSkuc2hpZnRUbygnbWludXRlcycsICdtaWxsaXNlY29uZHMnKS50b09iamVjdCgpIC8vPT4geyBtaW51dGVzOiA2MCwgbWlsbGlzZWNvbmRzOiAzMDAwMCB9XG4gICAgICogQHJldHVybiB7RHVyYXRpb259XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnNoaWZ0VG8gPSBmdW5jdGlvbiBzaGlmdFRvKCkge1xuICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIHVuaXRzID0gbmV3IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgICB1bml0c1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuXG4gICAgICBpZiAodW5pdHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfVxuXG4gICAgICB1bml0cyA9IHVuaXRzLm1hcChmdW5jdGlvbiAodSkge1xuICAgICAgICByZXR1cm4gRHVyYXRpb24ubm9ybWFsaXplVW5pdCh1KTtcbiAgICAgIH0pO1xuICAgICAgdmFyIGJ1aWx0ID0ge30sXG4gICAgICAgICAgYWNjdW11bGF0ZWQgPSB7fSxcbiAgICAgICAgICB2YWxzID0gdGhpcy50b09iamVjdCgpO1xuICAgICAgdmFyIGxhc3RVbml0O1xuICAgICAgbm9ybWFsaXplVmFsdWVzKHRoaXMubWF0cml4LCB2YWxzKTtcblxuICAgICAgZm9yICh2YXIgX2kzID0gMCwgX29yZGVyZWRVbml0czIgPSBvcmRlcmVkVW5pdHM7IF9pMyA8IF9vcmRlcmVkVW5pdHMyLmxlbmd0aDsgX2kzKyspIHtcbiAgICAgICAgdmFyIGsgPSBfb3JkZXJlZFVuaXRzMltfaTNdO1xuXG4gICAgICAgIGlmICh1bml0cy5pbmRleE9mKGspID49IDApIHtcbiAgICAgICAgICBsYXN0VW5pdCA9IGs7XG4gICAgICAgICAgdmFyIG93biA9IDA7IC8vIGFueXRoaW5nIHdlIGhhdmVuJ3QgYm9pbGVkIGRvd24geWV0IHNob3VsZCBnZXQgYm9pbGVkIHRvIHRoaXMgdW5pdFxuXG4gICAgICAgICAgZm9yICh2YXIgYWsgaW4gYWNjdW11bGF0ZWQpIHtcbiAgICAgICAgICAgIG93biArPSB0aGlzLm1hdHJpeFtha11ba10gKiBhY2N1bXVsYXRlZFtha107XG4gICAgICAgICAgICBhY2N1bXVsYXRlZFtha10gPSAwO1xuICAgICAgICAgIH0gLy8gcGx1cyBhbnl0aGluZyB0aGF0J3MgYWxyZWFkeSBpbiB0aGlzIHVuaXRcblxuXG4gICAgICAgICAgaWYgKGlzTnVtYmVyKHZhbHNba10pKSB7XG4gICAgICAgICAgICBvd24gKz0gdmFsc1trXTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YXIgaSA9IE1hdGgudHJ1bmMob3duKTtcbiAgICAgICAgICBidWlsdFtrXSA9IGk7XG4gICAgICAgICAgYWNjdW11bGF0ZWRba10gPSBvd24gLSBpOyAvLyB3ZSdkIGxpa2UgdG8gYWJzb3JiIHRoZXNlIGZyYWN0aW9ucyBpbiBhbm90aGVyIHVuaXRcbiAgICAgICAgICAvLyBwbHVzIGFueXRoaW5nIGZ1cnRoZXIgZG93biB0aGUgY2hhaW4gdGhhdCBzaG91bGQgYmUgcm9sbGVkIHVwIGluIHRvIHRoaXNcblxuICAgICAgICAgIGZvciAodmFyIGRvd24gaW4gdmFscykge1xuICAgICAgICAgICAgaWYgKG9yZGVyZWRVbml0cy5pbmRleE9mKGRvd24pID4gb3JkZXJlZFVuaXRzLmluZGV4T2YoaykpIHtcbiAgICAgICAgICAgICAgY29udmVydCh0aGlzLm1hdHJpeCwgdmFscywgZG93biwgYnVpbHQsIGspO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gLy8gb3RoZXJ3aXNlLCBrZWVwIGl0IGluIHRoZSB3aW5ncyB0byBib2lsIGl0IGxhdGVyXG5cbiAgICAgICAgfSBlbHNlIGlmIChpc051bWJlcih2YWxzW2tdKSkge1xuICAgICAgICAgIGFjY3VtdWxhdGVkW2tdID0gdmFsc1trXTtcbiAgICAgICAgfVxuICAgICAgfSAvLyBhbnl0aGluZyBsZWZ0b3ZlciBiZWNvbWVzIHRoZSBkZWNpbWFsIGZvciB0aGUgbGFzdCB1bml0XG4gICAgICAvLyBsYXN0VW5pdCBtdXN0IGJlIGRlZmluZWQgc2luY2UgdW5pdHMgaXMgbm90IGVtcHR5XG5cblxuICAgICAgZm9yICh2YXIga2V5IGluIGFjY3VtdWxhdGVkKSB7XG4gICAgICAgIGlmIChhY2N1bXVsYXRlZFtrZXldICE9PSAwKSB7XG4gICAgICAgICAgYnVpbHRbbGFzdFVuaXRdICs9IGtleSA9PT0gbGFzdFVuaXQgPyBhY2N1bXVsYXRlZFtrZXldIDogYWNjdW11bGF0ZWRba2V5XSAvIHRoaXMubWF0cml4W2xhc3RVbml0XVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjbG9uZSh0aGlzLCB7XG4gICAgICAgIHZhbHVlczogYnVpbHRcbiAgICAgIH0sIHRydWUpLm5vcm1hbGl6ZSgpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIG5lZ2F0aXZlIG9mIHRoaXMgRHVyYXRpb24uXG4gICAgICogQGV4YW1wbGUgRHVyYXRpb24uZnJvbU9iamVjdCh7IGhvdXJzOiAxLCBzZWNvbmRzOiAzMCB9KS5uZWdhdGUoKS50b09iamVjdCgpIC8vPT4geyBob3VyczogLTEsIHNlY29uZHM6IC0zMCB9XG4gICAgICogQHJldHVybiB7RHVyYXRpb259XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLm5lZ2F0ZSA9IGZ1bmN0aW9uIG5lZ2F0ZSgpIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSByZXR1cm4gdGhpcztcbiAgICAgIHZhciBuZWdhdGVkID0ge307XG5cbiAgICAgIGZvciAodmFyIF9pNCA9IDAsIF9PYmplY3Qka2V5czIgPSBPYmplY3Qua2V5cyh0aGlzLnZhbHVlcyk7IF9pNCA8IF9PYmplY3Qka2V5czIubGVuZ3RoOyBfaTQrKykge1xuICAgICAgICB2YXIgayA9IF9PYmplY3Qka2V5czJbX2k0XTtcbiAgICAgICAgbmVnYXRlZFtrXSA9IC10aGlzLnZhbHVlc1trXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGNsb25lKHRoaXMsIHtcbiAgICAgICAgdmFsdWVzOiBuZWdhdGVkXG4gICAgICB9LCB0cnVlKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogR2V0IHRoZSB5ZWFycy5cbiAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAqL1xuICAgIDtcblxuICAgIC8qKlxuICAgICAqIEVxdWFsaXR5IGNoZWNrXG4gICAgICogVHdvIER1cmF0aW9ucyBhcmUgZXF1YWwgaWZmIHRoZXkgaGF2ZSB0aGUgc2FtZSB1bml0cyBhbmQgdGhlIHNhbWUgdmFsdWVzIGZvciBlYWNoIHVuaXQuXG4gICAgICogQHBhcmFtIHtEdXJhdGlvbn0gb3RoZXJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIF9wcm90by5lcXVhbHMgPSBmdW5jdGlvbiBlcXVhbHMob3RoZXIpIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkIHx8ICFvdGhlci5pc1ZhbGlkKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmxvYy5lcXVhbHMob3RoZXIubG9jKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIF9pNSA9IDAsIF9vcmRlcmVkVW5pdHMzID0gb3JkZXJlZFVuaXRzOyBfaTUgPCBfb3JkZXJlZFVuaXRzMy5sZW5ndGg7IF9pNSsrKSB7XG4gICAgICAgIHZhciB1ID0gX29yZGVyZWRVbml0czNbX2k1XTtcblxuICAgICAgICBpZiAodGhpcy52YWx1ZXNbdV0gIT09IG90aGVyLnZhbHVlc1t1XSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9O1xuXG4gICAgX2NyZWF0ZUNsYXNzKER1cmF0aW9uLCBbe1xuICAgICAga2V5OiBcImxvY2FsZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLmxvYy5sb2NhbGUgOiBudWxsO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIG51bWJlcmluZyBzeXN0ZW0gb2YgYSBEdXJhdGlvbiwgc3VjaCAnYmVuZycuIFRoZSBudW1iZXJpbmcgc3lzdGVtIGlzIHVzZWQgd2hlbiBmb3JtYXR0aW5nIHRoZSBEdXJhdGlvblxuICAgICAgICpcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJudW1iZXJpbmdTeXN0ZW1cIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy5sb2MubnVtYmVyaW5nU3lzdGVtIDogbnVsbDtcbiAgICAgIH1cbiAgICB9LCB7XG4gICAgICBrZXk6IFwieWVhcnNcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy52YWx1ZXMueWVhcnMgfHwgMCA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBxdWFydGVycy5cbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJxdWFydGVyc1wiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLnZhbHVlcy5xdWFydGVycyB8fCAwIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIG1vbnRocy5cbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJtb250aHNcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy52YWx1ZXMubW9udGhzIHx8IDAgOiBOYU47XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgd2Vla3NcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ3ZWVrc1wiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLnZhbHVlcy53ZWVrcyB8fCAwIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIGRheXMuXG4gICAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiZGF5c1wiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLnZhbHVlcy5kYXlzIHx8IDAgOiBOYU47XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgaG91cnMuXG4gICAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaG91cnNcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy52YWx1ZXMuaG91cnMgfHwgMCA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBtaW51dGVzLlxuICAgICAgICogQHR5cGUge251bWJlcn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm1pbnV0ZXNcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy52YWx1ZXMubWludXRlcyB8fCAwIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIHNlY29uZHMuXG4gICAgICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJzZWNvbmRzXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMudmFsdWVzLnNlY29uZHMgfHwgMCA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBtaWxsaXNlY29uZHMuXG4gICAgICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJtaWxsaXNlY29uZHNcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy52YWx1ZXMubWlsbGlzZWNvbmRzIHx8IDAgOiBOYU47XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFJldHVybnMgd2hldGhlciB0aGUgRHVyYXRpb24gaXMgaW52YWxpZC4gSW52YWxpZCBkdXJhdGlvbnMgYXJlIHJldHVybmVkIGJ5IGRpZmYgb3BlcmF0aW9uc1xuICAgICAgICogb24gaW52YWxpZCBEYXRlVGltZXMgb3IgSW50ZXJ2YWxzLlxuICAgICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcImlzVmFsaWRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnZhbGlkID09PSBudWxsO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBSZXR1cm5zIGFuIGVycm9yIGNvZGUgaWYgdGhpcyBEdXJhdGlvbiBiZWNhbWUgaW52YWxpZCwgb3IgbnVsbCBpZiB0aGUgRHVyYXRpb24gaXMgdmFsaWRcbiAgICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcImludmFsaWRSZWFzb25cIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnZhbGlkID8gdGhpcy5pbnZhbGlkLnJlYXNvbiA6IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFJldHVybnMgYW4gZXhwbGFuYXRpb24gb2Ygd2h5IHRoaXMgRHVyYXRpb24gYmVjYW1lIGludmFsaWQsIG9yIG51bGwgaWYgdGhlIER1cmF0aW9uIGlzIHZhbGlkXG4gICAgICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaW52YWxpZEV4cGxhbmF0aW9uXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52YWxpZCA/IHRoaXMuaW52YWxpZC5leHBsYW5hdGlvbiA6IG51bGw7XG4gICAgICB9XG4gICAgfV0pO1xuXG4gICAgcmV0dXJuIER1cmF0aW9uO1xuICB9KCk7XG4gIGZ1bmN0aW9uIGZyaWVuZGx5RHVyYXRpb24oZHVyYXRpb25pc2gpIHtcbiAgICBpZiAoaXNOdW1iZXIoZHVyYXRpb25pc2gpKSB7XG4gICAgICByZXR1cm4gRHVyYXRpb24uZnJvbU1pbGxpcyhkdXJhdGlvbmlzaCk7XG4gICAgfSBlbHNlIGlmIChEdXJhdGlvbi5pc0R1cmF0aW9uKGR1cmF0aW9uaXNoKSkge1xuICAgICAgcmV0dXJuIGR1cmF0aW9uaXNoO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGR1cmF0aW9uaXNoID09PSBcIm9iamVjdFwiKSB7XG4gICAgICByZXR1cm4gRHVyYXRpb24uZnJvbU9iamVjdChkdXJhdGlvbmlzaCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRocm93IG5ldyBJbnZhbGlkQXJndW1lbnRFcnJvcihcIlVua25vd24gZHVyYXRpb24gYXJndW1lbnQgXCIgKyBkdXJhdGlvbmlzaCArIFwiIG9mIHR5cGUgXCIgKyB0eXBlb2YgZHVyYXRpb25pc2gpO1xuICAgIH1cbiAgfVxuXG4gIHZhciBJTlZBTElEJDEgPSBcIkludmFsaWQgSW50ZXJ2YWxcIjsgLy8gY2hlY2tzIGlmIHRoZSBzdGFydCBpcyBlcXVhbCB0byBvciBiZWZvcmUgdGhlIGVuZFxuXG4gIGZ1bmN0aW9uIHZhbGlkYXRlU3RhcnRFbmQoc3RhcnQsIGVuZCkge1xuICAgIGlmICghc3RhcnQgfHwgIXN0YXJ0LmlzVmFsaWQpIHtcbiAgICAgIHJldHVybiBJbnRlcnZhbC5pbnZhbGlkKFwibWlzc2luZyBvciBpbnZhbGlkIHN0YXJ0XCIpO1xuICAgIH0gZWxzZSBpZiAoIWVuZCB8fCAhZW5kLmlzVmFsaWQpIHtcbiAgICAgIHJldHVybiBJbnRlcnZhbC5pbnZhbGlkKFwibWlzc2luZyBvciBpbnZhbGlkIGVuZFwiKTtcbiAgICB9IGVsc2UgaWYgKGVuZCA8IHN0YXJ0KSB7XG4gICAgICByZXR1cm4gSW50ZXJ2YWwuaW52YWxpZChcImVuZCBiZWZvcmUgc3RhcnRcIiwgXCJUaGUgZW5kIG9mIGFuIGludGVydmFsIG11c3QgYmUgYWZ0ZXIgaXRzIHN0YXJ0LCBidXQgeW91IGhhZCBzdGFydD1cIiArIHN0YXJ0LnRvSVNPKCkgKyBcIiBhbmQgZW5kPVwiICsgZW5kLnRvSVNPKCkpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cbiAgLyoqXG4gICAqIEFuIEludGVydmFsIG9iamVjdCByZXByZXNlbnRzIGEgaGFsZi1vcGVuIGludGVydmFsIG9mIHRpbWUsIHdoZXJlIGVhY2ggZW5kcG9pbnQgaXMgYSB7QGxpbmsgRGF0ZVRpbWV9LiBDb25jZXB0dWFsbHksIGl0J3MgYSBjb250YWluZXIgZm9yIHRob3NlIHR3byBlbmRwb2ludHMsIGFjY29tcGFuaWVkIGJ5IG1ldGhvZHMgZm9yIGNyZWF0aW5nLCBwYXJzaW5nLCBpbnRlcnJvZ2F0aW5nLCBjb21wYXJpbmcsIHRyYW5zZm9ybWluZywgYW5kIGZvcm1hdHRpbmcgdGhlbS5cbiAgICpcbiAgICogSGVyZSBpcyBhIGJyaWVmIG92ZXJ2aWV3IG9mIHRoZSBtb3N0IGNvbW1vbmx5IHVzZWQgbWV0aG9kcyBhbmQgZ2V0dGVycyBpbiBJbnRlcnZhbDpcbiAgICpcbiAgICogKiAqKkNyZWF0aW9uKiogVG8gY3JlYXRlIGFuIEludGVydmFsLCB1c2Uge0BsaW5rIGZyb21EYXRlVGltZXN9LCB7QGxpbmsgYWZ0ZXJ9LCB7QGxpbmsgYmVmb3JlfSwgb3Ige0BsaW5rIGZyb21JU099LlxuICAgKiAqICoqQWNjZXNzb3JzKiogVXNlIHtAbGluayBzdGFydH0gYW5kIHtAbGluayBlbmR9IHRvIGdldCB0aGUgc3RhcnQgYW5kIGVuZC5cbiAgICogKiAqKkludGVycm9nYXRpb24qKiBUbyBhbmFseXplIHRoZSBJbnRlcnZhbCwgdXNlIHtAbGluayBjb3VudH0sIHtAbGluayBsZW5ndGh9LCB7QGxpbmsgaGFzU2FtZX0sIHtAbGluayBjb250YWluc30sIHtAbGluayBpc0FmdGVyfSwgb3Ige0BsaW5rIGlzQmVmb3JlfS5cbiAgICogKiAqKlRyYW5zZm9ybWF0aW9uKiogVG8gY3JlYXRlIG90aGVyIEludGVydmFscyBvdXQgb2YgdGhpcyBvbmUsIHVzZSB7QGxpbmsgc2V0fSwge0BsaW5rIHNwbGl0QXR9LCB7QGxpbmsgc3BsaXRCeX0sIHtAbGluayBkaXZpZGVFcXVhbGx5fSwge0BsaW5rIG1lcmdlfSwge0BsaW5rIHhvcn0sIHtAbGluayB1bmlvbn0sIHtAbGluayBpbnRlcnNlY3Rpb259LCBvciB7QGxpbmsgZGlmZmVyZW5jZX0uXG4gICAqICogKipDb21wYXJpc29uKiogVG8gY29tcGFyZSB0aGlzIEludGVydmFsIHRvIGFub3RoZXIgb25lLCB1c2Uge0BsaW5rIGVxdWFsc30sIHtAbGluayBvdmVybGFwc30sIHtAbGluayBhYnV0c1N0YXJ0fSwge0BsaW5rIGFidXRzRW5kfSwge0BsaW5rIGVuZ3VsZnN9XG4gICAqICogKipPdXRwdXQqKiBUbyBjb252ZXJ0IHRoZSBJbnRlcnZhbCBpbnRvIG90aGVyIHJlcHJlc2VudGF0aW9ucywgc2VlIHtAbGluayB0b1N0cmluZ30sIHtAbGluayB0b0lTT30sIHtAbGluayB0b0lTT0RhdGV9LCB7QGxpbmsgdG9JU09UaW1lfSwge0BsaW5rIHRvRm9ybWF0fSwgYW5kIHtAbGluayB0b0R1cmF0aW9ufS5cbiAgICovXG5cblxuICB2YXIgSW50ZXJ2YWwgPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIGZ1bmN0aW9uIEludGVydmFsKGNvbmZpZykge1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuICAgICAgdGhpcy5zID0gY29uZmlnLnN0YXJ0O1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLmUgPSBjb25maWcuZW5kO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLmludmFsaWQgPSBjb25maWcuaW52YWxpZCB8fCBudWxsO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLmlzTHV4b25JbnRlcnZhbCA9IHRydWU7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhbiBpbnZhbGlkIEludGVydmFsLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSByZWFzb24gLSBzaW1wbGUgc3RyaW5nIG9mIHdoeSB0aGlzIEludGVydmFsIGlzIGludmFsaWQuIFNob3VsZCBub3QgY29udGFpbiBwYXJhbWV0ZXJzIG9yIGFueXRoaW5nIGVsc2UgZGF0YS1kZXBlbmRlbnRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW2V4cGxhbmF0aW9uPW51bGxdIC0gbG9uZ2VyIGV4cGxhbmF0aW9uLCBtYXkgaW5jbHVkZSBwYXJhbWV0ZXJzIGFuZCBvdGhlciB1c2VmdWwgZGVidWdnaW5nIGluZm9ybWF0aW9uXG4gICAgICogQHJldHVybiB7SW50ZXJ2YWx9XG4gICAgICovXG5cblxuICAgIEludGVydmFsLmludmFsaWQgPSBmdW5jdGlvbiBpbnZhbGlkKHJlYXNvbiwgZXhwbGFuYXRpb24pIHtcbiAgICAgIGlmIChleHBsYW5hdGlvbiA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGV4cGxhbmF0aW9uID0gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKCFyZWFzb24pIHtcbiAgICAgICAgdGhyb3cgbmV3IEludmFsaWRBcmd1bWVudEVycm9yKFwibmVlZCB0byBzcGVjaWZ5IGEgcmVhc29uIHRoZSBJbnRlcnZhbCBpcyBpbnZhbGlkXCIpO1xuICAgICAgfVxuXG4gICAgICB2YXIgaW52YWxpZCA9IHJlYXNvbiBpbnN0YW5jZW9mIEludmFsaWQgPyByZWFzb24gOiBuZXcgSW52YWxpZChyZWFzb24sIGV4cGxhbmF0aW9uKTtcblxuICAgICAgaWYgKFNldHRpbmdzLnRocm93T25JbnZhbGlkKSB7XG4gICAgICAgIHRocm93IG5ldyBJbnZhbGlkSW50ZXJ2YWxFcnJvcihpbnZhbGlkKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBuZXcgSW50ZXJ2YWwoe1xuICAgICAgICAgIGludmFsaWQ6IGludmFsaWRcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhbiBJbnRlcnZhbCBmcm9tIGEgc3RhcnQgRGF0ZVRpbWUgYW5kIGFuIGVuZCBEYXRlVGltZS4gSW5jbHVzaXZlIG9mIHRoZSBzdGFydCBidXQgbm90IHRoZSBlbmQuXG4gICAgICogQHBhcmFtIHtEYXRlVGltZXxEYXRlfE9iamVjdH0gc3RhcnRcbiAgICAgKiBAcGFyYW0ge0RhdGVUaW1lfERhdGV8T2JqZWN0fSBlbmRcbiAgICAgKiBAcmV0dXJuIHtJbnRlcnZhbH1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzID0gZnVuY3Rpb24gZnJvbURhdGVUaW1lcyhzdGFydCwgZW5kKSB7XG4gICAgICB2YXIgYnVpbHRTdGFydCA9IGZyaWVuZGx5RGF0ZVRpbWUoc3RhcnQpLFxuICAgICAgICAgIGJ1aWx0RW5kID0gZnJpZW5kbHlEYXRlVGltZShlbmQpO1xuICAgICAgdmFyIHZhbGlkYXRlRXJyb3IgPSB2YWxpZGF0ZVN0YXJ0RW5kKGJ1aWx0U3RhcnQsIGJ1aWx0RW5kKTtcblxuICAgICAgaWYgKHZhbGlkYXRlRXJyb3IgPT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gbmV3IEludGVydmFsKHtcbiAgICAgICAgICBzdGFydDogYnVpbHRTdGFydCxcbiAgICAgICAgICBlbmQ6IGJ1aWx0RW5kXG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHZhbGlkYXRlRXJyb3I7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhbiBJbnRlcnZhbCBmcm9tIGEgc3RhcnQgRGF0ZVRpbWUgYW5kIGEgRHVyYXRpb24gdG8gZXh0ZW5kIHRvLlxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV8RGF0ZXxPYmplY3R9IHN0YXJ0XG4gICAgICogQHBhcmFtIHtEdXJhdGlvbnxPYmplY3R8bnVtYmVyfSBkdXJhdGlvbiAtIHRoZSBsZW5ndGggb2YgdGhlIEludGVydmFsLlxuICAgICAqIEByZXR1cm4ge0ludGVydmFsfVxuICAgICAqL1xuICAgIDtcblxuICAgIEludGVydmFsLmFmdGVyID0gZnVuY3Rpb24gYWZ0ZXIoc3RhcnQsIGR1cmF0aW9uKSB7XG4gICAgICB2YXIgZHVyID0gZnJpZW5kbHlEdXJhdGlvbihkdXJhdGlvbiksXG4gICAgICAgICAgZHQgPSBmcmllbmRseURhdGVUaW1lKHN0YXJ0KTtcbiAgICAgIHJldHVybiBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0LCBkdC5wbHVzKGR1cikpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYW4gSW50ZXJ2YWwgZnJvbSBhbiBlbmQgRGF0ZVRpbWUgYW5kIGEgRHVyYXRpb24gdG8gZXh0ZW5kIGJhY2t3YXJkcyB0by5cbiAgICAgKiBAcGFyYW0ge0RhdGVUaW1lfERhdGV8T2JqZWN0fSBlbmRcbiAgICAgKiBAcGFyYW0ge0R1cmF0aW9ufE9iamVjdHxudW1iZXJ9IGR1cmF0aW9uIC0gdGhlIGxlbmd0aCBvZiB0aGUgSW50ZXJ2YWwuXG4gICAgICogQHJldHVybiB7SW50ZXJ2YWx9XG4gICAgICovXG4gICAgO1xuXG4gICAgSW50ZXJ2YWwuYmVmb3JlID0gZnVuY3Rpb24gYmVmb3JlKGVuZCwgZHVyYXRpb24pIHtcbiAgICAgIHZhciBkdXIgPSBmcmllbmRseUR1cmF0aW9uKGR1cmF0aW9uKSxcbiAgICAgICAgICBkdCA9IGZyaWVuZGx5RGF0ZVRpbWUoZW5kKTtcbiAgICAgIHJldHVybiBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0Lm1pbnVzKGR1ciksIGR0KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGFuIEludGVydmFsIGZyb20gYW4gSVNPIDg2MDEgc3RyaW5nLlxuICAgICAqIEFjY2VwdHMgYDxzdGFydD4vPGVuZD5gLCBgPHN0YXJ0Pi88ZHVyYXRpb24+YCwgYW5kIGA8ZHVyYXRpb24+LzxlbmQ+YCBmb3JtYXRzLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0IC0gdGhlIElTTyBzdHJpbmcgdG8gcGFyc2VcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gW29wdHNdIC0gb3B0aW9ucyB0byBwYXNzIHtAbGluayBEYXRlVGltZS5mcm9tSVNPfSBhbmQgb3B0aW9uYWxseSB7QGxpbmsgRHVyYXRpb24uZnJvbUlTT31cbiAgICAgKiBAc2VlIGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0lTT184NjAxI1RpbWVfaW50ZXJ2YWxzXG4gICAgICogQHJldHVybiB7SW50ZXJ2YWx9XG4gICAgICovXG4gICAgO1xuXG4gICAgSW50ZXJ2YWwuZnJvbUlTTyA9IGZ1bmN0aW9uIGZyb21JU08odGV4dCwgb3B0cykge1xuICAgICAgdmFyIF9zcGxpdCA9ICh0ZXh0IHx8IFwiXCIpLnNwbGl0KFwiL1wiLCAyKSxcbiAgICAgICAgICBzID0gX3NwbGl0WzBdLFxuICAgICAgICAgIGUgPSBfc3BsaXRbMV07XG5cbiAgICAgIGlmIChzICYmIGUpIHtcbiAgICAgICAgdmFyIHN0YXJ0ID0gRGF0ZVRpbWUuZnJvbUlTTyhzLCBvcHRzKSxcbiAgICAgICAgICAgIGVuZCA9IERhdGVUaW1lLmZyb21JU08oZSwgb3B0cyk7XG5cbiAgICAgICAgaWYgKHN0YXJ0LmlzVmFsaWQgJiYgZW5kLmlzVmFsaWQpIHtcbiAgICAgICAgICByZXR1cm4gSW50ZXJ2YWwuZnJvbURhdGVUaW1lcyhzdGFydCwgZW5kKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChzdGFydC5pc1ZhbGlkKSB7XG4gICAgICAgICAgdmFyIGR1ciA9IER1cmF0aW9uLmZyb21JU08oZSwgb3B0cyk7XG5cbiAgICAgICAgICBpZiAoZHVyLmlzVmFsaWQpIHtcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnZhbC5hZnRlcihzdGFydCwgZHVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAoZW5kLmlzVmFsaWQpIHtcbiAgICAgICAgICB2YXIgX2R1ciA9IER1cmF0aW9uLmZyb21JU08ocywgb3B0cyk7XG5cbiAgICAgICAgICBpZiAoX2R1ci5pc1ZhbGlkKSB7XG4gICAgICAgICAgICByZXR1cm4gSW50ZXJ2YWwuYmVmb3JlKGVuZCwgX2R1cik7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBJbnRlcnZhbC5pbnZhbGlkKFwidW5wYXJzYWJsZVwiLCBcInRoZSBpbnB1dCBcXFwiXCIgKyB0ZXh0ICsgXCJcXFwiIGNhbid0IGJlIHBhcnNlZCBhc0lTTyA4NjAxXCIpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDaGVjayBpZiBhbiBvYmplY3QgaXMgYW4gSW50ZXJ2YWwuIFdvcmtzIGFjcm9zcyBjb250ZXh0IGJvdW5kYXJpZXNcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gb1xuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgO1xuXG4gICAgSW50ZXJ2YWwuaXNJbnRlcnZhbCA9IGZ1bmN0aW9uIGlzSW50ZXJ2YWwobykge1xuICAgICAgcmV0dXJuIG8gJiYgby5pc0x1eG9uSW50ZXJ2YWwgfHwgZmFsc2U7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIHN0YXJ0IG9mIHRoZSBJbnRlcnZhbFxuICAgICAqIEB0eXBlIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICB2YXIgX3Byb3RvID0gSW50ZXJ2YWwucHJvdG90eXBlO1xuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgbGVuZ3RoIG9mIHRoZSBJbnRlcnZhbCBpbiB0aGUgc3BlY2lmaWVkIHVuaXQuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVuaXQgLSB0aGUgdW5pdCAoc3VjaCBhcyAnaG91cnMnIG9yICdkYXlzJykgdG8gcmV0dXJuIHRoZSBsZW5ndGggaW4uXG4gICAgICogQHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuICAgIF9wcm90by5sZW5ndGggPSBmdW5jdGlvbiBsZW5ndGgodW5pdCkge1xuICAgICAgaWYgKHVuaXQgPT09IHZvaWQgMCkge1xuICAgICAgICB1bml0ID0gXCJtaWxsaXNlY29uZHNcIjtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMudG9EdXJhdGlvbi5hcHBseSh0aGlzLCBbdW5pdF0pLmdldCh1bml0KSA6IE5hTjtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY291bnQgb2YgbWludXRlcywgaG91cnMsIGRheXMsIG1vbnRocywgb3IgeWVhcnMgaW5jbHVkZWQgaW4gdGhlIEludGVydmFsLCBldmVuIGluIHBhcnQuXG4gICAgICogVW5saWtlIHtAbGluayBsZW5ndGh9IHRoaXMgY291bnRzIHNlY3Rpb25zIG9mIHRoZSBjYWxlbmRhciwgbm90IHBlcmlvZHMgb2YgdGltZSwgZS5nLiBzcGVjaWZ5aW5nICdkYXknXG4gICAgICogYXNrcyAnd2hhdCBkYXRlcyBhcmUgaW5jbHVkZWQgaW4gdGhpcyBpbnRlcnZhbD8nLCBub3QgJ2hvdyBtYW55IGRheXMgbG9uZyBpcyB0aGlzIGludGVydmFsPydcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW3VuaXQ9J21pbGxpc2Vjb25kcyddIC0gdGhlIHVuaXQgb2YgdGltZSB0byBjb3VudC5cbiAgICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLmNvdW50ID0gZnVuY3Rpb24gY291bnQodW5pdCkge1xuICAgICAgaWYgKHVuaXQgPT09IHZvaWQgMCkge1xuICAgICAgICB1bml0ID0gXCJtaWxsaXNlY29uZHNcIjtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBOYU47XG4gICAgICB2YXIgc3RhcnQgPSB0aGlzLnN0YXJ0LnN0YXJ0T2YodW5pdCksXG4gICAgICAgICAgZW5kID0gdGhpcy5lbmQuc3RhcnRPZih1bml0KTtcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKGVuZC5kaWZmKHN0YXJ0LCB1bml0KS5nZXQodW5pdCkpICsgMTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyB3aGV0aGVyIHRoaXMgSW50ZXJ2YWwncyBzdGFydCBhbmQgZW5kIGFyZSBib3RoIGluIHRoZSBzYW1lIHVuaXQgb2YgdGltZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1bml0IC0gdGhlIHVuaXQgb2YgdGltZSB0byBjaGVjayBzYW1lbmVzcyBvblxuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLmhhc1NhbWUgPSBmdW5jdGlvbiBoYXNTYW1lKHVuaXQpIHtcbiAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLmUubWludXMoMSkuaGFzU2FtZSh0aGlzLnMsIHVuaXQpIDogZmFsc2U7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiB3aGV0aGVyIHRoaXMgSW50ZXJ2YWwgaGFzIHRoZSBzYW1lIHN0YXJ0IGFuZCBlbmQgRGF0ZVRpbWVzLlxuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLmlzRW1wdHkgPSBmdW5jdGlvbiBpc0VtcHR5KCkge1xuICAgICAgcmV0dXJuIHRoaXMucy52YWx1ZU9mKCkgPT09IHRoaXMuZS52YWx1ZU9mKCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiB3aGV0aGVyIHRoaXMgSW50ZXJ2YWwncyBzdGFydCBpcyBhZnRlciB0aGUgc3BlY2lmaWVkIERhdGVUaW1lLlxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IGRhdGVUaW1lXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uaXNBZnRlciA9IGZ1bmN0aW9uIGlzQWZ0ZXIoZGF0ZVRpbWUpIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSByZXR1cm4gZmFsc2U7XG4gICAgICByZXR1cm4gdGhpcy5zID4gZGF0ZVRpbWU7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiB3aGV0aGVyIHRoaXMgSW50ZXJ2YWwncyBlbmQgaXMgYmVmb3JlIHRoZSBzcGVjaWZpZWQgRGF0ZVRpbWUuXG4gICAgICogQHBhcmFtIHtEYXRlVGltZX0gZGF0ZVRpbWVcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5pc0JlZm9yZSA9IGZ1bmN0aW9uIGlzQmVmb3JlKGRhdGVUaW1lKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIGZhbHNlO1xuICAgICAgcmV0dXJuIHRoaXMuZSA8PSBkYXRlVGltZTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHdoZXRoZXIgdGhpcyBJbnRlcnZhbCBjb250YWlucyB0aGUgc3BlY2lmaWVkIERhdGVUaW1lLlxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IGRhdGVUaW1lXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uY29udGFpbnMgPSBmdW5jdGlvbiBjb250YWlucyhkYXRlVGltZSkge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBmYWxzZTtcbiAgICAgIHJldHVybiB0aGlzLnMgPD0gZGF0ZVRpbWUgJiYgdGhpcy5lID4gZGF0ZVRpbWU7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFwiU2V0c1wiIHRoZSBzdGFydCBhbmQvb3IgZW5kIGRhdGVzLiBSZXR1cm5zIGEgbmV3bHktY29uc3RydWN0ZWQgSW50ZXJ2YWwuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IHZhbHVlcyAtIHRoZSB2YWx1ZXMgdG8gc2V0XG4gICAgICogQHBhcmFtIHtEYXRlVGltZX0gdmFsdWVzLnN0YXJ0IC0gdGhlIHN0YXJ0aW5nIERhdGVUaW1lXG4gICAgICogQHBhcmFtIHtEYXRlVGltZX0gdmFsdWVzLmVuZCAtIHRoZSBlbmRpbmcgRGF0ZVRpbWVcbiAgICAgKiBAcmV0dXJuIHtJbnRlcnZhbH1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uc2V0ID0gZnVuY3Rpb24gc2V0KF90ZW1wKSB7XG4gICAgICB2YXIgX3JlZiA9IF90ZW1wID09PSB2b2lkIDAgPyB7fSA6IF90ZW1wLFxuICAgICAgICAgIHN0YXJ0ID0gX3JlZi5zdGFydCxcbiAgICAgICAgICBlbmQgPSBfcmVmLmVuZDtcblxuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuICAgICAgcmV0dXJuIEludGVydmFsLmZyb21EYXRlVGltZXMoc3RhcnQgfHwgdGhpcy5zLCBlbmQgfHwgdGhpcy5lKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogU3BsaXQgdGhpcyBJbnRlcnZhbCBhdCBlYWNoIG9mIHRoZSBzcGVjaWZpZWQgRGF0ZVRpbWVzXG4gICAgICogQHBhcmFtIHsuLi5bRGF0ZVRpbWVdfSBkYXRlVGltZXMgLSB0aGUgdW5pdCBvZiB0aW1lIHRvIGNvdW50LlxuICAgICAqIEByZXR1cm4ge1tJbnRlcnZhbF19XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnNwbGl0QXQgPSBmdW5jdGlvbiBzcGxpdEF0KCkge1xuICAgICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBbXTtcblxuICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGRhdGVUaW1lcyA9IG5ldyBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgZGF0ZVRpbWVzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgICAgfVxuXG4gICAgICB2YXIgc29ydGVkID0gZGF0ZVRpbWVzLm1hcChmcmllbmRseURhdGVUaW1lKS5maWx0ZXIoZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzLmNvbnRhaW5zKGQpO1xuICAgICAgfSkuc29ydCgpLFxuICAgICAgICAgIHJlc3VsdHMgPSBbXTtcbiAgICAgIHZhciBzID0gdGhpcy5zLFxuICAgICAgICAgIGkgPSAwO1xuXG4gICAgICB3aGlsZSAocyA8IHRoaXMuZSkge1xuICAgICAgICB2YXIgYWRkZWQgPSBzb3J0ZWRbaV0gfHwgdGhpcy5lLFxuICAgICAgICAgICAgbmV4dCA9ICthZGRlZCA+ICt0aGlzLmUgPyB0aGlzLmUgOiBhZGRlZDtcbiAgICAgICAgcmVzdWx0cy5wdXNoKEludGVydmFsLmZyb21EYXRlVGltZXMocywgbmV4dCkpO1xuICAgICAgICBzID0gbmV4dDtcbiAgICAgICAgaSArPSAxO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmVzdWx0cztcbiAgICB9XG4gICAgLyoqXG4gICAgICogU3BsaXQgdGhpcyBJbnRlcnZhbCBpbnRvIHNtYWxsZXIgSW50ZXJ2YWxzLCBlYWNoIG9mIHRoZSBzcGVjaWZpZWQgbGVuZ3RoLlxuICAgICAqIExlZnQgb3ZlciB0aW1lIGlzIGdyb3VwZWQgaW50byBhIHNtYWxsZXIgaW50ZXJ2YWxcbiAgICAgKiBAcGFyYW0ge0R1cmF0aW9ufE9iamVjdHxudW1iZXJ9IGR1cmF0aW9uIC0gVGhlIGxlbmd0aCBvZiBlYWNoIHJlc3VsdGluZyBpbnRlcnZhbC5cbiAgICAgKiBAcmV0dXJuIHtbSW50ZXJ2YWxdfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5zcGxpdEJ5ID0gZnVuY3Rpb24gc3BsaXRCeShkdXJhdGlvbikge1xuICAgICAgdmFyIGR1ciA9IGZyaWVuZGx5RHVyYXRpb24oZHVyYXRpb24pO1xuXG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCB8fCAhZHVyLmlzVmFsaWQgfHwgZHVyLmFzKFwibWlsbGlzZWNvbmRzXCIpID09PSAwKSB7XG4gICAgICAgIHJldHVybiBbXTtcbiAgICAgIH1cblxuICAgICAgdmFyIHMgPSB0aGlzLnMsXG4gICAgICAgICAgYWRkZWQsXG4gICAgICAgICAgbmV4dDtcbiAgICAgIHZhciByZXN1bHRzID0gW107XG5cbiAgICAgIHdoaWxlIChzIDwgdGhpcy5lKSB7XG4gICAgICAgIGFkZGVkID0gcy5wbHVzKGR1cik7XG4gICAgICAgIG5leHQgPSArYWRkZWQgPiArdGhpcy5lID8gdGhpcy5lIDogYWRkZWQ7XG4gICAgICAgIHJlc3VsdHMucHVzaChJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKHMsIG5leHQpKTtcbiAgICAgICAgcyA9IG5leHQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiByZXN1bHRzO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBTcGxpdCB0aGlzIEludGVydmFsIGludG8gdGhlIHNwZWNpZmllZCBudW1iZXIgb2Ygc21hbGxlciBpbnRlcnZhbHMuXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG51bWJlck9mUGFydHMgLSBUaGUgbnVtYmVyIG9mIEludGVydmFscyB0byBkaXZpZGUgdGhlIEludGVydmFsIGludG8uXG4gICAgICogQHJldHVybiB7W0ludGVydmFsXX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZGl2aWRlRXF1YWxseSA9IGZ1bmN0aW9uIGRpdmlkZUVxdWFsbHkobnVtYmVyT2ZQYXJ0cykge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBbXTtcbiAgICAgIHJldHVybiB0aGlzLnNwbGl0QnkodGhpcy5sZW5ndGgoKSAvIG51bWJlck9mUGFydHMpLnNsaWNlKDAsIG51bWJlck9mUGFydHMpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gd2hldGhlciB0aGlzIEludGVydmFsIG92ZXJsYXBzIHdpdGggdGhlIHNwZWNpZmllZCBJbnRlcnZhbFxuICAgICAqIEBwYXJhbSB7SW50ZXJ2YWx9IG90aGVyXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8ub3ZlcmxhcHMgPSBmdW5jdGlvbiBvdmVybGFwcyhvdGhlcikge1xuICAgICAgcmV0dXJuIHRoaXMuZSA+IG90aGVyLnMgJiYgdGhpcy5zIDwgb3RoZXIuZTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHdoZXRoZXIgdGhpcyBJbnRlcnZhbCdzIGVuZCBpcyBhZGphY2VudCB0byB0aGUgc3BlY2lmaWVkIEludGVydmFsJ3Mgc3RhcnQuXG4gICAgICogQHBhcmFtIHtJbnRlcnZhbH0gb3RoZXJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5hYnV0c1N0YXJ0ID0gZnVuY3Rpb24gYWJ1dHNTdGFydChvdGhlcikge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBmYWxzZTtcbiAgICAgIHJldHVybiArdGhpcy5lID09PSArb3RoZXIucztcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHdoZXRoZXIgdGhpcyBJbnRlcnZhbCdzIHN0YXJ0IGlzIGFkamFjZW50IHRvIHRoZSBzcGVjaWZpZWQgSW50ZXJ2YWwncyBlbmQuXG4gICAgICogQHBhcmFtIHtJbnRlcnZhbH0gb3RoZXJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5hYnV0c0VuZCA9IGZ1bmN0aW9uIGFidXRzRW5kKG90aGVyKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIGZhbHNlO1xuICAgICAgcmV0dXJuICtvdGhlci5lID09PSArdGhpcy5zO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gd2hldGhlciB0aGlzIEludGVydmFsIGVuZ3VsZnMgdGhlIHN0YXJ0IGFuZCBlbmQgb2YgdGhlIHNwZWNpZmllZCBJbnRlcnZhbC5cbiAgICAgKiBAcGFyYW0ge0ludGVydmFsfSBvdGhlclxuICAgICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLmVuZ3VsZnMgPSBmdW5jdGlvbiBlbmd1bGZzKG90aGVyKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIGZhbHNlO1xuICAgICAgcmV0dXJuIHRoaXMucyA8PSBvdGhlci5zICYmIHRoaXMuZSA+PSBvdGhlci5lO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gd2hldGhlciB0aGlzIEludGVydmFsIGhhcyB0aGUgc2FtZSBzdGFydCBhbmQgZW5kIGFzIHRoZSBzcGVjaWZpZWQgSW50ZXJ2YWwuXG4gICAgICogQHBhcmFtIHtJbnRlcnZhbH0gb3RoZXJcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5lcXVhbHMgPSBmdW5jdGlvbiBlcXVhbHMob3RoZXIpIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkIHx8ICFvdGhlci5pc1ZhbGlkKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMucy5lcXVhbHMob3RoZXIucykgJiYgdGhpcy5lLmVxdWFscyhvdGhlci5lKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIGFuIEludGVydmFsIHJlcHJlc2VudGluZyB0aGUgaW50ZXJzZWN0aW9uIG9mIHRoaXMgSW50ZXJ2YWwgYW5kIHRoZSBzcGVjaWZpZWQgSW50ZXJ2YWwuXG4gICAgICogU3BlY2lmaWNhbGx5LCB0aGUgcmVzdWx0aW5nIEludGVydmFsIGhhcyB0aGUgbWF4aW11bSBzdGFydCB0aW1lIGFuZCB0aGUgbWluaW11bSBlbmQgdGltZSBvZiB0aGUgdHdvIEludGVydmFscy5cbiAgICAgKiBSZXR1cm5zIG51bGwgaWYgdGhlIGludGVyc2VjdGlvbiBpcyBlbXB0eSwgbWVhbmluZywgdGhlIGludGVydmFscyBkb24ndCBpbnRlcnNlY3QuXG4gICAgICogQHBhcmFtIHtJbnRlcnZhbH0gb3RoZXJcbiAgICAgKiBAcmV0dXJuIHtJbnRlcnZhbH1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uaW50ZXJzZWN0aW9uID0gZnVuY3Rpb24gaW50ZXJzZWN0aW9uKG90aGVyKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHRoaXM7XG4gICAgICB2YXIgcyA9IHRoaXMucyA+IG90aGVyLnMgPyB0aGlzLnMgOiBvdGhlci5zLFxuICAgICAgICAgIGUgPSB0aGlzLmUgPCBvdGhlci5lID8gdGhpcy5lIDogb3RoZXIuZTtcblxuICAgICAgaWYgKHMgPiBlKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIEludGVydmFsLmZyb21EYXRlVGltZXMocywgZSk7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiBhbiBJbnRlcnZhbCByZXByZXNlbnRpbmcgdGhlIHVuaW9uIG9mIHRoaXMgSW50ZXJ2YWwgYW5kIHRoZSBzcGVjaWZpZWQgSW50ZXJ2YWwuXG4gICAgICogU3BlY2lmaWNhbGx5LCB0aGUgcmVzdWx0aW5nIEludGVydmFsIGhhcyB0aGUgbWluaW11bSBzdGFydCB0aW1lIGFuZCB0aGUgbWF4aW11bSBlbmQgdGltZSBvZiB0aGUgdHdvIEludGVydmFscy5cbiAgICAgKiBAcGFyYW0ge0ludGVydmFsfSBvdGhlclxuICAgICAqIEByZXR1cm4ge0ludGVydmFsfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by51bmlvbiA9IGZ1bmN0aW9uIHVuaW9uKG90aGVyKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHRoaXM7XG4gICAgICB2YXIgcyA9IHRoaXMucyA8IG90aGVyLnMgPyB0aGlzLnMgOiBvdGhlci5zLFxuICAgICAgICAgIGUgPSB0aGlzLmUgPiBvdGhlci5lID8gdGhpcy5lIDogb3RoZXIuZTtcbiAgICAgIHJldHVybiBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKHMsIGUpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBNZXJnZSBhbiBhcnJheSBvZiBJbnRlcnZhbHMgaW50byBhIGVxdWl2YWxlbnQgbWluaW1hbCBzZXQgb2YgSW50ZXJ2YWxzLlxuICAgICAqIENvbWJpbmVzIG92ZXJsYXBwaW5nIGFuZCBhZGphY2VudCBJbnRlcnZhbHMuXG4gICAgICogQHBhcmFtIHtbSW50ZXJ2YWxdfSBpbnRlcnZhbHNcbiAgICAgKiBAcmV0dXJuIHtbSW50ZXJ2YWxdfVxuICAgICAqL1xuICAgIDtcblxuICAgIEludGVydmFsLm1lcmdlID0gZnVuY3Rpb24gbWVyZ2UoaW50ZXJ2YWxzKSB7XG4gICAgICB2YXIgX2ludGVydmFscyRzb3J0JHJlZHVjID0gaW50ZXJ2YWxzLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgICAgcmV0dXJuIGEucyAtIGIucztcbiAgICAgIH0pLnJlZHVjZShmdW5jdGlvbiAoX3JlZjIsIGl0ZW0pIHtcbiAgICAgICAgdmFyIHNvZmFyID0gX3JlZjJbMF0sXG4gICAgICAgICAgICBjdXJyZW50ID0gX3JlZjJbMV07XG5cbiAgICAgICAgaWYgKCFjdXJyZW50KSB7XG4gICAgICAgICAgcmV0dXJuIFtzb2ZhciwgaXRlbV07XG4gICAgICAgIH0gZWxzZSBpZiAoY3VycmVudC5vdmVybGFwcyhpdGVtKSB8fCBjdXJyZW50LmFidXRzU3RhcnQoaXRlbSkpIHtcbiAgICAgICAgICByZXR1cm4gW3NvZmFyLCBjdXJyZW50LnVuaW9uKGl0ZW0pXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gW3NvZmFyLmNvbmNhdChbY3VycmVudF0pLCBpdGVtXTtcbiAgICAgICAgfVxuICAgICAgfSwgW1tdLCBudWxsXSksXG4gICAgICAgICAgZm91bmQgPSBfaW50ZXJ2YWxzJHNvcnQkcmVkdWNbMF0sXG4gICAgICAgICAgZmluYWwgPSBfaW50ZXJ2YWxzJHNvcnQkcmVkdWNbMV07XG5cbiAgICAgIGlmIChmaW5hbCkge1xuICAgICAgICBmb3VuZC5wdXNoKGZpbmFsKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZvdW5kO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gYW4gYXJyYXkgb2YgSW50ZXJ2YWxzIHJlcHJlc2VudGluZyB0aGUgc3BhbnMgb2YgdGltZSB0aGF0IG9ubHkgYXBwZWFyIGluIG9uZSBvZiB0aGUgc3BlY2lmaWVkIEludGVydmFscy5cbiAgICAgKiBAcGFyYW0ge1tJbnRlcnZhbF19IGludGVydmFsc1xuICAgICAqIEByZXR1cm4ge1tJbnRlcnZhbF19XG4gICAgICovXG4gICAgO1xuXG4gICAgSW50ZXJ2YWwueG9yID0gZnVuY3Rpb24geG9yKGludGVydmFscykge1xuICAgICAgdmFyIF9BcnJheSRwcm90b3R5cGU7XG5cbiAgICAgIHZhciBzdGFydCA9IG51bGwsXG4gICAgICAgICAgY3VycmVudENvdW50ID0gMDtcblxuICAgICAgdmFyIHJlc3VsdHMgPSBbXSxcbiAgICAgICAgICBlbmRzID0gaW50ZXJ2YWxzLm1hcChmdW5jdGlvbiAoaSkge1xuICAgICAgICByZXR1cm4gW3tcbiAgICAgICAgICB0aW1lOiBpLnMsXG4gICAgICAgICAgdHlwZTogXCJzXCJcbiAgICAgICAgfSwge1xuICAgICAgICAgIHRpbWU6IGkuZSxcbiAgICAgICAgICB0eXBlOiBcImVcIlxuICAgICAgICB9XTtcbiAgICAgIH0pLFxuICAgICAgICAgIGZsYXR0ZW5lZCA9IChfQXJyYXkkcHJvdG90eXBlID0gQXJyYXkucHJvdG90eXBlKS5jb25jYXQuYXBwbHkoX0FycmF5JHByb3RvdHlwZSwgZW5kcyksXG4gICAgICAgICAgYXJyID0gZmxhdHRlbmVkLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgICAgcmV0dXJuIGEudGltZSAtIGIudGltZTtcbiAgICAgIH0pO1xuXG4gICAgICBmb3IgKHZhciBfaXRlcmF0b3IgPSBhcnIsIF9pc0FycmF5ID0gQXJyYXkuaXNBcnJheShfaXRlcmF0b3IpLCBfaSA9IDAsIF9pdGVyYXRvciA9IF9pc0FycmF5ID8gX2l0ZXJhdG9yIDogX2l0ZXJhdG9yW1N5bWJvbC5pdGVyYXRvcl0oKTs7KSB7XG4gICAgICAgIHZhciBfcmVmMztcblxuICAgICAgICBpZiAoX2lzQXJyYXkpIHtcbiAgICAgICAgICBpZiAoX2kgPj0gX2l0ZXJhdG9yLmxlbmd0aCkgYnJlYWs7XG4gICAgICAgICAgX3JlZjMgPSBfaXRlcmF0b3JbX2krK107XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX2kgPSBfaXRlcmF0b3IubmV4dCgpO1xuICAgICAgICAgIGlmIChfaS5kb25lKSBicmVhaztcbiAgICAgICAgICBfcmVmMyA9IF9pLnZhbHVlO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGkgPSBfcmVmMztcbiAgICAgICAgY3VycmVudENvdW50ICs9IGkudHlwZSA9PT0gXCJzXCIgPyAxIDogLTE7XG5cbiAgICAgICAgaWYgKGN1cnJlbnRDb3VudCA9PT0gMSkge1xuICAgICAgICAgIHN0YXJ0ID0gaS50aW1lO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmIChzdGFydCAmJiArc3RhcnQgIT09ICtpLnRpbWUpIHtcbiAgICAgICAgICAgIHJlc3VsdHMucHVzaChJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKHN0YXJ0LCBpLnRpbWUpKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBzdGFydCA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIEludGVydmFsLm1lcmdlKHJlc3VsdHMpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gYW4gSW50ZXJ2YWwgcmVwcmVzZW50aW5nIHRoZSBzcGFuIG9mIHRpbWUgaW4gdGhpcyBJbnRlcnZhbCB0aGF0IGRvZXNuJ3Qgb3ZlcmxhcCB3aXRoIGFueSBvZiB0aGUgc3BlY2lmaWVkIEludGVydmFscy5cbiAgICAgKiBAcGFyYW0gey4uLkludGVydmFsfSBpbnRlcnZhbHNcbiAgICAgKiBAcmV0dXJuIHtbSW50ZXJ2YWxdfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5kaWZmZXJlbmNlID0gZnVuY3Rpb24gZGlmZmVyZW5jZSgpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICBmb3IgKHZhciBfbGVuMiA9IGFyZ3VtZW50cy5sZW5ndGgsIGludGVydmFscyA9IG5ldyBBcnJheShfbGVuMiksIF9rZXkyID0gMDsgX2tleTIgPCBfbGVuMjsgX2tleTIrKykge1xuICAgICAgICBpbnRlcnZhbHNbX2tleTJdID0gYXJndW1lbnRzW19rZXkyXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIEludGVydmFsLnhvcihbdGhpc10uY29uY2F0KGludGVydmFscykpLm1hcChmdW5jdGlvbiAoaSkge1xuICAgICAgICByZXR1cm4gX3RoaXMyLmludGVyc2VjdGlvbihpKTtcbiAgICAgIH0pLmZpbHRlcihmdW5jdGlvbiAoaSkge1xuICAgICAgICByZXR1cm4gaSAmJiAhaS5pc0VtcHR5KCk7XG4gICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIEludGVydmFsIGFwcHJvcHJpYXRlIGZvciBkZWJ1Z2dpbmcuXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBJTlZBTElEJDE7XG4gICAgICByZXR1cm4gXCJbXCIgKyB0aGlzLnMudG9JU08oKSArIFwiIFxcdTIwMTMgXCIgKyB0aGlzLmUudG9JU08oKSArIFwiKVwiO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIElTTyA4NjAxLWNvbXBsaWFudCBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBJbnRlcnZhbC5cbiAgICAgKiBAc2VlIGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0lTT184NjAxI1RpbWVfaW50ZXJ2YWxzXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBUaGUgc2FtZSBvcHRpb25zIGFzIHtAbGluayBEYXRlVGltZS50b0lTT31cbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvSVNPID0gZnVuY3Rpb24gdG9JU08ob3B0cykge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBJTlZBTElEJDE7XG4gICAgICByZXR1cm4gdGhpcy5zLnRvSVNPKG9wdHMpICsgXCIvXCIgKyB0aGlzLmUudG9JU08ob3B0cyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gSVNPIDg2MDEtY29tcGxpYW50IHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiBkYXRlIG9mIHRoaXMgSW50ZXJ2YWwuXG4gICAgICogVGhlIHRpbWUgY29tcG9uZW50cyBhcmUgaWdub3JlZC5cbiAgICAgKiBAc2VlIGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0lTT184NjAxI1RpbWVfaW50ZXJ2YWxzXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b0lTT0RhdGUgPSBmdW5jdGlvbiB0b0lTT0RhdGUoKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIElOVkFMSUQkMTtcbiAgICAgIHJldHVybiB0aGlzLnMudG9JU09EYXRlKCkgKyBcIi9cIiArIHRoaXMuZS50b0lTT0RhdGUoKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhbiBJU08gODYwMS1jb21wbGlhbnQgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRpbWUgb2YgdGhpcyBJbnRlcnZhbC5cbiAgICAgKiBUaGUgZGF0ZSBjb21wb25lbnRzIGFyZSBpZ25vcmVkLlxuICAgICAqIEBzZWUgaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvSVNPXzg2MDEjVGltZV9pbnRlcnZhbHNcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIFRoZSBzYW1lIG9wdGlvbnMgYXMge0BsaW5rIERhdGVUaW1lLnRvSVNPfVxuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9JU09UaW1lID0gZnVuY3Rpb24gdG9JU09UaW1lKG9wdHMpIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSByZXR1cm4gSU5WQUxJRCQxO1xuICAgICAgcmV0dXJuIHRoaXMucy50b0lTT1RpbWUob3B0cykgKyBcIi9cIiArIHRoaXMuZS50b0lTT1RpbWUob3B0cyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYSBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBJbnRlcnZhbCBmb3JtYXR0ZWQgYWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpZWQgZm9ybWF0IHN0cmluZy5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZGF0ZUZvcm1hdCAtIHRoZSBmb3JtYXQgc3RyaW5nLiBUaGlzIHN0cmluZyBmb3JtYXRzIHRoZSBzdGFydCBhbmQgZW5kIHRpbWUuIFNlZSB7QGxpbmsgRGF0ZVRpbWUudG9Gb3JtYXR9IGZvciBkZXRhaWxzLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9uc1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5zZXBhcmF0b3IgPSAgJyDigJMgJ10gLSBhIHNlcGFyYXRvciB0byBwbGFjZSBiZXR3ZWVuIHRoZSBzdGFydCBhbmQgZW5kIHJlcHJlc2VudGF0aW9uc1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9Gb3JtYXQgPSBmdW5jdGlvbiB0b0Zvcm1hdChkYXRlRm9ybWF0LCBfdGVtcDIpIHtcbiAgICAgIHZhciBfcmVmNCA9IF90ZW1wMiA9PT0gdm9pZCAwID8ge30gOiBfdGVtcDIsXG4gICAgICAgICAgX3JlZjQkc2VwYXJhdG9yID0gX3JlZjQuc2VwYXJhdG9yLFxuICAgICAgICAgIHNlcGFyYXRvciA9IF9yZWY0JHNlcGFyYXRvciA9PT0gdm9pZCAwID8gXCIg4oCTIFwiIDogX3JlZjQkc2VwYXJhdG9yO1xuXG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIElOVkFMSUQkMTtcbiAgICAgIHJldHVybiBcIlwiICsgdGhpcy5zLnRvRm9ybWF0KGRhdGVGb3JtYXQpICsgc2VwYXJhdG9yICsgdGhpcy5lLnRvRm9ybWF0KGRhdGVGb3JtYXQpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gYSBEdXJhdGlvbiByZXByZXNlbnRpbmcgdGhlIHRpbWUgc3Bhbm5lZCBieSB0aGlzIGludGVydmFsLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfHN0cmluZ1tdfSBbdW5pdD1bJ21pbGxpc2Vjb25kcyddXSAtIHRoZSB1bml0IG9yIHVuaXRzIChzdWNoIGFzICdob3Vycycgb3IgJ2RheXMnKSB0byBpbmNsdWRlIGluIHRoZSBkdXJhdGlvbi5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnMgdGhhdCBhZmZlY3QgdGhlIGNyZWF0aW9uIG9mIHRoZSBEdXJhdGlvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5jb252ZXJzaW9uQWNjdXJhY3k9J2Nhc3VhbCddIC0gdGhlIGNvbnZlcnNpb24gc3lzdGVtIHRvIHVzZVxuICAgICAqIEBleGFtcGxlIEludGVydmFsLmZyb21EYXRlVGltZXMoZHQxLCBkdDIpLnRvRHVyYXRpb24oKS50b09iamVjdCgpIC8vPT4geyBtaWxsaXNlY29uZHM6IDg4NDg5MjU3IH1cbiAgICAgKiBAZXhhbXBsZSBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0MSwgZHQyKS50b0R1cmF0aW9uKCdkYXlzJykudG9PYmplY3QoKSAvLz0+IHsgZGF5czogMS4wMjQxODEyMTUyNzc3Nzc4IH1cbiAgICAgKiBAZXhhbXBsZSBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0MSwgZHQyKS50b0R1cmF0aW9uKFsnaG91cnMnLCAnbWludXRlcyddKS50b09iamVjdCgpIC8vPT4geyBob3VyczogMjQsIG1pbnV0ZXM6IDM0LjgyMDk1IH1cbiAgICAgKiBAZXhhbXBsZSBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0MSwgZHQyKS50b0R1cmF0aW9uKFsnaG91cnMnLCAnbWludXRlcycsICdzZWNvbmRzJ10pLnRvT2JqZWN0KCkgLy89PiB7IGhvdXJzOiAyNCwgbWludXRlczogMzQsIHNlY29uZHM6IDQ5LjI1NyB9XG4gICAgICogQGV4YW1wbGUgSW50ZXJ2YWwuZnJvbURhdGVUaW1lcyhkdDEsIGR0MikudG9EdXJhdGlvbignc2Vjb25kcycpLnRvT2JqZWN0KCkgLy89PiB7IHNlY29uZHM6IDg4NDg5LjI1NyB9XG4gICAgICogQHJldHVybiB7RHVyYXRpb259XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvRHVyYXRpb24gPSBmdW5jdGlvbiB0b0R1cmF0aW9uKHVuaXQsIG9wdHMpIHtcbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSB7XG4gICAgICAgIHJldHVybiBEdXJhdGlvbi5pbnZhbGlkKHRoaXMuaW52YWxpZFJlYXNvbik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLmUuZGlmZih0aGlzLnMsIHVuaXQsIG9wdHMpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSdW4gbWFwRm4gb24gdGhlIGludGVydmFsIHN0YXJ0IGFuZCBlbmQsIHJldHVybmluZyBhIG5ldyBJbnRlcnZhbCBmcm9tIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWVzXG4gICAgICogQHBhcmFtIHtmdW5jdGlvbn0gbWFwRm5cbiAgICAgKiBAcmV0dXJuIHtJbnRlcnZhbH1cbiAgICAgKiBAZXhhbXBsZSBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0MSwgZHQyKS5tYXBFbmRwb2ludHMoZW5kcG9pbnQgPT4gZW5kcG9pbnQudG9VVEMoKSlcbiAgICAgKiBAZXhhbXBsZSBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKGR0MSwgZHQyKS5tYXBFbmRwb2ludHMoZW5kcG9pbnQgPT4gZW5kcG9pbnQucGx1cyh7IGhvdXJzOiAyIH0pKVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5tYXBFbmRwb2ludHMgPSBmdW5jdGlvbiBtYXBFbmRwb2ludHMobWFwRm4pIHtcbiAgICAgIHJldHVybiBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKG1hcEZuKHRoaXMucyksIG1hcEZuKHRoaXMuZSkpO1xuICAgIH07XG5cbiAgICBfY3JlYXRlQ2xhc3MoSW50ZXJ2YWwsIFt7XG4gICAgICBrZXk6IFwic3RhcnRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy5zIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyB0aGUgZW5kIG9mIHRoZSBJbnRlcnZhbFxuICAgICAgICogQHR5cGUge0RhdGVUaW1lfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiZW5kXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMuZSA6IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFJldHVybnMgd2hldGhlciB0aGlzIEludGVydmFsJ3MgZW5kIGlzIGF0IGxlYXN0IGl0cyBzdGFydCwgbWVhbmluZyB0aGF0IHRoZSBJbnRlcnZhbCBpc24ndCAnYmFja3dhcmRzJy5cbiAgICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaXNWYWxpZFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmludmFsaWRSZWFzb24gPT09IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFJldHVybnMgYW4gZXJyb3IgY29kZSBpZiB0aGlzIEludGVydmFsIGlzIGludmFsaWQsIG9yIG51bGwgaWYgdGhlIEludGVydmFsIGlzIHZhbGlkXG4gICAgICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaW52YWxpZFJlYXNvblwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmludmFsaWQgPyB0aGlzLmludmFsaWQucmVhc29uIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyBhbiBleHBsYW5hdGlvbiBvZiB3aHkgdGhpcyBJbnRlcnZhbCBiZWNhbWUgaW52YWxpZCwgb3IgbnVsbCBpZiB0aGUgSW50ZXJ2YWwgaXMgdmFsaWRcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJpbnZhbGlkRXhwbGFuYXRpb25cIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnZhbGlkID8gdGhpcy5pbnZhbGlkLmV4cGxhbmF0aW9uIDogbnVsbDtcbiAgICAgIH1cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gSW50ZXJ2YWw7XG4gIH0oKTtcblxuICAvKipcbiAgICogVGhlIEluZm8gY2xhc3MgY29udGFpbnMgc3RhdGljIG1ldGhvZHMgZm9yIHJldHJpZXZpbmcgZ2VuZXJhbCB0aW1lIGFuZCBkYXRlIHJlbGF0ZWQgZGF0YS4gRm9yIGV4YW1wbGUsIGl0IGhhcyBtZXRob2RzIGZvciBmaW5kaW5nIG91dCBpZiBhIHRpbWUgem9uZSBoYXMgYSBEU1QsIGZvciBsaXN0aW5nIHRoZSBtb250aHMgaW4gYW55IHN1cHBvcnRlZCBsb2NhbGUsIGFuZCBmb3IgZGlzY292ZXJpbmcgd2hpY2ggb2YgTHV4b24gZmVhdHVyZXMgYXJlIGF2YWlsYWJsZSBpbiB0aGUgY3VycmVudCBlbnZpcm9ubWVudC5cbiAgICovXG5cbiAgdmFyIEluZm8gPVxuICAvKiNfX1BVUkVfXyovXG4gIGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBJbmZvKCkge31cblxuICAgIC8qKlxuICAgICAqIFJldHVybiB3aGV0aGVyIHRoZSBzcGVjaWZpZWQgem9uZSBjb250YWlucyBhIERTVC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xab25lfSBbem9uZT0nbG9jYWwnXSAtIFpvbmUgdG8gY2hlY2suIERlZmF1bHRzIHRvIHRoZSBlbnZpcm9ubWVudCdzIGxvY2FsIHpvbmUuXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBJbmZvLmhhc0RTVCA9IGZ1bmN0aW9uIGhhc0RTVCh6b25lKSB7XG4gICAgICBpZiAoem9uZSA9PT0gdm9pZCAwKSB7XG4gICAgICAgIHpvbmUgPSBTZXR0aW5ncy5kZWZhdWx0Wm9uZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHByb3RvID0gRGF0ZVRpbWUubG9jYWwoKS5zZXRab25lKHpvbmUpLnNldCh7XG4gICAgICAgIG1vbnRoOiAxMlxuICAgICAgfSk7XG4gICAgICByZXR1cm4gIXpvbmUudW5pdmVyc2FsICYmIHByb3RvLm9mZnNldCAhPT0gcHJvdG8uc2V0KHtcbiAgICAgICAgbW9udGg6IDZcbiAgICAgIH0pLm9mZnNldDtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHdoZXRoZXIgdGhlIHNwZWNpZmllZCB6b25lIGlzIGEgdmFsaWQgSUFOQSBzcGVjaWZpZXIuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHpvbmUgLSBab25lIHRvIGNoZWNrXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBJbmZvLmlzVmFsaWRJQU5BWm9uZSA9IGZ1bmN0aW9uIGlzVmFsaWRJQU5BWm9uZSh6b25lKSB7XG4gICAgICByZXR1cm4gSUFOQVpvbmUuaXNWYWxpZFNwZWNpZmllcih6b25lKSAmJiBJQU5BWm9uZS5pc1ZhbGlkWm9uZSh6b25lKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhlIGlucHV0IGludG8gYSB7QGxpbmsgWm9uZX0gaW5zdGFuY2UuXG4gICAgICpcbiAgICAgKiAqIElmIGBpbnB1dGAgaXMgYWxyZWFkeSBhIFpvbmUgaW5zdGFuY2UsIGl0IGlzIHJldHVybmVkIHVuY2hhbmdlZC5cbiAgICAgKiAqIElmIGBpbnB1dGAgaXMgYSBzdHJpbmcgY29udGFpbmluZyBhIHZhbGlkIHRpbWUgem9uZSBuYW1lLCBhIFpvbmUgaW5zdGFuY2VcbiAgICAgKiAgIHdpdGggdGhhdCBuYW1lIGlzIHJldHVybmVkLlxuICAgICAqICogSWYgYGlucHV0YCBpcyBhIHN0cmluZyB0aGF0IGRvZXNuJ3QgcmVmZXIgdG8gYSBrbm93biB0aW1lIHpvbmUsIGEgWm9uZVxuICAgICAqICAgaW5zdGFuY2Ugd2l0aCB7QGxpbmsgWm9uZS5pc1ZhbGlkfSA9PSBmYWxzZSBpcyByZXR1cm5lZC5cbiAgICAgKiAqIElmIGBpbnB1dCBpcyBhIG51bWJlciwgYSBab25lIGluc3RhbmNlIHdpdGggdGhlIHNwZWNpZmllZCBmaXhlZCBvZmZzZXRcbiAgICAgKiAgIGluIG1pbnV0ZXMgaXMgcmV0dXJuZWQuXG4gICAgICogKiBJZiBgaW5wdXRgIGlzIGBudWxsYCBvciBgdW5kZWZpbmVkYCwgdGhlIGRlZmF1bHQgem9uZSBpcyByZXR1cm5lZC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xab25lfG51bWJlcn0gW2lucHV0XSAtIHRoZSB2YWx1ZSB0byBiZSBjb252ZXJ0ZWRcbiAgICAgKiBAcmV0dXJuIHtab25lfVxuICAgICAqL1xuICAgIDtcblxuICAgIEluZm8ubm9ybWFsaXplWm9uZSA9IGZ1bmN0aW9uIG5vcm1hbGl6ZVpvbmUkMShpbnB1dCkge1xuICAgICAgcmV0dXJuIG5vcm1hbGl6ZVpvbmUoaW5wdXQsIFNldHRpbmdzLmRlZmF1bHRab25lKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIGFuIGFycmF5IG9mIHN0YW5kYWxvbmUgbW9udGggbmFtZXMuXG4gICAgICogQHNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9EYXRlVGltZUZvcm1hdFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbbGVuZ3RoPSdsb25nJ10gLSB0aGUgbGVuZ3RoIG9mIHRoZSBtb250aCByZXByZXNlbnRhdGlvbiwgc3VjaCBhcyBcIm51bWVyaWNcIiwgXCIyLWRpZ2l0XCIsIFwibmFycm93XCIsIFwic2hvcnRcIiwgXCJsb25nXCJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW29wdHMubG9jYWxlXSAtIHRoZSBsb2NhbGUgY29kZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5udW1iZXJpbmdTeXN0ZW09bnVsbF0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5vdXRwdXRDYWxlbmRhcj0nZ3JlZ29yeSddIC0gdGhlIGNhbGVuZGFyXG4gICAgICogQGV4YW1wbGUgSW5mby5tb250aHMoKVswXSAvLz0+ICdKYW51YXJ5J1xuICAgICAqIEBleGFtcGxlIEluZm8ubW9udGhzKCdzaG9ydCcpWzBdIC8vPT4gJ0phbidcbiAgICAgKiBAZXhhbXBsZSBJbmZvLm1vbnRocygnbnVtZXJpYycpWzBdIC8vPT4gJzEnXG4gICAgICogQGV4YW1wbGUgSW5mby5tb250aHMoJ3Nob3J0JywgeyBsb2NhbGU6ICdmci1DQScgfSApWzBdIC8vPT4gJ2phbnYuJ1xuICAgICAqIEBleGFtcGxlIEluZm8ubW9udGhzKCdudW1lcmljJywgeyBsb2NhbGU6ICdhcicgfSlbMF0gLy89PiAn2aEnXG4gICAgICogQGV4YW1wbGUgSW5mby5tb250aHMoJ2xvbmcnLCB7IG91dHB1dENhbGVuZGFyOiAnaXNsYW1pYycgfSlbMF0gLy89PiAnUmFiacq7IEknXG4gICAgICogQHJldHVybiB7W3N0cmluZ119XG4gICAgICovXG4gICAgO1xuXG4gICAgSW5mby5tb250aHMgPSBmdW5jdGlvbiBtb250aHMobGVuZ3RoLCBfdGVtcCkge1xuICAgICAgaWYgKGxlbmd0aCA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGxlbmd0aCA9IFwibG9uZ1wiO1xuICAgICAgfVxuXG4gICAgICB2YXIgX3JlZiA9IF90ZW1wID09PSB2b2lkIDAgPyB7fSA6IF90ZW1wLFxuICAgICAgICAgIF9yZWYkbG9jYWxlID0gX3JlZi5sb2NhbGUsXG4gICAgICAgICAgbG9jYWxlID0gX3JlZiRsb2NhbGUgPT09IHZvaWQgMCA/IG51bGwgOiBfcmVmJGxvY2FsZSxcbiAgICAgICAgICBfcmVmJG51bWJlcmluZ1N5c3RlbSA9IF9yZWYubnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIG51bWJlcmluZ1N5c3RlbSA9IF9yZWYkbnVtYmVyaW5nU3lzdGVtID09PSB2b2lkIDAgPyBudWxsIDogX3JlZiRudW1iZXJpbmdTeXN0ZW0sXG4gICAgICAgICAgX3JlZiRvdXRwdXRDYWxlbmRhciA9IF9yZWYub3V0cHV0Q2FsZW5kYXIsXG4gICAgICAgICAgb3V0cHV0Q2FsZW5kYXIgPSBfcmVmJG91dHB1dENhbGVuZGFyID09PSB2b2lkIDAgPyBcImdyZWdvcnlcIiA6IF9yZWYkb3V0cHV0Q2FsZW5kYXI7XG5cbiAgICAgIHJldHVybiBMb2NhbGUuY3JlYXRlKGxvY2FsZSwgbnVtYmVyaW5nU3lzdGVtLCBvdXRwdXRDYWxlbmRhcikubW9udGhzKGxlbmd0aCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiBhbiBhcnJheSBvZiBmb3JtYXQgbW9udGggbmFtZXMuXG4gICAgICogRm9ybWF0IG1vbnRocyBkaWZmZXIgZnJvbSBzdGFuZGFsb25lIG1vbnRocyBpbiB0aGF0IHRoZXkncmUgbWVhbnQgdG8gYXBwZWFyIG5leHQgdG8gdGhlIGRheSBvZiB0aGUgbW9udGguIEluIHNvbWUgbGFuZ3VhZ2VzLCB0aGF0XG4gICAgICogY2hhbmdlcyB0aGUgc3RyaW5nLlxuICAgICAqIFNlZSB7QGxpbmsgbW9udGhzfVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbbGVuZ3RoPSdsb25nJ10gLSB0aGUgbGVuZ3RoIG9mIHRoZSBtb250aCByZXByZXNlbnRhdGlvbiwgc3VjaCBhcyBcIm51bWVyaWNcIiwgXCIyLWRpZ2l0XCIsIFwibmFycm93XCIsIFwic2hvcnRcIiwgXCJsb25nXCJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW29wdHMubG9jYWxlXSAtIHRoZSBsb2NhbGUgY29kZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5udW1iZXJpbmdTeXN0ZW09bnVsbF0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5vdXRwdXRDYWxlbmRhcj0nZ3JlZ29yeSddIC0gdGhlIGNhbGVuZGFyXG4gICAgICogQHJldHVybiB7W3N0cmluZ119XG4gICAgICovXG4gICAgO1xuXG4gICAgSW5mby5tb250aHNGb3JtYXQgPSBmdW5jdGlvbiBtb250aHNGb3JtYXQobGVuZ3RoLCBfdGVtcDIpIHtcbiAgICAgIGlmIChsZW5ndGggPT09IHZvaWQgMCkge1xuICAgICAgICBsZW5ndGggPSBcImxvbmdcIjtcbiAgICAgIH1cblxuICAgICAgdmFyIF9yZWYyID0gX3RlbXAyID09PSB2b2lkIDAgPyB7fSA6IF90ZW1wMixcbiAgICAgICAgICBfcmVmMiRsb2NhbGUgPSBfcmVmMi5sb2NhbGUsXG4gICAgICAgICAgbG9jYWxlID0gX3JlZjIkbG9jYWxlID09PSB2b2lkIDAgPyBudWxsIDogX3JlZjIkbG9jYWxlLFxuICAgICAgICAgIF9yZWYyJG51bWJlcmluZ1N5c3RlbSA9IF9yZWYyLm51bWJlcmluZ1N5c3RlbSxcbiAgICAgICAgICBudW1iZXJpbmdTeXN0ZW0gPSBfcmVmMiRudW1iZXJpbmdTeXN0ZW0gPT09IHZvaWQgMCA/IG51bGwgOiBfcmVmMiRudW1iZXJpbmdTeXN0ZW0sXG4gICAgICAgICAgX3JlZjIkb3V0cHV0Q2FsZW5kYXIgPSBfcmVmMi5vdXRwdXRDYWxlbmRhcixcbiAgICAgICAgICBvdXRwdXRDYWxlbmRhciA9IF9yZWYyJG91dHB1dENhbGVuZGFyID09PSB2b2lkIDAgPyBcImdyZWdvcnlcIiA6IF9yZWYyJG91dHB1dENhbGVuZGFyO1xuXG4gICAgICByZXR1cm4gTG9jYWxlLmNyZWF0ZShsb2NhbGUsIG51bWJlcmluZ1N5c3RlbSwgb3V0cHV0Q2FsZW5kYXIpLm1vbnRocyhsZW5ndGgsIHRydWUpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gYW4gYXJyYXkgb2Ygc3RhbmRhbG9uZSB3ZWVrIG5hbWVzLlxuICAgICAqIEBzZWUgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvRGF0ZVRpbWVGb3JtYXRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW2xlbmd0aD0nbG9uZyddIC0gdGhlIGxlbmd0aCBvZiB0aGUgbW9udGggcmVwcmVzZW50YXRpb24sIHN1Y2ggYXMgXCJuYXJyb3dcIiwgXCJzaG9ydFwiLCBcImxvbmdcIi5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW29wdHMubG9jYWxlXSAtIHRoZSBsb2NhbGUgY29kZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5udW1iZXJpbmdTeXN0ZW09bnVsbF0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbVxuICAgICAqIEBleGFtcGxlIEluZm8ud2Vla2RheXMoKVswXSAvLz0+ICdNb25kYXknXG4gICAgICogQGV4YW1wbGUgSW5mby53ZWVrZGF5cygnc2hvcnQnKVswXSAvLz0+ICdNb24nXG4gICAgICogQGV4YW1wbGUgSW5mby53ZWVrZGF5cygnc2hvcnQnLCB7IGxvY2FsZTogJ2ZyLUNBJyB9KVswXSAvLz0+ICdsdW4uJ1xuICAgICAqIEBleGFtcGxlIEluZm8ud2Vla2RheXMoJ3Nob3J0JywgeyBsb2NhbGU6ICdhcicgfSlbMF0gLy89PiAn2KfZhNin2KvZhtmK2YYnXG4gICAgICogQHJldHVybiB7W3N0cmluZ119XG4gICAgICovXG4gICAgO1xuXG4gICAgSW5mby53ZWVrZGF5cyA9IGZ1bmN0aW9uIHdlZWtkYXlzKGxlbmd0aCwgX3RlbXAzKSB7XG4gICAgICBpZiAobGVuZ3RoID09PSB2b2lkIDApIHtcbiAgICAgICAgbGVuZ3RoID0gXCJsb25nXCI7XG4gICAgICB9XG5cbiAgICAgIHZhciBfcmVmMyA9IF90ZW1wMyA9PT0gdm9pZCAwID8ge30gOiBfdGVtcDMsXG4gICAgICAgICAgX3JlZjMkbG9jYWxlID0gX3JlZjMubG9jYWxlLFxuICAgICAgICAgIGxvY2FsZSA9IF9yZWYzJGxvY2FsZSA9PT0gdm9pZCAwID8gbnVsbCA6IF9yZWYzJGxvY2FsZSxcbiAgICAgICAgICBfcmVmMyRudW1iZXJpbmdTeXN0ZW0gPSBfcmVmMy5udW1iZXJpbmdTeXN0ZW0sXG4gICAgICAgICAgbnVtYmVyaW5nU3lzdGVtID0gX3JlZjMkbnVtYmVyaW5nU3lzdGVtID09PSB2b2lkIDAgPyBudWxsIDogX3JlZjMkbnVtYmVyaW5nU3lzdGVtO1xuXG4gICAgICByZXR1cm4gTG9jYWxlLmNyZWF0ZShsb2NhbGUsIG51bWJlcmluZ1N5c3RlbSwgbnVsbCkud2Vla2RheXMobGVuZ3RoKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIGFuIGFycmF5IG9mIGZvcm1hdCB3ZWVrIG5hbWVzLlxuICAgICAqIEZvcm1hdCB3ZWVrZGF5cyBkaWZmZXIgZnJvbSBzdGFuZGFsb25lIHdlZWtkYXlzIGluIHRoYXQgdGhleSdyZSBtZWFudCB0byBhcHBlYXIgbmV4dCB0byBtb3JlIGRhdGUgaW5mb3JtYXRpb24uIEluIHNvbWUgbGFuZ3VhZ2VzLCB0aGF0XG4gICAgICogY2hhbmdlcyB0aGUgc3RyaW5nLlxuICAgICAqIFNlZSB7QGxpbmsgd2Vla2RheXN9XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtsZW5ndGg9J2xvbmcnXSAtIHRoZSBsZW5ndGggb2YgdGhlIG1vbnRoIHJlcHJlc2VudGF0aW9uLCBzdWNoIGFzIFwibmFycm93XCIsIFwic2hvcnRcIiwgXCJsb25nXCIuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRpb25zXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmxvY2FsZT1udWxsXSAtIHRoZSBsb2NhbGUgY29kZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5udW1iZXJpbmdTeXN0ZW09bnVsbF0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbVxuICAgICAqIEByZXR1cm4ge1tzdHJpbmddfVxuICAgICAqL1xuICAgIDtcblxuICAgIEluZm8ud2Vla2RheXNGb3JtYXQgPSBmdW5jdGlvbiB3ZWVrZGF5c0Zvcm1hdChsZW5ndGgsIF90ZW1wNCkge1xuICAgICAgaWYgKGxlbmd0aCA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGxlbmd0aCA9IFwibG9uZ1wiO1xuICAgICAgfVxuXG4gICAgICB2YXIgX3JlZjQgPSBfdGVtcDQgPT09IHZvaWQgMCA/IHt9IDogX3RlbXA0LFxuICAgICAgICAgIF9yZWY0JGxvY2FsZSA9IF9yZWY0LmxvY2FsZSxcbiAgICAgICAgICBsb2NhbGUgPSBfcmVmNCRsb2NhbGUgPT09IHZvaWQgMCA/IG51bGwgOiBfcmVmNCRsb2NhbGUsXG4gICAgICAgICAgX3JlZjQkbnVtYmVyaW5nU3lzdGVtID0gX3JlZjQubnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIG51bWJlcmluZ1N5c3RlbSA9IF9yZWY0JG51bWJlcmluZ1N5c3RlbSA9PT0gdm9pZCAwID8gbnVsbCA6IF9yZWY0JG51bWJlcmluZ1N5c3RlbTtcblxuICAgICAgcmV0dXJuIExvY2FsZS5jcmVhdGUobG9jYWxlLCBudW1iZXJpbmdTeXN0ZW0sIG51bGwpLndlZWtkYXlzKGxlbmd0aCwgdHJ1ZSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybiBhbiBhcnJheSBvZiBtZXJpZGllbXMuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRpb25zXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmxvY2FsZV0gLSB0aGUgbG9jYWxlIGNvZGVcbiAgICAgKiBAZXhhbXBsZSBJbmZvLm1lcmlkaWVtcygpIC8vPT4gWyAnQU0nLCAnUE0nIF1cbiAgICAgKiBAZXhhbXBsZSBJbmZvLm1lcmlkaWVtcyh7IGxvY2FsZTogJ215JyB9KSAvLz0+IFsgJ+GAlOGAtuGAlOGAgOGAuicsICfhgIrhgJThgLEnIF1cbiAgICAgKiBAcmV0dXJuIHtbc3RyaW5nXX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBJbmZvLm1lcmlkaWVtcyA9IGZ1bmN0aW9uIG1lcmlkaWVtcyhfdGVtcDUpIHtcbiAgICAgIHZhciBfcmVmNSA9IF90ZW1wNSA9PT0gdm9pZCAwID8ge30gOiBfdGVtcDUsXG4gICAgICAgICAgX3JlZjUkbG9jYWxlID0gX3JlZjUubG9jYWxlLFxuICAgICAgICAgIGxvY2FsZSA9IF9yZWY1JGxvY2FsZSA9PT0gdm9pZCAwID8gbnVsbCA6IF9yZWY1JGxvY2FsZTtcblxuICAgICAgcmV0dXJuIExvY2FsZS5jcmVhdGUobG9jYWxlKS5tZXJpZGllbXMoKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIGFuIGFycmF5IG9mIGVyYXMsIHN1Y2ggYXMgWydCQycsICdBRCddLiBUaGUgbG9jYWxlIGNhbiBiZSBzcGVjaWZpZWQsIGJ1dCB0aGUgY2FsZW5kYXIgc3lzdGVtIGlzIGFsd2F5cyBHcmVnb3JpYW4uXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtsZW5ndGg9J3Nob3J0J10gLSB0aGUgbGVuZ3RoIG9mIHRoZSBlcmEgcmVwcmVzZW50YXRpb24sIHN1Y2ggYXMgXCJzaG9ydFwiIG9yIFwibG9uZ1wiLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9uc1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5sb2NhbGVdIC0gdGhlIGxvY2FsZSBjb2RlXG4gICAgICogQGV4YW1wbGUgSW5mby5lcmFzKCkgLy89PiBbICdCQycsICdBRCcgXVxuICAgICAqIEBleGFtcGxlIEluZm8uZXJhcygnbG9uZycpIC8vPT4gWyAnQmVmb3JlIENocmlzdCcsICdBbm5vIERvbWluaScgXVxuICAgICAqIEBleGFtcGxlIEluZm8uZXJhcygnbG9uZycsIHsgbG9jYWxlOiAnZnInIH0pIC8vPT4gWyAnYXZhbnQgSsOpc3VzLUNocmlzdCcsICdhcHLDqHMgSsOpc3VzLUNocmlzdCcgXVxuICAgICAqIEByZXR1cm4ge1tzdHJpbmddfVxuICAgICAqL1xuICAgIDtcblxuICAgIEluZm8uZXJhcyA9IGZ1bmN0aW9uIGVyYXMobGVuZ3RoLCBfdGVtcDYpIHtcbiAgICAgIGlmIChsZW5ndGggPT09IHZvaWQgMCkge1xuICAgICAgICBsZW5ndGggPSBcInNob3J0XCI7XG4gICAgICB9XG5cbiAgICAgIHZhciBfcmVmNiA9IF90ZW1wNiA9PT0gdm9pZCAwID8ge30gOiBfdGVtcDYsXG4gICAgICAgICAgX3JlZjYkbG9jYWxlID0gX3JlZjYubG9jYWxlLFxuICAgICAgICAgIGxvY2FsZSA9IF9yZWY2JGxvY2FsZSA9PT0gdm9pZCAwID8gbnVsbCA6IF9yZWY2JGxvY2FsZTtcblxuICAgICAgcmV0dXJuIExvY2FsZS5jcmVhdGUobG9jYWxlLCBudWxsLCBcImdyZWdvcnlcIikuZXJhcyhsZW5ndGgpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIHNldCBvZiBhdmFpbGFibGUgZmVhdHVyZXMgaW4gdGhpcyBlbnZpcm9ubWVudC5cbiAgICAgKiBTb21lIGZlYXR1cmVzIG9mIEx1eG9uIGFyZSBub3QgYXZhaWxhYmxlIGluIGFsbCBlbnZpcm9ubWVudHMuIEZvciBleGFtcGxlLCBvbiBvbGRlciBicm93c2VycywgdGltZXpvbmUgc3VwcG9ydCBpcyBub3QgYXZhaWxhYmxlLiBVc2UgdGhpcyBmdW5jdGlvbiB0byBmaWd1cmUgb3V0IGlmIHRoYXQncyB0aGUgY2FzZS5cbiAgICAgKiBLZXlzOlxuICAgICAqICogYHpvbmVzYDogd2hldGhlciB0aGlzIGVudmlyb25tZW50IHN1cHBvcnRzIElBTkEgdGltZXpvbmVzXG4gICAgICogKiBgaW50bFRva2Vuc2A6IHdoZXRoZXIgdGhpcyBlbnZpcm9ubWVudCBzdXBwb3J0cyBpbnRlcm5hdGlvbmFsaXplZCB0b2tlbi1iYXNlZCBmb3JtYXR0aW5nL3BhcnNpbmdcbiAgICAgKiAqIGBpbnRsYDogd2hldGhlciB0aGlzIGVudmlyb25tZW50IHN1cHBvcnRzIGdlbmVyYWwgaW50ZXJuYXRpb25hbGl6YXRpb25cbiAgICAgKiAqIGByZWxhdGl2ZWA6IHdoZXRoZXIgdGhpcyBlbnZpcm9ubWVudCBzdXBwb3J0cyByZWxhdGl2ZSB0aW1lIGZvcm1hdHRpbmdcbiAgICAgKiBAZXhhbXBsZSBJbmZvLmZlYXR1cmVzKCkgLy89PiB7IGludGw6IHRydWUsIGludGxUb2tlbnM6IGZhbHNlLCB6b25lczogdHJ1ZSwgcmVsYXRpdmU6IGZhbHNlIH1cbiAgICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAgICovXG4gICAgO1xuXG4gICAgSW5mby5mZWF0dXJlcyA9IGZ1bmN0aW9uIGZlYXR1cmVzKCkge1xuICAgICAgdmFyIGludGwgPSBmYWxzZSxcbiAgICAgICAgICBpbnRsVG9rZW5zID0gZmFsc2UsXG4gICAgICAgICAgem9uZXMgPSBmYWxzZSxcbiAgICAgICAgICByZWxhdGl2ZSA9IGZhbHNlO1xuXG4gICAgICBpZiAoaGFzSW50bCgpKSB7XG4gICAgICAgIGludGwgPSB0cnVlO1xuICAgICAgICBpbnRsVG9rZW5zID0gaGFzRm9ybWF0VG9QYXJ0cygpO1xuICAgICAgICByZWxhdGl2ZSA9IGhhc1JlbGF0aXZlKCk7XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICB6b25lcyA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KFwiZW5cIiwge1xuICAgICAgICAgICAgdGltZVpvbmU6IFwiQW1lcmljYS9OZXdfWW9ya1wiXG4gICAgICAgICAgfSkucmVzb2x2ZWRPcHRpb25zKCkudGltZVpvbmUgPT09IFwiQW1lcmljYS9OZXdfWW9ya1wiO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgem9uZXMgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBpbnRsOiBpbnRsLFxuICAgICAgICBpbnRsVG9rZW5zOiBpbnRsVG9rZW5zLFxuICAgICAgICB6b25lczogem9uZXMsXG4gICAgICAgIHJlbGF0aXZlOiByZWxhdGl2ZVxuICAgICAgfTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIEluZm87XG4gIH0oKTtcblxuICBmdW5jdGlvbiBkYXlEaWZmKGVhcmxpZXIsIGxhdGVyKSB7XG4gICAgdmFyIHV0Y0RheVN0YXJ0ID0gZnVuY3Rpb24gdXRjRGF5U3RhcnQoZHQpIHtcbiAgICAgIHJldHVybiBkdC50b1VUQygwLCB7XG4gICAgICAgIGtlZXBMb2NhbFRpbWU6IHRydWVcbiAgICAgIH0pLnN0YXJ0T2YoXCJkYXlcIikudmFsdWVPZigpO1xuICAgIH0sXG4gICAgICAgIG1zID0gdXRjRGF5U3RhcnQobGF0ZXIpIC0gdXRjRGF5U3RhcnQoZWFybGllcik7XG5cbiAgICByZXR1cm4gTWF0aC5mbG9vcihEdXJhdGlvbi5mcm9tTWlsbGlzKG1zKS5hcyhcImRheXNcIikpO1xuICB9XG5cbiAgZnVuY3Rpb24gaGlnaE9yZGVyRGlmZnMoY3Vyc29yLCBsYXRlciwgdW5pdHMpIHtcbiAgICB2YXIgZGlmZmVycyA9IFtbXCJ5ZWFyc1wiLCBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgcmV0dXJuIGIueWVhciAtIGEueWVhcjtcbiAgICB9XSwgW1wibW9udGhzXCIsIGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICByZXR1cm4gYi5tb250aCAtIGEubW9udGggKyAoYi55ZWFyIC0gYS55ZWFyKSAqIDEyO1xuICAgIH1dLCBbXCJ3ZWVrc1wiLCBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgdmFyIGRheXMgPSBkYXlEaWZmKGEsIGIpO1xuICAgICAgcmV0dXJuIChkYXlzIC0gZGF5cyAlIDcpIC8gNztcbiAgICB9XSwgW1wiZGF5c1wiLCBkYXlEaWZmXV07XG4gICAgdmFyIHJlc3VsdHMgPSB7fTtcbiAgICB2YXIgbG93ZXN0T3JkZXIsIGhpZ2hXYXRlcjtcblxuICAgIGZvciAodmFyIF9pID0gMCwgX2RpZmZlcnMgPSBkaWZmZXJzOyBfaSA8IF9kaWZmZXJzLmxlbmd0aDsgX2krKykge1xuICAgICAgdmFyIF9kaWZmZXJzJF9pID0gX2RpZmZlcnNbX2ldLFxuICAgICAgICAgIHVuaXQgPSBfZGlmZmVycyRfaVswXSxcbiAgICAgICAgICBkaWZmZXIgPSBfZGlmZmVycyRfaVsxXTtcblxuICAgICAgaWYgKHVuaXRzLmluZGV4T2YodW5pdCkgPj0gMCkge1xuICAgICAgICB2YXIgX2N1cnNvciRwbHVzO1xuXG4gICAgICAgIGxvd2VzdE9yZGVyID0gdW5pdDtcbiAgICAgICAgdmFyIGRlbHRhID0gZGlmZmVyKGN1cnNvciwgbGF0ZXIpO1xuICAgICAgICBoaWdoV2F0ZXIgPSBjdXJzb3IucGx1cygoX2N1cnNvciRwbHVzID0ge30sIF9jdXJzb3IkcGx1c1t1bml0XSA9IGRlbHRhLCBfY3Vyc29yJHBsdXMpKTtcblxuICAgICAgICBpZiAoaGlnaFdhdGVyID4gbGF0ZXIpIHtcbiAgICAgICAgICB2YXIgX2N1cnNvciRwbHVzMjtcblxuICAgICAgICAgIGN1cnNvciA9IGN1cnNvci5wbHVzKChfY3Vyc29yJHBsdXMyID0ge30sIF9jdXJzb3IkcGx1czJbdW5pdF0gPSBkZWx0YSAtIDEsIF9jdXJzb3IkcGx1czIpKTtcbiAgICAgICAgICBkZWx0YSAtPSAxO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGN1cnNvciA9IGhpZ2hXYXRlcjtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlc3VsdHNbdW5pdF0gPSBkZWx0YTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gW2N1cnNvciwgcmVzdWx0cywgaGlnaFdhdGVyLCBsb3dlc3RPcmRlcl07XG4gIH1cblxuICBmdW5jdGlvbiBfZGlmZiAoZWFybGllciwgbGF0ZXIsIHVuaXRzLCBvcHRzKSB7XG4gICAgdmFyIF9oaWdoT3JkZXJEaWZmcyA9IGhpZ2hPcmRlckRpZmZzKGVhcmxpZXIsIGxhdGVyLCB1bml0cyksXG4gICAgICAgIGN1cnNvciA9IF9oaWdoT3JkZXJEaWZmc1swXSxcbiAgICAgICAgcmVzdWx0cyA9IF9oaWdoT3JkZXJEaWZmc1sxXSxcbiAgICAgICAgaGlnaFdhdGVyID0gX2hpZ2hPcmRlckRpZmZzWzJdLFxuICAgICAgICBsb3dlc3RPcmRlciA9IF9oaWdoT3JkZXJEaWZmc1szXTtcblxuICAgIHZhciByZW1haW5pbmdNaWxsaXMgPSBsYXRlciAtIGN1cnNvcjtcbiAgICB2YXIgbG93ZXJPcmRlclVuaXRzID0gdW5pdHMuZmlsdGVyKGZ1bmN0aW9uICh1KSB7XG4gICAgICByZXR1cm4gW1wiaG91cnNcIiwgXCJtaW51dGVzXCIsIFwic2Vjb25kc1wiLCBcIm1pbGxpc2Vjb25kc1wiXS5pbmRleE9mKHUpID49IDA7XG4gICAgfSk7XG5cbiAgICBpZiAobG93ZXJPcmRlclVuaXRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgaWYgKGhpZ2hXYXRlciA8IGxhdGVyKSB7XG4gICAgICAgIHZhciBfY3Vyc29yJHBsdXMzO1xuXG4gICAgICAgIGhpZ2hXYXRlciA9IGN1cnNvci5wbHVzKChfY3Vyc29yJHBsdXMzID0ge30sIF9jdXJzb3IkcGx1czNbbG93ZXN0T3JkZXJdID0gMSwgX2N1cnNvciRwbHVzMykpO1xuICAgICAgfVxuXG4gICAgICBpZiAoaGlnaFdhdGVyICE9PSBjdXJzb3IpIHtcbiAgICAgICAgcmVzdWx0c1tsb3dlc3RPcmRlcl0gPSAocmVzdWx0c1tsb3dlc3RPcmRlcl0gfHwgMCkgKyByZW1haW5pbmdNaWxsaXMgLyAoaGlnaFdhdGVyIC0gY3Vyc29yKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgZHVyYXRpb24gPSBEdXJhdGlvbi5mcm9tT2JqZWN0KE9iamVjdC5hc3NpZ24ocmVzdWx0cywgb3B0cykpO1xuXG4gICAgaWYgKGxvd2VyT3JkZXJVbml0cy5sZW5ndGggPiAwKSB7XG4gICAgICB2YXIgX0R1cmF0aW9uJGZyb21NaWxsaXM7XG5cbiAgICAgIHJldHVybiAoX0R1cmF0aW9uJGZyb21NaWxsaXMgPSBEdXJhdGlvbi5mcm9tTWlsbGlzKHJlbWFpbmluZ01pbGxpcywgb3B0cykpLnNoaWZ0VG8uYXBwbHkoX0R1cmF0aW9uJGZyb21NaWxsaXMsIGxvd2VyT3JkZXJVbml0cykucGx1cyhkdXJhdGlvbik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBkdXJhdGlvbjtcbiAgICB9XG4gIH1cblxuICB2YXIgbnVtYmVyaW5nU3lzdGVtcyA9IHtcbiAgICBhcmFiOiBcIltcXHUwNjYwLVxcdTA2NjldXCIsXG4gICAgYXJhYmV4dDogXCJbXFx1MDZGMC1cXHUwNkY5XVwiLFxuICAgIGJhbGk6IFwiW1xcdTFCNTAtXFx1MUI1OV1cIixcbiAgICBiZW5nOiBcIltcXHUwOUU2LVxcdTA5RUZdXCIsXG4gICAgZGV2YTogXCJbXFx1MDk2Ni1cXHUwOTZGXVwiLFxuICAgIGZ1bGx3aWRlOiBcIltcXHVGRjEwLVxcdUZGMTldXCIsXG4gICAgZ3VqcjogXCJbXFx1MEFFNi1cXHUwQUVGXVwiLFxuICAgIGhhbmlkZWM6IFwiW+OAh3zkuIB85LqMfOS4iXzlm5t85LqUfOWFrXzkuIN85YWrfOS5nV1cIixcbiAgICBraG1yOiBcIltcXHUxN0UwLVxcdTE3RTldXCIsXG4gICAga25kYTogXCJbXFx1MENFNi1cXHUwQ0VGXVwiLFxuICAgIGxhb286IFwiW1xcdTBFRDAtXFx1MEVEOV1cIixcbiAgICBsaW1iOiBcIltcXHUxOTQ2LVxcdTE5NEZdXCIsXG4gICAgbWx5bTogXCJbXFx1MEQ2Ni1cXHUwRDZGXVwiLFxuICAgIG1vbmc6IFwiW1xcdTE4MTAtXFx1MTgxOV1cIixcbiAgICBteW1yOiBcIltcXHUxMDQwLVxcdTEwNDldXCIsXG4gICAgb3J5YTogXCJbXFx1MEI2Ni1cXHUwQjZGXVwiLFxuICAgIHRhbWxkZWM6IFwiW1xcdTBCRTYtXFx1MEJFRl1cIixcbiAgICB0ZWx1OiBcIltcXHUwQzY2LVxcdTBDNkZdXCIsXG4gICAgdGhhaTogXCJbXFx1MEU1MC1cXHUwRTU5XVwiLFxuICAgIHRpYnQ6IFwiW1xcdTBGMjAtXFx1MEYyOV1cIixcbiAgICBsYXRuOiBcIlxcXFxkXCJcbiAgfTtcbiAgdmFyIG51bWJlcmluZ1N5c3RlbXNVVEYxNiA9IHtcbiAgICBhcmFiOiBbMTYzMiwgMTY0MV0sXG4gICAgYXJhYmV4dDogWzE3NzYsIDE3ODVdLFxuICAgIGJhbGk6IFs2OTkyLCA3MDAxXSxcbiAgICBiZW5nOiBbMjUzNCwgMjU0M10sXG4gICAgZGV2YTogWzI0MDYsIDI0MTVdLFxuICAgIGZ1bGx3aWRlOiBbNjUyOTYsIDY1MzAzXSxcbiAgICBndWpyOiBbMjc5MCwgMjc5OV0sXG4gICAga2htcjogWzYxMTIsIDYxMjFdLFxuICAgIGtuZGE6IFszMzAyLCAzMzExXSxcbiAgICBsYW9vOiBbMzc5MiwgMzgwMV0sXG4gICAgbGltYjogWzY0NzAsIDY0NzldLFxuICAgIG1seW06IFszNDMwLCAzNDM5XSxcbiAgICBtb25nOiBbNjE2MCwgNjE2OV0sXG4gICAgbXltcjogWzQxNjAsIDQxNjldLFxuICAgIG9yeWE6IFsyOTE4LCAyOTI3XSxcbiAgICB0YW1sZGVjOiBbMzA0NiwgMzA1NV0sXG4gICAgdGVsdTogWzMxNzQsIDMxODNdLFxuICAgIHRoYWk6IFszNjY0LCAzNjczXSxcbiAgICB0aWJ0OiBbMzg3MiwgMzg4MV1cbiAgfTsgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG5cbiAgdmFyIGhhbmlkZWNDaGFycyA9IG51bWJlcmluZ1N5c3RlbXMuaGFuaWRlYy5yZXBsYWNlKC9bXFxbfFxcXV0vZywgXCJcIikuc3BsaXQoXCJcIik7XG4gIGZ1bmN0aW9uIHBhcnNlRGlnaXRzKHN0cikge1xuICAgIHZhciB2YWx1ZSA9IHBhcnNlSW50KHN0ciwgMTApO1xuXG4gICAgaWYgKGlzTmFOKHZhbHVlKSkge1xuICAgICAgdmFsdWUgPSBcIlwiO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHN0ci5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgY29kZSA9IHN0ci5jaGFyQ29kZUF0KGkpO1xuXG4gICAgICAgIGlmIChzdHJbaV0uc2VhcmNoKG51bWJlcmluZ1N5c3RlbXMuaGFuaWRlYykgIT09IC0xKSB7XG4gICAgICAgICAgdmFsdWUgKz0gaGFuaWRlY0NoYXJzLmluZGV4T2Yoc3RyW2ldKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBmb3IgKHZhciBrZXkgaW4gbnVtYmVyaW5nU3lzdGVtc1VURjE2KSB7XG4gICAgICAgICAgICB2YXIgX251bWJlcmluZ1N5c3RlbXNVVEYgPSBudW1iZXJpbmdTeXN0ZW1zVVRGMTZba2V5XSxcbiAgICAgICAgICAgICAgICBtaW4gPSBfbnVtYmVyaW5nU3lzdGVtc1VURlswXSxcbiAgICAgICAgICAgICAgICBtYXggPSBfbnVtYmVyaW5nU3lzdGVtc1VURlsxXTtcblxuICAgICAgICAgICAgaWYgKGNvZGUgPj0gbWluICYmIGNvZGUgPD0gbWF4KSB7XG4gICAgICAgICAgICAgIHZhbHVlICs9IGNvZGUgLSBtaW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBwYXJzZUludCh2YWx1ZSwgMTApO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIGRpZ2l0UmVnZXgoX3JlZiwgYXBwZW5kKSB7XG4gICAgdmFyIG51bWJlcmluZ1N5c3RlbSA9IF9yZWYubnVtYmVyaW5nU3lzdGVtO1xuXG4gICAgaWYgKGFwcGVuZCA9PT0gdm9pZCAwKSB7XG4gICAgICBhcHBlbmQgPSBcIlwiO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgUmVnRXhwKFwiXCIgKyBudW1iZXJpbmdTeXN0ZW1zW251bWJlcmluZ1N5c3RlbSB8fCBcImxhdG5cIl0gKyBhcHBlbmQpO1xuICB9XG5cbiAgdmFyIE1JU1NJTkdfRlRQID0gXCJtaXNzaW5nIEludGwuRGF0ZVRpbWVGb3JtYXQuZm9ybWF0VG9QYXJ0cyBzdXBwb3J0XCI7XG5cbiAgZnVuY3Rpb24gaW50VW5pdChyZWdleCwgcG9zdCkge1xuICAgIGlmIChwb3N0ID09PSB2b2lkIDApIHtcbiAgICAgIHBvc3QgPSBmdW5jdGlvbiBwb3N0KGkpIHtcbiAgICAgICAgcmV0dXJuIGk7XG4gICAgICB9O1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICByZWdleDogcmVnZXgsXG4gICAgICBkZXNlcjogZnVuY3Rpb24gZGVzZXIoX3JlZikge1xuICAgICAgICB2YXIgcyA9IF9yZWZbMF07XG4gICAgICAgIHJldHVybiBwb3N0KHBhcnNlRGlnaXRzKHMpKTtcbiAgICAgIH1cbiAgICB9O1xuICB9XG5cbiAgZnVuY3Rpb24gZml4TGlzdFJlZ2V4KHMpIHtcbiAgICAvLyBtYWtlIGRvdHMgb3B0aW9uYWwgYW5kIGFsc28gbWFrZSB0aGVtIGxpdGVyYWxcbiAgICByZXR1cm4gcy5yZXBsYWNlKC9cXC4vLCBcIlxcXFwuP1wiKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHN0cmlwSW5zZW5zaXRpdml0aWVzKHMpIHtcbiAgICByZXR1cm4gcy5yZXBsYWNlKC9cXC4vLCBcIlwiKS50b0xvd2VyQ2FzZSgpO1xuICB9XG5cbiAgZnVuY3Rpb24gb25lT2Yoc3RyaW5ncywgc3RhcnRJbmRleCkge1xuICAgIGlmIChzdHJpbmdzID09PSBudWxsKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgcmVnZXg6IFJlZ0V4cChzdHJpbmdzLm1hcChmaXhMaXN0UmVnZXgpLmpvaW4oXCJ8XCIpKSxcbiAgICAgICAgZGVzZXI6IGZ1bmN0aW9uIGRlc2VyKF9yZWYyKSB7XG4gICAgICAgICAgdmFyIHMgPSBfcmVmMlswXTtcbiAgICAgICAgICByZXR1cm4gc3RyaW5ncy5maW5kSW5kZXgoZnVuY3Rpb24gKGkpIHtcbiAgICAgICAgICAgIHJldHVybiBzdHJpcEluc2Vuc2l0aXZpdGllcyhzKSA9PT0gc3RyaXBJbnNlbnNpdGl2aXRpZXMoaSk7XG4gICAgICAgICAgfSkgKyBzdGFydEluZGV4O1xuICAgICAgICB9XG4gICAgICB9O1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIG9mZnNldChyZWdleCwgZ3JvdXBzKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlZ2V4OiByZWdleCxcbiAgICAgIGRlc2VyOiBmdW5jdGlvbiBkZXNlcihfcmVmMykge1xuICAgICAgICB2YXIgaCA9IF9yZWYzWzFdLFxuICAgICAgICAgICAgbSA9IF9yZWYzWzJdO1xuICAgICAgICByZXR1cm4gc2lnbmVkT2Zmc2V0KGgsIG0pO1xuICAgICAgfSxcbiAgICAgIGdyb3VwczogZ3JvdXBzXG4gICAgfTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHNpbXBsZShyZWdleCkge1xuICAgIHJldHVybiB7XG4gICAgICByZWdleDogcmVnZXgsXG4gICAgICBkZXNlcjogZnVuY3Rpb24gZGVzZXIoX3JlZjQpIHtcbiAgICAgICAgdmFyIHMgPSBfcmVmNFswXTtcbiAgICAgICAgcmV0dXJuIHM7XG4gICAgICB9XG4gICAgfTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGVzY2FwZVRva2VuKHZhbHVlKSB7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVzZWxlc3MtZXNjYXBlXG4gICAgcmV0dXJuIHZhbHVlLnJlcGxhY2UoL1tcXC1cXFtcXF17fSgpKis/LixcXFxcXFxeJHwjXFxzXS9nLCBcIlxcXFwkJlwiKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHVuaXRGb3JUb2tlbih0b2tlbiwgbG9jKSB7XG4gICAgdmFyIG9uZSA9IGRpZ2l0UmVnZXgobG9jKSxcbiAgICAgICAgdHdvID0gZGlnaXRSZWdleChsb2MsIFwiezJ9XCIpLFxuICAgICAgICB0aHJlZSA9IGRpZ2l0UmVnZXgobG9jLCBcInszfVwiKSxcbiAgICAgICAgZm91ciA9IGRpZ2l0UmVnZXgobG9jLCBcIns0fVwiKSxcbiAgICAgICAgc2l4ID0gZGlnaXRSZWdleChsb2MsIFwiezZ9XCIpLFxuICAgICAgICBvbmVPclR3byA9IGRpZ2l0UmVnZXgobG9jLCBcInsxLDJ9XCIpLFxuICAgICAgICBvbmVUb1RocmVlID0gZGlnaXRSZWdleChsb2MsIFwiezEsM31cIiksXG4gICAgICAgIG9uZVRvU2l4ID0gZGlnaXRSZWdleChsb2MsIFwiezEsNn1cIiksXG4gICAgICAgIG9uZVRvTmluZSA9IGRpZ2l0UmVnZXgobG9jLCBcInsxLDl9XCIpLFxuICAgICAgICB0d29Ub0ZvdXIgPSBkaWdpdFJlZ2V4KGxvYywgXCJ7Miw0fVwiKSxcbiAgICAgICAgZm91clRvU2l4ID0gZGlnaXRSZWdleChsb2MsIFwiezQsNn1cIiksXG4gICAgICAgIGxpdGVyYWwgPSBmdW5jdGlvbiBsaXRlcmFsKHQpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHJlZ2V4OiBSZWdFeHAoZXNjYXBlVG9rZW4odC52YWwpKSxcbiAgICAgICAgZGVzZXI6IGZ1bmN0aW9uIGRlc2VyKF9yZWY1KSB7XG4gICAgICAgICAgdmFyIHMgPSBfcmVmNVswXTtcbiAgICAgICAgICByZXR1cm4gcztcbiAgICAgICAgfSxcbiAgICAgICAgbGl0ZXJhbDogdHJ1ZVxuICAgICAgfTtcbiAgICB9LFxuICAgICAgICB1bml0YXRlID0gZnVuY3Rpb24gdW5pdGF0ZSh0KSB7XG4gICAgICBpZiAodG9rZW4ubGl0ZXJhbCkge1xuICAgICAgICByZXR1cm4gbGl0ZXJhbCh0KTtcbiAgICAgIH1cblxuICAgICAgc3dpdGNoICh0LnZhbCkge1xuICAgICAgICAvLyBlcmFcbiAgICAgICAgY2FzZSBcIkdcIjpcbiAgICAgICAgICByZXR1cm4gb25lT2YobG9jLmVyYXMoXCJzaG9ydFwiLCBmYWxzZSksIDApO1xuXG4gICAgICAgIGNhc2UgXCJHR1wiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2MuZXJhcyhcImxvbmdcIiwgZmFsc2UpLCAwKTtcbiAgICAgICAgLy8geWVhcnNcblxuICAgICAgICBjYXNlIFwieVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KG9uZVRvU2l4KTtcblxuICAgICAgICBjYXNlIFwieXlcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdCh0d29Ub0ZvdXIsIHVudHJ1bmNhdGVZZWFyKTtcblxuICAgICAgICBjYXNlIFwieXl5eVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KGZvdXIpO1xuXG4gICAgICAgIGNhc2UgXCJ5eXl5eVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KGZvdXJUb1NpeCk7XG5cbiAgICAgICAgY2FzZSBcInl5eXl5eVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KHNpeCk7XG4gICAgICAgIC8vIG1vbnRoc1xuXG4gICAgICAgIGNhc2UgXCJNXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQob25lT3JUd28pO1xuXG4gICAgICAgIGNhc2UgXCJNTVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KHR3byk7XG5cbiAgICAgICAgY2FzZSBcIk1NTVwiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2MubW9udGhzKFwic2hvcnRcIiwgdHJ1ZSwgZmFsc2UpLCAxKTtcblxuICAgICAgICBjYXNlIFwiTU1NTVwiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2MubW9udGhzKFwibG9uZ1wiLCB0cnVlLCBmYWxzZSksIDEpO1xuXG4gICAgICAgIGNhc2UgXCJMXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQob25lT3JUd28pO1xuXG4gICAgICAgIGNhc2UgXCJMTFwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KHR3byk7XG5cbiAgICAgICAgY2FzZSBcIkxMTFwiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2MubW9udGhzKFwic2hvcnRcIiwgZmFsc2UsIGZhbHNlKSwgMSk7XG5cbiAgICAgICAgY2FzZSBcIkxMTExcIjpcbiAgICAgICAgICByZXR1cm4gb25lT2YobG9jLm1vbnRocyhcImxvbmdcIiwgZmFsc2UsIGZhbHNlKSwgMSk7XG4gICAgICAgIC8vIGRhdGVzXG5cbiAgICAgICAgY2FzZSBcImRcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdChvbmVPclR3byk7XG5cbiAgICAgICAgY2FzZSBcImRkXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQodHdvKTtcbiAgICAgICAgLy8gb3JkaW5hbHNcblxuICAgICAgICBjYXNlIFwib1wiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KG9uZVRvVGhyZWUpO1xuXG4gICAgICAgIGNhc2UgXCJvb29cIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdCh0aHJlZSk7XG4gICAgICAgIC8vIHRpbWVcblxuICAgICAgICBjYXNlIFwiSEhcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdCh0d28pO1xuXG4gICAgICAgIGNhc2UgXCJIXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQob25lT3JUd28pO1xuXG4gICAgICAgIGNhc2UgXCJoaFwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KHR3byk7XG5cbiAgICAgICAgY2FzZSBcImhcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdChvbmVPclR3byk7XG5cbiAgICAgICAgY2FzZSBcIm1tXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQodHdvKTtcblxuICAgICAgICBjYXNlIFwibVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KG9uZU9yVHdvKTtcblxuICAgICAgICBjYXNlIFwicVwiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KG9uZU9yVHdvKTtcblxuICAgICAgICBjYXNlIFwicXFcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdCh0d28pO1xuXG4gICAgICAgIGNhc2UgXCJzXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQob25lT3JUd28pO1xuXG4gICAgICAgIGNhc2UgXCJzc1wiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KHR3byk7XG5cbiAgICAgICAgY2FzZSBcIlNcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdChvbmVUb1RocmVlKTtcblxuICAgICAgICBjYXNlIFwiU1NTXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQodGhyZWUpO1xuXG4gICAgICAgIGNhc2UgXCJ1XCI6XG4gICAgICAgICAgcmV0dXJuIHNpbXBsZShvbmVUb05pbmUpO1xuICAgICAgICAvLyBtZXJpZGllbVxuXG4gICAgICAgIGNhc2UgXCJhXCI6XG4gICAgICAgICAgcmV0dXJuIG9uZU9mKGxvYy5tZXJpZGllbXMoKSwgMCk7XG4gICAgICAgIC8vIHdlZWtZZWFyIChrKVxuXG4gICAgICAgIGNhc2UgXCJra2trXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQoZm91cik7XG5cbiAgICAgICAgY2FzZSBcImtrXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQodHdvVG9Gb3VyLCB1bnRydW5jYXRlWWVhcik7XG4gICAgICAgIC8vIHdlZWtOdW1iZXIgKFcpXG5cbiAgICAgICAgY2FzZSBcIldcIjpcbiAgICAgICAgICByZXR1cm4gaW50VW5pdChvbmVPclR3byk7XG5cbiAgICAgICAgY2FzZSBcIldXXCI6XG4gICAgICAgICAgcmV0dXJuIGludFVuaXQodHdvKTtcbiAgICAgICAgLy8gd2Vla2RheXNcblxuICAgICAgICBjYXNlIFwiRVwiOlxuICAgICAgICBjYXNlIFwiY1wiOlxuICAgICAgICAgIHJldHVybiBpbnRVbml0KG9uZSk7XG5cbiAgICAgICAgY2FzZSBcIkVFRVwiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2Mud2Vla2RheXMoXCJzaG9ydFwiLCBmYWxzZSwgZmFsc2UpLCAxKTtcblxuICAgICAgICBjYXNlIFwiRUVFRVwiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2Mud2Vla2RheXMoXCJsb25nXCIsIGZhbHNlLCBmYWxzZSksIDEpO1xuXG4gICAgICAgIGNhc2UgXCJjY2NcIjpcbiAgICAgICAgICByZXR1cm4gb25lT2YobG9jLndlZWtkYXlzKFwic2hvcnRcIiwgdHJ1ZSwgZmFsc2UpLCAxKTtcblxuICAgICAgICBjYXNlIFwiY2NjY1wiOlxuICAgICAgICAgIHJldHVybiBvbmVPZihsb2Mud2Vla2RheXMoXCJsb25nXCIsIHRydWUsIGZhbHNlKSwgMSk7XG4gICAgICAgIC8vIG9mZnNldC96b25lXG5cbiAgICAgICAgY2FzZSBcIlpcIjpcbiAgICAgICAgY2FzZSBcIlpaXCI6XG4gICAgICAgICAgcmV0dXJuIG9mZnNldChuZXcgUmVnRXhwKFwiKFsrLV1cIiArIG9uZU9yVHdvLnNvdXJjZSArIFwiKSg/OjooXCIgKyB0d28uc291cmNlICsgXCIpKT9cIiksIDIpO1xuXG4gICAgICAgIGNhc2UgXCJaWlpcIjpcbiAgICAgICAgICByZXR1cm4gb2Zmc2V0KG5ldyBSZWdFeHAoXCIoWystXVwiICsgb25lT3JUd28uc291cmNlICsgXCIpKFwiICsgdHdvLnNvdXJjZSArIFwiKT9cIiksIDIpO1xuICAgICAgICAvLyB3ZSBkb24ndCBzdXBwb3J0IFpaWlogKFBTVCkgb3IgWlpaWlogKFBhY2lmaWMgU3RhbmRhcmQgVGltZSkgaW4gcGFyc2luZ1xuICAgICAgICAvLyBiZWNhdXNlIHdlIGRvbid0IGhhdmUgYW55IHdheSB0byBmaWd1cmUgb3V0IHdoYXQgdGhleSBhcmVcblxuICAgICAgICBjYXNlIFwielwiOlxuICAgICAgICAgIHJldHVybiBzaW1wbGUoL1thLXpfKy0vXXsxLDI1Nn0/L2kpO1xuXG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgcmV0dXJuIGxpdGVyYWwodCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIHZhciB1bml0ID0gdW5pdGF0ZSh0b2tlbikgfHwge1xuICAgICAgaW52YWxpZFJlYXNvbjogTUlTU0lOR19GVFBcbiAgICB9O1xuICAgIHVuaXQudG9rZW4gPSB0b2tlbjtcbiAgICByZXR1cm4gdW5pdDtcbiAgfVxuXG4gIHZhciBwYXJ0VHlwZVN0eWxlVG9Ub2tlblZhbCA9IHtcbiAgICB5ZWFyOiB7XG4gICAgICBcIjItZGlnaXRcIjogXCJ5eVwiLFxuICAgICAgbnVtZXJpYzogXCJ5eXl5eVwiXG4gICAgfSxcbiAgICBtb250aDoge1xuICAgICAgbnVtZXJpYzogXCJNXCIsXG4gICAgICBcIjItZGlnaXRcIjogXCJNTVwiLFxuICAgICAgc2hvcnQ6IFwiTU1NXCIsXG4gICAgICBsb25nOiBcIk1NTU1cIlxuICAgIH0sXG4gICAgZGF5OiB7XG4gICAgICBudW1lcmljOiBcImRcIixcbiAgICAgIFwiMi1kaWdpdFwiOiBcImRkXCJcbiAgICB9LFxuICAgIHdlZWtkYXk6IHtcbiAgICAgIHNob3J0OiBcIkVFRVwiLFxuICAgICAgbG9uZzogXCJFRUVFXCJcbiAgICB9LFxuICAgIGRheXBlcmlvZDogXCJhXCIsXG4gICAgZGF5UGVyaW9kOiBcImFcIixcbiAgICBob3VyOiB7XG4gICAgICBudW1lcmljOiBcImhcIixcbiAgICAgIFwiMi1kaWdpdFwiOiBcImhoXCJcbiAgICB9LFxuICAgIG1pbnV0ZToge1xuICAgICAgbnVtZXJpYzogXCJtXCIsXG4gICAgICBcIjItZGlnaXRcIjogXCJtbVwiXG4gICAgfSxcbiAgICBzZWNvbmQ6IHtcbiAgICAgIG51bWVyaWM6IFwic1wiLFxuICAgICAgXCIyLWRpZ2l0XCI6IFwic3NcIlxuICAgIH1cbiAgfTtcblxuICBmdW5jdGlvbiB0b2tlbkZvclBhcnQocGFydCwgbG9jYWxlLCBmb3JtYXRPcHRzKSB7XG4gICAgdmFyIHR5cGUgPSBwYXJ0LnR5cGUsXG4gICAgICAgIHZhbHVlID0gcGFydC52YWx1ZTtcblxuICAgIGlmICh0eXBlID09PSBcImxpdGVyYWxcIikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbGl0ZXJhbDogdHJ1ZSxcbiAgICAgICAgdmFsOiB2YWx1ZVxuICAgICAgfTtcbiAgICB9XG5cbiAgICB2YXIgc3R5bGUgPSBmb3JtYXRPcHRzW3R5cGVdO1xuICAgIHZhciB2YWwgPSBwYXJ0VHlwZVN0eWxlVG9Ub2tlblZhbFt0eXBlXTtcblxuICAgIGlmICh0eXBlb2YgdmFsID09PSBcIm9iamVjdFwiKSB7XG4gICAgICB2YWwgPSB2YWxbc3R5bGVdO1xuICAgIH1cblxuICAgIGlmICh2YWwpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxpdGVyYWw6IGZhbHNlLFxuICAgICAgICB2YWw6IHZhbFxuICAgICAgfTtcbiAgICB9XG5cbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9XG5cbiAgZnVuY3Rpb24gYnVpbGRSZWdleCh1bml0cykge1xuICAgIHZhciByZSA9IHVuaXRzLm1hcChmdW5jdGlvbiAodSkge1xuICAgICAgcmV0dXJuIHUucmVnZXg7XG4gICAgfSkucmVkdWNlKGZ1bmN0aW9uIChmLCByKSB7XG4gICAgICByZXR1cm4gZiArIFwiKFwiICsgci5zb3VyY2UgKyBcIilcIjtcbiAgICB9LCBcIlwiKTtcbiAgICByZXR1cm4gW1wiXlwiICsgcmUgKyBcIiRcIiwgdW5pdHNdO1xuICB9XG5cbiAgZnVuY3Rpb24gbWF0Y2goaW5wdXQsIHJlZ2V4LCBoYW5kbGVycykge1xuICAgIHZhciBtYXRjaGVzID0gaW5wdXQubWF0Y2gocmVnZXgpO1xuXG4gICAgaWYgKG1hdGNoZXMpIHtcbiAgICAgIHZhciBhbGwgPSB7fTtcbiAgICAgIHZhciBtYXRjaEluZGV4ID0gMTtcblxuICAgICAgZm9yICh2YXIgaSBpbiBoYW5kbGVycykge1xuICAgICAgICBpZiAoaGFzT3duUHJvcGVydHkoaGFuZGxlcnMsIGkpKSB7XG4gICAgICAgICAgdmFyIGggPSBoYW5kbGVyc1tpXSxcbiAgICAgICAgICAgICAgZ3JvdXBzID0gaC5ncm91cHMgPyBoLmdyb3VwcyArIDEgOiAxO1xuXG4gICAgICAgICAgaWYgKCFoLmxpdGVyYWwgJiYgaC50b2tlbikge1xuICAgICAgICAgICAgYWxsW2gudG9rZW4udmFsWzBdXSA9IGguZGVzZXIobWF0Y2hlcy5zbGljZShtYXRjaEluZGV4LCBtYXRjaEluZGV4ICsgZ3JvdXBzKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgbWF0Y2hJbmRleCArPSBncm91cHM7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIFttYXRjaGVzLCBhbGxdO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gW21hdGNoZXMsIHt9XTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBkYXRlVGltZUZyb21NYXRjaGVzKG1hdGNoZXMpIHtcbiAgICB2YXIgdG9GaWVsZCA9IGZ1bmN0aW9uIHRvRmllbGQodG9rZW4pIHtcbiAgICAgIHN3aXRjaCAodG9rZW4pIHtcbiAgICAgICAgY2FzZSBcIlNcIjpcbiAgICAgICAgICByZXR1cm4gXCJtaWxsaXNlY29uZFwiO1xuXG4gICAgICAgIGNhc2UgXCJzXCI6XG4gICAgICAgICAgcmV0dXJuIFwic2Vjb25kXCI7XG5cbiAgICAgICAgY2FzZSBcIm1cIjpcbiAgICAgICAgICByZXR1cm4gXCJtaW51dGVcIjtcblxuICAgICAgICBjYXNlIFwiaFwiOlxuICAgICAgICBjYXNlIFwiSFwiOlxuICAgICAgICAgIHJldHVybiBcImhvdXJcIjtcblxuICAgICAgICBjYXNlIFwiZFwiOlxuICAgICAgICAgIHJldHVybiBcImRheVwiO1xuXG4gICAgICAgIGNhc2UgXCJvXCI6XG4gICAgICAgICAgcmV0dXJuIFwib3JkaW5hbFwiO1xuXG4gICAgICAgIGNhc2UgXCJMXCI6XG4gICAgICAgIGNhc2UgXCJNXCI6XG4gICAgICAgICAgcmV0dXJuIFwibW9udGhcIjtcblxuICAgICAgICBjYXNlIFwieVwiOlxuICAgICAgICAgIHJldHVybiBcInllYXJcIjtcblxuICAgICAgICBjYXNlIFwiRVwiOlxuICAgICAgICBjYXNlIFwiY1wiOlxuICAgICAgICAgIHJldHVybiBcIndlZWtkYXlcIjtcblxuICAgICAgICBjYXNlIFwiV1wiOlxuICAgICAgICAgIHJldHVybiBcIndlZWtOdW1iZXJcIjtcblxuICAgICAgICBjYXNlIFwia1wiOlxuICAgICAgICAgIHJldHVybiBcIndlZWtZZWFyXCI7XG5cbiAgICAgICAgY2FzZSBcInFcIjpcbiAgICAgICAgICByZXR1cm4gXCJxdWFydGVyXCI7XG5cbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIHpvbmU7XG5cbiAgICBpZiAoIWlzVW5kZWZpbmVkKG1hdGNoZXMuWikpIHtcbiAgICAgIHpvbmUgPSBuZXcgRml4ZWRPZmZzZXRab25lKG1hdGNoZXMuWik7XG4gICAgfSBlbHNlIGlmICghaXNVbmRlZmluZWQobWF0Y2hlcy56KSkge1xuICAgICAgem9uZSA9IElBTkFab25lLmNyZWF0ZShtYXRjaGVzLnopO1xuICAgIH0gZWxzZSB7XG4gICAgICB6b25lID0gbnVsbDtcbiAgICB9XG5cbiAgICBpZiAoIWlzVW5kZWZpbmVkKG1hdGNoZXMucSkpIHtcbiAgICAgIG1hdGNoZXMuTSA9IChtYXRjaGVzLnEgLSAxKSAqIDMgKyAxO1xuICAgIH1cblxuICAgIGlmICghaXNVbmRlZmluZWQobWF0Y2hlcy5oKSkge1xuICAgICAgaWYgKG1hdGNoZXMuaCA8IDEyICYmIG1hdGNoZXMuYSA9PT0gMSkge1xuICAgICAgICBtYXRjaGVzLmggKz0gMTI7XG4gICAgICB9IGVsc2UgaWYgKG1hdGNoZXMuaCA9PT0gMTIgJiYgbWF0Y2hlcy5hID09PSAwKSB7XG4gICAgICAgIG1hdGNoZXMuaCA9IDA7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKG1hdGNoZXMuRyA9PT0gMCAmJiBtYXRjaGVzLnkpIHtcbiAgICAgIG1hdGNoZXMueSA9IC1tYXRjaGVzLnk7XG4gICAgfVxuXG4gICAgaWYgKCFpc1VuZGVmaW5lZChtYXRjaGVzLnUpKSB7XG4gICAgICBtYXRjaGVzLlMgPSBwYXJzZU1pbGxpcyhtYXRjaGVzLnUpO1xuICAgIH1cblxuICAgIHZhciB2YWxzID0gT2JqZWN0LmtleXMobWF0Y2hlcykucmVkdWNlKGZ1bmN0aW9uIChyLCBrKSB7XG4gICAgICB2YXIgZiA9IHRvRmllbGQoayk7XG5cbiAgICAgIGlmIChmKSB7XG4gICAgICAgIHJbZl0gPSBtYXRjaGVzW2tdO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcjtcbiAgICB9LCB7fSk7XG4gICAgcmV0dXJuIFt2YWxzLCB6b25lXTtcbiAgfVxuXG4gIHZhciBkdW1teURhdGVUaW1lQ2FjaGUgPSBudWxsO1xuXG4gIGZ1bmN0aW9uIGdldER1bW15RGF0ZVRpbWUoKSB7XG4gICAgaWYgKCFkdW1teURhdGVUaW1lQ2FjaGUpIHtcbiAgICAgIGR1bW15RGF0ZVRpbWVDYWNoZSA9IERhdGVUaW1lLmZyb21NaWxsaXMoMTU1NTU1NTU1NTU1NSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGR1bW15RGF0ZVRpbWVDYWNoZTtcbiAgfVxuXG4gIGZ1bmN0aW9uIG1heWJlRXhwYW5kTWFjcm9Ub2tlbih0b2tlbiwgbG9jYWxlKSB7XG4gICAgaWYgKHRva2VuLmxpdGVyYWwpIHtcbiAgICAgIHJldHVybiB0b2tlbjtcbiAgICB9XG5cbiAgICB2YXIgZm9ybWF0T3B0cyA9IEZvcm1hdHRlci5tYWNyb1Rva2VuVG9Gb3JtYXRPcHRzKHRva2VuLnZhbCk7XG5cbiAgICBpZiAoIWZvcm1hdE9wdHMpIHtcbiAgICAgIHJldHVybiB0b2tlbjtcbiAgICB9XG5cbiAgICB2YXIgZm9ybWF0dGVyID0gRm9ybWF0dGVyLmNyZWF0ZShsb2NhbGUsIGZvcm1hdE9wdHMpO1xuICAgIHZhciBwYXJ0cyA9IGZvcm1hdHRlci5mb3JtYXREYXRlVGltZVBhcnRzKGdldER1bW15RGF0ZVRpbWUoKSk7XG4gICAgdmFyIHRva2VucyA9IHBhcnRzLm1hcChmdW5jdGlvbiAocCkge1xuICAgICAgcmV0dXJuIHRva2VuRm9yUGFydChwLCBsb2NhbGUsIGZvcm1hdE9wdHMpO1xuICAgIH0pO1xuXG4gICAgaWYgKHRva2Vucy5pbmNsdWRlcyh1bmRlZmluZWQpKSB7XG4gICAgICByZXR1cm4gdG9rZW47XG4gICAgfVxuXG4gICAgcmV0dXJuIHRva2VucztcbiAgfVxuXG4gIGZ1bmN0aW9uIGV4cGFuZE1hY3JvVG9rZW5zKHRva2VucywgbG9jYWxlKSB7XG4gICAgdmFyIF9BcnJheSRwcm90b3R5cGU7XG5cbiAgICByZXR1cm4gKF9BcnJheSRwcm90b3R5cGUgPSBBcnJheS5wcm90b3R5cGUpLmNvbmNhdC5hcHBseShfQXJyYXkkcHJvdG90eXBlLCB0b2tlbnMubWFwKGZ1bmN0aW9uICh0KSB7XG4gICAgICByZXR1cm4gbWF5YmVFeHBhbmRNYWNyb1Rva2VuKHQsIGxvY2FsZSk7XG4gICAgfSkpO1xuICB9XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIGZ1bmN0aW9uIGV4cGxhaW5Gcm9tVG9rZW5zKGxvY2FsZSwgaW5wdXQsIGZvcm1hdCkge1xuICAgIHZhciB0b2tlbnMgPSBleHBhbmRNYWNyb1Rva2VucyhGb3JtYXR0ZXIucGFyc2VGb3JtYXQoZm9ybWF0KSwgbG9jYWxlKSxcbiAgICAgICAgdW5pdHMgPSB0b2tlbnMubWFwKGZ1bmN0aW9uICh0KSB7XG4gICAgICByZXR1cm4gdW5pdEZvclRva2VuKHQsIGxvY2FsZSk7XG4gICAgfSksXG4gICAgICAgIGRpc3F1YWxpZnlpbmdVbml0ID0gdW5pdHMuZmluZChmdW5jdGlvbiAodCkge1xuICAgICAgcmV0dXJuIHQuaW52YWxpZFJlYXNvbjtcbiAgICB9KTtcblxuICAgIGlmIChkaXNxdWFsaWZ5aW5nVW5pdCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaW5wdXQ6IGlucHV0LFxuICAgICAgICB0b2tlbnM6IHRva2VucyxcbiAgICAgICAgaW52YWxpZFJlYXNvbjogZGlzcXVhbGlmeWluZ1VuaXQuaW52YWxpZFJlYXNvblxuICAgICAgfTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIF9idWlsZFJlZ2V4ID0gYnVpbGRSZWdleCh1bml0cyksXG4gICAgICAgICAgcmVnZXhTdHJpbmcgPSBfYnVpbGRSZWdleFswXSxcbiAgICAgICAgICBoYW5kbGVycyA9IF9idWlsZFJlZ2V4WzFdLFxuICAgICAgICAgIHJlZ2V4ID0gUmVnRXhwKHJlZ2V4U3RyaW5nLCBcImlcIiksXG4gICAgICAgICAgX21hdGNoID0gbWF0Y2goaW5wdXQsIHJlZ2V4LCBoYW5kbGVycyksXG4gICAgICAgICAgcmF3TWF0Y2hlcyA9IF9tYXRjaFswXSxcbiAgICAgICAgICBtYXRjaGVzID0gX21hdGNoWzFdLFxuICAgICAgICAgIF9yZWY2ID0gbWF0Y2hlcyA/IGRhdGVUaW1lRnJvbU1hdGNoZXMobWF0Y2hlcykgOiBbbnVsbCwgbnVsbF0sXG4gICAgICAgICAgcmVzdWx0ID0gX3JlZjZbMF0sXG4gICAgICAgICAgem9uZSA9IF9yZWY2WzFdO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBpbnB1dDogaW5wdXQsXG4gICAgICAgIHRva2VuczogdG9rZW5zLFxuICAgICAgICByZWdleDogcmVnZXgsXG4gICAgICAgIHJhd01hdGNoZXM6IHJhd01hdGNoZXMsXG4gICAgICAgIG1hdGNoZXM6IG1hdGNoZXMsXG4gICAgICAgIHJlc3VsdDogcmVzdWx0LFxuICAgICAgICB6b25lOiB6b25lXG4gICAgICB9O1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiBwYXJzZUZyb21Ub2tlbnMobG9jYWxlLCBpbnB1dCwgZm9ybWF0KSB7XG4gICAgdmFyIF9leHBsYWluRnJvbVRva2VucyA9IGV4cGxhaW5Gcm9tVG9rZW5zKGxvY2FsZSwgaW5wdXQsIGZvcm1hdCksXG4gICAgICAgIHJlc3VsdCA9IF9leHBsYWluRnJvbVRva2Vucy5yZXN1bHQsXG4gICAgICAgIHpvbmUgPSBfZXhwbGFpbkZyb21Ub2tlbnMuem9uZSxcbiAgICAgICAgaW52YWxpZFJlYXNvbiA9IF9leHBsYWluRnJvbVRva2Vucy5pbnZhbGlkUmVhc29uO1xuXG4gICAgcmV0dXJuIFtyZXN1bHQsIHpvbmUsIGludmFsaWRSZWFzb25dO1xuICB9XG5cbiAgdmFyIG5vbkxlYXBMYWRkZXIgPSBbMCwgMzEsIDU5LCA5MCwgMTIwLCAxNTEsIDE4MSwgMjEyLCAyNDMsIDI3MywgMzA0LCAzMzRdLFxuICAgICAgbGVhcExhZGRlciA9IFswLCAzMSwgNjAsIDkxLCAxMjEsIDE1MiwgMTgyLCAyMTMsIDI0NCwgMjc0LCAzMDUsIDMzNV07XG5cbiAgZnVuY3Rpb24gdW5pdE91dE9mUmFuZ2UodW5pdCwgdmFsdWUpIHtcbiAgICByZXR1cm4gbmV3IEludmFsaWQoXCJ1bml0IG91dCBvZiByYW5nZVwiLCBcInlvdSBzcGVjaWZpZWQgXCIgKyB2YWx1ZSArIFwiIChvZiB0eXBlIFwiICsgdHlwZW9mIHZhbHVlICsgXCIpIGFzIGEgXCIgKyB1bml0ICsgXCIsIHdoaWNoIGlzIGludmFsaWRcIik7XG4gIH1cblxuICBmdW5jdGlvbiBkYXlPZldlZWsoeWVhciwgbW9udGgsIGRheSkge1xuICAgIHZhciBqcyA9IG5ldyBEYXRlKERhdGUuVVRDKHllYXIsIG1vbnRoIC0gMSwgZGF5KSkuZ2V0VVRDRGF5KCk7XG4gICAgcmV0dXJuIGpzID09PSAwID8gNyA6IGpzO1xuICB9XG5cbiAgZnVuY3Rpb24gY29tcHV0ZU9yZGluYWwoeWVhciwgbW9udGgsIGRheSkge1xuICAgIHJldHVybiBkYXkgKyAoaXNMZWFwWWVhcih5ZWFyKSA/IGxlYXBMYWRkZXIgOiBub25MZWFwTGFkZGVyKVttb250aCAtIDFdO1xuICB9XG5cbiAgZnVuY3Rpb24gdW5jb21wdXRlT3JkaW5hbCh5ZWFyLCBvcmRpbmFsKSB7XG4gICAgdmFyIHRhYmxlID0gaXNMZWFwWWVhcih5ZWFyKSA/IGxlYXBMYWRkZXIgOiBub25MZWFwTGFkZGVyLFxuICAgICAgICBtb250aDAgPSB0YWJsZS5maW5kSW5kZXgoZnVuY3Rpb24gKGkpIHtcbiAgICAgIHJldHVybiBpIDwgb3JkaW5hbDtcbiAgICB9KSxcbiAgICAgICAgZGF5ID0gb3JkaW5hbCAtIHRhYmxlW21vbnRoMF07XG4gICAgcmV0dXJuIHtcbiAgICAgIG1vbnRoOiBtb250aDAgKyAxLFxuICAgICAgZGF5OiBkYXlcbiAgICB9O1xuICB9XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIGZ1bmN0aW9uIGdyZWdvcmlhblRvV2VlayhncmVnT2JqKSB7XG4gICAgdmFyIHllYXIgPSBncmVnT2JqLnllYXIsXG4gICAgICAgIG1vbnRoID0gZ3JlZ09iai5tb250aCxcbiAgICAgICAgZGF5ID0gZ3JlZ09iai5kYXksXG4gICAgICAgIG9yZGluYWwgPSBjb21wdXRlT3JkaW5hbCh5ZWFyLCBtb250aCwgZGF5KSxcbiAgICAgICAgd2Vla2RheSA9IGRheU9mV2Vlayh5ZWFyLCBtb250aCwgZGF5KTtcbiAgICB2YXIgd2Vla051bWJlciA9IE1hdGguZmxvb3IoKG9yZGluYWwgLSB3ZWVrZGF5ICsgMTApIC8gNyksXG4gICAgICAgIHdlZWtZZWFyO1xuXG4gICAgaWYgKHdlZWtOdW1iZXIgPCAxKSB7XG4gICAgICB3ZWVrWWVhciA9IHllYXIgLSAxO1xuICAgICAgd2Vla051bWJlciA9IHdlZWtzSW5XZWVrWWVhcih3ZWVrWWVhcik7XG4gICAgfSBlbHNlIGlmICh3ZWVrTnVtYmVyID4gd2Vla3NJbldlZWtZZWFyKHllYXIpKSB7XG4gICAgICB3ZWVrWWVhciA9IHllYXIgKyAxO1xuICAgICAgd2Vla051bWJlciA9IDE7XG4gICAgfSBlbHNlIHtcbiAgICAgIHdlZWtZZWFyID0geWVhcjtcbiAgICB9XG5cbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbih7XG4gICAgICB3ZWVrWWVhcjogd2Vla1llYXIsXG4gICAgICB3ZWVrTnVtYmVyOiB3ZWVrTnVtYmVyLFxuICAgICAgd2Vla2RheTogd2Vla2RheVxuICAgIH0sIHRpbWVPYmplY3QoZ3JlZ09iaikpO1xuICB9XG4gIGZ1bmN0aW9uIHdlZWtUb0dyZWdvcmlhbih3ZWVrRGF0YSkge1xuICAgIHZhciB3ZWVrWWVhciA9IHdlZWtEYXRhLndlZWtZZWFyLFxuICAgICAgICB3ZWVrTnVtYmVyID0gd2Vla0RhdGEud2Vla051bWJlcixcbiAgICAgICAgd2Vla2RheSA9IHdlZWtEYXRhLndlZWtkYXksXG4gICAgICAgIHdlZWtkYXlPZkphbjQgPSBkYXlPZldlZWsod2Vla1llYXIsIDEsIDQpLFxuICAgICAgICB5ZWFySW5EYXlzID0gZGF5c0luWWVhcih3ZWVrWWVhcik7XG4gICAgdmFyIG9yZGluYWwgPSB3ZWVrTnVtYmVyICogNyArIHdlZWtkYXkgLSB3ZWVrZGF5T2ZKYW40IC0gMyxcbiAgICAgICAgeWVhcjtcblxuICAgIGlmIChvcmRpbmFsIDwgMSkge1xuICAgICAgeWVhciA9IHdlZWtZZWFyIC0gMTtcbiAgICAgIG9yZGluYWwgKz0gZGF5c0luWWVhcih5ZWFyKTtcbiAgICB9IGVsc2UgaWYgKG9yZGluYWwgPiB5ZWFySW5EYXlzKSB7XG4gICAgICB5ZWFyID0gd2Vla1llYXIgKyAxO1xuICAgICAgb3JkaW5hbCAtPSBkYXlzSW5ZZWFyKHdlZWtZZWFyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgeWVhciA9IHdlZWtZZWFyO1xuICAgIH1cblxuICAgIHZhciBfdW5jb21wdXRlT3JkaW5hbCA9IHVuY29tcHV0ZU9yZGluYWwoeWVhciwgb3JkaW5hbCksXG4gICAgICAgIG1vbnRoID0gX3VuY29tcHV0ZU9yZGluYWwubW9udGgsXG4gICAgICAgIGRheSA9IF91bmNvbXB1dGVPcmRpbmFsLmRheTtcblxuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHtcbiAgICAgIHllYXI6IHllYXIsXG4gICAgICBtb250aDogbW9udGgsXG4gICAgICBkYXk6IGRheVxuICAgIH0sIHRpbWVPYmplY3Qod2Vla0RhdGEpKTtcbiAgfVxuICBmdW5jdGlvbiBncmVnb3JpYW5Ub09yZGluYWwoZ3JlZ0RhdGEpIHtcbiAgICB2YXIgeWVhciA9IGdyZWdEYXRhLnllYXIsXG4gICAgICAgIG1vbnRoID0gZ3JlZ0RhdGEubW9udGgsXG4gICAgICAgIGRheSA9IGdyZWdEYXRhLmRheSxcbiAgICAgICAgb3JkaW5hbCA9IGNvbXB1dGVPcmRpbmFsKHllYXIsIG1vbnRoLCBkYXkpO1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHtcbiAgICAgIHllYXI6IHllYXIsXG4gICAgICBvcmRpbmFsOiBvcmRpbmFsXG4gICAgfSwgdGltZU9iamVjdChncmVnRGF0YSkpO1xuICB9XG4gIGZ1bmN0aW9uIG9yZGluYWxUb0dyZWdvcmlhbihvcmRpbmFsRGF0YSkge1xuICAgIHZhciB5ZWFyID0gb3JkaW5hbERhdGEueWVhcixcbiAgICAgICAgb3JkaW5hbCA9IG9yZGluYWxEYXRhLm9yZGluYWwsXG4gICAgICAgIF91bmNvbXB1dGVPcmRpbmFsMiA9IHVuY29tcHV0ZU9yZGluYWwoeWVhciwgb3JkaW5hbCksXG4gICAgICAgIG1vbnRoID0gX3VuY29tcHV0ZU9yZGluYWwyLm1vbnRoLFxuICAgICAgICBkYXkgPSBfdW5jb21wdXRlT3JkaW5hbDIuZGF5O1xuXG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe1xuICAgICAgeWVhcjogeWVhcixcbiAgICAgIG1vbnRoOiBtb250aCxcbiAgICAgIGRheTogZGF5XG4gICAgfSwgdGltZU9iamVjdChvcmRpbmFsRGF0YSkpO1xuICB9XG4gIGZ1bmN0aW9uIGhhc0ludmFsaWRXZWVrRGF0YShvYmopIHtcbiAgICB2YXIgdmFsaWRZZWFyID0gaXNJbnRlZ2VyKG9iai53ZWVrWWVhciksXG4gICAgICAgIHZhbGlkV2VlayA9IGludGVnZXJCZXR3ZWVuKG9iai53ZWVrTnVtYmVyLCAxLCB3ZWVrc0luV2Vla1llYXIob2JqLndlZWtZZWFyKSksXG4gICAgICAgIHZhbGlkV2Vla2RheSA9IGludGVnZXJCZXR3ZWVuKG9iai53ZWVrZGF5LCAxLCA3KTtcblxuICAgIGlmICghdmFsaWRZZWFyKSB7XG4gICAgICByZXR1cm4gdW5pdE91dE9mUmFuZ2UoXCJ3ZWVrWWVhclwiLCBvYmoud2Vla1llYXIpO1xuICAgIH0gZWxzZSBpZiAoIXZhbGlkV2Vlaykge1xuICAgICAgcmV0dXJuIHVuaXRPdXRPZlJhbmdlKFwid2Vla1wiLCBvYmoud2Vlayk7XG4gICAgfSBlbHNlIGlmICghdmFsaWRXZWVrZGF5KSB7XG4gICAgICByZXR1cm4gdW5pdE91dE9mUmFuZ2UoXCJ3ZWVrZGF5XCIsIG9iai53ZWVrZGF5KTtcbiAgICB9IGVsc2UgcmV0dXJuIGZhbHNlO1xuICB9XG4gIGZ1bmN0aW9uIGhhc0ludmFsaWRPcmRpbmFsRGF0YShvYmopIHtcbiAgICB2YXIgdmFsaWRZZWFyID0gaXNJbnRlZ2VyKG9iai55ZWFyKSxcbiAgICAgICAgdmFsaWRPcmRpbmFsID0gaW50ZWdlckJldHdlZW4ob2JqLm9yZGluYWwsIDEsIGRheXNJblllYXIob2JqLnllYXIpKTtcblxuICAgIGlmICghdmFsaWRZZWFyKSB7XG4gICAgICByZXR1cm4gdW5pdE91dE9mUmFuZ2UoXCJ5ZWFyXCIsIG9iai55ZWFyKTtcbiAgICB9IGVsc2UgaWYgKCF2YWxpZE9yZGluYWwpIHtcbiAgICAgIHJldHVybiB1bml0T3V0T2ZSYW5nZShcIm9yZGluYWxcIiwgb2JqLm9yZGluYWwpO1xuICAgIH0gZWxzZSByZXR1cm4gZmFsc2U7XG4gIH1cbiAgZnVuY3Rpb24gaGFzSW52YWxpZEdyZWdvcmlhbkRhdGEob2JqKSB7XG4gICAgdmFyIHZhbGlkWWVhciA9IGlzSW50ZWdlcihvYmoueWVhciksXG4gICAgICAgIHZhbGlkTW9udGggPSBpbnRlZ2VyQmV0d2VlbihvYmoubW9udGgsIDEsIDEyKSxcbiAgICAgICAgdmFsaWREYXkgPSBpbnRlZ2VyQmV0d2VlbihvYmouZGF5LCAxLCBkYXlzSW5Nb250aChvYmoueWVhciwgb2JqLm1vbnRoKSk7XG5cbiAgICBpZiAoIXZhbGlkWWVhcikge1xuICAgICAgcmV0dXJuIHVuaXRPdXRPZlJhbmdlKFwieWVhclwiLCBvYmoueWVhcik7XG4gICAgfSBlbHNlIGlmICghdmFsaWRNb250aCkge1xuICAgICAgcmV0dXJuIHVuaXRPdXRPZlJhbmdlKFwibW9udGhcIiwgb2JqLm1vbnRoKTtcbiAgICB9IGVsc2UgaWYgKCF2YWxpZERheSkge1xuICAgICAgcmV0dXJuIHVuaXRPdXRPZlJhbmdlKFwiZGF5XCIsIG9iai5kYXkpO1xuICAgIH0gZWxzZSByZXR1cm4gZmFsc2U7XG4gIH1cbiAgZnVuY3Rpb24gaGFzSW52YWxpZFRpbWVEYXRhKG9iaikge1xuICAgIHZhciBob3VyID0gb2JqLmhvdXIsXG4gICAgICAgIG1pbnV0ZSA9IG9iai5taW51dGUsXG4gICAgICAgIHNlY29uZCA9IG9iai5zZWNvbmQsXG4gICAgICAgIG1pbGxpc2Vjb25kID0gb2JqLm1pbGxpc2Vjb25kO1xuICAgIHZhciB2YWxpZEhvdXIgPSBpbnRlZ2VyQmV0d2Vlbihob3VyLCAwLCAyMykgfHwgaG91ciA9PT0gMjQgJiYgbWludXRlID09PSAwICYmIHNlY29uZCA9PT0gMCAmJiBtaWxsaXNlY29uZCA9PT0gMCxcbiAgICAgICAgdmFsaWRNaW51dGUgPSBpbnRlZ2VyQmV0d2VlbihtaW51dGUsIDAsIDU5KSxcbiAgICAgICAgdmFsaWRTZWNvbmQgPSBpbnRlZ2VyQmV0d2VlbihzZWNvbmQsIDAsIDU5KSxcbiAgICAgICAgdmFsaWRNaWxsaXNlY29uZCA9IGludGVnZXJCZXR3ZWVuKG1pbGxpc2Vjb25kLCAwLCA5OTkpO1xuXG4gICAgaWYgKCF2YWxpZEhvdXIpIHtcbiAgICAgIHJldHVybiB1bml0T3V0T2ZSYW5nZShcImhvdXJcIiwgaG91cik7XG4gICAgfSBlbHNlIGlmICghdmFsaWRNaW51dGUpIHtcbiAgICAgIHJldHVybiB1bml0T3V0T2ZSYW5nZShcIm1pbnV0ZVwiLCBtaW51dGUpO1xuICAgIH0gZWxzZSBpZiAoIXZhbGlkU2Vjb25kKSB7XG4gICAgICByZXR1cm4gdW5pdE91dE9mUmFuZ2UoXCJzZWNvbmRcIiwgc2Vjb25kKTtcbiAgICB9IGVsc2UgaWYgKCF2YWxpZE1pbGxpc2Vjb25kKSB7XG4gICAgICByZXR1cm4gdW5pdE91dE9mUmFuZ2UoXCJtaWxsaXNlY29uZFwiLCBtaWxsaXNlY29uZCk7XG4gICAgfSBlbHNlIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHZhciBJTlZBTElEJDIgPSBcIkludmFsaWQgRGF0ZVRpbWVcIjtcbiAgdmFyIE1BWF9EQVRFID0gOC42NGUxNTtcblxuICBmdW5jdGlvbiB1bnN1cHBvcnRlZFpvbmUoem9uZSkge1xuICAgIHJldHVybiBuZXcgSW52YWxpZChcInVuc3VwcG9ydGVkIHpvbmVcIiwgXCJ0aGUgem9uZSBcXFwiXCIgKyB6b25lLm5hbWUgKyBcIlxcXCIgaXMgbm90IHN1cHBvcnRlZFwiKTtcbiAgfSAvLyB3ZSBjYWNoZSB3ZWVrIGRhdGEgb24gdGhlIERUIG9iamVjdCBhbmQgdGhpcyBpbnRlcm1lZGlhdGVzIHRoZSBjYWNoZVxuXG5cbiAgZnVuY3Rpb24gcG9zc2libHlDYWNoZWRXZWVrRGF0YShkdCkge1xuICAgIGlmIChkdC53ZWVrRGF0YSA9PT0gbnVsbCkge1xuICAgICAgZHQud2Vla0RhdGEgPSBncmVnb3JpYW5Ub1dlZWsoZHQuYyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGR0LndlZWtEYXRhO1xuICB9IC8vIGNsb25lIHJlYWxseSBtZWFucywgXCJtYWtlIGEgbmV3IG9iamVjdCB3aXRoIHRoZXNlIG1vZGlmaWNhdGlvbnNcIi4gYWxsIFwic2V0dGVyc1wiIHJlYWxseSB1c2UgdGhpc1xuICAvLyB0byBjcmVhdGUgYSBuZXcgb2JqZWN0IHdoaWxlIG9ubHkgY2hhbmdpbmcgc29tZSBvZiB0aGUgcHJvcGVydGllc1xuXG5cbiAgZnVuY3Rpb24gY2xvbmUkMShpbnN0LCBhbHRzKSB7XG4gICAgdmFyIGN1cnJlbnQgPSB7XG4gICAgICB0czogaW5zdC50cyxcbiAgICAgIHpvbmU6IGluc3Quem9uZSxcbiAgICAgIGM6IGluc3QuYyxcbiAgICAgIG86IGluc3QubyxcbiAgICAgIGxvYzogaW5zdC5sb2MsXG4gICAgICBpbnZhbGlkOiBpbnN0LmludmFsaWRcbiAgICB9O1xuICAgIHJldHVybiBuZXcgRGF0ZVRpbWUoT2JqZWN0LmFzc2lnbih7fSwgY3VycmVudCwgYWx0cywge1xuICAgICAgb2xkOiBjdXJyZW50XG4gICAgfSkpO1xuICB9IC8vIGZpbmQgdGhlIHJpZ2h0IG9mZnNldCBhIGdpdmVuIGxvY2FsIHRpbWUuIFRoZSBvIGlucHV0IGlzIG91ciBndWVzcywgd2hpY2ggZGV0ZXJtaW5lcyB3aGljaFxuICAvLyBvZmZzZXQgd2UnbGwgcGljayBpbiBhbWJpZ3VvdXMgY2FzZXMgKGUuZy4gdGhlcmUgYXJlIHR3byAzIEFNcyBiL2MgRmFsbGJhY2sgRFNUKVxuXG5cbiAgZnVuY3Rpb24gZml4T2Zmc2V0KGxvY2FsVFMsIG8sIHR6KSB7XG4gICAgLy8gT3VyIFVUQyB0aW1lIGlzIGp1c3QgYSBndWVzcyBiZWNhdXNlIG91ciBvZmZzZXQgaXMganVzdCBhIGd1ZXNzXG4gICAgdmFyIHV0Y0d1ZXNzID0gbG9jYWxUUyAtIG8gKiA2MCAqIDEwMDA7IC8vIFRlc3Qgd2hldGhlciB0aGUgem9uZSBtYXRjaGVzIHRoZSBvZmZzZXQgZm9yIHRoaXMgdHNcblxuICAgIHZhciBvMiA9IHR6Lm9mZnNldCh1dGNHdWVzcyk7IC8vIElmIHNvLCBvZmZzZXQgZGlkbid0IGNoYW5nZSBhbmQgd2UncmUgZG9uZVxuXG4gICAgaWYgKG8gPT09IG8yKSB7XG4gICAgICByZXR1cm4gW3V0Y0d1ZXNzLCBvXTtcbiAgICB9IC8vIElmIG5vdCwgY2hhbmdlIHRoZSB0cyBieSB0aGUgZGlmZmVyZW5jZSBpbiB0aGUgb2Zmc2V0XG5cblxuICAgIHV0Y0d1ZXNzIC09IChvMiAtIG8pICogNjAgKiAxMDAwOyAvLyBJZiB0aGF0IGdpdmVzIHVzIHRoZSBsb2NhbCB0aW1lIHdlIHdhbnQsIHdlJ3JlIGRvbmVcblxuICAgIHZhciBvMyA9IHR6Lm9mZnNldCh1dGNHdWVzcyk7XG5cbiAgICBpZiAobzIgPT09IG8zKSB7XG4gICAgICByZXR1cm4gW3V0Y0d1ZXNzLCBvMl07XG4gICAgfSAvLyBJZiBpdCdzIGRpZmZlcmVudCwgd2UncmUgaW4gYSBob2xlIHRpbWUuIFRoZSBvZmZzZXQgaGFzIGNoYW5nZWQsIGJ1dCB0aGUgd2UgZG9uJ3QgYWRqdXN0IHRoZSB0aW1lXG5cblxuICAgIHJldHVybiBbbG9jYWxUUyAtIE1hdGgubWluKG8yLCBvMykgKiA2MCAqIDEwMDAsIE1hdGgubWF4KG8yLCBvMyldO1xuICB9IC8vIGNvbnZlcnQgYW4gZXBvY2ggdGltZXN0YW1wIGludG8gYSBjYWxlbmRhciBvYmplY3Qgd2l0aCB0aGUgZ2l2ZW4gb2Zmc2V0XG5cblxuICBmdW5jdGlvbiB0c1RvT2JqKHRzLCBvZmZzZXQpIHtcbiAgICB0cyArPSBvZmZzZXQgKiA2MCAqIDEwMDA7XG4gICAgdmFyIGQgPSBuZXcgRGF0ZSh0cyk7XG4gICAgcmV0dXJuIHtcbiAgICAgIHllYXI6IGQuZ2V0VVRDRnVsbFllYXIoKSxcbiAgICAgIG1vbnRoOiBkLmdldFVUQ01vbnRoKCkgKyAxLFxuICAgICAgZGF5OiBkLmdldFVUQ0RhdGUoKSxcbiAgICAgIGhvdXI6IGQuZ2V0VVRDSG91cnMoKSxcbiAgICAgIG1pbnV0ZTogZC5nZXRVVENNaW51dGVzKCksXG4gICAgICBzZWNvbmQ6IGQuZ2V0VVRDU2Vjb25kcygpLFxuICAgICAgbWlsbGlzZWNvbmQ6IGQuZ2V0VVRDTWlsbGlzZWNvbmRzKClcbiAgICB9O1xuICB9IC8vIGNvbnZlcnQgYSBjYWxlbmRhciBvYmplY3QgdG8gYSBlcG9jaCB0aW1lc3RhbXBcblxuXG4gIGZ1bmN0aW9uIG9ialRvVFMob2JqLCBvZmZzZXQsIHpvbmUpIHtcbiAgICByZXR1cm4gZml4T2Zmc2V0KG9ialRvTG9jYWxUUyhvYmopLCBvZmZzZXQsIHpvbmUpO1xuICB9IC8vIGNyZWF0ZSBhIG5ldyBEVCBpbnN0YW5jZSBieSBhZGRpbmcgYSBkdXJhdGlvbiwgYWRqdXN0aW5nIGZvciBEU1RzXG5cblxuICBmdW5jdGlvbiBhZGp1c3RUaW1lKGluc3QsIGR1cikge1xuICAgIHZhciBfZHVyO1xuXG4gICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhkdXIudmFsdWVzKTtcblxuICAgIGlmIChrZXlzLmluZGV4T2YoXCJtaWxsaXNlY29uZHNcIikgPT09IC0xKSB7XG4gICAgICBrZXlzLnB1c2goXCJtaWxsaXNlY29uZHNcIik7XG4gICAgfVxuXG4gICAgZHVyID0gKF9kdXIgPSBkdXIpLnNoaWZ0VG8uYXBwbHkoX2R1ciwga2V5cyk7XG4gICAgdmFyIG9QcmUgPSBpbnN0Lm8sXG4gICAgICAgIHllYXIgPSBpbnN0LmMueWVhciArIGR1ci55ZWFycyxcbiAgICAgICAgbW9udGggPSBpbnN0LmMubW9udGggKyBkdXIubW9udGhzICsgZHVyLnF1YXJ0ZXJzICogMyxcbiAgICAgICAgYyA9IE9iamVjdC5hc3NpZ24oe30sIGluc3QuYywge1xuICAgICAgeWVhcjogeWVhcixcbiAgICAgIG1vbnRoOiBtb250aCxcbiAgICAgIGRheTogTWF0aC5taW4oaW5zdC5jLmRheSwgZGF5c0luTW9udGgoeWVhciwgbW9udGgpKSArIGR1ci5kYXlzICsgZHVyLndlZWtzICogN1xuICAgIH0pLFxuICAgICAgICBtaWxsaXNUb0FkZCA9IER1cmF0aW9uLmZyb21PYmplY3Qoe1xuICAgICAgaG91cnM6IGR1ci5ob3VycyxcbiAgICAgIG1pbnV0ZXM6IGR1ci5taW51dGVzLFxuICAgICAgc2Vjb25kczogZHVyLnNlY29uZHMsXG4gICAgICBtaWxsaXNlY29uZHM6IGR1ci5taWxsaXNlY29uZHNcbiAgICB9KS5hcyhcIm1pbGxpc2Vjb25kc1wiKSxcbiAgICAgICAgbG9jYWxUUyA9IG9ialRvTG9jYWxUUyhjKTtcblxuICAgIHZhciBfZml4T2Zmc2V0ID0gZml4T2Zmc2V0KGxvY2FsVFMsIG9QcmUsIGluc3Quem9uZSksXG4gICAgICAgIHRzID0gX2ZpeE9mZnNldFswXSxcbiAgICAgICAgbyA9IF9maXhPZmZzZXRbMV07XG5cbiAgICBpZiAobWlsbGlzVG9BZGQgIT09IDApIHtcbiAgICAgIHRzICs9IG1pbGxpc1RvQWRkOyAvLyB0aGF0IGNvdWxkIGhhdmUgY2hhbmdlZCB0aGUgb2Zmc2V0IGJ5IGdvaW5nIG92ZXIgYSBEU1QsIGJ1dCB3ZSB3YW50IHRvIGtlZXAgdGhlIHRzIHRoZSBzYW1lXG5cbiAgICAgIG8gPSBpbnN0LnpvbmUub2Zmc2V0KHRzKTtcbiAgICB9XG5cbiAgICByZXR1cm4ge1xuICAgICAgdHM6IHRzLFxuICAgICAgbzogb1xuICAgIH07XG4gIH0gLy8gaGVscGVyIHVzZWZ1bCBpbiB0dXJuaW5nIHRoZSByZXN1bHRzIG9mIHBhcnNpbmcgaW50byByZWFsIGRhdGVzXG4gIC8vIGJ5IGhhbmRsaW5nIHRoZSB6b25lIG9wdGlvbnNcblxuXG4gIGZ1bmN0aW9uIHBhcnNlRGF0YVRvRGF0ZVRpbWUocGFyc2VkLCBwYXJzZWRab25lLCBvcHRzLCBmb3JtYXQsIHRleHQpIHtcbiAgICB2YXIgc2V0Wm9uZSA9IG9wdHMuc2V0Wm9uZSxcbiAgICAgICAgem9uZSA9IG9wdHMuem9uZTtcblxuICAgIGlmIChwYXJzZWQgJiYgT2JqZWN0LmtleXMocGFyc2VkKS5sZW5ndGggIT09IDApIHtcbiAgICAgIHZhciBpbnRlcnByZXRhdGlvblpvbmUgPSBwYXJzZWRab25lIHx8IHpvbmUsXG4gICAgICAgICAgaW5zdCA9IERhdGVUaW1lLmZyb21PYmplY3QoT2JqZWN0LmFzc2lnbihwYXJzZWQsIG9wdHMsIHtcbiAgICAgICAgem9uZTogaW50ZXJwcmV0YXRpb25ab25lLFxuICAgICAgICAvLyBzZXRab25lIGlzIGEgdmFsaWQgb3B0aW9uIGluIHRoZSBjYWxsaW5nIG1ldGhvZHMsIGJ1dCBub3QgaW4gZnJvbU9iamVjdFxuICAgICAgICBzZXRab25lOiB1bmRlZmluZWRcbiAgICAgIH0pKTtcbiAgICAgIHJldHVybiBzZXRab25lID8gaW5zdCA6IGluc3Quc2V0Wm9uZSh6b25lKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIERhdGVUaW1lLmludmFsaWQobmV3IEludmFsaWQoXCJ1bnBhcnNhYmxlXCIsIFwidGhlIGlucHV0IFxcXCJcIiArIHRleHQgKyBcIlxcXCIgY2FuJ3QgYmUgcGFyc2VkIGFzIFwiICsgZm9ybWF0KSk7XG4gICAgfVxuICB9IC8vIGlmIHlvdSB3YW50IHRvIG91dHB1dCBhIHRlY2huaWNhbCBmb3JtYXQgKGUuZy4gUkZDIDI4MjIpLCB0aGlzIGhlbHBlclxuICAvLyBoZWxwcyBoYW5kbGUgdGhlIGRldGFpbHNcblxuXG4gIGZ1bmN0aW9uIHRvVGVjaEZvcm1hdChkdCwgZm9ybWF0KSB7XG4gICAgcmV0dXJuIGR0LmlzVmFsaWQgPyBGb3JtYXR0ZXIuY3JlYXRlKExvY2FsZS5jcmVhdGUoXCJlbi1VU1wiKSwge1xuICAgICAgYWxsb3daOiB0cnVlLFxuICAgICAgZm9yY2VTaW1wbGU6IHRydWVcbiAgICB9KS5mb3JtYXREYXRlVGltZUZyb21TdHJpbmcoZHQsIGZvcm1hdCkgOiBudWxsO1xuICB9IC8vIHRlY2huaWNhbCB0aW1lIGZvcm1hdHMgKGUuZy4gdGhlIHRpbWUgcGFydCBvZiBJU08gODYwMSksIHRha2Ugc29tZSBvcHRpb25zXG4gIC8vIGFuZCB0aGlzIGNvbW1vbml6ZXMgdGhlaXIgaGFuZGxpbmdcblxuXG4gIGZ1bmN0aW9uIHRvVGVjaFRpbWVGb3JtYXQoZHQsIF9yZWYpIHtcbiAgICB2YXIgX3JlZiRzdXBwcmVzc1NlY29uZHMgPSBfcmVmLnN1cHByZXNzU2Vjb25kcyxcbiAgICAgICAgc3VwcHJlc3NTZWNvbmRzID0gX3JlZiRzdXBwcmVzc1NlY29uZHMgPT09IHZvaWQgMCA/IGZhbHNlIDogX3JlZiRzdXBwcmVzc1NlY29uZHMsXG4gICAgICAgIF9yZWYkc3VwcHJlc3NNaWxsaXNlYyA9IF9yZWYuc3VwcHJlc3NNaWxsaXNlY29uZHMsXG4gICAgICAgIHN1cHByZXNzTWlsbGlzZWNvbmRzID0gX3JlZiRzdXBwcmVzc01pbGxpc2VjID09PSB2b2lkIDAgPyBmYWxzZSA6IF9yZWYkc3VwcHJlc3NNaWxsaXNlYyxcbiAgICAgICAgaW5jbHVkZU9mZnNldCA9IF9yZWYuaW5jbHVkZU9mZnNldCxcbiAgICAgICAgX3JlZiRpbmNsdWRlWm9uZSA9IF9yZWYuaW5jbHVkZVpvbmUsXG4gICAgICAgIGluY2x1ZGVab25lID0gX3JlZiRpbmNsdWRlWm9uZSA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcmVmJGluY2x1ZGVab25lLFxuICAgICAgICBfcmVmJHNwYWNlWm9uZSA9IF9yZWYuc3BhY2Vab25lLFxuICAgICAgICBzcGFjZVpvbmUgPSBfcmVmJHNwYWNlWm9uZSA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcmVmJHNwYWNlWm9uZTtcbiAgICB2YXIgZm10ID0gXCJISDptbVwiO1xuXG4gICAgaWYgKCFzdXBwcmVzc1NlY29uZHMgfHwgZHQuc2Vjb25kICE9PSAwIHx8IGR0Lm1pbGxpc2Vjb25kICE9PSAwKSB7XG4gICAgICBmbXQgKz0gXCI6c3NcIjtcblxuICAgICAgaWYgKCFzdXBwcmVzc01pbGxpc2Vjb25kcyB8fCBkdC5taWxsaXNlY29uZCAhPT0gMCkge1xuICAgICAgICBmbXQgKz0gXCIuU1NTXCI7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKChpbmNsdWRlWm9uZSB8fCBpbmNsdWRlT2Zmc2V0KSAmJiBzcGFjZVpvbmUpIHtcbiAgICAgIGZtdCArPSBcIiBcIjtcbiAgICB9XG5cbiAgICBpZiAoaW5jbHVkZVpvbmUpIHtcbiAgICAgIGZtdCArPSBcInpcIjtcbiAgICB9IGVsc2UgaWYgKGluY2x1ZGVPZmZzZXQpIHtcbiAgICAgIGZtdCArPSBcIlpaXCI7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRvVGVjaEZvcm1hdChkdCwgZm10KTtcbiAgfSAvLyBkZWZhdWx0cyBmb3IgdW5zcGVjaWZpZWQgdW5pdHMgaW4gdGhlIHN1cHBvcnRlZCBjYWxlbmRhcnNcblxuXG4gIHZhciBkZWZhdWx0VW5pdFZhbHVlcyA9IHtcbiAgICBtb250aDogMSxcbiAgICBkYXk6IDEsXG4gICAgaG91cjogMCxcbiAgICBtaW51dGU6IDAsXG4gICAgc2Vjb25kOiAwLFxuICAgIG1pbGxpc2Vjb25kOiAwXG4gIH0sXG4gICAgICBkZWZhdWx0V2Vla1VuaXRWYWx1ZXMgPSB7XG4gICAgd2Vla051bWJlcjogMSxcbiAgICB3ZWVrZGF5OiAxLFxuICAgIGhvdXI6IDAsXG4gICAgbWludXRlOiAwLFxuICAgIHNlY29uZDogMCxcbiAgICBtaWxsaXNlY29uZDogMFxuICB9LFxuICAgICAgZGVmYXVsdE9yZGluYWxVbml0VmFsdWVzID0ge1xuICAgIG9yZGluYWw6IDEsXG4gICAgaG91cjogMCxcbiAgICBtaW51dGU6IDAsXG4gICAgc2Vjb25kOiAwLFxuICAgIG1pbGxpc2Vjb25kOiAwXG4gIH07IC8vIFVuaXRzIGluIHRoZSBzdXBwb3J0ZWQgY2FsZW5kYXJzLCBzb3J0ZWQgYnkgYmlnbmVzc1xuXG4gIHZhciBvcmRlcmVkVW5pdHMkMSA9IFtcInllYXJcIiwgXCJtb250aFwiLCBcImRheVwiLCBcImhvdXJcIiwgXCJtaW51dGVcIiwgXCJzZWNvbmRcIiwgXCJtaWxsaXNlY29uZFwiXSxcbiAgICAgIG9yZGVyZWRXZWVrVW5pdHMgPSBbXCJ3ZWVrWWVhclwiLCBcIndlZWtOdW1iZXJcIiwgXCJ3ZWVrZGF5XCIsIFwiaG91clwiLCBcIm1pbnV0ZVwiLCBcInNlY29uZFwiLCBcIm1pbGxpc2Vjb25kXCJdLFxuICAgICAgb3JkZXJlZE9yZGluYWxVbml0cyA9IFtcInllYXJcIiwgXCJvcmRpbmFsXCIsIFwiaG91clwiLCBcIm1pbnV0ZVwiLCBcInNlY29uZFwiLCBcIm1pbGxpc2Vjb25kXCJdOyAvLyBzdGFuZGFyZGl6ZSBjYXNlIGFuZCBwbHVyYWxpdHkgaW4gdW5pdHNcblxuICBmdW5jdGlvbiBub3JtYWxpemVVbml0KHVuaXQpIHtcbiAgICB2YXIgbm9ybWFsaXplZCA9IHtcbiAgICAgIHllYXI6IFwieWVhclwiLFxuICAgICAgeWVhcnM6IFwieWVhclwiLFxuICAgICAgbW9udGg6IFwibW9udGhcIixcbiAgICAgIG1vbnRoczogXCJtb250aFwiLFxuICAgICAgZGF5OiBcImRheVwiLFxuICAgICAgZGF5czogXCJkYXlcIixcbiAgICAgIGhvdXI6IFwiaG91clwiLFxuICAgICAgaG91cnM6IFwiaG91clwiLFxuICAgICAgbWludXRlOiBcIm1pbnV0ZVwiLFxuICAgICAgbWludXRlczogXCJtaW51dGVcIixcbiAgICAgIHF1YXJ0ZXI6IFwicXVhcnRlclwiLFxuICAgICAgcXVhcnRlcnM6IFwicXVhcnRlclwiLFxuICAgICAgc2Vjb25kOiBcInNlY29uZFwiLFxuICAgICAgc2Vjb25kczogXCJzZWNvbmRcIixcbiAgICAgIG1pbGxpc2Vjb25kOiBcIm1pbGxpc2Vjb25kXCIsXG4gICAgICBtaWxsaXNlY29uZHM6IFwibWlsbGlzZWNvbmRcIixcbiAgICAgIHdlZWtkYXk6IFwid2Vla2RheVwiLFxuICAgICAgd2Vla2RheXM6IFwid2Vla2RheVwiLFxuICAgICAgd2Vla251bWJlcjogXCJ3ZWVrTnVtYmVyXCIsXG4gICAgICB3ZWVrc251bWJlcjogXCJ3ZWVrTnVtYmVyXCIsXG4gICAgICB3ZWVrbnVtYmVyczogXCJ3ZWVrTnVtYmVyXCIsXG4gICAgICB3ZWVreWVhcjogXCJ3ZWVrWWVhclwiLFxuICAgICAgd2Vla3llYXJzOiBcIndlZWtZZWFyXCIsXG4gICAgICBvcmRpbmFsOiBcIm9yZGluYWxcIlxuICAgIH1bdW5pdC50b0xvd2VyQ2FzZSgpXTtcbiAgICBpZiAoIW5vcm1hbGl6ZWQpIHRocm93IG5ldyBJbnZhbGlkVW5pdEVycm9yKHVuaXQpO1xuICAgIHJldHVybiBub3JtYWxpemVkO1xuICB9IC8vIHRoaXMgaXMgYSBkdW1iZWQgZG93biB2ZXJzaW9uIG9mIGZyb21PYmplY3QoKSB0aGF0IHJ1bnMgYWJvdXQgNjAlIGZhc3RlclxuICAvLyBidXQgZG9lc24ndCBkbyBhbnkgdmFsaWRhdGlvbiwgbWFrZXMgYSBidW5jaCBvZiBhc3N1bXB0aW9ucyBhYm91dCB3aGF0IHVuaXRzXG4gIC8vIGFyZSBwcmVzZW50LCBhbmQgc28gb24uXG5cblxuICBmdW5jdGlvbiBxdWlja0RUKG9iaiwgem9uZSkge1xuICAgIC8vIGFzc3VtZSB3ZSBoYXZlIHRoZSBoaWdoZXItb3JkZXIgdW5pdHNcbiAgICBmb3IgKHZhciBfaSA9IDAsIF9vcmRlcmVkVW5pdHMgPSBvcmRlcmVkVW5pdHMkMTsgX2kgPCBfb3JkZXJlZFVuaXRzLmxlbmd0aDsgX2krKykge1xuICAgICAgdmFyIHUgPSBfb3JkZXJlZFVuaXRzW19pXTtcblxuICAgICAgaWYgKGlzVW5kZWZpbmVkKG9ialt1XSkpIHtcbiAgICAgICAgb2JqW3VdID0gZGVmYXVsdFVuaXRWYWx1ZXNbdV07XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIGludmFsaWQgPSBoYXNJbnZhbGlkR3JlZ29yaWFuRGF0YShvYmopIHx8IGhhc0ludmFsaWRUaW1lRGF0YShvYmopO1xuXG4gICAgaWYgKGludmFsaWQpIHtcbiAgICAgIHJldHVybiBEYXRlVGltZS5pbnZhbGlkKGludmFsaWQpO1xuICAgIH1cblxuICAgIHZhciB0c05vdyA9IFNldHRpbmdzLm5vdygpLFxuICAgICAgICBvZmZzZXRQcm92aXMgPSB6b25lLm9mZnNldCh0c05vdyksXG4gICAgICAgIF9vYmpUb1RTID0gb2JqVG9UUyhvYmosIG9mZnNldFByb3Zpcywgem9uZSksXG4gICAgICAgIHRzID0gX29ialRvVFNbMF0sXG4gICAgICAgIG8gPSBfb2JqVG9UU1sxXTtcblxuICAgIHJldHVybiBuZXcgRGF0ZVRpbWUoe1xuICAgICAgdHM6IHRzLFxuICAgICAgem9uZTogem9uZSxcbiAgICAgIG86IG9cbiAgICB9KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGRpZmZSZWxhdGl2ZShzdGFydCwgZW5kLCBvcHRzKSB7XG4gICAgdmFyIHJvdW5kID0gaXNVbmRlZmluZWQob3B0cy5yb3VuZCkgPyB0cnVlIDogb3B0cy5yb3VuZCxcbiAgICAgICAgZm9ybWF0ID0gZnVuY3Rpb24gZm9ybWF0KGMsIHVuaXQpIHtcbiAgICAgIGMgPSByb3VuZFRvKGMsIHJvdW5kIHx8IG9wdHMuY2FsZW5kYXJ5ID8gMCA6IDIsIHRydWUpO1xuICAgICAgdmFyIGZvcm1hdHRlciA9IGVuZC5sb2MuY2xvbmUob3B0cykucmVsRm9ybWF0dGVyKG9wdHMpO1xuICAgICAgcmV0dXJuIGZvcm1hdHRlci5mb3JtYXQoYywgdW5pdCk7XG4gICAgfSxcbiAgICAgICAgZGlmZmVyID0gZnVuY3Rpb24gZGlmZmVyKHVuaXQpIHtcbiAgICAgIGlmIChvcHRzLmNhbGVuZGFyeSkge1xuICAgICAgICBpZiAoIWVuZC5oYXNTYW1lKHN0YXJ0LCB1bml0KSkge1xuICAgICAgICAgIHJldHVybiBlbmQuc3RhcnRPZih1bml0KS5kaWZmKHN0YXJ0LnN0YXJ0T2YodW5pdCksIHVuaXQpLmdldCh1bml0KTtcbiAgICAgICAgfSBlbHNlIHJldHVybiAwO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIGVuZC5kaWZmKHN0YXJ0LCB1bml0KS5nZXQodW5pdCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIGlmIChvcHRzLnVuaXQpIHtcbiAgICAgIHJldHVybiBmb3JtYXQoZGlmZmVyKG9wdHMudW5pdCksIG9wdHMudW5pdCk7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgX2l0ZXJhdG9yID0gb3B0cy51bml0cywgX2lzQXJyYXkgPSBBcnJheS5pc0FycmF5KF9pdGVyYXRvciksIF9pMiA9IDAsIF9pdGVyYXRvciA9IF9pc0FycmF5ID8gX2l0ZXJhdG9yIDogX2l0ZXJhdG9yW1N5bWJvbC5pdGVyYXRvcl0oKTs7KSB7XG4gICAgICB2YXIgX3JlZjI7XG5cbiAgICAgIGlmIChfaXNBcnJheSkge1xuICAgICAgICBpZiAoX2kyID49IF9pdGVyYXRvci5sZW5ndGgpIGJyZWFrO1xuICAgICAgICBfcmVmMiA9IF9pdGVyYXRvcltfaTIrK107XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBfaTIgPSBfaXRlcmF0b3IubmV4dCgpO1xuICAgICAgICBpZiAoX2kyLmRvbmUpIGJyZWFrO1xuICAgICAgICBfcmVmMiA9IF9pMi52YWx1ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHVuaXQgPSBfcmVmMjtcbiAgICAgIHZhciBjb3VudCA9IGRpZmZlcih1bml0KTtcblxuICAgICAgaWYgKE1hdGguYWJzKGNvdW50KSA+PSAxKSB7XG4gICAgICAgIHJldHVybiBmb3JtYXQoY291bnQsIHVuaXQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmb3JtYXQoMCwgb3B0cy51bml0c1tvcHRzLnVuaXRzLmxlbmd0aCAtIDFdKTtcbiAgfVxuICAvKipcbiAgICogQSBEYXRlVGltZSBpcyBhbiBpbW11dGFibGUgZGF0YSBzdHJ1Y3R1cmUgcmVwcmVzZW50aW5nIGEgc3BlY2lmaWMgZGF0ZSBhbmQgdGltZSBhbmQgYWNjb21wYW55aW5nIG1ldGhvZHMuIEl0IGNvbnRhaW5zIGNsYXNzIGFuZCBpbnN0YW5jZSBtZXRob2RzIGZvciBjcmVhdGluZywgcGFyc2luZywgaW50ZXJyb2dhdGluZywgdHJhbnNmb3JtaW5nLCBhbmQgZm9ybWF0dGluZyB0aGVtLlxuICAgKlxuICAgKiBBIERhdGVUaW1lIGNvbXByaXNlcyBvZjpcbiAgICogKiBBIHRpbWVzdGFtcC4gRWFjaCBEYXRlVGltZSBpbnN0YW5jZSByZWZlcnMgdG8gYSBzcGVjaWZpYyBtaWxsaXNlY29uZCBvZiB0aGUgVW5peCBlcG9jaC5cbiAgICogKiBBIHRpbWUgem9uZS4gRWFjaCBpbnN0YW5jZSBpcyBjb25zaWRlcmVkIGluIHRoZSBjb250ZXh0IG9mIGEgc3BlY2lmaWMgem9uZSAoYnkgZGVmYXVsdCB0aGUgbG9jYWwgc3lzdGVtJ3Mgem9uZSkuXG4gICAqICogQ29uZmlndXJhdGlvbiBwcm9wZXJ0aWVzIHRoYXQgZWZmZWN0IGhvdyBvdXRwdXQgc3RyaW5ncyBhcmUgZm9ybWF0dGVkLCBzdWNoIGFzIGBsb2NhbGVgLCBgbnVtYmVyaW5nU3lzdGVtYCwgYW5kIGBvdXRwdXRDYWxlbmRhcmAuXG4gICAqXG4gICAqIEhlcmUgaXMgYSBicmllZiBvdmVydmlldyBvZiB0aGUgbW9zdCBjb21tb25seSB1c2VkIGZ1bmN0aW9uYWxpdHkgaXQgcHJvdmlkZXM6XG4gICAqXG4gICAqICogKipDcmVhdGlvbioqOiBUbyBjcmVhdGUgYSBEYXRlVGltZSBmcm9tIGl0cyBjb21wb25lbnRzLCB1c2Ugb25lIG9mIGl0cyBmYWN0b3J5IGNsYXNzIG1ldGhvZHM6IHtAbGluayBsb2NhbH0sIHtAbGluayB1dGN9LCBhbmQgKG1vc3QgZmxleGlibHkpIHtAbGluayBmcm9tT2JqZWN0fS4gVG8gY3JlYXRlIG9uZSBmcm9tIGEgc3RhbmRhcmQgc3RyaW5nIGZvcm1hdCwgdXNlIHtAbGluayBmcm9tSVNPfSwge0BsaW5rIGZyb21IVFRQfSwgYW5kIHtAbGluayBmcm9tUkZDMjgyMn0uIFRvIGNyZWF0ZSBvbmUgZnJvbSBhIGN1c3RvbSBzdHJpbmcgZm9ybWF0LCB1c2Uge0BsaW5rIGZyb21Gb3JtYXR9LiBUbyBjcmVhdGUgb25lIGZyb20gYSBuYXRpdmUgSlMgZGF0ZSwgdXNlIHtAbGluayBmcm9tSlNEYXRlfS5cbiAgICogKiAqKkdyZWdvcmlhbiBjYWxlbmRhciBhbmQgdGltZSoqOiBUbyBleGFtaW5lIHRoZSBHcmVnb3JpYW4gcHJvcGVydGllcyBvZiBhIERhdGVUaW1lIGluZGl2aWR1YWxseSAoaS5lIGFzIG9wcG9zZWQgdG8gY29sbGVjdGl2ZWx5IHRocm91Z2gge0BsaW5rIHRvT2JqZWN0fSksIHVzZSB0aGUge0BsaW5rIHllYXJ9LCB7QGxpbmsgbW9udGh9LFxuICAgKiB7QGxpbmsgZGF5fSwge0BsaW5rIGhvdXJ9LCB7QGxpbmsgbWludXRlfSwge0BsaW5rIHNlY29uZH0sIHtAbGluayBtaWxsaXNlY29uZH0gYWNjZXNzb3JzLlxuICAgKiAqICoqV2VlayBjYWxlbmRhcioqOiBGb3IgSVNPIHdlZWsgY2FsZW5kYXIgYXR0cmlidXRlcywgc2VlIHRoZSB7QGxpbmsgd2Vla1llYXJ9LCB7QGxpbmsgd2Vla051bWJlcn0sIGFuZCB7QGxpbmsgd2Vla2RheX0gYWNjZXNzb3JzLlxuICAgKiAqICoqQ29uZmlndXJhdGlvbioqIFNlZSB0aGUge0BsaW5rIGxvY2FsZX0gYW5kIHtAbGluayBudW1iZXJpbmdTeXN0ZW19IGFjY2Vzc29ycy5cbiAgICogKiAqKlRyYW5zZm9ybWF0aW9uKio6IFRvIHRyYW5zZm9ybSB0aGUgRGF0ZVRpbWUgaW50byBvdGhlciBEYXRlVGltZXMsIHVzZSB7QGxpbmsgc2V0fSwge0BsaW5rIHJlY29uZmlndXJlfSwge0BsaW5rIHNldFpvbmV9LCB7QGxpbmsgc2V0TG9jYWxlfSwge0BsaW5rIHBsdXN9LCB7QGxpbmsgbWludXN9LCB7QGxpbmsgZW5kT2Z9LCB7QGxpbmsgc3RhcnRPZn0sIHtAbGluayB0b1VUQ30sIGFuZCB7QGxpbmsgdG9Mb2NhbH0uXG4gICAqICogKipPdXRwdXQqKjogVG8gY29udmVydCB0aGUgRGF0ZVRpbWUgdG8gb3RoZXIgcmVwcmVzZW50YXRpb25zLCB1c2UgdGhlIHtAbGluayB0b1JlbGF0aXZlfSwge0BsaW5rIHRvUmVsYXRpdmVDYWxlbmRhcn0sIHtAbGluayB0b0pTT059LCB7QGxpbmsgdG9JU099LCB7QGxpbmsgdG9IVFRQfSwge0BsaW5rIHRvT2JqZWN0fSwge0BsaW5rIHRvUkZDMjgyMn0sIHtAbGluayB0b1N0cmluZ30sIHtAbGluayB0b0xvY2FsZVN0cmluZ30sIHtAbGluayB0b0Zvcm1hdH0sIHtAbGluayB0b01pbGxpc30gYW5kIHtAbGluayB0b0pTRGF0ZX0uXG4gICAqXG4gICAqIFRoZXJlJ3MgcGxlbnR5IG90aGVycyBkb2N1bWVudGVkIGJlbG93LiBJbiBhZGRpdGlvbiwgZm9yIG1vcmUgaW5mb3JtYXRpb24gb24gc3VidGxlciB0b3BpY3MgbGlrZSBpbnRlcm5hdGlvbmFsaXphdGlvbiwgdGltZSB6b25lcywgYWx0ZXJuYXRpdmUgY2FsZW5kYXJzLCB2YWxpZGl0eSwgYW5kIHNvIG9uLCBzZWUgdGhlIGV4dGVybmFsIGRvY3VtZW50YXRpb24uXG4gICAqL1xuXG5cbiAgdmFyIERhdGVUaW1lID1cbiAgLyojX19QVVJFX18qL1xuICBmdW5jdGlvbiAoKSB7XG4gICAgLyoqXG4gICAgICogQGFjY2VzcyBwcml2YXRlXG4gICAgICovXG4gICAgZnVuY3Rpb24gRGF0ZVRpbWUoY29uZmlnKSB7XG4gICAgICB2YXIgem9uZSA9IGNvbmZpZy56b25lIHx8IFNldHRpbmdzLmRlZmF1bHRab25lO1xuICAgICAgdmFyIGludmFsaWQgPSBjb25maWcuaW52YWxpZCB8fCAoTnVtYmVyLmlzTmFOKGNvbmZpZy50cykgPyBuZXcgSW52YWxpZChcImludmFsaWQgaW5wdXRcIikgOiBudWxsKSB8fCAoIXpvbmUuaXNWYWxpZCA/IHVuc3VwcG9ydGVkWm9uZSh6b25lKSA6IG51bGwpO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLnRzID0gaXNVbmRlZmluZWQoY29uZmlnLnRzKSA/IFNldHRpbmdzLm5vdygpIDogY29uZmlnLnRzO1xuICAgICAgdmFyIGMgPSBudWxsLFxuICAgICAgICAgIG8gPSBudWxsO1xuXG4gICAgICBpZiAoIWludmFsaWQpIHtcbiAgICAgICAgdmFyIHVuY2hhbmdlZCA9IGNvbmZpZy5vbGQgJiYgY29uZmlnLm9sZC50cyA9PT0gdGhpcy50cyAmJiBjb25maWcub2xkLnpvbmUuZXF1YWxzKHpvbmUpO1xuXG4gICAgICAgIGlmICh1bmNoYW5nZWQpIHtcbiAgICAgICAgICB2YXIgX3JlZjMgPSBbY29uZmlnLm9sZC5jLCBjb25maWcub2xkLm9dO1xuICAgICAgICAgIGMgPSBfcmVmM1swXTtcbiAgICAgICAgICBvID0gX3JlZjNbMV07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYyA9IHRzVG9PYmoodGhpcy50cywgem9uZS5vZmZzZXQodGhpcy50cykpO1xuICAgICAgICAgIGludmFsaWQgPSBOdW1iZXIuaXNOYU4oYy55ZWFyKSA/IG5ldyBJbnZhbGlkKFwiaW52YWxpZCBpbnB1dFwiKSA6IG51bGw7XG4gICAgICAgICAgYyA9IGludmFsaWQgPyBudWxsIDogYztcbiAgICAgICAgICBvID0gaW52YWxpZCA/IG51bGwgOiB6b25lLm9mZnNldCh0aGlzLnRzKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG5cbiAgICAgIHRoaXMuX3pvbmUgPSB6b25lO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLmxvYyA9IGNvbmZpZy5sb2MgfHwgTG9jYWxlLmNyZWF0ZSgpO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLmludmFsaWQgPSBpbnZhbGlkO1xuICAgICAgLyoqXG4gICAgICAgKiBAYWNjZXNzIHByaXZhdGVcbiAgICAgICAqL1xuXG4gICAgICB0aGlzLndlZWtEYXRhID0gbnVsbDtcbiAgICAgIC8qKlxuICAgICAgICogQGFjY2VzcyBwcml2YXRlXG4gICAgICAgKi9cblxuICAgICAgdGhpcy5jID0gYztcbiAgICAgIC8qKlxuICAgICAgICogQGFjY2VzcyBwcml2YXRlXG4gICAgICAgKi9cblxuICAgICAgdGhpcy5vID0gbztcbiAgICAgIC8qKlxuICAgICAgICogQGFjY2VzcyBwcml2YXRlXG4gICAgICAgKi9cblxuICAgICAgdGhpcy5pc0x1eG9uRGF0ZVRpbWUgPSB0cnVlO1xuICAgIH0gLy8gQ09OU1RSVUNUXG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBsb2NhbCBEYXRlVGltZVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbeWVhcl0gLSBUaGUgY2FsZW5kYXIgeWVhci4gSWYgb21pdHRlZCAoYXMgaW4sIGNhbGwgYGxvY2FsKClgIHdpdGggbm8gYXJndW1lbnRzKSwgdGhlIGN1cnJlbnQgdGltZSB3aWxsIGJlIHVzZWRcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW21vbnRoPTFdIC0gVGhlIG1vbnRoLCAxLWluZGV4ZWRcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2RheT0xXSAtIFRoZSBkYXkgb2YgdGhlIG1vbnRoXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtob3VyPTBdIC0gVGhlIGhvdXIgb2YgdGhlIGRheSwgaW4gMjQtaG91ciB0aW1lXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFttaW51dGU9MF0gLSBUaGUgbWludXRlIG9mIHRoZSBob3VyLCBtZWFuaW5nIGEgbnVtYmVyIGJldHdlZW4gMCBhbmQgNTlcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW3NlY29uZD0wXSAtIFRoZSBzZWNvbmQgb2YgdGhlIG1pbnV0ZSwgbWVhbmluZyBhIG51bWJlciBiZXR3ZWVuIDAgYW5kIDU5XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFttaWxsaXNlY29uZD0wXSAtIFRoZSBtaWxsaXNlY29uZCBvZiB0aGUgc2Vjb25kLCBtZWFuaW5nIGEgbnVtYmVyIGJldHdlZW4gMCBhbmQgOTk5XG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKSAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL34+IG5vd1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcpICAgICAgICAgICAgICAgICAgICAgICAgLy9+PiAyMDE3LTAxLTAxVDAwOjAwOjAwXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgMykgICAgICAgICAgICAgICAgICAgICAvL34+IDIwMTctMDMtMDFUMDA6MDA6MDBcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCAzLCAxMikgICAgICAgICAgICAgICAgIC8vfj4gMjAxNy0wMy0xMlQwMDowMDowMFxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDMsIDEyLCA1KSAgICAgICAgICAgICAgLy9+PiAyMDE3LTAzLTEyVDA1OjAwOjAwXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgMywgMTIsIDUsIDQ1KSAgICAgICAgICAvL34+IDIwMTctMDMtMTJUMDU6NDU6MDBcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCAzLCAxMiwgNSwgNDUsIDEwKSAgICAgIC8vfj4gMjAxNy0wMy0xMlQwNTo0NToxMFxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDMsIDEyLCA1LCA0NSwgMTAsIDc2NSkgLy9+PiAyMDE3LTAzLTEyVDA1OjQ1OjEwLjc2NVxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuXG5cbiAgICBEYXRlVGltZS5sb2NhbCA9IGZ1bmN0aW9uIGxvY2FsKHllYXIsIG1vbnRoLCBkYXksIGhvdXIsIG1pbnV0ZSwgc2Vjb25kLCBtaWxsaXNlY29uZCkge1xuICAgICAgaWYgKGlzVW5kZWZpbmVkKHllYXIpKSB7XG4gICAgICAgIHJldHVybiBuZXcgRGF0ZVRpbWUoe1xuICAgICAgICAgIHRzOiBTZXR0aW5ncy5ub3coKVxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBxdWlja0RUKHtcbiAgICAgICAgICB5ZWFyOiB5ZWFyLFxuICAgICAgICAgIG1vbnRoOiBtb250aCxcbiAgICAgICAgICBkYXk6IGRheSxcbiAgICAgICAgICBob3VyOiBob3VyLFxuICAgICAgICAgIG1pbnV0ZTogbWludXRlLFxuICAgICAgICAgIHNlY29uZDogc2Vjb25kLFxuICAgICAgICAgIG1pbGxpc2Vjb25kOiBtaWxsaXNlY29uZFxuICAgICAgICB9LCBTZXR0aW5ncy5kZWZhdWx0Wm9uZSk7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhIERhdGVUaW1lIGluIFVUQ1xuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbeWVhcl0gLSBUaGUgY2FsZW5kYXIgeWVhci4gSWYgb21pdHRlZCAoYXMgaW4sIGNhbGwgYHV0YygpYCB3aXRoIG5vIGFyZ3VtZW50cyksIHRoZSBjdXJyZW50IHRpbWUgd2lsbCBiZSB1c2VkXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFttb250aD0xXSAtIFRoZSBtb250aCwgMS1pbmRleGVkXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtkYXk9MV0gLSBUaGUgZGF5IG9mIHRoZSBtb250aFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbaG91cj0wXSAtIFRoZSBob3VyIG9mIHRoZSBkYXksIGluIDI0LWhvdXIgdGltZVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbbWludXRlPTBdIC0gVGhlIG1pbnV0ZSBvZiB0aGUgaG91ciwgbWVhbmluZyBhIG51bWJlciBiZXR3ZWVuIDAgYW5kIDU5XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtzZWNvbmQ9MF0gLSBUaGUgc2Vjb25kIG9mIHRoZSBtaW51dGUsIG1lYW5pbmcgYSBudW1iZXIgYmV0d2VlbiAwIGFuZCA1OVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbbWlsbGlzZWNvbmQ9MF0gLSBUaGUgbWlsbGlzZWNvbmQgb2YgdGhlIHNlY29uZCwgbWVhbmluZyBhIG51bWJlciBiZXR3ZWVuIDAgYW5kIDk5OVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygpICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vfj4gbm93XG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUudXRjKDIwMTcpICAgICAgICAgICAgICAgICAgICAgICAgLy9+PiAyMDE3LTAxLTAxVDAwOjAwOjAwWlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygyMDE3LCAzKSAgICAgICAgICAgICAgICAgICAgIC8vfj4gMjAxNy0wMy0wMVQwMDowMDowMFpcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS51dGMoMjAxNywgMywgMTIpICAgICAgICAgICAgICAgICAvL34+IDIwMTctMDMtMTJUMDA6MDA6MDBaXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUudXRjKDIwMTcsIDMsIDEyLCA1KSAgICAgICAgICAgICAgLy9+PiAyMDE3LTAzLTEyVDA1OjAwOjAwWlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygyMDE3LCAzLCAxMiwgNSwgNDUpICAgICAgICAgIC8vfj4gMjAxNy0wMy0xMlQwNTo0NTowMFpcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS51dGMoMjAxNywgMywgMTIsIDUsIDQ1LCAxMCkgICAgICAvL34+IDIwMTctMDMtMTJUMDU6NDU6MTBaXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUudXRjKDIwMTcsIDMsIDEyLCA1LCA0NSwgMTAsIDc2NSkgLy9+PiAyMDE3LTAzLTEyVDA1OjQ1OjEwLjc2NVpcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBEYXRlVGltZS51dGMgPSBmdW5jdGlvbiB1dGMoeWVhciwgbW9udGgsIGRheSwgaG91ciwgbWludXRlLCBzZWNvbmQsIG1pbGxpc2Vjb25kKSB7XG4gICAgICBpZiAoaXNVbmRlZmluZWQoeWVhcikpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBEYXRlVGltZSh7XG4gICAgICAgICAgdHM6IFNldHRpbmdzLm5vdygpLFxuICAgICAgICAgIHpvbmU6IEZpeGVkT2Zmc2V0Wm9uZS51dGNJbnN0YW5jZVxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBxdWlja0RUKHtcbiAgICAgICAgICB5ZWFyOiB5ZWFyLFxuICAgICAgICAgIG1vbnRoOiBtb250aCxcbiAgICAgICAgICBkYXk6IGRheSxcbiAgICAgICAgICBob3VyOiBob3VyLFxuICAgICAgICAgIG1pbnV0ZTogbWludXRlLFxuICAgICAgICAgIHNlY29uZDogc2Vjb25kLFxuICAgICAgICAgIG1pbGxpc2Vjb25kOiBtaWxsaXNlY29uZFxuICAgICAgICB9LCBGaXhlZE9mZnNldFpvbmUudXRjSW5zdGFuY2UpO1xuICAgICAgfVxuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBEYXRlVGltZSBmcm9tIGEgSmF2YXNjcmlwdCBEYXRlIG9iamVjdC4gVXNlcyB0aGUgZGVmYXVsdCB6b25lLlxuICAgICAqIEBwYXJhbSB7RGF0ZX0gZGF0ZSAtIGEgSmF2YXNjcmlwdCBEYXRlIG9iamVjdFxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zIC0gY29uZmlndXJhdGlvbiBvcHRpb25zIGZvciB0aGUgRGF0ZVRpbWVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xab25lfSBbb3B0aW9ucy56b25lPSdsb2NhbCddIC0gdGhlIHpvbmUgdG8gcGxhY2UgdGhlIERhdGVUaW1lIGludG9cbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBEYXRlVGltZS5mcm9tSlNEYXRlID0gZnVuY3Rpb24gZnJvbUpTRGF0ZShkYXRlLCBvcHRpb25zKSB7XG4gICAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgdmFyIHRzID0gaXNEYXRlKGRhdGUpID8gZGF0ZS52YWx1ZU9mKCkgOiBOYU47XG5cbiAgICAgIGlmIChOdW1iZXIuaXNOYU4odHMpKSB7XG4gICAgICAgIHJldHVybiBEYXRlVGltZS5pbnZhbGlkKFwiaW52YWxpZCBpbnB1dFwiKTtcbiAgICAgIH1cblxuICAgICAgdmFyIHpvbmVUb1VzZSA9IG5vcm1hbGl6ZVpvbmUob3B0aW9ucy56b25lLCBTZXR0aW5ncy5kZWZhdWx0Wm9uZSk7XG5cbiAgICAgIGlmICghem9uZVRvVXNlLmlzVmFsaWQpIHtcbiAgICAgICAgcmV0dXJuIERhdGVUaW1lLmludmFsaWQodW5zdXBwb3J0ZWRab25lKHpvbmVUb1VzZSkpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbmV3IERhdGVUaW1lKHtcbiAgICAgICAgdHM6IHRzLFxuICAgICAgICB6b25lOiB6b25lVG9Vc2UsXG4gICAgICAgIGxvYzogTG9jYWxlLmZyb21PYmplY3Qob3B0aW9ucylcbiAgICAgIH0pO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBEYXRlVGltZSBmcm9tIGEgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyBzaW5jZSB0aGUgZXBvY2ggKG1lYW5pbmcgc2luY2UgMSBKYW51YXJ5IDE5NzAgMDA6MDA6MDAgVVRDKS4gVXNlcyB0aGUgZGVmYXVsdCB6b25lLlxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBtaWxsaXNlY29uZHMgLSBhIG51bWJlciBvZiBtaWxsaXNlY29uZHMgc2luY2UgMTk3MCBVVENcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyAtIGNvbmZpZ3VyYXRpb24gb3B0aW9ucyBmb3IgdGhlIERhdGVUaW1lXG4gICAgICogQHBhcmFtIHtzdHJpbmd8Wm9uZX0gW29wdGlvbnMuem9uZT0nbG9jYWwnXSAtIHRoZSB6b25lIHRvIHBsYWNlIHRoZSBEYXRlVGltZSBpbnRvXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRpb25zLmxvY2FsZV0gLSBhIGxvY2FsZSB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvcHRpb25zLm91dHB1dENhbGVuZGFyIC0gdGhlIG91dHB1dCBjYWxlbmRhciB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvcHRpb25zLm51bWJlcmluZ1N5c3RlbSAtIHRoZSBudW1iZXJpbmcgc3lzdGVtIHRvIHNldCBvbiB0aGUgcmVzdWx0aW5nIERhdGVUaW1lIGluc3RhbmNlXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9XG4gICAgICovXG4gICAgO1xuXG4gICAgRGF0ZVRpbWUuZnJvbU1pbGxpcyA9IGZ1bmN0aW9uIGZyb21NaWxsaXMobWlsbGlzZWNvbmRzLCBvcHRpb25zKSB7XG4gICAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgaWYgKCFpc051bWJlcihtaWxsaXNlY29uZHMpKSB7XG4gICAgICAgIHRocm93IG5ldyBJbnZhbGlkQXJndW1lbnRFcnJvcihcImZyb21NaWxsaXMgcmVxdWlyZXMgYSBudW1lcmljYWwgaW5wdXRcIik7XG4gICAgICB9IGVsc2UgaWYgKG1pbGxpc2Vjb25kcyA8IC1NQVhfREFURSB8fCBtaWxsaXNlY29uZHMgPiBNQVhfREFURSkge1xuICAgICAgICAvLyB0aGlzIGlzbid0IHBlcmZlY3QgYmVjYXVzZSBiZWNhdXNlIHdlIGNhbiBzdGlsbCBlbmQgdXAgb3V0IG9mIHJhbmdlIGJlY2F1c2Ugb2YgYWRkaXRpb25hbCBzaGlmdGluZywgYnV0IGl0J3MgYSBzdGFydFxuICAgICAgICByZXR1cm4gRGF0ZVRpbWUuaW52YWxpZChcIlRpbWVzdGFtcCBvdXQgb2YgcmFuZ2VcIik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gbmV3IERhdGVUaW1lKHtcbiAgICAgICAgICB0czogbWlsbGlzZWNvbmRzLFxuICAgICAgICAgIHpvbmU6IG5vcm1hbGl6ZVpvbmUob3B0aW9ucy56b25lLCBTZXR0aW5ncy5kZWZhdWx0Wm9uZSksXG4gICAgICAgICAgbG9jOiBMb2NhbGUuZnJvbU9iamVjdChvcHRpb25zKVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgRGF0ZVRpbWUgZnJvbSBhIG51bWJlciBvZiBzZWNvbmRzIHNpbmNlIHRoZSBlcG9jaCAobWVhbmluZyBzaW5jZSAxIEphbnVhcnkgMTk3MCAwMDowMDowMCBVVEMpLiBVc2VzIHRoZSBkZWZhdWx0IHpvbmUuXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHNlY29uZHMgLSBhIG51bWJlciBvZiBzZWNvbmRzIHNpbmNlIDE5NzAgVVRDXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMgLSBjb25maWd1cmF0aW9uIG9wdGlvbnMgZm9yIHRoZSBEYXRlVGltZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfFpvbmV9IFtvcHRpb25zLnpvbmU9J2xvY2FsJ10gLSB0aGUgem9uZSB0byBwbGFjZSB0aGUgRGF0ZVRpbWUgaW50b1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0aW9ucy5sb2NhbGVdIC0gYSBsb2NhbGUgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0aW9ucy5vdXRwdXRDYWxlbmRhciAtIHRoZSBvdXRwdXQgY2FsZW5kYXIgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0aW9ucy5udW1iZXJpbmdTeXN0ZW0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbSB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLmZyb21TZWNvbmRzID0gZnVuY3Rpb24gZnJvbVNlY29uZHMoc2Vjb25kcywgb3B0aW9ucykge1xuICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRpb25zID0ge307XG4gICAgICB9XG5cbiAgICAgIGlmICghaXNOdW1iZXIoc2Vjb25kcykpIHtcbiAgICAgICAgdGhyb3cgbmV3IEludmFsaWRBcmd1bWVudEVycm9yKFwiZnJvbVNlY29uZHMgcmVxdWlyZXMgYSBudW1lcmljYWwgaW5wdXRcIik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gbmV3IERhdGVUaW1lKHtcbiAgICAgICAgICB0czogc2Vjb25kcyAqIDEwMDAsXG4gICAgICAgICAgem9uZTogbm9ybWFsaXplWm9uZShvcHRpb25zLnpvbmUsIFNldHRpbmdzLmRlZmF1bHRab25lKSxcbiAgICAgICAgICBsb2M6IExvY2FsZS5mcm9tT2JqZWN0KG9wdGlvbnMpXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBEYXRlVGltZSBmcm9tIGEgSmF2YXNjcmlwdCBvYmplY3Qgd2l0aCBrZXlzIGxpa2UgJ3llYXInIGFuZCAnaG91cicgd2l0aCByZWFzb25hYmxlIGRlZmF1bHRzLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvYmogLSB0aGUgb2JqZWN0IHRvIGNyZWF0ZSB0aGUgRGF0ZVRpbWUgZnJvbVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoueWVhciAtIGEgeWVhciwgc3VjaCBhcyAxOTg3XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9iai5tb250aCAtIGEgbW9udGgsIDEtMTJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gb2JqLmRheSAtIGEgZGF5IG9mIHRoZSBtb250aCwgMS0zMSwgZGVwZW5kaW5nIG9uIHRoZSBtb250aFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoub3JkaW5hbCAtIGRheSBvZiB0aGUgeWVhciwgMS0zNjUgb3IgMzY2XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9iai53ZWVrWWVhciAtIGFuIElTTyB3ZWVrIHllYXJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gb2JqLndlZWtOdW1iZXIgLSBhbiBJU08gd2VlayBudW1iZXIsIGJldHdlZW4gMSBhbmQgNTIgb3IgNTMsIGRlcGVuZGluZyBvbiB0aGUgeWVhclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoud2Vla2RheSAtIGFuIElTTyB3ZWVrZGF5LCAxLTcsIHdoZXJlIDEgaXMgTW9uZGF5IGFuZCA3IGlzIFN1bmRheVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmouaG91ciAtIGhvdXIgb2YgdGhlIGRheSwgMC0yM1xuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoubWludXRlIC0gbWludXRlIG9mIHRoZSBob3VyLCAwLTU5XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IG9iai5zZWNvbmQgLSBzZWNvbmQgb2YgdGhlIG1pbnV0ZSwgMC01OVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvYmoubWlsbGlzZWNvbmQgLSBtaWxsaXNlY29uZCBvZiB0aGUgc2Vjb25kLCAwLTk5OVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfFpvbmV9IFtvYmouem9uZT0nbG9jYWwnXSAtIGludGVycHJldCB0aGUgbnVtYmVycyBpbiB0aGUgY29udGV4dCBvZiBhIHBhcnRpY3VsYXIgem9uZS4gQ2FuIHRha2UgYW55IHZhbHVlIHRha2VuIGFzIHRoZSBmaXJzdCBhcmd1bWVudCB0byBzZXRab25lKClcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gW29iai5sb2NhbGU9J3N5c3RlbSdzIGxvY2FsZSddIC0gYSBsb2NhbGUgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb2JqLm91dHB1dENhbGVuZGFyIC0gdGhlIG91dHB1dCBjYWxlbmRhciB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvYmoubnVtYmVyaW5nU3lzdGVtIC0gdGhlIG51bWJlcmluZyBzeXN0ZW0gdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5mcm9tT2JqZWN0KHsgeWVhcjogMTk4MiwgbW9udGg6IDUsIGRheTogMjV9KS50b0lTT0RhdGUoKSAvLz0+ICcxOTgyLTA1LTI1J1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21PYmplY3QoeyB5ZWFyOiAxOTgyIH0pLnRvSVNPRGF0ZSgpIC8vPT4gJzE5ODItMDEtMDEnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbU9iamVjdCh7IGhvdXI6IDEwLCBtaW51dGU6IDI2LCBzZWNvbmQ6IDYgfSkgLy9+PiB0b2RheSBhdCAxMDoyNjowNlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21PYmplY3QoeyBob3VyOiAxMCwgbWludXRlOiAyNiwgc2Vjb25kOiA2LCB6b25lOiAndXRjJyB9KSxcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5mcm9tT2JqZWN0KHsgaG91cjogMTAsIG1pbnV0ZTogMjYsIHNlY29uZDogNiwgem9uZTogJ2xvY2FsJyB9KVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21PYmplY3QoeyBob3VyOiAxMCwgbWludXRlOiAyNiwgc2Vjb25kOiA2LCB6b25lOiAnQW1lcmljYS9OZXdfWW9yaycgfSlcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5mcm9tT2JqZWN0KHsgd2Vla1llYXI6IDIwMTYsIHdlZWtOdW1iZXI6IDIsIHdlZWtkYXk6IDMgfSkudG9JU09EYXRlKCkgLy89PiAnMjAxNi0wMS0xMydcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBEYXRlVGltZS5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmopIHtcbiAgICAgIHZhciB6b25lVG9Vc2UgPSBub3JtYWxpemVab25lKG9iai56b25lLCBTZXR0aW5ncy5kZWZhdWx0Wm9uZSk7XG5cbiAgICAgIGlmICghem9uZVRvVXNlLmlzVmFsaWQpIHtcbiAgICAgICAgcmV0dXJuIERhdGVUaW1lLmludmFsaWQodW5zdXBwb3J0ZWRab25lKHpvbmVUb1VzZSkpO1xuICAgICAgfVxuXG4gICAgICB2YXIgdHNOb3cgPSBTZXR0aW5ncy5ub3coKSxcbiAgICAgICAgICBvZmZzZXRQcm92aXMgPSB6b25lVG9Vc2Uub2Zmc2V0KHRzTm93KSxcbiAgICAgICAgICBub3JtYWxpemVkID0gbm9ybWFsaXplT2JqZWN0KG9iaiwgbm9ybWFsaXplVW5pdCwgW1wiem9uZVwiLCBcImxvY2FsZVwiLCBcIm91dHB1dENhbGVuZGFyXCIsIFwibnVtYmVyaW5nU3lzdGVtXCJdKSxcbiAgICAgICAgICBjb250YWluc09yZGluYWwgPSAhaXNVbmRlZmluZWQobm9ybWFsaXplZC5vcmRpbmFsKSxcbiAgICAgICAgICBjb250YWluc0dyZWdvclllYXIgPSAhaXNVbmRlZmluZWQobm9ybWFsaXplZC55ZWFyKSxcbiAgICAgICAgICBjb250YWluc0dyZWdvck1EID0gIWlzVW5kZWZpbmVkKG5vcm1hbGl6ZWQubW9udGgpIHx8ICFpc1VuZGVmaW5lZChub3JtYWxpemVkLmRheSksXG4gICAgICAgICAgY29udGFpbnNHcmVnb3IgPSBjb250YWluc0dyZWdvclllYXIgfHwgY29udGFpbnNHcmVnb3JNRCxcbiAgICAgICAgICBkZWZpbml0ZVdlZWtEZWYgPSBub3JtYWxpemVkLndlZWtZZWFyIHx8IG5vcm1hbGl6ZWQud2Vla051bWJlcixcbiAgICAgICAgICBsb2MgPSBMb2NhbGUuZnJvbU9iamVjdChvYmopOyAvLyBjYXNlczpcbiAgICAgIC8vIGp1c3QgYSB3ZWVrZGF5IC0+IHRoaXMgd2VlaydzIGluc3RhbmNlIG9mIHRoYXQgd2Vla2RheSwgbm8gd29ycmllc1xuICAgICAgLy8gKGdyZWdvcmlhbiBkYXRhIG9yIG9yZGluYWwpICsgKHdlZWtZZWFyIG9yIHdlZWtOdW1iZXIpIC0+IGVycm9yXG4gICAgICAvLyAoZ3JlZ29yaWFuIG1vbnRoIG9yIGRheSkgKyBvcmRpbmFsIC0+IGVycm9yXG4gICAgICAvLyBvdGhlcndpc2UganVzdCB1c2Ugd2Vla3Mgb3Igb3JkaW5hbHMgb3IgZ3JlZ29yaWFuLCBkZXBlbmRpbmcgb24gd2hhdCdzIHNwZWNpZmllZFxuXG4gICAgICBpZiAoKGNvbnRhaW5zR3JlZ29yIHx8IGNvbnRhaW5zT3JkaW5hbCkgJiYgZGVmaW5pdGVXZWVrRGVmKSB7XG4gICAgICAgIHRocm93IG5ldyBDb25mbGljdGluZ1NwZWNpZmljYXRpb25FcnJvcihcIkNhbid0IG1peCB3ZWVrWWVhci93ZWVrTnVtYmVyIHVuaXRzIHdpdGggeWVhci9tb250aC9kYXkgb3Igb3JkaW5hbHNcIik7XG4gICAgICB9XG5cbiAgICAgIGlmIChjb250YWluc0dyZWdvck1EICYmIGNvbnRhaW5zT3JkaW5hbCkge1xuICAgICAgICB0aHJvdyBuZXcgQ29uZmxpY3RpbmdTcGVjaWZpY2F0aW9uRXJyb3IoXCJDYW4ndCBtaXggb3JkaW5hbCBkYXRlcyB3aXRoIG1vbnRoL2RheVwiKTtcbiAgICAgIH1cblxuICAgICAgdmFyIHVzZVdlZWtEYXRhID0gZGVmaW5pdGVXZWVrRGVmIHx8IG5vcm1hbGl6ZWQud2Vla2RheSAmJiAhY29udGFpbnNHcmVnb3I7IC8vIGNvbmZpZ3VyZSBvdXJzZWx2ZXMgdG8gZGVhbCB3aXRoIGdyZWdvcmlhbiBkYXRlcyBvciB3ZWVrIHN0dWZmXG5cbiAgICAgIHZhciB1bml0cyxcbiAgICAgICAgICBkZWZhdWx0VmFsdWVzLFxuICAgICAgICAgIG9iak5vdyA9IHRzVG9PYmoodHNOb3csIG9mZnNldFByb3Zpcyk7XG5cbiAgICAgIGlmICh1c2VXZWVrRGF0YSkge1xuICAgICAgICB1bml0cyA9IG9yZGVyZWRXZWVrVW5pdHM7XG4gICAgICAgIGRlZmF1bHRWYWx1ZXMgPSBkZWZhdWx0V2Vla1VuaXRWYWx1ZXM7XG4gICAgICAgIG9iak5vdyA9IGdyZWdvcmlhblRvV2VlayhvYmpOb3cpO1xuICAgICAgfSBlbHNlIGlmIChjb250YWluc09yZGluYWwpIHtcbiAgICAgICAgdW5pdHMgPSBvcmRlcmVkT3JkaW5hbFVuaXRzO1xuICAgICAgICBkZWZhdWx0VmFsdWVzID0gZGVmYXVsdE9yZGluYWxVbml0VmFsdWVzO1xuICAgICAgICBvYmpOb3cgPSBncmVnb3JpYW5Ub09yZGluYWwob2JqTm93KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHVuaXRzID0gb3JkZXJlZFVuaXRzJDE7XG4gICAgICAgIGRlZmF1bHRWYWx1ZXMgPSBkZWZhdWx0VW5pdFZhbHVlcztcbiAgICAgIH0gLy8gc2V0IGRlZmF1bHQgdmFsdWVzIGZvciBtaXNzaW5nIHN0dWZmXG5cblxuICAgICAgdmFyIGZvdW5kRmlyc3QgPSBmYWxzZTtcblxuICAgICAgZm9yICh2YXIgX2l0ZXJhdG9yMiA9IHVuaXRzLCBfaXNBcnJheTIgPSBBcnJheS5pc0FycmF5KF9pdGVyYXRvcjIpLCBfaTMgPSAwLCBfaXRlcmF0b3IyID0gX2lzQXJyYXkyID8gX2l0ZXJhdG9yMiA6IF9pdGVyYXRvcjJbU3ltYm9sLml0ZXJhdG9yXSgpOzspIHtcbiAgICAgICAgdmFyIF9yZWY0O1xuXG4gICAgICAgIGlmIChfaXNBcnJheTIpIHtcbiAgICAgICAgICBpZiAoX2kzID49IF9pdGVyYXRvcjIubGVuZ3RoKSBicmVhaztcbiAgICAgICAgICBfcmVmNCA9IF9pdGVyYXRvcjJbX2kzKytdO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF9pMyA9IF9pdGVyYXRvcjIubmV4dCgpO1xuICAgICAgICAgIGlmIChfaTMuZG9uZSkgYnJlYWs7XG4gICAgICAgICAgX3JlZjQgPSBfaTMudmFsdWU7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgdSA9IF9yZWY0O1xuICAgICAgICB2YXIgdiA9IG5vcm1hbGl6ZWRbdV07XG5cbiAgICAgICAgaWYgKCFpc1VuZGVmaW5lZCh2KSkge1xuICAgICAgICAgIGZvdW5kRmlyc3QgPSB0cnVlO1xuICAgICAgICB9IGVsc2UgaWYgKGZvdW5kRmlyc3QpIHtcbiAgICAgICAgICBub3JtYWxpemVkW3VdID0gZGVmYXVsdFZhbHVlc1t1XTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBub3JtYWxpemVkW3VdID0gb2JqTm93W3VdO1xuICAgICAgICB9XG4gICAgICB9IC8vIG1ha2Ugc3VyZSB0aGUgdmFsdWVzIHdlIGhhdmUgYXJlIGluIHJhbmdlXG5cblxuICAgICAgdmFyIGhpZ2hlck9yZGVySW52YWxpZCA9IHVzZVdlZWtEYXRhID8gaGFzSW52YWxpZFdlZWtEYXRhKG5vcm1hbGl6ZWQpIDogY29udGFpbnNPcmRpbmFsID8gaGFzSW52YWxpZE9yZGluYWxEYXRhKG5vcm1hbGl6ZWQpIDogaGFzSW52YWxpZEdyZWdvcmlhbkRhdGEobm9ybWFsaXplZCksXG4gICAgICAgICAgaW52YWxpZCA9IGhpZ2hlck9yZGVySW52YWxpZCB8fCBoYXNJbnZhbGlkVGltZURhdGEobm9ybWFsaXplZCk7XG5cbiAgICAgIGlmIChpbnZhbGlkKSB7XG4gICAgICAgIHJldHVybiBEYXRlVGltZS5pbnZhbGlkKGludmFsaWQpO1xuICAgICAgfSAvLyBjb21wdXRlIHRoZSBhY3R1YWwgdGltZVxuXG5cbiAgICAgIHZhciBncmVnb3JpYW4gPSB1c2VXZWVrRGF0YSA/IHdlZWtUb0dyZWdvcmlhbihub3JtYWxpemVkKSA6IGNvbnRhaW5zT3JkaW5hbCA/IG9yZGluYWxUb0dyZWdvcmlhbihub3JtYWxpemVkKSA6IG5vcm1hbGl6ZWQsXG4gICAgICAgICAgX29ialRvVFMyID0gb2JqVG9UUyhncmVnb3JpYW4sIG9mZnNldFByb3Zpcywgem9uZVRvVXNlKSxcbiAgICAgICAgICB0c0ZpbmFsID0gX29ialRvVFMyWzBdLFxuICAgICAgICAgIG9mZnNldEZpbmFsID0gX29ialRvVFMyWzFdLFxuICAgICAgICAgIGluc3QgPSBuZXcgRGF0ZVRpbWUoe1xuICAgICAgICB0czogdHNGaW5hbCxcbiAgICAgICAgem9uZTogem9uZVRvVXNlLFxuICAgICAgICBvOiBvZmZzZXRGaW5hbCxcbiAgICAgICAgbG9jOiBsb2NcbiAgICAgIH0pOyAvLyBncmVnb3JpYW4gZGF0YSArIHdlZWtkYXkgc2VydmVzIG9ubHkgdG8gdmFsaWRhdGVcblxuXG4gICAgICBpZiAobm9ybWFsaXplZC53ZWVrZGF5ICYmIGNvbnRhaW5zR3JlZ29yICYmIG9iai53ZWVrZGF5ICE9PSBpbnN0LndlZWtkYXkpIHtcbiAgICAgICAgcmV0dXJuIERhdGVUaW1lLmludmFsaWQoXCJtaXNtYXRjaGVkIHdlZWtkYXlcIiwgXCJ5b3UgY2FuJ3Qgc3BlY2lmeSBib3RoIGEgd2Vla2RheSBvZiBcIiArIG5vcm1hbGl6ZWQud2Vla2RheSArIFwiIGFuZCBhIGRhdGUgb2YgXCIgKyBpbnN0LnRvSVNPKCkpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gaW5zdDtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgRGF0ZVRpbWUgZnJvbSBhbiBJU08gODYwMSBzdHJpbmdcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdGV4dCAtIHRoZSBJU08gc3RyaW5nXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRpb25zIHRvIGFmZmVjdCB0aGUgY3JlYXRpb25cbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xab25lfSBbb3B0cy56b25lPSdsb2NhbCddIC0gdXNlIHRoaXMgem9uZSBpZiBubyBvZmZzZXQgaXMgc3BlY2lmaWVkIGluIHRoZSBpbnB1dCBzdHJpbmcgaXRzZWxmLiBXaWxsIGFsc28gY29udmVydCB0aGUgdGltZSB0byB0aGlzIHpvbmVcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRzLnNldFpvbmU9ZmFsc2VdIC0gb3ZlcnJpZGUgdGhlIHpvbmUgd2l0aCBhIGZpeGVkLW9mZnNldCB6b25lIHNwZWNpZmllZCBpbiB0aGUgc3RyaW5nIGl0c2VsZiwgaWYgaXQgc3BlY2lmaWVzIG9uZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5sb2NhbGU9J3N5c3RlbSdzIGxvY2FsZSddIC0gYSBsb2NhbGUgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5vdXRwdXRDYWxlbmRhciAtIHRoZSBvdXRwdXQgY2FsZW5kYXIgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5udW1iZXJpbmdTeXN0ZW0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbSB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21JU08oJzIwMTYtMDUtMjVUMDk6MDg6MzQuMTIzJylcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5mcm9tSVNPKCcyMDE2LTA1LTI1VDA5OjA4OjM0LjEyMyswNjowMCcpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbUlTTygnMjAxNi0wNS0yNVQwOTowODozNC4xMjMrMDY6MDAnLCB7c2V0Wm9uZTogdHJ1ZX0pXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbUlTTygnMjAxNi0wNS0yNVQwOTowODozNC4xMjMnLCB7em9uZTogJ3V0Yyd9KVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21JU08oJzIwMTYtVzA1LTQnKVxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLmZyb21JU08gPSBmdW5jdGlvbiBmcm9tSVNPKHRleHQsIG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICB2YXIgX3BhcnNlSVNPRGF0ZSA9IHBhcnNlSVNPRGF0ZSh0ZXh0KSxcbiAgICAgICAgICB2YWxzID0gX3BhcnNlSVNPRGF0ZVswXSxcbiAgICAgICAgICBwYXJzZWRab25lID0gX3BhcnNlSVNPRGF0ZVsxXTtcblxuICAgICAgcmV0dXJuIHBhcnNlRGF0YVRvRGF0ZVRpbWUodmFscywgcGFyc2VkWm9uZSwgb3B0cywgXCJJU08gODYwMVwiLCB0ZXh0KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgRGF0ZVRpbWUgZnJvbSBhbiBSRkMgMjgyMiBzdHJpbmdcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdGV4dCAtIHRoZSBSRkMgMjgyMiBzdHJpbmdcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnMgdG8gYWZmZWN0IHRoZSBjcmVhdGlvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nfFpvbmV9IFtvcHRzLnpvbmU9J2xvY2FsJ10gLSBjb252ZXJ0IHRoZSB0aW1lIHRvIHRoaXMgem9uZS4gU2luY2UgdGhlIG9mZnNldCBpcyBhbHdheXMgc3BlY2lmaWVkIGluIHRoZSBzdHJpbmcgaXRzZWxmLCB0aGlzIGhhcyBubyBlZmZlY3Qgb24gdGhlIGludGVycHJldGF0aW9uIG9mIHN0cmluZywgbWVyZWx5IHRoZSB6b25lIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaXMgZXhwcmVzc2VkIGluLlxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc2V0Wm9uZT1mYWxzZV0gLSBvdmVycmlkZSB0aGUgem9uZSB3aXRoIGEgZml4ZWQtb2Zmc2V0IHpvbmUgc3BlY2lmaWVkIGluIHRoZSBzdHJpbmcgaXRzZWxmLCBpZiBpdCBzcGVjaWZpZXMgb25lXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmxvY2FsZT0nc3lzdGVtJ3MgbG9jYWxlJ10gLSBhIGxvY2FsZSB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvcHRzLm91dHB1dENhbGVuZGFyIC0gdGhlIG91dHB1dCBjYWxlbmRhciB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvcHRzLm51bWJlcmluZ1N5c3RlbSAtIHRoZSBudW1iZXJpbmcgc3lzdGVtIHRvIHNldCBvbiB0aGUgcmVzdWx0aW5nIERhdGVUaW1lIGluc3RhbmNlXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbVJGQzI4MjIoJzI1IE5vdiAyMDE2IDEzOjIzOjEyIEdNVCcpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbVJGQzI4MjIoJ0ZyaSwgMjUgTm92IDIwMTYgMTM6MjM6MTIgKzA2MDAnKVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21SRkMyODIyKCcyNSBOb3YgMjAxNiAxMzoyMyBaJylcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBEYXRlVGltZS5mcm9tUkZDMjgyMiA9IGZ1bmN0aW9uIGZyb21SRkMyODIyKHRleHQsIG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICB2YXIgX3BhcnNlUkZDMjgyMkRhdGUgPSBwYXJzZVJGQzI4MjJEYXRlKHRleHQpLFxuICAgICAgICAgIHZhbHMgPSBfcGFyc2VSRkMyODIyRGF0ZVswXSxcbiAgICAgICAgICBwYXJzZWRab25lID0gX3BhcnNlUkZDMjgyMkRhdGVbMV07XG5cbiAgICAgIHJldHVybiBwYXJzZURhdGFUb0RhdGVUaW1lKHZhbHMsIHBhcnNlZFpvbmUsIG9wdHMsIFwiUkZDIDI4MjJcIiwgdGV4dCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhIERhdGVUaW1lIGZyb20gYW4gSFRUUCBoZWFkZXIgZGF0ZVxuICAgICAqIEBzZWUgaHR0cHM6Ly93d3cudzMub3JnL1Byb3RvY29scy9yZmMyNjE2L3JmYzI2MTYtc2VjMy5odG1sI3NlYzMuMy4xXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRleHQgLSB0aGUgSFRUUCBoZWFkZXIgZGF0ZVxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9ucyB0byBhZmZlY3QgdGhlIGNyZWF0aW9uXG4gICAgICogQHBhcmFtIHtzdHJpbmd8Wm9uZX0gW29wdHMuem9uZT0nbG9jYWwnXSAtIGNvbnZlcnQgdGhlIHRpbWUgdG8gdGhpcyB6b25lLiBTaW5jZSBIVFRQIGRhdGVzIGFyZSBhbHdheXMgaW4gVVRDLCB0aGlzIGhhcyBubyBlZmZlY3Qgb24gdGhlIGludGVycHJldGF0aW9uIG9mIHN0cmluZywgbWVyZWx5IHRoZSB6b25lIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaXMgZXhwcmVzc2VkIGluLlxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc2V0Wm9uZT1mYWxzZV0gLSBvdmVycmlkZSB0aGUgem9uZSB3aXRoIHRoZSBmaXhlZC1vZmZzZXQgem9uZSBzcGVjaWZpZWQgaW4gdGhlIHN0cmluZy4gRm9yIEhUVFAgZGF0ZXMsIHRoaXMgaXMgYWx3YXlzIFVUQywgc28gdGhpcyBvcHRpb24gaXMgZXF1aXZhbGVudCB0byBzZXR0aW5nIHRoZSBgem9uZWAgb3B0aW9uIHRvICd1dGMnLCBidXQgdGhpcyBvcHRpb24gaXMgaW5jbHVkZWQgZm9yIGNvbnNpc3RlbmN5IHdpdGggc2ltaWxhciBtZXRob2RzLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5sb2NhbGU9J3N5c3RlbSdzIGxvY2FsZSddIC0gYSBsb2NhbGUgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5vdXRwdXRDYWxlbmRhciAtIHRoZSBvdXRwdXQgY2FsZW5kYXIgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5udW1iZXJpbmdTeXN0ZW0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbSB0byBzZXQgb24gdGhlIHJlc3VsdGluZyBEYXRlVGltZSBpbnN0YW5jZVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21IVFRQKCdTdW4sIDA2IE5vdiAxOTk0IDA4OjQ5OjM3IEdNVCcpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbUhUVFAoJ1N1bmRheSwgMDYtTm92LTk0IDA4OjQ5OjM3IEdNVCcpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbUhUVFAoJ1N1biBOb3YgIDYgMDg6NDk6MzcgMTk5NCcpXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9XG4gICAgICovXG4gICAgO1xuXG4gICAgRGF0ZVRpbWUuZnJvbUhUVFAgPSBmdW5jdGlvbiBmcm9tSFRUUCh0ZXh0LCBvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgdmFyIF9wYXJzZUhUVFBEYXRlID0gcGFyc2VIVFRQRGF0ZSh0ZXh0KSxcbiAgICAgICAgICB2YWxzID0gX3BhcnNlSFRUUERhdGVbMF0sXG4gICAgICAgICAgcGFyc2VkWm9uZSA9IF9wYXJzZUhUVFBEYXRlWzFdO1xuXG4gICAgICByZXR1cm4gcGFyc2VEYXRhVG9EYXRlVGltZSh2YWxzLCBwYXJzZWRab25lLCBvcHRzLCBcIkhUVFBcIiwgb3B0cyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhIERhdGVUaW1lIGZyb20gYW4gaW5wdXQgc3RyaW5nIGFuZCBmb3JtYXQgc3RyaW5nLlxuICAgICAqIERlZmF1bHRzIHRvIGVuLVVTIGlmIG5vIGxvY2FsZSBoYXMgYmVlbiBzcGVjaWZpZWQsIHJlZ2FyZGxlc3Mgb2YgdGhlIHN5c3RlbSdzIGxvY2FsZS5cbiAgICAgKiBAc2VlIGh0dHBzOi8vbW9tZW50LmdpdGh1Yi5pby9sdXhvbi9kb2NzL21hbnVhbC9wYXJzaW5nLmh0bWwjdGFibGUtb2YtdG9rZW5zXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRleHQgLSB0aGUgc3RyaW5nIHRvIHBhcnNlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGZtdCAtIHRoZSBmb3JtYXQgdGhlIHN0cmluZyBpcyBleHBlY3RlZCB0byBiZSBpbiAoc2VlIHRoZSBsaW5rIGJlbG93IGZvciB0aGUgZm9ybWF0cylcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnMgdG8gYWZmZWN0IHRoZSBjcmVhdGlvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nfFpvbmV9IFtvcHRzLnpvbmU9J2xvY2FsJ10gLSB1c2UgdGhpcyB6b25lIGlmIG5vIG9mZnNldCBpcyBzcGVjaWZpZWQgaW4gdGhlIGlucHV0IHN0cmluZyBpdHNlbGYuIFdpbGwgYWxzbyBjb252ZXJ0IHRoZSBEYXRlVGltZSB0byB0aGlzIHpvbmVcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRzLnNldFpvbmU9ZmFsc2VdIC0gb3ZlcnJpZGUgdGhlIHpvbmUgd2l0aCBhIHpvbmUgc3BlY2lmaWVkIGluIHRoZSBzdHJpbmcgaXRzZWxmLCBpZiBpdCBzcGVjaWZpZXMgb25lXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmxvY2FsZT0nZW4tVVMnXSAtIGEgbG9jYWxlIHN0cmluZyB0byB1c2Ugd2hlbiBwYXJzaW5nLiBXaWxsIGFsc28gc2V0IHRoZSBEYXRlVGltZSB0byB0aGlzIGxvY2FsZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvcHRzLm51bWJlcmluZ1N5c3RlbSAtIHRoZSBudW1iZXJpbmcgc3lzdGVtIHRvIHVzZSB3aGVuIHBhcnNpbmcuIFdpbGwgYWxzbyBzZXQgdGhlIHJlc3VsdGluZyBEYXRlVGltZSB0byB0aGlzIG51bWJlcmluZyBzeXN0ZW1cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5vdXRwdXRDYWxlbmRhciAtIHRoZSBvdXRwdXQgY2FsZW5kYXIgdG8gc2V0IG9uIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgaW5zdGFuY2VcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBEYXRlVGltZS5mcm9tRm9ybWF0ID0gZnVuY3Rpb24gZnJvbUZvcm1hdCh0ZXh0LCBmbXQsIG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNVbmRlZmluZWQodGV4dCkgfHwgaXNVbmRlZmluZWQoZm10KSkge1xuICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEFyZ3VtZW50RXJyb3IoXCJmcm9tRm9ybWF0IHJlcXVpcmVzIGFuIGlucHV0IHN0cmluZyBhbmQgYSBmb3JtYXRcIik7XG4gICAgICB9XG5cbiAgICAgIHZhciBfb3B0cyA9IG9wdHMsXG4gICAgICAgICAgX29wdHMkbG9jYWxlID0gX29wdHMubG9jYWxlLFxuICAgICAgICAgIGxvY2FsZSA9IF9vcHRzJGxvY2FsZSA9PT0gdm9pZCAwID8gbnVsbCA6IF9vcHRzJGxvY2FsZSxcbiAgICAgICAgICBfb3B0cyRudW1iZXJpbmdTeXN0ZW0gPSBfb3B0cy5udW1iZXJpbmdTeXN0ZW0sXG4gICAgICAgICAgbnVtYmVyaW5nU3lzdGVtID0gX29wdHMkbnVtYmVyaW5nU3lzdGVtID09PSB2b2lkIDAgPyBudWxsIDogX29wdHMkbnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIGxvY2FsZVRvVXNlID0gTG9jYWxlLmZyb21PcHRzKHtcbiAgICAgICAgbG9jYWxlOiBsb2NhbGUsXG4gICAgICAgIG51bWJlcmluZ1N5c3RlbTogbnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICBkZWZhdWx0VG9FTjogdHJ1ZVxuICAgICAgfSksXG4gICAgICAgICAgX3BhcnNlRnJvbVRva2VucyA9IHBhcnNlRnJvbVRva2Vucyhsb2NhbGVUb1VzZSwgdGV4dCwgZm10KSxcbiAgICAgICAgICB2YWxzID0gX3BhcnNlRnJvbVRva2Vuc1swXSxcbiAgICAgICAgICBwYXJzZWRab25lID0gX3BhcnNlRnJvbVRva2Vuc1sxXSxcbiAgICAgICAgICBpbnZhbGlkID0gX3BhcnNlRnJvbVRva2Vuc1syXTtcblxuICAgICAgaWYgKGludmFsaWQpIHtcbiAgICAgICAgcmV0dXJuIERhdGVUaW1lLmludmFsaWQoaW52YWxpZCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gcGFyc2VEYXRhVG9EYXRlVGltZSh2YWxzLCBwYXJzZWRab25lLCBvcHRzLCBcImZvcm1hdCBcIiArIGZtdCwgdGV4dCk7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBkZXByZWNhdGVkIHVzZSBmcm9tRm9ybWF0IGluc3RlYWRcbiAgICAgKi9cbiAgICA7XG5cbiAgICBEYXRlVGltZS5mcm9tU3RyaW5nID0gZnVuY3Rpb24gZnJvbVN0cmluZyh0ZXh0LCBmbXQsIG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gRGF0ZVRpbWUuZnJvbUZvcm1hdCh0ZXh0LCBmbXQsIG9wdHMpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBEYXRlVGltZSBmcm9tIGEgU1FMIGRhdGUsIHRpbWUsIG9yIGRhdGV0aW1lXG4gICAgICogRGVmYXVsdHMgdG8gZW4tVVMgaWYgbm8gbG9jYWxlIGhhcyBiZWVuIHNwZWNpZmllZCwgcmVnYXJkbGVzcyBvZiB0aGUgc3lzdGVtJ3MgbG9jYWxlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRleHQgLSB0aGUgc3RyaW5nIHRvIHBhcnNlXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRpb25zIHRvIGFmZmVjdCB0aGUgY3JlYXRpb25cbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xab25lfSBbb3B0cy56b25lPSdsb2NhbCddIC0gdXNlIHRoaXMgem9uZSBpZiBubyBvZmZzZXQgaXMgc3BlY2lmaWVkIGluIHRoZSBpbnB1dCBzdHJpbmcgaXRzZWxmLiBXaWxsIGFsc28gY29udmVydCB0aGUgRGF0ZVRpbWUgdG8gdGhpcyB6b25lXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbb3B0cy5zZXRab25lPWZhbHNlXSAtIG92ZXJyaWRlIHRoZSB6b25lIHdpdGggYSB6b25lIHNwZWNpZmllZCBpbiB0aGUgc3RyaW5nIGl0c2VsZiwgaWYgaXQgc3BlY2lmaWVzIG9uZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5sb2NhbGU9J2VuLVVTJ10gLSBhIGxvY2FsZSBzdHJpbmcgdG8gdXNlIHdoZW4gcGFyc2luZy4gV2lsbCBhbHNvIHNldCB0aGUgRGF0ZVRpbWUgdG8gdGhpcyBsb2NhbGVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0cy5udW1iZXJpbmdTeXN0ZW0gLSB0aGUgbnVtYmVyaW5nIHN5c3RlbSB0byB1c2Ugd2hlbiBwYXJzaW5nLiBXaWxsIGFsc28gc2V0IHRoZSByZXN1bHRpbmcgRGF0ZVRpbWUgdG8gdGhpcyBudW1iZXJpbmcgc3lzdGVtXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdHMub3V0cHV0Q2FsZW5kYXIgLSB0aGUgb3V0cHV0IGNhbGVuZGFyIHRvIHNldCBvbiB0aGUgcmVzdWx0aW5nIERhdGVUaW1lIGluc3RhbmNlXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbVNRTCgnMjAxNy0wNS0xNScpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbVNRTCgnMjAxNy0wNS0xNSAwOToxMjozNCcpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbVNRTCgnMjAxNy0wNS0xNSAwOToxMjozNC4zNDInKVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21TUUwoJzIwMTctMDUtMTUgMDk6MTI6MzQuMzQyKzA2OjAwJylcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5mcm9tU1FMKCcyMDE3LTA1LTE1IDA5OjEyOjM0LjM0MiBBbWVyaWNhL0xvc19BbmdlbGVzJylcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5mcm9tU1FMKCcyMDE3LTA1LTE1IDA5OjEyOjM0LjM0MiBBbWVyaWNhL0xvc19BbmdlbGVzJywgeyBzZXRab25lOiB0cnVlIH0pXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUuZnJvbVNRTCgnMjAxNy0wNS0xNSAwOToxMjozNC4zNDInLCB7IHpvbmU6ICdBbWVyaWNhL0xvc19BbmdlbGVzJyB9KVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmZyb21TUUwoJzA5OjEyOjM0LjM0MicpXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9XG4gICAgICovXG4gICAgO1xuXG4gICAgRGF0ZVRpbWUuZnJvbVNRTCA9IGZ1bmN0aW9uIGZyb21TUUwodGV4dCwgb3B0cykge1xuICAgICAgaWYgKG9wdHMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIHZhciBfcGFyc2VTUUwgPSBwYXJzZVNRTCh0ZXh0KSxcbiAgICAgICAgICB2YWxzID0gX3BhcnNlU1FMWzBdLFxuICAgICAgICAgIHBhcnNlZFpvbmUgPSBfcGFyc2VTUUxbMV07XG5cbiAgICAgIHJldHVybiBwYXJzZURhdGFUb0RhdGVUaW1lKHZhbHMsIHBhcnNlZFpvbmUsIG9wdHMsIFwiU1FMXCIsIHRleHQpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYW4gaW52YWxpZCBEYXRlVGltZS5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmVhc29uIC0gc2ltcGxlIHN0cmluZyBvZiB3aHkgdGhpcyBEYXRlVGltZSBpcyBpbnZhbGlkLiBTaG91bGQgbm90IGNvbnRhaW4gcGFyYW1ldGVycyBvciBhbnl0aGluZyBlbHNlIGRhdGEtZGVwZW5kZW50XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IFtleHBsYW5hdGlvbj1udWxsXSAtIGxvbmdlciBleHBsYW5hdGlvbiwgbWF5IGluY2x1ZGUgcGFyYW1ldGVycyBhbmQgb3RoZXIgdXNlZnVsIGRlYnVnZ2luZyBpbmZvcm1hdGlvblxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLmludmFsaWQgPSBmdW5jdGlvbiBpbnZhbGlkKHJlYXNvbiwgZXhwbGFuYXRpb24pIHtcbiAgICAgIGlmIChleHBsYW5hdGlvbiA9PT0gdm9pZCAwKSB7XG4gICAgICAgIGV4cGxhbmF0aW9uID0gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKCFyZWFzb24pIHtcbiAgICAgICAgdGhyb3cgbmV3IEludmFsaWRBcmd1bWVudEVycm9yKFwibmVlZCB0byBzcGVjaWZ5IGEgcmVhc29uIHRoZSBEYXRlVGltZSBpcyBpbnZhbGlkXCIpO1xuICAgICAgfVxuXG4gICAgICB2YXIgaW52YWxpZCA9IHJlYXNvbiBpbnN0YW5jZW9mIEludmFsaWQgPyByZWFzb24gOiBuZXcgSW52YWxpZChyZWFzb24sIGV4cGxhbmF0aW9uKTtcblxuICAgICAgaWYgKFNldHRpbmdzLnRocm93T25JbnZhbGlkKSB7XG4gICAgICAgIHRocm93IG5ldyBJbnZhbGlkRGF0ZVRpbWVFcnJvcihpbnZhbGlkKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBuZXcgRGF0ZVRpbWUoe1xuICAgICAgICAgIGludmFsaWQ6IGludmFsaWRcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICAgIC8qKlxuICAgICAqIENoZWNrIGlmIGFuIG9iamVjdCBpcyBhIERhdGVUaW1lLiBXb3JrcyBhY3Jvc3MgY29udGV4dCBib3VuZGFyaWVzXG4gICAgICogQHBhcmFtIHtvYmplY3R9IG9cbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLmlzRGF0ZVRpbWUgPSBmdW5jdGlvbiBpc0RhdGVUaW1lKG8pIHtcbiAgICAgIHJldHVybiBvICYmIG8uaXNMdXhvbkRhdGVUaW1lIHx8IGZhbHNlO1xuICAgIH0gLy8gSU5GT1xuXG4gICAgLyoqXG4gICAgICogR2V0IHRoZSB2YWx1ZSBvZiB1bml0LlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1bml0IC0gYSB1bml0IHN1Y2ggYXMgJ21pbnV0ZScgb3IgJ2RheSdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCA3LCA0KS5nZXQoJ21vbnRoJyk7IC8vPT4gN1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDcsIDQpLmdldCgnZGF5Jyk7IC8vPT4gNFxuICAgICAqIEByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICB2YXIgX3Byb3RvID0gRGF0ZVRpbWUucHJvdG90eXBlO1xuXG4gICAgX3Byb3RvLmdldCA9IGZ1bmN0aW9uIGdldCh1bml0KSB7XG4gICAgICByZXR1cm4gdGhpc1t1bml0XTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyB3aGV0aGVyIHRoZSBEYXRlVGltZSBpcyB2YWxpZC4gSW52YWxpZCBEYXRlVGltZXMgb2NjdXIgd2hlbjpcbiAgICAgKiAqIFRoZSBEYXRlVGltZSB3YXMgY3JlYXRlZCBmcm9tIGludmFsaWQgY2FsZW5kYXIgaW5mb3JtYXRpb24sIHN1Y2ggYXMgdGhlIDEzdGggbW9udGggb3IgRmVicnVhcnkgMzBcbiAgICAgKiAqIFRoZSBEYXRlVGltZSB3YXMgY3JlYXRlZCBieSBhbiBvcGVyYXRpb24gb24gYW5vdGhlciBpbnZhbGlkIGRhdGVcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSByZXNvbHZlZCBJbnRsIG9wdGlvbnMgZm9yIHRoaXMgRGF0ZVRpbWUuXG4gICAgICogVGhpcyBpcyB1c2VmdWwgaW4gdW5kZXJzdGFuZGluZyB0aGUgYmVoYXZpb3Igb2YgZm9ybWF0dGluZyBtZXRob2RzXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSB0aGUgc2FtZSBvcHRpb25zIGFzIHRvTG9jYWxlU3RyaW5nXG4gICAgICogQHJldHVybiB7T2JqZWN0fVxuICAgICAqL1xuICAgIF9wcm90by5yZXNvbHZlZExvY2FsZU9wdHMgPSBmdW5jdGlvbiByZXNvbHZlZExvY2FsZU9wdHMob3B0cykge1xuICAgICAgaWYgKG9wdHMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIHZhciBfRm9ybWF0dGVyJGNyZWF0ZSRyZXMgPSBGb3JtYXR0ZXIuY3JlYXRlKHRoaXMubG9jLmNsb25lKG9wdHMpLCBvcHRzKS5yZXNvbHZlZE9wdGlvbnModGhpcyksXG4gICAgICAgICAgbG9jYWxlID0gX0Zvcm1hdHRlciRjcmVhdGUkcmVzLmxvY2FsZSxcbiAgICAgICAgICBudW1iZXJpbmdTeXN0ZW0gPSBfRm9ybWF0dGVyJGNyZWF0ZSRyZXMubnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIGNhbGVuZGFyID0gX0Zvcm1hdHRlciRjcmVhdGUkcmVzLmNhbGVuZGFyO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBsb2NhbGU6IGxvY2FsZSxcbiAgICAgICAgbnVtYmVyaW5nU3lzdGVtOiBudW1iZXJpbmdTeXN0ZW0sXG4gICAgICAgIG91dHB1dENhbGVuZGFyOiBjYWxlbmRhclxuICAgICAgfTtcbiAgICB9IC8vIFRSQU5TRk9STVxuXG4gICAgLyoqXG4gICAgICogXCJTZXRcIiB0aGUgRGF0ZVRpbWUncyB6b25lIHRvIFVUQy4gUmV0dXJucyBhIG5ld2x5LWNvbnN0cnVjdGVkIERhdGVUaW1lLlxuICAgICAqXG4gICAgICogRXF1aXZhbGVudCB0byB7QGxpbmsgc2V0Wm9uZX0oJ3V0YycpXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtvZmZzZXQ9MF0gLSBvcHRpb25hbGx5LCBhbiBvZmZzZXQgZnJvbSBVVEMgaW4gbWludXRlc1xuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0cz17fV0gLSBvcHRpb25zIHRvIHBhc3MgdG8gYHNldFpvbmUoKWBcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9VVEMgPSBmdW5jdGlvbiB0b1VUQyhvZmZzZXQsIG9wdHMpIHtcbiAgICAgIGlmIChvZmZzZXQgPT09IHZvaWQgMCkge1xuICAgICAgICBvZmZzZXQgPSAwO1xuICAgICAgfVxuXG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuc2V0Wm9uZShGaXhlZE9mZnNldFpvbmUuaW5zdGFuY2Uob2Zmc2V0KSwgb3B0cyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFwiU2V0XCIgdGhlIERhdGVUaW1lJ3Mgem9uZSB0byB0aGUgaG9zdCdzIGxvY2FsIHpvbmUuIFJldHVybnMgYSBuZXdseS1jb25zdHJ1Y3RlZCBEYXRlVGltZS5cbiAgICAgKlxuICAgICAqIEVxdWl2YWxlbnQgdG8gYHNldFpvbmUoJ2xvY2FsJylgXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvTG9jYWwgPSBmdW5jdGlvbiB0b0xvY2FsKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc2V0Wm9uZShTZXR0aW5ncy5kZWZhdWx0Wm9uZSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFwiU2V0XCIgdGhlIERhdGVUaW1lJ3Mgem9uZSB0byBzcGVjaWZpZWQgem9uZS4gUmV0dXJucyBhIG5ld2x5LWNvbnN0cnVjdGVkIERhdGVUaW1lLlxuICAgICAqXG4gICAgICogQnkgZGVmYXVsdCwgdGhlIHNldHRlciBrZWVwcyB0aGUgdW5kZXJseWluZyB0aW1lIHRoZSBzYW1lIChhcyBpbiwgdGhlIHNhbWUgdGltZXN0YW1wKSwgYnV0IHRoZSBuZXcgaW5zdGFuY2Ugd2lsbCByZXBvcnQgZGlmZmVyZW50IGxvY2FsIHRpbWVzIGFuZCBjb25zaWRlciBEU1RzIHdoZW4gbWFraW5nIGNvbXB1dGF0aW9ucywgYXMgd2l0aCB7QGxpbmsgcGx1c30uIFlvdSBtYXkgd2lzaCB0byB1c2Uge0BsaW5rIHRvTG9jYWx9IGFuZCB7QGxpbmsgdG9VVEN9IHdoaWNoIHByb3ZpZGUgc2ltcGxlIGNvbnZlbmllbmNlIHdyYXBwZXJzIGZvciBjb21tb25seSB1c2VkIHpvbmVzLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfFpvbmV9IFt6b25lPSdsb2NhbCddIC0gYSB6b25lIGlkZW50aWZpZXIuIEFzIGEgc3RyaW5nLCB0aGF0IGNhbiBiZSBhbnkgSUFOQSB6b25lIHN1cHBvcnRlZCBieSB0aGUgaG9zdCBlbnZpcm9ubWVudCwgb3IgYSBmaXhlZC1vZmZzZXQgbmFtZSBvZiB0aGUgZm9ybSAnVVRDKzMnLCBvciB0aGUgc3RyaW5ncyAnbG9jYWwnIG9yICd1dGMnLiBZb3UgbWF5IGFsc28gc3VwcGx5IGFuIGluc3RhbmNlIG9mIGEge0BsaW5rIFpvbmV9IGNsYXNzLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9uc1xuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMua2VlcExvY2FsVGltZT1mYWxzZV0gLSBJZiB0cnVlLCBhZGp1c3QgdGhlIHVuZGVybHlpbmcgdGltZSBzbyB0aGF0IHRoZSBsb2NhbCB0aW1lIHN0YXlzIHRoZSBzYW1lLCBidXQgaW4gdGhlIHRhcmdldCB6b25lLiBZb3Ugc2hvdWxkIHJhcmVseSBuZWVkIHRoaXMuXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnNldFpvbmUgPSBmdW5jdGlvbiBzZXRab25lKHpvbmUsIF90ZW1wKSB7XG4gICAgICB2YXIgX3JlZjUgPSBfdGVtcCA9PT0gdm9pZCAwID8ge30gOiBfdGVtcCxcbiAgICAgICAgICBfcmVmNSRrZWVwTG9jYWxUaW1lID0gX3JlZjUua2VlcExvY2FsVGltZSxcbiAgICAgICAgICBrZWVwTG9jYWxUaW1lID0gX3JlZjUka2VlcExvY2FsVGltZSA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcmVmNSRrZWVwTG9jYWxUaW1lLFxuICAgICAgICAgIF9yZWY1JGtlZXBDYWxlbmRhclRpbSA9IF9yZWY1LmtlZXBDYWxlbmRhclRpbWUsXG4gICAgICAgICAga2VlcENhbGVuZGFyVGltZSA9IF9yZWY1JGtlZXBDYWxlbmRhclRpbSA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcmVmNSRrZWVwQ2FsZW5kYXJUaW07XG5cbiAgICAgIHpvbmUgPSBub3JtYWxpemVab25lKHpvbmUsIFNldHRpbmdzLmRlZmF1bHRab25lKTtcblxuICAgICAgaWYgKHpvbmUuZXF1YWxzKHRoaXMuem9uZSkpIHtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9IGVsc2UgaWYgKCF6b25lLmlzVmFsaWQpIHtcbiAgICAgICAgcmV0dXJuIERhdGVUaW1lLmludmFsaWQodW5zdXBwb3J0ZWRab25lKHpvbmUpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBuZXdUUyA9IHRoaXMudHM7XG5cbiAgICAgICAgaWYgKGtlZXBMb2NhbFRpbWUgfHwga2VlcENhbGVuZGFyVGltZSkge1xuICAgICAgICAgIHZhciBvZmZzZXRHdWVzcyA9IHRoaXMubyAtIHpvbmUub2Zmc2V0KHRoaXMudHMpO1xuICAgICAgICAgIHZhciBhc09iaiA9IHRoaXMudG9PYmplY3QoKTtcblxuICAgICAgICAgIHZhciBfb2JqVG9UUzMgPSBvYmpUb1RTKGFzT2JqLCBvZmZzZXRHdWVzcywgem9uZSk7XG5cbiAgICAgICAgICBuZXdUUyA9IF9vYmpUb1RTM1swXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBjbG9uZSQxKHRoaXMsIHtcbiAgICAgICAgICB0czogbmV3VFMsXG4gICAgICAgICAgem9uZTogem9uZVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gICAgLyoqXG4gICAgICogXCJTZXRcIiB0aGUgbG9jYWxlLCBudW1iZXJpbmdTeXN0ZW0sIG9yIG91dHB1dENhbGVuZGFyLiBSZXR1cm5zIGEgbmV3bHktY29uc3RydWN0ZWQgRGF0ZVRpbWUuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IHByb3BlcnRpZXMgLSB0aGUgcHJvcGVydGllcyB0byBzZXRcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCA1LCAyNSkucmVjb25maWd1cmUoeyBsb2NhbGU6ICdlbi1HQicgfSlcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8ucmVjb25maWd1cmUgPSBmdW5jdGlvbiByZWNvbmZpZ3VyZShfdGVtcDIpIHtcbiAgICAgIHZhciBfcmVmNiA9IF90ZW1wMiA9PT0gdm9pZCAwID8ge30gOiBfdGVtcDIsXG4gICAgICAgICAgbG9jYWxlID0gX3JlZjYubG9jYWxlLFxuICAgICAgICAgIG51bWJlcmluZ1N5c3RlbSA9IF9yZWY2Lm51bWJlcmluZ1N5c3RlbSxcbiAgICAgICAgICBvdXRwdXRDYWxlbmRhciA9IF9yZWY2Lm91dHB1dENhbGVuZGFyO1xuXG4gICAgICB2YXIgbG9jID0gdGhpcy5sb2MuY2xvbmUoe1xuICAgICAgICBsb2NhbGU6IGxvY2FsZSxcbiAgICAgICAgbnVtYmVyaW5nU3lzdGVtOiBudW1iZXJpbmdTeXN0ZW0sXG4gICAgICAgIG91dHB1dENhbGVuZGFyOiBvdXRwdXRDYWxlbmRhclxuICAgICAgfSk7XG4gICAgICByZXR1cm4gY2xvbmUkMSh0aGlzLCB7XG4gICAgICAgIGxvYzogbG9jXG4gICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogXCJTZXRcIiB0aGUgbG9jYWxlLiBSZXR1cm5zIGEgbmV3bHktY29uc3RydWN0ZWQgRGF0ZVRpbWUuXG4gICAgICogSnVzdCBhIGNvbnZlbmllbnQgYWxpYXMgZm9yIHJlY29uZmlndXJlKHsgbG9jYWxlIH0pXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgNSwgMjUpLnNldExvY2FsZSgnZW4tR0InKVxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5zZXRMb2NhbGUgPSBmdW5jdGlvbiBzZXRMb2NhbGUobG9jYWxlKSB7XG4gICAgICByZXR1cm4gdGhpcy5yZWNvbmZpZ3VyZSh7XG4gICAgICAgIGxvY2FsZTogbG9jYWxlXG4gICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogXCJTZXRcIiB0aGUgdmFsdWVzIG9mIHNwZWNpZmllZCB1bml0cy4gUmV0dXJucyBhIG5ld2x5LWNvbnN0cnVjdGVkIERhdGVUaW1lLlxuICAgICAqIFlvdSBjYW4gb25seSBzZXQgdW5pdHMgd2l0aCB0aGlzIG1ldGhvZDsgZm9yIFwic2V0dGluZ1wiIG1ldGFkYXRhLCBzZWUge0BsaW5rIHJlY29uZmlndXJlfSBhbmQge0BsaW5rIHNldFpvbmV9LlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSB2YWx1ZXMgLSBhIG1hcHBpbmcgb2YgdW5pdHMgdG8gbnVtYmVyc1xuICAgICAqIEBleGFtcGxlIGR0LnNldCh7IHllYXI6IDIwMTcgfSlcbiAgICAgKiBAZXhhbXBsZSBkdC5zZXQoeyBob3VyOiA4LCBtaW51dGU6IDMwIH0pXG4gICAgICogQGV4YW1wbGUgZHQuc2V0KHsgd2Vla2RheTogNSB9KVxuICAgICAqIEBleGFtcGxlIGR0LnNldCh7IHllYXI6IDIwMDUsIG9yZGluYWw6IDIzNCB9KVxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5zZXQgPSBmdW5jdGlvbiBzZXQodmFsdWVzKSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHRoaXM7XG4gICAgICB2YXIgbm9ybWFsaXplZCA9IG5vcm1hbGl6ZU9iamVjdCh2YWx1ZXMsIG5vcm1hbGl6ZVVuaXQsIFtdKSxcbiAgICAgICAgICBzZXR0aW5nV2Vla1N0dWZmID0gIWlzVW5kZWZpbmVkKG5vcm1hbGl6ZWQud2Vla1llYXIpIHx8ICFpc1VuZGVmaW5lZChub3JtYWxpemVkLndlZWtOdW1iZXIpIHx8ICFpc1VuZGVmaW5lZChub3JtYWxpemVkLndlZWtkYXkpO1xuICAgICAgdmFyIG1peGVkO1xuXG4gICAgICBpZiAoc2V0dGluZ1dlZWtTdHVmZikge1xuICAgICAgICBtaXhlZCA9IHdlZWtUb0dyZWdvcmlhbihPYmplY3QuYXNzaWduKGdyZWdvcmlhblRvV2Vlayh0aGlzLmMpLCBub3JtYWxpemVkKSk7XG4gICAgICB9IGVsc2UgaWYgKCFpc1VuZGVmaW5lZChub3JtYWxpemVkLm9yZGluYWwpKSB7XG4gICAgICAgIG1peGVkID0gb3JkaW5hbFRvR3JlZ29yaWFuKE9iamVjdC5hc3NpZ24oZ3JlZ29yaWFuVG9PcmRpbmFsKHRoaXMuYyksIG5vcm1hbGl6ZWQpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG1peGVkID0gT2JqZWN0LmFzc2lnbih0aGlzLnRvT2JqZWN0KCksIG5vcm1hbGl6ZWQpOyAvLyBpZiB3ZSBkaWRuJ3Qgc2V0IHRoZSBkYXkgYnV0IHdlIGVuZGVkIHVwIG9uIGFuIG92ZXJmbG93IGRhdGUsXG4gICAgICAgIC8vIHVzZSB0aGUgbGFzdCBkYXkgb2YgdGhlIHJpZ2h0IG1vbnRoXG5cbiAgICAgICAgaWYgKGlzVW5kZWZpbmVkKG5vcm1hbGl6ZWQuZGF5KSkge1xuICAgICAgICAgIG1peGVkLmRheSA9IE1hdGgubWluKGRheXNJbk1vbnRoKG1peGVkLnllYXIsIG1peGVkLm1vbnRoKSwgbWl4ZWQuZGF5KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB2YXIgX29ialRvVFM0ID0gb2JqVG9UUyhtaXhlZCwgdGhpcy5vLCB0aGlzLnpvbmUpLFxuICAgICAgICAgIHRzID0gX29ialRvVFM0WzBdLFxuICAgICAgICAgIG8gPSBfb2JqVG9UUzRbMV07XG5cbiAgICAgIHJldHVybiBjbG9uZSQxKHRoaXMsIHtcbiAgICAgICAgdHM6IHRzLFxuICAgICAgICBvOiBvXG4gICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQWRkIGEgcGVyaW9kIG9mIHRpbWUgdG8gdGhpcyBEYXRlVGltZSBhbmQgcmV0dXJuIHRoZSByZXN1bHRpbmcgRGF0ZVRpbWVcbiAgICAgKlxuICAgICAqIEFkZGluZyBob3VycywgbWludXRlcywgc2Vjb25kcywgb3IgbWlsbGlzZWNvbmRzIGluY3JlYXNlcyB0aGUgdGltZXN0YW1wIGJ5IHRoZSByaWdodCBudW1iZXIgb2YgbWlsbGlzZWNvbmRzLiBBZGRpbmcgZGF5cywgbW9udGhzLCBvciB5ZWFycyBzaGlmdHMgdGhlIGNhbGVuZGFyLCBhY2NvdW50aW5nIGZvciBEU1RzIGFuZCBsZWFwIHllYXJzIGFsb25nIHRoZSB3YXkuIFRodXMsIGBkdC5wbHVzKHsgaG91cnM6IDI0IH0pYCBtYXkgcmVzdWx0IGluIGEgZGlmZmVyZW50IHRpbWUgdGhhbiBgZHQucGx1cyh7IGRheXM6IDEgfSlgIGlmIHRoZXJlJ3MgYSBEU1Qgc2hpZnQgaW4gYmV0d2Vlbi5cbiAgICAgKiBAcGFyYW0ge0R1cmF0aW9ufE9iamVjdHxudW1iZXJ9IGR1cmF0aW9uIC0gVGhlIGFtb3VudCB0byBhZGQuIEVpdGhlciBhIEx1eG9uIER1cmF0aW9uLCBhIG51bWJlciBvZiBtaWxsaXNlY29uZHMsIHRoZSBvYmplY3QgYXJndW1lbnQgdG8gRHVyYXRpb24uZnJvbU9iamVjdCgpXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5wbHVzKDEyMykgLy9+PiBpbiAxMjMgbWlsbGlzZWNvbmRzXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5wbHVzKHsgbWludXRlczogMTUgfSkgLy9+PiBpbiAxNSBtaW51dGVzXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5wbHVzKHsgZGF5czogMSB9KSAvL34+IHRoaXMgdGltZSB0b21vcnJvd1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkucGx1cyh7IGRheXM6IC0xIH0pIC8vfj4gdGhpcyB0aW1lIHllc3RlcmRheVxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkucGx1cyh7IGhvdXJzOiAzLCBtaW51dGVzOiAxMyB9KSAvL34+IGluIDMgaHIsIDEzIG1pblxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkucGx1cyhEdXJhdGlvbi5mcm9tT2JqZWN0KHsgaG91cnM6IDMsIG1pbnV0ZXM6IDEzIH0pKSAvL34+IGluIDMgaHIsIDEzIG1pblxuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5wbHVzID0gZnVuY3Rpb24gcGx1cyhkdXJhdGlvbikge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuICAgICAgdmFyIGR1ciA9IGZyaWVuZGx5RHVyYXRpb24oZHVyYXRpb24pO1xuICAgICAgcmV0dXJuIGNsb25lJDEodGhpcywgYWRqdXN0VGltZSh0aGlzLCBkdXIpKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogU3VidHJhY3QgYSBwZXJpb2Qgb2YgdGltZSB0byB0aGlzIERhdGVUaW1lIGFuZCByZXR1cm4gdGhlIHJlc3VsdGluZyBEYXRlVGltZVxuICAgICAqIFNlZSB7QGxpbmsgcGx1c31cbiAgICAgKiBAcGFyYW0ge0R1cmF0aW9ufE9iamVjdHxudW1iZXJ9IGR1cmF0aW9uIC0gVGhlIGFtb3VudCB0byBzdWJ0cmFjdC4gRWl0aGVyIGEgTHV4b24gRHVyYXRpb24sIGEgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcywgdGhlIG9iamVjdCBhcmd1bWVudCB0byBEdXJhdGlvbi5mcm9tT2JqZWN0KClcbiAgICAgQHJldHVybiB7RGF0ZVRpbWV9XG4gICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8ubWludXMgPSBmdW5jdGlvbiBtaW51cyhkdXJhdGlvbikge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuICAgICAgdmFyIGR1ciA9IGZyaWVuZGx5RHVyYXRpb24oZHVyYXRpb24pLm5lZ2F0ZSgpO1xuICAgICAgcmV0dXJuIGNsb25lJDEodGhpcywgYWRqdXN0VGltZSh0aGlzLCBkdXIpKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogXCJTZXRcIiB0aGlzIERhdGVUaW1lIHRvIHRoZSBiZWdpbm5pbmcgb2YgYSB1bml0IG9mIHRpbWUuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVuaXQgLSBUaGUgdW5pdCB0byBnbyB0byB0aGUgYmVnaW5uaW5nIG9mLiBDYW4gYmUgJ3llYXInLCAncXVhcnRlcicsICdtb250aCcsICd3ZWVrJywgJ2RheScsICdob3VyJywgJ21pbnV0ZScsICdzZWNvbmQnLCBvciAnbWlsbGlzZWNvbmQnLlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTQsIDMsIDMpLnN0YXJ0T2YoJ21vbnRoJykudG9JU09EYXRlKCk7IC8vPT4gJzIwMTQtMDMtMDEnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNCwgMywgMykuc3RhcnRPZigneWVhcicpLnRvSVNPRGF0ZSgpOyAvLz0+ICcyMDE0LTAxLTAxJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTQsIDMsIDMsIDUsIDMwKS5zdGFydE9mKCdkYXknKS50b0lTT1RpbWUoKTsgLy89PiAnMDA6MDAuMDAwLTA1OjAwJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTQsIDMsIDMsIDUsIDMwKS5zdGFydE9mKCdob3VyJykudG9JU09UaW1lKCk7IC8vPT4gJzA1OjAwOjAwLjAwMC0wNTowMCdcbiAgICAgKiBAcmV0dXJuIHtEYXRlVGltZX1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uc3RhcnRPZiA9IGZ1bmN0aW9uIHN0YXJ0T2YodW5pdCkge1xuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiB0aGlzO1xuICAgICAgdmFyIG8gPSB7fSxcbiAgICAgICAgICBub3JtYWxpemVkVW5pdCA9IER1cmF0aW9uLm5vcm1hbGl6ZVVuaXQodW5pdCk7XG5cbiAgICAgIHN3aXRjaCAobm9ybWFsaXplZFVuaXQpIHtcbiAgICAgICAgY2FzZSBcInllYXJzXCI6XG4gICAgICAgICAgby5tb250aCA9IDE7XG4gICAgICAgIC8vIGZhbGxzIHRocm91Z2hcblxuICAgICAgICBjYXNlIFwicXVhcnRlcnNcIjpcbiAgICAgICAgY2FzZSBcIm1vbnRoc1wiOlxuICAgICAgICAgIG8uZGF5ID0gMTtcbiAgICAgICAgLy8gZmFsbHMgdGhyb3VnaFxuXG4gICAgICAgIGNhc2UgXCJ3ZWVrc1wiOlxuICAgICAgICBjYXNlIFwiZGF5c1wiOlxuICAgICAgICAgIG8uaG91ciA9IDA7XG4gICAgICAgIC8vIGZhbGxzIHRocm91Z2hcblxuICAgICAgICBjYXNlIFwiaG91cnNcIjpcbiAgICAgICAgICBvLm1pbnV0ZSA9IDA7XG4gICAgICAgIC8vIGZhbGxzIHRocm91Z2hcblxuICAgICAgICBjYXNlIFwibWludXRlc1wiOlxuICAgICAgICAgIG8uc2Vjb25kID0gMDtcbiAgICAgICAgLy8gZmFsbHMgdGhyb3VnaFxuXG4gICAgICAgIGNhc2UgXCJzZWNvbmRzXCI6XG4gICAgICAgICAgby5taWxsaXNlY29uZCA9IDA7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBcIm1pbGxpc2Vjb25kc1wiOlxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICAvLyBubyBkZWZhdWx0LCBpbnZhbGlkIHVuaXRzIHRocm93IGluIG5vcm1hbGl6ZVVuaXQoKVxuICAgICAgfVxuXG4gICAgICBpZiAobm9ybWFsaXplZFVuaXQgPT09IFwid2Vla3NcIikge1xuICAgICAgICBvLndlZWtkYXkgPSAxO1xuICAgICAgfVxuXG4gICAgICBpZiAobm9ybWFsaXplZFVuaXQgPT09IFwicXVhcnRlcnNcIikge1xuICAgICAgICB2YXIgcSA9IE1hdGguY2VpbCh0aGlzLm1vbnRoIC8gMyk7XG4gICAgICAgIG8ubW9udGggPSAocSAtIDEpICogMyArIDE7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLnNldChvKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogXCJTZXRcIiB0aGlzIERhdGVUaW1lIHRvIHRoZSBlbmQgKG1lYW5pbmcgdGhlIGxhc3QgbWlsbGlzZWNvbmQpIG9mIGEgdW5pdCBvZiB0aW1lXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVuaXQgLSBUaGUgdW5pdCB0byBnbyB0byB0aGUgZW5kIG9mLiBDYW4gYmUgJ3llYXInLCAnbW9udGgnLCAnZGF5JywgJ2hvdXInLCAnbWludXRlJywgJ3NlY29uZCcsIG9yICdtaWxsaXNlY29uZCcuXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNCwgMywgMykuZW5kT2YoJ21vbnRoJykudG9JU08oKTsgLy89PiAnMjAxNC0wMy0zMVQyMzo1OTo1OS45OTktMDU6MDAnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNCwgMywgMykuZW5kT2YoJ3llYXInKS50b0lTTygpOyAvLz0+ICcyMDE0LTEyLTMxVDIzOjU5OjU5Ljk5OS0wNTowMCdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE0LCAzLCAzLCA1LCAzMCkuZW5kT2YoJ2RheScpLnRvSVNPKCk7IC8vPT4gJzIwMTQtMDMtMDNUMjM6NTk6NTkuOTk5LTA1OjAwJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTQsIDMsIDMsIDUsIDMwKS5lbmRPZignaG91cicpLnRvSVNPKCk7IC8vPT4gJzIwMTQtMDMtMDNUMDU6NTk6NTkuOTk5LTA1OjAwJ1xuICAgICAqIEByZXR1cm4ge0RhdGVUaW1lfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5lbmRPZiA9IGZ1bmN0aW9uIGVuZE9mKHVuaXQpIHtcbiAgICAgIHZhciBfdGhpcyRwbHVzO1xuXG4gICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy5wbHVzKChfdGhpcyRwbHVzID0ge30sIF90aGlzJHBsdXNbdW5pdF0gPSAxLCBfdGhpcyRwbHVzKSkuc3RhcnRPZih1bml0KS5taW51cygxKSA6IHRoaXM7XG4gICAgfSAvLyBPVVRQVVRcblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYSBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBEYXRlVGltZSBmb3JtYXR0ZWQgYWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpZWQgZm9ybWF0IHN0cmluZy5cbiAgICAgKiAqKllvdSBtYXkgbm90IHdhbnQgdGhpcy4qKiBTZWUge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3IgYSBtb3JlIGZsZXhpYmxlIGZvcm1hdHRpbmcgdG9vbC4gRm9yIGEgdGFibGUgb2YgdG9rZW5zIGFuZCB0aGVpciBpbnRlcnByZXRhdGlvbnMsIHNlZSBbaGVyZV0oaHR0cHM6Ly9tb21lbnQuZ2l0aHViLmlvL2x1eG9uL2RvY3MvbWFudWFsL2Zvcm1hdHRpbmcuaHRtbCN0YWJsZS1vZi10b2tlbnMpLlxuICAgICAqIERlZmF1bHRzIHRvIGVuLVVTIGlmIG5vIGxvY2FsZSBoYXMgYmVlbiBzcGVjaWZpZWQsIHJlZ2FyZGxlc3Mgb2YgdGhlIHN5c3RlbSdzIGxvY2FsZS5cbiAgICAgKiBAc2VlIGh0dHBzOi8vbW9tZW50LmdpdGh1Yi5pby9sdXhvbi9kb2NzL21hbnVhbC9mb3JtYXR0aW5nLmh0bWwjdGFibGUtb2YtdG9rZW5zXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGZtdCAtIHRoZSBmb3JtYXQgc3RyaW5nXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRzIHRvIG92ZXJyaWRlIHRoZSBjb25maWd1cmF0aW9uIG9wdGlvbnNcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnRvRm9ybWF0KCd5eXl5IExMTCBkZCcpIC8vPT4gJzIwMTcgQXByIDIyJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkuc2V0TG9jYWxlKCdmcicpLnRvRm9ybWF0KCd5eXl5IExMTCBkZCcpIC8vPT4gJzIwMTcgYXZyLiAyMidcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnRvRm9ybWF0KCd5eXl5IExMTCBkZCcsIHsgbG9jYWxlOiBcImZyXCIgfSkgLy89PiAnMjAxNyBhdnIuIDIyJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkudG9Gb3JtYXQoXCJISCAnaG91cnMgYW5kJyBtbSAnbWludXRlcydcIikgLy89PiAnMjAgaG91cnMgYW5kIDU1IG1pbnV0ZXMnXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b0Zvcm1hdCA9IGZ1bmN0aW9uIHRvRm9ybWF0KGZtdCwgb3B0cykge1xuICAgICAgaWYgKG9wdHMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyBGb3JtYXR0ZXIuY3JlYXRlKHRoaXMubG9jLnJlZGVmYXVsdFRvRU4ob3B0cykpLmZvcm1hdERhdGVUaW1lRnJvbVN0cmluZyh0aGlzLCBmbXQpIDogSU5WQUxJRCQyO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgbG9jYWxpemVkIHN0cmluZyByZXByZXNlbnRpbmcgdGhpcyBkYXRlLiBBY2NlcHRzIHRoZSBzYW1lIG9wdGlvbnMgYXMgdGhlIEludGwuRGF0ZVRpbWVGb3JtYXQgY29uc3RydWN0b3IgYW5kIGFueSBwcmVzZXRzIGRlZmluZWQgYnkgTHV4b24sIHN1Y2ggYXMgYERhdGVUaW1lLkRBVEVfRlVMTGAgb3IgYERhdGVUaW1lLlRJTUVfU0lNUExFYC5cbiAgICAgKiBUaGUgZXhhY3QgYmVoYXZpb3Igb2YgdGhpcyBtZXRob2QgaXMgYnJvd3Nlci1zcGVjaWZpYywgYnV0IGluIGdlbmVyYWwgaXQgd2lsbCByZXR1cm4gYW4gYXBwcm9wcmlhdGUgcmVwcmVzZW50YXRpb25cbiAgICAgKiBvZiB0aGUgRGF0ZVRpbWUgaW4gdGhlIGFzc2lnbmVkIGxvY2FsZS5cbiAgICAgKiBEZWZhdWx0cyB0byB0aGUgc3lzdGVtJ3MgbG9jYWxlIGlmIG5vIGxvY2FsZSBoYXMgYmVlbiBzcGVjaWZpZWRcbiAgICAgKiBAc2VlIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0phdmFTY3JpcHQvUmVmZXJlbmNlL0dsb2JhbF9PYmplY3RzL0RhdGVUaW1lRm9ybWF0XG4gICAgICogQHBhcmFtIG9wdHMge09iamVjdH0gLSBJbnRsLkRhdGVUaW1lRm9ybWF0IGNvbnN0cnVjdG9yIG9wdGlvbnMgYW5kIGNvbmZpZ3VyYXRpb24gb3B0aW9uc1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkudG9Mb2NhbGVTdHJpbmcoKTsgLy89PiA0LzIwLzIwMTdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnNldExvY2FsZSgnZW4tZ2InKS50b0xvY2FsZVN0cmluZygpOyAvLz0+ICcyMC8wNC8yMDE3J1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkudG9Mb2NhbGVTdHJpbmcoeyBsb2NhbGU6ICdlbi1nYicgfSk7IC8vPT4gJzIwLzA0LzIwMTcnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b0xvY2FsZVN0cmluZyhEYXRlVGltZS5EQVRFX0ZVTEwpOyAvLz0+ICdBcHJpbCAyMCwgMjAxNydcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnRvTG9jYWxlU3RyaW5nKERhdGVUaW1lLlRJTUVfU0lNUExFKTsgLy89PiAnMTE6MzIgQU0nXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b0xvY2FsZVN0cmluZyhEYXRlVGltZS5EQVRFVElNRV9TSE9SVCk7IC8vPT4gJzQvMjAvMjAxNywgMTE6MzIgQU0nXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b0xvY2FsZVN0cmluZyh7IHdlZWtkYXk6ICdsb25nJywgbW9udGg6ICdsb25nJywgZGF5OiAnMi1kaWdpdCcgfSk7IC8vPT4gJ1RodXJzZGF5LCBBcHJpbCAyMCdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnRvTG9jYWxlU3RyaW5nKHsgd2Vla2RheTogJ3Nob3J0JywgbW9udGg6ICdzaG9ydCcsIGRheTogJzItZGlnaXQnLCBob3VyOiAnMi1kaWdpdCcsIG1pbnV0ZTogJzItZGlnaXQnIH0pOyAvLz0+ICdUaHUsIEFwciAyMCwgMTE6MjcgQU0nXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b0xvY2FsZVN0cmluZyh7IGhvdXI6ICcyLWRpZ2l0JywgbWludXRlOiAnMi1kaWdpdCcsIGhvdXIxMjogZmFsc2UgfSk7IC8vPT4gJzExOjMyJ1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9Mb2NhbGVTdHJpbmcgPSBmdW5jdGlvbiB0b0xvY2FsZVN0cmluZyhvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSBEQVRFX1NIT1JUO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gRm9ybWF0dGVyLmNyZWF0ZSh0aGlzLmxvYy5jbG9uZShvcHRzKSwgb3B0cykuZm9ybWF0RGF0ZVRpbWUodGhpcykgOiBJTlZBTElEJDI7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gYXJyYXkgb2YgZm9ybWF0IFwicGFydHNcIiwgbWVhbmluZyBpbmRpdmlkdWFsIHRva2VucyBhbG9uZyB3aXRoIG1ldGFkYXRhLiBUaGlzIGlzIGFsbG93cyBjYWxsZXJzIHRvIHBvc3QtcHJvY2VzcyBpbmRpdmlkdWFsIHNlY3Rpb25zIG9mIHRoZSBmb3JtYXR0ZWQgb3V0cHV0LlxuICAgICAqIERlZmF1bHRzIHRvIHRoZSBzeXN0ZW0ncyBsb2NhbGUgaWYgbm8gbG9jYWxlIGhhcyBiZWVuIHNwZWNpZmllZFxuICAgICAqIEBzZWUgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvRGF0ZVRpbWVGb3JtYXQvZm9ybWF0VG9QYXJ0c1xuICAgICAqIEBwYXJhbSBvcHRzIHtPYmplY3R9IC0gSW50bC5EYXRlVGltZUZvcm1hdCBjb25zdHJ1Y3RvciBvcHRpb25zLCBzYW1lIGFzIGB0b0xvY2FsZVN0cmluZ2AuXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b0xvY2FsZVN0cmluZygpOyAvLz0+IFtcbiAgICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vPT4gICB7IHR5cGU6ICdkYXknLCB2YWx1ZTogJzI1JyB9LFxuICAgICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy89PiAgIHsgdHlwZTogJ2xpdGVyYWwnLCB2YWx1ZTogJy8nIH0sXG4gICAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLz0+ICAgeyB0eXBlOiAnbW9udGgnLCB2YWx1ZTogJzA1JyB9LFxuICAgICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy89PiAgIHsgdHlwZTogJ2xpdGVyYWwnLCB2YWx1ZTogJy8nIH0sXG4gICAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLz0+ICAgeyB0eXBlOiAneWVhcicsIHZhbHVlOiAnMTk4MicgfVxuICAgICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy89PiBdXG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvTG9jYWxlUGFydHMgPSBmdW5jdGlvbiB0b0xvY2FsZVBhcnRzKG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gRm9ybWF0dGVyLmNyZWF0ZSh0aGlzLmxvYy5jbG9uZShvcHRzKSwgb3B0cykuZm9ybWF0RGF0ZVRpbWVQYXJ0cyh0aGlzKSA6IFtdO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIElTTyA4NjAxLWNvbXBsaWFudCBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBEYXRlVGltZVxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9uc1xuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc3VwcHJlc3NNaWxsaXNlY29uZHM9ZmFsc2VdIC0gZXhjbHVkZSBtaWxsaXNlY29uZHMgZnJvbSB0aGUgZm9ybWF0IGlmIHRoZXkncmUgMFxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc3VwcHJlc3NTZWNvbmRzPWZhbHNlXSAtIGV4Y2x1ZGUgc2Vjb25kcyBmcm9tIHRoZSBmb3JtYXQgaWYgdGhleSdyZSAwXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbb3B0cy5pbmNsdWRlT2Zmc2V0PXRydWVdIC0gaW5jbHVkZSB0aGUgb2Zmc2V0LCBzdWNoIGFzICdaJyBvciAnLTA0OjAwJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygxOTgyLCA1LCAyNSkudG9JU08oKSAvLz0+ICcxOTgyLTA1LTI1VDAwOjAwOjAwLjAwMFonXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b0lTTygpIC8vPT4gJzIwMTctMDQtMjJUMjA6NDc6MDUuMzM1LTA0OjAwJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkudG9JU08oeyBpbmNsdWRlT2Zmc2V0OiBmYWxzZSB9KSAvLz0+ICcyMDE3LTA0LTIyVDIwOjQ3OjA1LjMzNSdcbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvSVNPID0gZnVuY3Rpb24gdG9JU08ob3B0cykge1xuICAgICAgaWYgKG9wdHMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy50b0lTT0RhdGUoKSArIFwiVFwiICsgdGhpcy50b0lTT1RpbWUob3B0cyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gSVNPIDg2MDEtY29tcGxpYW50IHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIERhdGVUaW1lJ3MgZGF0ZSBjb21wb25lbnRcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS51dGMoMTk4MiwgNSwgMjUpLnRvSVNPRGF0ZSgpIC8vPT4gJzE5ODItMDUtMjUnXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b0lTT0RhdGUgPSBmdW5jdGlvbiB0b0lTT0RhdGUoKSB7XG4gICAgICB2YXIgZm9ybWF0ID0gXCJ5eXl5LU1NLWRkXCI7XG5cbiAgICAgIGlmICh0aGlzLnllYXIgPiA5OTk5KSB7XG4gICAgICAgIGZvcm1hdCA9IFwiK1wiICsgZm9ybWF0O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdG9UZWNoRm9ybWF0KHRoaXMsIGZvcm1hdCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gSVNPIDg2MDEtY29tcGxpYW50IHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIERhdGVUaW1lJ3Mgd2VlayBkYXRlXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUudXRjKDE5ODIsIDUsIDI1KS50b0lTT1dlZWtEYXRlKCkgLy89PiAnMTk4Mi1XMjEtMidcbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvSVNPV2Vla0RhdGUgPSBmdW5jdGlvbiB0b0lTT1dlZWtEYXRlKCkge1xuICAgICAgcmV0dXJuIHRvVGVjaEZvcm1hdCh0aGlzLCBcImtra2stJ1cnV1ctY1wiKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhbiBJU08gODYwMS1jb21wbGlhbnQgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRoaXMgRGF0ZVRpbWUncyB0aW1lIGNvbXBvbmVudFxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzIC0gb3B0aW9uc1xuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc3VwcHJlc3NNaWxsaXNlY29uZHM9ZmFsc2VdIC0gZXhjbHVkZSBtaWxsaXNlY29uZHMgZnJvbSB0aGUgZm9ybWF0IGlmIHRoZXkncmUgMFxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc3VwcHJlc3NTZWNvbmRzPWZhbHNlXSAtIGV4Y2x1ZGUgc2Vjb25kcyBmcm9tIHRoZSBmb3JtYXQgaWYgdGhleSdyZSAwXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbb3B0cy5pbmNsdWRlT2Zmc2V0PXRydWVdIC0gaW5jbHVkZSB0aGUgb2Zmc2V0LCBzdWNoIGFzICdaJyBvciAnLTA0OjAwJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygpLmhvdXIoNykubWludXRlKDM0KS50b0lTT1RpbWUoKSAvLz0+ICcwNzozNDoxOS4zNjFaJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygpLmhvdXIoNykubWludXRlKDM0KS50b0lTT1RpbWUoeyBzdXBwcmVzc1NlY29uZHM6IHRydWUgfSkgLy89PiAnMDc6MzRaJ1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9JU09UaW1lID0gZnVuY3Rpb24gdG9JU09UaW1lKF90ZW1wMykge1xuICAgICAgdmFyIF9yZWY3ID0gX3RlbXAzID09PSB2b2lkIDAgPyB7fSA6IF90ZW1wMyxcbiAgICAgICAgICBfcmVmNyRzdXBwcmVzc01pbGxpc2UgPSBfcmVmNy5zdXBwcmVzc01pbGxpc2Vjb25kcyxcbiAgICAgICAgICBzdXBwcmVzc01pbGxpc2Vjb25kcyA9IF9yZWY3JHN1cHByZXNzTWlsbGlzZSA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcmVmNyRzdXBwcmVzc01pbGxpc2UsXG4gICAgICAgICAgX3JlZjckc3VwcHJlc3NTZWNvbmRzID0gX3JlZjcuc3VwcHJlc3NTZWNvbmRzLFxuICAgICAgICAgIHN1cHByZXNzU2Vjb25kcyA9IF9yZWY3JHN1cHByZXNzU2Vjb25kcyA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcmVmNyRzdXBwcmVzc1NlY29uZHMsXG4gICAgICAgICAgX3JlZjckaW5jbHVkZU9mZnNldCA9IF9yZWY3LmluY2x1ZGVPZmZzZXQsXG4gICAgICAgICAgaW5jbHVkZU9mZnNldCA9IF9yZWY3JGluY2x1ZGVPZmZzZXQgPT09IHZvaWQgMCA/IHRydWUgOiBfcmVmNyRpbmNsdWRlT2Zmc2V0O1xuXG4gICAgICByZXR1cm4gdG9UZWNoVGltZUZvcm1hdCh0aGlzLCB7XG4gICAgICAgIHN1cHByZXNzU2Vjb25kczogc3VwcHJlc3NTZWNvbmRzLFxuICAgICAgICBzdXBwcmVzc01pbGxpc2Vjb25kczogc3VwcHJlc3NNaWxsaXNlY29uZHMsXG4gICAgICAgIGluY2x1ZGVPZmZzZXQ6IGluY2x1ZGVPZmZzZXRcbiAgICAgIH0pO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIFJGQyAyODIyLWNvbXBhdGlibGUgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRoaXMgRGF0ZVRpbWUsIGFsd2F5cyBpbiBVVENcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS51dGMoMjAxNCwgNywgMTMpLnRvUkZDMjgyMigpIC8vPT4gJ1N1biwgMTMgSnVsIDIwMTQgMDA6MDA6MDAgKzAwMDAnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNCwgNywgMTMpLnRvUkZDMjgyMigpIC8vPT4gJ1N1biwgMTMgSnVsIDIwMTQgMDA6MDA6MDAgLTA0MDAnXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b1JGQzI4MjIgPSBmdW5jdGlvbiB0b1JGQzI4MjIoKSB7XG4gICAgICByZXR1cm4gdG9UZWNoRm9ybWF0KHRoaXMsIFwiRUVFLCBkZCBMTEwgeXl5eSBISDptbTpzcyBaWlpcIik7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYSBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBEYXRlVGltZSBhcHByb3ByaWF0ZSBmb3IgdXNlIGluIEhUVFAgaGVhZGVycy5cbiAgICAgKiBTcGVjaWZpY2FsbHksIHRoZSBzdHJpbmcgY29uZm9ybXMgdG8gUkZDIDExMjMuXG4gICAgICogQHNlZSBodHRwczovL3d3dy53My5vcmcvUHJvdG9jb2xzL3JmYzI2MTYvcmZjMjYxNi1zZWMzLmh0bWwjc2VjMy4zLjFcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS51dGMoMjAxNCwgNywgMTMpLnRvSFRUUCgpIC8vPT4gJ1N1biwgMTMgSnVsIDIwMTQgMDA6MDA6MDAgR01UJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygyMDE0LCA3LCAxMywgMTkpLnRvSFRUUCgpIC8vPT4gJ1N1biwgMTMgSnVsIDIwMTQgMTk6MDA6MDAgR01UJ1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9IVFRQID0gZnVuY3Rpb24gdG9IVFRQKCkge1xuICAgICAgcmV0dXJuIHRvVGVjaEZvcm1hdCh0aGlzLnRvVVRDKCksIFwiRUVFLCBkZCBMTEwgeXl5eSBISDptbTpzcyAnR01UJ1wiKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIERhdGVUaW1lIGFwcHJvcHJpYXRlIGZvciB1c2UgaW4gU1FMIERhdGVcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS51dGMoMjAxNCwgNywgMTMpLnRvU1FMRGF0ZSgpIC8vPT4gJzIwMTQtMDctMTMnXG4gICAgICogQHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b1NRTERhdGUgPSBmdW5jdGlvbiB0b1NRTERhdGUoKSB7XG4gICAgICByZXR1cm4gdG9UZWNoRm9ybWF0KHRoaXMsIFwieXl5eS1NTS1kZFwiKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIERhdGVUaW1lIGFwcHJvcHJpYXRlIGZvciB1c2UgaW4gU1FMIFRpbWVcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnNcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRzLmluY2x1ZGVab25lPWZhbHNlXSAtIGluY2x1ZGUgdGhlIHpvbmUsIHN1Y2ggYXMgJ0FtZXJpY2EvTmV3X1lvcmsnLiBPdmVycmlkZXMgaW5jbHVkZU9mZnNldC5cbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRzLmluY2x1ZGVPZmZzZXQ9dHJ1ZV0gLSBpbmNsdWRlIHRoZSBvZmZzZXQsIHN1Y2ggYXMgJ1onIG9yICctMDQ6MDAnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUudXRjKCkudG9TUUwoKSAvLz0+ICcwNToxNToxNi4zNDUnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b1NRTCgpIC8vPT4gJzA1OjE1OjE2LjM0NSAtMDQ6MDAnXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS50b1NRTCh7IGluY2x1ZGVPZmZzZXQ6IGZhbHNlIH0pIC8vPT4gJzA1OjE1OjE2LjM0NSdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnRvU1FMKHsgaW5jbHVkZVpvbmU6IGZhbHNlIH0pIC8vPT4gJzA1OjE1OjE2LjM0NSBBbWVyaWNhL05ld19Zb3JrJ1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9TUUxUaW1lID0gZnVuY3Rpb24gdG9TUUxUaW1lKF90ZW1wNCkge1xuICAgICAgdmFyIF9yZWY4ID0gX3RlbXA0ID09PSB2b2lkIDAgPyB7fSA6IF90ZW1wNCxcbiAgICAgICAgICBfcmVmOCRpbmNsdWRlT2Zmc2V0ID0gX3JlZjguaW5jbHVkZU9mZnNldCxcbiAgICAgICAgICBpbmNsdWRlT2Zmc2V0ID0gX3JlZjgkaW5jbHVkZU9mZnNldCA9PT0gdm9pZCAwID8gdHJ1ZSA6IF9yZWY4JGluY2x1ZGVPZmZzZXQsXG4gICAgICAgICAgX3JlZjgkaW5jbHVkZVpvbmUgPSBfcmVmOC5pbmNsdWRlWm9uZSxcbiAgICAgICAgICBpbmNsdWRlWm9uZSA9IF9yZWY4JGluY2x1ZGVab25lID09PSB2b2lkIDAgPyBmYWxzZSA6IF9yZWY4JGluY2x1ZGVab25lO1xuXG4gICAgICByZXR1cm4gdG9UZWNoVGltZUZvcm1hdCh0aGlzLCB7XG4gICAgICAgIGluY2x1ZGVPZmZzZXQ6IGluY2x1ZGVPZmZzZXQsXG4gICAgICAgIGluY2x1ZGVab25lOiBpbmNsdWRlWm9uZSxcbiAgICAgICAgc3BhY2Vab25lOiB0cnVlXG4gICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIERhdGVUaW1lIGFwcHJvcHJpYXRlIGZvciB1c2UgaW4gU1FMIERhdGVUaW1lXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdHMgLSBvcHRpb25zXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbb3B0cy5pbmNsdWRlWm9uZT1mYWxzZV0gLSBpbmNsdWRlIHRoZSB6b25lLCBzdWNoIGFzICdBbWVyaWNhL05ld19Zb3JrJy4gT3ZlcnJpZGVzIGluY2x1ZGVPZmZzZXQuXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbb3B0cy5pbmNsdWRlT2Zmc2V0PXRydWVdIC0gaW5jbHVkZSB0aGUgb2Zmc2V0LCBzdWNoIGFzICdaJyBvciAnLTA0OjAwJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLnV0YygyMDE0LCA3LCAxMykudG9TUUwoKSAvLz0+ICcyMDE0LTA3LTEzIDAwOjAwOjAwLjAwMCBaJ1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTQsIDcsIDEzKS50b1NRTCgpIC8vPT4gJzIwMTQtMDctMTMgMDA6MDA6MDAuMDAwIC0wNDowMCdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE0LCA3LCAxMykudG9TUUwoeyBpbmNsdWRlT2Zmc2V0OiBmYWxzZSB9KSAvLz0+ICcyMDE0LTA3LTEzIDAwOjAwOjAwLjAwMCdcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE0LCA3LCAxMykudG9TUUwoeyBpbmNsdWRlWm9uZTogdHJ1ZSB9KSAvLz0+ICcyMDE0LTA3LTEzIDAwOjAwOjAwLjAwMCBBbWVyaWNhL05ld19Zb3JrJ1xuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9TUUwgPSBmdW5jdGlvbiB0b1NRTChvcHRzKSB7XG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLnRvU1FMRGF0ZSgpICsgXCIgXCIgKyB0aGlzLnRvU1FMVGltZShvcHRzKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGlzIERhdGVUaW1lIGFwcHJvcHJpYXRlIGZvciBkZWJ1Z2dpbmdcbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy50b0lTTygpIDogSU5WQUxJRCQyO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBlcG9jaCBtaWxsaXNlY29uZHMgb2YgdGhpcyBEYXRlVGltZS4gQWxpYXMgb2Yge0BsaW5rIHRvTWlsbGlzfVxuICAgICAqIEByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udmFsdWVPZiA9IGZ1bmN0aW9uIHZhbHVlT2YoKSB7XG4gICAgICByZXR1cm4gdGhpcy50b01pbGxpcygpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBlcG9jaCBtaWxsaXNlY29uZHMgb2YgdGhpcyBEYXRlVGltZS5cbiAgICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAgICovXG4gICAgO1xuXG4gICAgX3Byb3RvLnRvTWlsbGlzID0gZnVuY3Rpb24gdG9NaWxsaXMoKSB7XG4gICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy50cyA6IE5hTjtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgZXBvY2ggc2Vjb25kcyBvZiB0aGlzIERhdGVUaW1lLlxuICAgICAqIEByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9TZWNvbmRzID0gZnVuY3Rpb24gdG9TZWNvbmRzKCkge1xuICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMudHMgLyAxMDAwIDogTmFOO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIElTTyA4NjAxIHJlcHJlc2VudGF0aW9uIG9mIHRoaXMgRGF0ZVRpbWUgYXBwcm9wcmlhdGUgZm9yIHVzZSBpbiBKU09OLlxuICAgICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8udG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgcmV0dXJuIHRoaXMudG9JU08oKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIEJTT04gc2VyaWFsaXphYmxlIGVxdWl2YWxlbnQgdG8gdGhpcyBEYXRlVGltZS5cbiAgICAgKiBAcmV0dXJuIHtEYXRlfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b0JTT04gPSBmdW5jdGlvbiB0b0JTT04oKSB7XG4gICAgICByZXR1cm4gdGhpcy50b0pTRGF0ZSgpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgSmF2YXNjcmlwdCBvYmplY3Qgd2l0aCB0aGlzIERhdGVUaW1lJ3MgeWVhciwgbW9udGgsIGRheSwgYW5kIHNvIG9uLlxuICAgICAqIEBwYXJhbSBvcHRzIC0gb3B0aW9ucyBmb3IgZ2VuZXJhdGluZyB0aGUgb2JqZWN0XG4gICAgICogQHBhcmFtIHtib29sZWFufSBbb3B0cy5pbmNsdWRlQ29uZmlnPWZhbHNlXSAtIGluY2x1ZGUgY29uZmlndXJhdGlvbiBhdHRyaWJ1dGVzIGluIHRoZSBvdXRwdXRcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLnRvT2JqZWN0KCkgLy89PiB7IHllYXI6IDIwMTcsIG1vbnRoOiA0LCBkYXk6IDIyLCBob3VyOiAyMCwgbWludXRlOiA0OSwgc2Vjb25kOiA0MiwgbWlsbGlzZWNvbmQ6IDI2OCB9XG4gICAgICogQHJldHVybiB7T2JqZWN0fVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG9wdHMpIHtcbiAgICAgIGlmIChvcHRzID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0cyA9IHt9O1xuICAgICAgfVxuXG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIHt9O1xuICAgICAgdmFyIGJhc2UgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmMpO1xuXG4gICAgICBpZiAob3B0cy5pbmNsdWRlQ29uZmlnKSB7XG4gICAgICAgIGJhc2Uub3V0cHV0Q2FsZW5kYXIgPSB0aGlzLm91dHB1dENhbGVuZGFyO1xuICAgICAgICBiYXNlLm51bWJlcmluZ1N5c3RlbSA9IHRoaXMubG9jLm51bWJlcmluZ1N5c3RlbTtcbiAgICAgICAgYmFzZS5sb2NhbGUgPSB0aGlzLmxvYy5sb2NhbGU7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBiYXNlO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgSmF2YXNjcmlwdCBEYXRlIGVxdWl2YWxlbnQgdG8gdGhpcyBEYXRlVGltZS5cbiAgICAgKiBAcmV0dXJuIHtEYXRlfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b0pTRGF0ZSA9IGZ1bmN0aW9uIHRvSlNEYXRlKCkge1xuICAgICAgcmV0dXJuIG5ldyBEYXRlKHRoaXMuaXNWYWxpZCA/IHRoaXMudHMgOiBOYU4pO1xuICAgIH0gLy8gQ09NUEFSRVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gdHdvIERhdGVUaW1lcyBhcyBhIER1cmF0aW9uLlxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IG90aGVyRGF0ZVRpbWUgLSB0aGUgRGF0ZVRpbWUgdG8gY29tcGFyZSB0aGlzIG9uZSB0b1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfHN0cmluZ1tdfSBbdW5pdD1bJ21pbGxpc2Vjb25kcyddXSAtIHRoZSB1bml0IG9yIGFycmF5IG9mIHVuaXRzIChzdWNoIGFzICdob3Vycycgb3IgJ2RheXMnKSB0byBpbmNsdWRlIGluIHRoZSBkdXJhdGlvbi5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnMgdGhhdCBhZmZlY3QgdGhlIGNyZWF0aW9uIG9mIHRoZSBEdXJhdGlvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5jb252ZXJzaW9uQWNjdXJhY3k9J2Nhc3VhbCddIC0gdGhlIGNvbnZlcnNpb24gc3lzdGVtIHRvIHVzZVxuICAgICAqIEBleGFtcGxlXG4gICAgICogdmFyIGkxID0gRGF0ZVRpbWUuZnJvbUlTTygnMTk4Mi0wNS0yNVQwOTo0NScpLFxuICAgICAqICAgICBpMiA9IERhdGVUaW1lLmZyb21JU08oJzE5ODMtMTAtMTRUMTA6MzAnKTtcbiAgICAgKiBpMi5kaWZmKGkxKS50b09iamVjdCgpIC8vPT4geyBtaWxsaXNlY29uZHM6IDQzODA3NTAwMDAwIH1cbiAgICAgKiBpMi5kaWZmKGkxLCAnaG91cnMnKS50b09iamVjdCgpIC8vPT4geyBob3VyczogMTIxNjguNzUgfVxuICAgICAqIGkyLmRpZmYoaTEsIFsnbW9udGhzJywgJ2RheXMnXSkudG9PYmplY3QoKSAvLz0+IHsgbW9udGhzOiAxNiwgZGF5czogMTkuMDMxMjUgfVxuICAgICAqIGkyLmRpZmYoaTEsIFsnbW9udGhzJywgJ2RheXMnLCAnaG91cnMnXSkudG9PYmplY3QoKSAvLz0+IHsgbW9udGhzOiAxNiwgZGF5czogMTksIGhvdXJzOiAwLjc1IH1cbiAgICAgKiBAcmV0dXJuIHtEdXJhdGlvbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZGlmZiA9IGZ1bmN0aW9uIGRpZmYob3RoZXJEYXRlVGltZSwgdW5pdCwgb3B0cykge1xuICAgICAgaWYgKHVuaXQgPT09IHZvaWQgMCkge1xuICAgICAgICB1bml0ID0gXCJtaWxsaXNlY29uZHNcIjtcbiAgICAgIH1cblxuICAgICAgaWYgKG9wdHMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRzID0ge307XG4gICAgICB9XG5cbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkIHx8ICFvdGhlckRhdGVUaW1lLmlzVmFsaWQpIHtcbiAgICAgICAgcmV0dXJuIER1cmF0aW9uLmludmFsaWQodGhpcy5pbnZhbGlkIHx8IG90aGVyRGF0ZVRpbWUuaW52YWxpZCwgXCJjcmVhdGVkIGJ5IGRpZmZpbmcgYW4gaW52YWxpZCBEYXRlVGltZVwiKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGR1ck9wdHMgPSBPYmplY3QuYXNzaWduKHtcbiAgICAgICAgbG9jYWxlOiB0aGlzLmxvY2FsZSxcbiAgICAgICAgbnVtYmVyaW5nU3lzdGVtOiB0aGlzLm51bWJlcmluZ1N5c3RlbVxuICAgICAgfSwgb3B0cyk7XG5cbiAgICAgIHZhciB1bml0cyA9IG1heWJlQXJyYXkodW5pdCkubWFwKER1cmF0aW9uLm5vcm1hbGl6ZVVuaXQpLFxuICAgICAgICAgIG90aGVySXNMYXRlciA9IG90aGVyRGF0ZVRpbWUudmFsdWVPZigpID4gdGhpcy52YWx1ZU9mKCksXG4gICAgICAgICAgZWFybGllciA9IG90aGVySXNMYXRlciA/IHRoaXMgOiBvdGhlckRhdGVUaW1lLFxuICAgICAgICAgIGxhdGVyID0gb3RoZXJJc0xhdGVyID8gb3RoZXJEYXRlVGltZSA6IHRoaXMsXG4gICAgICAgICAgZGlmZmVkID0gX2RpZmYoZWFybGllciwgbGF0ZXIsIHVuaXRzLCBkdXJPcHRzKTtcblxuICAgICAgcmV0dXJuIG90aGVySXNMYXRlciA/IGRpZmZlZC5uZWdhdGUoKSA6IGRpZmZlZDtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSBkaWZmZXJlbmNlIGJldHdlZW4gdGhpcyBEYXRlVGltZSBhbmQgcmlnaHQgbm93LlxuICAgICAqIFNlZSB7QGxpbmsgZGlmZn1cbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xzdHJpbmdbXX0gW3VuaXQ9WydtaWxsaXNlY29uZHMnXV0gLSB0aGUgdW5pdCBvciB1bml0cyB1bml0cyAoc3VjaCBhcyAnaG91cnMnIG9yICdkYXlzJykgdG8gaW5jbHVkZSBpbiB0aGUgZHVyYXRpb25cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0cyAtIG9wdGlvbnMgdGhhdCBhZmZlY3QgdGhlIGNyZWF0aW9uIG9mIHRoZSBEdXJhdGlvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5jb252ZXJzaW9uQWNjdXJhY3k9J2Nhc3VhbCddIC0gdGhlIGNvbnZlcnNpb24gc3lzdGVtIHRvIHVzZVxuICAgICAqIEByZXR1cm4ge0R1cmF0aW9ufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5kaWZmTm93ID0gZnVuY3Rpb24gZGlmZk5vdyh1bml0LCBvcHRzKSB7XG4gICAgICBpZiAodW5pdCA9PT0gdm9pZCAwKSB7XG4gICAgICAgIHVuaXQgPSBcIm1pbGxpc2Vjb25kc1wiO1xuICAgICAgfVxuXG4gICAgICBpZiAob3B0cyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdHMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuZGlmZihEYXRlVGltZS5sb2NhbCgpLCB1bml0LCBvcHRzKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIGFuIEludGVydmFsIHNwYW5uaW5nIGJldHdlZW4gdGhpcyBEYXRlVGltZSBhbmQgYW5vdGhlciBEYXRlVGltZVxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IG90aGVyRGF0ZVRpbWUgLSB0aGUgb3RoZXIgZW5kIHBvaW50IG9mIHRoZSBJbnRlcnZhbFxuICAgICAqIEByZXR1cm4ge0ludGVydmFsfVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by51bnRpbCA9IGZ1bmN0aW9uIHVudGlsKG90aGVyRGF0ZVRpbWUpIHtcbiAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyBJbnRlcnZhbC5mcm9tRGF0ZVRpbWVzKHRoaXMsIG90aGVyRGF0ZVRpbWUpIDogdGhpcztcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmV0dXJuIHdoZXRoZXIgdGhpcyBEYXRlVGltZSBpcyBpbiB0aGUgc2FtZSB1bml0IG9mIHRpbWUgYXMgYW5vdGhlciBEYXRlVGltZVxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IG90aGVyRGF0ZVRpbWUgLSB0aGUgb3RoZXIgRGF0ZVRpbWVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdW5pdCAtIHRoZSB1bml0IG9mIHRpbWUgdG8gY2hlY2sgc2FtZW5lc3Mgb25cbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLmhhc1NhbWUob3RoZXJEVCwgJ2RheScpOyAvL34+IHRydWUgaWYgYm90aCB0aGUgc2FtZSBjYWxlbmRhciBkYXlcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by5oYXNTYW1lID0gZnVuY3Rpb24gaGFzU2FtZShvdGhlckRhdGVUaW1lLCB1bml0KSB7XG4gICAgICBpZiAoIXRoaXMuaXNWYWxpZCkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICBpZiAodW5pdCA9PT0gXCJtaWxsaXNlY29uZFwiKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnZhbHVlT2YoKSA9PT0gb3RoZXJEYXRlVGltZS52YWx1ZU9mKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgaW5wdXRNcyA9IG90aGVyRGF0ZVRpbWUudmFsdWVPZigpO1xuICAgICAgICByZXR1cm4gdGhpcy5zdGFydE9mKHVuaXQpIDw9IGlucHV0TXMgJiYgaW5wdXRNcyA8PSB0aGlzLmVuZE9mKHVuaXQpO1xuICAgICAgfVxuICAgIH1cbiAgICAvKipcbiAgICAgKiBFcXVhbGl0eSBjaGVja1xuICAgICAqIFR3byBEYXRlVGltZXMgYXJlIGVxdWFsIGlmZiB0aGV5IHJlcHJlc2VudCB0aGUgc2FtZSBtaWxsaXNlY29uZCwgaGF2ZSB0aGUgc2FtZSB6b25lIGFuZCBsb2NhdGlvbiwgYW5kIGFyZSBib3RoIHZhbGlkLlxuICAgICAqIFRvIGNvbXBhcmUganVzdCB0aGUgbWlsbGlzZWNvbmQgdmFsdWVzLCB1c2UgYCtkdDEgPT09ICtkdDJgLlxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IG90aGVyIC0gdGhlIG90aGVyIERhdGVUaW1lXG4gICAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfcHJvdG8uZXF1YWxzID0gZnVuY3Rpb24gZXF1YWxzKG90aGVyKSB7XG4gICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkICYmIG90aGVyLmlzVmFsaWQgJiYgdGhpcy52YWx1ZU9mKCkgPT09IG90aGVyLnZhbHVlT2YoKSAmJiB0aGlzLnpvbmUuZXF1YWxzKG90aGVyLnpvbmUpICYmIHRoaXMubG9jLmVxdWFscyhvdGhlci5sb2MpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIGEgdGhpcyB0aW1lIHJlbGF0aXZlIHRvIG5vdywgc3VjaCBhcyBcImluIHR3byBkYXlzXCIuIENhbiBvbmx5IGludGVybmF0aW9uYWxpemUgaWYgeW91clxuICAgICAqIHBsYXRmb3JtIHN1cHBvcnRzIEludGwuUmVsYXRpdmVUaW1lRm9ybWF0LiBSb3VuZHMgZG93biBieSBkZWZhdWx0LlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zIC0gb3B0aW9ucyB0aGF0IGFmZmVjdCB0aGUgb3V0cHV0XG4gICAgICogQHBhcmFtIHtEYXRlVGltZX0gW29wdGlvbnMuYmFzZT1EYXRlVGltZS5sb2NhbCgpXSAtIHRoZSBEYXRlVGltZSB0byB1c2UgYXMgdGhlIGJhc2lzIHRvIHdoaWNoIHRoaXMgdGltZSBpcyBjb21wYXJlZC4gRGVmYXVsdHMgdG8gbm93LlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0aW9ucy5zdHlsZT1cImxvbmdcIl0gLSB0aGUgc3R5bGUgb2YgdW5pdHMsIG11c3QgYmUgXCJsb25nXCIsIFwic2hvcnRcIiwgb3IgXCJuYXJyb3dcIlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBvcHRpb25zLnVuaXQgLSB1c2UgYSBzcGVjaWZpYyB1bml0OyBpZiBvbWl0dGVkLCB0aGUgbWV0aG9kIHdpbGwgcGljayB0aGUgdW5pdC4gVXNlIG9uZSBvZiBcInllYXJzXCIsIFwicXVhcnRlcnNcIiwgXCJtb250aHNcIiwgXCJ3ZWVrc1wiLCBcImRheXNcIiwgXCJob3Vyc1wiLCBcIm1pbnV0ZXNcIiwgb3IgXCJzZWNvbmRzXCJcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRpb25zLnJvdW5kPXRydWVdIC0gd2hldGhlciB0byByb3VuZCB0aGUgbnVtYmVycyBpbiB0aGUgb3V0cHV0LlxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdGlvbnMucGFkZGluZz0wXSAtIHBhZGRpbmcgaW4gbWlsbGlzZWNvbmRzLiBUaGlzIGFsbG93cyB5b3UgdG8gcm91bmQgdXAgdGhlIHJlc3VsdCBpZiBpdCBmaXRzIGluc2lkZSB0aGUgdGhyZXNob2xkLiBEb24ndCB1c2UgaW4gY29tYmluYXRpb24gd2l0aCB7cm91bmQ6IGZhbHNlfSBiZWNhdXNlIHRoZSBkZWNpbWFsIG91dHB1dCB3aWxsIGluY2x1ZGUgdGhlIHBhZGRpbmcuXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdGlvbnMubG9jYWxlIC0gb3ZlcnJpZGUgdGhlIGxvY2FsZSBvZiB0aGlzIERhdGVUaW1lXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG9wdGlvbnMubnVtYmVyaW5nU3lzdGVtIC0gb3ZlcnJpZGUgdGhlIG51bWJlcmluZ1N5c3RlbSBvZiB0aGlzIERhdGVUaW1lLiBUaGUgSW50bCBzeXN0ZW0gbWF5IGNob29zZSBub3QgdG8gaG9ub3IgdGhpc1xuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkucGx1cyh7IGRheXM6IDEgfSkudG9SZWxhdGl2ZSgpIC8vPT4gXCJpbiAxIGRheVwiXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5zZXRMb2NhbGUoXCJlc1wiKS50b1JlbGF0aXZlKHsgZGF5czogMSB9KSAvLz0+IFwiZGVudHJvIGRlIDEgZMOtYVwiXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5wbHVzKHsgZGF5czogMSB9KS50b1JlbGF0aXZlKHsgbG9jYWxlOiBcImZyXCIgfSkgLy89PiBcImRhbnMgMjMgaGV1cmVzXCJcbiAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgpLm1pbnVzKHsgZGF5czogMiB9KS50b1JlbGF0aXZlKCkgLy89PiBcIjIgZGF5cyBhZ29cIlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkubWludXMoeyBkYXlzOiAyIH0pLnRvUmVsYXRpdmUoeyB1bml0OiBcImhvdXJzXCIgfSkgLy89PiBcIjQ4IGhvdXJzIGFnb1wiXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5taW51cyh7IGhvdXJzOiAzNiB9KS50b1JlbGF0aXZlKHsgcm91bmQ6IGZhbHNlIH0pIC8vPT4gXCIxLjUgZGF5cyBhZ29cIlxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b1JlbGF0aXZlID0gZnVuY3Rpb24gdG9SZWxhdGl2ZShvcHRpb25zKSB7XG4gICAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmlzVmFsaWQpIHJldHVybiBudWxsO1xuICAgICAgdmFyIGJhc2UgPSBvcHRpb25zLmJhc2UgfHwgRGF0ZVRpbWUuZnJvbU9iamVjdCh7XG4gICAgICAgIHpvbmU6IHRoaXMuem9uZVxuICAgICAgfSksXG4gICAgICAgICAgcGFkZGluZyA9IG9wdGlvbnMucGFkZGluZyA/IHRoaXMgPCBiYXNlID8gLW9wdGlvbnMucGFkZGluZyA6IG9wdGlvbnMucGFkZGluZyA6IDA7XG4gICAgICByZXR1cm4gZGlmZlJlbGF0aXZlKGJhc2UsIHRoaXMucGx1cyhwYWRkaW5nKSwgT2JqZWN0LmFzc2lnbihvcHRpb25zLCB7XG4gICAgICAgIG51bWVyaWM6IFwiYWx3YXlzXCIsXG4gICAgICAgIHVuaXRzOiBbXCJ5ZWFyc1wiLCBcIm1vbnRoc1wiLCBcImRheXNcIiwgXCJob3Vyc1wiLCBcIm1pbnV0ZXNcIiwgXCJzZWNvbmRzXCJdXG4gICAgICB9KSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYSBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhpcyBkYXRlIHJlbGF0aXZlIHRvIHRvZGF5LCBzdWNoIGFzIFwieWVzdGVyZGF5XCIgb3IgXCJuZXh0IG1vbnRoXCIuXG4gICAgICogT25seSBpbnRlcm5hdGlvbmFsaXplcyBvbiBwbGF0Zm9ybXMgdGhhdCBzdXBwb3J0cyBJbnRsLlJlbGF0aXZlVGltZUZvcm1hdC5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyAtIG9wdGlvbnMgdGhhdCBhZmZlY3QgdGhlIG91dHB1dFxuICAgICAqIEBwYXJhbSB7RGF0ZVRpbWV9IFtvcHRpb25zLmJhc2U9RGF0ZVRpbWUubG9jYWwoKV0gLSB0aGUgRGF0ZVRpbWUgdG8gdXNlIGFzIHRoZSBiYXNpcyB0byB3aGljaCB0aGlzIHRpbWUgaXMgY29tcGFyZWQuIERlZmF1bHRzIHRvIG5vdy5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0aW9ucy5sb2NhbGUgLSBvdmVycmlkZSB0aGUgbG9jYWxlIG9mIHRoaXMgRGF0ZVRpbWVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0aW9ucy51bml0IC0gdXNlIGEgc3BlY2lmaWMgdW5pdDsgaWYgb21pdHRlZCwgdGhlIG1ldGhvZCB3aWxsIHBpY2sgdGhlIHVuaXQuIFVzZSBvbmUgb2YgXCJ5ZWFyc1wiLCBcInF1YXJ0ZXJzXCIsIFwibW9udGhzXCIsIFwid2Vla3NcIiwgb3IgXCJkYXlzXCJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gb3B0aW9ucy5udW1iZXJpbmdTeXN0ZW0gLSBvdmVycmlkZSB0aGUgbnVtYmVyaW5nU3lzdGVtIG9mIHRoaXMgRGF0ZVRpbWUuIFRoZSBJbnRsIHN5c3RlbSBtYXkgY2hvb3NlIG5vdCB0byBob25vciB0aGlzXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5wbHVzKHsgZGF5czogMSB9KS50b1JlbGF0aXZlQ2FsZW5kYXIoKSAvLz0+IFwidG9tb3Jyb3dcIlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkuc2V0TG9jYWxlKFwiZXNcIikucGx1cyh7IGRheXM6IDEgfSkudG9SZWxhdGl2ZSgpIC8vPT4gXCJcIm1hw7FhbmFcIlxuICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkucGx1cyh7IGRheXM6IDEgfSkudG9SZWxhdGl2ZUNhbGVuZGFyKHsgbG9jYWxlOiBcImZyXCIgfSkgLy89PiBcImRlbWFpblwiXG4gICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoKS5taW51cyh7IGRheXM6IDIgfSkudG9SZWxhdGl2ZUNhbGVuZGFyKCkgLy89PiBcIjIgZGF5cyBhZ29cIlxuICAgICAqL1xuICAgIDtcblxuICAgIF9wcm90by50b1JlbGF0aXZlQ2FsZW5kYXIgPSBmdW5jdGlvbiB0b1JlbGF0aXZlQ2FsZW5kYXIob3B0aW9ucykge1xuICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkge1xuICAgICAgICBvcHRpb25zID0ge307XG4gICAgICB9XG5cbiAgICAgIGlmICghdGhpcy5pc1ZhbGlkKSByZXR1cm4gbnVsbDtcbiAgICAgIHJldHVybiBkaWZmUmVsYXRpdmUob3B0aW9ucy5iYXNlIHx8IERhdGVUaW1lLmZyb21PYmplY3Qoe1xuICAgICAgICB6b25lOiB0aGlzLnpvbmVcbiAgICAgIH0pLCB0aGlzLCBPYmplY3QuYXNzaWduKG9wdGlvbnMsIHtcbiAgICAgICAgbnVtZXJpYzogXCJhdXRvXCIsXG4gICAgICAgIHVuaXRzOiBbXCJ5ZWFyc1wiLCBcIm1vbnRoc1wiLCBcImRheXNcIl0sXG4gICAgICAgIGNhbGVuZGFyeTogdHJ1ZVxuICAgICAgfSkpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIG1pbiBvZiBzZXZlcmFsIGRhdGUgdGltZXNcbiAgICAgKiBAcGFyYW0gey4uLkRhdGVUaW1lfSBkYXRlVGltZXMgLSB0aGUgRGF0ZVRpbWVzIGZyb20gd2hpY2ggdG8gY2hvb3NlIHRoZSBtaW5pbXVtXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9IHRoZSBtaW4gRGF0ZVRpbWUsIG9yIHVuZGVmaW5lZCBpZiBjYWxsZWQgd2l0aCBubyBhcmd1bWVudFxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLm1pbiA9IGZ1bmN0aW9uIG1pbigpIHtcbiAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBkYXRlVGltZXMgPSBuZXcgQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICAgIGRhdGVUaW1lc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgIH1cblxuICAgICAgaWYgKCFkYXRlVGltZXMuZXZlcnkoRGF0ZVRpbWUuaXNEYXRlVGltZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IEludmFsaWRBcmd1bWVudEVycm9yKFwibWluIHJlcXVpcmVzIGFsbCBhcmd1bWVudHMgYmUgRGF0ZVRpbWVzXCIpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gYmVzdEJ5KGRhdGVUaW1lcywgZnVuY3Rpb24gKGkpIHtcbiAgICAgICAgcmV0dXJuIGkudmFsdWVPZigpO1xuICAgICAgfSwgTWF0aC5taW4pO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIG1heCBvZiBzZXZlcmFsIGRhdGUgdGltZXNcbiAgICAgKiBAcGFyYW0gey4uLkRhdGVUaW1lfSBkYXRlVGltZXMgLSB0aGUgRGF0ZVRpbWVzIGZyb20gd2hpY2ggdG8gY2hvb3NlIHRoZSBtYXhpbXVtXG4gICAgICogQHJldHVybiB7RGF0ZVRpbWV9IHRoZSBtYXggRGF0ZVRpbWUsIG9yIHVuZGVmaW5lZCBpZiBjYWxsZWQgd2l0aCBubyBhcmd1bWVudFxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLm1heCA9IGZ1bmN0aW9uIG1heCgpIHtcbiAgICAgIGZvciAodmFyIF9sZW4yID0gYXJndW1lbnRzLmxlbmd0aCwgZGF0ZVRpbWVzID0gbmV3IEFycmF5KF9sZW4yKSwgX2tleTIgPSAwOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICAgIGRhdGVUaW1lc1tfa2V5Ml0gPSBhcmd1bWVudHNbX2tleTJdO1xuICAgICAgfVxuXG4gICAgICBpZiAoIWRhdGVUaW1lcy5ldmVyeShEYXRlVGltZS5pc0RhdGVUaW1lKSkge1xuICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEFyZ3VtZW50RXJyb3IoXCJtYXggcmVxdWlyZXMgYWxsIGFyZ3VtZW50cyBiZSBEYXRlVGltZXNcIik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBiZXN0QnkoZGF0ZVRpbWVzLCBmdW5jdGlvbiAoaSkge1xuICAgICAgICByZXR1cm4gaS52YWx1ZU9mKCk7XG4gICAgICB9LCBNYXRoLm1heCk7XG4gICAgfSAvLyBNSVNDXG5cbiAgICAvKipcbiAgICAgKiBFeHBsYWluIGhvdyBhIHN0cmluZyB3b3VsZCBiZSBwYXJzZWQgYnkgZnJvbUZvcm1hdCgpXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRleHQgLSB0aGUgc3RyaW5nIHRvIHBhcnNlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGZtdCAtIHRoZSBmb3JtYXQgdGhlIHN0cmluZyBpcyBleHBlY3RlZCB0byBiZSBpbiAoc2VlIGRlc2NyaXB0aW9uKVxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zIC0gb3B0aW9ucyB0YWtlbiBieSBmcm9tRm9ybWF0KClcbiAgICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAgICovXG4gICAgO1xuXG4gICAgRGF0ZVRpbWUuZnJvbUZvcm1hdEV4cGxhaW4gPSBmdW5jdGlvbiBmcm9tRm9ybWF0RXhwbGFpbih0ZXh0LCBmbXQsIG9wdGlvbnMpIHtcbiAgICAgIGlmIChvcHRpb25zID09PSB2b2lkIDApIHtcbiAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgfVxuXG4gICAgICB2YXIgX29wdGlvbnMgPSBvcHRpb25zLFxuICAgICAgICAgIF9vcHRpb25zJGxvY2FsZSA9IF9vcHRpb25zLmxvY2FsZSxcbiAgICAgICAgICBsb2NhbGUgPSBfb3B0aW9ucyRsb2NhbGUgPT09IHZvaWQgMCA/IG51bGwgOiBfb3B0aW9ucyRsb2NhbGUsXG4gICAgICAgICAgX29wdGlvbnMkbnVtYmVyaW5nU3lzID0gX29wdGlvbnMubnVtYmVyaW5nU3lzdGVtLFxuICAgICAgICAgIG51bWJlcmluZ1N5c3RlbSA9IF9vcHRpb25zJG51bWJlcmluZ1N5cyA9PT0gdm9pZCAwID8gbnVsbCA6IF9vcHRpb25zJG51bWJlcmluZ1N5cyxcbiAgICAgICAgICBsb2NhbGVUb1VzZSA9IExvY2FsZS5mcm9tT3B0cyh7XG4gICAgICAgIGxvY2FsZTogbG9jYWxlLFxuICAgICAgICBudW1iZXJpbmdTeXN0ZW06IG51bWJlcmluZ1N5c3RlbSxcbiAgICAgICAgZGVmYXVsdFRvRU46IHRydWVcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIGV4cGxhaW5Gcm9tVG9rZW5zKGxvY2FsZVRvVXNlLCB0ZXh0LCBmbXQpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBAZGVwcmVjYXRlZCB1c2UgZnJvbUZvcm1hdEV4cGxhaW4gaW5zdGVhZFxuICAgICAqL1xuICAgIDtcblxuICAgIERhdGVUaW1lLmZyb21TdHJpbmdFeHBsYWluID0gZnVuY3Rpb24gZnJvbVN0cmluZ0V4cGxhaW4odGV4dCwgZm10LCBvcHRpb25zKSB7XG4gICAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7XG4gICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIERhdGVUaW1lLmZyb21Gb3JtYXRFeHBsYWluKHRleHQsIGZtdCwgb3B0aW9ucyk7XG4gICAgfSAvLyBGT1JNQVQgUFJFU0VUU1xuXG4gICAgLyoqXG4gICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAxMC8xNC8xOTgzXG4gICAgICogQHR5cGUge09iamVjdH1cbiAgICAgKi9cbiAgICA7XG5cbiAgICBfY3JlYXRlQ2xhc3MoRGF0ZVRpbWUsIFt7XG4gICAgICBrZXk6IFwiaXNWYWxpZFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmludmFsaWQgPT09IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIFJldHVybnMgYW4gZXJyb3IgY29kZSBpZiB0aGlzIERhdGVUaW1lIGlzIGludmFsaWQsIG9yIG51bGwgaWYgdGhlIERhdGVUaW1lIGlzIHZhbGlkXG4gICAgICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaW52YWxpZFJlYXNvblwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmludmFsaWQgPyB0aGlzLmludmFsaWQucmVhc29uIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyBhbiBleHBsYW5hdGlvbiBvZiB3aHkgdGhpcyBEYXRlVGltZSBiZWNhbWUgaW52YWxpZCwgb3IgbnVsbCBpZiB0aGUgRGF0ZVRpbWUgaXMgdmFsaWRcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJpbnZhbGlkRXhwbGFuYXRpb25cIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnZhbGlkID8gdGhpcy5pbnZhbGlkLmV4cGxhbmF0aW9uIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBsb2NhbGUgb2YgYSBEYXRlVGltZSwgc3VjaCAnZW4tR0InLiBUaGUgbG9jYWxlIGlzIHVzZWQgd2hlbiBmb3JtYXR0aW5nIHRoZSBEYXRlVGltZVxuICAgICAgICpcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJsb2NhbGVcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy5sb2MubG9jYWxlIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBudW1iZXJpbmcgc3lzdGVtIG9mIGEgRGF0ZVRpbWUsIHN1Y2ggJ2JlbmcnLiBUaGUgbnVtYmVyaW5nIHN5c3RlbSBpcyB1c2VkIHdoZW4gZm9ybWF0dGluZyB0aGUgRGF0ZVRpbWVcbiAgICAgICAqXG4gICAgICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwibnVtYmVyaW5nU3lzdGVtXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMubG9jLm51bWJlcmluZ1N5c3RlbSA6IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgb3V0cHV0IGNhbGVuZGFyIG9mIGEgRGF0ZVRpbWUsIHN1Y2ggJ2lzbGFtaWMnLiBUaGUgb3V0cHV0IGNhbGVuZGFyIGlzIHVzZWQgd2hlbiBmb3JtYXR0aW5nIHRoZSBEYXRlVGltZVxuICAgICAgICpcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJvdXRwdXRDYWxlbmRhclwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLmxvYy5vdXRwdXRDYWxlbmRhciA6IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgdGltZSB6b25lIGFzc29jaWF0ZWQgd2l0aCB0aGlzIERhdGVUaW1lLlxuICAgICAgICogQHR5cGUge1pvbmV9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ6b25lXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3pvbmU7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgbmFtZSBvZiB0aGUgdGltZSB6b25lLlxuICAgICAgICogQHR5cGUge3N0cmluZ31cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcInpvbmVOYW1lXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMuem9uZS5uYW1lIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSB5ZWFyXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCA1LCAyNSkueWVhciAvLz0+IDIwMTdcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ5ZWFyXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMuYy55ZWFyIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIHF1YXJ0ZXJcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDUsIDI1KS5xdWFydGVyIC8vPT4gMlxuICAgICAgICogQHR5cGUge251bWJlcn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcInF1YXJ0ZXJcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gTWF0aC5jZWlsKHRoaXMuYy5tb250aCAvIDMpIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIG1vbnRoICgxLTEyKS5cbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDUsIDI1KS5tb250aCAvLz0+IDVcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJtb250aFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLmMubW9udGggOiBOYU47XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgZGF5IG9mIHRoZSBtb250aCAoMS0zMGlzaCkuXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCA1LCAyNSkuZGF5IC8vPT4gMjVcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJkYXlcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gdGhpcy5jLmRheSA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBob3VyIG9mIHRoZSBkYXkgKDAtMjMpLlxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgNSwgMjUsIDkpLmhvdXIgLy89PiA5XG4gICAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaG91clwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLmMuaG91ciA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBtaW51dGUgb2YgdGhlIGhvdXIgKDAtNTkpLlxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgNSwgMjUsIDksIDMwKS5taW51dGUgLy89PiAzMFxuICAgICAgICogQHR5cGUge251bWJlcn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm1pbnV0ZVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB0aGlzLmMubWludXRlIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIHNlY29uZCBvZiB0aGUgbWludXRlICgwLTU5KS5cbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDUsIDI1LCA5LCAzMCwgNTIpLnNlY29uZCAvLz0+IDUyXG4gICAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwic2Vjb25kXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMuYy5zZWNvbmQgOiBOYU47XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgbWlsbGlzZWNvbmQgb2YgdGhlIHNlY29uZCAoMC05OTkpLlxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgNSwgMjUsIDksIDMwLCA1MiwgNjU0KS5taWxsaXNlY29uZCAvLz0+IDY1NFxuICAgICAgICogQHR5cGUge251bWJlcn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm1pbGxpc2Vjb25kXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMuYy5taWxsaXNlY29uZCA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSB3ZWVrIHllYXJcbiAgICAgICAqIEBzZWUgaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvSVNPX3dlZWtfZGF0ZVxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNCwgMTEsIDMxKS53ZWVrWWVhciAvLz0+IDIwMTVcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ3ZWVrWWVhclwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyBwb3NzaWJseUNhY2hlZFdlZWtEYXRhKHRoaXMpLndlZWtZZWFyIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIHdlZWsgbnVtYmVyIG9mIHRoZSB3ZWVrIHllYXIgKDEtNTJpc2gpLlxuICAgICAgICogQHNlZSBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9JU09fd2Vla19kYXRlXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCA1LCAyNSkud2Vla051bWJlciAvLz0+IDIxXG4gICAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwid2Vla051bWJlclwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyBwb3NzaWJseUNhY2hlZFdlZWtEYXRhKHRoaXMpLndlZWtOdW1iZXIgOiBOYU47XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgZGF5IG9mIHRoZSB3ZWVrLlxuICAgICAgICogMSBpcyBNb25kYXkgYW5kIDcgaXMgU3VuZGF5XG4gICAgICAgKiBAc2VlIGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0lTT193ZWVrX2RhdGVcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTQsIDExLCAzMSkud2Vla2RheSAvLz0+IDRcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ3ZWVrZGF5XCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHBvc3NpYmx5Q2FjaGVkV2Vla0RhdGEodGhpcykud2Vla2RheSA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBvcmRpbmFsIChtZWFuaW5nIHRoZSBkYXkgb2YgdGhlIHllYXIpXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCA1LCAyNSkub3JkaW5hbCAvLz0+IDE0NVxuICAgICAgICogQHR5cGUge251bWJlcnxEYXRlVGltZX1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm9yZGluYWxcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gZ3JlZ29yaWFuVG9PcmRpbmFsKHRoaXMuYykub3JkaW5hbCA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBodW1hbiByZWFkYWJsZSBzaG9ydCBtb250aCBuYW1lLCBzdWNoIGFzICdPY3QnLlxuICAgICAgICogRGVmYXVsdHMgdG8gdGhlIHN5c3RlbSdzIGxvY2FsZSBpZiBubyBsb2NhbGUgaGFzIGJlZW4gc3BlY2lmaWVkXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCAxMCwgMzApLm1vbnRoU2hvcnQgLy89PiBPY3RcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJtb250aFNob3J0XCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IEluZm8ubW9udGhzKFwic2hvcnRcIiwge1xuICAgICAgICAgIGxvY2FsZTogdGhpcy5sb2NhbGVcbiAgICAgICAgfSlbdGhpcy5tb250aCAtIDFdIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBodW1hbiByZWFkYWJsZSBsb25nIG1vbnRoIG5hbWUsIHN1Y2ggYXMgJ09jdG9iZXInLlxuICAgICAgICogRGVmYXVsdHMgdG8gdGhlIHN5c3RlbSdzIGxvY2FsZSBpZiBubyBsb2NhbGUgaGFzIGJlZW4gc3BlY2lmaWVkXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE3LCAxMCwgMzApLm1vbnRoTG9uZyAvLz0+IE9jdG9iZXJcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJtb250aExvbmdcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gSW5mby5tb250aHMoXCJsb25nXCIsIHtcbiAgICAgICAgICBsb2NhbGU6IHRoaXMubG9jYWxlXG4gICAgICAgIH0pW3RoaXMubW9udGggLSAxXSA6IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgaHVtYW4gcmVhZGFibGUgc2hvcnQgd2Vla2RheSwgc3VjaCBhcyAnTW9uJy5cbiAgICAgICAqIERlZmF1bHRzIHRvIHRoZSBzeXN0ZW0ncyBsb2NhbGUgaWYgbm8gbG9jYWxlIGhhcyBiZWVuIHNwZWNpZmllZFxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNywgMTAsIDMwKS53ZWVrZGF5U2hvcnQgLy89PiBNb25cbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ3ZWVrZGF5U2hvcnRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1ZhbGlkID8gSW5mby53ZWVrZGF5cyhcInNob3J0XCIsIHtcbiAgICAgICAgICBsb2NhbGU6IHRoaXMubG9jYWxlXG4gICAgICAgIH0pW3RoaXMud2Vla2RheSAtIDFdIDogbnVsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBodW1hbiByZWFkYWJsZSBsb25nIHdlZWtkYXksIHN1Y2ggYXMgJ01vbmRheScuXG4gICAgICAgKiBEZWZhdWx0cyB0byB0aGUgc3lzdGVtJ3MgbG9jYWxlIGlmIG5vIGxvY2FsZSBoYXMgYmVlbiBzcGVjaWZpZWRcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTcsIDEwLCAzMCkud2Vla2RheUxvbmcgLy89PiBNb25kYXlcbiAgICAgICAqIEB0eXBlIHtzdHJpbmd9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJ3ZWVrZGF5TG9uZ1wiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyBJbmZvLndlZWtkYXlzKFwibG9uZ1wiLCB7XG4gICAgICAgICAgbG9jYWxlOiB0aGlzLmxvY2FsZVxuICAgICAgICB9KVt0aGlzLndlZWtkYXkgLSAxXSA6IG51bGw7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB0aGUgVVRDIG9mZnNldCBvZiB0aGlzIERhdGVUaW1lIGluIG1pbnV0ZXNcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKCkub2Zmc2V0IC8vPT4gLTI0MFxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUudXRjKCkub2Zmc2V0IC8vPT4gMFxuICAgICAgICogQHR5cGUge251bWJlcn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm9mZnNldFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyArdGhpcy5vIDogTmFOO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgdGhlIHNob3J0IGh1bWFuIG5hbWUgZm9yIHRoZSB6b25lJ3MgY3VycmVudCBvZmZzZXQsIGZvciBleGFtcGxlIFwiRVNUXCIgb3IgXCJFRFRcIi5cbiAgICAgICAqIERlZmF1bHRzIHRvIHRoZSBzeXN0ZW0ncyBsb2NhbGUgaWYgbm8gbG9jYWxlIGhhcyBiZWVuIHNwZWNpZmllZFxuICAgICAgICogQHR5cGUge3N0cmluZ31cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm9mZnNldE5hbWVTaG9ydFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy56b25lLm9mZnNldE5hbWUodGhpcy50cywge1xuICAgICAgICAgICAgZm9ybWF0OiBcInNob3J0XCIsXG4gICAgICAgICAgICBsb2NhbGU6IHRoaXMubG9jYWxlXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IHRoZSBsb25nIGh1bWFuIG5hbWUgZm9yIHRoZSB6b25lJ3MgY3VycmVudCBvZmZzZXQsIGZvciBleGFtcGxlIFwiRWFzdGVybiBTdGFuZGFyZCBUaW1lXCIgb3IgXCJFYXN0ZXJuIERheWxpZ2h0IFRpbWVcIi5cbiAgICAgICAqIERlZmF1bHRzIHRvIHRoZSBzeXN0ZW0ncyBsb2NhbGUgaWYgbm8gbG9jYWxlIGhhcyBiZWVuIHNwZWNpZmllZFxuICAgICAgICogQHR5cGUge3N0cmluZ31cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIm9mZnNldE5hbWVMb25nXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNWYWxpZCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLnpvbmUub2Zmc2V0TmFtZSh0aGlzLnRzLCB7XG4gICAgICAgICAgICBmb3JtYXQ6IFwibG9uZ1wiLFxuICAgICAgICAgICAgbG9jYWxlOiB0aGlzLmxvY2FsZVxuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCB3aGV0aGVyIHRoaXMgem9uZSdzIG9mZnNldCBldmVyIGNoYW5nZXMsIGFzIGluIGEgRFNULlxuICAgICAgICogQHR5cGUge2Jvb2xlYW59XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJpc09mZnNldEZpeGVkXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IHRoaXMuem9uZS51bml2ZXJzYWwgOiBudWxsO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBHZXQgd2hldGhlciB0aGUgRGF0ZVRpbWUgaXMgaW4gYSBEU1QuXG4gICAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcImlzSW5EU1RcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICBpZiAodGhpcy5pc09mZnNldEZpeGVkKSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiB0aGlzLm9mZnNldCA+IHRoaXMuc2V0KHtcbiAgICAgICAgICAgIG1vbnRoOiAxXG4gICAgICAgICAgfSkub2Zmc2V0IHx8IHRoaXMub2Zmc2V0ID4gdGhpcy5zZXQoe1xuICAgICAgICAgICAgbW9udGg6IDVcbiAgICAgICAgICB9KS5vZmZzZXQ7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyB0cnVlIGlmIHRoaXMgRGF0ZVRpbWUgaXMgaW4gYSBsZWFwIHllYXIsIGZhbHNlIG90aGVyd2lzZVxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxNikuaXNJbkxlYXBZZWFyIC8vPT4gdHJ1ZVxuICAgICAgICogQGV4YW1wbGUgRGF0ZVRpbWUubG9jYWwoMjAxMykuaXNJbkxlYXBZZWFyIC8vPT4gZmFsc2VcbiAgICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiaXNJbkxlYXBZZWFyXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIGlzTGVhcFllYXIodGhpcy55ZWFyKTtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyB0aGUgbnVtYmVyIG9mIGRheXMgaW4gdGhpcyBEYXRlVGltZSdzIG1vbnRoXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE2LCAyKS5kYXlzSW5Nb250aCAvLz0+IDI5XG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDE2LCAzKS5kYXlzSW5Nb250aCAvLz0+IDMxXG4gICAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiZGF5c0luTW9udGhcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gZGF5c0luTW9udGgodGhpcy55ZWFyLCB0aGlzLm1vbnRoKTtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyB0aGUgbnVtYmVyIG9mIGRheXMgaW4gdGhpcyBEYXRlVGltZSdzIHllYXJcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTYpLmRheXNJblllYXIgLy89PiAzNjZcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMTMpLmRheXNJblllYXIgLy89PiAzNjVcbiAgICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJkYXlzSW5ZZWFyXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZCA/IGRheXNJblllYXIodGhpcy55ZWFyKSA6IE5hTjtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmV0dXJucyB0aGUgbnVtYmVyIG9mIHdlZWtzIGluIHRoaXMgRGF0ZVRpbWUncyB5ZWFyXG4gICAgICAgKiBAc2VlIGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0lTT193ZWVrX2RhdGVcbiAgICAgICAqIEBleGFtcGxlIERhdGVUaW1lLmxvY2FsKDIwMDQpLndlZWtzSW5XZWVrWWVhciAvLz0+IDUzXG4gICAgICAgKiBAZXhhbXBsZSBEYXRlVGltZS5sb2NhbCgyMDEzKS53ZWVrc0luV2Vla1llYXIgLy89PiA1MlxuICAgICAgICogQHR5cGUge251bWJlcn1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIndlZWtzSW5XZWVrWWVhclwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQgPyB3ZWVrc0luV2Vla1llYXIodGhpcy53ZWVrWWVhcikgOiBOYU47XG4gICAgICB9XG4gICAgfV0sIFt7XG4gICAgICBrZXk6IFwiREFURV9TSE9SVFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBEQVRFX1NIT1JUO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICdPY3QgMTQsIDE5ODMnXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiREFURV9NRURcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gREFURV9NRUQ7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ30gZm9ybWF0IGxpa2UgJ09jdG9iZXIgMTQsIDE5ODMnXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiREFURV9GVUxMXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIERBVEVfRlVMTDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnVHVlc2RheSwgT2N0b2JlciAxNCwgMTk4MydcbiAgICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJEQVRFX0hVR0VcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gREFURV9IVUdFO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICcwOTozMCBBTScuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIlRJTUVfU0lNUExFXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIFRJTUVfU0lNUExFO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICcwOTozMDoyMyBBTScuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIlRJTUVfV0lUSF9TRUNPTkRTXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIFRJTUVfV0lUSF9TRUNPTkRTO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICcwOTozMDoyMyBBTSBFRFQnLiBPbmx5IDEyLWhvdXIgaWYgdGhlIGxvY2FsZSBpcy5cbiAgICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJUSU1FX1dJVEhfU0hPUlRfT0ZGU0VUXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIFRJTUVfV0lUSF9TSE9SVF9PRkZTRVQ7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ30gZm9ybWF0IGxpa2UgJzA5OjMwOjIzIEFNIEVhc3Rlcm4gRGF5bGlnaHQgVGltZScuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIlRJTUVfV0lUSF9MT05HX09GRlNFVFwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBUSU1FX1dJVEhfTE9OR19PRkZTRVQ7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ30gZm9ybWF0IGxpa2UgJzA5OjMwJywgYWx3YXlzIDI0LWhvdXIuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiVElNRV8yNF9TSU1QTEVcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gVElNRV8yNF9TSU1QTEU7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ30gZm9ybWF0IGxpa2UgJzA5OjMwOjIzJywgYWx3YXlzIDI0LWhvdXIuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiVElNRV8yNF9XSVRIX1NFQ09ORFNcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gVElNRV8yNF9XSVRIX1NFQ09ORFM7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIHtAbGluayB0b0xvY2FsZVN0cmluZ30gZm9ybWF0IGxpa2UgJzA5OjMwOjIzIEVEVCcsIGFsd2F5cyAyNC1ob3VyLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIlRJTUVfMjRfV0lUSF9TSE9SVF9PRkZTRVRcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gVElNRV8yNF9XSVRIX1NIT1JUX09GRlNFVDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnMDk6MzA6MjMgRWFzdGVybiBEYXlsaWdodCBUaW1lJywgYWx3YXlzIDI0LWhvdXIuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiVElNRV8yNF9XSVRIX0xPTkdfT0ZGU0VUXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIFRJTUVfMjRfV0lUSF9MT05HX09GRlNFVDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnMTAvMTQvMTk4MywgOTozMCBBTScuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIkRBVEVUSU1FX1NIT1JUXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIERBVEVUSU1FX1NIT1JUO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICcxMC8xNC8xOTgzLCA5OjMwOjMzIEFNJy4gT25seSAxMi1ob3VyIGlmIHRoZSBsb2NhbGUgaXMuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiREFURVRJTUVfU0hPUlRfV0lUSF9TRUNPTkRTXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIERBVEVUSU1FX1NIT1JUX1dJVEhfU0VDT05EUztcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnT2N0IDE0LCAxOTgzLCA5OjMwIEFNJy4gT25seSAxMi1ob3VyIGlmIHRoZSBsb2NhbGUgaXMuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiREFURVRJTUVfTUVEXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIERBVEVUSU1FX01FRDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnT2N0IDE0LCAxOTgzLCA5OjMwOjMzIEFNJy4gT25seSAxMi1ob3VyIGlmIHRoZSBsb2NhbGUgaXMuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiREFURVRJTUVfTUVEX1dJVEhfU0VDT05EU1wiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBEQVRFVElNRV9NRURfV0lUSF9TRUNPTkRTO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICdGcmksIDE0IE9jdCAxOTgzLCA5OjMwIEFNJy4gT25seSAxMi1ob3VyIGlmIHRoZSBsb2NhbGUgaXMuXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAgICovXG5cbiAgICB9LCB7XG4gICAgICBrZXk6IFwiREFURVRJTUVfTUVEX1dJVEhfV0VFS0RBWVwiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBEQVRFVElNRV9NRURfV0lUSF9XRUVLREFZO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB7QGxpbmsgdG9Mb2NhbGVTdHJpbmd9IGZvcm1hdCBsaWtlICdPY3RvYmVyIDE0LCAxOTgzLCA5OjMwIEFNIEVEVCcuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIkRBVEVUSU1FX0ZVTExcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gREFURVRJTUVfRlVMTDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnT2N0b2JlciAxNCwgMTk4MywgOTozMDozMyBBTSBFRFQnLiBPbmx5IDEyLWhvdXIgaWYgdGhlIGxvY2FsZSBpcy5cbiAgICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAgICAgKi9cblxuICAgIH0sIHtcbiAgICAgIGtleTogXCJEQVRFVElNRV9GVUxMX1dJVEhfU0VDT05EU1wiLFxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgIHJldHVybiBEQVRFVElNRV9GVUxMX1dJVEhfU0VDT05EUztcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnRnJpZGF5LCBPY3RvYmVyIDE0LCAxOTgzLCA5OjMwIEFNIEVhc3Rlcm4gRGF5bGlnaHQgVGltZScuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIkRBVEVUSU1FX0hVR0VcIixcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICByZXR1cm4gREFURVRJTUVfSFVHRTtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICoge0BsaW5rIHRvTG9jYWxlU3RyaW5nfSBmb3JtYXQgbGlrZSAnRnJpZGF5LCBPY3RvYmVyIDE0LCAxOTgzLCA5OjMwOjMzIEFNIEVhc3Rlcm4gRGF5bGlnaHQgVGltZScuIE9ubHkgMTItaG91ciBpZiB0aGUgbG9jYWxlIGlzLlxuICAgICAgICogQHR5cGUge09iamVjdH1cbiAgICAgICAqL1xuXG4gICAgfSwge1xuICAgICAga2V5OiBcIkRBVEVUSU1FX0hVR0VfV0lUSF9TRUNPTkRTXCIsXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIERBVEVUSU1FX0hVR0VfV0lUSF9TRUNPTkRTO1xuICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBEYXRlVGltZTtcbiAgfSgpO1xuICBmdW5jdGlvbiBmcmllbmRseURhdGVUaW1lKGRhdGVUaW1laXNoKSB7XG4gICAgaWYgKERhdGVUaW1lLmlzRGF0ZVRpbWUoZGF0ZVRpbWVpc2gpKSB7XG4gICAgICByZXR1cm4gZGF0ZVRpbWVpc2g7XG4gICAgfSBlbHNlIGlmIChkYXRlVGltZWlzaCAmJiBkYXRlVGltZWlzaC52YWx1ZU9mICYmIGlzTnVtYmVyKGRhdGVUaW1laXNoLnZhbHVlT2YoKSkpIHtcbiAgICAgIHJldHVybiBEYXRlVGltZS5mcm9tSlNEYXRlKGRhdGVUaW1laXNoKTtcbiAgICB9IGVsc2UgaWYgKGRhdGVUaW1laXNoICYmIHR5cGVvZiBkYXRlVGltZWlzaCA9PT0gXCJvYmplY3RcIikge1xuICAgICAgcmV0dXJuIERhdGVUaW1lLmZyb21PYmplY3QoZGF0ZVRpbWVpc2gpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgSW52YWxpZEFyZ3VtZW50RXJyb3IoXCJVbmtub3duIGRhdGV0aW1lIGFyZ3VtZW50OiBcIiArIGRhdGVUaW1laXNoICsgXCIsIG9mIHR5cGUgXCIgKyB0eXBlb2YgZGF0ZVRpbWVpc2gpO1xuICAgIH1cbiAgfVxuXG4gIGV4cG9ydHMuRGF0ZVRpbWUgPSBEYXRlVGltZTtcbiAgZXhwb3J0cy5EdXJhdGlvbiA9IER1cmF0aW9uO1xuICBleHBvcnRzLkZpeGVkT2Zmc2V0Wm9uZSA9IEZpeGVkT2Zmc2V0Wm9uZTtcbiAgZXhwb3J0cy5JQU5BWm9uZSA9IElBTkFab25lO1xuICBleHBvcnRzLkluZm8gPSBJbmZvO1xuICBleHBvcnRzLkludGVydmFsID0gSW50ZXJ2YWw7XG4gIGV4cG9ydHMuSW52YWxpZFpvbmUgPSBJbnZhbGlkWm9uZTtcbiAgZXhwb3J0cy5Mb2NhbFpvbmUgPSBMb2NhbFpvbmU7XG4gIGV4cG9ydHMuU2V0dGluZ3MgPSBTZXR0aW5ncztcbiAgZXhwb3J0cy5ab25lID0gWm9uZTtcblxuICByZXR1cm4gZXhwb3J0cztcblxufSh7fSkpO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9bHV4b24uanMubWFwXG4iXX0=
/* log */ 
//# sourceMappingURL=global.js.map