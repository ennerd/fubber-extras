var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "toastr", "./utils.js"], function (require, exports, toastr_1, utils_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    toastr_1 = __importDefault(toastr_1);
    (0, utils_js_1.loadCss)(utils_js_1.baseURL + '/node_modules/toastr/build/toastr.min.css');
    exports.default = toastr_1.default;
});
//# sourceMappingURL=toastr.js.map