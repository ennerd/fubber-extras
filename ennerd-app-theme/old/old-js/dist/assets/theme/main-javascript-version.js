"use strict";
define(["./dialog", "require"], (Dialog, require) => {
    class Theme {
        construct(window) {
            this.window = window;
            this.dialog = new Dialog(this);
        }
        get Dialog() {
            return this.dialog;
        }
    }
    return new Theme(window);
});
/*
var Theme = {
    Toast: ((T)=>{
        T.options.showMethod = 'slideDown';
        T.options.hideMethod = 'fadeOut';
        T.options.closeMethod = 'fadeOut';
        T.options.closeButton = true;
        T.options.positionClass = "toast-top-center";
        return T;
    })(toastr),
    isDirty: false,
    parse: function(pattern, vars) {
        var res = '' + pattern;
        for(var i in vars) {
            res = res.replace('{' + i + '}', vars[i]);
        }
        return res;
    },
    init: function() {
        var activateTab = function() {
            var $tabs = $(this);
            $tabs.find('a.tab').each(function() {
                var $this = $(this);
                var id = $this.attr('href').split("#")[1];
                if(!id) return;
                id = id.split(":")[1];
                if(!$this.hasClass('active')) {
                    $(document.getElementById(id)).hide();
                } else {
                    $(document.getElementById(id)).show();
                }
            });
        };
        $("body").on("change", "form input,select,textarea", function(e) {
            Theme.isDirty = true;
            $(this).addClass('dirty');
        });

        window.onbeforeunload = (event) => {
            if (Theme.isDirty) {
                event.preventDefault();
                event.returnValue = "Sure?";
            }
        };
        $("body").on('submit', "form", function(e) {
            let found = false;
            $(this).find(".dirty").each((index, element) => {
                found = true;
            });
            if (found) {
                $(".dirty").removeClass("dirty");
                Theme.isDirty = false;
            }
        });
        $(".tabs").each(function(){
            activateTab.apply(this);
        });
        if(!document.body._tabs_enabled) {
            var updateAccordingToHash = function() {
                var hash = document.location.hash;
                var $activated;
                $(".tabs .tab").each(function() {
                    if($(this).attr('href') == hash) {
                        $activated = $(this);
                    }
                });
                if($activated) {
                    $activated.parent().find('.tab').each(function() {
                        $(this).removeClass('active');
                        var id = $(this).attr('href').split("#")[1];
                        if(id) {
                            id = id.split(":")[1];
                            if(id) {
                                $(document.getElementById(id)).hide();
                            }
                        }
                    });
                    $activated.addClass('active');
                    var id = $activated.attr('href').split("#")[1];
                    if(id) {
                        id = id.split(":")[1];
                        if(id) {
                            $(document.getElementById(id)).show();
                        }
                    }
                }
            }
            $(updateAccordingToHash);
            $(window).on('hashchange', updateAccordingToHash);
           
            document.body._tabs_enabled = true;
        }
        $("input[data-picker]:not(.eap-init").each(function() {
            if(!Theme._data_editor) {
                Theme._data_editor = true;
                $(document.body).on('click', '*[data-view-route]', function() {
                    var $this = $(this);
                    var $hidden = $this.parent().find('*[data-the-hidden]').first();
                    if($hidden.val()!='') {
                        var url = $this.attr('data-view-route').replace(':id', $hidden.val());
                        Theme.Dialog.open(url, 'tall', function(res) {
                            console.log(res);
                        });
                    }
                });
            }
            var $this = $(this);
            $this.attr('readonly', true);
            console.log(this.name);
            var $hidden = $('<input />').attr('data-the-hidden','true').attr('type', 'hidden');
            $hidden.attr('name', $this.attr('name'));
            $hidden.val($this.val());
            if($this.attr('data-current-string')=='' && $this.val() != '')
                $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
            else
                $this.val($this.attr('data-current-string'));
            
            $this.removeAttr('name');
            $this.after($hidden);
            $this.addClass('eap-init');
            var $button = $("<button />").html('<i class="fas fa-search"></i>').addClass('f-picker').attr('type', 'button');
            if(!Theme._data_picker) {
                Theme._data_picker = true;
                $(document.body).on('click', '.f-picker', function() {
                    var $this = $(this).parent().find('*[data-picker]').first();
                    var $hidden = $(this).parent().find('*[data-the-hidden]').first();

                    Theme.Dialog.open($this.attr('data-picker'), 'tall', function(res) {
                        if(res) {
                            if(!res['_string'])
                                $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
                            else
                                $this.val(res._string);
                            $hidden.val(res.id);
                        }
                    });
                });
            }
            $this.before($button);
            if($this.attr('data-creater')) {
                var $createButton = $("<button />").html('<i class="fas fa-plus"></i>').addClass('f-creater').attr('type', 'button');
                if(!Theme._data_creater) {
                    Theme._data_creater = true;
                    $(document.body).on('click', '.f-creater', function() {
                        var $this = $(this).parent().find('*[data-creater]').first();
                        var $hidden = $(this).parent().find('*[data-the-hidden]').first();
                        Theme.Dialog.open($this.attr('data-creater'), 'tall', function(res) {
                            if(res) {
                                if(!res['_string'])
                                    $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
                                else
                                    $this.val(res._string);
                                $hidden.val(res.id);
                            }
                        });
                    });
                }
                $this.before($createButton);
            }
        });
        
        var tableIdIndex = 0;
        $("table[data-tablesource]:not(.eap-init)").each(function() {
            var $this = $(this);
            $this.addClass('eap-init');
            $.get($this.attr('data-tablesource'), function(data, textStatus, jqXHR) {
                var id;
                if(!$this.attr('id'))
                    $this.attr('id', 'dynamic-id-' + tableIdIndex++);
                var css = '';
                // Build Table Head, unless it is already built
                var $thead = $("<thead />");
                var $tr = $("<tr />");
                var idx = 1;
                for(var i in data.cols) {
                    var $th = $("<th />").text(data.cols[i][0]);
                    $th.attr('data-dynatable-column', i);
                    $tr.append($th);
                    if(data.cols[i][1] || data.cols[i][2]) {
                        css += '#' + $this.attr('id') + ' td:nth-child(' + idx + '), #' + $this.attr('id') + ' th:nth-child(' + idx++ + ') {';
                        if(data.cols[i][1]) css += 'text-align: ' + data.cols[i][1] + ';';
                        if(data.cols[i][2]) css += 'width: ' + data.cols[i][2] + 'px;';
                        
                        css += '}';
                        var $col = $("<col />");
                        $col.attr('align', data.cols[i][1]);
                        $this.append($col);
                    }
                }
                var $style = $("<style />").text(css);
                $(document.body).append($style);
                $thead.append($tr);
                $this.append($thead);
    
                // Build table body
                var $tbody = $("<tbody />");
                $this.append($tbody);
                var settings = {
                    features: {
                        pushState: false
                    },
                    dataset: {
                        perPageDefault: 20,
                        ajax: true,
                        ajaxUrl: $this.attr('data-tablesource'),
                        ajaxOnLoad: true,
                        records: []
                    }
                };
                if($this.attr('data-rowwriter')) {
                    // Must use a custom row-writer to create clickable rows
                    var rowWriter = window[$this.attr('data-rowwriter')];
                    settings.writers = {
                        _rowWriter: function(rowIndex, record, columns, cellWriter) {
                            return rowWriter.apply(record, arguments);
                        }
                    };
                } else if($this.attr('data-href')) {
                    var pattern = $this.attr('data-href');
                    settings.writers = {
                        _rowWriter: function(rowIndex, record, columns, cellWriter) {
                            var tr = '<tr data-href="' + Theme.parse(pattern, record) + '">';
                            for(var i in data.cols) {
                                tr += "<td>" + record[i] + "</td>";
                            }
                            tr += "</tr>";
                            return tr;
                        }
                    };
                }
    console.log("dt settings", settings);
                $this.dynatable(settings);
            });
        });
    }
};
if(parent && parent !== window && parent.Theme) {
    Theme.Toast = parent.Theme.Toast;
}

jQuery(function($) {
    var isMobile = function() {
        return !window.matchMedia( "(min-width: 700px)" ).matches;
    };
    
    function parse(pattern, vars) {
        return Theme.parse(pattern, vars);
        var res = '' + pattern;
        for(var i in vars) {
            res = res.replace('{' + i + '}', vars[i]);
        }
        return res;
    }

    $(window).on( 'resize', function() {
        if( isMobile() ) {
            $(document.body).addClass('mobile');
        } else {
            $(document.body).removeClass('mobile');
        }
    });

    $(window).on( 'scroll', function() {
        var st = $(window).scrollTop();
        if(st == 0) {
            $(document.body).removeClass('scrolled');
        } else {
            $(document.body).addClass('scrolled');
        }
    });
    
    $(window).on( 'submit', function() {
        $('form').on('submit', function() {
            return false;
        });
    } );
    
    $(document.body).on('click', 'tr[data-href]', function(e) {
        if(e.target.tagName == "TD") {
            location.href = $(this).attr('data-href');
        }
    });


    Theme.init();
});
*/
//# sourceMappingURL=main-javascript-version.js.map