var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "./dialogs.js", "toastr", "./dynatable.js", "./magic/theme-magic.js"], function (require, exports, dialogs_js_1, toastr_1, dynatable_js_1, theme_magic_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    dialogs_js_1 = __importDefault(dialogs_js_1);
    toastr_1 = __importDefault(toastr_1);
    dynatable_js_1 = __importDefault(dynatable_js_1);
    class Theme {
        constructor(window) {
            this.formIsDirty = false;
            this.window = window;
            this.dialogs = dialogs_js_1.default;
            this.toastr = toastr_1.default;
            this.init();
        }
        init() {
            (0, dynatable_js_1.default)();
            (0, theme_magic_js_1.setupDirtyFormHandling)(this);
            (0, theme_magic_js_1.setupTabHandling)(this);
            (0, theme_magic_js_1.setupThemeMagic)();
            (0, theme_magic_js_1.setupAccordion)();
            (0, theme_magic_js_1.setupRendered)();
            (0, theme_magic_js_1.setupRepeater)();
        }
    }
    exports.default = Theme;
});
//# sourceMappingURL=Theme.js.map