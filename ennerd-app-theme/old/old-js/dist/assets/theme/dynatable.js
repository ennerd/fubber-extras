define(["require", "exports", "../utils.js"], function (require, exports, utils_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function setupDynatable() {
        var tableIdIndex = 0;
        $("table[data-tablesource]:not(.eap-init)").each(function () {
            var $this = $(this);
            $this.addClass('eap-init');
            $.get($this.attr('data-tablesource'), function (data, textStatus, jqXHR) {
                var id;
                if (!$this.attr('id'))
                    $this.attr('id', 'dynamic-id-' + tableIdIndex++);
                var css = '';
                // Build Table Head, unless it is already built
                var $thead = $("<thead />");
                var $tr = $("<tr />");
                var idx = 1;
                for (var i in data.cols) {
                    var $th = $("<th />").text(data.cols[i][0]);
                    $th.attr('data-dynatable-column', i);
                    $tr.append($th);
                    if (data.cols[i][1] || data.cols[i][2]) {
                        css += '#' + $this.attr('id') + ' td:nth-child(' + idx + '), #' + $this.attr('id') + ' th:nth-child(' + idx++ + ') {';
                        if (data.cols[i][1])
                            css += 'text-align: ' + data.cols[i][1] + ';';
                        if (data.cols[i][2])
                            css += 'width: ' + data.cols[i][2] + 'px;';
                        css += '}';
                        var $col = $("<col />");
                        $col.attr('align', data.cols[i][1]);
                        $this.append($col);
                    }
                }
                var $style = $("<style />").text(css);
                $(document.body).append($style);
                $thead.append($tr);
                $this.append($thead);
                // Build table body
                var $tbody = $("<tbody />");
                $this.append($tbody);
                var settings = {
                    features: {
                        pushState: false
                    },
                    dataset: {
                        perPageDefault: 20,
                        ajax: true,
                        ajaxUrl: $this.attr('data-tablesource'),
                        ajaxOnLoad: true,
                        records: []
                    }
                };
                if ($this.attr('data-rowwriter')) {
                    // Must use a custom row-writer to create clickable rows
                    var rowWriter = window[$this.attr('data-rowwriter')];
                    settings.writers = {
                        _rowWriter: function (rowIndex, record, columns, cellWriter) {
                            return rowWriter.apply(record, arguments);
                        }
                    };
                }
                else if ($this.attr('data-href')) {
                    var pattern = $this.attr('data-href');
                    settings.writers = {
                        _rowWriter: function (rowIndex, record, columns, cellWriter) {
                            var tr = '<tr data-href="' + (0, utils_js_1.parseString)(pattern, record) + '">';
                            for (var i in data.cols) {
                                tr += "<td>" + record[i] + "</td>";
                            }
                            tr += "</tr>";
                            return tr;
                        }
                    };
                }
                console.log("dt settings", settings);
                $this.dynatable(settings);
            });
        });
    }
    exports.default = setupDynatable;
    ;
});
//# sourceMappingURL=dynatable.js.map