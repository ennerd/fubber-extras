/*
window.require = {
    baseUrl: baseUrl,
    paths: {
        // "toastr-private": 'toastr/toastr'
        "@private/jquery-ui": "jquery-ui/dist/jquery-ui.min",
        "@private/jquery-inputmask": "inputmask/dist/jquery.inputmask.min",
        "@private/jquery-dynatable": "Dynatable/jquery.dynatable",
        "@private/jquery-fileupload": "blueimp-file-upload/js/jquery.fileupload",
        "@private/chartist": "chartist/dist/index.umd"
    },
    packages: [
        {
            name: 'main',
            location: './dist',
            main: 'index'
        },
        {
            name: 'Theme',
            location: '../assets/theme',
            main: 'main'
        },
        {
            name: "jquery",
            location: "node_modules/jquery",
            main: "dist/jquery.js"
        }
    ]
};
function css(path) {
    let el = document.createElement('link');
    el.setAttribute('rel', 'stylesheet');
    el.setAttribute('href', path);
    document.head.appendChild(el);
}
define('fontawesome', () => FontAwesome);
define('jquery', () => jQuery);
define('toastr', ['toastr/toastr'], (toastr) => {
    css(baseUrl + '/node_modules/toastr/build/toastr.min.css');
    return toastr;
});
define('jquery-ui', ['@private/jquery-ui', 'jquery'], (ui, jq) => {
    css(baseUrl + '/node_modules/jquery-ui/dist/themes/base/jquery-ui.min.css');
    css(baseUrl + '/assets/jquery-ui-custom.css');
    return ui;
});
define('jquery-inputmask', ['@private/jquery-inputmask', 'jquery'], function(im, jq) {
    return jq.fn.inputmask;
});
define('jquery-dynatable', ['@private/jquery-dynatable', 'jquery'], function(dynatable, jquery) {
    css(baseUrl + '/node_modules/Dynatable/jquery.dynatable.css');
    css(baseUrl + '/assets/jquery-dynatable-custom.css');
    jquery.dynatableSetup({
        inputs: <?=json_encode([
            "pageText" => trans("Pages:") . ' ',
            "perPageText" => trans("Show:") . ' ',
            "searchText" => trans("Search:") . ' ',
            "paginationPrev" => trans("Previous"),
            "paginationNext" => trans("Next"),
            "processingText" => trans("Processing..."),
            "recordCountText" => trans("Showing"),
            "recordCountFilteredTemplate" => trans(" (filtered from {recordsTotal} total records)"),
            "recordCountTotalTemplate" => trans("{recordsQueryCount} {collectionName}"),
            "recordCountPageUnboundedTemplate" => trans("{recordsShown} of"),
            "recordCountPageBoundTemplate" => trans("{pageLowerBound} to {pageUpperBound} of"),
        ]); ?>
    });
    return jquery.fn.dynatable;
});
define('jquery-fileupload', ['@private/jquery-fileupload', 'jquery'], function(fileupload, jquery) {
    css(baseUrl + '/node_modules/blueimp-file-upload/css/jquery.fileupload.css');
    css(baseUrl + '/node_modules/blueimp-file-upload/css/jquery.fileupload-ui.css');
    return jquery.fn.fileupload;
});
define('classic-chartist', ['@private/chartist'], function(chartist) {
    css(baseUrl + '/node_modules/chartist/dist/index.css');
    return chartist;
});
*/