import dialogs from "./dialogs.js";
import toastr from "toastr";
import setupDynatable from "./dynatable.js";
import { setupAccordion, setupDirtyFormHandling, setupRendered, setupRepeater, setupTabHandling, setupThemeMagic } from "./magic/theme-magic.js";

export default class Theme {
    window: Window;
    dialogs: typeof dialogs;
    toastr: Toastr;
    formIsDirty: boolean = false;
    
    constructor(window: Window) {
        this.window = window;
        this.dialogs = dialogs;
        this.toastr = toastr;
        this.init();
    }

    init() {
        setupDynatable();
        setupDirtyFormHandling(this);
        setupTabHandling(this);
        setupThemeMagic();
        setupAccordion();
        setupRendered();
        setupRepeater();
    }
}