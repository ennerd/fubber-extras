import { baseURL, isMobile, loadCss } from "../../utils.js";
import Theme from "../Theme.js";

/**
 * Handle tabs via url hash
 */
export function setupTabHandling(theme: Theme): void {
    var activateTab = function(el: HTMLElement) {
        var $tabs = $(el);
        $tabs.find('a.tab').each(function() {
            var $this = $(this);
            var id = $this.attr('href')?.split("#")[1];
            if(!id) return;
            id = id.split(":")[1];
            if(!$this.hasClass('active')) {
                $(document.getElementById(id) as HTMLElement).hide();
            } else {
                $(document.getElementById(id) as HTMLElement).show();
            }
        });
    };
    $(".tabs").each(function(){
        activateTab(this);
    });

    var updateAccordingToHash = function() {
        var hash = document.location.hash;
        var $activated: JQuery<HTMLElement>|null = null;

        for (const el of $(".tabs .tab")) {
            if ($(el).attr('href') == hash) {
                $activated = $(el);
            }
        }
        
        if($activated) {
            $activated.parent().find('.tab').each(function() {
                const $this = $(this as HTMLElement);
                $this.removeClass('active');
                var id = $this.attr('href')?.split("#")[1];
                if(id) {
                    id = id.split(":")[1];
                    if(id) {
                        $("#" + id).hide();
                    }
                }
            });
            $activated.addClass('active');
            var id = $activated.attr('href')?.split("#")[1];
            if(id) {
                id = id.split(":")[1];
                if(id) {
                    $("#" + id).show();
                }
            }
        }
    }
    $(updateAccordingToHash);
    $(window).on('hashchange', updateAccordingToHash);
}

var dirtyFormHandlingInitialized = false;
export function setupDirtyFormHandling(theme: Theme): void {
    if (dirtyFormHandlingInitialized) {
        return;
    }
    dirtyFormHandlingInitialized = true;

    /**
     * Notice when stuff changes
     */
    $("body").on("change", "form input, form select, form textarea", function(e) {
        theme.formIsDirty = true;
        $(this).addClass('dirty');
    });

    /**
     * Notice when form is being submitted
     */
    $("body").on('submit', "form", function(e) {
        let found = false;
        $(this).find(".dirty").each((index, element) => {
            found = true;
        });
        if (found) {
            $(".dirty").removeClass("dirty");
            theme.formIsDirty = false;
        }
    });

    /**
     * Notice when leaving page that is dirty
     */
    window.onbeforeunload = (event) => {
        if (theme.formIsDirty) {
            event.preventDefault();
            event.returnValue = "Sure?";
        }
    };
}

var themeMagicInitialized = false;
export function setupThemeMagic() {
    if (themeMagicInitialized) {
        return;
    }
    themeMagicInitialized = true;

    $(document.body).on('click', 'tr[data-href]', function(e) {
        if(e.target.tagName == "TD") {
            window.location.href = $(this).attr('data-href') as string;
        }
    });

    $(window).on( 'submit', function() {
        $('form').on('submit', function() {
            return false;
        });
    });

    $(window).on( 'resize', function() {
        if( isMobile() ) {
            $(document.body).addClass('mobile');
        } else {
            $(document.body).removeClass('mobile');
        }
    });    

    $(window).on( 'scroll', function() {
        var st = $(window).scrollTop();
        if(st == 0) {
            $(document.body).removeClass('scrolled');
        } else {
            $(document.body).addClass('scrolled');
        }
    });
    
}

var dataPickerInitialized = false;
export function setupDataPicker(theme: Theme): void {
    $("input[data-picker]:not(.eap-init").each(function() {
        $(document.body).on('click', '*[data-view-route]', function() {
            var $this = $(this);
            var $hidden = $this.parent().find('*[data-the-hidden]').first();
            var hiddenVal = $hidden?.val() as string;
            if(hiddenVal && hiddenVal != '') {
                var url = $this.attr('data-view-route')?.replace(':id', hiddenVal);
                theme.dialogs.open(url, 'tall', function(res: any) {
                    console.log(res);
                });
            }
        });
        var $this = $(this);
        $this.attr('readonly', "readonly");
        var $hidden = $('<input />').attr('data-the-hidden','true').attr('type', 'hidden');
        $hidden.attr('name', $this.attr('name') ?? '');
        $hidden.val($this.val() ?? '');
        if($this.attr('data-current-string')=='' && $this.val() != '')
            $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
        else
            $this.val($this.attr('data-current-string') ?? '');
        
        $this.removeAttr('name');
        $this.after($hidden);
        $this.addClass('eap-init');
        var $button = $("<button />").html('<i class="fas fa-search"></i>').addClass('f-picker').attr('type', 'button');
        $this.before($button);
        if($this.attr('data-creater')) {
            var $createButton = $("<button />").html('<i class="fas fa-plus"></i>').addClass('f-creater').attr('type', 'button'); 
            $(document.body).on('click', '.f-creater', function() {
                var $this = $(this).parent().find('*[data-creater]').first();
                var $hidden = $(this).parent().find('*[data-the-hidden]').first();
                theme.dialogs.open($this.attr('data-creater'), 'tall', function(res: any) {
                    if(res) {
                        if(!res['_string'])
                            $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
                        else
                            $this.val(res._string);
                        $hidden.val(res.id);
                    }
                });
            });
            $this.before($createButton);
        }
    });

    if(!dataPickerInitialized) {
        dataPickerInitialized = true;
        $(document.body).on('click', '.f-picker', function() {
            var $this = $(this).parent().find('*[data-picker]').first();
            var $hidden = $(this).parent().find('*[data-the-hidden]').first();

            theme.dialogs.open($this.attr('data-picker'), 'tall', function(res: any) {
                if(res) {
                    if(!res['_string'])
                        $this.val('_string not set in JSON. Override Model::jsonSerialize to add');
                    else
                        $this.val(res._string);
                    $hidden.val(res.id);
                }
            });
        });
    }
}

var accordionInitialized = false;
export function setupAccordion() {
    if (accordionInitialized) {
        return;
    }
    accordionInitialized = true;
    jQuery(function($) {
        $(document.body).on('click', '.rendered-accordion-title', function() {
            var $this = $(this);
            var $pane = $this.parent('.rendered-accordion-pane');
            $pane.toggleClass('rendered-accordion-active');
        });
    });
    loadCss(baseURL + '/assets/css/theme-accordion.css');
}

var renderedInitialized = false;
export function setupRendered() {
    if (renderedInitialized) {
        return;
    }
    renderedInitialized = true;
    loadCss(baseURL + '/assets/css/theme-rendered.css');
}

var repeaterInitialized = false;
export function setupRepeater(theme: Theme) {
    if (!repeaterInitialized) {
        loadCss(baseURL + '/assets/css/theme-repeater.css');
    }
    repeaterInitialized = true;
    jQuery(function($) {
        $(".rendered-repeater > div.rendered-repeater-blocks").sortable({ axis: "y", handle: ".rendered-repeater-handle", update: function() {
            
        } });
        $(document.body).on('focus mousedown', '.rendered-repeater-adder *', function() {
            //if($(this).is('button')) return false;
            var $template = $(this).closest('.rendered-repeater-adder').find(".rendered-repeater-template");
            var $newTemplate = $template.clone();
            $template.removeClass('rendered-repeater-template');
            $newTemplate.addClass('rendered-repeater-template');
            $newTemplate.insertAfter($template);
            $template.appendTo($(this).closest('.rendered-repeater').find('.rendered-repeater-blocks').first());
            $(this).focus();
            //renumber($(this).closest('form')[0]);
            setTimeout(function() {
                theme.init();
            }, 50);
            return false;
        });
        var trashTimeout: number;
        $(document.body).on('click', '.rendered-repeater-trash-button', function() {
            var $this = $(this);
            if($this.is('.must-confirm')) {
                $this.closest('.rendered-repeater-block').remove();
                return;
            }
            $this.closest('.rendered-repeater').find('.must-confirm').each(function() {
                $(this).removeClass('must-confirm');
                $(this).find('i').removeClass('fa-question').addClass('fa-trash');
            });
            $this.find('i').removeClass('fa-trash').addClass('fa-question');
            $this.addClass('must-confirm');
            clearTimeout(trashTimeout);
            trashTimeout = setTimeout(function() {
                if($this.is('.must-confirm')) {
                    $this.removeClass('must-confirm');
                    $this.find('i').removeClass('fa-question').addClass('fa-trash');
                }
            }, 2000);
        });
        $(document.body).on('submit', function() {
            $(this).find('input[type=checkbox]').each(function() {
                var $this = $(this);
                var checked = this.checked;
                $(this).attr('data-was-checkbox', this.value);
                $(this).attr('type', 'text');
                this.value = checked ? this.value : '';
            });
            var $template = $(".rendered-repeater-template");
            var $parent = $template.parent();
            $template.remove();
            setTimeout(function() {
                $parent.append($template);
                $(document.body).find('input[data-was-checkbox]').each(function() {
                    var $this = $(this);
                    var checked = this.value != '';
                    this.value = $this.attr('data-was-checkbox');
                    $(this).removeAttr('data-was-checkbox');
                    $(this).attr('type', 'checkbox');
                    this.checked = checked;
                });
            }, 0);
            return true;
        });
    });
}