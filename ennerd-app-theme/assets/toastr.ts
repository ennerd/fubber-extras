import toastr from "toastr";
import { baseURL, loadCss } from "./utils.js";

loadCss(baseURL + '/node_modules/toastr/build/toastr.min.css');

export default toastr;