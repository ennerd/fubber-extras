jQuery(function($) {
    $(".rendered-repeater > div.rendered-repeater-blocks").sortable({ axis: "y", handle: ".rendered-repeater-handle", update: function() {
        
    } });
    $(document.body).on('focus mousedown', '.rendered-repeater-adder *', function() {
        //if($(this).is('button')) return false;
        var $template = $(this).closest('.rendered-repeater-adder').find(".rendered-repeater-template");
        var $newTemplate = $template.clone();
        $template.removeClass('rendered-repeater-template');
        $newTemplate.addClass('rendered-repeater-template');
        $newTemplate.insertAfter($template);
        $template.appendTo($(this).closest('.rendered-repeater').find('.rendered-repeater-blocks').first());
        $(this).focus();
        //renumber($(this).closest('form')[0]);
        setTimeout(function() {
            Theme.init();
        }, 50);
        return false;
    });
    var trashTimeout;
    $(document.body).on('click', '.rendered-repeater-trash-button', function() {
        var $this = $(this);
        if($this.is('.must-confirm')) {
            $this.closest('.rendered-repeater-block').remove();
            return;
        }
        $this.closest('.rendered-repeater').find('.must-confirm').each(function() {
            $(this).removeClass('must-confirm');
            $(this).find('i').removeClass('fa-question').addClass('fa-trash');
        });
        $this.find('i').removeClass('fa-trash').addClass('fa-question');
        $this.addClass('must-confirm');
        clearTimeout(trashTimeout);
        trashTimeout = setTimeout(function() {
            if($this.is('.must-confirm')) {
                $this.removeClass('must-confirm');
                $this.find('i').removeClass('fa-question').addClass('fa-trash');
                Theme.init();
            }
        }, 2000);
    });
    $(document.body).on('submit', function() {
        $(this).find('input[type=checkbox]').each(function() {
            var $this = $(this);
            var checked = this.checked;
            $(this).attr('data-was-checkbox', this.value);
            $(this).attr('type', 'text');
            this.value = checked ? this.value : '';
        });
        var $template = $(".rendered-repeater-template");
        var $parent = $template.parent();
        $template.remove();
        setTimeout(function() {
            $parent.append($template);
            $(document.body).find('input[data-was-checkbox]').each(function() {
                var $this = $(this);
                var checked = this.value != '';
                this.value = $this.attr('data-was-checkbox');
                $(this).removeAttr('data-was-checkbox');
                $(this).attr('type', 'checkbox');
                this.checked = checked;
            });
        }, 0);
        return true;
    });
});