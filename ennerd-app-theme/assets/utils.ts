export function loadCss(path: string): void {
    const el = document.createElement('link');
    el.setAttribute('href', path);
    el.setAttribute('rel', 'stylesheet');
    el.setAttribute('type', 'text/css');
    document.head.appendChild(el);
};

export function parseString(pattern: string, vars: { [key: string]: string }) {
    var res = '' + pattern;
    for(var i in vars) {
        res = res.replace('{' + i + '}', vars[i]);
    }
    return res;
}

export function isMobile(): boolean {
    return !window.matchMedia( "(min-width: 700px)" ).matches; 
};


export const baseURL = "/app/extra/ennerd-app-theme";