<?php
    $iterate = function($spec, $row) use (&$iterate) {
        if(!method_exists($row, 'meta'))
            throw new \Exceptions\CodingErrorException("Model must have meta() method");
        $meta = $row::meta();
        if(!isset($meta['cols']))
            throw new \Exceptions\CodingErrorException("meta() must return 'col' specification.");
        $res = '<div class="items">';
        foreach($spec as $item) {
            $res .= '<div class="item">';
            switch($item[0]) {
                case 'field' :
                    $res .= '<div class="item-field">';
                    $res .= '<label>'.htmlspecialchars($meta['cols'][$item[1]]['caption']).'</label>';
                    $res .= '<span>'.htmlspecialchars($row->$item[1]).'</span>';
                    $res .= '</div>';
                    break;
                case 'cols' :
                    $res .= '<div class="item-cols">';
                    for($i = 1; $i < sizeof($item); $i++) {
                        $res .= $iterate($item[$i], $row);
                    }
                    $res .= '</div>';
                    break;
            }
            $res .= '</div>';
        }
        $res .= "</div>";
        return $res;
    }
?>
<div class='info-layout'>
    <?=$iterate($spec, $row); ?>
</div>
<pre>
    <?php print_r($row->meta()); ?>
    <?php print_r($spec); ?>
    <?php print_r($row); ?>
</pre>