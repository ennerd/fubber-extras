<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('Control Panel'))
@section('main')
<div class="padding">
    <h1>{{trans("Control Panel")}}</h1>
    <div>
        <?php
        $items = [];
        $items = Fubber\Hooks::filter('Theme.ControlPanel.Icons', $items, $state);
        usort($items, function($a,$b) {
            $aa = 0;
            $bb = 0;
            if(is_object($a) && isset($a->weight))
                $aa = $a->weight;
            if(is_object($b) && isset($b->weight))
                $bb = $b->weight;
            if($aa == $bb) return 0;
            return $aa < $bb ? -1 : 1;
        });
        if(sizeof($items) > 0) {
        ?>
            <ul class='icons'>
                <?php foreach($items as $icon) { ?>
                <li><?=$icon; ?></li>
                <?php } ?>
            </ul>
        <?php   
        }
        ?>
    </div>
</div>
<?php foreach(Fubber\Hooks::filter('Theme.ControlPanel.Footer', [], $state) as $view) { ?>
<?php /* The ControlPanel.view hook is now the Theme.ControlPanel.Footer filter*/ ?>
    <?=$view->__toString(); ?>
<?php } ?>
@stop
