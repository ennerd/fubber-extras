<?php
use Fubber\I18n\Translatable;
use function Fubber\trans; ?><!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fontsource/barlow@4.5.9/index.css" integrity="sha256-xBzhwzIhqPTsuRxAym65yGSQfgg0fLlgx4/rGyc4fuo=" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.0/css/all.min.css" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/combine/npm/es5-shim@4.6.7,npm/es6-shim@0.35.6/es6-shim.min.js,npm/jquery@3.6.1,npm/jquery-ui@1.13.2/dist/jquery-ui.min.js,npm/inputmask@5.0.7/dist/jquery.inputmask.min.js,npm/chartist@1.2.1,npm/luxon@3.0.4,npm/toastr@2.1.4/build/toastr.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/gh/alfajango/jquery-dynatable@v0.3.1/jquery.dynatable.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/combine/npm/jquery-ui@1.13.2/dist/themes/base/jquery-ui.min.css,npm/jquery-ui@1.13.2/dist/themes/base/theme.min.css,npm/toastr@2.1.4/build/toastr.min.css,gh/alfajango/jquery-dynatable@v0.3.1/jquery.dynatable.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fontsource/barlow@4.5.9/index.css" integrity="sha256-xBzhwzIhqPTsuRxAym65yGSQfgg0fLlgx4/rGyc4fuo=" crossorigin="anonymous">
        <script src="/app/extra/ennerd-app-theme/legacy/theme/js/theme.js"></script>
		<script src="/app/extra/ennerd-app-theme/legacy/theme/js/dialogs.js"></script>
		<script src="/app/extra/ennerd-app-theme/legacy/theme/js/chartist.js"></script>

        <!--
        
        <script src="https://cdn.jsdelivr.net/npm/es5-shim@4.6.7/es5-shim.min.js" integrity="sha256-YVbnCb9iv5plopND6qHAog9bAhpm/PFr4njWU3Kj0Xs=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/es6-shim@0.35.6/es6-shim.min.js" integrity="sha256-uh9aKfQ3Sg6o6bBsrB9JyF8zbHS80mmtWHP0bKwLIrc=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-ui@1.13.2/dist/jquery-ui.min.js" integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/inputmask@5.0.7/dist/jquery.inputmask.min.js" integrity="sha256-roLwrdwEWBEs8kKLczjbBYuMOmXQdLXX9rAhPct2NfQ=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartist@1.2.1/dist/index.umd.js" integrity="sha256-60xI7OmQDIrt4yjSE/+pgw3dnyoL/BZyjd0GtwSU2wE=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/luxon@3.0.4/build/global/luxon.min.js" integrity="sha256-APLcdSn5AMXqZzoKhlczWWyV93oXBTrfkjhqtMX8vEM=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.js" integrity="sha256-Hgwq1OBpJ276HUP9H3VJkSv9ZCGRGQN+JldPJ8pNcUM=" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-ui@1.13.2/dist/themes/base/jquery-ui.min.css" integrity="sha256-VNxxeWv78fBpVZ3cM8LomS7+xUH2IXl6hJ1EKmmCJpY=" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-ui@1.13.2/dist/themes/base/theme.css" integrity="sha256-wcgHeWH6rwi8WlbWbSh9ZnCT1KxaROvJXL2TV8wIn8o=" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" integrity="sha256-R91pD48xW+oHbpJYGn5xR0Q7tMhH4xOrWn1QqMRINtA=" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.0/css/all.min.css" integrity="sha256-AbA177XfpSnFEvgpYu1jMygiLabzPCJCRIBtR5jGc0k=" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fontsource/barlow@4.5.9/index.css" integrity="sha256-xBzhwzIhqPTsuRxAym65yGSQfgg0fLlgx4/rGyc4fuo=" crossorigin="anonymous">


-->

        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/theme.css">
        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/chartist.css">
        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/jquery-dynatable.css">
        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/rendered.css">
        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/repeater.css">
        <link rel="stylesheet" type="text/css" href="/app/extra/ennerd-app-theme/legacy/theme/css/accordion.css">

        <?php if(isset($state->scripts)) foreach($state->scripts as $_url) { ?>
		    <script src="{{$_url}}"></script>
        <?php } ?>
        <?php if(isset($state->stylesheets)) foreach($state->stylesheets as $_url)  { ?>
		    <link rel="stylesheet" type="text/css" href="{{$_url}}" />
        <?php } ?>

    	<?php foreach(Fubber\Hooks::dispatch('Theme.Head', $state) as $part) { ?>
            <?=$part; ?>
        <?php } ?>
@section('head')
@show
        <?php if(isset($state->inline_scripts)) foreach($state->inline_scripts as $_script) { ?>
                <script type="text/javascript"><?=$_script;?></script>
        <?php } ?>
    </head>
    <body class="<?php if(isset($page_class)) echo htmlspecialchars($page_class); ?>">
@section('body')
Override the 'body' section, please.
@show
<?php foreach(Fubber\Hooks::dispatch('Theme.Body') as $part) {
    echo $part;
} ?>
<script>
<?php
$messages = \Fubber\FlashMessage::getMessages($state);
if($messages) foreach($messages as $message) {
	switch($message->classNames) {
		case \Fubber\FlashMessage::SUCCESS :
		case \Fubber\FlashMessage::POSITIVE :
			echo "Theme.Toast.success(".json_encode($message->message).");";
			break;
		case \Fubber\FlashMessage::INFO :
		case \Fubber\FlashMessage::FLOATING :
			echo "Theme.Toast.info(".json_encode($message->message).");";
			break;
		case \Fubber\FlashMessage::WARNING :
			echo "Theme.Toast.warning(".json_encode($message->message).");";
			break;
		case \Fubber\FlashMessage::ERROR :
		case \Fubber\FlashMessage::NEGATIVE :
			echo "Theme.Toast.error(".json_encode($message->message).");";
			break;
	}
}
?>
</script>
<div id="load-indicator" class="sk-folding-cube" style="display: none; top: 40%">
  <div class="sk-cube1 sk-cube"></div>
  <div class="sk-cube2 sk-cube"></div>
  <div class="sk-cube4 sk-cube"></div>
  <div class="sk-cube3 sk-cube"></div>
</div>
    </body>
</html>
