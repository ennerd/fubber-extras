@extends('theme/base')
@section('body')
@include('theme/parts/header')
<div class='theme-body'>
<div class='nav-and-main'>
@include('theme/parts/nav')
@include('theme/parts/main')
</div>
@include('theme/parts/footer')
</div>
@stop
