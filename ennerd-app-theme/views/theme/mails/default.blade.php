<h1>@yield('title')</h1>

@yield('contents')

<p><small>This e-mail was sent from {{$base_url}}</small></p>