@extends($dialog ? 'theme/dialog' : 'theme/default')
@section('title', isset($page_title) ? $page_title : $title)
@section('main')
<div class="padding" data-template="<?=__FILE__; ?>">
    <h1>{{$title}}</h1>

    <?php if(!empty($toolbar)) { ?>
        @renderview($toolbar, $row)
    <?php } ?>
    <?php foreach(Fubber\Hooks::dispatch($controller.'.view.top', $state, $row) as $view) if($view) { ?>
        <?=(string) $view; ?>
    <?php } ?>

    <p>{{$text}}</p>
    <?php if(!empty($view_def)) { ?>
    @renderview($view_def, $row)
    <?php } else { ?>

        <table class='form'>
            <tbody>
        <?php foreach($cols as $spec) { ?>

        <?php $col = array_shift($spec); ?>

                <tr>
                    <th>{{$spec[0]}}</th>
                    <td><?php
                    if(isset($spec['filter']))
                        echo $spec['filter']($row->$col);
                    else
                        echo htmlspecialchars($row->$col);
                    ?></td>
                </tr>
        <?php } ?>
            </tbody>
        </table>
        <p>
            <?php if($edit_button) { ?>
            <button onclick="editButton();">{{$edit_button}}</button>
            <?php } ?>
            <?php if($delete_button) { ?>
            <button onclick="deleteButton();">{{$delete_button}}</button>
            <?php } ?>
            <button onclick="backButton();">{{$back_button}}</button>
        </p>
        
    <?php } ?>
</div>
<?php foreach(Fubber\Hooks::dispatch($controller.'.view', $state, $row) as $view) if($view) { ?>
    <?=(string) $view; ?>
<?php } ?>
<script>
    var dialog = <?=json_encode($dialog); ?>;
    var edited = false;
    function editButton() {
        if(dialog) {
            Theme.Dialog.open('{{$edit_url}}dialog/', 'tall', function(res) {
                if(res)
                    location.href = location.href;
            });
        } else {
            location.href='{{$edit_url}}';
        }
    }
    function deleteButton() {
        if(dialog) {
            Theme.Dialog.open('{{$delete_url}}dialog/', 'px300', function(res) {
                if(res)
                    Theme.Dialog.close(res);
            });
        } else {
            location.href='{{$delete_url}}';
        }
    }
    function backButton() {
        if(dialog) {
            Theme.Dialog.close();
        } else {
            location.href='{{$back_url}}';
        }
    }
    $("button.edit").on('click', editButton);
    $("button.delete").on('click', deleteButton);
    $("button.back").on('click', backButton);
</script>
@stop
