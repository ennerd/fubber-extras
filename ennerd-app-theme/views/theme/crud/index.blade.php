@extends($dialog ? 'theme/dialog' : 'theme/default')
@section('title', $title)
@section('main')
<div class="padding" data-template="<?=__FILE__; ?>">
    <h1>{{$title}}</h1>
    
    <?php if(!empty($toolbar)) { ?>
    @renderview($toolbar, $row)
    <?php } ?>

    <?php foreach(Fubber\Hooks::dispatch($controller.'.index.top', $state) as $view) if($view) { ?>
        <?=(string) $view; ?>
    <?php } ?>

    <p>{{$text}}</p>



    <table id='index_table' data-tablesource="/api/tablesource/{{$tableSource}}/<?=!empty($tableSourceParams) ? '?' . http_build_query($tableSourceParams) : ''; ?>" data-rowwriter="{{$tableSource}}_rowwriter"></table>
    <?php if($create_button) { ?>
    <p><button onclick="{{$tableSource}}_create()">{{$create_button}}</button></p>
    <?php } ?>
</div>
<?php foreach(Fubber\Hooks::dispatch($controller.'.index', $state) as $view) if($view) { ?>
    <?=(string) $view; ?>
<?php } ?>
<script>
    var primaryKey = <?=json_encode($primaryKey); ?>;
    var rowSource = <?=json_encode("/api/tablesource/".$tableSource."/".(!empty($tableSourceParams) ? '?' . http_build_query($tableSourceParams) : '')); ?>;
    var dialog = <?=json_encode($dialog); ?>;
    var pick = <?=json_encode($pick); ?>;
    var prefix = <?=json_encode($tableSource); ?>;

    function {{$tableSource}}_rowwriter(row) {

        var map = {};
        for(var i in this) {
            map['{' + i + '}'] = this[i];
        }
        for (var i in this._raw) {
            map['{raw.' + i + '}'] = this._raw[i];
        }

        var linkTemplate = <?=json_encode($linkTemplate); ?>;
        var cols = <?=json_encode($cols); ?>;
        for(var i in map) {
            linkTemplate = linkTemplate.replace(i, map[i]);
        }
        var res = '<tr data-raw="' + JSON.stringify(this._raw).replace(/"/g, "&quot;") + '"';

        if (dialog) {
            res += ' onclick="{{$tableSource}}_rowclick(this);"';
        } else {
            res += ' data-href="' + linkTemplate + '"';
        }
        res += '>';
        /*
        if(dialog) {
            res = '<tr onclick="{{$tableSource}}_rowclick(' + JSON.stringify(this._raw).replace(/"/g, "&quot;") + ')">';
            res = '<tr data-row-id="' + JSON.stringify(this._row).replace(/"/g, "&quot;") + '">';
        } else {
            res = '<tr data-href="' + linkTemplate + '">';
        }
        */
        for(var i in cols) {
            res += '<td>' + this[i] + '</td>';
        }
        res += '</tr>';
        return res;
    }
    function {{$tableSource}}_rowclick(row) {
        var raw = JSON.parse($(row).attr('data-raw'));
        var tpl = <?=json_encode($linkTemplate); ?>;
        if (pick) {
            Theme.Dialog.close(raw);
        } else {
            {{$tableSource}}_dialog(tpl.replace('{id}', raw.id) + 'dialog/');
        }
    }
    function {{$tableSource}}_dialog(link) {
        Theme.Dialog.open(link, 'tall', function(res) {
            $("#index_table").data('dynatable').process();
        });
    }
    function niu_{{$tableSource}}_pick(row) {
        Theme.Dialog.close(row);
    }
    function {{$tableSource}}_create() {
        if(dialog) {
            Theme.Dialog.open('<?php list($url, $q) = explode("?", $create_url); echo $url.'dialog/?'.$q; ?>', 'tall', function(res) {
                if(pick) {
                    Theme.Dialog.close(res);
                } else {
                    $("#index_table").data('dynatable').process();
                }
            });
        } else {
            location.href='{{$create_url}}';        
        }
    }
    var qel = document.createElement('div');
    var qcel = document.createElement('div');
    qel.appendChild(qcel);
    function quoteAttr(v) {
        qcel.setAttribute('data-attr', v);
        var res = qel.innerHTML.substring(16);
        return res.substring(0, res.length - 8);
    }

</script>
@stop
