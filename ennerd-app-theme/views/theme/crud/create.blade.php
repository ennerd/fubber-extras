@extends($dialog ? 'theme/dialog' : 'theme/default')
@section('title', $title)
@section('main')
<div class="padding" data-template="<?=__FILE__; ?>">
    <h1>{{$title}}</h1>
    <?php if(!empty($toolbar)) { ?>
    @renderview($toolbar, $row)
    <?php } ?>

    <?php foreach(Fubber\Hooks::dispatch($controller.'.create.top', $state) as $view) if($view) { ?>
        <?=(string) $view; ?>
    <?php } ?>

    <p>{{$text}}</p>

    <?php if(!empty($create_def)) { ?>
        @renderform($form, $create_def, $row)
    <?php } else { ?>

        <?=$form->begin(); ?>
        <?=$form->render(); ?>
        <p><?=$form->submit('save', $save_button); ?> <button type="button" onclick="backButton()">{{$back_button}}</button></p>
        <?=$form->end(); ?>

    <?php }?>

</div>
<script>
    var dialog = <?=json_encode($dialog); ?>;
    function backButton() {
        if(dialog)
            Theme.Dialog.close();
        else
            location.href='{{$back_url}}';
    }
    $("button.back").on('click', backButton);
</script>
@stop