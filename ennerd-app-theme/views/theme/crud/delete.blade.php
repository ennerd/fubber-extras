@extends($dialog ? 'theme/dialog' : 'theme/default')
@section('title', $title)
@section('main')
<div class="padding" data-template="<?=__FILE__; ?>">
    <h1>{{$title}}</h1>

    <p>{{$text}}</p>
    
    <form method="post">
    <p>
        <button>{{$confirm_button}}</button>
        <button type="button" onclick="backButton();">{{$back_button}}</button>
    </p>
    </form>
</div>
<script>
    var dialog = <?=json_encode($dialog); ?>;
    function backButton() {
        if(dialog) {
            Theme.Dialog.close();
        } else {
            location.href='{{$back_url}}';
        }
    }
</script>
@stop