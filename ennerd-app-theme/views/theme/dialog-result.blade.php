<?php
use function Fubber\trans;
?>@extends('theme/dialog')
@section('title', trans('Closing Dialog'))
@section('main')
<script>
    Theme.Dialog.close(<?=json_encode(isset($result) ? $result : null);?>);
</script>
@stop
