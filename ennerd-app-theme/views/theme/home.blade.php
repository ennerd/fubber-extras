@extends('theme/default')
@section('title', 'The Title')
@section('main')
<div class="padding max-width">
    <h1><em>Hva</em> er Lorem Ipsum?</h1>
    <p><strong>Lorem Ipsum</strong> er rett og slett dummytekst fra og for trykkeindustrien. <a href="#">Lorem Ipsum</a> har vært bransjens standard for dummytekst helt siden 1500-tallet, da en ukjent boktrykker stokket en mengde bokstaver for å lage et prøveeksemplar av en bok. Lorem Ipsum har tålt tidens tann usedvanlig godt, og har i tillegg til å bestå gjennom fem århundrer også tålt spranget over til elektronisk typografi uten vesentlige endringer. Lorem Ipsum ble gjort allment kjent i 1960-årene ved lanseringen av Letraset-ark med avsnitt fra Lorem Ipsum, og senere med sideombrekkingsprogrammet Aldus PageMaker som tok i bruk nettopp Lorem Ipsum for dummytekst.</p>
    
    <h2>Hvor kommer det fra?</h2>
    <p>I motsetning til hva mange tror, er ikke Lorem Ipsum bare tilfeldig tekst. Dets røtter springer helt tilbake til et stykke klassisk latinsk litteratur fra 45 år f.kr., hvilket gjør det over 2000 år gammelt. Richard McClintock - professor i latin ved Hampden-Sydney College i Virginia, USA - slo opp flere av de mer obskure latinske ordene, consectetur, fra en del av Lorem Ipsum, og fant dets utvilsomme opprinnelse gjennom å studere bruken av disse ordene i klassisk litteratur. Lorem Ipsum kommer fra seksjon 1.10.32 og 1.10.33 i "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) av Cicero, skrevet i år 45 f.kr. Boken er en avhandling om teorier rundt etikk, og var veldig populær under renessansen. Den første linjen av Lorem Ipsum, "Lorem Ipsum dolor sit amet...", er hentet fra en linje i seksjon 1.10.32.</p>
    
    <h3>Lister</h3>
    <p>I tredje nivå ønsker vi å se på punktlister:</p>
    <ul>
        <li>Første punkt</li>
        <li>Andre punkt kommer etter første</li>
        <li>Siste punkt</li>
    </ul>
    <p>Og selvsagt nummerlister:</p>
    <ol>
        <li>Dette punktet er nummerert</li>
        <li>Og dette også</li>
        <li>Her er siste punkt</li>
    </ol>
    
    <h3>Tabeller</h3>
    
    <p>Her vises en tabell eller to:</p>
    
    <table>
        <thead>
            <tr>
                <th>Første kolonne</th>
                <th>Andre kolonne</th>
                <th>Tredje kolonne</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1.1</td>
                <td>1.2</td>
                <td>1.3</td>
            </tr>
        </tbody>
    </table>
    
    <p>Og etter tabellen bør det være fornuftig med luft.</p>
</div>
@stop
