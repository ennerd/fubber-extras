<?php
    $items = [];
    $items = \Fubber\Hooks::filter('Theme.Header.Nav', $items, $state);
    if( sizeof($items) == 0) $items[] = '<a href="#test">Theme.Header.Nav Example Item</a>';
?><!-- theme/parts/header/nav -->
<div class='nav'>
    <ul>
        <?php foreach($items as $item) { ?>
            <li><?=$item; ?></li>
        <?php } ?>
    </ul>
</div>
<!-- /theme/parts/header/nav -->
