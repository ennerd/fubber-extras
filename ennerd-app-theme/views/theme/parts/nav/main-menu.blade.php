<?php
    use function Fubber\trans;
    $user = User::getCurrent($state);
    
    $items = [];
    $items = \Fubber\Hooks::filter('Theme.Sidebar.Nav', $items, $state);
    if(sizeof(Fubber\Hooks::filter('Theme.ControlPanel.Icons', [], $state)) > 0) {
        $items[] = "<a href='/admin/'><i class='fas fa-wrench'></i>" . trans("Control Panel") . "</a>";
    }
?><!-- theme.parts.nav.main-menu -->
<ul class='vertical dropdown'>
    <?php foreach($items as $item) { ?>
    <li><?=$item; ?></li>
    <?php } ?>
    
</ul>
<!-- /theme.parts.nav.main-menu -->
