<!-- theme.parts.header -->
<header class='theme-header'>
@include('theme/parts/header/logo')
<div class='flex-spacer'></div>
@include('theme/parts/header/nav')
</header>
<!-- /theme.parts.header -->
