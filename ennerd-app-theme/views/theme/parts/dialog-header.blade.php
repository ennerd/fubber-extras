<!-- theme.parts.header -->
<header class='theme-header'>
    <div class="title"><h1>@yield('title')</h1></div>
    <div class='flex-spacer'></div>
    <div class="window-close"><i class="fas fa-times"></i></div>
</header>
<!-- /theme.parts.header -->
