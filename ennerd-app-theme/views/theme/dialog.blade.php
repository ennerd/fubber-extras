@extends('theme/base')
@section('body')
<div class='theme-body dialog'>
@include('theme/parts/dialog-header')
<div class='nav-and-main'>
@include('theme/parts/main')
</div>
@include('theme/parts/footer')
</div>
<script>
$(document.body).on('click', '.window-close', function() {
    Theme.Dialog.close();
});
</script>
@stop
