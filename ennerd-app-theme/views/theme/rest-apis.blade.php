<?php
use function Fubber\trans;
?>@extends('theme/default')
@section('title', trans('REST API documentation'))
@section('main')
<div class='padding'>
    <h1>{{trans("REST APIs and RPC endpoints")}}</h1>
    <?php foreach($apis as $apiSet) { ?>
        <?php if (isset($apiSet['#'])) { ?>
        <h1>{{$apiSet['#']}}</h1>
        <?php }?>
        <?php foreach($apiSet as $k => $api) { ?>
            <?php if ($k === '#') continue; ?>
            <h3>{{$api['title'] ?? $api['name'] ?? trans('Unnamed API')}}</h3>
            <?php if (isset($api['note'])) { ?>
            <p><em>{{$api['note']}}</em></p>
            <?php } ?>
            <table>
                <tr>
                    <th style='width: 150px'>{{trans("PATH:")}}</th>
                    <td style='font-family: monospace'><pre style='margin: 0.1em 0; padding: 0.2em; border: 1px dashed #ddd;'>{{$api['path']}}</pre></td>
                </tr>
                <tr>
                    <th style='vertical-align: top; '>{{trans("HTTP methods:")}}</th>
                    <td style='font-family: monospace'><pre style='margin: 0.1em 0; padding: 0.2em; border: 1px dashed #ddd;'><?=str_replace("|", "\n", $api['method']); ?></pre></td>
                </tr>
                <?php if (isset($api['response'])) { ?>
                <tr>
                    <th style='vertical-align: top; '>{{trans("Example response:")}}</th>
                    <td style='font-family: monospace'><pre style='margin: 0.1em 0; padding: 0.2em; border: 1px dashed #ddd;'><?=htmlspecialchars(json_encode($api['response'])); ?></pre></td>
                </tr>
                <?php } ?>
                <tr>
                    <th style='vertical-align: top; '>{{trans("Description:")}}</th>
                    <td style='font-family: monospace; white-space: break-spaces;'><div style='margin: 0.1em 0; padding: 0.2em; border: 1px dashed #ddd;'><?php
    $desc = trim($api['description'] ? $api['description'] : trans('Not described'));
    $desc = str_replace("\n\n", "<br><br>", $desc);
    $desc = str_replace("\n", " ", $desc);
    $desc = preg_replace_callback('|`+|m', function($matches) {
            static $isOdd = false;
            $isOdd = !$isOdd;
            if ($isOdd) {
                return '<pre style="display: inline; background-color: #eee;">';
            } else {
                return '</pre>';
            }
            return "(((".$matches[0]."";
        }, $desc);
    echo $desc;
?>
                    </div></td>
                </tr>
            </table>
        <?php } ?>    
        <hr>
    <?php } ?>
</div>
@stop
