<?php
namespace EnnerdAppTheme;

use ArrayObject;
use CodingErrorException;
use eftec\bladeone\BladeOne;
use Fubber\AssetManager\Asset;
use Fubber\Blade\BladeTemplateEngine;
use Fubber\NPM\NPMRepository;
use Fubber\NPM\Package;
use Fubber\Hooks;
use Fubber\Kernel;
use Fubber\Kernel\State;
use Fubber\LogicException;
use Fubber\NotFoundException;
use Fubber\NPM\RequestHandler;
use Psr\Http\Server\RequestHandlerInterface;

class ThemeController {

    public static ?NPMRepository $assets = null;
    protected static RequestHandlerInterface $assetServer;

    public static function load() {
        /*
        static::$assets = new NPMRepository(\dirname(\dirname(__DIR__)));
        static::$assetServer = new RequestHandler(static::$assets);
        */
    }

    public static function init() {
        /*
        $moduleRoot = realpath(__DIR__ . '/../../');

        $basePath = Kernel::$instance->env->baseRoot;
        $baseUrl = Kernel::$instance->env->baseUrl;

        if (!\str_starts_with($moduleRoot, $basePath)) {
            throw new LogicException("Can't serve assets when ".$moduleRoot." is outside of ".$basePath);
        }

        $trimPathLength = \strlen($basePath);

        $getUrlFor = function(string $path) use($baseUrl, $trimPathLength, $moduleRoot): string {
            if (!\str_starts_with($path, $moduleRoot)) {
                throw new LogicException("Asset path $path is not inside $basePath");
            }
            return $baseUrl . \substr($path, $trimPathLength);
        };

        echo "<pre>";
        $package = new NPMRepository($moduleRoot);
        $bundle = new Bundle($package, new Dependency('Dynatable', '*'));

        echo "Contents of $bundle\n";
        $map = [
        ];
        foreach ($bundle as $asset) {
*/
            /** @var Package */
/*
            $payload = $asset->payload;

            if ($payload->browser && \is_string($payload->browser)) {
                $main = $payload->browser;
            } else {
                $main = $payload->main ?? 'index';
            }

            $extension = \pathinfo($main, \PATHINFO_EXTENSION);
            if (!$extension) {
                $found = false;
                foreach (['.min.mjs', '.min.js', '.mjs', '.js'] as $extension) {
                    $path = $payload->getPath($main . $extension);
                    if (\is_file($path)) {
                        $found = true;
                        $main = $main . $extension;
                        break;
                    } 
                }
                if (!$found) {
                    $main = null;
                }
            }

            $files = [];
            foreach ($payload->files ?? [] as $file) {
                $pathLength = \strlen($payload->getPath());
                $path = $payload->getPath($file);
                if (\is_dir($path)) {
                    foreach (new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::CURRENT_AS_PATHNAME | RecursiveDirectoryIterator::SKIP_DOTS) as $path) {
                        if (\is_dir($path)) {
                            continue;
                        }
                        $files[] = \substr($path, $pathLength + 1);
                    }
                } else {
                    $files[] = \substr($path, $pathLength + 1);
                }
            }
            $entry = [
                'base_url' => $getUrlFor($asset->payload->getPath()),
                'main' => $main,
                'files' => $files,
                'references' => $bundle->getReferencesFor($asset),
            ];
            $map[$asset->name] = $entry;
        }
        print_r($map);
        die("ThemeController done");

        $dumpTree = function(Bundle $bundle, int $indent = 0) use (&$dumpTree) {
            foreach ($bundle as $asset) {
            }
        };
        foreach ($bundle as $id => $asset) {
            echo " - $id\n";
        }
        die("ThemeController");
*/
        /*
        self::$assets->add(
            new Asset('jquery', '3.2.1', new ResourceSet(headJS: 'assets/jquery-3.2.1.min.js')),
            new Asset('jquery', '2.2.1', new ResourceSet(headJS: ['/app/assets/jquery-2.2.1.min.js'])),
            new Asset('jquery-balloon', '1.1.2', new ResourceSet(headJS: '/app/assets/jquery-balloon.min.js'), new Dependency('jquery')),
            new Asset('jquery-ui', '1.11.4',
                new ResourceSet(
                    headJS: "assets/jquery-ui-1.12.1.custom/jquery-ui.js",
                    headCSS: "assets/jquery-ui-1.12.1.custom/jquery-ui.min.css",
                ),                
                new Dependency('jquery')
            ),
            new Asset('inputmask', '4.0.0-beta44',
                new ResourceSet(
                    headJS: "assets/Inputmask-4.x/dist/min/jquery.inputmask.bundle.min.js",
                    packageJson: "assets/Inputmask-4.x/package.json",
                )
            ),
            new PackageJsonAsset($basePath . '/assets/CodeSeven-toastr-61c48a6/package.json'),
            new PackageJsonAsset($basePath . '/assets/')
            new Asset('toastr', '2.1.4',
                new ResourceSet(
                    packageJson: "assets/CodeSeven-toastr-61c48a6/package.json",
                    headJS: "assets/CodeSeven-toastr-61c48a6/build/toastr.min.js",
                    headCSS: "assets/CodeSeven-toastr-61c48a6/build/toastr.min.css"
                ),
                new Dependency('jquery', '>=1.12.0'),
            )
        );
        */

        State::$onConstructed->listen(function(State $state) {
            $state->stylesheets = [];
            $state->scripts = [];
            $state->inline_scripts = [];
            $state->dialog = false;
            $state->pick = false;
        });

        BladeTemplateEngine::$onBladeOneConstructed->listen(static::extendBladeOneTemplates(...));

        /**
         * Allow code to dynamically add scripts and stylesheets on demand to the template:
         *
         * For example:
         * $style->scripts['jquery'] = '/app/assets/jquery.min.js'
         */
        Hooks::listen('State', function($state) {

            if (!isset($state->inline_scripts)) {
                $state->inline_scripts = [];
            }
            if (!isset($state->scripts)) {
                $state->scripts = [];
            }
            if (!isset($state->stylesheets)) {
                $state->stylesheets = [];
            }
        });
        Hooks::listen('Kernel.Ready', function() {
            if(!class_exists(\Theme::class)) {
                class_alias( Theme::class, \Theme::class, true );
            }
        });    
        Hooks::listen(\Fubber\Forms\Form::class.'.Method.blobform', [FormFields\BlobForm::class, 'render']);
    }

    protected static function es6_asset(string $path) {
        $parts = explode("/", $path);

        $moduleName = $parts[0];

        $modules = self::$assets[$moduleName];
        if (\is_array($modules)) {
            foreach ($modules as $name => $asset) {
                return self::es6_serve_package($asset->payload);
            }
        } elseif ($modules instanceof Asset) {
            return self::es6_serve_package($modules->payload);
        } else {
            throw new NotFoundException("ES6 module ".$moduleName." not found");
        }
    }

    /**
     * Serve the package as an ES6 module
     * 
     * @param Package $package 
     * @return never 
     */
    protected static function es6_serve_package(Package $package) {
        $main = $package->browser ?? $package->module ?? $package->main ?? 'index';

        $ext = \pathinfo($main, \PATHINFO_EXTENSION);
        if (!$ext) {
            foreach (['.min.mjs', '.mjs', '.min.js', '.js'] as $ext) {
                if (\is_file($path = $package->getPath($main . $ext))) {
                    goto found;
                }
            }
        } else {
            if (\is_file($path = $package->getPath($main))) {
                goto found;
            }
        }

        throw new LogicException("Unable to determine the main file for ".$package->name);

        found:

        // determine if this is an mjs file already
        if ($ext === 'mjs') {
            die("mjs");
        } else {
            die("must make mjs of $path");
        }
    }
    
    public static function routes() {
        return [
            //"es6-asset GET /theme/assets/es6/{path}" => static::$assetServer,
            "home GET /" => [ static::class, 'home' ],
            "admin GET /admin/" => [ static::class, 'admin' ],
            "GET /api/tablesource/{name}/" => [ static::class, 'api_tablesource' ],
            "GET /rest-api/" => [ static::class, 'rest_api_documentation', ],
            ];
    }
    
    public static function rest_api_documentation(State $state) {
        $restApis = \Fubber\Hooks::dispatch('Theme.RestApis', $state);
        return $state->view('theme/rest-apis', ['apis' => $restApis]);
    }
    
    public static function home(State $state) {
        $state->response->noCache();
        $result = $state->view('theme/home');
        if (isset($_GET['frode'])) {
//    var_dump($result);die("RESULT");
}
        return $result;
    }
    
    public static function admin(State $state) {
        if(!\User::getCurrent($state)) throw new \Exceptions\AccessDeniedException("Access Denied");
        return $state->view('theme/control-panel');
    }

    public static function api_tablesource(State $state, string $name) {
        $state->json = true;
        $state->response->noCache();
        $data = Hooks::dispatchToFirst('Theme.TableSource.'.$name, $state);
        $data = Hooks::filter('Theme.TableSource', $data, $state, $name);

        if ($data === NULL) {
            throw new \Exceptions\NotFoundException("Table not found");
        }
        $sendTheRecords = false;
        $totalRecordCount = $data['records']->count();

        if(isset($state->get['sorts'])) {
            $sort = key($state->get['sorts']);
            if (!isset($data['cols'][$sort])) {
                throw new CodingErrorException("Column $sort is not defined");
            }
            $data['records']->order($sort, $state->get['sorts'][$sort] < 0);
        }

        if(isset($state->get['page'])) {
            // This is a data request, so we'll send data
            $data['records']->limit(intval($state->get['perPage']), intval($state->get['offset']));
            $sendTheRecords = true;
            //var_dump(intval($state->get['perPage']), intval($state->get['offset']));
        }

        if(isset($state->get['queries'])) {
            foreach($state->get['queries'] as $k => $v) {
                if($k == 'search') {
                    $sqls = [];
                    $vals = [];
                    foreach($data['cols'] as $col => $spec) {
//                        if(!in_array($col, $data['records']->getCols()))
                        if(!in_array($col, $data['records']->getColumnNames()))
                            continue;
                        $sqls[] = $col." LIKE ?";
                        $vals[] = '%'.trim(str_replace("*", "%", $v), '%').'%';
                    }
                    
                    $data['records']->complexWhere(implode(" OR ", $sqls), $vals);
                }
            }
        }
        $data['serialized'] = serialize($data['records']);
        $data['queryRecordCount'] = $data['records']->count();
        $data['totalRecordCount'] = $totalRecordCount;

        $modelClass = $data['records']->getModelClass();
        $meta = \Theme\Meta\Meta::create($state, $modelClass);
        if (!$meta) {
            throw new \Exceptions\CodingErrorException("Can't use joins without meta");
        }
        if (!$sendTheRecords) {
            $data['records'] = [];
        } else {
            // Does any columns have a filter specified?
            $filter = new \Theme\Meta\Filter($state);
            
            $records = [];
            $unfiltered = [];
            foreach($data['records'] as $record) {
                $newRecord = $originalRecord = $record->jsonSerialize();
                $unfiltered[] = $newRecord;
                foreach($data['cols'] as $col => $spec) {
                    if(isset($spec['filter'])) {
                        //var_dump($spec['filter']);die();
                        //Perhaps the filter is a super closure
                        if(!is_callable($spec['filter']) && is_subclass_of($spec['filter'], \Fubber\Table\IBasicTable::class)) {
                            $that = $spec['filter'];
                            $that = $that::load($newRecord[$col]);
                            $that = $that->jsonSerialize();
                            $newRecord[$col] = $that['_string'];
                        } else {
                            $newRecord[$col] = $spec['filter']($newRecord[$col], $record);
                        }
                    } elseif (isset($spec['value'])) {
                        if (is_callable($spec['value'])) {
                            $newRecord[$col] = ($spec['value'])($record, $state);
                        } else {
                            throw (new CodingErrorException("The column value property must provide a function(State, Row): string {} callback."));
                        }
                    } else {
                        $newRecord[$col] = $filter->html($record, $col);
                    }
                }
                $newRecord['_raw'] = $originalRecord;
                if(!isset($newRecord['id']) && isset($record->id))
                    $newRecord['id'] = $record->id;
                $newRecord['_raw'] = $originalRecord;
                $records[] = $newRecord;
            }
            
            if(!empty($data['joins'])) {
                foreach($data['joins'] as $join) {
                    $col = $meta->col($join);
                    $thatClass = $col->getType();
                    $vals = array_column($unfiltered, $join);
                    $users = $thatClass::loadMany($vals);
                    foreach($records as $k => &$record) {
                        $record['_'.$join] = isset($unfiltered[$k][$join]) && isset($users[$unfiltered[$k][$join]]) ? $users[$unfiltered[$k][$join]] : null;
                        if($record['_'.$join]) {
                            $record['_'.$join] = $record['_'.$join]->jsonSerialize();
                        }
                    }
                }
            }

            $data['records'] = $records;
        }

        if(!empty($data['cols'])) {
            /**
             * We must prepare the column title descriptions and send them with captions,
             * alignment and column width.
             */
            foreach($data['cols'] as $key => &$col) {
                // Heuristics to determine if table is new style or old style
                if (!empty($col['meta'])) {

                }
                $doit = false;
                if($col[0]===null) {
                    // format is [null, 'left', 100]
                    array_shift($col);
                    $doit = true;
                } else if(!isset($col[1]) || is_int($col[1])) {
                    // format is ['left'] or ['left', 123],
                    $doit = true;
                } else if(($col[0]=='left' || $col[0]=='right') && ($meta && !$meta->col($col[0], false))) {
                    // format is ['left/right', ]
                    $doit = true;
                }
                if($doit) {
                    if (isset($col['caption'])) {
                        array_unshift($col, $col['caption']);
                        unset($col['caption'], $col['value']);
                    } else {
                        array_unshift($col, $meta->col($key)->getCaption());
                    }
                }
            }
        }
        //echo "<pre>";print_r($data);
        //die("DON\n");
        return $state->json($data);
    }
    
    
    public static function extendBladeOneTemplates(BladeOne $compiler) {
        /**
         * Translatable raw string @t("Some string {var}", [ "var" => "example"])
         */
        $compiler->directive('t', function($args) {
            return '<?=(string) new \Fubber\I18n\Translatable('.$args.'); ?>';
        });
        
        /**
         * Translatable JS string @tjs("Some string")
         */
        $compiler->directive('tjs', function($args) {
            return '<?=\json_encode(new \Fubber\I18n\Translatable('.$args.')); ?>';
        });

        $compiler->directive('renderview', function($args) {
            return '<?=\Theme\Meta\Render::view($state,'.$args.'); ?>';
        });
        $compiler->directive('renderform', function($args) {
            return '<?=\Theme\Meta\Render::form($state,'.$args.'); ?>';
        });
        $compiler->directive('form', function($args) {
            return '<?php '.
                'if(isset($_form) && $_form)'.
                    'trigger_error("Already within a @form");'.
                '$_void = [ '.$args.' ];'.
                '$_form = $_void[0];'.
                'echo $_form->begin( $_void[1] ?? []);'.
                '?>';
        });
        $compiler->directive('endform', function() {
            return '<?php if(!isset($_form) || !$_form) trigger_error("No matching start @form"); else { echo $_form->end(); $_form = null; } ?>';
        } );
        $compiler->directive('field', function($args) {
            // $_t = function(){return func_get_args();};list($_name, $_type, $_label) = $_t('.$args.');
            $res = '<?php if(!isset($_form) || !$_form) trigger_error("Not inside a @form"); else { $_void = [ ' . $args . ']; $_name = $_void[0]; $_type = $_void[1]; $_label = $_void[2]; $_attrs = $_void[3] ?? []; ?>';
            $res .= <<<'EOT'
<div id='<?=$_name; ?>-wrap' class='<?=$_type;?> field <?php if($_form->hasError($_name)) echo 'invalid'; ?>' data-error="<?=$_form->hasError($_name);?>">
    <div class='label-wrap'><?=$_form->label($_name, $_label); ?></div>
    <div class='field-wrap'><?=$_form->$_type($_name, attrs: $_attrs); ?></div>
    <div class='error-wrap'><?=$_form->hasError($_name); ?></div>
</div>
EOT;
            return $res.'<?php } ?>';
        });
        $compiler->share('base_url', Kernel::$instance->env->baseUrl);
    }

}
