<?php
namespace EnnerdAppTheme;

use Fubber\Kernel\State;

class Theme {
    
    public static function breadcrumb($name, $title, $parentName=null) {
        
    }
    
    public static function icon(State $state, $title, $imageTag, $attributes=[]) {
        return '<div class="icon" '.static::arrayToAttributes($attributes).'>
            <div class="figure">
                '.$imageTag.'
            </div>
            <div class="label">
                '.htmlspecialchars($title).'
            </div></div>';
    }
    
    protected static function arrayToAttributes($attrs) {
        $res = [];
        foreach($attrs as $k => $v) {
            $res[] = $k."=\"".htmlspecialchars($v)."\"";
        }
        return implode(" ", $res);
    }
}