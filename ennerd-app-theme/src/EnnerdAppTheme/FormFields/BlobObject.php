<?php
namespace EnnerdAppTheme\FormFields;

use Fubber\Kernel\State;

class BlobObject {
    public static function create(State $state, $value, $meta) {
        $theClass = new class($state, $value) extends BlobObject {
            protected static $_meta;
        };
        $theClass::$_meta = $meta;
        return $theClass;
    }

    protected static $_meta;
    protected $_state;
    protected $_object;
    public function __construct($state, $object) {
        $this->_state = $state;
        $this->_object = (array) $object;
    }
    public static function meta(State $state) {
        return static::get_meta();
    }
    protected static function get_meta() {
        return static::$_meta;
    }
    public function __get($name) {
        $meta = \Theme\Meta\Meta::create($this->_state, $this);
        try {
            $col = $meta->col($name);
            if(isset($this->_object[$name]))
                return $this->_object[$name];
        } catch(\Exception $e) {
            throw new \Exception("BlobObject does not have the field '$name' defined in meta.");
        }
        return null;
    }
    public function __set($name, $value) {
        $meta = \Theme\Meta\Meta::create($this->_state, $this);
        try {
            $col = $meta->col($name);
            return $this->_object[$name] = $value;
        } catch(\Exception $e) {
            throw new \Exception("BlobObject does not have the field '$name' defined in meta.");
        }
        return null;
    }
}