<?php
namespace EnnerdAppTheme\FormFields;

use Fubber\Kernel\State;

/**
 * Creates a form field object which can be rendered and embedded into forms.
 */
class BlobForm extends \Fubber\Forms\Form  {
    
    // Hook for Form - called with ($state, $form, $name, $attributes)
    public static function render(State $state, $form, $name, $attributes) {
        //echo "<pre>";
        //var_dump(debug_backtrace());die();
        //$options = $form->getOptions($name);
        return new static($state, $form, $name, $attributes);
    }
    
    protected $state;
    protected $parentForm;
    protected $fieldName;
    protected $attributes;
    protected $object;
    protected $def;
    
    public function __construct(State $state, $parentForm, $fieldName, $attributes) {
        $this->state = $state;
        $this->parentForm = $parentForm;
        $this->fieldName = $fieldName;
        $this->attributes = $attributes;
        
        // Check that we have the form options we need to render this blob as a sub form
        $options = $parentForm->getOptions($fieldName);
        if(!isset($options['meta']))
            throw new \Exceptions\CodingErrorException("form_options must include a 'meta' key that contains the meta information for the fields that will be stored in the blob.");
        if(!isset($options['def']))
            throw new \Exceptions\CodingErrorException("form_options must include a 'def' key that describes the form structure.");

        $this->def = $options['def'];

        /**
         * Dynamically create a static class and provide it with the meta data we
         * want.
         */
        $value = $this->parentForm->$fieldName;
        if(!is_string($value)) throw new \CodingErrorException("Expected value of property $fieldName to be a serialized string.");
        if(trim($value)=='') $value = [];
        else $value = unserialize($value);
        
        $this->object = BlobObject::create($state, $value, $options['meta']);

        parent::__construct($state, $this->fieldName.'-form', []);
        /**
         * Extract which fields are being used in the form def.
         */
        $parseFields = function($def) use (&$parseFields) {
            $cols = [];
            foreach($def as $item) {
                if(is_array($item) && isset($item[0]) && $item[0]=='field') {
                    $cols[] = $item[1];
                } else if(is_array($item)) {
                    $cols = array_merge($cols, $parseFields($item));
                }
            }
            return $cols;
        };
        $meta = \Theme\Meta\Meta::create($this->state, $this->object);
        $cols = $parseFields($this->def);
        foreach($cols as $col) {
            $this->addField($col.($meta->col($col)->isMultiple() ? '[]' : ''), $this->object->$col, $meta->col($col)->getFormOptions());
        }
    }
    
    public function getName($name, $inc=false) {
        $parentName = $this->parentForm->getName($this->fieldName);
        return $parentName.'['.$name.']';
    }
    
    public function getId($name, $inc=false) {
        $parentId = $this->parentForm->getName($this->fieldName);
        return $parentId.'_'.$name;
    }
    
    public function __toString() {
        // Render the sub form
        try {
            $renderer = new \Theme\Meta\Render($this->state, $this->def, $this->object, $this);
            return $renderer->render(true);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
    }
}