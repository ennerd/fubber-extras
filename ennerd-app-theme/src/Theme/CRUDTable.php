<?php
namespace Theme;

use Fubber\FubberAPI;
use Fubber\Kernel\State;
use Fubber\Table\IBasicTable;

/**
 * @template T
 * @package Theme
 * @inherits \Fubber\Table<T>
 */
class CRUDTable extends \Fubber\Table {
    use FubberAPI;
    
    public static function canList(State $state) {
        return $state->access->isSuperAdmin();
    }
    public static function canCreate(State $state) {
        return $state->access->isSuperAdmin();
    }
    public function canEdit(State $state) {
        return static::canCreate($state);
    }
    public function canDelete(State $state) {
        return $this->canEdit($state);
    }
    public function canView(State $state) {
        return $this->canEdit($state);
    }
}