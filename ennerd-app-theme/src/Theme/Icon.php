<?php
namespace Theme;

class Icon {
    public $title, $onclick, $icon;
    public $weight = 0;
    
    public function __construct($label, $onclick=null, $icon=null) {
        $this->label = $label;
        $this->onclick = $onclick;
        $this->icon = $icon;
    }
    
    public function url($url) {
        $this->url = $url;
        return $this;
    }
    
    public function icon($name) {
        $this->icon = $name;
        return $this;
    }
    
    public function image($url) {
        $this->image = $url;
    }
    
    public function addProperty($name, $value) {
        $this->properties[$name] = $value;
    }
    
    public function __toString() {
        return '<div class="icon" onclick="'.htmlspecialchars($this->onclick).'">
    <div class="figure">'.($this->icon ? $this->icon : '<i class="fas fa-circle"></i>').'</div>
    <div class="label">'.htmlspecialchars($this->label).'</div>
</div>';
    }
}
