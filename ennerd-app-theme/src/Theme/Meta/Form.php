<?php
namespace Theme\Meta;

use Fubber\Kernel\State;

class Form extends \Fubber\Forms\TableForm {
    protected $meta;
    
    public function __construct(State $state, \Fubber\Table\IBasicTable $row) {
        $this->meta = Meta::create($state, $row);
        $cols = [];
        foreach($this->meta->getCols() as $key => $col) {
            $options = $col->getFormOptions();
            if($options)
                $cols[] = [$key, $options];
            else
                $cols[] = $key;
        }
        parent::__construct($state, $row, $cols);
    }
}
