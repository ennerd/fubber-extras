<?php
namespace Theme\Meta;

use Fubber\Kernel\State;

use function Fubber\trans;

class Errors extends \Fubber\Util\Errors {
    
    protected $_state, $_meta;
    
    /**
     * $meta must be an instance of Meta - if $object does not provide the meta() method
     */
    public function __construct(State $state, $object, Meta $meta=null) {
        if(!is_object($object) && !$meta) {
            throw new \TypeError('$object must be an object instance, or $meta must be provided.');
        }
        if($meta) $this->_meta = $meta;
        else $this->_meta = Meta::create($state, $object);
        
        $this->_state = $state;

        parent::__construct($object);
    }
    
    public function isInvalid(): ?array {
        // Parse meta column rules, and invoke these. Other validations may have already happened if
        // a validation method has already been called.
        $cols = $this->meta->getCols();
        foreach($cols as $name => $col) {
            $rules = $col->getRules();
            $subErrors = new parent(['value' => $this->getValue($name)]);
            $method = array_shift($rules);
            if(method_exists($subErrors, $method)) {
                $args = [];
                // Extract numeric values
                $i = 0;
                while(isset($rules[0]))
                    $args[] = array_shift($rules);
                call_user_func_array([$subErrors, $method], $args);
                $subErrors = $subErrors->isInvalid();
                if($subErrors) {
                    if(isset($rules['error']))
                        $this->addError($name, $rules['error']);
                    else
                        $this->addError($name, trans($subErrors[$name]));
                }
            }
        }
        return parent::isInvalid();
    }
}
