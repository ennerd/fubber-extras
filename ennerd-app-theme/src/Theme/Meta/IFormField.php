<?php
namespace Theme\Meta;

interface IFormField {
    public static function renderFormField(\Fubber\Forms\Form $form, $fieldName);
}