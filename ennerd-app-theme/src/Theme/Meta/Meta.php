<?php
namespace Theme\Meta;

use Fubber\Kernel\State;

/**
 * Some Fubber\Table objects implement the meta()-method. This class makes the data returned by the meta method more easily
 * accessible.
 * 
 * Structure of meta:
 * 
 *  [
 *      'names' => [
 *          'singular' => 'User',
 *          'plural' => 'Users',
 *      ],
 *      'description' => 'User accounts',
 *      'column_definition' => [
 *          // USED WHENEVER OTHER CLASSES REFERENCE THIS CLASS AS A COLUMN
 *          // Defaults to [ 'caption' => <singular name>, 'type' => <class name> ]
 *          ],
 *      'cols' => [
 * 
 *          // FOREIGN KEY EXAMPLES:
 * 
 *          'friend1_id' => User::class,                                        The entire def will be fetched from column_def of the referenced class. Including caption.
 * 
 *          'friend2_id' => [
 *              'caption' => 'Friend number 2',                                 // Field caption, used in tables, forms and views
 *              'type' => 'text',                                               // Standard text field, typically <input type=text>. User must type in the user ID.
 *              ],
 *
 *          'friend3_id' => [
 *              'caption' => 'Friend number 3',                                 // Caption
 *              'type' => User::class,                                          // Since the Form class supports field types which are class names, this works.
 *              ],
 * 
 *          'friend4_id' => [
 *              'caption' => 'Friend 4',                                        // This caption overrides the referenced caption
 *              'extends' => User::class,                                       // the entire definition will be fetched from column_def in the referenced class meta
 *              ],
 * 
 *          // NORMAL FIELD EXAMPLES
 * 
 *          'tags' => [
 *              'caption' => 'Tags',
 *              'type' => 'text',
 *              'multiple' => true,                                             // This basically says that the ->tags property is an array. Storage of this array must be handled.
 *              ],
 * 
 *          'account_type' => [
 *              'caption' => 'Account Type'
 *              'type' => 'select',                                             // Form field type that requires options
 *              'form_options' => [                                             // Form options are used by some form fields
 *                  'admin' => 'Administrator Account',
 *                  'user' => 'User',
 *                  'guest' => 'Guest',
 *                  ],
 *              ],
 * 
 *          'some_property' => [
 *              'caption' => 'Some Property',
 *              'type' => 'text',
 *              'readonly' => true,
 *              ],
 *          
 *          // PREDEFINED FIELD EXAMPLES
 * 
 *          'created_date' => 'created_date',                                   // See source of ColumnDefs::created_date
 *          
 *          'created_date_2' => [ ColumnDefs::class, 'created_date'],           // Same as above. Column defs can be methods which will be invoked with $state
 * 
 *          'something_else' => 'whatever_you_want',                              // Hook Theme\Meta\ColumnDefs.Def.whatever_you_want 
 * 
 *          // VALIDATION RULE EXAMPLES
 * 
 *          'overvalidated' => [
 *              'caption' => 'This will never validate',
 *              'type' => 'text',
 *              'rules' => [
 *                  ['required'],                                               // Field is required
 *                  ['required', 'error' => trans("This is required")],     // Custom error message, overrides the default
 *                  ['className', User::class],                                 // Field must contain a class name that is_a($value, User::class)
 *                  ['slug'],                                                   // See Fubber\Util\Errors::slug
 *                  ['password'],
 *                  ['minLen', 5],
 *                  ['minVal', 5],
 *                  ['maxVal', 5],
 *                  ['maxLen', 5],
 *                  ['notNull'],
 *                  ['email'],
 *                  ['oneOf'],
 *                  ['url'],
 *                  ['integer'],                                                // Field must be an integer
 *                  ['float'],
 *                  ['datetime'],                                               // Must be format 'Y-m-d H:i:s'
 *                  ['foreignKey', User::class],                                // Must exist in User::class-table
 *                  ['unique', User::class],                                    // Must be unique within the class
 *                  ['groupedUnique', User::class, 'user_type'],                // Value must be unique among other rows with same user_type
 *                  // Field is required, with custom error message
 *                  
 * 
 *                  // Custom validator
 *                  function($v) use ($state) { if($v == '2') return trans("The value 2 is illegal"); },
 *                  
 *                  //
 * 
 *                  ['required', 'You must provide an overvalidated value'],
 *                  ],
 * 
 *              ],
 * 
 * 
 *          ],
 *  ]
 * 
 * 
 */

class Meta {
    protected static $sharedCache;
    protected $meta;
    protected $sub;
    protected $cache = [];
    protected static $extra = [];
    protected static $kernel;
    
    /**
     * Get Meta information about a class.
     *
     * @param State $state
     * @param [type] $source
     * @param boolean $throws
     * @return static|null
     */
    public static function create(State $state, object|string $source, $throws=true): ?static {
        if(!$source) {
            throw new \TypeError('Argument 2 must be a class name or an object instance.');
        }

        $instance = null;
        if(is_object($source)) {
            $instance = $source;
            $source = get_class($source);
        }

        $key = 'meta:'.$source;

        if(array_key_exists($key, $state->cache)) {
            if ($throws && $state->cache[$key] === null) {
                throw new MissingMetaException("The class '".$source.'" must implement the meta() method to use the "'.static::class.'" class.');
            }
            return $state->cache[$key];
        }

        try {
            return $state->cache[$key] = new static($state, $source);
        } catch (MissingMetaException $e) {
            $state->cache[$key] = null;
            if($throws) {
                throw new MissingMetaException("The class '".$source.'" must implement the meta() method to use the "'.static::class.'" class.');
            }
        }

        if ($instance !== null) {
            var_dump($instance);die();
        }

        return null;
    }
    
    /**
     * Add any metadata to any class, by manipulating the returned array. For example
     * 
     * $metadata = &Meta::extraMetaData(\User::class);
     * $metadata += [
     *      'names' => [
     *          'plural' => 'Users',
     *          'singular' => 'User',
     *      ]
     * ];
     * 
     */
    public static function &extraMetaData(string $className) {
        if(!isset(static::$extra[$className])) {
            static::$extra[$className] = [];
        }
        return static::$extra[$className];        
    }
    
    protected function __construct(State $state, string $source) {
        if (!static::$kernel) {
            static::$kernel = \Fubber\Kernel::init();
        }
        if (!method_exists($source, 'meta')) {
            throw new MissingMetaException("The class '".$source.'" must implement the meta() method to use the "'.static::class.'" class.');
        }
        $this->state = $state;
        $this->source = $source;
        if(isset(static::$extra[$this->source])) {
            $this->meta = array_merge_recursive($source::meta($state), static::$extra[$this->source]);
        } else {
            $this->meta = $source::meta($state);
        }
    }

    public function addColumn(string $name, array|Column $definition) {
        if (is_array($definition)) {
            $definition = new Column($this->state, $definition);
        }
    }
    
    public function getClassName() {
        return $this->source;
    }
    
    public function getPluralName() {
        if(!empty($this->meta['names']['plural'])) {
            return $this->meta['names']['plural'];
        }
        return 'Error: Meta does not provide ["names"]["plural"].';
    }
    
    public function getSingularName() {
        if(!empty($this->meta['names']['singular'])) {
            return $this->meta['names']['singular'];
        }
        return 'Error: Meta does not provide ["names"]["singular"].';
    }
    
    /**
     * Get the column definition for when this class is being
     * referenced from another form.
     *
     * @return array
     */
    public function getColumnDefinition() {
        if(!empty($this->meta['column_definition'])) {
            return $this->meta['column_definition'];
        }
            
        return [
            'caption' => $this->getSingularName(),
            'type' => $this->getClassName(),
            ];
    }
    
    public function getDescription() {
        if(empty($this->meta['description']))
            return null;
        return $this->meta['description'];
    }
    
    /**
     * Get the URL for a named item
     *
     * @param string $name
     * @param mixed ...$arguments
     */
    public function getRoute(string $name) {
        if(empty($this->meta['routes'][$name]))
            return null;
        $args = func_get_args();
        $args[0] = $this->meta['routes'][$name];
        return static::$kernel->http->router->url(...$args);
    }

    public function getCols() {
        $res = [];
        foreach($this->meta['cols'] as $key => $spec) {
            $res[$key] = $this->col($key);
        }
        return $res;
    }
    
    public function col($col, $throws=true): ?Column {
        if(!empty($this->cache['col'.$col]))
            return $this->cache['col'.$col];
        else if(isset($this->meta['cols'][$col]))
            return $this->cache['col'.$col] = new Column($this->state, $this->meta['cols'][$col]);
        else
            $this->cache['col'.$col] = null;
            
        if($throws) {
//            echo "<pre>";var_dump($this->meta['cols']);die();
//            echo "<pre>";var_dump(debug_backtrace(false));die();
            throw new \Exceptions\CodingErrorException("There is no column definition for the column \"$col\" in the class \"".$this->source."\".");
        }
        return null;
    }
    
    public function row(\Fubber\Table\IBasicTable $row): Row {
        return new Row($this->state, $this, $row);
    }
    
    public function getMetaData(): array {
        return $this->meta;
    }
}
