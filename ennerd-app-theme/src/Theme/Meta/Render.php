<?php
namespace Theme\Meta;

use Fubber\CodingErrorException;
use Fubber\Forms\Form;
use Fubber\Kernel\State;
use Fubber\LogicException;
use Fubber\Table;

/**
view defs:

[
<spec>              [ <item>..., 'attrs' => null ]
<item>:             <function>, <field>, <cols>, <form_checkbox>, <toolbar>, <accordion>, <buttons>
<function>:         [ function($args) {}[, $args...] ],
<field>:            [ 'field', 'fieldName'[, 'readonly'=>true][, 'filter'=>function($row,$fieldName)] ],
<cols>:             [ 'cols', [<spec>...] ]
<form_checkbox>:    [ 'form_checkbox', 'fieldName' ]
<toolbar>           [ <item>... ]
<accordion>         [ 'accordion', [ <accordionPane>... ] ]
<accordionPane>     [ 'title' => <string>, 'active' => <boolean|null> ] + [ <item>... ]
<button>            [ 'submit', <string>, 'attrs' => [] ]
<submit>            [ 'button', <string>, 'attrs' => [] ]
<buttons>           [ 'buttons', [ <item>... ] ]
<html>              [ 'html', <string> ]
]
 */
class Render {

    public static function view(State $state, array $spec, Table $row=null) {
        $renderer = new static($state, $spec, $row);
        $result = $renderer->render();
        return $state->plain($result);
    }
    
    public static function form(State $state, Form $form, $spec, $row) {
        $renderer = new static($state, $spec, $row, $form);
        return $state->plain($renderer->render());
    }

    protected State $state;
    protected array $spec;
    protected Table $row;
    protected ?Form $form = null;
    protected Meta $meta;
    protected Filter $filter;


    public function __construct(State $state, array $spec, object $row, Form $form=null) {
        if (!self::isSpec($spec)) {
            throw new CodingErrorException("Render spec is invalid: '".json_encode($spec)."'");
        }
        if(!method_exists($row, 'meta')) {
            throw new \Exceptions\CodingErrorException("Model must have meta() method");
        }
        $this->state = $state;
        $this->spec = $spec;
        $this->row = $row;
        $this->form = $form;
        $this->meta = Meta::create($state, $row);
        $this->filter = new Filter($state, $row);
        $this->state->stylesheets[static::class.'-rendered'] = '/app/extra/ennerd-app-theme/assets/Render/rendered.css';
        $this->state->scripts[static::class.'-rendered'] = '/app/extra/ennerd-app-theme/assets/Render/rendered.js';
    }
    
    /**
     * Renders a <div> with elements defined via a structured array.
     *
     * @param boolean $_removeFormTag
     * @return void
     */
    public function render($_removeFormTag=false) {
        try {
            $res = '<div class="rendered '.($_removeFormTag ? '' : 'basicform f-form').'">';
            //$res .= '<pre>'.print_r($this->spec, true).'</pre>';
            if(!$_removeFormTag && $this->form) {
                $res .= $this->form->begin(['autocomplete' => 'off', 'role' => 'presentation']);
                $res .= '<input type="password" style="width: 0;height: 0; visibility: hidden;position:absolute;left:0;top:0;"/>';
            }

            /**
             * The following tests allows you to have a single element instead of a list of elements
             */
            /*
            if(!is_array($this->spec[0])) {
                $res .= $this->item($this->spec);
            } else foreach($this->spec as $item) {
                $res .= $this->item($item);
            }
            */
            for ($i = 0; isset($this->spec[$i]); $i++) {
                if (!is_array($this->spec[$i])) {
                    throw new LogicException("Each item must be an item descriptor array in '".json_encode($this->spec)."'. Did you mean to call Render::item()?");
                }
                $res .= $this->item($this->spec[$i]);
            }
            if(!$_removeFormTag && $this->form) {
                $res .= $this->form->end();
            }
            $res .= '</div>';
        } catch (\Throwable $e) {
            throw $e;
            return "<div>Error: <strong>".htmlspecialchars($e->getMessage())."</strong> ".$e->getFile().":".$e->getLine()."</div>";
        }
        return $res;
    }
    
    /**
     * An item is an array with one of the following shapes:
     * 
     * ['tag-name', ...arguments]
     * [callable, ...arguments]
     * 
     *
     * @param [type] $item
     * @return void
     */
    public function item(array $item) {
        $res = '';
        if (is_callable($item[0])) {
            $cb = $item[0];
            $next = 1;
            while (isset($item[$next])) {
                $item[$next-1] = $item[$next];
                unset($item[$next]);
                $next++;
            }
            $res .= $cb($item);
        } elseif (is_string($item[0]) && method_exists($this, $item[0])) {
            $method = $item[0];
            $next = 1;
            while (isset($item[$next])) {
                $item[$next-1] = $item[$next];
                unset($item[$next]);
                $next++;
            }
            $res .= $this->$method($item);
        } else {
            throw new \Exception("Invalid item descriptor '".json_encode($item)."' in spec '".json_encode($this->spec)."'");
            $res .= '<div style="border:1px solid red; padding: 5px; margin: 3px; display: inline-block;"><strong>'.json_encode($item).'</strong> not supported</div>';
        }
/*
        } elseif(is_string($item[0]) && !method_exists($this, $item[0])) {
            
        } else {
            $method = array_shift($item);
            if (!is_string($method)) {
                echo "<pre>OOO";
                var_dump($method);
                die("method");
                var_dump(new \Exception("WHERE AM I")); die();
            }
            $res .= $this->$method($item);
        }
*/
        return $res;
    }

    /**
     * [ 'repeater', ???? ]
     */
    public function repeater($args) {
        $this->state->stylesheets[static::class.'-repeater'] = '/app/extra/ennerd-app-theme/assets/Render/repeater.css';
        $this->state->scripts[static::class.'-repeater'] = '/app/extra/ennerd-app-theme/assets/Render/repeater.js';
        $cols = $args[0];
        $firstColName = $cols[0][1];
        
        $id = 'auto_'.md5(serialize($args).serialize($this->spec));
        $res = '<div id="'.$id.'" class="rendered-repeater"><div class="rendered-repeater-blocks ui-sortable">';

        while($this->form->hasMoreValues($firstColName.'[]')) {
            $res .= '<div class="rendered-repeater-block">';
            $res .= $this->_repeater_row($cols);
            $res .= '</div>';
        }

        $res .= '</div>';

        // Template row
        $res .= '<div class="rendered-repeater-adder"><div class="rendered-repeater-block rendered-repeater-template">';
        $res .= $this->_repeater_row($cols);
        $res .= '</div></div></div>';
        return $res;
    }
    
    protected function _repeater_row($cols) {
        $res = '<div class="rendered-repeater-handle ui-sortable-handle">
                <i class="fas fa-bars"></i>
            </div>
            <div class="rendered-repeater-contents">';
        foreach($cols as $colDef) {
            $name = $colDef[1];
            $col = $this->meta->col($name);
            if($this->form && !$col->isReadonly()) {
                $this->form->addOptionalField($name);
                $caption = $col->getCaption();
                $fieldType = $col->getType();
                switch($fieldType) {
                    default:
                    case 'text' :
                        $res .= '<div class="rendered-repeater-field rendered-repeater-field-'.str_replace(["\\"], ["-"], $fieldType).'">'.$this->form->$fieldType($name.'[]', ['placeholder' => $caption]).'</div>';
                        break;
                        
                    //default:
                        throw new \Exceptions\CodingErrorException("Repeater does not support fields of type '".$col->getType()."'");
                }
            } else {
                die("MUIST IMPLEMENT READ ONLY FIELDS IN REPEATER");
            }
        }
        $res .= '</div>
                <div class="rendered-repeater-buttons"><button class="rendered-repeater-trash-button button" type="button" tabindex="-1">
                    <i class="fas fa-trash"></i>
                </button></div>';
        return $res;
    }

    /**
     * [ 'field', 'fieldName', 'readonly' => false, 'filter' => null ]
     *
     * Editable field rendering:
     * Uses $this->form->$fieldType, where $fieldType is from meta()['cols'][$fieldName]['type']
     *
     * Read only rendering:
     * [ 'field', 'fieldName', 'readonly' => true, 'filter' => function($row, $name) {} ]
     * or
     * Uses $this->filter->html() for rendering the value
     */
    public function field($args) {
        $name = $args[0];
        if($this->form && !$this->meta->col($name)->isReadonly() && empty($args['readonly'])) {
            $fieldType = $this->meta->col($name)->getType();
            if(!$this->meta->col($name)->isReadonly() && method_exists($this, $subMethod = 'form_'.$fieldType)) {
                // There exists specific code to handle this field type
                return $this->$subMethod($args);
            } else {
                $res = '<div class="field'.($this->form->hasError($name) ? ' has-error' : '').'">';
                $res .= $this->form->label($name, $this->meta->col($name)->getCaption());
                $attrs = [];

                if($this->meta->col($name)->isReadonly()) {
                    $attrs['readonly'] = 'readonly';
                }

                $res .= $this->form->$fieldType($name, $attrs);
/*
                if(!method_exists($this->form, $fieldType)) {
                    
                    $res .= $this->form->
                    
                    // This is an unknown field type, so we'll use hooks to query if somebody else provides this field type
                    $fieldRenderer = Hooks::dispatchToFirst(static::class.'.FieldType.'.$fieldType);

                    if($fieldRenderer) {
                        $res .= $fieldRenderer($this->form, $name);
                    }                    
                    var_dump($fieldType);die();
                    
                    //TODO: Check that class implements IFormField in any of the parents.
                    if(class_exists($fieldType) && method_exists($fieldType, 'meta')) {
                        // We're referencing another document type. Perhaps it has a 
                        $picker = Meta::create($this->state, $fieldType)->getRoute('pick_dialog');
                        var_dump($picker);

                        die("The target has meta()");
                        $res .= $fieldType::renderFormField($this->form, $name, $attrs);
                    } else {
                        $res .= $this->form->text($name, $attrs);
                    }
                }
*/
                $res .= '</div>';
            }
        } else {
            $res = '<div class="field" data-name="'.htmlspecialchars($name).'">';
            $res .= '<div class="f-label">'.htmlspecialchars($this->meta->col($name)->getCaption()).'</div>';
            if (isset($args['filter'])) {
                $cb = $args['filter'];
                $res .= '<div class="f-value">'.$cb($this->row, $name).'</div>';
            } else {
                $res .= '<div class="f-value">'.$this->filter->html($this->row, $name).'</div>';     
            }
            $res .= '</div>';
        }
        return $res;
    }
    
    public function form_checkbox($args) {
        $name = $args[0];
        $res = '<div class="rendered-checkbox field'.($this->form->hasError($name) ? ' has-error' : '').'">';
        $res .= $this->form->label($name, $this->meta->col($name)->getCaption());
        $res .= '<div class="f-value">';
        $res .= $this->form->checkbox($name);
        $res .= '</div>';
        $res .= '</div>';
        return $res;
    }
    
    public function toolbar($args) {
        $this->state->stylesheets[static::class.'-toolbar'] = '/app/extra/ennerd-app-theme/assets/Render/toolbar.css';
        $res = '<div class="rendered-toolbar">';
        foreach($args[0] as $toolbarItem)
            $res .= $this->item($toolbarItem);
        $res .= '</div>';
        return $res;
    }
    
    public function accordion($args) {
        if (!isset($args[0])) {
            throw new CodingErrorException("Accordion requires at least one spec: ['accordion', ...\$spec]");
        }
        foreach ($args as $k => $v) {
            if (!is_int($k)) {
                throw new CodingErrorException("Accordion does not support named properties, got '$k'");
            }
            unset($v['title']);
            unset($v['active']);
            if (!self::isSpec($v)) {
                throw new CodingErrorException("Accordion spec invalid: '".json_encode($v)."'");
            }
        }

        $id = 'auto_'.md5(serialize($args).serialize($this->spec));

        $this->state->stylesheets[static::class.'-accordion'] = '/app/extra/ennerd-app-theme/assets/Render/accordion.css';
        $this->state->scripts[static::class.'-accordion'] = '/app/extra/ennerd-app-theme/assets/Render/accordion.js';
        $res = '<div class="rendered-accordion">';

        $first = true;
        /**
         * is now: [ 'accordion', <spec>... ]
        */
        foreach($args as $k => $accordion) {
            /*
            source from cclive prod, don't think it should be here
            if (is_string($accordion)) {
                continue;
            }
            */
            if (empty($accordion['title'])) {
                throw new CodingErrorException("Accordion page has no `title` key (".\json_encode($accordion, \JSON_PRETTY_PRINT).")");
            }
            $title = !empty($accordion['title']) ? $accordion['title'] : 'Accordion panes must have "title" and may have "active"';
            $active = !empty($accordion['active']);
            unset($accordion['title']);
            unset($accordion['active']);
            $aid = $id.md5(serialize($accordion));
            $res .= '<div class="rendered-accordion-pane'.($active ? ' rendered-accordion-active' : '').'">';
            $res .= "<div class='rendered-accordion-title'>".htmlspecialchars($title)."</div>";
            $res .= "<div class='rendered-accordion-contents'>\n";

//            $res .= \var_export($accordion, true);
            $sub = new static($this->state, $accordion, $this->row, $this->form);
            $res .= $sub->render(true);
            
            $res .= "\n</div></div>";
        }
        
        $res .= '</div>';
        return $res;
    }
    
    public function buttons($args) {
        $res = '<p class="rendered-buttons buttons">';
        
        foreach($args[0] as $button) {
            $res .= $this->item($button);
        }
        
        $res .= '</p>';
        return $res;
    }
    
    public function submit($args) {
        $attrs = !empty($args['attrs']) ? $args['attrs'] : [];
        if(isset($attrs['class']))
            $attrs['class'] .= ' f-form f-submit';
        $attrs += ['type' => 'submit', 'class' => 'rendered-submit f-form f-submit'];
        $res = '<button';
        foreach($attrs as $k => $v)
            $res .= " ".$k.='="'.htmlspecialchars($v).'"';
        $res .= '>';
        return $res.$args[0].'</button>';
    }
    
    public function button($args) {
        $attrs = !empty($args['attrs']) ? $args['attrs'] : [];
        if(isset($attrs['class']))
            $attrs['class'] .= ' f-form f-submit';
        $attrs += ['type' => 'button', 'class' => 'rendered-button f-form f-button'];
        $res = '<button';
        foreach($attrs as $k => $v)
            $res .= " ".$k.='="'.htmlspecialchars($v).'"';
        $res .= '>';
        return $res.$args[0].'</button>';
    }
    
    public function html($args) {
        $attrs = !empty($args['attrs']) ? $args['attrs'] : [];
        $res = '<div class="rendered-html">'.$args[0].'</div>';
        return $res;
    }
    
    public function cols($args) {
        $res = '<div class="rendered-cols">';
        for ($i = 0; isset($args[$i]); $i++) {
            if (self::isSpec($args[$i])) {
                $renderer = new static($this->state, $args[$i], $this->row, $this->form);
                $res .= '<div class="rendered">' . $renderer->render(true) . '</div>';
            } else {
                $res .= '<div class="rendered">' . $this->item($args[$i]) . '</div>';
            }
        }
        return $res.'</div>';
    }

    protected function getCaption($name) {
        $col = $this->getCol($name);
        if(!isset($col['caption']))
            throw new \Exception('No caption defined for column "'.$name.'" in meta data ('.get_class($this->row).'::meta()).');
        return $col['caption'];
    }
    
    protected function getCol($name) {
        if(!isset($this->meta['cols'][$name]))
            throw new \Exception('Column "'.$name.'" not defined in meta data.');
        return $this->meta['cols'][$name];
    }

    /**
     * A "spec" is an array with only numerical keys where each value is an item descriptor.
     * This array represents a list of renderable items.
     *
     * @param array $spec
     * @return boolean
     */
    protected static function isSpec(array $spec) {
        foreach ($spec as $k => $v) {
            if (!is_int($k)) {
                return false;
            }
            if (!is_array($v)) {
                return false;
            }
        }
        return true;
    }
}
