<?php
namespace Theme\Meta;
use Fubber\Kernel\State;

abstract class Table extends \Fubber\Table {
    abstract public static function meta(State $state);
    
    public function isInvalid(): ?array {
        return Meta::create($this->state, $this)->row($this)->isInvalid();
    }
}
