<?php
namespace Theme\Meta;

use Fubber\Hooks;
use Fubber\Kernel\State;

use function Fubber\trans;

/**
 * Parses and builds a column definition array structure. A column definition has
 * this shape:
 * 
 *  [
 *      'caption' => string|Translatable,
 *      'type' => string,
 *      'readonly' => boolean|void,
 *      'rules' => string|void,
 *      'extends' => string|array|callable|void,
 *  ]
 */
class ColumnDefs {
    
    /**
     * Accepts $state and $def.
     */
    public static function getDef(State $state, string|array|callable $def) {
        if(is_callable($def)) {
            $def = $def($state);
        }
            
        if(!is_array($def)) {
            $key = 'def:'.$def;
            if(isset($state->cache[$key])) {
                $def = $state->cache[$key];
            } else if(method_exists(static::class, $def)) {
                $def = $state->cache[$key] = static::$def($state);
            } else if($newDef = Hooks::dispatchToFirst(static::class.'.Def.'.$def, $state)) {
                $def = $newDef;
            } else if(class_exists($def) && ($meta = Meta::create($state, $def, false))) {
                $def = $meta->getColumnDefinition();
            } else {
                throw new MissingMetaException("There is no definition named '$def'. Perhaps implement hook: '".static::class.".Def.".$def."'");
            }
        }
        while(isset($def['extends'])) {
            $extends = static::getDef($state, $def['extends']);
            // unsetting this means we'll get 'extends' from the parent class, allowing for cascading inheritance
            unset($def['extends']);
            $def += $extends;
        }
        return Hooks::filter(static::class.'.Filter', $def, $state);
    }
    
    public static function slug(State $state) {
        return [
            'caption' => trans('Slug'),
            'type' => 'text',
            'rules' => 'slug',
            ];
    }
    
    public static function name(State $state) {
        return [
            'caption' => trans('Name'),
            'type' => 'text',
            ];
    }
    
    public static function mimetype(State $state) {
        return [
            'caption' => trans('MIME-type'),
            'type' => 'text',
            ];
    }
    
    public static function created_date(State $state) {
        return [
            'caption' => trans('Created Date'),
            'type' => 'datetime',
            'readonly' => true,
            'rules' => 'datetime',
            ];
    }
    
    public static function id(State $state) {
        return [
            'caption' => trans('ID'),
            'type' => 'primarykey',
            'readonly' => true,
            'rules' => 'integer',
            ];
    }
}