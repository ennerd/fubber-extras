<?php
namespace Theme\Meta;

use Fubber\Kernel\State;

class Row {
    protected $spec;
    
    public function __construct(State $state, Meta $meta, \Fubber\Table\IBasicTable $row) {
        $this->state = $state;
        $this->meta = $meta;
        $this->row = $row;
    }
    
    public function getDisplayName() {
        $meta = $this->meta->getMetaData();
        if(isset($meta['displayName']) || isset($meta['display_name'])) {
            if(!isset($meta['displayName']))
                $meta['displayName'] = $meta['display_name'];
            if(is_callable($meta['displayName']))
                return call_user_func($meta['displayName'], $this->row, $this->state, $this->meta);
            else if(is_string($meta['displayName'])) {
                $displayName = $meta['displayName'];
                return $this->row->$displayName;
            } else {
                throw new InvalidSpecificationException($this->meta->getClassName().' displayName must be a field name or a callback which accepts an object instance.');
            }
        }
        // Todo: Should be able to declare name in meta()
        $json = $this->row->jsonSerialize();
        if(isset($json['_string'])) {
            return $json['_string'];
        }

        if (isset($meta['names']['singular'])) {
            return $meta['names']['singular'];
        }

        return null;
    }
    
    public function isInvalid(): ?array {
        $validator = new Errors($this->state, $this->row, $this->meta);
        return $validator->isInvalid();
    }
}
