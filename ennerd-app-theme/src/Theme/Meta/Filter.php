<?php
namespace Theme\Meta;

use Fubber\Hooks;
use Fubber\Kernel\State;

use function Fubber\trans;

/**
 * Class designed to filter data from tables before presenting
 * the data in various contexts. For example it can format dates
 * to the user timezone or escape HTML or ensure a date is
 * serialized on a consistent way for JSON.
 * 
 * Some implementation details should be refactored and modularized,
 * in particular the methods attached to the class.
 */
class Filter {
    
    protected $state;
    protected $object;
    protected $timezone;
    protected $datetimeFormat;
    
    public static function create(State $state) {
        if(isset($state->cache[static::class])) {
            return $state->cache[static::class];
        }
        return $state->cache[static::class] = new static($state);
    }
    
    public function __construct(State $state) {
        $this->state = $state;
    }
    
    /**
     * Filter the data for output as HTML, so that it is properly displayed to the end user. For 
     * example, an e-mail address will be hyperlinked, a foreign key to another
     * class will display it with name instead of object id.
     */
    public function html($object, $property=null) {
        if(!$object) {
            throw new \TypeError("Argument 1 must be an object");
        }
        if($property===null) {
            $text = $this->text($object);
            $meta = Meta::create($this->state, $object);
            $url = $meta->getRoute('view', $object->id);
            if(!$url) return $text;
            $url_dialog = $meta->getRoute('view_dialog', $object->id);
            $theId = 'autoid_'.md5($url_dialog);
            $onclick = htmlspecialchars('if(Theme.Dialog.isDialog()) {Theme.Dialog.open('.json_encode($url_dialog).', "tall", function() {console.log("reload"); reloadPart('.json_encode('#'.$theId).'); }); return false;}');
            return '<span id="'.$theId.'"><a  href="'.htmlspecialchars($url).'" onclick="'.$onclick.'">'.htmlspecialchars($text).'</a></span>';
        }
        if($col = $this->getCol($object, $property)) {
            $res = Hooks::filter(static::class.'.Html.'.$col->getType(), $object->$property, $this->state, $object, $property);
            // Was it modified, then we'll accept the change.
            if($res != $object->$property)
                return $res;

            if($object->$property === null)
                return '&nbsp;';
                
            $method = 'html_'.$col->getType();
            if(method_exists($this, $method))
                return $this->$method($object->$property, $col);
            if($col->getType()!==strtolower($col->getType()) || strpos($col->getType(), '/')!==false) {
                $className = $col->getType();
                $row = $className::load($object->$property);
                if(!$row) throw new \Exceptions\NotFoundException("Could not load object $className id ".$object->$property);
                return $this->html($row);
            }
        }
        if(!is_string($object->$property))
            return $object->$property;
        return htmlspecialchars($object->$property);
    }
    
    public function text($object, $property=null) {
        if(!$object)
            throw new \TypeError("Argument 1 must be an object");
        if($property === null) {
            $meta = Meta::create($this->state, $object);
            return $meta->row($object)->getDisplayName();
        } else if($col = $this->getCol($object, $property)) {
            $res = Hooks::filter(static::class.'.Text.'.$col->getType(), $object->$property, $this->state, $object, $property);
            if($res != $object->$property)
                return $res;
            $method = 'text_'.$col->getType();
            if(method_exists($this, $method))
                return $this->$method($object->$property);
            if($subMeta = Meta::create($this->state, $className = $col->getType())) {
                $className = $subMeta->getClassName();
                $row = $className::load($object->$property);
                return $this->text($row);
            }
        }
        return htmlspecialchars($object->$property);
    }
    
    /**
     * Filter the property so that it is properly formatted for json
     */
    public function json($object, $property) {
        if($col = $this->getCol($object, $property)) {
            if(isset($col['type'])) {
                $res = Hooks::filter(static::class.'.Json.'.$col['type'], $object->$property, $this->state, $object, $property);
                if($res != $object->$property)
                    return $res;
                $method = 'json_'.$col['type'];
                if(method_exists($this, $method)) {
                    return $this->$method($object->$property);
                }
            }
        }
        return $object->$property;
    }
    
    protected function getCol($object, $property) {
        try {
            $meta = Meta::create($this->state, $object);
        } catch (\Theme\Meta\MissingMetaException $e) {
            return null;
        }
        return $meta->col($property);
    }
    
    public function html_checkbox($value) {
        return $value ? trans('Yes') : trans('No');
    }
    
    public function html_email($value) {
        return '<a href="mailto:'.htmlspecialchars($value).'">'.htmlspecialchars($value).'</a>';
    }

    public function html_url($value) {
        return '<a href="'.htmlspecialchars($value).'" target="_blank">'.htmlspecialchars($value).'</a>';
    }
    
    public function html_datetime($value) {
        if($value===null)
            return '';
        if(!$this->timezone) 
            $this->timezone = ($this->state->user ? $this->state->user->getTimezone() : new \DateTimeZone(date_default_timezone_get()));
        if(!$this->datetimeFormat)
            $this->datetimeFormat = ($this->state->user ? $this->state->user->getDateTimeFormat() : 'Y-m-d H:i:s');

        $time = new \DateTime($value);
        $time->setTimezone($this->timezone);
        return $time->format($this->datetimeFormat);
    }
    
    public function html_select($value, $col) {
        $values = $col->getFormOptions();
        if(isset($values[$value]))
            return $values[$value];
        return $value;
    }
}