<?php
namespace Theme\Meta;

use Fubber\CodingErrorException;
use Fubber\Kernel\State;

/**
 * Represents metadata about a data source column/field.
 */
class Column {
    protected $spec;

    const VALID_KEYS = [
        'caption',
        'type',
        'rules',
        'form_options',
        'readonly',
        'description',
        'default',
        'override',
    ];
    
    public function __construct(State $state, string|array $spec) {
        $this->spec = ColumnDefs::getDef($state, $spec);
        foreach ($this->spec as $k => $v) {
            if (!\in_array($k, self::VALID_KEYS)) {
                throw new CodingErrorException("Column spec `".\json_encode($spec)."` can't contain key `$k`");
            }
        }
    }

    public function forInstance(object $instance): static {
        die("Meta\\Column override opportunity");
        return $this;
    }
    
    public function getColumn() {
        return !empty($this->spec['meta']) ? $this->spec['meta'] : null;
    }
    
    public function isForeignKey() {
        
    }
    
    public function isMultiple() {
        return !empty($this->spec['multiple']);
    }
    
    public function getCaption() {
        return !empty($this->spec['caption']) ? $this->spec['caption'] : 'No caption declared';
    }
    
    public function getType() {
        return !empty($this->spec['type']) ? $this->spec['type'] : 'text';
    }
    
    public function getRules() {
        return !empty($this->spec['rules']) ? $this->spec['rules'] : [];
    }
    
    public function getFormOptions() {
        if(!empty($this->spec['form_options'])) {
            if(is_callable($this->spec['form_options']))
                $this->spec['form_options'] = call_user_func($this->spec['form_options']);
            return $this->spec['form_options'];
        }
        return null;
    }
    
    public function isReadonly() {
        return !empty($this->spec['readonly']);
    }
}