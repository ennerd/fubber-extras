<?php
namespace Theme;

/**
 * Helps to create more custom fields
 * 
 */
class CRUDField {
    public function __construct($row=null) {
        $this->row = $row;
    }
    
    public function display() {
        return $this->row['id'];
    }
    
    public function showField(\Fubber\Forms\Form $form) {
        return "SHOULD SHOW FIELD";
    }
    
    public function parseField(\Fubber\Forms\Form $form) {}
}
