<?php
namespace Theme;

use Fubber\Kernel\State;

class Chart {
    protected $series = [];
    protected $labels;
    protected $options = [];

    public static function gaugeChart(State $state, $values, $options=[]) {
        $options += [];
        $chart = new static();
        $chart->setSeries($values);
        
        $chart->setOptions([
            'donut' => true,
            'donutWidth' => '40%',
            'startAngle' => 270,
            'total' => array_sum($values) * 2,
            ]);
        return $chart->pie($state);
    }

    public static function pieChart(State $state, $values, $options=[]) {
        $chart = new static();
        $chart->setSeries($values);
        return $chart->pie($state);
    }
    
    public function __construct() {
    }

    /**
     * Add a series of values for the X-axis. 
     * 
     * Simple: [ 42, 58 ]
     * Multi dimensional: [ [ 2, 5], [ 3, 6] ]
     */
    public function setSeries(array $values) {
        $this->series = $values;
        return $this;
    }
    
    /**
     * Set labels for the values on the X-axis.
     */
    public function setLabels(array $labels) {
        $this->labels = $labels;
        return $this;
    }
    
    /**
     * Set options.
     */
    public function setOptions(array $options) {
        $this->options = $options;
        return $this;
    }
    
    
    
    public function pie(State $state) {
        $this->addAssets($state);
        $data = new \stdClass();
        $data->series = $this->series;
        if($this->labels) {
            //$data->labels = $this->labels;
        }
        
        return $state->view('theme/charts/pie', ['data' => $data, 'options' => $this->options]);
    }
    
    protected function addAssets(State $state) {
/*
print_r($state);
die("ADDING CHARTS!");
*/
        $state->scripts['chartist-js'] = '/app/extra/ennerd-app-theme/assets/chartist-js/chartist.min.js';
        $state->stylesheets['chartist-js'] = '/app/extra/ennerd-app-theme/assets/chartist-js/chartist.min.css';
        $state->scripts['chartist-js-integration'] = '/app/extra/ennerd-app-theme/assets/chartist.js';
        $state->stylesheets['chartist-js-integration'] = '/app/extra/ennerd-app-theme/assets/chartist.css';
    }
}
        
