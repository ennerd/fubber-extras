<?php
namespace Theme\Template;

use Fubber\AssetManager\Asset;
use Fubber\AssetManager\Dependency;
use stdClass;

/**
 * A special Asset which automatically is built from an NPM package.json
 * file.
 * 
 * @package Theme\Template
 */
class PackageJsonAsset extends Asset {

    public readonly string $path;
    protected readonly stdClass $package;

    public function __construct(string $path) {
        $this->path = $this->path;
        $this->package = \json_decode(\file_get_contents($this->path));

        $dependencies = [];
        if (isset($this->package->dependencies)) {
            foreach ($this->package->dependencies as $identifier => $version) {
                $dependencies[] = new Dependency($identifier, $version);
            }
        }

        parent::__construct(
            $this->package->name,
            $this->package->version,
            $this->path,
            ...$dependencies
        );
    }

}