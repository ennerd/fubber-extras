<?php
namespace Theme\Template;

/**
 * Contains additional tags for the HEAD section of the template.
 * 
 * @package AssetController
 */
class ResourceSet {
    public readonly array $headJS;
    public readonly array $headCSS;
    public readonly array $headRaw;
    public readonly ?string $packageJson;
    public readonly ?string $basePath;

    public function __construct(
        array|string $headJS=[],
        array|string $headCSS=[],
        array|string $headRaw=[],
        ?string $packageJson=null,
        ?string $basePath=null
    ) {
        $this->headJS = is_string($headJS) ? [ $headJS ] : $headJS;
        $this->headCSS = is_string($headCSS) ? [ $headCSS ] : $headCSS;
        $this->headRaw = is_string($headRaw) ? [ $headRaw ] : $headRaw;
        $this->packageJson = $packageJson;
        $this->basePath = $basePath;
    }
}