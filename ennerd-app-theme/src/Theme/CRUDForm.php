<?php
namespace Theme;

use Fubber\Html\DomNode;
use Fubber\Html\Tag;
use Fubber\Kernel\State;

class CRUDForm extends \Fubber\Forms\TableForm {
    protected $spec;

    /**
     * $spec format:
     * 
     *  [
     *      [ 'first_name', First name:', 'text' ],      // Field name, label and field type
     *      [ 'last_name', 'Last name:', 'text' ],       // Field name, label and field type
     *      [ 'bio', 'Biography:', 'textarea', 'wide' ],      // Field name, label, field type and full width.
     *      [ 'gender', 'Gender:', 'select', 'values' => ['male','female']],
     *  ]
     */
    public function __construct(State $state, \Fubber\Table\IBasicTable $row, array $spec) {
        $this->spec = $spec;
        $cols = [];
        foreach($spec as $col) {
            if(isset($col[0])) {
                if(isset($col['values']))
                    $cols[] = [$col[0], $col['values']];
                else
                    $cols[] = $col[0];
            }
        }
        parent::__construct($state, $row, $cols);
    }
    
    /**
     * Renders a nice form according to the spec format provided as third argument to the constructor.
     */
    public function render() {
        $errors = $this->errors;
        if(!$errors) $errors = [];
        $html = '<div class="basicform f-form">';
        // $html .= '<pre>'.print_r($this->spec, true).'</pre>';

        foreach($this->spec as $col) {
            
            $html .= '<div class="field'.(isset($col[3]) ? ' '.$col[3]:'').(isset($errors[$col[0]]) ? ' has-error' : '').'"'.(isset($errors[$col[0]]) ? ' data-error="'.htmlspecialchars($errors[$col[0]]).'"' : '').'>';
            
            $html .= $this->label($col[0], $col[1]);
            $fieldType = $col[2];
            if(!method_exists($this, $fieldType)) {
                // This may be a special field
                if(class_exists($fieldType) && is_subclass_of($fieldType, \Theme\CRUDController::class)) {
                    $html .= $this->controllerField($col[0]);
                } else {
                    $html .= $this->text($col[0]);
                }
            } else {
                $html .= $this->$fieldType($col[0]);
            }
            
            $html .= '</div>';
            
        }
        
        $html .= '</div>';
        
        
        return $html;
    }
    
    /**
     * Renders a field that uses CRUDController to select
     */
    public function controllerField($name, array $attrs=[]) {
        $spec = null;
        foreach($this->spec as $specTmp) {
            if($specTmp[0]==$name) {
                $spec = $specTmp;
                break;
            }
        }
        
        $controller = $spec[2];
        $model = $controller::$model;
        $prefix = $controller::$prefix;
        if($this->$name) {
            $value = $this->$name;
            $row = $model::load($value)->jsonSerialize();
            $currentString = isset($row['_string']) ? $row['_string'] : '';
        } else 
            $currentString = '';
        $this->_usedFields[] = $name;
        $domNode = new DomNode(new Tag('input', static::mergeAttrs($attrs, [
            'type' => 'text',
            'id' => $name,
            'name' => $name,
            'value' => $this->$name,
            'class' => 'f-form f-controller-field',
            'data-picker' => route($prefix.'_pick'),
            'data-current-string' => $currentString,
            ])));
        return $domNode;
    }
}
