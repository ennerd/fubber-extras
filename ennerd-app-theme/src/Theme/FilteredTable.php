<?php
namespace Theme;

use Fubber\Kernel\State;

/**
 * DO NOT USE! NOT READY FOR USE!
 * 
 * When table rows have meta(), this class will filter the properties, so that if you for
 * example try to retrieve a datetime column - you will get localized datetime.
 */
class FilteredTable {
    use \Fubber\Util\ProxyTrait {
        __get as protected proxy_get;
    }
    
    protected $_state;
    protected $_object;
    protected $_meta;
    
    public function __construct(\Fubber\Table\IBasicTable $object, State $state) {
        $this->_state = $state;
        $this->_object = $object;
        if(!method_exists($object, 'meta'))
            throw new \Exceptions\CodingErrorException("Filtered Table objects must implement the meta() method");
        $this->_meta = $this->meta();
    }
    
    protected function getProxiedObject() {
        return $this->_object;
    }
    
    public function __get($name) {
        if(isset($this->_meta['cols'][$name]))
            $col = $this->getCol($name);
        return static::proxy_get($name);
    }
    
    protected function getCol($name) {
        if(!isset($this->_meta['cols'][$name]))
            throw new \Exception('Column "'.$name.'" not defined in meta data.');
        return $this->_meta['cols'][$name];
    }
}
