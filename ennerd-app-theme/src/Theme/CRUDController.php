<?php
namespace Theme;

use Exceptions\CodingErrorException as ExceptionsCodingErrorException;
use Fubber\FlashMessage;
use Fubber\Hooks;
use Fubber\Kernel\State;
use Fubber\Html\DomNode;
use Fubber\Html\Tag;
use Fubber\ValidationException;
use Fubber\CodingErrorException;
use Fubber\DeprecatedException;
use Fubber\FubberAPI;
use Fubber\I18n\Translatable;
use Fubber\Table\ITable;
use ReflectionMethod;
use ReflectionProperty;
use Theme\Meta\Form;
use Theme\Meta\Meta;
use TypeError;
use Theme\Meta\MissingMetaException;

use function Fubber\route;
use function Fubber\trans;

/**
 * Overview of CRUDController
 * ==========================
 * 
 * The CRUDController allows you to efficiently build powerful REST APIs and
 * user interfaces for (C)reate, (R)etrieve, (U)pdate and (D)elete operations
 * on Fubber\Table data models.
 * 
 * You define the user interface and API by extending this class and overriding
 * static properties in your child class. Alternatively you can implement static
 * methods in your child class which can further customize the user interface
 * based on who is requesting the page.
 * 
 * 
 * Important things to know:
 * 
 * Configuration Variables
 * -----------------------
 * 
 * All configurable properties are declared on the CRUDController class as 
 * static properties. You can provide a value for each of these properties by
 * either declaring the property in your child class, or by implementing a
 * static method with the same name as the property. Examples:
 * 
 * ```
 *     // This property does not have a value, so you are REQUIRED to provide
 *     // a value.
 *     CRUDController {
 *         public static string $model;
 *     }
 * 
 *     
 *     ChildClass extends CRUDController {
 * 
 *         // Declaring the value title statically
 *         public static string $model = Models\User::class;
 * 
 *         // Declaring the value dynamically
 *         public static function model(State $state): string {
 *             return Models\User::class;
 *         }
 *     }
 * ```
 * 
 * Templated Strings
 * -----------------
 * 
 * Whenever you provide a string for the user interface, you can use variable
 * placeholders:
 * 
 * ```
 *     public static string $view_title = 'Viewing {displayName} user account';
 * ```
 * 
 * In this case `{displayName}` will be replaced with the value of the `displayName`
 * property in the object we're viewing.
 * 
 * In addition to these variables, a few other variables are provided:
 * 
 *     `{_displayName}` is the display name for the row, as configured by in Meta.
 *     `{_plural}` is the data type name, for example "Users" as configured in Meta.
 *     `{_singular}` is the data type name in singular variant, for example 'User' (from Meta)
 *     `{_description}` is the data type description (from Meta)
 *     `{_class}` is the class name (from Meta)
 * 
 * 
 * Defining the various page layouts
 * ---------------------------------
 * 
 * Pages are defined via the properties `index_def`, `view_def`, `edit_def` and `create_def`.
 * These properties are configured by array structures:
 * 
 * ### `index_def`
 * 
 * The index page user interface is normally a simple grid-view with paging and a simple
 * search capability. The `index_def` array describes each of the columns of the grid-view.
 * 
 * ```
 *     public static array $view_def = [
 * 
 *         // 'id' is a property of the model. Column is left aligned and requires 50 pixels width.
 *         'id' => [ 'left', 50 ],
 * 
 *         // 'name' is a property of the model. Column is left aligned and has no specific width.
 *         'name' => [ 'left' ],                    // left aligned, flexible width
 * 
 *         // 'status' is NOT a property, so we must provide a 'caption' and a 'value'
 *         'status' => [ 'center', 20,              // center aligned, 20 pixels width
 *             'caption' => 'Logged in',            // Override the column caption
 *             'value' => function($user, $state) { // Provide a callback which returns the HTML for the column value
 *                 return $user->isLoggedIn() ? 'yes' : 'no';
 *             }
 *          ]
 *     ];
 * ```
 * 
 * 
 */ 
abstract class CRUDController {
    use FubberAPI;

    public static function getDynamicController() {
        return null;
    }
    /**
     * All of the following properties should be customized according to your
     * requirements. If you want to dynamically create a value, a method can be
     * created. For example:
     * 
     * public static function model() { return 'SomeClass'; }
     * 
     * will be used instead of
     * 
     * public static $model = 'SomeOtherClass';
     */

    /**
     * view_def reference
     *
     * <spec>              [ <item>..., 'attrs' => null ]
     * <item>:             <function>, <field>, <cols>, <form_checkbox>, <toolbar>, <accordion>, <buttons>
     * <function>:         [ function($args) {}[, $args...] ],
     * <field>:            [ 'field', 'fieldName'[, 'readonly'=>true][, 'filter'=>function($row,$fieldName)] ],
     * <cols>:             [ 'cols', [<spec>...] ]
     * <form_checkbox>:    [ 'form_checkbox', 'fieldName' ]
     * <toolbar>           [ <item>... ]
     * <accordion>         [ 'accordion', [ <accordionPane>... ] ]
     * <accordionPane>     <item> + [ 'title' => <string>, 'active' => <boolean|null> ]
     * <button>            [ 'submit', <string>, 'attrs' => [] ]
     * <submit>            [ 'button', <string>, 'attrs' => [] ]
     * <buttons>           [ 'buttons', [ <item>... ] ]
     * <html>              [ 'html', <string> ]
     */

    // `Hooks::filter($className.'::$propertyName')` can be used to override any value here
    public static bool $enabled = true;

    /**
     * The database model class name
     * 
     * @var class-string
     */
    public static string $model;

    /**
     * The prefix/base route for this controller
     */
    public static string $prefix;

    /**
     * Enable a simple JSON/REST API for the data model?
     *
     * @var boolean
     */
    public static bool $enable_rest_api = false;

    /**
     * Name of the API endpoint, in automatically generated documentation
     */
    public static string $api_name;

    /**
     * Base path of the API
     */
    public static string $api_path;
    
    /**
     * Title of the index/listing page
     *
     * @var string
     */
    public static string $index_title = '$index_title';

    /**
     * Text below the page title
     *
     * @var boolean
     */
    public static string $index_text;

    /**
     * Label for the button on the index page which links to the
     * create new row page.
     *
     * @var [type]
     */
    public static string $index_add_button;

    /**
     * Title of the view details page.
     *
     * @var string
     */
    public static string $view_title = '$view_title';

    /**
     * Text below the title on the view details page.
     *
     * @var boolean
     */
    public static ?string $view_text = null;

    /**
     * Label for the back button on the view details page.
     * 
     * @var string
     */
    public static string $view_back_button;

    /**
     * Label for the edit button on the view details page.
     *
     * @var string
     */
    public static string $view_edit_button;

    /**
     * Label for the delete button on the view details page.
     * 
     * @var string
     */
    public static string $view_delete_button;
    
    /**
     * Title for the edit object page.
     *
     * @var string
     */
    public static string $edit_title = '$edit_title';

    /**
     * Text below the title on the edit object page.
     * 
     * @var string
     */
    public static ?string $edit_text = null;

    /**
     * Label for the save button on the edit object page
     *
     * @var string
     */
    public static string $edit_save_button;

    /**
     * Label for the back button on the edit object page
     * 
     * @var string
     */
    public static string $edit_back_button;

    /**
     * Message displayed for the user after saving the object.
     * The message can contain variable {place_holders}, where the
     * value comes from the saved object.
     * 
     * @var string
     */
    public static string $edit_save_message;
    
    /**
     * A form definition
     *
     * @var [type]
     */
    public static array $create_def;
    public static $create_form_spec = null;
    public static string $create_title = '$create_title';
    public static ?string $create_text = null;
    public static string $create_save_message;
    public static string $create_save_button;
    public static string $create_back_button;
    
    public static $delete_title = '$delete_title';
    public static ?string $delete_text = null;
    public static string $delete_confirm_button;
    public static string $delete_back_button;
    public static string $delete_success_message;
    public static string $delete_failed_message;
    
    public static
        $index_template = 'theme/crud/index',
        $view_template = 'theme/crud/view',
        $edit_template = 'theme/crud/edit',
        $delete_template = 'theme/crud/delete',
        $create_template = 'theme/crud/create';

    /**
     * Holds a reference to State while processing a request, and
     * the $state variable is set back to null when the response
     * is returned.
     */
    private static ?State $state = null;

    protected static function respond(State $state, $response) {
        return $state->json($response);
    }


    /**
     * The filter `<className> property $name` can be used to alter the
     * behavior of CRUDControllers.
     *
     * In some cases, you want to dynamically generate specifications for the 
     * controller. This is done by creating a static method with the same name
     * as the property. This method checks if the method exists, and uses the
     * return value.
     */
    protected static final function _getVal(string $name, $state=null, $default=null, array $extraData=[]) {
        return Hooks::filter(static::class.'::$'.$name, (function() use($name, $state, $default, $extraData) {
            /**
             * If a static method exists with the name of the variable we want to fetch, then
             * call that method.
             */
            if(method_exists(static::class, $name) && (new ReflectionMethod(static::class, $name))->isStatic()) {
                return static::$name($state, $extraData);
            }

            
            /**
             * If a property is declared and initialized, use that value.
             */
            if (property_exists(static::class, $name)) {
                $rp = new ReflectionProperty(static::class, $name);

                if ($rp->isInitialized()) {
                    return static::$$name;
                } else {
                    return self::getDefaultText($name);
                }
            }

            /**
             * If a default value was not provided, this value is required.
             */
            if ($default===null) {
                throw new \Exceptions\CodingErrorException('Unable to retrieve value from "'.static::class.'::'.$name.'()" or "'.static::class.'::$'.$name.'".');
            }
            return $default;
        })(), $state, $default);
    }
    
    private static function getDefaultText(string $propertyName) {
        static $defaults = [
            'index_add_button' => new Translatable("Add"),
            'view_back_button' => new Translatable("Back"),
            'view_edit_button' => new Translatable("Edit"),
            'view_delete_button' => new Translatable("Delete"),
            'edit_save_button' => new Translatable("Save"),
            'edit_back_button' => new Translatable("Back"),
            'edit_save_message' => new Translatable("Saved"),
            'create_save_button' => new Translatable("Save"),
            'create_save_message' => new Translatable("Saved"),
            'create_back_button' => new Translatable("Cancel"),
            'delete_confirm_button' => new Translatable("Delete"),
            'delete_back_button' => new Translatable("Cancel"),
            'delete_success_message' => new Translatable("Record was deleted"),
            'delete_failed_message' => new Translatable("Record could not be deleted"),
        ];
        return $defaults[$propertyName] ?? null;
    }

    public static function canList(State $state) {
        return $state->access(static::_getVal('model', $state))->hasPrivilege('List');
    }
    
    public static function canCreate(State $state) {
        return $state->access(static::_getVal('model', $state))->hasPrivilege('Create');
        if(method_exists( static::_getVal('model', $state), 'canCreate' )) {
            $className = static::_getVal('model', $state);
            return $className::canCreate($state);
        }
        return true;
    }

    public static function canView(State $state, $row) {
        return $state->access($row)->hasPrivilege('View');
        if(method_exists($row, 'canView')) {
            return $row->canView($state);
        }
        return true;
    }

    public static function canEdit(State $state, $row) {
        return $state->access($row)->hasPrivilege('Edit');
        if(method_exists($row, 'canEdit')) {
            return $row->canEdit($state);
        }
        return true;
    }

    public static function canDelete(State $state, $row) {
        return $state->access($row)->hasPrivilege('Delete');
    }
    
    public static function init() {
        if (!static::_getVal('enabled')) {
            return;
        }

        if (static::$model === null) {
            throw new CodingErrorException(static::class.'::$model must contain a model class name');
        }
        if (static::$prefix === null) {
            throw new CodingErrorException(static::class.'::$prefix must contain a path prefix string for routes');
        }

        Hooks::listen($t = 'Theme.TableSource.index_'.str_replace(['-', "/"], "_", static::_getVal('prefix')), static::index_tablesource(...));

        if(static::_getVal('enable_rest_api')) {
            Hooks::listen('Theme.RestApis', [static::class, 'rest_apis']);
        }
        
        Hooks::listen('Fubber\Forms\Form.Method.'.static::_getVal('model'), [ static::class, 'form_field']);
        
        $array = &Meta::extraMetaData(static::_getVal('model'));
        if(!isset($array['routes'])) 
            $array['routes'] = [];

        /**
         * Routes can be generated for objects if this information is added to the meta-array.
         */
        $prefix = static::_getVal('prefix');
        $array['routes'] += [
            'index' =>          $prefix,
            'view' =>           $prefix.'_view',
            'edit' =>           $prefix.'_edit',
            'delete' =>         $prefix.'_delete',
            'create' =>         $prefix.'_create',
            'index_dialog' =>   $prefix.'_index_dialog',
            'view_dialog' =>    $prefix.'_view_dialog',
            'edit_dialog' =>    $prefix.'_edit_dialog',
            'delete_dialog' =>  $prefix.'_delete_dialog',
            'create_dialog' =>  $prefix.'_create_dialog',
            'pick_dialog' =>    $prefix.'_pick',
            ];
    }
    
    public static function rest_apis(State $state) {
        $api_name = static::_getVal('api_name');
        if(!$api_name) {
            $api_name = static::_getVal('model', $state)::$_table;
        }

        $meta = static::getMeta($state); 
        $res = [];

        $res['#'] = trans("APIs for :name", ['name' => static::getMeta($state)->getPluralName()]);
        $list_title = trans('Collection');
        $create_title = trans('Create item');
        $view_title = trans('Retrieve item');
        $delete_title = trans("Delete item");

        $res['index'] = [
            'name' => $list_title,
            'method' => 'GET',
            'path' => route('api_'.$api_name.'_list'),
            'description' => self::interpolateString(static::_getVal('index_text', $state), null, false),
            ];
        $res['create'] = [
            'name' => $create_title,
            'method' => 'POST',
            'path' => route('api_'.$api_name.'_create'),
            'description' => self::interpolateString(static::_getVal('create_text', $state), null, false),
            ];
        $res['retrieve'] = [
            'name' => $view_title,
            'method' => 'GET',
            'path' => route('api_'.$api_name.'_retrieve', '{id}'),
            'description' => self::interpolateString(static::_getVal('view_text', $state),  null, false),
            ];
        $res['delete'] = [
            'name' => $delete_title,
            'note' => trans("Delete an item from the collection"),
            'method' => 'DELETE',
            'path' => route('api_'.$api_name.'_delete', '{id}'),
            'description' => self::interpolateString(static::_getVal('delete_text', $state),  null, false),
            'response' => ['result' => true],
            ];

        $actions = static::getApiActions();
        foreach ($actions as $action) {
            
            foreach (['title', 'note', 'name', 'method', 'path', 'response', 'description'] as $requiredKey) {
                if (!array_key_exists($requiredKey, $action)) {
                    throw new \Fubber\CodingErrorException("Action '".$action['path']."' must have a '$requiredKey' key");
                }
            }
            if (isset($res[$action['name']])) {
                throw new \Fubber\CodingErrorException("Action '".$action['name']."' added twice");
            }
            $res[$action['name']] = $action;
/*
            $res[$action['name']] = [
                'title' => $action['title'],
                'note' => $action['note'],
                'name' => $action['name'],
                'method' => $action['method'],
                'path' => $action['path'], // route($action['route'], '{id}'),
                'payload' => json_encode($action['payload'] ?? null),
                'description' => $action['description'] ?? $action['info']->description,
                'note' => trans("RPC endpoint"),
                ];
*/
        }

        $res = Hooks::filter(static::class.'::rest_apis()', $res, $state);
        return $res;
    }

    public static function routes() {
        if (!static::_getVal('enabled')) {
            return [];
        }
        $prefix = static::_getVal('prefix');
        $res = [
            $prefix.' GET /'.$prefix.'/' => [ static::class, 'index' ],
            $prefix.'_create GET|POST /'.$prefix.'/create/' => [ static::class, 'create' ],
            $prefix.'_dialog GET /'.$prefix.'/dialog/' => [ static::class, 'index_dialog' ],
            $prefix.'_pick GET /'.$prefix.'/pick-dialog/' => [ static::class, 'index_pick' ],
            $prefix.'_create_dialog GET|POST /'.$prefix.'/create/dialog/' => [ static::class, 'create_dialog' ],
            $prefix.'_view GET /'.$prefix.'/{id}/' => [ static::class, 'view' ],
            $prefix.'_edit GET|POST /'.$prefix.'/{id}/edit/' => [ static::class, 'edit' ],
            $prefix.'_delete GET|POST /'.$prefix.'/{id}/delete/' => [ static::class, 'delete' ],
            $prefix.'_view_dialog GET /'.$prefix.'/{id}/dialog/' => [ static::class, 'view_dialog' ],
            $prefix.'_edit_dialog GET|POST /'.$prefix.'/{id}/edit/dialog/' => [ static::class, 'edit_dialog' ],
            $prefix.'_delete_dialog GET|POST /'.$prefix.'/{id}/delete/dialog/' => [ static::class, 'delete_dialog' ],
            ];


        if(static::_getVal('enable_rest_api')) {
            $api_name = static::_getVal('api_name');
            if(!$api_name) {
                $api_name = static::_getVal('model')::$_table;
            }
            $api_path = static::_getVal('api_path');
            if ( !$api_path ) {
                $api_path = '/rest-api/'.$api_name;
            }
            /**
             * CRUD style API for the collection
             */
            // Operations on the collection
            $res['api_'.$api_name.'_list GET '.$api_path.'/'] = [ static::class, 'api_list' ];
            // PUT method is for replacing the entire collection and is not supported
            $res['api_'.$api_name.'_create POST '.$api_path.'/'] = [ static::class, 'api_create' ];
            // DELETE method is for deleting the entire collection and is not supported

            // Operations on the items
            $res['api_'.$api_name.'_retrieve GET '.$api_path.'/{id}/'] = [ static::class, 'api_item_retrieve' ];
            $res['api_'.$api_name.'_replace PUT '.$api_path.'/{id}/'] = [ static::class, 'api_item_replace' ];
            $res['api_'.$api_name.'_update POST '.$api_path.'/{id}/'] = [ static::class, 'api_item_update' ];
            $res['api_'.$api_name.'_delete DELETE '.$api_path.'/{id}/'] = [ static::class, 'api_item_delete' ];

            // Verbs are automatically registered as endpoints
            $actions = static::getApiActions();
            foreach ($actions as $action) {
                $res[$action['route'].' '.$action['method'].' '.$action['path']] = $action['callback'];
            }
        }

        return $res;
    }

    protected static function getApiActions(State $state=null) {
        $api_name = static::_getVal('api_name');
        if(!$api_name) {
            $api_name = static::_getVal('model')::$_table;
        }
        $api_path = static::_getVal('api_path');
        if ( !$api_path ) {
            $api_path = '/rest-api/'.$api_name;
        }
        $actions = [];

        $ref = new \ReflectionClass(static::class);
        $parser = new \Fubber\Util\PhpCommentParser();
        foreach ($ref->getMethods(\ReflectionMethod::IS_STATIC | \ReflectionMethod::IS_FINAL) as $method) {
            $doc = $method->getDocComment();
            $forMethod = $method->class.'::'.$method->name;

            // Optimization - won't parse stuff we don't need to
            if (!$doc || strpos($doc, '@')===false) {
                continue;
            }

            $parsed = $parser->parse($doc);

            foreach ($parser->parse($doc)->tags as $tag) {
                if ($tag->name === '@CRUDController.action') {
                    $argName = $tag->args[0] ?? null;
                    $argOpts = $tag->args[1] ?? [];

                    if (defined('DEBUG') && DEBUG) {
                        if (!is_string($argName)) {
                            throw new \Fubber\CodingErrorException("$forMethod: @CRUDController.action parameter 1: Must be a string identifying this api for this controller.");
                        }
                        $knownOptions = ['title', 'payload', 'description', 'note', 'method', 'methods', 'response'];
                        foreach ($argOpts as $k => $v) {
                            if (!in_array($k, $knownOptions)) {
                                throw new \Fubber\CodingErrorException("$forMethod: @CRUDController.action parameter 2: Unknown option '$k'.");
                            }
                        }
                    }

                    $action = [
                        'title' => $argOpts['title'] ?? $parsed->title,
                        'note' => $argOpts['note'] ?? trim($parsed->description) ?? $argOpts['description'] ?? 'No further description',
                        'description' => $argOpts['description'] ?? $parsed->description ?? 'Not described',
                        'name' => $argName,
                        'response' => $argOpts['response'] ?? null,
                        ];

                    // Must be strings
                    if (defined('DEBUG') && DEBUG) {
                        foreach (['name', 'title', 'description', 'note'] as $key) {
                            if ($action[$key] !== null && !is_string($action[$key])) {
                                throw new \Fubber\CodingErrorException("$forMethod: @CRUDController.action parameter 2: Option '$key' must be a string.");
                            }
                        }
                    }

                    if (isset($argOpts['method'])) {
                        if (!is_string($argOpts['method'])) {
                            throw new \Fubber\CodingErrorException("$forMethod: @CRUDController.action parameter 2: Option 'method' must be a string. Did you mean to use 'methods'?");
                        }
                        $action['method'] = $argOpts['method'];
                    } elseif (isset($argOpts['methods'])) {
                        if (!is_array($argOpts['methods'])) {
                            throw new \Fubber\CodingErrorException("$forMethod: @CRUDController.action parameter 2: Option 'methods' must be an array like '[\"GET\",\"POST\"]' Did you mean to use 'method'?");
                        }
                        $action['method'] = implode("|", $argOpts['methods']);
                    } else {
                        $action['method'] = 'GET|POST';
                    }

                    $target = 'collection';
                    $paramCandidates = [];
                    foreach ($method->getParameters() as $i => $param) {
                        if ($i === 0 && ( ($param->hasType() && $param->getType()->getName()===State::class) || $param->getName() === 'state') ) {
                            // Ignore the state parameter if it is the first parameter
                        } elseif ( $param->hasType() && $param->getType()->getName() === static::_getVal('model') ) {
                            $paramCandidates[] = $param->getName();
                        } elseif ( !$param->hasType() ) { //&& $param->getName() == 'id' ) { // $param->hasType() && $param->getType()->getName() === static::_getVal('model') ) {
                            $paramCandidates[] = $param->getName();
                        } else {
                            throw new \LogicException("$forMethod: @CRUDController.action: Method has parameter '\$".$param->getName()."' which won't be available.");
                        }
                    }
//                    $action['name'] = $tag->args[1]['title'] ?? $tag->args[0];
                    $actionName = $argName; //json_decode($tag->args[0]);

                    if (sizeof($paramCandidates) === 1) {
                        $action['route'] = 'api_'.$api_name.'_'.$actionName;
                        $action['path'] = $api_path.'/'.$paramCandidates[0].'/'.$actionName.'!';
                    } elseif (sizeof($paramCandidates) === 0) {
                        $action['route'] = 'api_'.$api_name.'_'.$actionName;
                        $action['path'] = $api_path.'/'.$actionName.'!';
                    } else {
                        throw new \Fubber\CodingErrorException("$forMethod: @CRUDController.action: Method has parameters '\$".implode(", \$", $paramCandidates)." which won't be available.");
                    }
//                    $action['methods'] = 'POST|GET';
                    $action['callback'] = [ static::class, $method->getName() ];

                    $actions[] = $action;
                }

            }

        }
        return $actions;
    }
    
    /**
     * Lists all items in the collection
     */
    public static function api_list(State $state) {
        $state->json = true;
        if(!static::canList($state)) {
            throw new \Fubber\UnauthorizedException();
        }
        $rows = static::getRows($state);
        
        $get = $state->get;
        $model = static::_getVal('model', $state);
        $cols = $model::getColumnNames();
//        $foreignKeys = $model::$_foreignKeys;
        $foreignKeys = $model::getForeignKeys();
        foreach($cols as $col) {
            if(isset($get[$col])) {
                $val = $get[$col];
                if(is_array($val)) {
                    foreach($val as $op => $v) {
                        $rows->where($col, $op, $v);
                    }
                } else {
                    $rows->where($col,'=',$val);
                }
            }
        }
        if(isset($get['l']) && intval($get['l'])>1000)
            throw new \Exceptions\AccessDeniedException("Not possible to fetch more than 1000 rows per query");
        if(isset($get['l']) && isset($get['o']))
            $rows->limit($get['l'], $get['o']);
        else if(isset($get['l']))
            $rows->limit($get['l']);
        else if(isset($get['o']))
            $rows->limit(1000, $get['o']);
        
        if(isset($get['s'])) {
            if(in_array($get['s'], $cols)) {
                $rows->order($get['s']);
            }
        }
        
        $includes = null;
        if(isset($get['include'])) {
            $includes = explode(",", $get['include']);
            foreach($includes as $include) {
                if(!isset($foreignKeys[$include]))
                    throw new \Exceptions\AccessDeniedException("Can't include '".$include."'. No such foreign key.");
            }
        }
        
        $res = [];
        foreach($rows as $row) {
            if($includes) {
                $original = $row;
                $row = $row->jsonSerialize();
                $row['_includes'] = [];
                foreach($includes as $include) {
                    $className = $foreignKeys[$include];
                    $extraRow = $className::load($original->$include);
                    $row['_includes'][$include] = $extraRow;
                }
            }
            $res[] = $row;
        }

        $res = static::api_list_filter($state, $res);
        $res = Hooks::filter(static::class.'::api_list', $res, $state);
        return $state->json($res);
        return $state->json(static::api_list_filter($state, $res));
    }
    
    public static function api_list_filter(State $state, $res) {
        return $res;
    }
    
    /**
     * Create a new item, return created item json (as if fetched with api_item_retrieve)
     */
    public static function api_create(State $state) {
        $state->json = true;
        if(!static::canCreate($state))
            throw new \Fubber\UnauthorizedException("Access Denied");
        $post = $state->post;
        $row = static::_getVal('model', $state)::create($state);
        $create_def = static::_getVal('create_def', $state);
        $get_fields = function($def) use(&$get_fields) {
            $fields = [];
            foreach($def as &$item) {
                if(is_array($item)) {
                    if(!empty($item[0]) && $item[0]=='field') {
                        $fields[] = $item;
                    } else {
                        $fields = array_merge($fields, $get_fields($item));
                    }
                }
            }
            return $fields;
        };
        $fields = $get_fields($create_def);

        foreach($fields as $field) {
            $fieldName = $field[1];
            if(isset($post[$fieldName]))
                $row->$fieldName = $post[$fieldName];
        }
        $res = $row->save();
        if($res) {
            return $state->json(static::api_create_filter($state, $row));
        } else {
            return $state->json(['error' => 'Unable to create']);
        }
    }
    
    public static function api_create_filter(State $state, $rows) {
        return $rows;
    }
    
    /**
     * Retrieve one item as a json
     */
    public static function api_item_retrieve(State $state, $id) {
        $get = $state->get;
        $state->json = true;
        $row = static::getRow($state, $id);
        if(!$row) {
            var_dump($id); die("CRUDController 123");
            //echo "<pre>";var_dump(debug_backtrace());die();
            throw new \Exceptions\NotFoundException($id.' not found', 404);
        }
        if(!static::canView($state, $row))
            throw new \Fubber\UnauthorizedException('View access denied', 403);

        $model = static::_getVal('model', $state);
        $foreignKeys = $model::getForeignKeys();
/*
        if (property_exists($model, "_foreignKeys")) {
            $foreignKeys = $model::$_foreignKeys;
        } else {
            $foreignKeys = [];
        }
*/
        $includes = null;
        if(isset($get['include'])) {
            $includes = explode(",", $get['include']);
            foreach($includes as $include) {
                if(!isset($foreignKeys[$include]))
                    throw new \Exceptions\AccessDeniedException("Can't include '".$include."'. No such foreign key.");
            }
            $row = $row->jsonSerialize();
            foreach($includes as $include) {
                if(isset($row[$include])) {
                    $c = $foreignKeys[$include];
                    $i = $c::load($row[$include]);
                    if($i->canView($state))
                        $row['_'.$include] = $i;
                }
            }
        }

//        $res = static::api_item_retrieve_filter($state, $row);
        $res = Hooks::filter(static::class.'::api_item_retrieve', $row, $state);
//        return $state->json($res);
        return $state->json(static::api_item_retrieve_filter($state, $row));
    }

    public static function api_item_retrieve_filter(State $state, $row) {
        return $row;
    }

    /**
     * Create or replace the item in the collection
     */
    public static function api_item_replace(State $state, $id) {
        $state->json = true;
        throw new \Fubber\UnauthorizedException("Not implemented");
    }

    /**
     * Update values in the item
     */
    public static function api_item_update(State $state, $id) {
        $state->json = true;
        $returnValue = null;
        extract(Hooks::filter(static::class.'::api_item_update', ['state' => $state, 'id' => $id]), \EXTR_IF_EXISTS);
        if ($returnValue !== null) {
            return $returnValue;
        }
        if (!$state->post) {
            throw new \Fubber\BadRequestException("Missing post data");
        }
        $row = static::getRow($state, $id);
        if(!$row) {
            throw new \Exceptions\NotFoundException('Resource not found');
        }
        $state->response->noCache();
        if(!static::canEdit($state, $row)) {
            throw new \Fubber\UnauthorizedException("Access Denied");
        }
        try {
            $meta = static::getMeta($state, $row);
        } catch (MissingMetaException $e) {}

        $edit_def = static::_getVal('edit_def', $state, false, [
            // extra data can be used to customize edit fields
            'id' => $id,
            'row' => $row,
        ]);

        // find field names used in edit_def
        $recurse = function(&$def) use (&$fields, &$recurse) {
            foreach($def as &$item) {
                if(is_array($item)) {
                    if($item[0]=='field' && empty($item['readonly'])) {
                        $fields[] = $item[1];
                    } else {
                        $recurse($item);
                    }
                }
            }
        };

        $fields = [];
        $recurse($edit_def);
        $postValues = [];
        foreach ($state->post as $field => $value) {
            if (!in_array($field, $fields)) {
                throw new \Fubber\BadRequestException("Unexpected field '$field' in request");
            }
        }
        foreach ($fields as $field) {
            if (isset($state->post[$field])) {
                if ($meta && $meta->col($field)) {
                    if ($meta->col($field)->isReadOnly()) {
                        throw new \Fubber\AccessDeniedException("Access denied updating read-only field '$field'");
                        // cant update read-only fields
                        continue;
                    }
                }
                if ($field == static::getPrimaryKey()) {
                    throw new \Fubber\BadRequestException("Unexpected field '$field' in request");
                    // cant update primary key
                    continue;
                }
                $postValues[$field] = $state->post[$field];
            }
        }

        foreach ($postValues as $fieldName => $value) {
            $row->$fieldName = $value;
        }

        if ($errors = $row->isInvalid()) {
            throw new ValidationException($errors);
        }
        $result = $row->save();

        $pk = static::getPrimaryKey();
        if ($result) {
            return Hooks::filter(static::class.'::api_item_update()', static::api_item_retrieve($state, $row->$pk), $state, $row);
        }
        return Hooks::filter(static::class.'::api_item_update()-not-modified', static::api_item_retrieve($state, $row->$pk), $state, $row);
        throw new \Fubber\InternalErrorException("Unable to save (".serialize($result).")");
    }

    /**
     * Delete the item
     */
    public static function api_item_delete(State $state, $id) {
        $state->json = true;
        extract(Hooks::filter(static::class.'::api_item_delete', ['state' => $state, 'id' => $id]), \EXTR_IF_EXISTS);
        $row = static::getRow($state, $id);
        if(!$row) {
            throw new \Exceptions\NotFoundException("Resource not found");
        }
        if(!static::canDelete($state, $row)) {
            throw new \Fubber\UnauthorizedException("You're not authorized to delete this resource");
        }

        $result = $row->delete();
        return Hooks::filter(static::class.'::api_item_delete()', $state->json(['result' => $result]), $state, $row);
    }

    public static function index(State $state) {
        self::setState($state);
        try {
            $state->response->noCache();
            $get = $state->get;
            if(!static::canList($state)) {
                throw new \Fubber\UnauthorizedException("Access Denied");
            }

            $title = static::_getVal('index_title', $state);
            if($title == '$index_title') {
                $title = static::getMeta($state)->getPluralName();
            }

            $tableSourceParams = $get;
    /*
            $model = static::_getVal('model', $state);
            $cols = $model::$_cols;
            $foreignKeys = $model::$_foreignKeys;
            foreach($cols as $col) {
                if(isset($get[$col])) {
                    $val = $get[$col];
                    if(is_array($val)) {
                        foreach($val as $op => $v) {
                            $tableSourceParams['wheres'][] = [$col, $op, $v];
                        }
                    } else {
                        $tableSourceParams['wheres'][] = [$col, '=', $val];
                    }
                }
            }
            if(isset($get['l']) && intval($get['l'])>1000)
                throw new \Exceptions\AccessDeniedException("Not possible to fetch more than 1000 rows per query");
            if(isset($get['l']) && isset($get['o']))
                $tableSourceParams['limit'] = [$get['l'], $get['o']];
            else if(isset($get['l']))
                $tableSourceParams['limit'] = [$get['l']];
            else if(isset($get['o']))
                $tableSourceParams['limit'] = [1000, $get['l']];
            
            if(isset($get['o'])) {
                if(in_array($get['o'], $cols))
                    $tableSourceParams['order'] = $get['o'];
            }
    */
        return $state->view(static::_getVal('index_template', $state), [
                'controller' => static::class,
                'dialog' => !!$state->dialog,
                'pick' => !!$state->pick,
                'page_class' => static::createPageClass('index'),
                'title' => $title,
                'text' => static::interpolateString(static::_getVal('index_text', $state), null, false),
                'tableSource' => 'index_'.str_replace(['-', "/"], "_", static::_getVal('prefix', $state)),
                'tableSourceParams' => $tableSourceParams,
                'cols' => static::_getVal('index_cols', $state),
                'primaryKey' => static::getPrimaryKey(),
                'linkTemplate' => '/'.static::_getVal('prefix', $state).'/{id}/',
                'create_button' => static::canCreate($state) ? static::_getVal('index_add_button', $state) : false,
                'create_url' => route(static::_getVal('prefix', $state).'_create').'?'.http_build_query($get),
                'prefix' => static::_getVal('prefix', $state),
                ]);
        } finally {
            self::setState(null);
        }
    }
    public static function index_dialog(State $state) {
        $state->dialog = true;
        return static::index($state);
    }
    public static function index_pick(State $state) {
        $state->pick = true;
        return static::index_dialog($state);
    }

    public static function view_toolbar(State $state, $row) {
        return null;
    }
    
    public static function view(State $state, $id) {
        self::setState($state);
        try {
            $state->response->noCache();
            $row = static::getRow($state, $id);
            if(!$row) {
                throw new \Exceptions\NotFoundException('Resource not found');
            }
            
            if(!static::canView($state, $row)) {
                throw new \Fubber\UnauthorizedException();
            }

            $toolbar = static::view_toolbar($state, $row);

            $view_def = static::_getVal('view_def', $state, false, ['id' => $id, 'row' => $row]);
            if(!$view_def) {
                $view_def = static::_getVal('edit_def', $state, false, ['id' => $id, 'row' => $row]);
            }
                
            if($view_def) {
                $meta = static::getMeta($state);

                $buttons = ['buttons', []];
                if(static::canEdit($state, $row))
                    $buttons[1][] = ["button", static::interpolateString(static::_getVal('view_edit_button', $state), $row), 'attrs' => ['class' => 'edit']];
                if(static::canDelete($state, $row))
                    $buttons[1][] = ["button", static::interpolateString(static::_getVal('view_delete_button', $state), $row), 'attrs' => ['class' => 'delete']];
                $buttons[1][] = ["button", static::interpolateString(static::_getVal('view_back_button', $state), $row), 'attrs' => ['class' => 'back']];
                $view_def[] = $buttons;

                $view_title = static::interpolateString(static::_getVal('view_title', $state), $row);
                if($view_title == '$view_title') {
                    $view_title = static::getMeta($state)->row($row)->getDisplayName();
                }

                return $state->view(static::_getVal('view_template', $state), [
                    'controller' => static::class,
                    'dialog' => !!$state->dialog,
                    'page_class' => static::createPageClass('view'),
                    'title' => $view_title,
                    'text' => static::interpolateString(static::_getVal('view_text', $state), $row, false),
                    'row' => $row,
                    'view_def' => $view_def,
                    'meta' => $meta,
                    'edit_button' => (static::canEdit($state, $row) ? static::interpolateString(static::_getVal('view_edit_button', $state), $row) : false),
                    'edit_url' => route(static::_getVal('prefix', $state).'_edit', $id), 
                    'delete_button' => (static::canDelete($state, $row) ? static::interpolateString(static::_getVal('view_delete_button', $state), $row) : false),
                    'delete_url' => route(static::_getVal('prefix', $state).'_delete', $id),
                    'back_button' => static::interpolateString(static::_getVal('view_back_button', $state), $row),
                    'back_url' => route(static::_getVal('prefix', $state)),
                    'toolbar' => $toolbar ? $toolbar : null,
                    ]);
            }
            throw new DeprecatedException("CRUD controller must define 'view_def', 'view_cols' is deprecated.");
        } finally {
            self::setState(null);
        }
    }

    public static function view_dialog(State $state, $id) {
        $state->dialog = true;
        return static::view($state, $id);
    }
    
    public static function delete(State $state, $id) {
        self::setState($state);
        try {
            $state->response->noCache();
            $row = static::getRow($state, $id);
            if(!$row) {
                throw new \Exceptions\NotFoundException('Resource not found');
            }

            if(!static::canDelete($state, $row)) {
                throw new \Fubber\UnauthorizedException();
            }
            
            if($state->request->getMethod() == "POST") {
                $res = $row->delete();
                if($res) {
                    new FlashMessage( $state, static::interpolateString(static::_getVal('delete_success_message', $state), $row), FlashMessage::SUCCESS);
                    if($state->dialog) {
                        return $state->view('theme/dialog-result', ['result' => true]);
                    }
                    return $state->redirect( route(static::_getVal('prefix', $state)) );
                } else {
                    new FlashMessage( $state, static::interpolateString(static::_getVal('delete_failed_message', $state), $row), FlashMessage::ERROR);
                    if($state->dialog) {
                        $result = $state->view('theme/dialog-result', ['result' => false]);
                    } else {
                        $result = $state->redirect( route(static::_getVal('prefix', $state).'_view', $id) );
                    }
                    return $result;
                }
            }
            
            $delete_title = static::interpolateString(static::_getVal('delete_title', $state), $row);
            if($delete_title == '$delete_title') {
                $delete_title = trans("Delete ':name?'", ['name' => static::getMeta($state)->row($row)->getDisplayName()]);
            }
            
            $result = $state->view( static::_getVal('delete_template', $state), [
                'controller' => static::class,
                'dialog' => !!$state->dialog,
                'page_class' => static::createPageClass('delete'),
                'title' => $delete_title,
                'text' => static::interpolateString(static::_getVal('delete_text', $state), $row, false),
                'row' => $row,
                'confirm_button' => static::interpolateString(static::_getVal('delete_confirm_button', $state), $row),
                'back_button' => static::interpolateString(static::_getVal('delete_back_button', $state), $row),
                'back_url' => route(static::_getVal('prefix', $state).'_view', $id),
                ]);
            return $result;
        } finally {
            self::setState(null);
        }
    }

    public static function delete_dialog(State $state, $id) {
        $state->dialog = true;
        return static::delete($state, $id);
    }
    
    public static function edit_dialog(State $state, $id) {
        $state->dialog = true;
        return static::edit($state, $id);
    }

    public static function edit(State $state, $id) {
        self::setState($state);
        try {
            $state->response->noCache();
            $row = static::getRow($state, $id);
            try {
                $meta = static::getMeta($state, $row);
            } catch (MissingMetaException $e) {}
            if(!$row) {
                throw new \Exceptions\NotFoundException('Resource not found');
            }
            if(!static::canEdit($state, $row)) {
                throw new \Fubber\UnauthorizedException();
            }
            
            $edit_def = static::_getVal('edit_def', $state, false, ['id' => $id, 'row' => $row]);
    
            if(!$edit_def)
                $edit_def = static::_getVal('view_def', $state, false);
    
            if($edit_def) {
                $form = new Form($state, $row);
            }
            if($form->success !== null) {
                if ($form->success) {
                    new FlashMessage($state, static::interpolateString(static::_getVal('edit_save_message', $state), $row), FlashMessage::SUCCESS);
                    if($state->dialog) {
                        return $state->view('theme/dialog-result', ['result' => $row]);
                    } else {
                        return $state->redirect(route(static::_getVal('prefix', $state).'_view', $id));
                    }
                } else {
                    new FlashMessage($state, trans("Nothing changed"), FlashMessage::INFO); //static::interpolateString(static::_getVal('edit_save_message', $state), $row), FlashMessage::INFO);
                    if($state->dialog) {
                        return $state->view('theme/dialog-result', ['result' => null]);
                    } else {
                        return $state->redirect(route(static::_getVal('prefix', $state).'_view', $id));
                    }
                }
            }
            
            $title = static::interpolateString(static::_getVal('edit_title', $state), $row);
            if($title == '$edit_title') {
                $title = trans("Editing: :name", [ 'name' => $meta->row($row)->getDisplayName() ]);
            }
            
            return $state->view( static::_getVal('edit_template', $state), [ 
                'controller' => static::class,
                'dialog' => !!$state->dialog,
                'page_class' => static::createPageClass('edit'),
                'title' => $title,
                'text' => static::interpolateString(static::_getVal('edit_text', $state), $row, false),
                'row' => $row,
                'form' => $form,
                'edit_def' => array_merge($edit_def, [["buttons", [
                    ["submit", static::interpolateString(static::_getVal('edit_save_button', $state), $row), 'attrs' => ['class' => 'submit']],
                    ["button", static::interpolateString(static::_getVal('edit_back_button', $state), $row), 'attrs' => ['class' => 'back']],
                    ]]]),
                'save_button' => static::interpolateString(static::_getVal('edit_save_button', $state), $row),
                'back_button' => static::interpolateString(static::_getVal('edit_back_button', $state), $row),
                'back_url' => route(static::_getVal('prefix', $state).'_view', $id),
                ]);
        } finally {
            self::setState(null);
        }
    }
    
    public static function create(State $state) {
        self::setState($state);
        try {
            $state->response->noCache();
            if(!static::canCreate($state)) {
                throw new \Fubber\UnauthorizedException();
            }
            $row = static::_getVal('model', $state)::create($state);

            $create_def = static::_getVal('create_def', $state, false, ['row' => $row]);

            if($create_def) {
                $get = $state->get;
                // Make a field read only. Returns false if the field was already read only, true if successful and null if field was not found
                $recurse = function(&$def, $name) use (&$recurse) {
                    foreach($def as &$item) {
                        if(is_array($item)) {
                            if($item[0]=='field' && $item[1]==$name) {
                                if (!empty($item['readonly'])) {
                                    return false;
                                } else {
                                    $item['readonly'] = true;
                                    return true;
                                }
                            } else {
                                $result = $recurse($item, $name);
                                if ($result !== null) {
                                    return false;
                                }
                            }
                        }
                    }
                    return null;
                };

                // The get request can be used to pre-seed the value
                foreach(static::getMeta($state)->getCols() as $key => $col) {
                    if(isset($get[$key])) {
                        $result = $recurse($create_def, $key);
                        if ($result === true) {
                            // Only allow overriding fields that are not read-only
                            $row->$key = $get[$key];
                        } elseif ($result === false) {
                            throw new \Fubber\BadRequestException("Field '$key' is read only");
                        }
                    }
                }
                
                $form = new Form($state, $row);
            } else {
                try {
                    $form_spec = static::_getVal('create_form_spec', $state, false);
                    if(!$form_spec) $form_spec = static::_getVal('edit_form_spec', $state);    
                } catch (\Throwable $e) {
                    throw new CodingErrorException("No `create_def` configured in CRUDController");
                }
                
                foreach($form_spec as $k => $spec) {
                    $fieldName = $spec[0];
                    if(isset($state->get[$fieldName])) {
                        unset($form_spec[$k]);
                        $row->$fieldName = $state->get[$fieldName];
                    }
                }
                
                $form = new CRUDForm($state, $row, $form_spec);
            }
            
            if($form->success !== null) {
                if ($form->success) {
                    new FlashMessage($state, static::interpolateString(static::_getVal('create_save_message', $state), $row));
                    if($state->dialog) {
                        return $state->view('theme/dialog-result', ['result' => $row]);
                    }
                    return $state->redirect(route(static::_getVal('prefix', $state).'_view', $row->id));
                } else {
                    new FlashMessage($state, trans("Unable to create"), FlashMessage::ERROR);
                }
            }
            
            $create_title = static::interpolateString(static::_getVal('create_title', $state), $row);
            if($create_title == '$create_title') {
                $create_title = trans("Create :name", ['name' => static::getMeta($state)->getSingularName()]);
            }

            return $state->view( static::_getVal('create_template', $state), [
                'controller' => static::class,
                'dialog' => !!$state->dialog,
                'page_class' => static::createPageClass('create'),
                'title' => $create_title,
                'text' => static::interpolateString(static::_getVal('create_text', $state), $row, false),
                'row' => $row,
                'form' => $form,
                'create_def' => array_merge($create_def, [["buttons", [
                    ["submit", static::interpolateString(static::_getVal('create_save_button', $state), $row), 'attrs' => ['class' => 'submit']],
                    ["button", static::interpolateString(static::_getVal('create_back_button', $state), $row), 'attrs' => ['class' => 'back']],
                    ]]]),
                'save_button' => static::interpolateString(static::_getVal('create_save_button', $state), $row),
                'back_button' => static::interpolateString(static::_getVal('create_back_button', $state), $row),
                'back_url' => route(static::_getVal('prefix', $state)),
                ]);
        } finally {
            self::setState(null);
        }
    }
    public static function create_dialog(State $state) {
        $state->dialog = true;
        return static::create($state);
    }
    
    
    public static function index_tablesource(State $state) {
        self::setState($state);
        try {
            // $filter = new Meta\Filter($state);
            $state->response->noCache();
            if(!static::canList($state)) {
                throw new \Fubber\UnauthorizedException(trans("Access Denied listing the {singularName} data source", [
                    'singularName' => static::getMeta($state)->getSingularName()
                ]));
            }
            
            $rows = static::getRows($state);
            $rows->parseQuery($state->get);
            return [
                'model' => static::_getVal('model'),
                'cols' => static::_getVal('index_cols', $state),
                'records' => $rows,
            ];
        } finally {
            self::setState(null);
        }
    }
    
    /**
     * Parse a string and use values from the $row object as placeholders.
     */
    protected static function interpolateString(?string $template, ?ITable $row, bool $throws=true): string {
        if ($template === null) {
            if ($throws) {
                throw new CodingErrorException("Expecting a configured text string");
            } else {
                return '';
            }
        } else {
            $string = $template;
        }
        $map = [];
        if ($row !== null) {
            $className = static::_getVal('model');
            $cols = $className::getColumnNames();
            foreach($cols as $col) {
                $map[':'.$col] = $row->$col;
                $map['{'.$col.'}'] = $row->$col;
            }
        }
        $meta = static::getMeta(self::$state);
        $extraVars = [
            '_displayName' => $row ? $meta->row($row)->getDisplayName() : $meta->getSingularName(),
            '_plural' => $meta->getPluralName(),
            '_singular' => $meta->getSingularName(),
            '_description' => $meta->getDescription(),
            '_class' => $meta->getClassName(),
        ];
        foreach ($extraVars as $name => $value) {
            $map[':'.$name] = $value;
            $map['{'.$name.'}'] = $value;
        }

        return strtr($string, $map);
    }

    protected static function getPrimaryKey() {
        $className = static::_getVal('model');
        $pk = $className::getPrimaryKeyNames();
        if (isset($pk[1])) {
            throw new CodingErrorException("CRUDController does not support models with complex primary keys");
        }
        return $pk[0];
    }

    protected static function getRows(State $state) {
        $className = static::_getVal('model');
        return $className::all();
    }

    protected static function getRow(State $state, $id) {
        $className = static::_getVal('model');
        return $className::load($id);
    }

    /**
     * @param State $state 
     * @param ITable|null $row 
     * @return Meta|null 
     * @throws ExceptionsCodingErrorException 
     * @throws CodingErrorException 
     * @throws TypeError 
     * @throws MissingMetaException 
     */
    protected static function getMeta(State $state, ITable $row=null) {
        return Meta::create($state, static::_getVal('model'));
    }
    protected static function createPageClass($page) {
        return 'model-'.str_replace('\\', '-', strtolower(static::_getVal('model'))).' prefix-'.str_replace("/","-",static::_getVal('prefix')).' '.$page.' ';
    }

    public static function form_field(State $state, \Fubber\Forms\Form $form, $name, $attrs) {
        $value = $form->getValue($name);
        $id = $form->getId($name, true);
        $meta = static::getMeta($state);

        if(substr($name, -2) === '[]') {
            $options = $form->getOptions(substr($name, 0, -2));
        } else {
            $options = $form->getOptions($name);
        }
        if($value && ($row = static::getRow($state, $value))) {
            $displayName = $meta->row($row)->getDisplayName();
        } else {
            $row = null;
            $value = null;
            $displayName = '';
        }
        
        $pickRoute = $meta->getRoute('pick_dialog').(is_array($options) ? '?'.http_build_query($options) : '' );;
        if(static::canCreate($state))
            $createRoute = $meta->getRoute('create_dialog').(is_array($options) ? '?'.http_build_query($options) : '' );
        else 
            $createRoute = null;
            
        $viewRoute = $meta->getRoute('view_dialog', ':id');

        $domNode = new DomNode(new Tag('input', \Fubber\Forms\Form::mergeAttrs($attrs, [
            'type' => 'text',
            'id' => $id,
            'name' => $name,
            'value' => $value,
            'class' => 'f-form f-controller-field',
            'data-picker' => $pickRoute,
            'data-creater' => $createRoute,
            'data-view-route' => $viewRoute,
            'data-current-string' => $displayName,
            'data-options' => json_encode($options),
            ])));
        return $domNode;
    }

    private static function setState(?State $state): void {
        if ($state !== null && self::$state !== null) {
            throw new CodingErrorException("setState(state) was called while a state already is active. Careful, don't allow a state object to remain after a previous call!!!");
        } elseif ($state === null && self::$state === null) {
            // not sure if this is helpful, so we'll keep it if it doesn't cause problems.
            throw new CodingErrorException("setState(NULL) was called to clear state, but there was no existing state. Careful! State management MUST be perfect for security reasons.");
        }
        self::$state = $state;
    }
}
