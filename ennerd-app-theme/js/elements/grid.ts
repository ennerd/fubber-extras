import {LitElement, html, css} from 'lit';
import {customElement, property, state} from 'lit/decorators.js';

@customElement("eat-grid")
export class Grid extends LitElement {

    static styles = css`
        :host {
            border: 1px solid red;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 32000;
        }
    `;

    @property() title = "Title";
    @property() dataSource: string;

    render() {
        return html`
            <div>Hello ${this.name}</div>
        `;
    }
}
