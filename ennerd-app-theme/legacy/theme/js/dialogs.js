Theme.Dialog = (function() {
    
    var dialogs = {
        zIndex: 30000,
        $iframes: [],
        $topOverlay: null,
        $bottomOverlay: null
    };

    function isDialog() {
        return !!(parent && parent !== window && parent.Theme);
    }
    
    function getHeight() {
        // If called from within a dialog, we use the parent window API instead of the local
        if(parent && parent !== window && parent.Theme) {
            return parent.Theme.Dialog.height();
        }
        if(!dialogs || !dialogs.$iframes || dialogs.$iframes.length === 0)
            throw "No dialogs open";
        return dialogs.$iframes[dialogs.$iframes.length - 1]._handler.height;
    }

    /**
     * Called when you want to close the current dialog window:
     * Theme.Dialog.close("return value");
     */
    function close(returnValue) {
        // If called from within a dialog, we use the parent window API instead of the local
        if(parent && parent !== window && parent.Theme) {
            return parent.Theme.Dialog.close(returnValue);
        }
        if(dialogs.$iframes.length === 0) {
            throw "No dialogs open";
        }
        // Call the close handler
        return dialogs.$iframes[dialogs.$iframes.length - 1]._handler.close(returnValue);
    }
    
    /**
     * Called when you want to open a dialog window:
     * var handler = Theme.Dialog.open(someUrl, height, onCloseCallback)
     * 
     * Force close the dialog from the parent window
     * handler.close("return value");
     * 
     * Legal heights include:
     * compact, medium, tall, full, pst30, pst40, pst50, pst60, pst70, pst80, pst90, pst100, px200, px300, px400, px500, px600, px700
     * 
     * You can also specify a height using standard css syntax such as 60%, 500px, 20rem etc.
     * A height of 53.25vw gives a dialog with aspect ratio 16:9.
     */
    function open(url, height, callback) {
        // If called from within a dialog, we use the parent window API instead of the local
        if(parent && parent !== window && parent.Theme) {
            return parent.Theme.Dialog.open(url, height, callback);
        }
        if(!height) {
            height = 'full';
        }
        
        var $i = $("<iframe />").addClass('dialog').attr('src', url);
        dialogs.$iframes.push($i);
        window.requestAnimationFrame(reflow);
        $i.css('z-index', dialogs.zIndex--);
        $(document.body).append($i);
        setTimeout(function() {
            if(!isNaN(parseFloat(height))) {
                $i.css('height', height);
            } else {
                $i.addClass(height);
            }
        }, 100);
        var index = dialogs.$iframes.length;
        var handler = {
            index: index,
            callback: callback,
            height: height,
            close: function(returnValue) {
                if(index == dialogs.$iframes.length) {
                    $i.remove();
                    dialogs.$iframes.pop();
                    if(callback) callback(returnValue);
                }
            }
        };
        $i._handler = handler;
        return handler;
    }
    
    /**
     * Replaces the current dialog window with a new, but keeps the callback to 
     * the parent window. This is equivalent to navigating inside the dialog
     * window, except you get an effect and you can change the dialog window height.
     */
    function replace(url, height) {
        // If called from within a dialog, we use the parent window API instead of the local
        if(parent && parent !== window && parent.Theme) {
            return parent.Theme.Dialog.replace(url, height);
        }
        if(dialogs.$iframes.length === 0) {
            throw "No dialogs open";
        }

        // Fetch the topmost dialog
        var $iframe = dialogs.$iframes.pop();

        // Remove the iframe
        $iframe.remove();
        
        open(url, height, $iframe._handler.callback);
        return;
    }
    //function _replace(url, height, callback)
    
    function reflow() {
        if(parent !== window) {
            // Let's not do this inside dialogs as well. Takes a lot of CPU.
            return;
        }
        if(dialogs.$iframes.length === 0) {
            if(dialogs.$topOverlay) {
                dialogs.$topOverlay.remove();
                dialogs.$topOverlay = null;
            }
            if(dialogs.$bottomOverlay) {
                dialogs.$bottomOverlay.remove();
                dialogs.$bottomOverlay = null;
            }
            return;
        } else {
            window.requestAnimationFrame(reflow);
        }
        if(!dialogs.$topOverlay) {
            dialogs.$topOverlay = $("<div />").addClass('overlay').addClass('top');
            $(document.body).append(dialogs.$topOverlay);
            dialogs.$topOverlay.click(function() {
                Theme.Dialog.close(null);
            });
            
        }
        if(!dialogs.$bottomOverlay) {
            dialogs.$bottomOverlay = $("<div />").addClass('overlay').addClass('bottom');
            $(document.body).append(dialogs.$bottomOverlay);
        }
        
        // Calculate combined height of dialogs
        var height = 0;
        for(var i = dialogs.$iframes.length - 1; i >= 0; i--) {
            if(height == 0) {
                height += dialogs.$iframes[i].height();
            } else {
                height += 50; // Parent dialogs is only 50 px tall
            }
        }
        // Calculate offset from the top (minus the banner)
        var topOffset = $(window).height() - height;
        if(topOffset < 50) topOffset = 50;
        var topMost = 10000;
        var secondMost = 10000;
        var count = 0;
        for(var i = dialogs.$iframes.length - 1; i >= 0; i--) {
            var currentTop = dialogs.$iframes[i].offset().top - $(window).scrollTop();
            var newTop = Math.floor(topOffset * 0.3 + currentTop * 0.7);
            if(newTop < topMost) topMost = newTop;
            dialogs.$iframes[i].css('top', newTop + "px");
            if(count === 0) {
                topOffset += dialogs.$iframes[i].height();
            } else {
                if(count === 1) {
                    secondMost = newTop;
                }
                topOffset += 50;
            }
            count++;
        }
        dialogs.$topOverlay.css('height', topMost + "px");
        dialogs.$bottomOverlay.css('top', secondMost + 'px');
        dialogs.$bottomOverlay.css('height', ($(window).height() - secondMost) + "px")
    }

    reflow();

    return {
        open: open,
        close: close,
        replace: replace,
        height: getHeight,
        isDialog: isDialog
    };
}());
