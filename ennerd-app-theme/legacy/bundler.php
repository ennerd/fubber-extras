<script src="https://cdn.jsdelivr.net/npm/es5-shim@4/es5-shim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-shim@0.35.6/es6-shim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartist@1/dist/index.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6/js/all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/luxon@3/build/global/luxon.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-ui@1/dist/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/inputmask@5/dist/jquery.inputmask.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/toastr@2/build/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/chartist@1/dist/index.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-ui@1/themes/base/all.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/toastr@2/build/toastr.min.css">
