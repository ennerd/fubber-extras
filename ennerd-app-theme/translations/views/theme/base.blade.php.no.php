<?php return array (
  'Seach' => 'Søk:',
  'Show' => 'Sidestørrelse:',
  'Pages' => 'Sider:',
  'Preus' => 'Forrige',
  'Next' => 'Neste',
  'Prong' => 'Venter...',
  'Shong' => 'Viser',
  'filedfromrecaltotalrecds' => ' (filtrert fra {recordsTotal} rader)',
  'recntcolme' => '{recordsQueryCount} rader',
  'recwnof' => '{recordsShown} av',
  'pagndtopagndof' => '{pageLowerBound} til {pageUpperBound} av',
  'en' => 'en',
);
